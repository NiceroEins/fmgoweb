export interface IArticleData {
  id: number
  status: string
  title: string
  abstractContent: string
  fullContent: string
  sourceURL: string
  imageURL: string
  timestamp: string | number
  platforms: string[]
  disableComment: boolean
  importance: number
  author: string
  reviewer: string
  type: string
  pageviews: number
}

export interface IOrgData {
  id: number
  name: string
  created_at: string
  updated_at: string
}

export interface ISelect {
  select:string
  context:string
}

export interface IQuestionData {
  id: number
  model_id: number
  question: string |undefined
  selects: Array<ISelect>
  answer: string
  type: string
}

export interface IUsers {
  id: number
  nick_name: string
  organization: string
  head_img: string
  total_count: number
  right_count: number
  wrong_count: number
}

export interface IRoleData {
  key: string
  name: string
  description: string
  routes: any
}

export interface ITransactionData {
  orderId: string
  timestamp: string | number
  username: string
  price: number
  status: string
}

export interface IUserData {
  id: number
  username: string
  password: string
  name: string
  email: string
  phone: string
  avatar: string
  introduction: string
  roles: string[]
}

export interface IQuestionIPO {
  question:string
  a:ISelect
  b:ISelect
  c:ISelect
  answer:string
}

export interface IQuestionIPOArr {
  arr:Array<IQuestionIPO>
  model_id:number
  type:string
}
