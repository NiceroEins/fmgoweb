import request from '@/utils/request'

export const getList = (url:string, params: any) =>
  request({
    url: url,
    method: 'get',
    params
  })

export const getOne = (url:string, params: any) =>
  request({
    url: url,
    method: 'get',
    params
  })

export const create = (url:string, data: any) =>
  request({
    url: url,
    method: 'post',
    data
  })

export const update = (url:string, data: any) =>
  request({
    url: url,
    method: 'put',
    data
  })

export const deleteOne = (url:string, data: any) =>
  request({
    url: url,
    method: 'delete',
    data
  })

export const getPageviews = (params: any) =>
  request({
    url: '/pageviews',
    method: 'get',
    params
  })
