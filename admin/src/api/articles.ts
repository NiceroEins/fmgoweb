import request from '@/utils/request'
import {IArticleData, IOrgData} from './types'

export const defaultArticleData: IArticleData = {
  id: 0,
  status: 'draft',
  title: '',
  fullContent: '',
  abstractContent: '',
  sourceURL: '',
  imageURL: '',
  timestamp: '',
  platforms: ['a-platform'],
  disableComment: false,
  importance: 0,
  author: '',
  reviewer: '',
  type: '',
  pageviews: 0
}

export const defaultOrgData: IOrgData = {
  id: 0,
  name: '',
  created_at: '',
  updated_at: ''
}

export const getArticles = (params: any) =>
  request({
    url: '/organizations',
    method: 'get',
    params
  })

export const getArticle = (id: number, params: any) =>
  request({
    url: `/articles/${id}`,
    method: 'get',
    params
  })

export const createArticle = (data: any) =>
  request({
    url: '/add_org',
    method: 'post',
    data
  })

export const updateArticle = (data: any) =>
  request({
    url: '/edit_org',
    method: 'put',
    data
  })

export const deleteArticle = (data: any) =>
  request({
    url: '/del_org',
    method: 'delete',
    data
  })

export const getPageviews = (params: any) =>
  request({
    url: '/pageviews',
    method: 'get',
    params
  })
