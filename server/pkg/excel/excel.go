package excel

import (
	"datacenter/models/template"
	"github.com/360EntSecGroup-Skylar/excelize"
	"io"
)

type Tag struct {
	ID         int
	Name       string
	CreatedBy  string
	ModifiedBy string
	State      int

	PageNum  int
	PageSize int
}

//导入excel
func Import(r io.Reader) error {
	xlsx, err := excelize.OpenReader(r)
	if err != nil {
		return err
	}

	rows := xlsx.GetRows("标签信息")
	for irow, row := range rows {
		if irow > 0 {
			var data []string
			for _, cell := range row {
				data = append(data, cell)
			}

			template.AddTag(data[1], 1, data[2])
		}
	}

	return nil
}

//func AddTag(name string, state int, createdBy string) error {
//	tag := Tag{
//		Name:      name,
//		State:     state,
//		CreatedBy: createdBy,
//	}
//	if err := db.Create(&tag).Error; err != nil {
//		return err
//	}
//
//	return nil
//}
