package gredis

import (
	"encoding/json"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"log"
)

//  String Set a key/value
func (r *RedisConn) Set(key string, data interface{}, time int) error {
	conn := r.pool.Get()
	defer conn.Close()

	value, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, err = conn.Do("SET", key, value)
	if err != nil {
		return err
	}

	if time > 0 {
		_, err = conn.Do("EXPIRE", key, time)
		if err != nil {
			return err
		}
	}

	return nil
}

// String Exists check a key
func (r *RedisConn) Exists(key string) bool {
	conn := r.pool.Get()
	defer conn.Close()

	exists, err := redis.Bool(conn.Do("EXISTS", key))
	if err != nil {
		return false
	}

	return exists
}

// String Get get a key
func (r *RedisConn) Get(key string) ([]byte, error) {
	conn := r.pool.Get()
	defer conn.Close()

	reply, err := redis.Bytes(conn.Do("GET", key))
	if err != nil {
		return nil, err
	}

	return reply, nil
}

func (r *RedisConn) GetString(key string) (string, error) {
	conn := r.pool.Get()
	defer conn.Close()

	reply, err := redis.String(conn.Do("GET", key))
	if err != nil {
		return "", err
	}

	return reply, nil
}

func (r *RedisConn) GetInt(key string) (int64, error) {
	conn := r.pool.Get()
	defer conn.Close()

	reply, err := redis.Int64(conn.Do("GET", key))
	if err != nil {
		return 0, err
	}

	return reply, nil
}

func (r *RedisConn) GetFloat(key string) (float64, error) {
	conn := r.pool.Get()
	defer conn.Close()

	reply, err := redis.Float64(conn.Do("GET", key))
	if err != nil {
		return 0, err
	}

	return reply, nil
}

// String Delete delete a kye
func (r *RedisConn) Delete(key string) (bool, error) {
	conn := r.pool.Get()
	defer conn.Close()

	return redis.Bool(conn.Do("DEL", key))
}

// String LikeDeletes batch delete
func (r *RedisConn) LikeDeletes(key string) error {
	conn := r.pool.Get()
	defer conn.Close()

	keys, err := redis.Strings(conn.Do("KEYS", "*"+key+"*"))
	if err != nil {
		return err
	}

	for _, key := range keys {
		_, err = Delete(key)
		if err != nil {
			return err
		}
	}

	return nil
}

//string 自增
func (r *RedisConn) IncrBy(key string, num int) int64 {
	conn := r.pool.Get()
	defer conn.Close()

	v, err := redis.Int64(conn.Do("INCRBY", key, num))
	if err != nil {
		log.Println("INCR failed:", err)
		return 0
	}
	return v
}

func (r *RedisConn) Expire(key string, time int64) {
	conn := r.pool.Get()
	defer conn.Close()

	n, err := conn.Do("EXPIRE", key, time)
	if err != nil {
		fmt.Println(key+" err while expiring:", err)
	} else if n != int64(1) {
		fmt.Println(key + " set expire failed")
	}
}

func (r *RedisConn) Keys(keys string) ([]string, error) {
	conn := r.pool.Get()
	defer conn.Close()

	return redis.Strings(conn.Do("KEYS", keys))
}

// Hash HSet
func (r *RedisConn) HSet(key string, field string, value interface{}) (err error) {
	conn := r.pool.Get()
	defer conn.Close()

	_, err = conn.Do("HSET", key, field, value)

	if err != nil {
		return err
	}

	return nil
}

//Hash HExists
func (r *RedisConn) HExists(key string, field string) bool {
	conn := r.pool.Get()
	defer conn.Close()

	isExist, err := redis.Bool(conn.Do("HEXISTS", key, field))
	if err != nil {
		return false
	}

	return isExist
}

//Hash HGet
func (r *RedisConn) HGet(key string, field string) (res interface{}, err error) {
	conn := r.pool.Get()
	defer conn.Close()

	res, err = conn.Do("HGET", key, field)

	if err != nil {
		return res, err
	} else {
		return res, nil
	}
}

//Hash HDel
func (r *RedisConn) HDel(key string, field string) (err error) {
	conn := r.pool.Get()
	defer conn.Close()

	_, err = conn.Do("HDEL", key, field)
	//if err != nil {
	//	fmt.Println("hdel failed", err.Error())
	//}
	return err
}

//删除指定Key
func (r *RedisConn) HDelField(key, field string, extField ...interface{}) error {
	conn := r.pool.Get()
	defer conn.Close()

	s := []interface{}{key, field}
	s = append(s, extField)
	_, err := conn.Do("HDEL", s...)
	if err != nil {
		return err
	}
	return nil
}

//Hash HGetAll

func (r *RedisConn) HGetAll(key string) (reslust []interface{}, err error) {
	conn := r.pool.Get()
	defer conn.Close()

	result, err := redis.Values(conn.Do("HGETALL", key))
	if err != nil {
		fmt.Println("hgetall failed", err.Error())
		return nil, err
	} else {
		return result, nil
	}
}

// HGETALL返回String map
func (r *RedisConn) HGetAllStringMap(key string) (map[string]string, error) {
	conn := r.pool.Get()
	defer conn.Close()

	return redis.StringMap(conn.Do("HGETALL", key))
}

//获取指定Map指定Key的bool值
func (r *RedisConn) HGetBoolean(key, feild string) (bool, error) {
	conn := r.pool.Get()
	defer conn.Close()

	return redis.Bool(conn.Do("HGET", key, feild))
}

//获取指定key指定field的String值
func (r *RedisConn) HGetString(key, feild string) (string, error) {
	conn := r.pool.Get()
	defer conn.Close()

	return redis.String(conn.Do("HGET", key, feild))
}

//获取指定key指定field的[]byte值
func (r *RedisConn) HGetBytes(key, feild string) ([]byte, error) {
	conn := r.pool.Get()
	defer conn.Close()

	return redis.Bytes(conn.Do("HGET", key, feild))
}

//获取指定Key的Float64值
func (r *RedisConn) HGetFloat64(key, feild string) (float64, error) {
	conn := r.pool.Get()
	defer conn.Close()

	return redis.Float64(conn.Do("HGET", key, feild))
}

//设置Hash value
func (r *RedisConn) HSetValue(key, field string, value interface{}) error {
	conn := r.pool.Get()
	defer conn.Close()

	_, err := conn.Do("HSET", key, field, value)
	if err != nil {
		return err
	}
	return nil
}

//Zset
//值存在 返回0，不存在则添加 ,返回1
func (r *RedisConn) ZNewAdd(key string, score string, field string) (res interface{}, err error) {
	conn := r.pool.Get()
	defer conn.Close()

	res, err = conn.Do("ZADD", key, score, field)
	if err != nil {
		panic(err)
	}
	return res, nil
}

//Zset 获取分数&&有分数,则存在,没有分数,则不存在
func (r *RedisConn) ZScore(key string, field string) (res interface{}, err error) {
	conn := r.pool.Get()
	defer conn.Close()

	res, err = conn.Do("ZSCORE", key, field)

	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return res, nil
}

//Zset 获取分数&&有分数,则存在,没有分数,则不存在
func (r *RedisConn) ZRange(key string, start, stop int, withScores bool) ([]string, error) {
	conn := r.pool.Get()
	defer conn.Close()

	var res []string
	var err error
	if withScores {
		res, err = redis.Strings(conn.Do("ZRANGE", key, start, stop, "WITHSCORES"))
	} else {
		res, err = redis.Strings(conn.Do("ZRANGE", key, start, stop))
	}

	if err != nil {
		fmt.Println(err.Error())
		return res, err
	}

	return res, nil
}

func (r *RedisConn) ZRevRank(key, field string) int64 {
	conn := r.pool.Get()
	defer conn.Close()

	v, err := redis.Int64(conn.Do("ZREVRANK", key, field))
	if err != nil {
		//log.Println("ZREVRANK failed:", err)
		return -1
	}

	return v
}

// 设置value到指定list
func (r *RedisConn) SetList(listName string, value interface{}) error {
	r.Delete(listName)
	return r.PushList(listName, value)
}

// 插入value到指定list中
func (r *RedisConn) PushList(listName string, value interface{}) error {
	return r.RPushList(listName, value)
}

// 插入到
func (r *RedisConn) RPushList(listName string, value interface{}) error {
	conn := r.pool.Get()
	defer func() {
		_ = conn.Close()
	}()

	_, err := conn.Do("RPUSH", redis.Args{}.Add(listName).AddFlat(value)...)
	if err != nil {
		return err
	}
	return nil
}

// 删除指定list中的key
func (r *RedisConn) RemoveKeyList(key, value string) error {
	conn := r.pool.Get()
	defer func() {
		_ = conn.Close()
	}()

	_, err := conn.Do("LREM", 0, value)
	if err != nil {
		return err
	}
	return nil
}

// list长度
func (r *RedisConn) LenList(listName string) (int, error) {
	conn := r.pool.Get()
	defer func() {
		_ = conn.Close()
	}()

	return redis.Int(conn.Do("LLEN", listName))
}

// pop one
func (r *RedisConn) PopListOne(listName string) (string, error) {
	conn := r.pool.Get()
	defer func() {
		_ = conn.Close()
	}()

	return redis.String(conn.Do("LPOP", listName))
}

// pop all
func (r *RedisConn) PopList(listName string) ([]string, error) {
	conn := r.pool.Get()
	defer func() {
		_ = conn.Close()
	}()
	cnt, err := r.LenList(listName)
	if err != nil {
		return []string{}, err
	}
	defer func() {
		_, _ = conn.Do("LTRIM", listName, cnt+1, -1)
	}()
	return redis.Strings(conn.Do("LRANGE", listName, 0, cnt))
}

func (r *RedisConn) FetchList(listName string) ([]string, error) {
	conn := r.pool.Get()
	defer func() {
		_ = conn.Close()
	}()
	cnt, err := r.LenList(listName)
	if err != nil {
		return []string{}, err
	}

	return redis.Strings(conn.Do("LRANGE", listName, 0, cnt))
}

func (r *RedisConn) SAdd(key, value string) (bool, error) {
	var err error
	conn := r.pool.Get()
	defer conn.Close()
	ok, err := redis.Bool(conn.Do("SADD", key, value))
	if err != nil || !ok {
		return false, err
	}
	return ok, nil
}

func (r *RedisConn) SCard(key string) (int, error) {
	var (
		err   error
		count int
	)
	conn := r.pool.Get()
	defer conn.Close()
	count, err = redis.Int(conn.Do("SCARD", key))
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (r *RedisConn) SDiffString(key1, key2 string) ([]string, error) {
	var (
		err     error
		reslust []string
	)
	conn := r.pool.Get()
	defer conn.Close()
	reslust, err = redis.Strings(conn.Do("SDIFF", key1, key2))
	if err != nil {
		return nil, err
	}
	return reslust, nil
}

func (r *RedisConn) SREM(key, member string) error {
	var (
		err error
	)
	conn := r.pool.Get()
	defer conn.Close()
	_, err = conn.Do("SREM", key, member)
	if err != nil {
		return err
	}
	return nil
}

func (r *RedisConn) HINCRBY(key, field string, inc int) error {
	conn := r.pool.Get()
	defer conn.Close()
	_, err := conn.Do("HINCRBY", key, field, inc)
	return err
}

func (r *RedisConn) SMEMBERS(key, value string) (bool, error) {
	var err error
	conn := r.pool.Get()
	defer conn.Close()
	ok, err := redis.Bool(conn.Do("SISMEMBER", key, value))
	if err != nil || !ok {
		return false, err
	}
	return ok, nil
}

func (r *RedisConn) HLEN(key string) (int64,error) {
	conn := r.pool.Get()
	defer conn.Close()
	cnt, err := redis.Int64(conn.Do("HLEN", key))
	return cnt,err
}

func (r *RedisConn) MEMBERS(key string) ([]string, error) {
	var (
		err     error
		result []string
	)
	conn := r.pool.Get()
	defer conn.Close()
	result, err = redis.Strings(conn.Do("SMEMBERS", key))
	if err != nil {
		return nil, err
	}
	return result, nil
}