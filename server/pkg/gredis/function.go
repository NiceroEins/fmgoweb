package gredis

import "datacenter/pkg/logging"

// push message to redis
// from: uid of user who sends messages
// to: uid of user who receives messages
// msg: message content
// returns: length of the message queue, and error
func Push(from, to string, msg string) (int, error) {
	// 发件人可以为空
	// 收件人或消息为空时抛弃消息
	if to == "" || msg == "" {
		return 0, nil
	}
	c := getInstance()
	err := c.PushList(getName(to), msg)
	if err != nil {
		logging.Error("middleware.Push() errors: ", err.Error())
		return 0, err
	}
	return c.LenList(getName(to))
}

// pop message from middleware
// uid: uid of user who receives messages
// returns: message content slice, and error
func Pop(uid string) ([]string, error) {
	c := getInstance()
	ctx, err := c.PopList(getName(uid))
	if err != nil {
		logging.Error("middleware.Pop() errors: ", err.Error())
		return []string{}, err
	}
	return ctx, nil
}

// pop message from middleware
// uid: uid of user who receives messages
// returns: message content, and error
func PopOne(uid string) (string, error) {
	c := getInstance()
	return c.PopListOne(getName(uid))
}

// fetch message list
func Fetch(uid string) ([]string, error) {
	c := getInstance()
	ctx, err := c.FetchList(getName(uid))
	if err != nil {
		logging.Error("middleware.Fetch() errors: ", err.Error())
		return []string{}, err
	}
	return ctx, nil
}

func getName(uid string) string {
	return "message_uid_" + uid
}

func getInstance() *RedisConn {
	return Instance
}
