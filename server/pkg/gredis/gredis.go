package gredis

import (
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"github.com/gomodule/redigo/redis"
	"sync"
	"time"
)

var Instance *RedisConn
var bak *RedisConn
var redisPool sync.Map
var bakPool sync.Map

type RedisConn struct {
	pool redis.Pool
}

// Setup Initialize the Redis instance
func Setup() error {
	Instance = &RedisConn{
		pool: newRedisPool(setting.RedisSetting.DB),
	}
	return nil
}

func newRedisPool(db int) redis.Pool {
	return redis.Pool{
		MaxIdle:     setting.RedisSetting.MaxIdle,
		MaxActive:   setting.RedisSetting.MaxActive,
		IdleTimeout: setting.RedisSetting.IdleTimeout,
		Wait:        true, //无可用redis连接,暂时等待
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", setting.RedisSetting.Host, redis.DialPassword(setting.RedisSetting.Password), redis.DialDatabase(db))
			if err != nil {
				logging.Error(err)
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func bakRedisPool(db int) redis.Pool {
	return redis.Pool{
		MaxIdle:     setting.RedisbakSetting.MaxIdle,
		MaxActive:   setting.RedisbakSetting.MaxActive,
		IdleTimeout: setting.RedisbakSetting.IdleTimeout,
		Wait:        true, //无可用redis连接,暂时等待
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", setting.RedisbakSetting.Host, redis.DialPassword(setting.RedisbakSetting.Password), redis.DialDatabase(db))
			if err != nil {
				logging.Error(err)
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func Clone(db int) *RedisConn {
	v, ok := redisPool.Load(db)
	if !ok || v == nil {
		value := &RedisConn{
			pool: newRedisPool(db),
		}
		redisPool.Store(db, value)
		return value
	}
	return v.(*RedisConn)
}

func Bakup(db int) *RedisConn {
	v, ok := bakPool.Load(db)
	if !ok || v == nil {
		value := &RedisConn{
			pool: bakRedisPool(db),
		}
		bakPool.Store(db, value)
		return value
	}
	return v.(*RedisConn)
}

//  String Set a key/value
func Set(key string, data interface{}, time int) error {
	return Instance.Set(key, data, time)
}

// String Exists check a key
func Exists(key string) bool {
	return Instance.Exists(key)
}

// String Get get a key
func Get(key string) ([]byte, error) {
	return Instance.Get(key)
}

func GetString(key string) (string, error) {
	return Instance.GetString(key)
}

func GetInt(key string) (int64, error) {
	return Instance.GetInt(key)
}

func GetFloat(key string) (float64, error) {
	return Instance.GetFloat(key)
}

// String Delete delete a kye
func Delete(key string) (bool, error) {
	return Instance.Delete(key)
}

// String LikeDeletes batch delete
func LikeDeletes(key string) error {
	return Instance.LikeDeletes(key)
}

//string 自增
func IncrBy(key string, num int) int64 {
	return Instance.IncrBy(key, num)
}

func Expire(key string, time int64) {
	Instance.Expire(key, time)
}

func Keys(keys string) ([]string, error) {
	return Instance.Keys(keys)
}

// Hash HSet
func HSet(key string, field string, value interface{}) (err error) {
	return Instance.HSet(key, field, value)
}

//Hash HExists
func HExists(key string, field string) bool {
	return Instance.HExists(key, field)
}

//Hash HGet
func HGet(key string, field string) (res interface{}, err error) {
	return Instance.HGet(key, field)
}

//Hash HDel
func HDel(key string, field string) (err error) {
	return Instance.HDel(key, field)
}

//删除指定Key
func HDelField(key, field string, extField ...interface{}) error {
	return Instance.HDelField(key, field, extField...)
}

//Hash HGetAll

func HGetAll(key string) (reslust []interface{}, err error) {
	return Instance.HGetAll(key)
}

// HGETALL返回String map
func HGetAllStringMap(key string) (map[string]string, error) {
	return Instance.HGetAllStringMap(key)
}

//获取指定Map指定Key的bool值
func HGetBoolean(key, field string) (bool, error) {
	return Instance.HGetBoolean(key, field)
}

//获取指定key指定field的String值
func HGetString(key, field string) (string, error) {
	return Instance.HGetString(key, field)
}

//获取指定key指定field的String值
func HGetBytes(key, field string) ([]byte, error) {
	return Instance.HGetBytes(key, field)
}

//获取指定Key的Float64值
func HGetFloat64(key, field string) (float64, error) {
	return Instance.HGetFloat64(key, field)
}

//设置Hash value
func HSetValue(key, field string, value interface{}) error {
	return Instance.HSetValue(key, field, value)
}

//Zset
//func Zadd(key string, field string, value int64) (err error) {
//	conn := Instance.pool.Get()
//	defer conn.Close()
//
//	_, err = conn.Do("zadd", key, "INCR", value, field)
//	if err != nil {
//		fmt.Println(err.Error())
//		return err
//	}
//	return nil
//}

//值存在 返回0，不存在则添加 ,返回1
func ZNewAdd(key string, score string, field string) (res interface{}, err error) {
	return Instance.ZNewAdd(key, score, field)
}

//Zset 获取分数&&有分数,则存在,没有分数,则不存在
func ZScore(key string, field string) (res interface{}, err error) {
	return Instance.ZScore(key, field)
}

//模糊匹配到所有的值，返回所有值的和
func KeysValueCount(keys string) int {
	keySlice, _ := Keys(keys)
	count := 0
	for _, v := range keySlice {
		n, _ := GetInt(v)
		count += int(n)
	}
	return count
}

func SetList(listName string, value interface{}) error {
	return Instance.SetList(listName, value)
}

func PushList(listName string, value interface{}) error {
	return Instance.PushList(listName, value)
}

func RPushList(listName string, value interface{}) error {
	return Instance.RPushList(listName, value)
}

func RemoveKeyList(key, value string) error {
	return Instance.RemoveKeyList(key, value)
}

func LenList(listName string) (int, error) {
	return Instance.LenList(listName)
}

func PopListOne(listName string) (string, error) {
	return Instance.PopListOne(listName)
}

func PopList(listName string) ([]string, error) {
	return Instance.PopList(listName)
}

func FetchList(listName string) ([]string, error) {
	return Instance.FetchList(listName)
}

func SAdd(key, value string) (bool, error) {
	return Instance.SAdd(key, value)
}

func SCard(key string) (int, error) {
	return Instance.SCard(key)
}

func SDiffString(key1, key2 string) ([]string, error) {
	return Instance.SDiffString(key1, key2)
}

func SREM(key, mem string) error {
	return Instance.SREM(key, mem)
}

func HINCRBY(key, field string, inc int) error {
	return Instance.HINCRBY(key, field, inc)
}

func SMEMBERS(key, value string) (bool, error) {
	return Instance.SMEMBERS(key, value)
}
