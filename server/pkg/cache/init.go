package cache

import (
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"time"
)

var (
	// Set the default expiration time.
	DefaultExpiration = 48 * time.Hour // The default for the default is two day.
)

func init() {
	// By default, use the in-memory cache.
	Instance = NewInMemoryCache(DefaultExpiration)
}

func Setup() {
	SetInstance("redis", setting.RedisSetting.Host, setting.RedisSetting.Password)
}

// for memcached,the opt is the hosts list.
// for redis,the opt is the host and password.
func SetInstance(cachetype string, opt ...string) {
	switch cachetype {
	case "memcached":
		Instance = NewMemcachedCache(opt, DefaultExpiration)
	case "redis":
		if len(opt) > 2 {
			logging.Error("redis can't config mulit hosts.", "opt", opt)
			return
		}
		password := opt[1]
		Instance = NewRedisCache(opt[0], password, DefaultExpiration)
	case "inmemory":
		Instance = NewInMemoryCache(DefaultExpiration)
	default:
		logging.Warn("default cache is memory.", "opt", opt)
	}
}
