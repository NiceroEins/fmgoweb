package logging

import (
	"datacenter/pkg/file"
	"datacenter/pkg/setting"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
)

type Level int

var (
	F *os.File

	DefaultPrefix      = ""
	DefaultCallerDepth = 2

	logger     *log.Logger
	logPrefix  = ""
	levelFlags = []string{"DEBUG", "INFO", "WARN", "ERROR", "FATAL"}
	logLevel   = 0
)

const (
	DEBUG Level = iota
	INFO
	WARNING
	ERROR
	FATAL
)

// Setup initialize the log instance
func Setup() {
	var err error
	filePath := getLogFilePath()
	fileName := getLogFileName()
	F, err = file.MustOpen(fileName, filePath)
	if err != nil {
		log.Fatalf("logging.Setup err: %v", err)
	}

	logger = log.New(F, DefaultPrefix, log.LstdFlags)

	for k, v := range levelFlags {
		if setting.AppSetting.LogLevel == v {
			logLevel = k
			break
		}
	}
}

// Debug output logs at debug level
func Debug(v ...interface{}) {
	if logLevel <= int(DEBUG) {
		setPrefix(DEBUG)
		logger.Println(v)
	}
}

// Info output logs at info level
func Info(v ...interface{}) {
	if logLevel <= int(INFO) {
		setPrefix(INFO)
		logger.Println(v)
	}
}

// Warn output logs at warn level
func Warn(v ...interface{}) {
	if logLevel <= int(WARNING) {
		setPrefix(WARNING)
		logger.Println(v)
	}
}

// Error output logs at error level
func Error(v ...interface{}) {
	if logLevel <= int(ERROR) {
		setPrefix(ERROR)
		logger.Println(v)
	}
}

// Fatal output logs at fatal level
func Fatal(v ...interface{}) {
	if logLevel <= int(FATAL) {
		setPrefix(FATAL)
		logger.Fatalln(v)
	}
}

// setPrefix set the prefix of the log output
func setPrefix(level Level) {
	_, file, line, ok := runtime.Caller(DefaultCallerDepth)
	if ok {
		logPrefix = fmt.Sprintf("[%s][%s:%d]", levelFlags[level], filepath.Base(file), line)
	} else {
		logPrefix = fmt.Sprintf("[%s]", levelFlags[level])
	}

	logger.SetPrefix(logPrefix)
}
