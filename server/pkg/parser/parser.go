package parser

import (

	"regexp"
	"strconv"
	"strings"
	"time"
)

//源:http://gxrb.gxrb.com.cn/html/#date(%Y-%m)#/#date(%d)#/node_5.htm
//目标:http://gxrb.gxrb.com.cn/html/2020-06/08/node_5.htm
//func Parse(source string) (dst string) {
//
//	now := time.Now()
//	res := now.Format("2006/01/02") //年月日
//	resSlice := strings.Split(res, "/")
//
//	//str := "http://gxrb.gxrb.com.cn/html/#date(%Y-%m)#/#date(%d)#/node_5.htm"
//	strSlice := strings.Split(source, "/")
//
//	if strSlice[4] == "#date(%Y-%m)#" {
//		strSlice[4] = resSlice[0] + "-" + resSlice[1]
//	}
//	if strSlice[5] == "#date(%d)#" {
//		strSlice[5] = resSlice[2]
//	}
//	dst = strings.Replace(strings.Trim(fmt.Sprint(strSlice), "[]"), " ", "/", -1)
//	return
//}

func Parse(source string) string {
	ret := source
	re := regexp.MustCompile("#date\\((.*?)\\)#")
	oldSubs := re.FindAllStringSubmatch(source, -1)
	date := strings.Split(time.Now().Format("2006-01-02"), "-")
	repMap := map[string]string{
		"%Y": date[0],
		"%m": date[1],
		"%d": date[2],
	}
	for _, v := range oldSubs {
		newSub := v[1]
		for k, u := range repMap {
			newSub = strings.Replace(newSub, k, u, -1)
		}
		ret = strings.Replace(ret, v[0], newSub, -1)
	}
	return ret
}

//判断一个字符串中是否包含字母
func IsLetter(s string) bool {
	res := false
	for _, r := range s {
		if (r < 'a' || r > 'z') && (r < 'A' || r > 'Z') {
			res = false
		} else {
			return true
		}
	}
	return res
}

//判断一个字符串中是否包含数字
//func IsNum(s string) bool {
//	res := false
//	//_, err := strconv.ParseFloat(s, 64)
//	//return err == nil
//	for _,r := range s{
//		res =unicode.IsDigit(r)
//		if res {
//			return true
//		}else{
//			res = false
//		}
//	}
//	return res
//
//}
//判断字符串是否是纯数字
func IsNum(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}
