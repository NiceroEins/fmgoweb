package parser

import (
	"fmt"
	"strings"
	"testing"
	"time"
)

//源:http://gxrb.gxrb.com.cn/html/#date(%Y-%m)#/#date(%d)#/node_5.htm
//目标:http://gxrb.gxrb.com.cn/html/2020-06/08/node_5.htm
func TestParse(t *testing.T) {
	now := time.Now()
	res := now.Format("2006/01/02") //年月日
	resSlice := strings.Split(res, "/")

	str := "http://paper.people.com.cn/rmrb/html/#date(%Y-%m)#/#date(%d)#/nbs.D110000renmrb_01.htm"
	//bool :=strings.Contains(str, "#date(%Y-%m)")

	strSlice := strings.Split(str, "/")

	if strSlice[4] == "#date(%Y-%m)#" {
		strSlice[4] = resSlice[0] + "-" + resSlice[1]
	}
	if strSlice[5] == "#date(%d)#" {
		strSlice[5] = resSlice[2]
	}
	data := strings.Replace(strings.Trim(fmt.Sprint(strSlice), "[]"), " ", "/", -1)
	t.Log(data)
}

//判断是一个字符串是否包含字母+数字
func TestIsLetter(t *testing.T) {
	str := "11asda312"
	//fmt.Println(IsLetter(str)) //包含字母测试,有字母返回true

	//num := "a1asd23a"
	//fmt.Println(IsNum(num))
	if IsLetter(str) && IsNum(str) {
		fmt.Println("包含数字+字母")
	} else {
		fmt.Println("不包含数字+字母")
	}
}

//判断一个字符串是否是纯数字
func TestIsNum(t *testing.T) {
	str := "12你好3"
	t.Log(IsNum(str))
}

func TestParse2(t *testing.T) {
	str := "http://gxrb.gxrb.com.cn/html/#node_5.htm"
	//bool :=strings.Contains(str, "#date(%Y-%m)")
	data := Parse(str)
	t.Log(data)
}
