package setting

import (
	"log"
	"time"

	"github.com/go-ini/ini"
)

type App struct {
	JwtSecret  string
	PageSize   int
	DefaultPwd string
	PrefixUrl  string

	RuntimeRootPath string

	ImageSavePath  string
	ImageMaxSize   int
	ImageAllowExts []string

	ExportSavePath string
	QrCodeSavePath string
	FontSavePath   string

	LogSavePath string
	LogSaveName string
	LogFileExt  string
	TimeFormat  string
	LogLevel    string
}

var AppSetting = &App{}

type Server struct {
	RunMode      string
	HttpPort     int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
	Type         string
	Stock        string
}

var ServerSetting = &Server{}

type Database struct {
	Type         string
	User         string
	Password     string
	Host         string
	Name         string
	TablePrefix  string
	MaxIdle      int
	DataHost     string
	DataUser     string
	DataPassword string
	DataName     string
	//DataHost     string
}

var DatabaseSetting = &Database{}

type Redis struct {
	Host        string
	Password    string
	DB          int
	MaxIdle     int
	MaxActive   int
	IdleTimeout time.Duration
	MessageDB   int
	StockDB     int
	SpiderDB    int
}

var RedisSetting = &Redis{}

type Redisbak struct {
	Host        string
	Password    string
	DB          int
	MaxIdle     int
	MaxActive   int
	IdleTimeout time.Duration
	MessageDB   int
	StockDB     int
	SpiderDB    int
}

var RedisbakSetting = &Redisbak{}

type Spider struct {
	Seed             int
	PoolSize         int
	MinCatchInterval int
	ProxyNumPerUse   int
}

var SpiderSetting = &Spider{}

type NLP struct {
	CorrelationHost string
	DuplicationHost string
	NamedEntityHost string
	SentimentHost   string
	QAHost          string
	TestHost        string
	KeywordsHost    string
	SimilarHost     string
}

var NLPSetting = &NLP{}

type FileService struct {
	AccessKeyID     string
	AccessKeySecret string
	Host            string
	Raw             string
	CallbackUrl     string
	UploadDir       string
	ExpireTime      int64
	Endpoint        string
	Bucket          string
}

var FileSetting = &FileService{}

type Stock struct {
	User     string
	Password string
	Scheme   string
	Address  string
	Path     string
	Query    string
	Source   string //inner/outer
}

var StockSetting = &Stock{}

type MongoDB struct {
	URI         string
	Timeout     time.Duration
	MaxPoolSize uint64
	User        string
	Password    string
	Database    string
}

var MongoDBSetting = &MongoDB{}

var cfg *ini.File

// Setup initialize the configuration instance
func Setup() {
	var err error
	cfg, err = ini.Load("conf/app.ini")
	if err != nil {
		log.Fatalf("setting.Setup, fail to parse 'conf/app.ini': %v", err)
	}

	mapTo("app", AppSetting)
	mapTo("server", ServerSetting)
	mapTo("database", DatabaseSetting)
	mapTo("redis", RedisSetting)
	mapTo("spider", SpiderSetting)
	mapTo("nlp", NLPSetting)
	mapTo("file", FileSetting)
	mapTo("stock", StockSetting)
	mapTo("mongodb", MongoDBSetting)
	mapTo("redisbak", RedisbakSetting)

	AppSetting.ImageMaxSize = AppSetting.ImageMaxSize * 1024 * 1024
	ServerSetting.ReadTimeout = ServerSetting.ReadTimeout * time.Second
	ServerSetting.WriteTimeout = ServerSetting.WriteTimeout * time.Second
	RedisSetting.IdleTimeout = RedisSetting.IdleTimeout * time.Second
}

// mapTo map section
func mapTo(section string, v interface{}) {
	err := cfg.Section(section).MapTo(v)
	if err != nil {
		log.Fatalf("Cfg.MapTo %s err: %v", section, err)
	}
}
