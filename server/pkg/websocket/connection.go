package websocket

import (
	"datacenter/models/message"
	"datacenter/models/statistics"
	"datacenter/models/stock"
	"datacenter/pkg/gredis"
	"datacenter/pkg/util"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"github.com/gorilla/websocket"

	"datacenter/pkg/logging"
)

type Connections struct {
	conn     sync.Map
	clientIP string
	//conn []*Connection
}

func (connections *Connections) Add(clientIP string, conn *Connection) {
	if connections == nil {
		return
	}
	connections.conn.Store(conn, 1)
}

func (connections *Connections) Del(conn *Connection) {
	if connections == nil {
		return
	}
	connections.conn.Delete(conn)
}

type Connection struct {
	conn        *websocket.Conn
	sendChan    chan *message.Message
	confirmChan chan *message.Message
	closeChan   chan int
	available   bool
	loss        uint32
	locker      uint32     //原子锁，0=不占用，1=占用
	once        *sync.Once // close只需要运行一次
	needConfirm bool
	bPing       bool
	clientIP    string
	channel     string
	//record      map[string]map[string]interface{}
	filter func(string, string) map[string]interface{}
}

func (connection *Connection) receiving(uid string) {
	defer func() {
		connection.Close()
		logging.Debug("websocket client " + uid + " receiving() exited")
	}()

	connection.conn.SetReadLimit(maxMessageSize)
	if connection.bPing {
		_ = connection.conn.SetReadDeadline(time.Now().Add(pongWait))
		connection.conn.SetPongHandler(func(string) error {
			_ = connection.conn.SetReadDeadline(time.Now().Add(pongWait))
			return nil
		})
	}

	logging.Debug("websocket client " + uid + " receiving() started")
	for {
		// 从client读到用户发送消息
		_, msgByte, err := connection.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				break
			} else {
				//logging.Debug("websocket.connHandle Errors: " + err.Error())
			}
			break
		}

		// 消息由直接送send通道，改为送hub待处理
		msg := message.UnmarshalMsg(msgByte)
		//msg = TraceLog(msg, "Received from client")
		switch msg.Type() {
		case message.Message_Tick:
			////11.19 转发
			//ctx := message.ParseMsg(msg.Type(), "stock", "", msg.Msg(), false)
			//ctx.SetChannel("stock")
			//ins.SendToHub(ctx)
			ins.SendAllToHub(connection.FilterStock(msg))
			go stock.SetTick(msg.Msg())
		case message.Message_Tick_Copy:
			decodeBytes, err := base64.StdEncoding.DecodeString(msg.Msg())
			if err != nil {
				logging.Error("websocket.receiving() base64 decode Errors: ", err.Error())
				continue
			}
			go stock.SetTick(string(decodeBytes))
		case message.Message_User_Event:
			go statistics.Do(msg.Msg())
		default:
			if c := connection.Channel(); c != "" {
				msg.SetChannel(c)
			}
			ins.SendAllToHub(connection.Filter(msg))
		}
	}
}

func (connection *Connection) sending(uid string) {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		connection.Close()
		logging.Debug("websocket client " + uid + " sending() exited")
	}()
	logging.Debug("websocket client " + uid + " sending() started")
	for {
		select {
		case msg, ok := <-connection.sendChan:
			if !ok {
				// The hub closed the channel.
				_ = connection.conn.WriteMessage(websocket.CloseMessage, []byte{})
				connection.SetStatus(false)
				// 消息通道被关闭，可以return
				return
			}
			msg = TraceLog(msg, "Got from sendChan")
			// 若对方在线，消息直接传递
			if connection.Available() {
				_ = connection.conn.SetWriteDeadline(time.Now().Add(writeWait))
				if !msg.Available() {
					// 若消息无效，跳过
					continue
				}
				msg = TraceLog(msg, "Ready to send")
				if err := connection.sendMsg(msg); err == nil {
					connection.SetStatus(true)
					continue
				} else {
					connection.SetStatus(false)
				}
			}
			ins.SendToHub(msg)
		case <-ticker.C:
			if connection.bPing {
				_ = connection.conn.SetWriteDeadline(time.Now().Add(writeWait))
				if err := connection.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
					connection.SetStatus(false)
				} else {
					connection.SetStatus(true)
				}
			}
		case <-connection.closeChan:
			return
		}
	}
}

func (connection *Connection) sendMsg(msg *message.Message) error {
	switch msg.Type() {
	case message.Message_Tick:
		ctx := make(map[string]interface{})
		err := json.Unmarshal([]byte(msg.Msg()), &ctx)
		if err != nil {
			return err
		}
		_, event := util.Getstring(ctx, "event")
		ok, data := util.GetMap(ctx, "data")
		_, needSign := util.GetBool(ctx, "needSign")
		if !ok {
			data = make(map[string]interface{})
		}
		text := util.GenerateTickContent(event, data, needSign)
		//fmt.Println(text)
		return connection.sendRaw([]byte(text))
	default:
		err := connection.sendRaw(message.MarshalMsg(msg))
		if err == nil {
			// add send count
			if msg.Type() == message.Message_Chat {
				connection.AddRecord(msg.GenerateKey(message.WsSendCount), 1)
				connection.AddRecord(msg.GenerateKey(message.WsSendBeforeReply), 1)
				// 清空对方的回复计数器
				connection.SetRecord(msg.GenerateKeyEx(msg.To(), msg.From(), message.WsSendBeforeReply), 0)
				//logging.Debug("sendMsg() added on " + msg.To())
			}
		}
		if connection.needConfirm {
			err = connection.WaitForConfirm(msg)
		}
		return err
	}
}

func (connection *Connection) sendRaw(msg []byte) error {
	if !connection.Available() {
		return fmt.Errorf("websocket.sendRaw() connection closed")
	}
	w, err := connection.conn.NextWriter(websocket.TextMessage)
	if err != nil {
		logging.Debug("websocket.sendMsg get writer errors: " + err.Error())
		return err
	}
	_, err = w.Write(msg)
	_ = w.Close()
	if err != nil {
		logging.Debug("websocket.sendMsg write message errors: " + err.Error())
		return err
	}
	return err
}

func (connection *Connection) Available() bool {
	//logging.Info(fmt.Sprintf("websocket.connection.Available() connection: %v, isChanClosed(connection.send): %v", connection, isChanClosed(connection.sendChan)))
	k := isChanClosed(connection.sendChan)
	return connection.available &&
		connection.conn != nil &&
		!k
}

func CheckConnection(connection *Connection) (bool, string) {
	if connection == nil {
		return false, "nil ptr"
	}
	if !connection.available {
		return false, "unavailable connection"
	}
	if connection.conn == nil {
		return false, "nil connection"
	}
	if isChanClosed(connection.sendChan) {
		return false, "send chan closed"
	}
	return true, ""
}

func CheckConnections(connections *Connections) bool {
	if connections == nil {
		return false
	}
	ret := false
	connections.conn.Range(func(k, v interface{}) bool {
		conn, ok := k.(*Connection)
		if !ok || conn == nil {
			connections.conn.Delete(k)
			return true
		}
		if ok, _ := CheckConnection(conn); ok {
			ret = true
		} else {
			connections.conn.Delete(k)
		}
		return true
	})
	return ret
}

func (connection *Connection) Send(msg *message.Message) {
	defer func() {
		if recover() != nil {
			// do nothing
		} else {
			LogSend(msg)
		}
	}()
	if !isChanClosed(connection.sendChan) {
		msg.SetId(ins.counter.Step())
		msg = TraceLog(msg, "Sent to sendChan")
		// add delay
		if msg.Delay() > 0 {
			time.Sleep(time.Duration(msg.Delay()) * time.Second)
		}
		connection.sendChan <- msg
	}
}

func (connection *Connection) Confirm(msg *message.Message) {
	logging.Debug("websocket confirm: " + string(message.MarshalMsg(msg)))
	connection.confirmChan <- msg
}

func (connection *Connection) WaitForConfirm(msg *message.Message) error {
	timer := time.NewTimer(confirmWait)
	for {
		select {
		case c, ok := <-connection.confirmChan:
			if !ok {
				return fmt.Errorf("invalid confirmChan channel")
			}
			if c.Id() == msg.Id() {
				return nil
			}
		case <-timer.C:
			return fmt.Errorf("confirming timeout")
		}
	}
}

func (connection *Connection) SetStatus(status bool) {
	connection.available = status
	if !status {
		// 离线策略，若干次失败后判定掉线
		//connection.loss += 1
		atomic.AddUint32(&connection.loss, 1)
		if connection.loss > maxLosses {
			//connection.closeChan <- 0
			connection.Close()
			atomic.StoreUint32(&connection.loss, 0)
		}
	} else {
		//connection.loss = 0
		atomic.StoreUint32(&connection.loss, 0)
	}
	//logging.Debug(fmt.Sprintf("websocket.SetStatus() set status: %v", status))
}

func (connection *Connection) SetPing(bPing bool) {
	connection.bPing = bPing
}

func (connection *Connection) Close() {
	//if !atomic.CompareAndSwapUint32(&connection.locker, 0, 1) {
	//	// 有占用
	//	return
	//}
	connection.once.Do(func() {
		defer func() {
			if recover() != nil {
				// do nothing
			}
			//atomic.StoreUint32(&connection.locker, 0)
			//connection.FreeLocker()
		}()
		_ = connection.conn.Close()
		close(connection.closeChan)
		close(connection.sendChan)
		logging.Debug("websocket.Close() called")
	})
}

func (connection *Connection) SetConfirm(status bool) {
	connection.needConfirm = status
}

func (connection *Connection) SetFilter(f func(string, string) map[string]interface{}) {
	connection.filter = f
}

func (connection *Connection) Filter(msg *message.Message) []*message.Message {
	// 对聊天及推荐进行filter
	if !msg.NeedFilter() || connection.filter == nil {
		return []*message.Message{msg}
	}

	condition := connection.filter(msg.From(), msg.To())
	//logging.Info("测试代码：", msg.Type())

	data := message.RemoveDup(msg.Filter(condition))
	return data
}

func (connection *Connection) FilterStock(msg *message.Message) []*message.Message {
	userList := []string{"test", "dev", "pre", "release", "test2"}
	var ret []*message.Message
	for _, v := range userList {
		data := base64.StdEncoding.EncodeToString([]byte(msg.Msg()))
		ctx := message.ParseMsg(message.Message_Tick_Copy, "stock", v, data, false)
		ctx.SetChannel("stock")
		ret = append(ret, ctx)
	}
	return ret
}

func (connection *Connection) Channel() string {
	return connection.channel
}

func (connection *Connection) AddRecord(key string, delta int) {
	//connection.GetLocker()
	//defer connection.FreeLocker()
	//v, ok := connection.record[uid]
	//if !ok {
	//	v = make(map[string]interface{})
	//}
	//value, _ := xx.GetInt(v, key)
	//v[key] = value + delta
	//connection.record[uid] = v
	//logging.Debug("AddRecord() uid: " + uid + " key: " + key + " added to: " + strconv.Itoa(value+delta))

	// redis instance
	ri := gredis.Clone(3)
	if delta > 0 {
		_ = ri.IncrBy(key, delta)
	} else {
		_ = ri.IncrBy(key, -delta)
	}
}

func (connection *Connection) SetRecord(key string, value int) {
	//connection.GetLocker()
	//defer connection.FreeLocker()
	//v, ok := connection.record[uid]
	//if !ok {
	//	v = make(map[string]interface{})
	//}
	//v[key] = value
	//connection.record[uid] = v
	ri := gredis.Clone(3)
	_ = ri.Set(key, value, -1)
}

func (connection *Connection) GetRecord(key string) int {
	//connection.GetLocker()
	//defer connection.FreeLocker()
	//v, ok := connection.record[uid]
	//if !ok {
	//	return 0
	//}
	//value, _ := xx.GetInt(v, key)
	//return value
	ri := gredis.Clone(3)
	val, err := ri.GetInt(key)
	if err != nil {
		logging.Debug("connection.GetRecord() Errors: " + err.Error())
		return 0
	}
	return int(val)
}

//
//func (connection *Connection) FetchLocker() bool {
//	if !atomic.CompareAndSwapUint32(&connection.locker, 0, 1) {
//		// 有占用
//		return false
//	} else {
//		return true
//	}
//}
//
//func (connection *Connection) GetLocker() {
//	for {
//		if connection.FetchLocker() {
//			break
//		}
//		runtime.Gosched()
//	}
//}
//
//func (connection *Connection) FreeLocker() {
//	atomic.StoreUint32(&connection.locker, 0)
//}

func (connection *Connection) kick(uid string) {
	go func(uid string) {
		logging.Debug("connection.kick() available: ", connection.Available())
		if connection.Available() {
			_ = connection.sendMsg(message.ParseMsg(message.Message_System, "system", uid, "kicked", false))
		}
		time.Sleep(1 * time.Second)
		connection.Close()
	}(uid)
}

func (connection *Connection) heartbeat() {
	ticker := time.NewTicker(60 * time.Second)
	defer func() {
		ticker.Stop()
		connection.Close()
		logging.Debug("websocket client heartbeat() exited")
	}()

	for {
		<-ticker.C
		if connection.Available() {
			//text := util.GenerateTickContent("ping", make(map[string]interface{}), true)
			//fmt.Println(text)
			//_ = connection.sendRaw([]byte(text))
			ctx, _ := json.Marshal(map[string]interface{}{
				"event":    "ping",
				"needSign": true,
				"data":     map[string]interface{}{},
			})
			connection.Send(message.ParseMsg(message.Message_Tick, "system", "", string(ctx), false))
		} else {
			return
		}
	}
}

func (connection *Connection) pong() {
	_ = connection.sendRaw([]byte("{\"event\":\"pong\",\"data\":\"suceess\"}"))
}
