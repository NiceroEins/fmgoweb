package websocket

import (
	"datacenter/models/message"
	"datacenter/pkg/gredis"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/gorilla/websocket"

	"datacenter/pkg/logging"
)

var (
	ins  *WsServer
	once sync.Once
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 500 * 1024

	// Maximum losses allowed from connection.
	maxLosses = 2

	// Time allowed to receive confirmChan message
	confirmWait = 3 * time.Second
)

type WsServer struct {
	connection map[string]*Connections
	upgrade    *websocket.Upgrader
	hub        chan *message.Message
	opt        chan string
	counter    *message.Counter
	count      uint32
	client     *Connection
}

func Setup() {
	//channel := []string{"/ws/news", "/ws/performance"}
	_ = GetInstance()
}

func GetInstance() *WsServer {
	once.Do(func() {
		ins = newWsServer()
	})
	return ins
}

// 创建一个ws对象
func newWsServer() *WsServer {
	ws := new(WsServer)
	ws.upgrade = &websocket.Upgrader{
		ReadBufferSize:  8192,
		WriteBufferSize: 2048,
		CheckOrigin: func(r *http.Request) bool {
			if r.Method != "GET" {
				logging.Error("websocket.NewWsServer wrong Method: ", r.Method)
				return false
			}
			//if r.URL.Path != "/ws" {
			//	logging.Error("websocket.NewWsServer wrong path: %s", r.URL.Path)
			//	return false
			//}
			return true
		},
	}
	//ws.f = nil
	ws.connection = make(map[string]*Connections)
	ws.hub = make(chan *message.Message, 1000)
	ws.opt = make(chan string, 100)
	ws.counter = message.NewCounter()
	// 启动Hub
	go ws.procHub()
	return ws
}

func (ws *WsServer) ServeHTTP(uid string, f func(string, string) map[string]interface{}, w http.ResponseWriter, r *http.Request, clientIP, channel string) {
	var upgrader = websocket.Upgrader{
		// 解决跨域问题
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logging.Error("websocket.ServeHTTP errors: " + err.Error())
		return
	}
	logging.Info("websocket client " + uid + " connected: " + conn.RemoteAddr().String())
	logging.Debug("websocket client " + uid + " connected: " + clientIP)
	connection := ws.SetConnection(uid, conn, clientIP, channel)
	if connection == nil {
		logging.Error("websocket.ServeHTTP errors: nil connection")
		return
	}
	connection.SetFilter(f)
	ws.opt <- uid
	go connection.receiving(uid)
	go connection.sending(uid)
}

//todo: 拆分行情接入端（转发）及数据处理端
// 链接服务端
func (ws *WsServer) Client(scheme, host, path, query, channel string, bPing bool) *Connection {
	u := url.URL{Scheme: scheme, Host: host, Path: path, RawQuery: query}
	logging.Info("websocket.Client() connecting to ", u.String())
	//header := http.Header{}
	//if bPing {
	//	header.Add("UID", "test01")
	//	header.Add("Channel", "stock")
	//}
	conn, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		logging.Error(err)
		return nil
	}
	connection := ws.SetConnection(channel, conn, "", "")
	connection.SetPing(bPing)
	go connection.receiving(channel)
	go connection.sending(channel)
	if !bPing {
		go connection.heartbeat()
	}
	return connection
}

// procHub, WsServer中用于处理在线数据分发、离线数据入库及客户端上线取历史记录操作
func (ws *WsServer) procHub() {
	for {
		select {
		case msg, ok := <-ws.hub:
			if ok {
				if msg == nil || !msg.Available() {
					//logging.Debug(fmt.Sprintf("websocket procHub: %v", msg))
					continue
				}
				var once sync.Once
				connections := ws.GetConnections(msg.To())
				f := func(ret string) {
					_, err := gredis.Push(msg.From(), msg.To(), string(message.MarshalMsg(msg)))
					if err != nil {
						logging.Error("websocket.procHub() push msg Errors: " + err.Error())
					} else {
						LogStore(msg, ret)
					}
				}
				if connections == nil {
					if msg.NeedStore() {
						go f("nil connections")
					}
					continue
				}
				connections.conn.Range(func(k, v interface{}) bool {
					connection, ok := k.(*Connection)
					if connection == nil || !ok {
						logging.Error("websocket.procHub() convert key to connection failed")
						return true
					}
					// 如果消息类型为客户端上行确认包
					// 则不予转发
					if msg.Type() == message.Message_Confirm {
						connection.SetConfirm(true)
						go connection.Confirm(msg)
						return true
					}
					// 若连接正常则发送
					// 否则存入消息中间件
					if ok, ret := CheckConnection(connection); ok {
						//msg = TraceLog(msg, "Got from hub")
						if msg.Channel() == connection.Channel() {
							go connection.Send(msg)
						} else {
							// 非当前管道的消息
							return true
						}
					} else if msg.NeedStore() {
						once.Do(func() {
							go f(ret)
						})
					}

					return true
				})

			} else {
				logging.Error("websocket.procHub() msg channel Errors")
			}
		case uid, ok := <-ws.opt:
			if ok {
				// 上线时从消息队列取数据
				go ws.Fetch(uid)
				// Todo 记录用户在线情况
			} else {
				logging.Error("websocket.procHub() opt channel Errors")
			}
		}
	}
}

func (ws *WsServer) SetConnection(uid string, conn *websocket.Conn, clientIP, channel string) *Connection {
	old, ok := ws.connection[uid]
	if ok {
		if conn != nil {
			// 0909 restructure:
			old.conn.Range(func(k, v interface{}) bool {
				u, ok := k.(*Connection)
				if !ok || u == nil {
					logging.Error("websocket.SetConnection() convert key to connection failed")
					return true
				}
				if !u.Available() {
					old.conn.Delete(k)
					return true
				}
				status, _ := v.(int)
				logging.Debug("websocket.SetConnection() uid, clientIP, u.clientIP, status = ", uid, clientIP, u.clientIP, status)
				if clientIP == "" || (u.clientIP != clientIP && status == 1) {
					u.kick(uid)
					old.conn.Delete(k)
				}
				return true
			})
			//old.Close()
			//logging.Debug("websocket ptr old: " + fmt.Sprint(old.conn))
		}
		//ws.connection[uid].conn = conn
	} else {
		old = &Connections{}
	}
	//logging.Debug("websocket ptr conn: " + fmt.Sprint(conn))
	// 区分管道
	// 默认管道置空（需要和msg.channel对齐，向上兼容）
	switch {
	case channel == "home":
	case channel == "stock":
	default:
		channel = ""
	}

	c := &Connection{
		conn:        conn,
		sendChan:    make(chan *message.Message),
		confirmChan: make(chan *message.Message),
		closeChan:   make(chan int),
		available:   true,
		loss:        0,
		locker:      0,
		once:        new(sync.Once),
		needConfirm: false,
		bPing:       true,
		clientIP:    clientIP,
		channel:     channel,
		//record:      make(map[string]map[string]interface{}),
	}
	old.clientIP = clientIP
	old.Add(clientIP, c)
	ws.connection[uid] = old
	return c
}

func (ws *WsServer) GetConnection(uid string) *Connection {
	if connections := ws.GetConnections(uid); connections == nil {
		return nil
	} else {
		var conn *Connection
		connections.conn.Range(func(k, v interface{}) bool {
			c, ok := k.(*Connection)
			st, _ := v.(int)
			if ok && c != nil && st == 1 {
				conn = c
				return false
			}
			return true
		})
		return conn
	}
}

func (ws *WsServer) GetConnections(uid string) *Connections {
	connections, ok := ws.connection[uid]
	if ok {
		return connections
	} else {
		// 对方不在线时，不再返回虚拟连接
		//return ws.SetConnection(uid, nil)
		return nil
	}
}

func (ws *WsServer) Fetch(uid string) {
	msgArr, _ := gredis.Pop(uid)
	connections := ws.GetConnections(uid)

	if !CheckConnections(connections) {
		for _, v := range msgArr {
			msg := message.UnmarshalMsg([]byte(v))
			msg.SetStore(false)
			ws.SendToHub(msg)
			// 为防大量消息瞬时阻塞发送通道
			// 给予每个消息0.1s发送间隔
			time.Sleep(100 * time.Millisecond)
		}
	}
}

func (ws *WsServer) SendToHub(msg *message.Message) {
	go func() {
		//logging.Debug(fmt.Sprintf("websocket SendToHub: %v", msg))
		ws.hub <- msg
	}()
}

func (ws *WsServer) SendAllToHub(msg []*message.Message) {
	if msg == nil {
		return
	}
	for _, v := range msg {
		ws.SendToHub(v)
	}
}

func (ws *WsServer) SysSend(msg *message.Message) {
	if msg == nil {
		return
	}
	var msgList []*message.Message
	switch msg.Type() {
	case message.Message_Futures_Info:
		msgList = msg.FilterFutures()
	default:
		msgList = msg.Filter(map[string]interface{}{})
	}
	LogMass(msgList)
	ws.SendAllToHub(msgList)
}

func LogStore(msg *message.Message, ext string) {
	logging.Debug("websocket stored due to " + ext + ": " + string(message.MarshalMsg(msg)))
}

func LogSend(msg *message.Message) {
	logging.Debug("websocket sent: " + string(message.MarshalMsg(msg)))
}

func TraceLog(msg *message.Message, operator string) *message.Message {
	//content := fmt.Sprintf("%s\n%s %s", msg.Msg(), time.Now().Format("2006-01-02 15:04:05"), operator)
	//msg.SetMsg(content)
	logging.Debug("websocket log ", operator, string(message.MarshalMsg(msg)))
	return msg
}

func LogMass(msg []*message.Message) {
	if msg == nil || len(msg) == 0 {
		return
	}
	var ret string
	for i := 0; i < len(msg); i++ {
		ret += " " + msg[i].To()
	}
	logging.Debug("websocket massed to" + ret + " text: " + string(message.MarshalMsg(msg[0])))
}
