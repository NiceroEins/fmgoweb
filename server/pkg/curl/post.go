package curl

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
)

type Post struct {
	Url      string
	PostData string
}

func NewPost(url string, postData string) *Post {
	return &Post{
		Url:      url,
		PostData: postData,
	}
}

//发送请求 json
func (p *Post) Send() (respData string, err error) {
	var jsonstr = []byte(p.PostData)
	buffer := bytes.NewBuffer(jsonstr)
	request, err := http.NewRequest("POST", p.Url, buffer)
	if err != nil {
		fmt.Printf("http.NewRequest%v", err)
		return "", err
	}
	request.Header.Set("Content-Type", "application/json;charset=UTF-8") //添加请求头
	client := http.Client{}                                              //创建客户端
	resp, err := client.Do(request.WithContext(context.TODO()))          //发送请求
	if err != nil {
		fmt.Printf("client.Do%v", err)
		return "", err
	}
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("ioutil.ReadAll%v", err)
		return "", err
	}
	return string(respBytes), nil
}

//发送请求 form data(登陆老系统专用)
func SendWithFormData(url string, postData *map[string]string) string {
	method := "POST"
	body := new(bytes.Buffer)
	w := multipart.NewWriter(body)
	for k, v := range *postData {
		w.WriteField(k, v)
	}
	w.Close()
	req, _ := http.NewRequest(method, url, body)
	req.Header.Set("Content-Type", w.FormDataContentType())
	resp, _ := http.DefaultClient.Do(req)
	//data, _ := ioutil.ReadAll(resp.Body)
	cookies := resp.Cookies()
	resp.Body.Close()
	if len(cookies) > 0 {
		return fmt.Sprintf("%s", cookies[1])
	} else {
		return ""
	}

	//fmt.Println(resp.StatusCode)
	//fmt.Printf("%s", data)
}
