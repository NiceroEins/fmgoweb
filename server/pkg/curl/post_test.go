package curl

import (
	"fmt"
	"testing"
)

//钉钉机器人发送群消息
func TestNewPost(t *testing.T) {
	jsonStr := `{
    "msgtype": "text", 
    "text": {
        "content": "我就是我, 是不一样的烟火@156xxxx8827"
    }, 
    "at": {
        "atMobiles": [
            "156xxxx8827", 
            "189xxxx8325"
        ], 
        "isAtAll": false
    }
}`
	handle := NewPost("https://oapi.dingtalk.com/robot/send?access_token=73b92551c6d13f8c2e1273392cd890104f3a71d27d7f573ee3ab717e1672e7da", jsonStr)
	res, _ := handle.Send()
	fmt.Println(res)
}
