package mongo

import (
	"context"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"gopkg.in/mgo.v2"
	"time"
)

// pool 连接池模式
func ConnectToMongoDB(name string) (*mongo.Database, context.Context, context.CancelFunc, error) {
	// 设置连接超时时间
	ctx, cancel := context.WithTimeout(context.Background(), setting.MongoDBSetting.Timeout*time.Second)

	// 通过传进来的uri连接相关的配置
	o := options.Client().ApplyURI("mongodb://" + setting.MongoDBSetting.User + ":" + setting.MongoDBSetting.Password + "@" + setting.MongoDBSetting.URI)
	// 设置最大连接数 - 默认是100 ，不设置就是最大 max 64
	o.SetMaxPoolSize(setting.MongoDBSetting.MaxPoolSize)
	// 发起链接
	client, err := mongo.Connect(ctx, o)
	if err != nil {
		fmt.Println("mongodb://" + setting.MongoDBSetting.User + ":" + setting.MongoDBSetting.Password + "@" + setting.MongoDBSetting.URI)
		logging.Error("MongoDB 链接出错：", err.Error())
		return nil, nil, cancel, err
	}
	// 判断服务是不是可用
	if err = client.Ping(context.Background(), readpref.Primary()); err != nil {
		logging.Error("MongoDB 服务不可用：", err.Error())
		return nil, nil, cancel, err
	}
	// 返回 client
	return client.Database(name), ctx, cancel, nil
}

var session *mgo.Session

func Setup() {
	_, _ = Session()
}

func Session() (*mgo.Session, error) {

	if session == nil {
		var err error
		session, err = mgo.Dial("mongodb://" + setting.MongoDBSetting.User + ":" + setting.MongoDBSetting.Password + "@" + setting.MongoDBSetting.URI)
		if err != nil {
			logging.Error("MongoDB 服务不可用：", err.Error())
			return nil, err
		}
		session.SetPoolLimit(1000)
		session.SetSocketTimeout(15 * time.Second)
		// Optional. Switch the session to a monotonic behavior.
		session.SetMode(mgo.Monotonic, true)
	}

	return session.Clone(), nil
}

func GetMongoDB(database, collection string, f func(*mgo.Collection) error) error {
	session, err := Session()
	if err != nil {
		logging.Error("MongoDB 服务不可用：", err.Error())
		return err
	}
	defer func() {
		if session != nil {
			session.Close()
		}
	}()
	c := session.DB(database).C(collection)
	return f(c)
}
