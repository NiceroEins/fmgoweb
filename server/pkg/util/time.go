package util

import (
	"fmt"
	"strconv"
	"time"
)

//生成毫秒时间戳字符串
func GenMicTimeStr() string {
	return strconv.FormatInt(time.Now().UnixNano()/1e6, 10)
}

//毫秒时间戳转字符串
func MicTimeToFormatStr() string {
	tm := time.Now()
	return tm.Format("2006-01-02 15:04:05")
}

//生成毫秒时间戳
func GenMicTime() int64 {
	return time.Now().UnixNano() / 1e6
}

//毫秒时间戳转字符串
func MicTimeToStr(i64 int64) string {
	tm := time.Unix(i64/1e3, 0)
	return tm.Format("2006-01-02 15:04:05")
}

//毫秒时间戳转字符串
func StrMicTimeToStr(s string) (string, error) {
	i64, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return "", err
	}
	tm := time.Unix(i64/1e3, 0)
	return tm.Format("2006-01-02 15:04:05"), nil
}

//字符串转毫秒时间戳
func StrToMicTime(s string) string {
	//获取本地location   	//待转化为时间戳的字符串 注意 这里的小时和分钟还要秒必须写 因为是跟着模板走的 修改模板的话也可以不写
	timeLayout := "2006-01-02 15:04:05"                    //转化所需模板
	loc, _ := time.LoadLocation("Local")                   //重要：获取时区
	theTime, _ := time.ParseInLocation(timeLayout, s, loc) //使用模板在对应时区转化为time.time类型
	return strconv.FormatInt(theTime.Unix()*1e3, 10)
}

//生成当前秒时间戳
func GenSecondTime() (timeSecond int64) {
	timeSecond = time.Now().Unix()
	return
}

//生成当天的0点和23：59：59,返回秒
func GenBeginEndTime() (beginTimeNum, endTimeNum int64) {
	timeStr := time.Now().Format("2006-01-02")
	t, _ := time.ParseInLocation("2006-01-02", timeStr, time.Local)
	beginTimeNum = t.Unix()
	endTimeNum = beginTimeNum + 86400
	return beginTimeNum, endTimeNum - 1
}

//根据传入时间,生成对应日期的 0点和23：59：59,返回秒
func GenBeginEndTimeThree(s string) (beginTimeNum, endTimeNum int64) {
	t, _ := time.ParseInLocation("2006-01-02", s, time.Local)
	beginTimeNum = t.Unix()
	endTimeNum = beginTimeNum + 86400
	return beginTimeNum, endTimeNum - 1
}

//根据指定时间生成当天的0点和23：59：59(日期)
func GenBeginEndTimeTwo(s string) (string, string) {
	t, _ := time.ParseInLocation("2006-01-02", s, time.Local)
	beginTimeNum := t.Unix()
	endTimeNum := beginTimeNum + 86399
	//转日期字符串
	beginTimeStr := Timastamp2DateTwo(beginTimeNum)
	endTimeStr := Timastamp2DateTwo(endTimeNum)
	return beginTimeStr, endTimeStr
}

//根据指定时间生成当天的0点和23：59：59()

//当前时间转:2020/06/08
func GenTimeToYearMonthDay() (res string) {
	now := time.Now()
	res = now.Format("2006/01/02") //年月日
	return res
}

//生成指定日期的00:00:01
func GenDate(date string) int64 {
	the_time, err := time.ParseInLocation("2006-01-02", date, time.Local)
	if err == nil {
		unix_time := the_time.Unix()
		return unix_time
	}
	return 0
}

//时间戳(秒)转日期
func Timastamp2Date(timeStamp int) string {
	//转化所需模板
	timeLayout := "2006-01-02 15:04:05"
	//进行格式化
	datetime := time.Unix(int64(timeStamp), 0).Format(timeLayout)
	return datetime
}
func Timastamp2DateTwo(timeStamp int64) string {
	//转化所需模板
	timeLayout := "2006-01-02 15:04:05"
	//进行格式化
	datetime := time.Unix(timeStamp, 0).Format(timeLayout)
	return datetime
}

//当前时间转年月日 2020-06-06 06:06:06(string)
func GenDateFormat() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

//当前时间日期 2020-06-06
func GenDateFormat2() string {
	return time.Now().Format("2006-01-02")
}

//当前时间转年月日 2020-06-06 06:06:06(time.Time)
func GenDateFormatTime() (timeTime time.Time, err error) {
	timeTime, err = time.ParseInLocation("2006-01-02 15:04:05", GenDateFormat(), time.Local)
	if err != nil {
		return time.Time{}, err
	}
	return
}

//时间字符串转年月日 2020-06-06 06:06:06(time.Time)
func GenDateFormatTime2(t string) (timeTime time.Time) {
	timeTime, _ = time.ParseInLocation("2006-01-02 15:04:05", t, time.Local)
	return
}

func ConvTimeToStdTime(t time.Time) (formatted string) {
	formatted = fmt.Sprintf("%v", time.Time(t).Format("2006-01-02 15:04:05"))
	return
}
func ConvTimeToStdTime2(t time.Time) (formatted string) {
	formatted = fmt.Sprintf("%v", time.Time(t).Format("2006-01-02"))
	return
}

//获取上一个月的时间
func GetLastMonthTime(nowTime time.Time) string {
	getTime := nowTime.AddDate(0, -1, 0)             //年，月，日   获取一个月前的时间
	resTime := getTime.Format("2006-01-02 15:04:05") //获取的时间的格式
	return resTime
}

//传入时间，返回时间上一周对应的时间(年月日),
func GetLastOneWeekTime(s string) string {
	tt, _ := time.ParseInLocation("2006-01-02", s, time.Local) //2006-01-02 15:04:05是转换的格式如php的"Y-m-d H:i:s"
	fmt.Println(tt.Unix())
	dstTime := tt.Unix() - 60*60*24*7
	dstTimeStr := time.Unix(dstTime, 0).Format("2006-01-02")
	return dstTimeStr
}

//传入时间，返回时间上二周对应的时间(年月日)
func GetLastTwoWeekTime(s string) string {
	tt, _ := time.ParseInLocation("2006-01-02", s, time.Local) //2006-01-02 15:04:05是转换的格式如php的"Y-m-d H:i:s"
	fmt.Println(tt.Unix())
	dstTime := tt.Unix() - 60*60*24*7*2
	dstTimeStr := time.Unix(dstTime, 0).Format("2006-01-02")
	return dstTimeStr
}

const YMDHM = "2006-01-02 15:04"
const YMDHMS = "2006-01-02 15:04:05"
const YMD = "2006-01-02"
const MD = "01-02"
const HMS = "15:04:05"

func TimePtr2String(t *time.Time, layout string) string {
	if t != nil {
		return t.Format(layout)
	} else {
		return ""
	}
}

func ReportPeriod(offset int) (string, int) {
	reportList := []string{"03-31", "06-30", "09-30", "12-31"}
	reportStartList := []string{"01-01", "01-01", "05-01", "09-01", "11-01"}
	//date:=time.Date(2020,8,31,0,0,1,0,time.Local)
	date := time.Now()
	var reportPeriod string
	q := 0
	for i, r := range reportStartList {
		if date.Format(MD) >= r {
			i = i - 1
			if (i+offset)%4 >= 0 {
				q = (i + offset) % 4
				reportPeriod = strconv.Itoa(date.Year()+(i+offset)/4) + "-" + reportList[q]
			} else {
				q = (i+offset)%4 + 4
				reportPeriod = strconv.Itoa(date.Year()+(i+offset-4)/4) + "-" + reportList[q]
			}
		}

	}
	return reportPeriod, q + 1
}
