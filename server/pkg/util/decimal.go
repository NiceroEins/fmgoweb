package util

import "github.com/shopspring/decimal"

func DecimalPtr2String(d *decimal.Decimal) string {
	if d != nil {
		return d.Round(2).String()
	} else {
		return ""
	}
}

func NullDecimal2String(d decimal.NullDecimal) string {
	if d.Valid == false {
		return ""
	} else {
		return d.Decimal.Round(2).String()
	}
}

func CalculateRate(before, now decimal.NullDecimal) decimal.NullDecimal {
	if !(before.Valid && now.Valid) || before.Decimal == decimal.Zero {
		return decimal.NullDecimal{}
	}
	return decimal.NullDecimal{
		now.Decimal.Sub(before.Decimal).Div(before.Decimal).Mul(decimal.NewFromInt32(100)),
		true,
	}
}
