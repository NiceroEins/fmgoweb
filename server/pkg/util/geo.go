package util

import (
	"math"
	"math/rand"
	"time"
)

type Pos struct{ X, Y float64 }

func (p Pos) Add(q Pos) Pos {
	p.X += q.X
	p.Y += q.Y
	return p
}

func (p Pos) Sub(q Pos) Pos {
	p.X -= q.X
	p.Y -= q.Y
	return p
}

func (p Pos) Mul(s float64) Pos {
	p.X *= s
	p.Y *= s
	return p
}

func (p Pos) Dot(q Pos) float64 {
	return p.X*q.X + p.Y*q.Y
}

func (p Pos) Len() float64 {
	return math.Hypot(p.X, p.Y)
}

func (p Pos) Distance(q Pos) float64 {
	return math.Hypot(q.X-p.X, q.Y-p.Y)
}

// 判断两个方块是否重叠
func Overlap(p1, p2 Pos, r float64) bool {
	s := p1.Sub(p2)
	b := math.Abs(s.X) < r
	return b && math.Abs(s.Y) < r
}

func (dp Pos) Collide(p1, p2 Pos, r float64) Pos {
	p := p1.Add(dp)
	if Overlap(p, p2, r) {
		s := p1.Sub(p2)
		dp.X = (math.Abs(s.X) - r) * Sign(dp.X)
		dp.Y = (math.Abs(s.Y) - r) * Sign(dp.Y)
	}
	return dp
}

func (p Pos) Map() map[string]float64 {
	m := map[string]float64{
		"x": p.X, "y": p.Y,
	}
	return m
}

func Displace(dir string, dis float64) Pos {
	var x, y float64
	switch dir {
	case "up":
		y = dis
		break
	case "down":
		y = -dis
		break
	case "left":
		x = -dis
		break
	case "right":
		x = dis
		break
	}
	return Pos{X: x, Y: y}
}

//生成长度为l的随机字符串
func GenerateRandomString(l int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < l; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

//生成count个[start,end)结束的不重复的随机数
func GenerateRandomNumbersEx(start int, end int, count int) []int {
	//范围检查
	if end < start || (end-start) < count {
		return nil
	}
	//存放结果的slice
	nums := make([]int, 0)
	//随机数生成器，加入时间戳保证每次生成的随机数不一样
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for len(nums) < count {
		//生成随机数
		num := r.Intn(end-start) + start
		//查重
		exist := false
		for _, v := range nums {
			if v == num {
				exist = true
				break
			}
		}
		if !exist {
			nums = append(nums, num)
		}
	}
	return nums
}

//生成总数为total的count个随机数
func GenerateRandomNumbers(count int, total int) []int {
	//平均值
	var avg int
	avg = total / count
	if total < count {
		return []int{0, 0, total}
	}
	if avg < 2 {
		return []int{1, 1, total - 2}
	}
	//设震荡区间为平均值的一半
	rangeV := avg / 2
	arr := make([]int, count)
	ran := rand.New(rand.NewSource(time.Now().UnixNano()))
	//将数组元素先设成平均值
	for i := 0; i < len(arr); i++ {
		arr[i] = avg
	}
	//随机获取2个数组元素，一个加上震荡值，一个减去震荡值
	for i := 0; i < len(arr); i++ {
		index := ran.Intn(len(arr))
		if i != index {
			add := ran.Intn(rangeV) + 1
			//不能减成负的
			if arr[index]-add >= 0 {
				arr[i] += add
				arr[index] -= add
			}
		}
	}
	//修正数组元素，因为平均值很可能不是由整除得到的。
	last := total - avg*count
	for i := 0; i < last; i++ {
		index := ran.Intn(len(arr))
		arr[index] += 1
	}
	return arr
}
