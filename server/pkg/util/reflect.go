package util

import "reflect"

func InterfaceAssert(unknow interface{})(retType reflect.Type, val reflect.Value){
	retType = reflect.TypeOf(unknow)
	val = reflect.ValueOf(unknow)
	return retType,val
}
