package util

import "github.com/shopspring/decimal"

func AtoB2(num1, num2 float64) float64 {
	decimal.DivisionPrecision = 2
	d1, _ := decimal.NewFromFloat(num1).Sub(decimal.NewFromFloat(num2)).Float64()
	return d1
}
