package util

import (
	"datacenter/pkg/setting"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

func GenerateTickContent(event string, data map[string]interface{}, needSign bool) string {
	st := struct {
		Event string                 `json:"event"`
		Data  map[string]interface{} `json:"data"`
	}{
		Event: event,
		Data:  data,
	}
	// test
	user := setting.StockSetting.User
	pwd := setting.StockSetting.Password
	if needSign {
		if data == nil {
			data = make(map[string]interface{})
		}
		ts := time.Now().Unix()
		sign := EncodeMD5(fmt.Sprintf("u=%s&p=%s&stamp=%d", user, pwd, ts))
		data["u"] = user
		data["sign"] = strings.ToUpper(sign)
		data["stamp"] = ts
	}
	ret, _ := json.Marshal(st)
	return string(ret)
}
