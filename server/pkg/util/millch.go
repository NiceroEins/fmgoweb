package util

import (
	"sync"
)

var (
	MaxWorker = 256
	onceQ     sync.Once
)

// Job represents the job to be run
type Job struct {
	Msg  map[string]interface{}
	Func func(map[string]interface{}) error
}

func NewJob(msg map[string]interface{}, action func(map[string]interface{}) error) *Job {
	ret := new(Job)
	ret.Msg = msg
	ret.Func = action
	return ret
}

//MARK: Do the callback
func (j *Job) Action() error {
	return j.Func(j.Msg)
}

// A buffered channel that we can send work requests on.
var JobQueue = make(chan Job)

//ACTION:只需调用这个函数就行
func SendTheMillData(job *Job) {
	onceQ.Do(func() {
		//Run this dispatcher below
		//Println("The maxWorker num is : ", MaxWorker)
		dispatcher := NewDispatcher(MaxWorker)
		dispatcher.Run()
	})
	JobQueue <- *job
}

// Worker represents the worker that executes the job
type Worker struct {
	WorkerPool chan chan Job
	JobChannel chan Job
	quit       chan bool
}

func NewWorker(workerPool chan chan Job) Worker {
	return Worker{
		WorkerPool: workerPool,
		JobChannel: make(chan Job),
		quit:       make(chan bool)}
}

// Start method starts the run loop for the worker, listening for a quit channel in
// case we need to stop it
func (w Worker) Start() {
	go func() {
		//defer TryE()
		for {
			// register the current worker into the worker queue.
			w.WorkerPool <- w.JobChannel

			select {
			case job := <-w.JobChannel:
				// we have received a work request.
				if err := job.Action(); err != nil {
					//Println("Millch get a error: ", err)
				}

			case <-w.quit:
				// we have received a signal to stop
				return
			}
		}
	}()
}

// Stop signals the worker to stop listening for work requests.
func (w Worker) Stop() {
	go func() {
		w.quit <- true
	}()
}

type Dispatcher struct {
	// A pool of workers channels that are registered with the dispatcher
	WorkerPool chan chan Job
	MaxWorkers int
}

func NewDispatcher(maxWorkers int) *Dispatcher {
	pool := make(chan chan Job, maxWorkers)
	ret := new(Dispatcher)
	ret.MaxWorkers = maxWorkers
	ret.WorkerPool = pool
	return ret
}

func (d *Dispatcher) Run() {
	// starting n number of workers
	for i := 0; i < d.MaxWorkers; i++ {
		worker := NewWorker(d.WorkerPool)
		worker.Start()
	}

	go d.dispatch()
}

func (d *Dispatcher) dispatch() {
	for {
		select {
		case job := <-JobQueue:
			// a job request has been received
			go func(job Job) {
				// try to obtain a worker job channel that is available.
				// this will block until a worker is idle
				jobChannel := <-d.WorkerPool

				// dispatch the job to the worker job channel
				jobChannel <- job
			}(job)
		}
	}
}
