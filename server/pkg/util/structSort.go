package util

type Person struct {
	Name string // 姓名
	Age  int    // 年纪
}

// 按照 Person.Age 从大到小排序
type PersonSlice []Person

//func (a PersonSlice) Len() int { // 重写 Len() 方法
//	return len(a)
//}
//func (a PersonSlice) Swap(i, j int) { // 重写 Swap() 方法
//	a[i], a[j] = a[j], a[i]
//}
//func (a PersonSlice) Less(i, j int) bool { // 重写 Less() 方法， 从大到小排序
//	return a[j].Age < a[i].Age
//}

//func aaa() {
//	people := []Person{
//		{"zhang san", 12},
//		{"li si", 30},
//		{"wang wu", 52},
//		{"zhao liu", 26},
//	}
//
//	fmt.Println(people)
//
//	sort.Sort(PersonSlice(people)) // 按照 Age 的逆序排序
//	fmt.Println(people)
//
//	sort.Sort(sort.Reverse(PersonSlice(people))) // 按照 Age 的升序排序
//	fmt.Println(people)
//
//}

//行业追踪页面:返回数据接结构体
type SearchIndustryTrackMysql struct {
	Id                         int64    //公告表主键id
	Name                       string   //股票名称
	Code                       string   //股票代码
	ExpectedPublishDate        string   //披露时间
	DisclosureTimeHistory      string   //披露时间历史
	DisclosureChangeAt         string   //披露时间更改时间
	IndividualShareTrack       string   //个股跟踪
	IndividualShareTrackSub100 string   //个股跟踪截取 100
	PerformanceIncrUpper       *float64 //本期预告(对应预告表里面的业绩变动幅度上限)
	PerformanceIncrLower       *float64 //本期预告(对应预告表里面的业绩变动幅度下限)
	ForecastPublishDate        string   //预告时间(对应预告表里面的公告日期)
	ForecastIncrease           string   //预告涨幅 中间变量
	ForecastIncreaseSorted     *float64 //预告涨幅 中间变量（返回字段）
	ForecastIncrease1          *float64 //预告涨幅 (看预告表里面的publish_date,和code去行情表里面，根据code publish 来查询,查询结果按照trade_date排序,涨幅天数1,就取第一个，3就取第三个)
	ForecastIncrease3          *float64 //预告涨幅 (看预告表里面的publish_date,和code去行情表里面，根据code publish 来查询,查询结果按照trade_date排序,涨幅天数1,就取第一个，3就取第三个)
	ForecastIncrease5          *float64 //预告涨幅 (看预告表里面的publish_date,和code去行情表里面，根据code publish 来查询,查询结果按照trade_date排序,涨幅天数1,就取第一个，3就取第三个)
	NetProfitYear              *float64 //本期公告(年度净利润！！！)
	AnnouncementIncrease       string   //公告涨幅  中间变量
	AnnouncementIncreaseSorted *float64 //公告涨幅  中间变量(返回字段)
	AnnouncementIncrease1      *float64 //公告涨幅  (同上，把预告表里面的换成公告表里面的就行了)
	AnnouncementIncrease3      *float64 //公告涨幅  (同上，把预告表里面的换成公告表里面的就行了)
	AnnouncementIncrease5      *float64 //公告涨幅  (同上，把预告表里面的换成公告表里面的就行了)

	NetProfit1 *float64 //净利润(4季度)
	NetProfit2 *float64 //净利润(4季度)
	NetProfit3 *float64 //净利润(4季度)
	NetProfit4 *float64 //净利润(4季度)

	NetProfitGrowthRate1 *float64 //净利润同比(4季度)
	NetProfitGrowthRate2 *float64 //净利润同比(4季度)
	NetProfitGrowthRate3 *float64 //净利润同比(4季度)
	NetProfitGrowthRate4 *float64 //净利润同比(4季度)

	BusinessRevenue1 *float64 //营业总收入(4季度)
	BusinessRevenue2 *float64 //营业总收入(4季度)
	BusinessRevenue3 *float64 //营业总收入(4季度)
	BusinessRevenue4 *float64 //营业总收入(4季度)

	BusinessRevenueGrowthRate1 *float64 // 营业总收入同比(4季度)
	BusinessRevenueGrowthRate2 *float64 // 营业总收入同比(4季度)
	BusinessRevenueGrowthRate3 *float64 // 营业总收入同比(4季度)
	BusinessRevenueGrowthRate4 *float64 // 营业总收入同比(4季度)

	NetProfitAfterDedQuarter1 *float64 //扣非净利润(4季度)
	NetProfitAfterDedQuarter2 *float64 //扣非净利润(4季度)
	NetProfitAfterDedQuarter3 *float64 //扣非净利润(4季度)
	NetProfitAfterDedQuarter4 *float64 //扣非净利润(4季度)

	NetProfitAfterDedGrowthRate1 *float64 //扣非净利润同比(4季度)
	NetProfitAfterDedGrowthRate2 *float64 //扣非净利润同比(4季度)
	NetProfitAfterDedGrowthRate3 *float64 //扣非净利润同比(4季度)
	NetProfitAfterDedGrowthRate4 *float64 //扣非净利润同比(4季度)

	CashFlowPerShare1 *float64 //每股现金流(4季度)
	CashFlowPerShare2 *float64 //每股现金流(4季度)
	CashFlowPerShare3 *float64 //每股现金流(4季度)
	CashFlowPerShare4 *float64 //每股现金流(4季度)

	//GrossProfitMargin           string //总销售毛利率(%)
	GrossProfitMarginQuarter1 *float64 //单季度销售毛利率(%)(4季度)
	GrossProfitMarginQuarter2 *float64 //单季度销售毛利率(%)(4季度)
	GrossProfitMarginQuarter3 *float64 //单季度销售毛利率(%)(4季度)
	GrossProfitMarginQuarter4 *float64 //单季度销售毛利率(%)(4季度)

	RealName     string //真实姓名
	IndustryName string //行业名称
}

//u_p_a,u_p_e两表的结构体

type UpaUpe struct {
	//u_p_a表
	Id                          int64    `json:"id"`
	Code                        string   `json:"code"`
	Name                        string   `json:"name"`
	IndividualShareTrack        string   `json:"individual_share_track"`
	NetProfitYear               *float64 `json:"net_profit_year"`
	NetProfit                   *float64 `json:"net_profit"`
	NetProfitQuarter            *float64 `json:"net_profit_quarter"`
	NetProfitGrowthRate         *float64 `json:"net_profit_growth_rate"`
	BusinessRevenue             *float64 `json:"business_revenue"`
	BusinessRevenueGrowthRate   *float64 `json:"business_revenue_growth_rate"`
	NetProfitAfterDedQuarter    *float64 `json:"net_profit_after_ded_quarter"`
	NetProfitAfterDedGrowthRate *float64 `json:"net_profit_after_ded_growth_rate"`
	CashFlowPerShare            *float64 `json:"cash_flow_per_share"`
	APublishDate                string   `json:"a_publish_date"`
	FPublishDate                string   `json:"f_publish_date"`
	GrossProfitMargin           *float64 `json:"gross_profit_margin"`
	GrossProfitMarginQuarter    *float64 `json:"gross_profit_margin_quarter"`

	PerformanceIncrUpper *float64 `json:"performance_incr_upper"`
	PerformanceIncrLower *float64 `json:"performance_incr_lower"`
	//行业表
	Realname              string `json:"realname"`
	IndustryName          string `json:"industry_name"`
	DisclosureTime        string `json:"disclosure_time"`
	DisclosureTimeHistory string `json:"disclosure_time_history"`
	DisclosureChangeAt    string `json:"disclosure_change_at"`
}

//type StructSlice []SearchIndustryTrackMysql
//
//func (a StructSlice) Len() int { // 重写 Len() 方法
//	return len(a)
//}
//func (a StructSlice) Swap(i, j int) { // 重写 Swap() 方法
//	a[i], a[j] = a[j], a[i]
//}
//func (a StructSlice) Less(i, j int) bool { // 重写 Less() 方法， 从大到小排序
//	return a[j].Age < a[i].Age
//}
//
//func StructSort4(s StructSlice, order string, column string) {
//
//}
