package util

import (
	"datacenter/pkg/gredis"
	"fmt"
	"log"
	"strconv"
	"strings"
	"testing"
)

func TestDesDecrypt(t *testing.T) {
	a := "b1855a068bc9200aae558c64b06fb83a6aa509e5f8251ca7"
	b, err := DesDecrypt(a)
	if err != nil {
		t.Error(err.Error())
	}
	t.Log(b)
}

func TestDesEncrypt(t *testing.T) {
	var a string
	var err error
	if a, err = DesEncrypt("asdasfasfafdasdasdasdd"); err != nil {
		t.Error(err.Error())
	}
	t.Logf("%s", a)
}

//slice int去重
func TestRemoveDuplicateElementInt(t *testing.T) {
	s := []int{1, 2, 3, 4, 5, 1, 3, 5}

	fmt.Println(RemoveDuplicateElementInt(s))
}

func TestRemoveDuplicateElementStr(t *testing.T) {
	s := []string{"hello", "world", "hello", "golang", "hello", "ruby", "php", "java"}
	fmt.Println(RemoveDuplicateElementStr(s))
}

type TestPerson struct {
	Age int
}

func TestIsEmpty(t *testing.T) {
	var (
		//testStr     string = ""
		//testInt     int = 0
		//testFloat64 float64 = 0.01
		//testFloat32 float32 = 0.00
		testStruct TestPerson
		//testSlice []string
		//testArray [10]int
		//testMap map[interface{}]interface{}
		testPtr       *int
		testInterface interface{}
	)
	testStruct.Age = 0
	//t.Log(IsEmpty(testStr))
	//t.Log(IsEmpty(testInt))
	//t.Log(IsEmpty(testFloat64))
	//t.Log(IsEmpty(testFloat32))
	//testStruct.Age = 0
	//t.Log(IsEmpty(testStruct))
	//testSlice = append(testSlice,"")
	//t.Log(IsEmpty(testSlice))
	//testArray[0] = 0
	//fmt.Println("数组长度:",len(testArray))
	//t.Log(IsEmpty(testArray))
	//testMap = make(map[interface{}]interface{})
	//testMap["a"] = 2
	//t.Log(IsEmpty(testMap))
	a := 0
	testPtr = &a

	t.Log(IsEmpty(testPtr))
	//testInterface = 1
	t.Log(IsEmpty(testInterface))

}

type test struct {
	disclosure_time string
	str             string
}

//func TestStructSort(t *testing.T) {
//	s := make([]test, 5)
//	s[0] = test{disclosure_time: "2020-01-02", str: "test2"}
//	s[1] = test{disclosure_time: "2020-01-07", str: "test4"}
//	s[2] = test{disclosure_time: "2019-12-12", str: "test1"}
//	s[3] = test{disclosure_time: "2019-05-05", str: "test5"}
//	s[4] = test{disclosure_time: "2019-07-07", str: "test3"}
//	s[4] = test{disclosure_time: "", str: "test6"}
//	a,b:= StructSort(s,"asc")
//	fmt.Println(a,b)
//
//
//}

func TestSortStrSlice(t *testing.T) {
	strSlice := make([]string, 0)
	strSlice = append(strSlice, "1.1")
	strSlice = append(strSlice, "3.6")
	strSlice = append(strSlice, "2.6")
	strSlice = append(strSlice, "10")
	strSlice = append(strSlice, "11")
	strSlice = append(strSlice, "99.987")
	res := SortStrSlice(&strSlice, "desc")
	t.Log(res)
}

func TestSub100ChineseCharacters(t *testing.T) {
	str := "日常开发中项目经理扔给你一个慢查询日志让你优化，是不是经常束手无策，虽然有 Explain 帮你优化 SQL，但还是迷迷糊糊不知道打印出来的参数的底层逻辑，其实很大程度上是不了解 MySQL 的索引原理导致的，不了解索引的底层原理又怎么能写出高效率的 SQL 语句呢。本场 Chat 帮大家总结了索引的底层原理和利用索引优化慢查询的建议：索引从数据结构角度分为：BTree 索引，Hash 索引，Fulltext 索引索引从存储角度分为：聚集索引和辅助索引索引从逻辑角度分为：主键索引、唯一索引、普通索引、单列索引、复合索引、覆盖索引建立索引的三大原则：最左匹配原则、高离散度原则、长度短小原则B+Tree 和 B-Tree 的区别？为什么 Mysql 中 B+Tree 的高度只有 1-3 层？B+Tree 最多可以存多少数据？Mysaim 的索引和Innodb 索引的区别一个表最多可以建多少个索引，Varchat 类型的列的长度超过 255 就不能建索引是为什么？怎么利用覆盖索引优化 SQL利用索引排序优化 SQL关于索引查找的随机 IO 和顺序 IO什么是回表查询，覆盖索引为什么可以避免回表查询？"
	dst := Sub100ChineseCharacters(str)
	t.Log(dst)
}

func TestSubFirstChar(t *testing.T) {
	t.Log(SubFirstChar("223456"))
}

func TestCount(t *testing.T) {
	t.Log(CountDigit("这里1SJ打卡08a————*&）1"))
}

func TestSubKeyWordBeforeBehind100(t *testing.T) {
	str := "2020 年 07 月 21 日 \n投资评级：买入(首次) \n目标价:194.5 元 \n市场数据 \n收盘价（元）           \n2 0 20-07-21 \n156.99 \n一年内最低/最高(元)  \n52.13/174.42 \n市盈率 \n市净率 \n基础数据 \n净资产收益率(%) \n资产负债率(%) \n总股本(亿股) \n60.4 \n9.17 \n46.95 \n13.3 \n1.2 \n最近 12 月股价走势 \n八方股份\n上证指数\n2 10 %\n1 80 %\n1 50 %\n1 20 %\n9 0%\n6 0%\n3 0%\n0 %\n- 30 %\n2 01 9 -0 7\n2 01 9 -1 1\n2 02 0 -0 3\n联系信息 \n彭勇 \nSAC 证书编号：S0160517110001 \npengy@ctsec.com \n管正月 \ngzy@ctsec.com \n相关报告 \n分析师 \n联系人 \n021-68592396 \n表 1：公司财务及预测数据摘要 \n营业收入（百万） \n  增长率 \n归属母公司股东净利润（百万） \n  增长率 \n每股收益（元） \n市盈率 (倍) \n数据来源：贝格数据，财通证券研究所 \n请阅读最后一页的重要声明 \n证\n券\n研\n究\n报\n告\n事\n件\n点\n评\n公\n司\n研\n究\n财\n通\n证\n券\n研\n究\n所\n八方股份(603489) \n立足全球，电踏车电机自主龙头 \n\uF06C 全球中置电机领先者，电踏车核心供应商 \n公司专注于电踏车电机及配套电气系统，是全球少数具备力矩传\n感器自主生产能力的企业之一。公司产品在欧洲、美国市场比肩\n德国博世、日本禧玛诺等国际顶尖品牌。受益于海外电踏车市场\n的繁荣，公司业绩持续高增，2019 年公司实现营业收入 12.0 亿元，\n同比增加 27.0%，归母净利润 3.2 亿元，同比增加 39. 4%，综合毛利\n率 42. 8%，净利率 27.1%。 2015-2019 年公司营业收入复合增长率\n44.7%，归母净利润复合增长率 67.2%。 \n\uF06C 轻松代步+运动健身，电踏车市场前景广阔 \n作为传统自行车的升级，电踏车在不改变“骑行”特质的前提下，\n让骑行变得更加轻松有趣，骑行半径更大，在发达国家受到热捧，\n2018 年全球电踏车销量 350 万辆，其中欧洲市场 278 万辆，同比增\n长 33%，日本市场 67 万辆，美国市场 25-30 万辆。受老龄化轻松代\n步和年轻人运动健身等个性需求增长，发达国家电踏车渗透率有\n望持续提升，并进一步向发展中国家的富裕阶层扩展，预计电踏\n车将持续保持高景气，长期来看全球销量有望达到千万辆以上。 \n\uF06C 募资扩产推动业务扩张，量价齐升打开成长空间 \n公司近三年产能利用率超过 130%，产能紧张，随着 IPO 募投项目\n的稳步推进有望大幅缓解产能瓶颈，助力公司业务扩张。受益欧\n美电踏车市场的快速增长，以及公司高毛利产品中置电机的比重\n提升，公司有望实现量价齐升，打开成长空间。 \n\uF06C 盈利预测及投资建议 \n公司是电踏车电机自主龙头，产品水平已经比肩海外巨头，受益\n全球电踏车渗透率提升，尤其欧洲和美国市场的快速增长，公司\n下游市场空间广阔，同时公司产品结构优化，中置电机占比不断\n提升，公司步入量价齐升的快速成长阶段，预计公司 2020-2022 年\n的 EPS 为 3.89/5.37 /7. 20 元，对应的 PE 为 40.3/ 29. 3/21.8 倍，首次覆\n盖，给予公司 2020 年 50 倍 PE，对应目标价 194.5 元，给予公司“买\n入”评级。 \n\uF06C 风险提示:电踏车市场不及预期；海外经济下行风险；行业竞争加\n剧风险。 \n2 0 1 8A \n942 \n53.1% \n232 \n335.7% \n1.94 \n81.1 \n2 0 1 9A \n2 0 2 0E \n2 0 2 1E \n2 0 2 2E \n1,197 \n27.0% \n324 \n39.4% \n2.70 \n58.2 \n1,556 \n30.0% \n467 \n44.2% \n3.89 \n40.3 \n2,178 \n40.0% \n644 \n37.9% \n5.37 \n29.3 \n2,831 \n30.0% \n864 \n34.1% \n7.20 \n21.8 \n以才聚财，财通天下 \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n     \n \n \n \n \n \n \n \n \n \n\f谨请参阅尾页重要声明及财通证券股票和行业评级标准                                                     2 \n证券研究报告 \n事件点评 \n公 司财务报表及指标预测 \n利润表 \n2 0 1 8A \n2 0 1 9A \n2 0 2 0E \n2 0 2 1E \n2 0 2 2E  财务指标 \n2 0 1 8A \n2 0 1 9A \n2 0 2 0E \n2 0 2 1E \n2 0 2 2E \n营业收入 \n9 4 2 \n1 , 1 97 \n1 , 5 56 \n2 , 1 78 \n2 , 8 31  成长性 \n减:营业成本 \n570 \n685 \n879 \n1,220 \n1,557  营业收入增长率 \n53.1% \n27.0% \n30.0% \n40.0% \n30.0% \n 营业税费 \n7 \n6 \n12 \n16 \n20  营业利润增长率 \n260.1% \n40.8% \n45.6% \n38.1% \n34.2% \n   销售费用 \n42 \n57 \n76 \n107 \n139  净利润增长率 \n335.7% \n39.4% \n44.2% \n37.9% \n34.1% \n   管理费用 \n23 \n33 \n47 \n65 \n85  \nEBITDA 增长率 \n188.8% \n37.7% \n38.6% \n42.3% \n35.6% \n   财务费用 \n1 \n1 \n-1 \n-1 \n-1  \nEBIT 增长率 \n191.2% \n38.0% \n29.1% \n38.2% \n34.3% \n  资产减值损失 \n-4 \n-0 \n21 \n22 \n26  \nNOPLAT 增长率 \n330.0% \n40.7% \n43.6% \n38.2% \n34.3% \n加 :公允价值变动收益 \n- \n- \n20 \n- \n-  投资资本增长率 \n69.7% \n48.9% \n190.3% \n51.6% \n43.5% \n 投资和汇兑收益 \n2 \n6 \n5 \n6 \n7  净资产增长率 \n71.5% \n329.8% \n16.3% \n20.0% \n21.2% \n营业利润 \n2 6 6 \n3 7 5 \n5 4 6 \n7 5 5 \n1 , 0 13  利润率 \n加:营业外净收支 \n4 \n2 \n3 \n3 \n3  毛利率 \n39.4% \n42.8% \n43.5% \n44.0% \n45.0% \n利润总额 \n2 7 1 \n3 7 7 \n5 4 9 \n7 5 8 \n1 , 0 16  营业利润率 \n28.3% \n31.4% \n35.1% \n34.7% \n35.8% \n减:所得税 \n38 \n54 \n82 \n114 \n152  净利润率 \n24.7% \n27.1% \n30.0% \n29.6% \n30.5% \n净利润 \n2 3 2 \n3 2 4 \n4 6 7 \n6 4 4 \n8 6 4  \nEBITDA/营业收入 \n33.0% \n35.8% \n38.2% \n38.8% \n40.5% \n资产负债表 \n2 0 1 8A \n2 0 1 9A \n2 0 2 0E \n2 0 2 1E \n2 0 2 2E  \nEBIT/营业收入 \n32.5% \n35.3% \n35.0% \n34.6% \n35.7% \n货币资金 \n318 \n790 \n665 \n783 \n938  运营效率 \n交易性金融资产 \n- \n980 \n1,000 \n1,000 \n1,000  固定资产周转天数 \n7 \n7 \n23 \n38 \n42 \n应收帐款 \n149 \n193 \n239 \n366 \n421  流 动 营业资本周转天数 \n27 \n33 \n38 \n33 \n35 \n应收票据 \n42 \n- \n52 \n21 \n74  流动资产周转天数 \n203 \n427 \n508 \n389 \n340 \n预付帐款 \n1 \n2 \n1 \n3 \n2  应收帐款周转天数 \n46 \n51 \n50 \n50 \n50 \n存货 \n118 \n165 \n201 \n307 \n342  存货周转天数 \n43 \n43 \n42 \n42 \n41 \n其他流动资产 \n8 \n76 \n30 \n38 \n48  总资产周转天数 \n220 \n448 \n569 \n490 \n458 \n可供出售金融资产 \n- \n- \n- \n- \n-  投资资本周转天数 \n43 \n53 \n95 \n127 \n143 \n持有至到期投资 \n- \n- \n- \n- \n-  投资回报率 \n长期股权投资 \n- \n- \n74 \n156 \n252  \nROE \n50.4% \n16.3% \n20.2% \n23.3% \n25.7% \n投资性房地产 \n0 \n- \n30 \n60 \n90  \nROA \n33.6% \n14.2% \n17.7% \n19.5% \n22.1% \n固定资产 \n20 \n25 \n172 \n289 \n373  \nROIC \n276.7% \n229.3% \n221.1% \n105.3% \n93.2% \n在建工程 \n0 \n19 \n74 \n141 \n224  费用率 \n无形资产 \n34 \n34 \n167 \n286 \n391  销售费用率 \n4.4% \n4.7% \n4.9% \n4.9% \n4.9% \n其他非流动资产 \n2 \n3 \n-71 \n-153 \n-248  管理费用率 \n2.4% \n2.8% \n3.0% \n3.0% \n3.0% \n资产总额 \n6 9 2 \n2 , 2 87 \n2 , 6 34 \n3 , 2 96 \n3 , 9 06  财务费用率 \n0.1% \n0.1% \n-0.1% \n-0.1% \n0.0% \n短期债务 \n- \n- \n- \n- \n-  三费/营业收入 \n6.9% \n7.6% \n7.8% \n7.8% \n7.9% \n应付帐款 \n179 \n243 \n260 \n438 \n453  偿债能力 \n应付票据 \n- \n- \n- \n- \n-  资产负债率 \n33.4% \n13.3% \n12.4% \n16.0% \n14.1% \n其他流动负债 \n52 \n60 \n68 \n89 \n97  负债权益比 \n50.1% \n15.3% \n14.2% \n19.0% \n16.4% \n长期借款 \n- \n- \n- \n- \n-  流动比率 \n2.75 \n7.26 \n6.68 \n4.77 \n5.14 \n其他非流动负债 \n- \n- \n- \n- \n-  速动比率 \n2.24 \n6.72 \n6.07 \n4.19 \n4.52 \n负债总额 \n2 3 1 \n3 0 4 \n3 2 8 \n5 2 7 \n5 5 0  利息保障倍数 \n496.69 \n510.66 \n-456.18 \n-633.66 \n-715.69 \n少数股东权益 \n- \n- \n- \n- \n-  分红指标 \n股本 \n90 \n120 \n120 \n120 \n120  \nDPS(元) \n0.33 \n1.00 \n1.19 \n1.52 \n2.30 \n留存收益 \n371 \n1,863 \n2,187 \n2,649 \n3,236  分红比率 \n17.2% \n37.1% \n30.6% \n28.3% \n32.0% \n股东权益 \n4 6 1 \n1 , 9 83 \n2 , 3 07 \n2 , 7 69 \n3 , 3 56  股息收益率 \n0.2% \n0.6% \n0.8% \n1.0% \n1.5% \n现金流量表 \n2 0 1 8A \n2 0 1 9A \n2 0 2 0E \n2 0 2 1E \n2 0 2 2E  业绩和估值指标 \n2 0 1 8A \n2 0 1 9A \n2 0 2 0E \n2 0 2 1E \n2 0 2 2E \n净利润 \n232 \n324 \n467 \n644 \n864  \nEPS(元) \n1.94 \n2.70 \n3.89 \n5.37 \n7.20 \n加:折旧和摊销 \n5 \n6 \n49 \n92 \n135  \nBVPS(元) \n3.84 \n16.52 \n19.22 \n23.07 \n27.97 \n  资产减值准备 \n4 \n0 \n21 \n22 \n26  \nPE(X) \n81.1 \n58.2 \n40.3 \n29.3 \n21.8 \n公允价值变动损失 \n- \n- \n20 \n- \n-  \nPB(X) \n40.8 \n9.5 \n8.2 \n6.8 \n5.6 \n   财务费用 \n0 \n-3 \n-1 \n-1 \n-1  \nP/FCF \n107.9 \n73.9 \n276.9 \n57.1 \n40.7 \n   投资收益 \n-2 \n-6 \n-5 \n-6 \n-7  \nP/S \n20.0 \n15.7 \n12.1 \n8.7 \n6.7 \n 少数股东损益 \n- \n- \n- \n- \n-  \nEV/EBITDA \n44.4 \n39.8 \n28.8 \n20.1 \n14.7 \n 营运资金的变动 \n-22 \n-124 \n-64 \n-11 \n-130  \nCAGR(%) \n40.5% \n38.7% \n106.1% \n40.5% \n38.7% \n经 营 活动产生现金流量 \n投 资 活动产生现金流量 \n融 资 活动产生现金流量 \n2 1 9 \n- 3 5 \n- 4 7 \n2 6 9 \n- 1 , 000 \n1 , 2 01 \n4 8 7 \n- 4 7 0 \n- 1 4 2 \n7 4 0 \n- 4 4 1 \n- 1 8 1 \n8 8 7  \n- 4 5 6  \n- 2 7 5  \nPEG \nROIC/WACC \nREP \n2.0 \n26.3 \n3.7 \n1.5 \n21.8 \n3.7 \n0.4 \n21.1 \n1.3 \n0.7 \n10.0 \n1.8 \n0.6 \n8.9 \n1.4 \n资料来源：贝格数据，财通证券研究所 \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n  \n \n \n \n \n \n \n \n\f事件点评 \n证券研究报告 \n信息披露 \n分析师承诺 \n作者具有中国证券业协会授予的证券投资咨询执业资格，并注册为证券分析师，具备专业胜任能力，保证报告所采用的\n数据均来自合规渠道，分析逻辑基于作者的职业理解。本报告清晰地反映了作者的研究观点，力求独立、客观和公正，\n结论不受任何第三方的授意或影响，作者也不会因本报告中的具体推荐意见或观点而直接或间接收到任何形式的补偿。 \n资质声明 \n财通证券股份有限公司具备中国证券监督管理委员会许可的证券投资咨询业务资格。 \n公司评级 \n买入：我们预计未来 6 个月内，个股相对大盘涨幅在 15%以上； \n增持：我们预计未来 6 个月内，个股相对大盘涨幅介于 5%与 15%之间； \n中性：我们预计未来 6 个月内，个股相对大盘涨幅介于-5%与 5%之间； \n减持：我们预计未来 6 个月内，个股相对大盘涨幅介于-5%与-15%之间； \n卖出：我们预计未来 6 个月内，个股相对大盘涨幅低于-15%。 \n行业评级 \n增持：我们预计未来 6 个月内，行业整体回报高于市场整体水平 5%以上； \n中性：我们预计未来 6 个月内，行业整体回报介于市场整体水平-5%与 5%之间； \n减持：我们预计未来 6 个月内，行业整体回报低于市场整体水平-5%以下。 \n免责声明 \n本报告仅供财通证券股份有限公司的客户使用。本公司不会因接收人收到本报告而视其为本公司的当然客户。 \n本报告的信息来源于已公开的资料，本公司不保证该等信息的准确性、完整性。本报告所载的资料、工具、意见及推测\n只提供给客户作参考之用，并非作为或被视为出售或购买证券或其他投资标的邀请或向他人作出邀请。 \n本报告所载的资料、意见及推测仅反映本公司于发布本报告当日的判断，本报告所指的证券或投资标的价格、价值及投\n资收入可能会波动。在不同时期，本公司可发出与本报告所载资料、意见及推测不一致的报告。 \n本公司通过信息隔离墙对可能存在利益冲突的业务部门或关联机构之间的信息流动进行控制。因此，客户应注意，在法\n律许可的情况下，本公司及其所属关联机构可能会持有报告中提到的公司所发行的证券或期权并进行证券或期权交易，\n也可能为这些公司提供或者争取提供投资银行、财务顾问或者金融产品等相关服务。在法律许可的情况下，本公司的员\n工可能担任本报告所提到的公司的董事。 \n本报告中所指的投资及服务可能不适合个别客户，不构成客户私人咨询建议。在任何情况下，本报告中的信息或所表述\n的意见均不构成对任何人的投资建议。在任何情况下，本公司不对任何人使用本报告中的任何内容所引致的任何损失负\n任何责任。 \n本报告仅作为客户作出投资决策和公司投资顾问为客户提供投资建议的参考。客户应当独立作出投资决策，而基于本报\n告作出任何投资决定或就本报告要求任何解释前应咨询所在证券机构投资顾问和服务人员的意见； \n本报告的版权归本公司所有，未经书面许可，任何机构和个人不得以任何形式翻版、复制、发表或引用，或再次分发给\n任何其他人，或以任何侵犯本公司版权的其他方式使用。 \n谨请参阅尾页重要声明及财通证券股票和行业评级标准                                                     3 \n \n \n \n  \n\f"
	a := strings.Replace(str, "\n", "", -1)
	s := SubKeyWordBeforeBehind100(a, "电气")
	t.Log(s)
}

func TestSubKeyWordBeforeBehind100Second(t *testing.T) {
	a := SubKeyWordBeforeBehind100Second("本报告仅供财电气通证券股份有限公司的客"+
		"户使用。\" +\n\t\t\"本公司不会因接收人收到本报告而视其为本公司的当然客"+
		"户。 \\n本报告的信息\" +\n\t\t\"来源于已公开的资料，本公司不保证该等信"+
		"息的准确性、完整性。本报告所载的资\" +\n\t\t\"料、工具、意见及推测\\n"+
		"只提供给客户作参考之用，并非作为或被视为出售或购买\" +\n\t\t\"证券或其"+
		"他投资标的邀请或向他人作出邀请。 \\n本报告所载的资料、意见及推测仅反\" "+
		"+\n\t\t\"映本公司于发布本报告当日的判断，本报告所指的证券或投资标的价"+
		"格、价值及投\\n\" +\n\t\t\"资收入可能会波动。在不同时期，本公司可发出"+
		"与本报告所载资料、意见及推测不一致\" +\n\t\t\"的报告。 \\n本公司通过信"+
		"息隔离墙对可能存在利益冲突的业务部门或关联机构之间\" +\n\t\t\"的信息流动进"+
		"行控制。因此，客户应注意，在法\\n律许可的情况下，本公司及其所属\" +\n\t\t"+
		"\"关联机构可能会持有报告中提到的公司所发行的证券或期权并进行证券或期权交易，"+
		"\\n\" +\n\t\t\"也可能为这些公司提供或者争取提供投资银行、财务"+
		"顾问或者金融电气产品等相关服务。在\" +\n\t\t\"法律许可的情况下，本"+
		"公司的员\\n工可能担任本报告所提到的公司的董事。", "电气")
	t.Log(a)
}
func TestSubString(t *testing.T) {
	t.Log(SubString("你好我是吴彦祖我长得非常的帅", 2, 3))
}

func TestStrToMicTime(t *testing.T) {
	//日期转时间戳
	a := StrToMicTime("2020-06-24 00:00:00")
	fmt.Println(a)

	now := GenSecondTime()
	fmt.Println(now)
	aa := GenDateFormat2()
	fmt.Println(aa)
}

func TestConvertAssign(t *testing.T) {
	var dst1 float64
	var dst2 int
	var dst3 float32

	src := "123"
	//字符串转float64
	if err := ConvertAssign(&dst1, src); err != nil {
		log.Fatalf("convert failed, %v", err)
	} else {
		log.Printf("convert ok: %f,类型：%T", dst1, dst1)
	}
	//字符串转float32
	if err := ConvertAssign(&dst3, src); err != nil {
		log.Fatalf("convert failed, %v", err)
	} else {
		log.Printf("convert ok: %f,类型:%T", dst3, dst3)
	}
	//字符串转int
	if err := ConvertAssign(&dst2, src); err != nil {
		log.Fatalf("convert failed, %v", err)
	} else {
		log.Printf("convert ok: %d,类型:%T", dst2, dst2)
	}
	//好多都可以转

}

func toFloat64Addr(in float64) *float64 {
	return &in
}

func toStringAddr(in string) *string {
	return &in
}

//func TestStruct2RoundedMapByTag(t *testing.T) {
//	data := GetResearchReportMysql{
//		Id:                       10,
//		Code:                     "000002",
//		Name:                     "中芯国际",
//		Organization:             "abc",
//		Author:                   "def",
//		Level:                    "持有",
//		LevelChange:              "上升",
//		LevelStatus:              "dfa",
//		CollectedYesterdayPrice:  toFloat64Addr(12212.25532),
//		TargetPrice:              toStringAddr("0.21211"),
//		TargetPriceSorted:        toFloat64Addr(0.23232),
//		RisingSpace:              toStringAddr("23039209302"),
//		RisingSpaceSorted:        toFloat64Addr(1251259182058),
//		Title:                    "",
//		ExpectedNetProfit:        nil,
//		ExpectedNetProfitSorted:  nil,
//		ForecastEps:              nil,
//		ForecastEpsSorted:        nil,
//		PriceEarningRatio:        nil,
//		PriceEarningRatioSorted:  nil,
//		Heat:                     0,
//		CompoundGrowthRate:       nil,
//		CompoundGrowthRateSorted: nil,
//		Content:                  "",
//		ContentArray:             []string{"a", "bbb", "cccc"},
//		RecommendUserId:          "",
//		RecommendUserIdArray:     []int{1, 2, 3, 4, 5},
//		SourceLink:               "",
//		ImportantSecurities:      false,
//		ImportantAuthor:          false,
//		Recommend:                0,
//	}
//	ret := Struct2RoundedMapByTag(data)
//	t.Log(ret)
//	//for k, v := range ret {
//	//	t.Log(k, v)
//	//}
//}

func TestStructDuplication(t *testing.T) {
	a := make(map[string]interface{}, 0)
	c := make(map[string]interface{}, 0)
	d := make(map[string]interface{}, 0)

	b := make([]map[string]interface{}, 0)

	a["code"] = "00001"
	a["level"] = "买入"
	a["target_price"] = "1.01"
	a["title"] = "我是吴彦祖"
	a["content"] = "我是1"
	b = append(b, a)
	//fmt.Println(b)
	//return
	//
	c["code"] = "00001"
	c["level"] = "买入"
	c["target_price"] = "1.01"
	c["title"] = "我是吴彦祖"
	c["content"] = "我是2"

	b = append(b, c)
	////fmt.Println(b)
	//
	d["code"] = "00001"
	d["level"] = "买入"
	d["target_price"] = "1.02"
	d["title"] = "我是吴彦祖"
	d["content"] = "我是3"
	b = append(b, d)
	//b[2] = d
	fmt.Println("去重前:", b)

	e := RemoveDuplicateElementMap(b)
	fmt.Println("去重后:", e)

}

func TestGetLastOneWeekTime(t *testing.T) {
	res := GetLastOneWeekTime("2020-07-27")
	t.Log(res)
}
func TestGetLastTwoWeekTime(t *testing.T) {
	res := GetLastTwoWeekTime("2020-07-27")
	t.Log(res)
}
func TestGenBeginEndTimeTwo(t *testing.T) {
	resBegin, resEnd := GenBeginEndTimeTwo("2020-07-27")
	t.Log(resBegin, resEnd)
}

func TestInIntSlice(t *testing.T) {
	a := 1
	b := []int{1, 2, 3, 4}
	t.Log(InIntSlice(a, b))
}

func TestRemove(t *testing.T) {
	RemoveSlice()
}

func TestIncrBy(t *testing.T) {

	err := gredis.Set("ipo:"+strconv.Itoa(1), 0, 60*60*24)
	if err != nil {
		t.Error(err)
	}

}

func TestEqual(t *testing.T) {
	var a int64 = 7
	var b int = 7
	if a == int64(b) {
		t.Log(true)
	} else {
		t.Log(false)
	}
}

func TestRedis(t *testing.T) {
	conn := gredis.Clone(14)
	conn.PushList("test", "a")
}
