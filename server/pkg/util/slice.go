package util

import (
	"fmt"
	"math"
	"reflect"
	"time"
)

func InIntSlice(d int, s []int) bool {
	for _, v := range s {
		if v == d {
			return true
		}
	}
	return false
}

// 删除切片中的元素
func Remove(slice []interface{}, elem interface{}) []interface{} {
	if len(slice) == 0 {
		return slice
	}
	for i, v := range slice {
		if v == elem {
			slice = append(slice[:i], slice[i+1:]...)
			return Remove(slice, elem)
			//break
		}
	}
	return slice
}

func RemoveZero(slice []interface{}) []interface{} {
	if len(slice) == 0 {
		return slice
	}
	for i, v := range slice {
		if IfZero(v) {
			slice = append(slice[:i], slice[i+1:]...)
			return RemoveZero(slice)
			//break
		}
	}
	return slice
}

func IfZero(arg interface{}) bool {
	if arg == nil {
		return true
	}
	switch v := arg.(type) {
	case int, int32, int16, int64:
		if v == 0 {
			return true
		}
	case float32:
		r := float64(v)
		return math.Abs(r-0) < 0.0000001
	case float64:
		return math.Abs(v-0) < 0.0000001
	case string:
		if v == "" || v == "%%" || v == "%" {
			return true
		}
	case *string, *int, *int64, *int32, *int16, *int8, *float32, *float64, *time.Time:
		if v == nil {
			return true
		}
	case time.Time:
		return v.IsZero()
	default:
		return false
	}
	return false
}

type A struct {
	Name string `json:"name"`
}

func RemoveSlice() {
	//a := make([]A, 0)
	b := make([]A, 0)
	d := make([]A, 0)
	x := A{
		Name: "测试1",
	}
	y := A{
		Name: "测试2",
	}
	z := A{
		Name: "测试3",
	}
	b = append(b, x)
	b = append(b, y)
	b = append(b, z)

	d = append(b[2:3])
	fmt.Println(d)

}

func Reverse(slice interface{}) {
	s := reflect.ValueOf(slice)
	// if s is a pointer of slice
	if s.Kind() == reflect.Ptr {
		s = s.Elem()
	}
	swp := reflect.Swapper(s.Interface())
	for i, j := 0, s.Len()-1; i < j; i, j = i+1, j-1 {
		swp(i, j)
	}
}
