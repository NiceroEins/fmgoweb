package util

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/shopspring/decimal"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
	"io/ioutil"
	"math"
	"net/http"
	"reflect"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
	"unicode"
)

var locker sync.Mutex

//字符串转int
func Atoi(s string) int {
	n, err := strconv.Atoi(s)
	if err != nil {
		return 0
	}
	return n
}

//字符串转float64
func Atof(s string) float64 {
	n, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0
	}
	return n
}

//float64转字符串
func Ftoa(f float64) (s string) {
	s = fmt.Sprintf("%.2f", f)
	return
}
func Atof1(s string) float64 {
	n, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0
	}
	return n
}

//保留4位小数
func Decimal(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.4f", value), 64)
	return value
}

//保留3位小数
func DecimalThree(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.3f", value), 64)
	return value
}

//字符串小数转百分数
func AtoP(s string) (p string) {
	middleFloat, err := strconv.ParseFloat(s, 64)
	if err != nil {
		middleFloat = 0
	}
	p = strconv.FormatFloat(middleFloat*100, 'f', 2, 64) + "%"
	return
}

//字符串 元 转 亿元,保留三位小数
func AtoB(s string) (b string) {
	if s != "" {
		middleFloat, err := strconv.ParseFloat(s, 64)
		if err != nil {
			middleFloat = 0
			fmt.Println("转float失败啦:", err.Error())
		}
		b = fmt.Sprintf("%.3f", middleFloat/100000000)
		return
	} else {
		middleFloat, err := strconv.ParseFloat("0", 64)
		if err != nil {
			middleFloat = 0
			fmt.Println("转float失败啦:", err.Error())
		}
		b = fmt.Sprintf("%.3f", middleFloat/100000000)
		return
	}
}

//float64 元转亿元保留3为小数
//func AtoB2(s float64) (b float64) {
//
//	bMiddle := fmt.Sprintf("%.3f", s/100000000)
//	b, _ = strconv.ParseFloat(strings.TrimSpace(bMiddle), 64)
//	return b
//
//}

//func AtoB2(f float64) float64 {
//	m := 3
//	n := strconv.FormatFloat(f, 'f', -1, 32)
//	if n == "" {
//		return 0
//	}
//	//if m >= len(n) {
//	//	return 0
//	//}
//	newn := strings.Split(n, ".")
//	if len(newn) < 2 || m >= len(newn[1]) {
//		return 0
//	}
//
//	res := newn[0] + "." + newn[1][:m]
//	//string转float
//	resDeal, _ := strconv.ParseFloat(res, 64)
//	return resDeal
//}

func ChangeNumber(f float64, m int) string {
	n := strconv.FormatFloat(f, 'f', -1, 32)
	if n == "" {
		return ""
	}
	if m >= len(n) {
		return n
	}
	newn := strings.Split(n, ".")
	if len(newn) < 2 || m >= len(newn[1]) {
		return n
	}
	return newn[0] + "." + newn[1][:m]
}

func Str2map(s string) map[string]interface{} {
	m := map[string]interface{}{}
	err := json.Unmarshal([]byte(s), &m)
	if err != nil {
		// Println("Str2map() ", err)
		return nil
	}
	return m
}

func Map2str(m map[string]interface{}) string {
	buf, err := json.Marshal(m)
	if err != nil {
		// Println("Map2str() ", err)
		return ""
	}
	return string(buf)
}

func Map2slice(input map[string]interface{}) []string {
	output := make([]string, 0)
	for _, value := range input {
		output = append(output, value.(string))
	}
	return output
}

func Mem2str(m *map[string]interface{}) string {
	return Map2str(*m)
}

func Array2str(a []map[string]interface{}) string {
	buf, err := json.Marshal(a)
	if err != nil {
		//Println("Map2str() ", err)
		return ""
	}
	return string(buf)
}

func Str2array(s string) []interface{} {
	a := make([]interface{}, 0)
	err := json.Unmarshal([]byte(s), &a)
	if err != nil {
		//Println("Str2map() ", err)
		return nil
	}
	return a
}

func StrMap2InterfaceMap(m map[string]string) map[string]interface{} {
	res := make(map[string]interface{})
	if m == nil || len(m) == 0 {
		return res
	}

	for k, v := range m {
		res[k] = v
	}
	return res
}

func InterfaceMap2StrMap(m map[string]interface{}) map[string]string {
	res := make(map[string]string)
	if m == nil || len(m) == 0 {
		return res
	}

	for k, v := range m {
		res[k] = fmt.Sprintf("%v", v)
	}
	return res
}

func GetStringNoLog(msg map[string]interface{}, k string) (bool, string) {
	locker.Lock()
	defer locker.Unlock()
	if msg == nil {
		return false, ""
	}
	val, ok := msg[k]
	if !ok {
		//Println("Getstring() unexist key: ", k)
		return false, ""
	}
	str, ok := val.(string)
	if !ok {
		//Printf("Getstring() value of \"%s\" is not a string!\n", k)
		return false, ""
	}
	return true, str
}

func GetString(msg map[string]interface{}, k string) (string, bool) {
	b, s := GetStringNoLog(msg, k)
	return s, b
}

func Getstring(msg map[string]interface{}, k string) (bool, string) {
	locker.Lock()
	defer locker.Unlock()
	if msg == nil {
		return false, ""
	}
	val, ok := msg[k]
	if !ok {
		//Println("Getstring() unexist key: ", k)
		return false, ""
	}
	str, ok := val.(string)
	if !ok {
		//Printf("Getstring() value of \"%s\" is not a string!\n", k)
		return false, ""
	}
	return true, str
}

func GetBool(msg map[string]interface{}, k string) (bool, bool) {
	locker.Lock()
	defer locker.Unlock()
	if msg == nil {
		return false, false
	}
	val, ok := msg[k]
	if !ok {
		//Println("Getstring() unexist key: ", k)
		return false, false
	}
	b, ok := val.(bool)
	if !ok {
		//Printf("Getstring() value of \"%s\" is not a boolean!\n", k)
		return false, false
	}
	return true, b
}

func GetBoolNoLog(msg map[string]interface{}, k string) (bool, bool) {
	locker.Lock()
	defer locker.Unlock()
	if msg == nil {
		return false, false
	}
	val, ok := msg[k]
	if !ok {
		//Println("Getstring() unexist key: ", k)
		return false, false
	}
	b, ok := val.(bool)
	if !ok {
		//Printf("Getstring() value of \"%s\" is not a boolean!\n", k)
		return false, false
	}
	return true, b
}

func GetNumber(msg map[string]interface{}, k string) (bool, float64) {
	locker.Lock()
	defer locker.Unlock()
	val, ok := msg[k]
	if !ok {
		//Println("Getnumber() unexist key: ", k)
		return false, 0
	}
	switch val.(type) {
	case int:
		return true, float64(val.(int))
	case int64:
		return true, float64(val.(int64))
	case float64:
		return true, val.(float64)
	case string:
		str, ok := val.(string)
		if ok {
			f, err := strconv.ParseFloat(str, 64)
			return err == nil, f
		}
	}
	//Printf("Getnumber() value of \"%s\" is not a number!\n", k)
	return false, 0
}

func GetIntNoLog(msg map[string]interface{}, k string) (bool, int) {
	ok, ret := GetNumber(msg, k)
	if ok {
		return ok, int(ret)
	} else {
		return false, 0
	}
}

func GetInt(msg map[string]interface{}, k string) (int, bool) {
	b, i := GetIntNoLog(msg, k)
	return i, b
}

func GetMap(msg map[string]interface{}, k string) (bool, map[string]interface{}) {
	locker.Lock()
	defer locker.Unlock()
	val, ok := msg[k]
	if !ok {
		//Println("Getmap() unexist key: ", k)
		return false, nil
	}
	tbl, ok := val.(map[string]interface{})
	if !ok {
		//Printf("Getmap() value of \"%s\" is not a map!\n", k)
		return false, nil
	}
	return true, tbl
}

func GetArray(msg map[string]interface{}, k string) (bool, []interface{}) {
	locker.Lock()
	defer locker.Unlock()
	val, ok := msg[k]
	if !ok {
		//Println("Getarray() unexist key: ", k)
		return false, nil
	}
	arr, ok := val.([]interface{})
	if !ok {
		//Printf("Getarray() value of \"%s\" is not array!\n", k)
		return false, nil
	}
	return true, arr
}

func GetMapArray(msg map[string]interface{}, k string) (bool, []map[string]interface{}) {
	locker.Lock()
	defer locker.Unlock()
	val, ok := msg[k]
	if !ok {
		//Println("Getmaparray() unexist key: ", k)
		return false, nil
	}
	arr, ok := val.([]map[string]interface{})
	if !ok {
		//Printf("Getmaparray() value of \"%s\" is not array!\n", k)
		return false, nil
	}
	return true, arr
}

func Append(m map[string]interface{}, n map[string]interface{}) map[string]interface{} {
	for k, v := range n {
		m[k] = v
	}
	return m
}

func Restrict(num, a, b float64) float64 {
	if num < a {
		return a
	}
	if num > b {
		return b
	}
	return num
}

func Sign(num float64) float64 {
	if num > 0 {
		return 1
	} else if num < 0 {
		return -1
	}
	return 0
}

func CompareInt(x, y int) int {
	switch {
	case x < y:
		return -1
	case x > y:
		return 1
	}
	return 0
}

func CompareInts(x, y []int) int {
	xnum := len(x)
	ynum := len(y)
	sort.Ints(x)
	sort.Ints(y)
	var l, r int
	switch {
	case xnum > ynum:
		l, r = ynum, 1
	case xnum < ynum:
		l, r = xnum, -1
	case xnum == ynum:
		l, r = xnum, 0
	}
	for i := 1; i <= l; i++ {
		j := l - i
		switch {
		case x[j] < y[j]:
			return -1
		case x[j] > y[j]:
			return 1
		}
	}
	return r
}

func Combination(n, m int) [][]int {
	comb := [][]int{}
	switch {
	case n < m:
		return comb
	case n == m || m == 0:
		comb = append(comb, array(m))
		return comb
	}
	comb = Combination(n-1, m)
	arr := Combination(n-1, m-1)
	for _, c := range arr {
		c = append(c, n)
		comb = append(comb, c)
	}
	return comb
}

func array(n int) []int {
	arr := []int{}
	for i := 1; i <= n; i++ {
		arr = append(arr, i)
	}
	return arr
}

//convert datatime from
//2001-2-3 4:5:6
//to 2001-02-03 04:05:06
func ConvDataStr(str string) string {
	_, err := time.Parse("2006-01-02", str)
	if err == nil {
		return str
	}

	res := ""
	r := []string{}
	t := strings.Split(str, " ")
	ls := []string{"-", ":"}

	for i, v := range t {
		s := strings.Split(v, ls[i])
		for j, v := range s {
			if len([]rune(v)) == 1 {
				s[j] = "0" + s[j]
			}
		}
		r = append(r, strings.Join(s, ls[i]))
	}

	res = strings.Join(r, " ")

	return res
}

//emoji表情解码
func UnicodeEscapeDecode(s string) string {
	//Println("In string: ", s)
	//emoji表情的数据表达式
	re := regexp.MustCompile("\\[[\\\\u0-9a-zA-Z]+]")
	//提取emoji数据表达式
	reg := regexp.MustCompile("\\[u|]")
	src := re.FindAllString(s, -1)
	for i := 0; i < len(src); i++ {
		e := reg.ReplaceAllString(src[i], "")
		p, err := strconv.ParseInt(e, 16, 32)
		if err == nil {
			s = strings.Replace(s, src[i], string(rune(p)), -1)
		}
	}
	//Println("Out string: ", s)
	return s
}

//emoji表情转换
//and 字符串转义
func UnicodeEscapeEncode(s string) string {
	ret := ""
	rs := []rune(s)
	for i := 0; i < len(rs); i++ {
		if len(string(rs[i])) == 4 {
			u := "[\\u" + strconv.FormatInt(int64(rs[i]), 16) + "]"
			ret += u
		} else {
			c := string(rs[i])
			if c == "\\" || c == "'" {
				c = "\\" + c
			}
			ret += c
			//ret += string(rs[i])
		}
	}
	return ret
}

func CombineHash(s string) string {
	rs := []rune(s)
	res := make([]rune, len(rs))
	j := 0
	for i := 0; i < len(rs); i += 2 {
		c1 := RuneConvert(rs[i])
		c2 := 0
		if i+1 >= len(rs) {
			c2 = 10
		} else {
			c2 = RuneConvert(rs[i+1])
		}
		res[j] = rune(c1*16 + c2)
		j++
	}
	return string(res)
}

func BKDRHash(s string) uint32 {
	var seed uint32 = 131
	var hash uint32 = 0

	rs := []rune(s)
	for i := 0; i < len(rs); i++ {
		hash = hash*seed + uint32(rs[i])
	}
	return hash & 0x7FFFFFFF
}

func RuneConvert(c rune) int {
	if int(c) >= int('0') && int(c) <= int('9') {
		return int(c) - int('0')
	} else if c == '#' {
		return 10
	} else {
		return 0
	}
}

func ImgurlToBase64(url string) string {
	tr := http.Transport{
		TLSClientConfig:   &tls.Config{InsecureSkipVerify: true},
		DisableKeepAlives: false,
	}
	client := http.Client{Transport: &tr}
	resp, err := client.Get(url)
	//resp, err := http.Get(url)
	if err != nil {
		//Println("xx.ImgurlToBase64() http.Get() errors: ", err)
		return ""
	}
	img, err := ioutil.ReadAll(resp.Body)
	buf := make([]byte, 64000)          //数据缓存
	base64.StdEncoding.Encode(buf, img) //byte转base64
	return string(buf)
}

// Momo 181225
// 基于反射的deepcopy
type Interface interface {
	DeepCopy() interface{}
}

func DeepCopy(value interface{}) interface{} {
	original := reflect.ValueOf(value)
	cpy := reflect.New(original.Type()).Elem()
	// Recursively copy the original
	copyRecursive(original, cpy, 0)

	// Return the copy as an interface
	return cpy.Interface()
}

// Momo 181226
// copy recursive
// set maxDepth = 30
func copyRecursive(original, cpy reflect.Value, depth int) {
	if depth > 30 {
		//Println("xx.copyRecursive() reached maxDepth 30")
		return
	}
	if original.CanInterface() {
		if copier, ok := original.Interface().(Interface); ok {
			cpy.Set(reflect.ValueOf(copier.DeepCopy()))
			return
		}
	}

	switch original.Kind() {
	case reflect.Ptr:
		originalValue := original.Elem()

		if !originalValue.IsValid() {
			return
		}
		cpy.Set(reflect.New(originalValue.Type()))
		copyRecursive(originalValue, cpy.Elem(), depth+1)

	case reflect.Interface:
		if original.IsNil() {
			return
		}
		originalValue := original.Elem()

		copyValue := reflect.New(originalValue.Type()).Elem()
		copyRecursive(originalValue, copyValue, depth+1)
		cpy.Set(copyValue)

	case reflect.Struct:
		t, ok := original.Interface().(time.Time)
		if ok {
			cpy.Set(reflect.ValueOf(t))
			return
		}
		for i := 0; i < original.NumField(); i++ {
			if original.Type().Field(i).PkgPath != "" {
				continue
			}
			copyRecursive(original.Field(i), cpy.Field(i), depth+1)
		}

	case reflect.Slice:
		if original.IsNil() {
			return
		}
		// Make a new slice and copy each element
		cpy.Set(reflect.MakeSlice(original.Type(), original.Len(), original.Cap()))
		for i := 0; i < original.Len(); i++ {
			copyRecursive(original.Index(i), cpy.Index(i), depth+1)
		}

	case reflect.Map:
		if original.IsNil() {
			return
		}
		cpy.Set(reflect.MakeMap(original.Type()))
		for _, key := range original.MapKeys() {
			originalValue := original.MapIndex(key)
			copyValue := reflect.New(originalValue.Type()).Elem()
			copyRecursive(originalValue, copyValue, depth+1)
			copyKey := DeepCopy(key.Interface())
			cpy.SetMapIndex(reflect.ValueOf(copyKey), copyValue)
		}

	default:
		cpy.Set(original)
	}
}

func ToFloat64(value interface{}) float64 {
	switch value.(type) {
	case int:
		return float64(value.(int))
	case int64:
		return float64(value.(int64))
	case float64:
		return value.(float64)
	case string:
		str, ok := value.(string)
		if ok {
			f, err := strconv.ParseFloat(str, 64)
			if err == nil {
				return f
			}
		}
	}
	return 0
}

func ToInt64(value interface{}) int64 {
	switch value.(type) {
	case int:
		return int64(value.(int))
	case int64:
		return value.(int64)
	case float64:
		return int64(value.(float64))
	case int32:
		return int64(value.(int32))
	case float32:
		return int64(value.(float32))
	case string:
		str, ok := value.(string)
		if ok {
			f, err := strconv.ParseInt(str, 10, 64)
			if err == nil {
				return f
			}
		}
	}
	return 0
}

func Max(value1, value2 interface{}) int {
	v1 := ToFloat64(value1)
	v2 := ToFloat64(value2)
	if v1 >= v2 {
		return int(v1)
	}
	return int(v2)
}

func Min(value1, value2 interface{}) int {
	v1 := ToFloat64(value1)
	v2 := ToFloat64(value2)
	if v1 <= v2 {
		return int(v1)
	}
	return int(v2)
}

func Abs(value interface{}) int {
	v := ToFloat64(value)
	return int(math.Abs(v))
}

func Infinity() int64 {
	return 0xFFFFFFFF
}

func InfiniteDuration() time.Duration {
	return time.Duration(Infinity()) * time.Second
}

func StrIn(src string, arr []string) bool {
	for _, v := range arr {
		if src == v {
			return true
		}
	}
	return false
}

func S2Id(id_s string) (int64, error) {
	if len(id_s) <= 0 {
		return 0, errors.New("id不能为空")
	}
	id, err := strconv.ParseInt(id_s, 10, 64)
	if err != nil {
		return 0, err
	}
	if id <= 0 {
		return 0, errors.New("id不能小于0")
	}
	return id, nil
}

func S2N(id_s string) (int64, error) {
	if len(id_s) <= 0 {
		return 0, nil
	}
	num, err := strconv.ParseInt(id_s, 10, 64)
	if err != nil {
		return 0, err
	}
	return num, nil
}

func S2F64(id_s string) (float64, error) {
	if len(id_s) <= 0 {
		return 0, nil
	}
	num, err := strconv.ParseFloat(id_s, 64)
	if err != nil {
		return 0, err
	}
	return num, nil
}

func S2I32(id_s string) (int32, error) {
	if len(id_s) <= 0 {
		return 0, nil
	}
	num, err := strconv.ParseInt(id_s, 10, 64)
	if err != nil {
		return 0, err
	}
	return int32(num), nil
}

func Struct2Map(obj interface{}) map[string]interface{} {
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)

	var data = make(map[string]interface{})
	for i := 0; i < t.NumField(); i++ {
		data[t.Field(i).Name] = v.Field(i).Interface()
	}
	return data
}

func OddsCompute(o1 float64, o2 float64, float_bit int) bool {
	return round(o1, float_bit) == round(o2, float_bit)
}

//计算结果取小数的n位
func round(f float64, n int) float64 {
	n10 := math.Pow10(n)
	return math.Trunc((f+0.5/n10)*n10) / n10
}

//func Clear(v interface{}) {
//	p := reflect.ValueOf(v).Elem()
//	p.Set(reflect.Zero(p.Type()))
//}
//[]bytes转int
func BytesToInt(bys []byte) int {
	bytebuff := bytes.NewBuffer(bys)
	var data int64
	binary.Read(bytebuff, binary.BigEndian, &data)
	return int(data)
}

//float32 转 string,保留6为小数
func FloatToString(input_num float32) string {
	// to convert a float number to a string
	return strconv.FormatFloat(float64(input_num), 'f', 6, 64)
}

//uint 转strgin
func UintToString(a uint8) string {
	return strconv.Itoa(int(a))
}
func ReplaceAll(raw string, rules map[string]string) string {
	ret := raw
	for k, v := range rules {
		ret = strings.Replace(ret, k, v, -1)
	}
	return ret
}

func SplitAndClear(raw string, sep string) []string {
	s := strings.Split(raw, sep)
	return Clear(s)
}

func Clear(raw []string) []string {
	ret := make([]string, 0)
	for i := 0; i < len(raw); i++ {
		duplicate := false
		for _, v := range ret {
			if v == raw[i] {
				duplicate = true
				break
			}
		}
		if raw[i] != "" && !duplicate {
			ret = append(ret, raw[i])
		}
	}
	return ret
}

func FormatTime(ts string) time.Time {
	layout := []string{
		"2006-01-02 15:04:05",
		"2006/01/02 15:04:05",
		"2006-01-02",
		"2006/01/02",
	}
	for _, v := range layout {
		t, err := time.ParseInLocation(v, ts, time.Local)
		if err == nil {
			return t
		}
	}
	return time.Now()
}

func Struct2RoundedMapByTag(obj interface{}) map[string]interface{} {
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)

	var data = make(map[string]interface{})
	for i := 0; i < t.NumField(); i++ {
		value := v.Field(i)
		key := t.Field(i).Tag.Get("json")
		if key == "" || key == "-" {
			continue
		}
		data[key] = value2Str(value)
	}
	return data
}

func value2Str(value reflect.Value) interface{} {
	if !value.IsValid() {
		return ""
	}

	switch value.Type().Kind() {
	case reflect.String, reflect.Uint, reflect.Uint32, reflect.Uint64, reflect.Int, reflect.Int32, reflect.Int64:
		return fmt.Sprintf("%v", value.Interface())
	case reflect.Float64, reflect.Float32:
		vf := value.Float()
		if vf == float64(int64(vf)) {
			// integer
			return fmt.Sprintf("%d", int64(vf))
		}
		return fmt.Sprintf("%.2f", vf)
	case reflect.Slice:
		return value.Interface()
	case reflect.Ptr:
		if value.IsNil() {
			return nil
		}
		return value2Str(value.Elem())
	default:
		return fmt.Sprintf("%v", value.Interface())
	}
}

func GbkToUtf8(s []byte) ([]byte, error) {
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewDecoder())
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}

func Utf8ToGbk(s []byte) ([]byte, error) {
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewEncoder())
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}

//科学计数法转string
func ConvScientificCount(str string) (string, error) {
	decimalNum, err := decimal.NewFromString(str)
	if err != nil {
		return "", fmt.Errorf("decimal.NewFromString error, numStr:%s, err:%v", str, err)
	}
	return decimalNum.String(), nil

	//a := AAA{Num: 1324654798761265}
	//res,_ := json.Marshal(a)
	//fmt.Println(string(res))
	//v := make(map[string]interface{})
	////str := `{"id":"123456789121543125647987"}`
	//json.Unmarshal([]byte(str),&v)
	//a,_ := json.Marshal(v)
	////d := json.NewDecoder(bytes.NewReader([]byte(str)))
	////d.UseNumber()
	////d.Decode(&v)
	//fmt.Println(string(a))
}

//下划线写法转为大驼峰
func Snake2Camel(name string) string {
	name = strings.Replace(name, "_", " ", -1)
	name = strings.Title(name)
	return strings.Replace(name, " ", "", -1)
}

func IsLetter(str string) bool {
	for _, v := range []rune(str) {
		if !alphaOnly(v) && !unicode.IsDigit(v) {
			return false
		}
	}
	return true
}

const alpha = "abcdefghijklmnopqrstuvwxyz"

func alphaOnly(s rune) bool {
	return strings.Contains(alpha, strings.ToLower(string(s)))
}
