package util

import (
	"regexp"
)

//从字符串中提取数字
func GetNumFromStr(str string) (num1, num2 string, length int) {
	r, _ := regexp.Compile("\\d+(\\.\\d+)?")
	res := r.FindAll([]byte(str), -1)
	length = len(res)
	if length == 0 {
		return "", "", 0
	}
	if length == 1 {
		return string(res[0]), "", length
	}
	if length >= 2 {
		return string(res[0]), string(res[1]), length
	}
	return
}
