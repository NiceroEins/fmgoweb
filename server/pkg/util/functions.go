package util

import (
	"reflect"
	"strings"
)

//判断该字段是否为其类型零值
// true:空字符串,0,0.0,false,空指针,空切片(长度0),空map(长度0),空数组(长度0),
func IsEmpty(src interface{}) bool {
	value := reflect.ValueOf(src)
	switch value.Kind() {
	case reflect.String:
		return value.Len() == 0
	case reflect.Bool:
		return !value.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return value.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return value.Uint() == 0
	case reflect.Float32, reflect.Float64:
		return value.Float() == 0
	case reflect.Ptr:
		return value.IsNil()
	case reflect.Slice, reflect.Array, reflect.Map:
		return value.Len() == 0
	}
	return reflect.DeepEqual(value.Interface(), reflect.Zero(value.Type()).Interface())
}

//rtype=true code  =false name
func GetIndexFromCode(code string, rtype bool) string {
	if strings.HasPrefix(code, "60") {
		if rtype {
			return "SH000001"
		} else {
			return "沪A"
		}
	}
	if strings.HasPrefix(code, "000") {
		if rtype {
			return "SZ399001"
		} else {
			return "深A"
		}
	}
	if strings.HasPrefix(code, "300") {
		if rtype {
			return "SZ399006"
		} else {
			return "创业板"
		}
	}
	if strings.HasPrefix(code, "00") {
		if rtype {
			return "SZ399005"
		} else {
			return "中小板"
		}
	}
	if strings.HasPrefix(code, "688") {
		if rtype {
			return "SH000688"
		} else {
			return "科创板"
		}
	}
	return ""
}
