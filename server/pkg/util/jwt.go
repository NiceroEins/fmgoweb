package util

import (
	"github.com/dgrijalva/jwt-go"
	"strconv"
	"time"
)

var jwtSecret []byte

type Claims struct {
	Userid   string `json:"userid"`
	Username string `json:"username"`
	Pwd      string `json:"pwd"`
	jwt.StandardClaims
}

//生成 jwt token
func GenerateToken(userId int, username, pwd,apiType string) (string, error) {
	var err error
	nowTime := time.Now()
	//过期时间 : 一周
	expireTime := nowTime.Add(24 * 7 * time.Hour)

	username, err = DesEncrypt(username)
	if err != nil {
		return "", err
	}
	pwd, err = DesEncrypt(pwd)
	if err != nil {
		return "", err
	}
	userIdStr, err := DesEncrypt(strconv.Itoa(userId))
	if err != nil {
		return "", err
	}
	claims := Claims{
		userIdStr,
		username,
		pwd,
		jwt.StandardClaims{
			ExpiresAt: expireTime.Unix(),
			Issuer:    "hshs",
		},
	}

	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := tokenClaims.SignedString(jwtSecret)
	if apiType == "front"{
		//前端保存1h
		//gredis.Set("loginfront:"+strconv.Itoa(userId), token, 60*60*24*7)
	}else{
		//7天过期,一个账号只能同时一个人登录
		//gredis.Set("loginlist:"+strconv.Itoa(userId), token, 60*60*24*7)
	}
	return token, err
}

//解析 jwt token
func ParseToken(token string) (*Claims, error) {
	tokenClaims, err := jwt.ParseWithClaims(token, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})

	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}

	return nil, err
}
