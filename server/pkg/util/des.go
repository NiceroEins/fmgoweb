package util

import (
	"bytes"
	"crypto/des"
	"encoding/hex"
	"errors"
)
//des密钥
var key = []byte("shitbaby")
//des加密
func DesEncrypt(text string) (string, error) {
	data := []byte(text)
	blockDes, err := des.NewCipher(key)
	if err != nil {
		return "", err
	}
	bs := blockDes.BlockSize()
	data = ZeroPadding(data, bs)
	if len(data)%bs != 0 {
		return "", errors.New("Need a multiple of the blocksize")
	}
	out := make([]byte, len(data))
	dst := out
	for len(data) > 0 {
		blockDes.Encrypt(dst, data[:bs])
		data = data[bs:]
		dst = dst[bs:]
	}
	return hex.EncodeToString(out), nil
}
//des解密
func DesDecrypt(decrypted string) (string, error) {
	data, err := hex.DecodeString(decrypted)
	if err != nil {
		return "", nil
	}
	blockDes, err := des.NewCipher(key)
	if err != nil {
		return "", err
	}
	out := make([]byte, len(data))
	dst := out
	bs := blockDes.BlockSize()
	if len(data)%bs != 0 {
		return "", errors.New("crypto/cipher: input not full blocks")
	}
	for len(data) > 0 {
		blockDes.Decrypt(dst, data[:bs])
		data = data[bs:]
		dst = dst[bs:]
	}
	out = ZeroUnPadding(out)
	return string(out), nil
}

func ZeroPadding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{0}, padding)
	return append(ciphertext, padtext...)
}
func ZeroUnPadding(origData []byte) []byte {
	return bytes.TrimFunc(origData, func(r rune) bool {
		return r == rune(0)
	})
}