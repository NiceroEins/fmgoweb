package util

import (
	"fmt"
	"strings"
)

func ContainsAny(src []string, sub ...string) bool {
	for _, v := range sub {
		for _, s := range src {
			if s == v {
				return true
			}
		}
	}
	return false
}

//截取字符串,超过中文100个字,后面省略号,不超过话,原样返回
func Sub100ChineseCharacters(str string) string {
	newStrRune := []rune(str)
	if len(newStrRune) >= 100 {
		newStr := string(newStrRune[0:100]) + "..."
		return newStr
	} else {
		return str
	}
}

//截取字符串,超过中文50个字,后面省略号,不超过话,原样返回
func Sub50ChineseCharacters(str string) string {
	newStrRune := []rune(str)
	if len(newStrRune) >= 50 {
		newStr := string(newStrRune[0:50]) + "..."
		return newStr
	} else {
		return str
	}
}

//截取数字第一位
func SubFirstChar(str string) string {
	return str[0:1]
}

func CountDigit(src string) int {
	cnt := 0
	for _, v := range []rune(src) {
		if v <= '9' && v >= '0' {
			cnt++
		}
	}
	return cnt
}

//截取第一个关键词出现的前面100个字,关键词,后面100个字
func SubKeyWordBeforeBehind100(str string, keyWord string) (dstStr string) {
	strIndex := strings.Index(str, keyWord)
	if strIndex < 0 {
		return ""
	}

	var beginIndex int
	var endIndex int

	beginIndex = strIndex - 300 //关键词往前推进100个字
	if beginIndex < 0 {
		beginIndex = 0
	}

	endIndex = strIndex + len(keyWord) + 300
	if endIndex > len(str) {
		endIndex = len(str)
	}
	fmt.Println("begin", beginIndex)
	dstStr = str[beginIndex:endIndex]
	return
}

//截取第二个关键词出现的前面100个字,关键词,后面100个字
func SubKeyWordBeforeBehind100Second(str string, keyWord string) string {
	//去掉第一个关键词,之后在新的str中找到第一个关键词,并取出他的索引,之后在老的str中 进行截取
	keyWordLength := len(keyWord)
	//第一个关键词出现的位置
	newStr := strings.Repeat("*", keyWordLength)
	//fmt.Println(newStr)
	middleStr := strings.Replace(str, keyWord, newStr, 1) //把第一个关键词变为*
	//fmt.Println("替换完结果",middleStr)
	//在新的字符串中，寻找第一个关键词出现的位置
	resStr := SubKeyWordBeforeBehind100(middleStr, keyWord)
	//fmt.Println("返回结果：", resStr)
	return strings.Replace(resStr, newStr, keyWord, -1)
}

// num 截取数量
// length 截取长度
func GetStringBetweenKeyword(str string, keyWord string, num int, length int) []string {
	var strList []string
	var ctx = "<span style=\"color:red;font-weight:bold\">"
	var edx = "</span>"
	start := 0
	for i := 0; i < num; i++ {
		if len(str) > start+len(keyWord) {
			n := strings.Index(str[start:], keyWord)
			if n >= 0 {
				strList = append(strList, strings.ReplaceAll(str[Max(start, start+n-length):Min(start+n+length, len(str))], keyWord, ctx+keyWord+edx))
				start = start + n + len(keyWord)
				//str = str[start+len(keyWord):]
			} else {
				break
			}
		}
	}
	return strList
}

func SubString(str string, begin, length int) (substr string) {
	// 将字符串的转换成[]rune
	rs := []rune(str)
	//lth := len(rs)
	if begin < 0 {
		begin = 0
	}
	//if begin >= lth {
	//	begin = lth
	//}
	end := begin + length
	//if end > lth {
	//	end = lth
	//}
	// 返回子串
	return string(rs[begin:end])
}
