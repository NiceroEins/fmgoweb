package util

import (
	"fmt"
)

//slice int 去重
func RemoveDuplicateElementInt(addrs []int) []int {
	result := make([]int, 0, len(addrs))
	temp := map[int]struct{}{}
	for _, item := range addrs {
		if _, ok := temp[item]; !ok {
			temp[item] = struct{}{}
			result = append(result, item)
		}
	}
	return result
}

//slice string 去重
func RemoveDuplicateElementStr(addrs []string) []string {
	result := make([]string, 0, len(addrs))
	temp := map[string]struct{}{}
	for _, item := range addrs {
		if _, ok := temp[item]; !ok {
			temp[item] = struct{}{}
			result = append(result, item)
		}
	}
	return result
}

//研报平台, 代码,评级,目标价,标题, 按照这四个字段去重
func RemoveDuplicateElementMap(s []map[string]interface{}) []map[string]interface{} {
	fmt.Println("去重前",s)
	dst := make([]map[string]interface{}, 0)
	if len(s) == 0 {
		return s
	}
	dst = append(dst, s[0])
	fmt.Println("第一个",dst)
	for k, v := range s {
		//fmt.Println("s:", s[k])
		for kk, _ := range dst {
			if s[k]["code"] == dst[kk]["code"] && s[k]["level"] == dst[kk]["level"] && s[k]["target_price"] == dst[kk]["target_price"] && s[k]["title"] == dst[kk]["title"] {
				continue
			} else {
				dst = append(dst, v)
			}
		}
	}
	fmt.Println("去重后",dst)
	return dst
}
