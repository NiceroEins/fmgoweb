package util

import (
	"datacenter/pkg/logging"
	"datacenter/service/performance_service"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"
)

//这个文件下其他不用看了 这个就够了
func Compare(value1, value2 reflect.Value) bool {
	switch value1.Kind() {
	case reflect.Ptr:
		//空指针直接返回
		if value1.IsNil() || value2.IsNil() {
			return value1.IsNil()
		}
		return Compare(value1.Elem(), value2.Elem())
	case reflect.Float64, reflect.Float32:
		return value1.Float() < value2.Float()
	case reflect.Int, reflect.Int64, reflect.Int32, reflect.Int16, reflect.Int8:
		return value1.Int() < value2.Int()
	case reflect.Uint, reflect.Uint64, reflect.Uint32, reflect.Uint16, reflect.Uint8:
		return value1.Uint() < value2.Uint()
	case reflect.String:
		v1, e1 := strconv.ParseFloat(value1.String(), 64)
		v2, e2 := strconv.ParseFloat(value2.String(), 64)
		if e1 == nil && e2 == nil {
			return v1 < v2
		} else {
			return value1.String() < value2.String()
		}
	case reflect.Struct:
		switch value1.Type().Name() {
		case "NullDecimal":
			v1, _ := value1.Interface().(decimal.NullDecimal)
			v2, _ := value2.Interface().(decimal.NullDecimal)
			if !v1.Valid {
				return true
			}
			return v1.Decimal.Cmp(v2.Decimal) < 0
		case "Decimal":
			v1, _ := value1.Interface().(decimal.Decimal)
			v2, _ := value2.Interface().(decimal.Decimal)
			return v1.Cmp(v2) < 0
		case "Time":
			v1, _ := value1.Interface().(time.Time)
			v2, _ := value2.Interface().(time.Time)
			return v1.Before(v2)
		default:
			return false
		}
	default:
		logging.Error("sort.compare() known type: ", value1.Kind().String())
		return false
	}
}

//数据库返回数据绑定结构体
type GetPerformanceMysql struct {
	ID        int        `gorm:"primary_key" json:"id"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" sql:"index"`
	//股票信息
	Name string `json:"name"`
	//股票代码
	Code string `json:"code"`
	//披露时间
	DisclosureTime        string `json:"disclosure_time"`
	DisclosureTimeHistory string `json:"disclosure_time_history"`
	DisclosureChangeAt    string `json:"disclosure_change_at"`

	//预测比例(上下限)
	ForecastProportionLower *float64 `json:"forecast_proportion_lower"`
	ForecastProportionUpper *float64 `json:"forecast_proportion_upper"`

	ForecastProportion *string `json:"forecast_proportion"`

	//预测净利润
	ForecastNetProfit *string `json:"forecast_net_profit"`
	//券商预测
	BrokerProportion *string `json:"broker_proportion"`

	//券商解读
	BrokerInterpretation string `json:"broker_interpretation"`

	//亮点
	HighLights string `json:"high_lights"`
	//亮点截取100个字,后面 + ...
	HighLightsSub100 string `json:"high_lights_sub_100"`

	//本期预告(业绩变动幅度上限)
	PerformanceIncrUpper *float64 `json:"performance_incr_upper"`
	//本期预告(业绩变动幅度下限)
	PerformanceIncrLower *float64 `json:"performance_incr_lower"`

	//净利润
	NetProfit *float64 `json:"net_profit"`
	//净利润同比
	NetProfitGrowthRate *float64 `json:"net_profit_growth_rate"`

	//营业总收入
	BusinessRevenue *float64 `json:"business_revenue"`

	//营业总收入同比
	BusinessRevenueGrowthRate *float64 `json:"business_revenue_growth_rate"`
	//扣非净利润
	NetProfitAfterDed *float64 `json:"net_profit_after_ded"`

	//重要程度
	Important string `json:"important"`
	//用户id
	UserID string `json:"user_id"`
	//展真实姓名
	Realname string `json:"realname"`
	//行业
	Industry     string `json:"industry"`
	IndustryName string `json:"industry_name"`
	//行业解读
	IndustryInterpretation string `json:"industry_interpretation"`
	//业绩变动原因
	PerformanceReason string `json:"performance_reason"`
	//类型
	Type      string `json:"type"`
	Recommend string `json:"recommend"`
}

//行业跟踪页面排序
func StructSort4(s []SearchIndustryTrackMysql, order string) (dst []SearchIndustryTrackMysql, err error) {
	orderArray := strings.Split(order, "_")
	orderArray0, _ := strconv.Atoi(orderArray[0])
	sortFieldMap := make(map[int]string)
	sortFieldMap[2] = "PerformanceIncrUpper"
	sortFieldMap[3] = "ForecastPublishDate"
	sortFieldMap[4] = "ForecastIncrease1"
	sortFieldMap[5] = "ForecastIncrease3"
	sortFieldMap[6] = "ForecastIncrease5"
	sortFieldMap[7] = "NetProfitYear"
	sortFieldMap[8] = "AnnouncementIncrease1"
	sortFieldMap[9] = "AnnouncementIncrease3"
	sortFieldMap[10] = "AnnouncementIncrease5"
	sortFieldMap[11] = "NetProfit1"
	sortFieldMap[12] = "NetProfit2"
	sortFieldMap[13] = "NetProfit3"
	sortFieldMap[14] = "NetProfit4"
	sortFieldMap[15] = "NetProfitGrowthRate1"
	sortFieldMap[16] = "NetProfitGrowthRate2"
	sortFieldMap[17] = "NetProfitGrowthRate3"
	sortFieldMap[18] = "NetProfitGrowthRate4"
	sortFieldMap[19] = "BusinessRevenue1"
	sortFieldMap[20] = "BusinessRevenue2"
	sortFieldMap[21] = "BusinessRevenue3"
	sortFieldMap[22] = "BusinessRevenue4"
	sortFieldMap[23] = "BusinessRevenueGrowthRate1"
	sortFieldMap[24] = "BusinessRevenueGrowthRate2"
	sortFieldMap[25] = "BusinessRevenueGrowthRate3"
	sortFieldMap[26] = "BusinessRevenueGrowthRate4"
	sortFieldMap[27] = "NetProfitAfterDedQuarter1"
	sortFieldMap[28] = "NetProfitAfterDedQuarter2"
	sortFieldMap[29] = "NetProfitAfterDedQuarter3"
	sortFieldMap[30] = "NetProfitAfterDedQuarter4"
	sortFieldMap[31] = "NetProfitAfterDedGrowthRate1"
	sortFieldMap[32] = "NetProfitAfterDedGrowthRate2"
	sortFieldMap[33] = "NetProfitAfterDedGrowthRate3"
	sortFieldMap[34] = "NetProfitAfterDedGrowthRate4"
	sortFieldMap[35] = "CashFlowPerShare1"
	sortFieldMap[36] = "CashFlowPerShare2"
	sortFieldMap[37] = "CashFlowPerShare3"
	sortFieldMap[38] = "CashFlowPerShare4"
	sortFieldMap[39] = "GrossProfitMarginQuarter1"
	sortFieldMap[40] = "GrossProfitMarginQuarter2"
	sortFieldMap[41] = "GrossProfitMarginQuarter3"
	sortFieldMap[42] = "GrossProfitMarginQuarter4"

	//为空的不参与排序 todo
	//var sNotNil  []SearchIndustryTrackMysql
	//	for _,v := range s{
	//		switch orderArray0 {
	//		case 2:
	//			if v.
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//		case 2:
	//
	//		}
	//	}
	//	sNotNil
	if orderArray[1] == "asc" {
		sort.Slice(s, func(i, j int) bool {
			return compare(reflect.ValueOf(s[i]).FieldByName(sortFieldMap[orderArray0]), reflect.ValueOf(s[j]).FieldByName(sortFieldMap[orderArray0]))
		})
		return s, nil
	} else {
		sort.Slice(s, func(i, j int) bool {
			return !compare(reflect.ValueOf(s[i]).FieldByName(sortFieldMap[orderArray0]), reflect.ValueOf(s[j]).FieldByName(sortFieldMap[orderArray0]))
		})
		return s, nil
	}

	//return
}
func compare(value1, value2 reflect.Value) bool {
	switch value1.Kind() {
	case reflect.Ptr:
		//空指针直接返回
		if value1.IsNil() {
			return true
		} else if value2.IsNil() {
			return false
		}
		return compare(value1.Elem(), value2.Elem())
	case reflect.Float64, reflect.Float32:
		return value1.Interface().(float64) < value2.Interface().(float64)
	case reflect.Int, reflect.Int64:
		return value1.Interface().(int64) < value2.Interface().(int64)
	case reflect.String:
		return value1.Interface().(string) < value2.Interface().(string)
	default:
		logging.Error("sort.compare() known type: ", value1.Kind().String())
		return true
	}
}

//业绩计算预测比例排序
func StructSort3(s []performance_service.GetPerformanceMysql, order string) (dst []performance_service.GetPerformanceMysql, err error) {
	switch order {
	case "asc":
		//预测比例排序
		if order == "asc" {
			sort.Slice(s, func(i, j int) bool {
				return compare(reflect.ValueOf(s[i]).FieldByName("ForecastProportionUpper"), reflect.ValueOf(s[j]).FieldByName("ForecastProportionUpper"))
			})
			return s, nil
		}

	case "desc":
		//预测比例排序
		if order == "desc" {
			sort.Slice(s, func(i, j int) bool {
				return !compare(reflect.ValueOf(s[i]).FieldByName("ForecastProportionUpper"), reflect.ValueOf(s[j]).FieldByName("ForecastProportionUpper"))
			})
			return s, nil
		}
	}
	return
}

//业绩计算券商预测排序
func StructSort5(s []performance_service.GetPerformanceMysql, order string) (dst []performance_service.GetPerformanceMysql, err error) {
	switch order {
	case "asc":
		if order == "asc" {
			sort.Slice(s, func(i, j int) bool {
				return compare(reflect.ValueOf(s[i]).FieldByName("BrokerProportionUpper"), reflect.ValueOf(s[j]).FieldByName("BrokerProportionUpper"))
			})
			return s, nil
		}
	case "desc":
		if order == "desc" {
			sort.Slice(s, func(i, j int) bool {
				return !compare(reflect.ValueOf(s[i]).FieldByName("BrokerProportionUpper"), reflect.ValueOf(s[j]).FieldByName("BrokerProportionUpper"))
			})
			return s, nil
		}
	}
	return
}

//业绩计算预测净利润排序
func StructSort6(s []performance_service.GetPerformanceMysql, order string) (dst []performance_service.GetPerformanceMysql, err error) {
	switch order {
	case "asc":
		if order == "asc" {
			sort.Slice(s, func(i, j int) bool {
				return compare(reflect.ValueOf(s[i]).FieldByName("ForecastNetProfitUpper"), reflect.ValueOf(s[j]).FieldByName("ForecastNetProfitUpper"))
			})
			return s, nil
		}
	case "desc":
		if order == "desc" {
			sort.Slice(s, func(i, j int) bool {
				return !compare(reflect.ValueOf(s[i]).FieldByName("ForecastNetProfitUpper"), reflect.ValueOf(s[j]).FieldByName("ForecastNetProfitUpper"))
			})
			return s, nil
		}
	}
	return
}

type GetResearchReportMysql struct {
	Id           int    `json:"id"`
	Code         string `json:"code"`         //股票代码
	Name         string `json:"name"`         //股票名称
	Organization string `json:"organization"` //机构
	Author       string `json:"author"`       //作者
	Level        string `json:"level"`        //评级
	LevelChange  string `json:"level_change"` //评级变动
	LevelStatus  string `json:"level_status"` //评级变动的箭头，向上或者向下
	LevelCalc    string `json:"level_calc"`   //评级实时计算（up/down）
	Industry     string `json:"industry"`
	//Did          int    `json:"did"`
	//Dauthor      string `json:"dauthor"`
	Type string `json:"type"`

	//CollectedYesterdayPrice  *float64 `json:"collected_yesterday_price"` //昨收价
	PreClose *float64 `json:"pre_close"` //昨收价

	TargetPrice              *string  `json:"target_price"` //目标价格
	TargetPriceSorted        *float64 `json:"target_price_sorted"`
	RisingSpace              *string  `json:"rising_space"` //上涨空间
	RisingSpaceSorted        *float64 `json:"rising_space_sorted"`
	Title                    string   `json:"title"`               //观点
	ExpectedNetProfit        *string  `json:"expected_net_profit"` //预测净利润
	ExpectedNetProfitSorted  *float64 `json:"expected_net_profit_sorted"`
	ForecastEps              *string  `json:"forecast_eps"` //预测eps
	ForecastEpsSorted        *float64 `json:"forecast_eps_sorted"`
	PriceEarningRatio        *string  `json:"price_earning_ratio"`        //预测PE
	PriceEarningRatioSorted  *float64 `json:"price_earning_ratio_sorted"` //预测PE
	Heat                     *int     `json:"heat"`                       //热度
	CompoundGrowthRate       *string  `json:"compound_growth_rate"`       //复合增长率
	CompoundGrowthRateSorted *float64 `json:"compound_growth_rate_sorted"`
	Content                  string   `json:"-"` //研报内容
	ContentArray             []string `json:"content_array"`
	RecommendUserId          string   `json:"-"`
	RecommendUserIdArray     []int    `json:"recommend_user_id_array"`
	SourceLink               string   `json:"source_link"`
	PdfFirstCoverage         *int     `json:"pdf_first_coverage"`
	PdfRaise                 *int     `json:"pdf_raise"`

	ImportantSecurities bool `json:"important_securities"` //重点券商
	ImportantAuthor     bool `json:"important_author"`     //重点作者

	PublishDate string `json:"publish_date"` //日期（废弃）
	SpiderTime  string `json:"spider_time"`  //日期
	Recommend   uint   `json:"recommend"`    //推荐

	ExpectedNetProfitRaise string `json:"expected_net_profit_raise"`
	Head                   int    `json:"head"`
}

//重点券商,重点作者
func (g *GetResearchReportMysql) AfterFind() (err error) {
	if g.Type == "重点券商" {
		g.ImportantSecurities = true
	}
	return
}

//研报排序
func StructSort7(s []GetResearchReportMysql, order string) (dst []GetResearchReportMysql, err error) {
	orderArray := strings.Split(order, "_")
	orderArray0, _ := strconv.Atoi(orderArray[0])
	sortMap := make(map[int]string)
	sortMap[1] = "TargetPriceSorted"
	sortMap[2] = "RisingSpaceSorted"
	sortMap[3] = "ExpectedNetProfitSorted"
	sortMap[4] = "ForecastEpsSorted"
	sortMap[5] = "PriceEarningRatioSorted"
	//sortMap[5] = ""

	if orderArray[1] == "asc" {
		sort.Slice(s, func(i, j int) bool {
			return compare(reflect.ValueOf(s[i]).FieldByName(sortMap[orderArray0]), reflect.ValueOf(s[j]).FieldByName(sortMap[orderArray0]))
		})
		return s, nil
	} else {
		sort.Slice(s, func(i, j int) bool {
			return !compare(reflect.ValueOf(s[i]).FieldByName(sortMap[orderArray0]), reflect.ValueOf(s[j]).FieldByName(sortMap[orderArray0]))
		})
		return s, nil
	}
	//return
}

//字符串(都是小数或整数)切片排序,返回升序字符串数组
func SortStrSlice(s *[]string, order string) (res []float64) {

	if order == "asc" {
		//排序,转float排序
		res = make([]float64, 0)
		for _, v := range *s {
			res = append(res, Atof(v))
		}
		sort.Float64s(res)
		return res
	} else {
		//排序,转float排序
		res = make([]float64, 0)
		for _, v := range *s {
			res = append(res, Atof(v))
		}
		sort.Float64s(res)
		res = reverse(res)
		return res
	}
}

func reverse(s []float64) []float64 {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return s
}
