package app

import (
	"datacenter/pkg/e"
	"datacenter/pkg/logging"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego/validation"
	"github.com/go-playground/validator/v10"
	"net/http"
	"strings"
)

// MarkErrors logs error logs
func MarkErrors(errors []*validation.Error) {
	for _, err := range errors {
		logging.Info(err.Key, err.Message)
	}

	return
}

func TransErrors(err error) []string {
	ret := make([]string, 0)
	switch err.(type) {
	case validator.ValidationErrors:
		for _, e := range err.(validator.ValidationErrors) {
			msg := e.Translate(trans)
			ret = append(ret, msg)
			logging.Info(msg)
		}
	case *json.UnmarshalTypeError:
		ret = append(ret, err.Error())
		logging.Info(err.Error())
	case *json.UnsupportedTypeError:
		ret = append(ret, err.Error())
		logging.Info(err.Error())
	case *json.InvalidUnmarshalError:
		ret = append(ret, err.Error())
		logging.Info(err.Error())
	case *json.MarshalerError:
		ret = append(ret, err.Error())
		logging.Info(err.Error())
	case *json.SyntaxError:
		ret = append(ret, err.Error())
		logging.Info(err.Error())
	}
	return ret
}

//解析数据到结构体
func (g *Gin) ParseRequest(request interface{}) error {
	err := g.C.ShouldBind(request)
	var errStr string
	if err != nil {
		switch err.(type) {
		case validator.ValidationErrors:
			errStr = Translate(err.(validator.ValidationErrors))
		case *json.UnmarshalTypeError:
			unmarshalTypeError := err.(*json.UnmarshalTypeError)
			errStr = fmt.Errorf("%s 类型错误，期望类型 %s", unmarshalTypeError.Field, unmarshalTypeError.Type.String()).Error()
		default:
			errStr = errors.New(err.Error()).Error()
		}
		logging.Error(err)
		g.Response(http.StatusBadRequest, e.INVALID_PARAMS, errStr)
		return err
	}
	return nil
}

func Translate(errs validator.ValidationErrors) string {
	var errList []string
	for _, e := range errs {
		// can translate each error one at a time.
		errList = append(errList, e.Translate(trans))
	}
	return strings.Join(errList, "|")
}
