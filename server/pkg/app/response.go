package app

import (
	"datacenter/pkg/util"
	"github.com/gin-gonic/gin"

	"datacenter/pkg/e"
)

type Gin struct {
	C *gin.Context
}

type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// Response setting gin.JSON
func (g *Gin) Response(httpCode, errCode int, data interface{}) {
	g.C.JSON(httpCode, Response{
		Code: errCode,
		Msg:  e.GetMsg(errCode),
		Data: data,
	})
	return
}
//通过token,获取用户信息
func (g *Gin) GetUserInfo()(userId,username string){
	var err error
	token := g.C.Query("token")
	//解析token,获得用户id,和用户名
	decryptToken, _ := util.ParseToken(token)
	userId,err = util.DesDecrypt(decryptToken.Userid)
	if err !=nil{
		return "0",""
	}
	username,err = util.DesDecrypt(decryptToken.Username)
	if err !=nil{
		return "0",""
	}
	return
}
