package app

import (
	// "datacenter/pkg/e"
	// "github.com/astaxie/beego/validation"
	// "github.com/gin-gonic/gin"
	// "net/http"

	"datacenter/pkg/e"
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	zh_translations "github.com/go-playground/validator/v10/translations/zh"
	"net/http"
)

var (
	validate *validator.Validate
	trans    ut.Translator
)

func Init() {
	zhIns := zh.New()
	uni := ut.New(zhIns, zhIns)
	trans, _ = uni.GetTranslator("zh")
	//validate = validator.New()

	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		validate = v
		//注册翻译器
		_ = zh_translations.RegisterDefaultTranslations(v, trans)

		////注册一个函数，获取struct tag里自定义的label作为字段名
		//v.RegisterTagNameFunc(func(fld reflect.StructField) string {
		//	name := fld.Tag.Get("label")
		//	return name
		//})
	}
}

// BindAndValid binds and validates data, using validator.v10
func BindAndCheck(c *gin.Context, form interface{}) (int, int, []string) {
	err := c.Bind(form)
	if err != nil {
		msg := TransErrors(err)
		return http.StatusBadRequest, e.INVALID_PARAMS, msg
		//return http.StatusBadRequest, e.INVALID_PARAMS, []string{err.Error()}
	}

	//err = validate.Struct(form)
	//if err != nil {
	//	msg := TransErrors(err)
	//	return http.StatusBadRequest, e.INVALID_PARAMS, msg
	//}

	return http.StatusOK, e.SUCCESS, nil
}

// BindAndValid binds and validates data
func BindAndValid(c *gin.Context, form interface{}) (int, int) {
	err := c.Bind(form)
	if err != nil {
		return http.StatusBadRequest, e.INVALID_PARAMS
	}

	valid := validation.Validation{}
	check, err := valid.Valid(form)
	if err != nil {
		return http.StatusInternalServerError, e.ERROR
	}
	if !check {
		MarkErrors(valid.Errors)
		return http.StatusBadRequest, e.INVALID_PARAMS
	}

	return http.StatusOK, e.SUCCESS
}
