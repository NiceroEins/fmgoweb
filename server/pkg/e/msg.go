package e

var MsgFlags = map[int]string{
	SUCCESS:                              "ok",
	ERROR:                                "fail",
	INVALID_PARAMS:                       "请求参数错误",
	ERROR_EXIST_TAG:                      "已存在该标签名称",
	ERROR_EXIST_TAG_FAIL:                 "获取已存在标签失败",
	ERROR_NOT_EXIST_TAG:                  "该标签不存在",
	ERROR_GET_TAGS_FAIL:                  "获取所有标签失败",
	ERROR_COUNT_TAG_FAIL:                 "统计标签失败",
	ERROR_ADD_TAG_FAIL:                   "新增标签失败",
	ERROR_EDIT_TAG_FAIL:                  "修改标签失败",
	ERROR_DELETE_TAG_FAIL:                "删除标签失败",
	ERROR_EXPORT_TAG_FAIL:                "导出标签失败",
	ERROR_IMPORT_TAG_FAIL:                "导入标签失败",
	ERROR_NOT_EXIST_ARTICLE:              "该文章不存在",
	ERROR_ADD_ARTICLE_FAIL:               "新增文章失败",
	ERROR_DELETE_ARTICLE_FAIL:            "删除文章失败",
	ERROR_CHECK_EXIST_ARTICLE_FAIL:       "检查文章是否存在失败",
	ERROR_EDIT_ARTICLE_FAIL:              "修改文章失败",
	ERROR_COUNT_ARTICLE_FAIL:             "统计文章失败",
	ERROR_GET_ARTICLES_FAIL:              "获取多个文章失败",
	ERROR_GET_ARTICLE_FAIL:               "获取单个文章失败",
	ERROR_GEN_ARTICLE_POSTER_FAIL:        "生成文章海报失败",
	ERROR_EDIT_SEED_FAIL:                 "编辑新闻源失败",
	ERROR_DELETE_SEED_FAIL:               "删除新闻源失败",
	ERROR_ADD_SEED_FAIL:                  "添加新闻源失败",
	ERROR_QUERY_SEED_FAIL:                "查询新闻源失败",
	ERROR_AUTH_CHECK_TOKEN_FAIL:          "Token鉴权失败",
	ERROR_AUTH_CHECK_TOKEN_TIMEOUT:       "Token已超时",
	ERROR_AUTH_TOKEN:                     "Token生成失败",
	ERROR_AUTH:                           "Token错误",
	ERROR_UPLOAD_SAVE_IMAGE_FAIL:         "保存图片失败",
	ERROR_UPLOAD_CHECK_IMAGE_FAIL:        "检查图片失败",
	ERROR_UPLOAD_CHECK_IMAGE_FORMAT:      "校验图片错误，图片格式或大小有问题",
	ERROR_UPLOAD_GET_PUBLICKEY_FAIL:      "校验失败",
	ERROR_GET_REDIS_DAY_PRODUCT_TASK_NUM: "获取redis:day_product_task_num失败",
	ERROR_PLZ_LOGIN:                      "请登录",
	ERROR_IPO_ERR_MSG:                    "iop_equity.go DuplicationInsertUIopEquityRelationship(),err : u_iop_equity_relationship表中有重复",
	ERROR_RECOMMOND_STOCK_NUM:            "推荐的有效股票已经超过5只",
	ERROR_RECOMMOND_STOCK:                "推荐股票失败",
	ERROR_RECOMMOND_STOCK_DEL:            "删除推荐股票失败",
	ERROR_RECOMMOND_STOCK_EXIST:          "推荐股票已存在",

	// 日历
	ERROR_CALENDAR_FAIL:                  "获取日历列表失败",
	ERROR_CALENDAR_ADD_FAIL:			  "日历添加失败",
	ERROR_CALNEDAR_EDIT_FAIL:			  "日历编辑失败",
	ERROR_CALENDAR_NOT_EXIST:			  "日历记录不存在",
	// 版本更新记录
	ERROR_CHANGELOG_FAIL:                 "版本日志添加失败",
	ERROR_CHANGELOG_NOT_EXIST:            "版本日志不存在",

	// 用户中心
	ERROR_HOME_STOCK_POOL_FAIL:           "添加股票失败，请重试",
	ERROR_HOME_STOCK_POOL_NOT_EXIST:      "删除股池记录不存在，请重试",
	ERROR_HOME_STOCK_POOL_EXIST:          "添加记录已存在，请重试",
	ERROR_HOME_EVENT_EXIST:               "点评失败，请重试",
	ERROR_HOME_PUSH_STOCK_FAIL:           "搜索参数有误，请重试",
	ERROR_HOME_INDUSTRY_EXISTS:           "添加的行业重复，请检查",

	// 拍卖
	ERROR_AUCTION_FAIL:                   "拍卖已读提交失败",

	ERROR_FUND_EXISTS:                    "添加基金失败",
	ERROR_FUND_FAIL:                      "添加基金已存在",
	ERROR_FUND_NOT_EXISTS:                "基金不存在",
	ERROR_FUND_DUPICATION:                "基金名称重复，添加失败",
	ERROR_FUND_EDIT:                      "基金名称重复，修改失败",
	ERROR_FUND_CUSTOMER_EXISTS: 		  "用户份额已存在",
	ERROR_FUND_CUSTOMER_FAIL:             "添加用户份额失败",
	ERROR_CUSTOMER_EXISTS: 		          "用户份额已存在",
	ERROR_CUSTOMER_NOT_EXISTS: 		      "用户不存在",
	ERROR_CUSTOMER_FAIL:                  "添加用户份额失败",
	ERROR_NOTICE_EXISTS:                  "基金公告已存在",
	ERROR_NOTICE_FAIL:                    "基金公告添加失败",
	ERROR_NOTICE_NOT_EXISTS:              "基金公告不存在",
	ERROR_BONUS_EXISTS:                   "基金分红存在",
	ERROR_BONUS_FAIL:                     "基金分红添加失败",
	ERROR_BONUS_NOT_EXISTS:               "基金分红纪录不存在",
	ERROR_BONUS_DUPLICATION:              "基金分红纪录已重复",
	ERROR_TICK_EXISTS:                    "净值维护记录存在",
	ERROR_TICK_FAIL:                      "净值维护失败",
	ERROR_TICK_NOT_EXISTS:                "净值维护纪录不存在",
	ERROR_USER_LOGIN_FAIL:                "用户登录失败",
	ERROR_USER_NOT_EXIST:                 "账号或密码错误",
	ERROR_USER_BONUS_NOTICE_FAIL:         "获取分红纪录失败，请重试",

	ERROR_STOCK_POOL_EXISTS:              "研究股池记录已经存在",
	ERROR_STOCK_POOL_NOT_EXISTS:          "研究股池记录不存在",
}

// GetMsg get error information based on Code
func GetMsg(code int) string {
	msg, ok := MsgFlags[code]
	if ok {
		return msg
	}

	return MsgFlags[ERROR]
}
