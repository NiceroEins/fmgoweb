# DataCenter

Hao Sheng Internal API System

## Installation
```
$ git clone http://120.27.210.105:8001/hs/newserver.git
```

## How to run

### Required

- Mysql
- Redis

### Conf

You should modify `conf/app.ini`

```
[database]
Type = mysql
User = root
Password =
Host = 127.0.0.1:3306
Name = blog
TablePrefix = blog_

[redis]
Host = 127.0.0.1:6379
Password =
MaxIdle = 30
MaxActive = 30
IdleTimeout = 200
...
```

### Run
```
$ cd $PROJECT_PATH/

$ go run main.go 
```