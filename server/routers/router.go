package routers

import (
	home "datacenter/routers/api/home"
	v1 "datacenter/routers/api/v1"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"net/http"

	"datacenter/middleware/jwt"
	"datacenter/pkg/export"
	"datacenter/pkg/qrcode"
	"datacenter/pkg/upload"
	"datacenter/routers/api"
	"datacenter/routers/api/v0"
)

// InitRouter initialize routing information
func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	r.StaticFS("/export", http.Dir(export.GetExcelFullPath()))
	r.StaticFS("/upload/images", http.Dir(upload.GetImageFullPath()))
	r.StaticFS("/qrcode", http.Dir(qrcode.GetQrCodeFullPath()))

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.POST("/upload", api.UploadImage)

	//WebSocket接口
	//connect
	gws1 := r.Group("/ws")
	gws1.Use(jwt.JWT())
	{
		gws1.GET("/push", v1.WSConnect) //消息推送
	}
	gws2 := r.Group("/ws")
	gws2.GET("/stock", v1.WSConnect) //行情

	gv0 := r.Group("/api/v0")
	gv0.Use(jwt.JWT())
	{
		//获取标签列表
		gv0.GET("/tags", v0.GetTags)
		//新建标签
		gv0.POST("/tags", v0.AddTag)
		//更新指定标签
		gv0.PUT("/tags/:id", v0.EditTag)
		//删除指定标签
		gv0.DELETE("/tags/:id", v0.DeleteTag)
		//导出标签
		r.POST("/tags/export", v0.ExportTag)
		//导入标签
		r.POST("/tags/import", v0.ImportTag)

		//获取文章列表
		gv0.GET("/articles", v0.GetArticles)
		//获取指定文章
		gv0.GET("/articles/:id", v0.GetArticle)
		//新建文章
		gv0.POST("/articles", v0.AddArticle)
		//更新指定文章
		gv0.PUT("/articles/:id", v0.EditArticle)
		//删除指定文章
		gv0.DELETE("/articles/:id", v0.DeleteArticle)
		//生成文章海报
		gv0.POST("/articles/poster/generate", v0.GenerateArticlePoster)
	}

	//爬虫接口
	gv1 := r.Group("/api/v1")
	{
		//以下是爬虫接口
		//任务状态监控
		gv1.GET("/monitor", v1.GetMonitor)
		//获取任务
		gv1.GET("/task", v1.GetTask)
		//手动写入任务
		gv1.POST("/task", v1.SetTask)
		//查看日志
		gv1.GET("/report", v1.GetReport)
		//接收爬虫数据,并做相应处理
		gv1.POST("/report", v1.PostReport)
		//监控信息
		gv1.GET("/realtimeinfo", v1.GetRealTimeInfo)
		//获取代理地址
		gv1.GET("/proxy", v1.GetProxy)

		//以下是文件服务接口
		//不验证token
		gv1.GET("/fs", v1.GetPolicyToken)
		gv1.POST("/fs", v1.FsCallback)

		//以下为WebServer接口
		//滚动新闻
		gv1.GET("/news", v1.GetNews)
		//新闻互动搜索
		gv1.GET("/search", v1.SearchNews)
		//登陆
		gv1.POST("/login", v1.Login)
		//退出登陆
		gv1.POST("/logout", v1.Logout)
		//研报导出
		gv1.GET("/export_research_report", v1.ExportResearchReport)

		//redis测试
		gv1.GET("/test_redis", v1.TestRedis)
		//量化导出
		gv1.GET("/quantization/export", v1.Export)
		//获取报告期列表
		gv1.GET("/report_periods", v1.GetReportPeriodList)
	}
	//webserver 中间件过滤
	gv2 := r.Group("/api/v1").Use(jwt.JWT())
	{
		//新增账号
		gv2.POST("/add_user", v1.AddUser)

		//账号管理:列表
		gv2.GET("/get_user_list", v1.GetUserList)
		//账号管理:编辑
		gv2.POST("/edit_user", v1.EditUser)
		//账号管理:删除(软删除)
		gv2.POST("/del_user", v1.DelUser)
		//账号管理:密码重置
		gv2.POST("/edit_pwd", v1.EditPwd)
		//账号管理:停用/启用
		gv2.POST("/edit_status", v1.EditStatus)
		//修改密码
		gv2.POST("/edit_user_pwd", v1.EditUserPwd)
		//权限列表(无user_id=all,有user_id=个人所有)
		gv2.GET("/get_privilege_list", v1.GetPrivilegeList)
		//获取配置文件
		gv2.GET("/profile", v1.GetUserProfile)
		//修改配置文件
		gv2.POST("/profile", v1.UpdateUserProfile)

		//业绩计算:根据系统时间,返回4个最近报告期
		gv2.GET("/get_report_period", v1.GetReportPeriod)
		//业绩计算:获取用户名,用户id接口
		gv2.GET("/get_performance_user_info", v1.PerformanceGetUserInfo)
		//通用获取用户
		gv2.GET("/get_user_info", v1.GetUserInfo)

		//业绩计算:搜索
		gv2.GET("/performance", v1.PerformanceCalculate)
		//业绩计算:编辑修改:(更新数据)
		gv2.POST("/post_edit_performance", v1.EditPerformanceCalculate)
		//业绩计算,导出
		gv1.GET("/performance_export", v1.ExportPerformanceCalculate)
		//业绩计算,导入excel
		gv1.POST("/import_performance_edit", v1.ImportPerformanceEdit)
		//业绩计算,导入excel
		//gv1.POST("/import_performance_excel", v1.ImportPerformanceExcel)
		//业绩趋势
		gv2.GET("/performance_trend", v1.GetPerformanceTrend)
		//业绩预告统计
		gv2.GET("/performance_forecast_statistics", v1.GetForecastStatistics)
		//业绩跟踪
		gv2.GET("/performance_trace", v1.GetTrace)
		//历史亮点
		gv2.GET("/high_lights_history", v1.GetHighLightsHistory)

		//行业跟踪:搜索
		gv2.GET("/search_industry_track", v1.PerformanceIndustryTrack)
		//行业跟踪:修改(post)
		gv2.POST("/post_edit_industry_track", v1.PostEditIndustryTrack)
		//行业跟踪:行业智能搜索
		gv2.GET("/get_industry_list", v1.PerformanceGetIndustry)

		//股票维护:查询页面
		gv2.GET("/search_stock_maintain", v1.SearchStockMaintain)
		//股票维护:添加页面,有一个股票搜索(u_stock),搜索出来之后,和行业相关联(行业是自己添加的)
		gv2.POST("/stock_maintain", v1.AddRelationShip)
		//股票维护:添加页面 股票名称和股票code 模糊搜索
		gv2.GET("/stock_search", v1.StockSearch)
		//股票维护:修改页面(POST)
		gv2.POST("/edit_stock_maintain", v1.EditRelationShip)
		//股票维护:删除
		gv2.POST("/del_stock_maintain", v1.DelStockMaintain)
		//股票维护:导入excel
		gv2.POST("/import_stock_maintain", v1.ImportStockMaintain)

		//行业维护:添加
		gv2.POST("/save_industry", v1.SaveIndustry)
		//行业维护:查询
		gv2.GET("/search_industry", v1.SearchIndustry)
		//行业维护:修改(post:更新)
		gv2.POST("/post_edit_industry", v1.PostEditIndustry)
		//行业维护:删除
		gv2.POST("/del_industry", v1.DelIndustry)

		//推送库查询
		gv2.GET("/push", v1.GetPushData)

		//消息事件
		gv2.POST("/event", v1.PushEvent)

		//研报
		gv2.GET("/get_research_report", v1.GetResearchReport)
		//研报推荐按钮
		gv2.POST("/edit_research_report_recommend", v1.EditResearchReportRecommend)

		//关键词列表
		gv2.GET("/keyword_list", v1.KeywordList)
		//个人关键词列表
		gv2.GET("/keyword_list_person", v1.KeywordListPerson)
		//新增关键词
		gv2.POST("/add_keyword", v1.AddKeyword)
		//编辑关键词
		gv2.POST("/edit_keyword", v1.EditKeyword)
		//删除关键词
		gv2.POST("/del_keyword", v1.DelKeyword)

		//新闻源维护:添加
		gv2.POST("/save_seed", v1.SaveSeed)
		//新闻源维护:查询
		gv2.GET("/get_seed", v1.GetSeeds)
		//新闻源维护:修改(post:更新)
		gv2.POST("/edit_seed", v1.EditSeed)
		//新闻源维护:删除
		gv2.POST("/del_seed", v1.DelSeed)
		//新闻源维护:分类智能查询
		gv2.GET("/get_seed_cata", v1.GetSeedCataList)

		//ipo股权
		gv2.GET("/ipo_list", v1.IpoList)

		//ipo股权
		gv2.GET("/ipo_relationship", v1.IpoRelationShip)

		//股池
		gv2.GET("/stock_pool_list", v1.StockPoolGetList)
		//股池类型
		gv2.GET("/stock_pool_type", v1.GetAllStockPoolType)
		// 运营股池
		gv2.GET("/stock_pool_manage", v1.StockPoolGetManage)
		// 添加
		gv2.POST("/stock_pool_add", v1.AddStockPool)
		// // 删除
		gv2.POST("/stock_pool_del", v1.DelStockPool)
		// 编辑
		gv2.POST("/stock_pool_edit", v1.EditStockPool)
		//股池导出
		gv1.GET("/stock_pool_export", v1.ExportStockPool)

		//新闻统计
		gv2.GET("/statistics_news", v1.StatNews)
		//用户统计
		gv2.GET("/statistics_user", v1.GetUserStatistics)
		//用户统计详情
		gv2.GET("/statistics_person", v1.GetPersonStatistics)
		//新闻源统计
		gv2.GET("/statistics_seed", v1.GetSeedStatisticsList)
		//新闻源统计折线图
		gv2.GET("/statistics_seed_map", v1.GetSeedStatisticsMap)
		//异常新闻源重置
		gv2.GET("/reset_seed", v1.ResetSeed)
		//行为统计列表
		gv2.GET("/statistics_privilege_list", v1.GetPrivilegeStatisticsList)
		//行为统计用户列表
		gv2.GET("/statistics_privilege_user_list", v1.GetPrivilegeStatisticsUserList)
		//用户行为列表
		gv2.GET("/user_statistics_privilege_total", v1.GetUserPrivilegeStatisticsTotal)
		//用户行为详情列表
		gv1.GET("/user_statistics_privilege_detail", v1.GetUserPrivilegeStatisticsDetail)
		//用户行为柱状图
		gv1.GET("/user_statistics_privilege_map", v1.GetUserPrivilegeStatisticsMap)

		//复盘
		//复盘平台列表
		gv2.GET("/replay_list", v1.GetTotalReplayList)
		//复盘平台统计
		gv2.GET("/replay_statistics", v1.GetReplayStatistics)
		//我的复盘列表
		gv2.GET("/my_replay_list", v1.GetMyReplayList)
		//添加我的复盘
		gv2.POST("/add_my_replay", v1.CreateMyReplay)
		//编辑我的复盘
		gv2.POST("/edit_my_replay", v1.EditMyReplay)
		//复盘导入
		gv2.POST("/import_replay", v1.ImportReplay)
		//删除我的复盘
		gv2.POST("/del_my_replay", v1.DelMyReplay)

		// 新业绩追踪

		//考核
		//考核评审
		gv2.GET("/exam_review", v1.ExamReview)
		//考核成绩
		gv2.GET("/exam_summary", v1.ExamSummary)
		//考核评星
		gv2.POST("/exam_set", v1.ExamSet)
		//考核删除
		gv2.POST("/exam_del", v1.ExamDel)
		//考核姓名
		gv2.GET("/exam_name", v1.ExamName)
		// 日历切换
		// 列表
		gv2.GET("/calendar_list", v1.CalendarLists)
		// 添加
		gv2.POST("/calendar_add", v1.CalendarAdd)
		// 删除
		gv2.GET("/calendar_del", v1.CalendarDel)
		// 编辑
		gv2.POST("/calendar_edit", v1.CalendarEdit)
		// 近期事件
		gv2.GET("/calendar_event", v1.CalendarListByMonth)
		// 非模糊时间展示
		gv2.GET("/calendar_month", v1.CalendarByMonth)

		// 行为日志
		gv2.GET("/changelog_list", v1.ChangelogLists)
		// 添加
		gv2.POST("/changelog_add", v1.ChangelogAdd)
		// 删除
		gv2.GET("/changelog_del", v1.ChangelogDel)
		// 编辑
		gv2.POST("/changelog_edit", v1.ChangelogEdit)

		// 用户中心
		//个人公告列表
		gv2.GET("/home/my_announcement_list", v1.GetMyAnnouncementEventList)
		//个人研报列表
		gv2.GET("/home/my_research_list", v1.GetMyResearchEventList)
		//个人调研列表
		gv2.GET("/home/my_investigation_list", v1.GetMyInvestigationList)
		// 个人股池 列表
		gv2.GET("/home/stock_pool_manage", home.StockPoolGetList)
		// 个人股池 添加
		gv2.POST("/home/stock_pool_add", home.AddStockPool)
		// 个人股池 删除
		gv2.GET("/home/stock_pool_del", home.DelStockPool)
		// 股池 行业列表
		gv2.GET("/home/industry_names", home.GetIndustryNames)
		// 个人股池 点评
		gv2.POST("/home/event", home.EventContent)
		// 公告类型
		gv2.GET("/home/annoucement_list", home.AnnouncementTypeNames)
		// 获取历史点评
		gv2.GET("/home/history_events", home.GetHistoryEvent)
		// 整合页面
		// 推送内容 股池
		gv2.GET("/home/stock_pool_push", home.StockPushList)
		// 用户行业管理
		gv2.GET("/home/user_industrys", home.IndustryNamesUser)
		// 用户行业管理编辑
		gv2.POST("/home/user_industrys_edit", home.IndustryNamesUserEdit)
		// 用户行业删除
		gv2.GET("/home/user_industrys_del", home.IndustryNamesUserDel)
		// 用户中心初始化
		gv2.GET("/home/start", home.Start)
		// 预测上调栏目
		gv2.GET("/home/net_profit_change_push_list", v1.GetNetProfitChangePushList)

		// 行业监控接口
		// 行业监控列表
		gv2.GET("/home/industry_monitor", home.IndustryMonitor)
		// 行业监控每日折线图
		gv2.GET("/home/industry_pic", home.IndustryPic)
		// 行业监控每秒折线图
		gv2.GET("/home/industry_pic_minute", home.IndustryPicPerMin)
		// 行业股票
		gv2.GET("/home/industry_industry_stock", home.GetIndustryStock)
		// 历史行业点评
		gv2.GET("/home/industry_history_events", home.GetIndustryHistoryEvent)

		// 拍卖接口
		gv2.GET("/auction_list", v1.AuctionLists)
		gv2.POST("/auction_read", v1.AuctionDeal)

		// 工作统计接口
		gv2.GET("/statistics/work", home.HomeStatistics)
		gv2.GET("/statistics/detail", home.HomeStatisticsDetail)
		//调研接口
		//数据搜索调研
		gv1.GET("/search_investigation", v1.GetInvestigationSearchList)
		//调研筛选
		gv2.GET("/filtered_investigation", v1.GetInvestigationFilteredList)

		//量化
		gv2.POST("/quantization/analyze", v1.Analyze)
		gv2.POST("/quantization/detail", v1.Detail)
		gv2.POST("/quantization/qa_analyze", v1.QAAnalyze)
		gv2.POST("/quantization/qa_detail", v1.QADetail)
		gv2.POST("/quantization/benchmark", v1.Benchmark)

		// 公募基金

		gv2.GET("/public_fund/scale", v1.FundScaleLists)
		gv2.GET("/public_fund/stock", v1.FundStockLists)
		gv2.GET("/public_fund/position", v1.FundPositionLists)
		gv2.GET("/public_fund/get_report_period", v1.FundReportPeriod)

		gv2.GET("/public_fund/manager", v1.FundManagerLists)
		gv2.GET("/public_fund/search_manager", v1.FundManagerSreach)
		gv2.GET("/public_fund/search_name", v1.PublicFundNameSreach)
		gv2.GET("/public_fund/fund_list", v1.FundLists)
		gv2.GET("/public_fund/stock_detail", v1.FundListsDetail)
		// 最新披露接口
		gv2.GET("/public_fund/new", v1.FundListsNew)

		// 第三期
		gv2.GET("/public_fund/fund_rank", v1.FundRankList)
		gv2.GET("/public_fund/fund_industry", v1.FundIndustryLists)
		gv2.GET("/public_fund/star_manager_list", v1.FundStartManager)
		//数据监控
		gv2.GET("/data_monitor_list", v1.DataMonitorList)

		//F10概念
		gv2.GET("/ften_concept_list", v1.NewConceptList)
		//F10概念 获取概念列表
		gv2.GET("/get_concept_list", v1.AllConceptList)

		//北向资金列表
		gv2.GET("/northward_list", v1.NorthWardList)
		//北向资金最新时间
		gv2.GET("/northward_latest_time", v1.NorthWardLatestTime)
		//行业
		//获取行业列表
		gv2.GET("/industry_list", v1.GetIndustryLIst)
		//获取行业详情
		gv2.GET("/industry_detail", v1.GetIndustryDetail)
		//编辑行业
		gv2.POST("/edit_industry", v1.EditIndustry)
		//添加行业
		gv2.POST("/add_industry", v1.AddIndustry)
		//行业删除
		gv2.POST("/industry_del", v1.IndustryDel)
		//行业删除股票
		gv2.POST("/industry_del_code", v1.IndustryDelCode)
		//行业添加股票
		gv2.POST("/industry_add_code", v1.IndustryAddCode)
		//行业设置龙头股票
		gv2.POST("/industry_set_head_code", v1.SetIndustryHeadCode)

		//舆情
		gv2.GET("/sentiment_edit", v1.SentimentEdit)
		gv2.GET("/sentiment_news", v1.SentimentNews)
		gv2.GET("/sentiment_correct", v1.SentimentCorrect)

		// 运营股池第二期
		gv2.GET("/research/stock_pool_manage", v1.ReasearchStockPoolManage)
		// 添加
		gv2.POST("/research/stock_pool_add", v1.ReasearchAddStockPool)
		// 删除
		gv2.POST("/research/stock_pool_del", v1.ReasearchDelStockPool)
		// 编辑
		gv2.POST("/research/stock_pool_edit", v1.ReasearchEditStockPool)
		// 编辑
		gv2.GET("/research/stock_pool_history", v1.ReasearchEditStockPoolHistory)

		// 纪要
		gv2.GET("/research/summary_list", v1.SummaryList)
		// 添加
		gv2.POST("/research/summary_add", v1.SummaryAdd)
		// 删除
		gv2.GET("/research/summary_del", v1.SummaryDel)
		// 编辑
		gv2.POST("/research/summary_edit", v1.SummaryEdit)

		// 基金
		// 基金列表
		gv2.GET("/fund_list", v1.FundList)
		// 添加
		gv2.POST("/fund_add", v1.FundAdd)
		// 删除
		gv2.GET("/fund_del", v1.FundDel)
		// 编辑
		gv2.POST("/fund_edit", v1.FundEdit)
		// 基金名称列表
		gv2.GET("/fund_names", v1.FundNamesList)

		// 基金用户
		gv2.GET("/customer_list", v1.ListCustomer)
		// 添加
		gv2.POST("/customer_add", v1.AddCustomer)
		// 删除
		gv2.GET("/customer_del", v1.DelCustomer)
		// 编辑
		gv2.POST("/customer_edit", v1.EditCustomer)
		// 基金用户列表
		gv2.GET("/customer_names", v1.CustomerNamesList)
		// 重置密码
		gv2.GET("/customer_reset_pwd", v1.CustomerResetPwd)

		// 用户份额
		gv2.GET("/fund_customer_list", v1.FundCustomerList)
		// 添加
		gv2.POST("/fund_customer_add", v1.FundCustomerAdd)
		// 删除
		gv2.GET("/fund_customer_del", v1.FundCustomerDel)
		// 编辑
		gv2.POST("/fund_customer_edit", v1.FundCustomerEdit)
		// 导入
		gv2.POST("/fund_customer_import", v1.FundCustomerImport)

		// 基金公告
		gv2.GET("/fund_notice_list", v1.FundNoticeList)
		// 添加
		gv2.POST("/fund_notice_add", v1.FundNoticeAdd)
		// 删除
		gv2.GET("/fund_notice_del", v1.FundNoticeDel)
		// 编辑
		gv2.POST("/fund_notice_edit", v1.FundNoticeEdit)

		// 分红公告
		gv2.GET("/fund_bonus_list", v1.FundBonusList)
		// 添加
		gv2.POST("/fund_bonus_add", v1.FundBonusAdd)
		// 删除
		gv2.GET("/fund_bonus_del", v1.FundBonusDel)
		// 编辑
		gv2.POST("/fund_bonus_edit", v1.FundBonusEdit)
		// 导入
		gv2.POST("/fund_bonus_import", v1.FundBonusImport)

		// 净值维护列表
		gv2.GET("/fund_tick_list", v1.FundTickList)
		// 添加
		gv2.POST("/fund_tick_add", v1.FundTickAdd)
		// 删除
		gv2.GET("/fund_tick_del", v1.FundTickDel)
		// 编辑
		gv2.POST("/fund_tick_edit", v1.FundTickEdit)

		//事件回测
		//分类查询
		gv2.GET("/market_class", v1.MarketClass)
		//名称查询
		gv2.GET("/market_category", v1.MarketCategory)
		//数据查询
		gv2.GET("/market_data", v1.MarketData)
		//详情查询
		gv2.GET("/market_detail", v1.MarketDetail)
		//修改影响股票
		gv2.POST("/market_stock", v1.EditMarketStock)
		//修改事件数据
		gv2.POST("/market_data", v1.EditMarketData)
		//研报事件详情
		gv2.GET("/market/research_detail", v1.ResearchDetail)
		//详情查询-大宗产品曲线图
		gv2.GET("/market_detail/product/value_list", v1.MarketDetailProductValueList)
		//详情查询-大宗产品回测成功率
		gv2.GET("/market_detail/product/stock_rate", v1.MarketDetailProductStockRate)
		// 市场事件
		gv2.GET("/events/industry_event_list", v1.IndustryEvent)
		// 市场事件
		gv2.GET("/events/calendar_event_list", v1.CalendarEvent)
		gv2.POST("/events/edit_stock", v1.EditIndustryStock)
	}

	gvDati := r.Group("/api/v3")
	{
		gvDati.POST("/login", v1.LoginDati)
		gvDati.GET("/wxappLogin", v1.AddWxUser)
		gvDati.GET("/dati_user",v1.GetWxUser)
		gvDati.GET("/info", v1.GetAdminUserInfo)
		//问题 admin
		gvDati.POST("/add_question", v1.AddQuestion)
		gvDati.PUT("/edit_question", v1.EditQuestion)
		gvDati.DELETE("/del_question", v1.DelQuestion)
		gvDati.GET("/questions", v1.GetQuestions)
		gvDati.GET("/rand_questions", v1.GetRandQuestions)
		gvDati.GET("/user_question_log", v1.GetQuestionLog)
		gvDati.GET("/question_log_detail", v1.GetQuestionLogDetail)
		gvDati.POST("/set_question_log", v1.SetQuestionLog)
		//gvDati.POST("/del_question_log", v1.DelQuestionLog)
		gvDati.POST("/import_questions", v1.ImportQuestions)
		//单位 admin
		gvDati.POST("/add_org", v1.AddOrganization)
		gvDati.PUT("/edit_org", v1.EditOrganization)
		gvDati.DELETE("/del_org", v1.DelOrganization)
		gvDati.GET("/organizations", v1.GetOrganizations)
		gvDati.GET("/organizations_all", v1.GetOrganizationsAll)
		gvDati.POST("/import_organization", v1.ImportOrganizations)
		gvDati.POST("/set_user_organization", v1.AddUserOrganization)


		gvDati.GET("/dati_users",v1.GetDatiUsers)
		gvDati.POST("/users/logout",v1.DatiAdminLogout)
		gvDati.POST("/upload",v1.UploadHandler)
		gvDati.GET("/kline",v1.ExportKLine)
	}

	//微信前端接口
	gFrontV1 := r.Group("/api/v2")
	{
		gFrontV1.POST("/login", v1.CustomerLogin)
		// 顾客修改密码
		gFrontV1.POST("/customer_edit_pwd", v1.CustomerEditPwd)
	}
	gFront := r.Group("/api/v2").Use(jwt.Customer())
	{
		// 顾客基金列表
		gFront.GET("/customer_fund_list", v1.FundListByUserId)
		// 顾客基金日线
		gFront.GET("/customer_fund_tick", v1.FundDetailByFundId)
		// 获取公告内容
		gFront.GET("/customer_fund_notice", v1.FundNoticeByFundId)
		// 获取分红公告
		gFront.GET("/customer_bonus_notice", v1.FundBonusNoticeById)
	}
	return r
}
