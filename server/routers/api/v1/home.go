package v1

import (
	home2 "datacenter/controller/home"
	"datacenter/models/home"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/crud_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func GetMyAnnouncementEventList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form home.AnnouncementEventQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	form.UserID, _ = strconv.Atoi(appG.C.GetHeader("UID"))
	data, cnt, err := home2.GetMyAnnouncementEventList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List:    data,
		Total:   cnt,
		PageNum: form.PageNum,
	})
}

func GetMyResearchEventList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form home.ResearchEventQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	form.UserID, _ = strconv.Atoi(appG.C.GetHeader("UID"))
	data, err := home2.GetMyResearchEventList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

func GetMyInvestigationList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form home.InvestigationUCQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	form.UserID, _ = strconv.Atoi(appG.C.GetHeader("UID"))
	data, err := home2.GetMyInvestigationList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

func GetNetProfitChangePushList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form home.NetProfitChangePushQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	form.UserId, _ = strconv.Atoi(appG.C.GetHeader("UID"))
	data, cnt, err := home2.GetNetProfitChangePushList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List:    data,
		Total:   cnt,
		PageNum: form.PageNum,
	})
}
