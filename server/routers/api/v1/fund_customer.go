package v1

import (
    "datacenter/models/fund"
    "datacenter/pkg/app"
    "datacenter/pkg/e"
    "datacenter/pkg/logging"
    "datacenter/pkg/util"
    "datacenter/service/fund_service"
    "github.com/gin-gonic/gin"
    "github.com/shopspring/decimal"
    "github.com/unknwon/com"
    "net/http"
    "time"
)

// @Summary 用户份额列表
// @Produce  json
// @Tags WebApi-用户份额
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param realname query int false "用户姓名"
// @Param fund_id query int false "基金id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_customer_list [get]
func FundCustomerList(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        form fund.CustomerFundRequest
        err  error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    customerService := fund_service.FundCustomer{
        PageNum:  form.PageNum,
        PageSize: form.PageSize,
        Realname: form.Realname,
        FundID: form.FundID,
    }
    result, err := customerService.GetLists()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_CUSTOMER_FAIL, nil)
        return
    }

    count, err := customerService.Count()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": result,
        "total": count,
    })
}

// @Summary 用户份额添加
// @Produce  json
// @Tags WebApi-用户份额
// @Param fund_id body int true "基金id"
// @Param user_id body int true "用户id"
// @Param num body string false "份额"
// @Param created_by body int false "CreatedBy"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_customer_add [post]
// @Tags Test
func FundCustomerAdd(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form fund.CustomerFundAddRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    _,err := decimal.NewFromString(form.Num)
    if err != nil {
        appG.Response(httpCode,  e.INVALID_PARAMS, nil)
        return
    }
    customerService := fund_service.FundCustomer{
        UserID: form.UserID,
        FundID: form.FundID,
        Num:    form.Num,
        Created: time.Now().Format(util.YMDHMS),
        Updated: time.Now().Format(util.YMDHMS),
    }
    code,err := checkParams(customerService)
    if err != nil {
        appG.Response(http.StatusInternalServerError, code, nil)
        return
    }
    exists,err := customerService.ExistByName()
    if err != nil {
        appG.Response(http.StatusInternalServerError, code, nil)
        return
    }
    if exists {
        appG.Response(http.StatusOK, e.ERROR_FUND_CUSTOMER_FAIL, nil)
        return
    }
    err = customerService.Add()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 用户份额删除
// @Produce  json
// @Tags WebApi-用户份额
// @Param id path int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_customer_del [get]
// @Tags Test
func FundCustomerDel(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form = fund.CustomerDelByIDRequest{ID: com.StrTo(c.Param("id")).MustInt()}
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    customerService := fund_service.FundCustomer{
        ID:         form.ID,
    }
    _, err := customerService.Exist()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_CUSTOMER_FAIL, nil)
        return
    }

    err = customerService.Del()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 用户份额编辑
// @Produce  json
// @Tags WebApi-用户份额
// @Param id path int true "ID"
// @Param fund_id body int true "基金id"
// @Param user_id body int true "用户id"
// @Param num body string false "份额"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_customer_edit [post]
// @Tags Test
func FundCustomerEdit(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form  fund.CustomerFundEditRequest
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    customerService := fund_service.FundCustomer{
        ID:              form.ID,
        UserID:          form.UserID,
        FundID:          form.FundID,
        Num:             form.Num,
    }

    code,err := checkParams(customerService)
    if err != nil {
        appG.Response(http.StatusOK, code, nil)
        return
    }
    exists,err := customerService.CheckFundCostmer()
    if err != nil {
        appG.Response(http.StatusOK, code, nil)
        return
    }
    if exists {
        appG.Response(http.StatusOK, e.ERROR_FUND_CUSTOMER_FAIL, nil)
        return
    }
    err = customerService.Edit()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}


// @Summary 用户份额导入
// @Produce  json
// @Tags WebApi-用户份额
// @Param file body string true "Excel File"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_customer_import [post]
// @Tags Test
func FundCustomerImport(c *gin.Context) {
    appG := app.Gin{C: c}
    file, _, err := c.Request.FormFile("file")
    var (
        failRaws  = 0
        successRaws  = 0
    )
    if err != nil {
        logging.Warn(err)
        appG.Response(http.StatusInternalServerError, e.ERROR, nil)
        return
    }

    customerService := fund_service.FundCustomer{}
    failRaws,successRaws,err = customerService.Import(file)
    if err != nil {
        logging.Warn(err)
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "failRaws": failRaws,
        "successRaws":successRaws,
    })
}


func checkParams(customerService fund_service.FundCustomer) (code int,err error) {
    checkName, numErr := customerService.ExistByName()
    if numErr != nil {
        return e.ERROR_FUND_CUSTOMER_EXISTS,numErr
    }
    if checkName {
        return e.ERROR_FUND_CUSTOMER_FAIL,numErr
    }
    checkFund,fundErr := customerService.ExistByFund()
    if fundErr != nil {
        return e.ERROR_FUND_NOT_EXISTS,fundErr
    }
    if !checkFund {
        return e.ERROR_FUND_EXISTS,fundErr
    }
    checkCustomer,customerErr := customerService.ExistByCustomer()
    if customerErr != nil {
        return e.ERROR_CUSTOMER_NOT_EXISTS,customerErr
    }
    if !checkCustomer {
        return e.ERROR_CUSTOMER_NOT_EXISTS,customerErr
    }
    return
}

// @Summary 基金列表
// @Produce  json
// @Tags WebApi-基金页面接口
// @Param user_id query int false "用户ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v2/customer_fund_list [get]
func FundListByUserId(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        err  error
        form = fund.FundListByUserId{UserID: com.StrTo(c.Param("id")).MustInt()}
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    fundService := fund_service.FundCustomer{
        UserID:         form.UserID,
    }
    result,registeDate, err := fundService.GetListsByUserID()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }
    noticeService := fund_service.FundNotice{}
    notice,err := noticeService.GetAllCustomerLists()
    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "funds":        result,
        "notice":       notice,
        "register_date":registeDate,
    })
}


// @Summary 基金日线列表
// @Produce  json
// @Tags WebApi-基金页面接口
// @Param user_id query int false "用户ID"
// @Param fund_id query int false "基金ID"
// @Param start query string false "开始时间"
// @Param end query string false "结束时间"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v2/customer_fund_tick [get]
func FundDetailByFundId(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        err  error
        form fund.FundTickCustomerRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    fundService := fund_service.FundCustomer{
        UserID:         form.UserID,
        FundID:         form.FundID,
    }
    result,registeDate, err := fundService.GetListsByUserID()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }
    noticeService := fund_service.FundNotice{
        FundID:form.FundID,
    }
    notice,noticeErr := noticeService.GetNoticeByFund()
    if noticeErr != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }
    tickService := fund_service.FundTick{
        Day: form.Day,
        FundID: form.FundID,
    }
    tick,tickErr := tickService.GetFundTickByFundID()
    if tickErr != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }
    bonusService := fund_service.FundBonus{
        FundID: form.FundID,
        UserID: form.UserID,
    }
    bonus,bonusErr := bonusService.GetFundNoticeContentByFundId()
    if bonusErr != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }
    maxUnitValue,minUnitValue := tickService.GetFundTickDateByFundID(tick)
    lastTick,lastTickErr := tickService.GetFundTickDateOne()
    if lastTickErr != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "funds":  result,
        "notice": notice,
        "tick":   tick,
        "bonus":  bonus,
        "register_date":registeDate,
        "max_unit_value":maxUnitValue,
        "min_unit_value":minUnitValue,
        "last_tick":lastTick,
    })
}

// @Summary 获取公告内容
// @Produce  json
// @Tags WebApi-基金页面接口
// @Param notice_id query int false "基金公告ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v2/customer_fund_notice [get]
func FundNoticeByFundId(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        err  error
        form fund.FundNoticeRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    fundNoticeService := fund_service.FundNotice{
        ID: form.NoticeID,
    }
    ret , err := fundNoticeService.ExistByID()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_NOT_EXISTS, nil)
        return
    }
    if !ret {
        appG.Response(http.StatusOK, e.ERROR_FUND_NOT_EXISTS, nil)
        return
    }
    notice,err := fundNoticeService.GetNoticeContentByFundID()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_FAIL, nil)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "notice": notice,
    })
}