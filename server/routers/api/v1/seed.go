package v1

import (
	"datacenter/models/seed"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

type SeedResponse struct {
	Data    []seed.SeedVO `json:"data"`
	Total   int           `json:"total"`
	PageNum int           `json:"page_num"`
}

// @Summary 添加新闻源
// @Tags WebApi-新闻源
// @Produce json
// @Param catch_site query string true "网站名称"
// @Param catch_path query string true "栏目名称"
// @Param catch_link query string true "抓取地址"
// @Param selector_page_link query string true "页面链接"
// @Param selector_list_time query string false "列表时间"
// @Param selector_list_title query string true "列表标题"
// @Param star query int true "星级"
// @Param cata query string true "分类"
// @Param catch_interval query int true "抓取频率"
// @Param catch_mode query string true "抓取模式"
// @Param show_in_home query int true "是否首页隐藏"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/save_seed [post]
func SaveSeed(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form seed.SeedIO
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    err := seed.InsertSeed(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_ADD_SEED_FAIL, err.Error())
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 获取新闻源
// @Tags WebApi-新闻源
// @Produce json
// @Param catch_site query string false "网站名称"
// @Param catch_path query string false "栏目名称"
// @Param star query int false "星级"
// @Param cata query string false "分类"
// @Param catch_mode query string false "抓取模式"
// @Param show_in_home query int false "是否首页隐藏"
// @Param status query string false "状态"
// @Param page_size query int true "每页条数"
// @Param page_num query int true "页码"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/get_seed [get]
func GetSeeds(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form seed.SeedQO
        vos []seed.SeedVO
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    data, cnt, err := seed.GetSeedList(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_QUERY_SEED_FAIL, err.Error())
        return
    }
    for _, v := range data {
        p := v.DTO2VO()
        vos = append(vos, *p)
    }
    ret := SeedResponse{
        Data:    vos,
        Total:   cnt,
        PageNum: form.PageNum,
    }
    appG.Response(http.StatusOK, e.SUCCESS, ret)
}

// @Summary 编辑新闻源
// @Tags WebApi-新闻源
// @Produce json
// @Param id query int true "id"
// @Param catch_site query string true "网站名称"
// @Param catch_path query string true "栏目名称"
// @Param catch_link query string true "抓取地址"
// @Param selector_page_link query string true "页面链接"
// @Param selector_list_time query string false "列表时间"
// @Param selector_list_title query string true "列表标题"
// @Param star query int true "星级"
// @Param cata query string true "分类"
// @Param catch_interval query int true "抓取频率"
// @Param catch_mode query string true "抓取模式"
// @Param show_in_home query int true "是否首页隐藏"
// @Param status query string true "状态"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_seed [post]
func EditSeed(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form seed.SeedEO
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    err := seed.EditSeed(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_EDIT_SEED_FAIL, err.Error())
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 删除新闻源
// @Tags WebApi-新闻源
// @Produce json
// @Param id query int true "id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/del_seed [post]
func DelSeed(c *gin.Context) {
	type delSeed struct {
		Id int `json:"id" form:"id" binding:"required,gt=0"`
	}
	var (
		appG = app.Gin{C: c}
		id   = delSeed{}
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &id)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := seed.DelSeed(id.Id)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 查询新闻源分类
// @Tags WebApi-新闻源
// @Produce json
// @Param value query string false "搜索内容"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/get_seed_cata [get]
func GetSeedCataList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = seed.SeedCataQO{}
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := seed.SearchSeedCata(form)
	var ret = make([]*seed.SeedCataVO, 0)
	for _, v := range data {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_DELETE_SEED_FAIL, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, ret)
}
