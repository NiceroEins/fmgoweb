package v1

import (
	"datacenter/controller/market"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 查询分类
// @Tags WebApi
// @Produce json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/market_class [get]
func MarketClass(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	resp, err := market.ListClass()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, resp)
}

// @Summary 查询其他相关数据
// @Tags WebApi
// @Produce json
// @Param category_id query int true "必填，分类id（不是数据id）"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/market_category [get]
func MarketCategory(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req market.CategoryReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	resp, err := market.QueryCategory(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, resp)
}

// @Summary 事件回测
// @Tags WebApi
// @Produce json
// @Param name query string false "事件名称"
// @Param class_id query int false "分类id"
// @Param sort query string false "排序字段"
// @Param order query string false "顺序,asc/desc"
// @Param p query int false "页码"
// @Param n query int false "单页条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/market_data [get]
func MarketData(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req market.DataReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	resp, err := market.QueryData(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, resp)
}

// @Summary 事件回测详情
// @Tags WebApi
// @Produce json
// @Param category_id query int false "数据项category_id"
// @Param begin query string false "开始时间"
// @Param end query string false "结束时间"
// @Param sort query string false "排序字段"
// @Param order query string false "顺序,asc/desc"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/market_detail [get]
func MarketDetail(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req market.DetailReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	resp, err := market.QueryDetail(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, resp)
}

func MarketDetailProductValueList(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req market.ProductReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	res, err := market.QueryProductValueList(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, res)
}

func MarketDetailProductStockRate(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req market.ProductReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	res, err := market.QueryProductStockRate(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, res)
}

// @Summary 编辑影响股票
// @Tags WebApi
// @Produce json
// @Param category_id body int true "数据项category_id"
// @Param data body array true "影响股票数组(code[必填]/name[选填])"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/market_stock [post]
func EditMarketStock(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req market.EditStockReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}
	err := market.EditStock(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 修改事件数据
// @Tags WebApi
// @Produce json
// @Param id body int true "数据id"
// @Param value body number  true "影响股票数组(code[必填]/name[选填])"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/market_data [post]
func EditMarketData(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req market.EditDataReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}
	err := market.EditData(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 研报事件行情
// @Tags WebApi
// @Produce json
// @Param organization query string false "机构名称"
// @Param begin query string false "开始时间"
// @Param end query string false "结束时间"
// @Param sort query string false "排序字段"
// @Param order query string false "顺序,asc/desc"
// @Param p query int false "页码"
// @Param n query int false "单页条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/market/research_detail [get]
func ResearchDetail(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req market.ResearchReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	resp, err := market.QueryResearchDetail(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, resp)
}
