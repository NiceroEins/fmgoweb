package v1

import (
	Controller "datacenter/controller/public_fund"
	"datacenter/models/public_fund"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 规模图
// @Tags WebApi-公募基金
// @Produce  json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/scale [get]
func FundScaleLists(c *gin.Context) {
	appG := app.Gin{C: c}
	ret,err := Controller.GetScaleList()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, ret)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, ret)
}

// @Summary 规模列表
// @Tags WebApi-公募基金
// @Produce  json
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param sort_key query string false "排序字段名"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/stock [get]
func FundStockLists(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundHistoryReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,cnt,err := Controller.GetStockList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
		"total": cnt,
	})
}

// @Summary 基金持仓
// @Tags WebApi-公募基金
// @Produce  json
// @Param page_num query int true "页码"
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param sort_key query string false "排序字段名"
// @Param code query string false "股票ID"
// @Param report_date query string true "报告期"
// @Param is_new query string true "是否新股"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/position [get]
func FundPositionLists(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundPositionReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,cnt,err := Controller.GetPositionList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
		"total": cnt,
	})
}


// @Summary 基金经理列表
// @Tags WebApi-公募基金2期
// @Produce  json
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param sort_key query string false "排序字段名"
// @Param manager_id query int true "基金经理人"
// @Param report_date query string true "报告期"
// @Param is_new query string true "是否新股"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/manager [get]
func FundManagerLists(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundManagerReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,cnt,err := Controller.GetManagerData(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	head,err := Controller.GetManagerHeadData(&form)
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
		"total": cnt,
		"head":head,
	})
}

// @Summary 基金经理查询
// @Tags WebApi-公募基金2期
// @Produce  json
// @Param manager_name query string true "基金经理人"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/search_manager [get]
func FundManagerSreach(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundManagerSearchReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,err := Controller.GetManagerNameSreach(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
	})
}

// @Summary 基金名称查询
// @Tags WebApi-公募基金2期
// @Produce  json
// @Param manager_name query string true "基金名称查询"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/search_name [get]
func PublicFundNameSreach(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundNameSearchReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,err := Controller.GetFundNameSreach(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
	})
}

// @Summary 基金页面
// @Tags WebApi-公募基金2期
// @Produce  json
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param sort_key query string false "排序字段名"
// @Param fund_code query string true "基金代码"
// @Param report_date query string true "报告期"
// @Param is_new query string true "是否新股"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/fund_list [get]
func FundLists(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,cnt,err := Controller.GetFundList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	head,err := Controller.GetFundScale(&form)
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
		"total": cnt,
		"head":head,
	})
}

// @Summary 基金持仓详情
// @Tags WebApi-公募基金2期
// @Produce  json
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param sort_key query string false "排序字段名"
// @Param stock_code query string true "股票代码"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/stock_detail [get]
func FundListsDetail(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundStockDetailReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,cnt,err := Controller.GetStockFundDetail(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
		"total": cnt,
	})
}

// @Summary 规模列表
// @Tags WebApi-公募基金
// @Produce  json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/get_report_period [get]
func FundReportPeriod(c *gin.Context) {
	appG := app.Gin{C: c}
	ret,err := Controller.GetReportPeriod()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
	})
}

// @Summary 基金行业配置
// @Tags WebApi-公募基金3期
// @Produce  json
// @Param sort_key query string false "排序字段名"
// @Param report_date query string true "报告期"
// @Param fund_code query string true "基金代码"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/fund_industry [get]
func FundIndustryLists(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundIndustryReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,err := Controller.GetFundIndustry(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
	})
}
// @Summary 基金最新披露
// @Tags WebApi-公募基金2期
// @Produce  json
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param sort_key query string false "排序字段名"
// @Param report_date query string true "报告期"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/new [get]
func FundListsNew(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundNewReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,err := Controller.GetFundNew(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	cnt,err := Controller.GetFundNewCount(&form)
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
		"total": cnt,

	})
}
// @Summary 基金排行榜
// @Tags WebApi-公募基金3期
// @Produce  json
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param report_date query string true "报告期"
// @Param sort_key query string false "排序字段名"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/fund_rank [get]
func FundRankList(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form public_fund.FundRankReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	ret,cnt,err := Controller.GetFundRank(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
		"total": cnt,
	})
}


// @Summary 明星经理人列表
// @Tags WebApi-公募基金2期
// @Produce  json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/public_fund/star_manager_list [get]
func FundStartManager(c *gin.Context) {
	appG := app.Gin{C: c}
	ret,err := Controller.GetStarManagerList()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
	})
}