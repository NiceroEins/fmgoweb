package v1

import (
    "datacenter/models/fund"
    "datacenter/pkg/app"
    "datacenter/pkg/e"
    "datacenter/pkg/util"
    "datacenter/service/fund_service"
    "github.com/gin-gonic/gin"
    "github.com/unknwon/com"
    "net/http"
    "time"
)

// @Summary 基金列表
// @Produce  json
// @Tags WebApi-基金列表
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_list [get]
func FundList(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        err  error
    )
    fundService := fund_service.Fund{}
    result, err := fundService.GetLists()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": result,
    })
}

// @Summary 基金列表添加
// @Produce  json
// @Tags WebApi-基金列表
// @Param name body string true "基金名称"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_add [post]
// @Tags Test
func FundAdd(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form fund.FundAddRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    fundService := fund_service.Fund{
        Name:            form.Name,
        Created:         time.Now().Format(util.YMDHMS),
    }

    checkName, numErr := fundService.ExistByName()
    if numErr != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }
    if checkName {
        appG.Response(http.StatusOK, e.ERROR_FUND_DUPICATION, nil)
        return
    }
    err := fundService.AddName()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 基金列表删除
// @Produce  json
// @Tags WebApi-基金列表
// @Param id path int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_del [get]
// @Tags Test
func FundDel(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form = fund.FundDelByIDRequest{ID: com.StrTo(c.Param("id")).MustInt()}
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    fundService := fund_service.Fund{
        ID:         form.ID,
    }

    _, err := fundService.ExistByID()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_FAIL, nil)
        return
    }

    err = fundService.Del()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 基金列表编辑
// @Produce  json
// @Tags WebApi-基金列表
// @Param id path int true "ID"
// @Param name body int true "基金名称"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_edit [post]
// @Tags Test
func FundEdit(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form  fund.FundEditRequest
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    fundService := fund_service.Fund{
        ID: form.ID,
        Name:form.Name,
    }

    exists, err := fundService.ExistByID()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }

    if !exists {
        appG.Response(http.StatusOK, e.ERROR_FUND_EXISTS, nil)
        return
    }
    checkName, numErr := fundService.ExistByName()
    if numErr != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
        return
    }
    if checkName {
        appG.Response(http.StatusOK, e.ERROR_FUND_EDIT, nil)
        return
    }
    err = fundService.Edit()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_FUND_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 基金名称列表
// @Produce  json
// @Tags WebApi-基金列表
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_names [get]
func FundNamesList(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        err  error
    )
    fundService := fund_service.Fund{}
    result, err := fundService.GetNames()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": result,
    })
}
