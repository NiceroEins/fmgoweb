package v1

import (
	performancec "datacenter/controller/performance"
	"datacenter/models/performance"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/crud_service"
	"datacenter/service/excel_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// @Summary 行业跟踪:搜索
// @Tags WebApi-业绩
// @Produce json
// @Param sort_id query int false "排序季度 范围1-4"
// @Param direction query bool false "升降序 默认false"
// @Param sort_key query string false "默认为空 值是对应字段名 eg:net_profit"
// @Param search query string false "搜索条件,输入内容后端会自动识别,eg:000001"
// @Param report_period query string true "报告期 eg:2020-06-30"
// @Param search query string false "股票搜索"
// @Param search_industry query string false "搜索行业"
// @Param forecast_days query int true "预测天数"
// @Param announce_days query int true "公告天数"
// @Param user_id query string false "用户id,eg:1"
// @Param page_num query int true "页码,eg:1"
// @Param page_size query int true "每页展示条数,eg:10"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/search_industry_track [get]
func PerformanceIndustryTrack(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form performance.PrformanceQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := performancec.GetPerformanceIndustryTrack(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{List: data, Total: cnt, PageNum: form.PageNum})
}

// @Summary 业绩计算页面
// @Tags WebApi-业绩
// @Produce json
// @Param report_period query string true "报告期,eg:2020-06-09"
// @Param search query string false "搜索股票"
// @Param search_industry query string false "搜索行业"
// @Param type query string false "类型(下拉框),全部:空字符串,妖股:1,业绩增长:2"
// @Param important query string false "重要程度,全部:空字符串,非常重要:1,重要:2,不重要:3"
// @Param user_id query int false "用户名(此字段传递用户id,eg:1),全部:空字符串"
// @Param recommend query string false "是否推荐,是:1,否:2"
// @Param disclosure_start_time query string false "披露开始时间,eg:2020-06-08"
// @Param disclosure_end_time query string false "披露结束时间,eg:2020-06-10"
// @Param sort_key query int false "排序字段 默认0 范围0-22"
// @Param direction query bool false "升降序 默认false"
// @Param amount_size query int false "成交额规模 默认0 1：5亿以上 2:2-5亿 3:2亿以下"
// @Param page_num query int true "页码,eg:1"
// @Param page_size query int true "每页展示条数,eg:10"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/performance [get]
func PerformanceCalculate(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form performance.CaculateQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := performancec.PerformanceCalculate(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{List: data, Total: cnt, PageNum: form.PageNum})
}

// @Summary 业绩编辑修改(post):更新数据
// @Tags WebApi-业绩
// @Produce json
// @Param code query string true "股票代码,eg:000001"
// @Param type query string false "类型,前端为多选,所以:1 妖股,2 业绩增长,提交格式为:1,2"
// @Param important query int false "重要程度:1 非常重要， 2重要 3 不重要"
// @Param recommend query int false "是否推荐:  1 是, 2 否"
// @Param forecast_proportion query string true "预测比例"
// @Param forecast_net_profit query string true "预测净利润"
// @Param broker_proportion query string true "券商预测"
// @Param broker_interpretation query string true "券商解读"
// @Param high_lights query string true "亮点"
// @Param report_period query string true "报告期,eg:2020-06-30"
// @Param board query int false "板块 0-全部 1-创业板 2-中小板 3-科创板 4-沪市主板  5-深市主板"
// @Param hurry query int false "是否加急 1 否 2 是"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/post_edit_performance [post]
func EditPerformanceCalculate(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form performance.PerformanceEditIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := performancec.EditPerformanceEdit(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 导入券商预测
// @Produce  json
// @Tags WebApi-业绩
// @Param file formData file true "excel文件"
// @Param report_period formData string true "报告期"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/import_performance_edit [post]
func ImportPerformanceEdit(c *gin.Context) {
	var (
		appG     = app.Gin{C: c}
		fileType []string
	)
	file, header, err := appG.C.Request.FormFile("file")
	if header != nil {
		fileType = strings.Split(header.Filename, ".")
	}
	if fileType == nil || (fileType[len(fileType)-1] != "xlsx" && fileType[len(fileType)-1] != "xls") {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, "非法文件类型")
		return
	}
	reportPeriod := appG.C.PostForm("report_period")
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, err.Error())
		return
	}
	uid := appG.C.GetHeader("UID")
	total, data, err := performancec.ImportPerformanceEdit(file, uid, reportPeriod)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"total":     total,
		"fail":      len(data),
		"fail_list": data,
	})
}

// @Summary 业绩计算获取用户信息接口
// @Tags WebApi-业绩
// @Produce json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/get_performance_user_info [get]
func PerformanceGetUserInfo(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
	)
	data, err := performancec.GetUserInfo()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

// @Summary 业绩计算导出
// @Produce  json
// @Tags WebApi-业绩
// @Param report_period query string true "报告期,eg:2020-06-09"
// @Param search query string false "搜索股票"
// @Param search_industry query string false "搜索行业"
// @Param type query string false "类型(下拉框),全部:空字符串,妖股:1,业绩增长:2"
// @Param important query string false "重要程度,全部:空字符串,3 非常重要 2重要 1 不重要"
// @Param user_id query int false "用户名(此字段传递用户id,eg:1),全部:空字符串"
// @Param recommend query string false "是否推荐,是:1,否:2"
// @Param disclosure_start_time query string false "披露开始时间,eg:2020-06-08"
// @Param disclosure_end_time query string false "披露结束时间,eg:2020-06-10"
// @Param sort_key query int false "排序字段 默认0 范围0-10"
// @Param direction query bool false "升降序 默认false"
// @Param board query int false "板块 0-全部 1-创业板 2-中小板 3-科创板 4-沪市主板  5-深市主板"
// @Param hurry query int false "是否加急 1 否 2 是"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/performance_export [get]
func ExportPerformanceCalculate(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form performance.CaculateEPQO
		vos  []interface{}
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := performancec.PerformanceCalculateAll(&form)
	for _, v := range data {
		p := v.(performance.CalculateDTO).DTO2EPO()
		vos = append(vos, *p)
	}
	title := []string{"股票代码", "股票名称", "披露时间", "预测比例", "预测净利润", "支撑业绩逻辑", "本期预告", "券商预测", "券商解读", "机器预测", "机器预测规则"}
	err = excel_service.HandlerDownload(c, "业绩计算", vos, title)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

type industrySearchReq struct {
	Search string `json:"search" form:"search"`
}

// @Summary 业绩获取行业信息接口
// @Tags WebApi-业绩
// @Produce json
// @Param search query int false "搜索内容"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/get_industry_list [get]
func PerformanceGetIndustry(c *gin.Context) {
	var (
		appG   = app.Gin{C: c}
		search industrySearchReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &search)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := performancec.GetPerformanceIndustry(search.Search)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

type editRelationShipReq struct {
	ID         int    `json:"id" form:"id" binding:"gt=0"`
	IndustryId int    `json:"industry_id" form:"industry_id" binding:"gt=0"`
	UserId     string `json:"user_id" form:"user_id"`
}

// @Summary 业绩股票维护编辑接口
// @Tags WebApi-业绩
// @Produce json
// @Param id query int true "id"
// @Param industry_id query int true "行业id"
// @Param user_id query int false "用户id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_stock_maintain [post]
func EditRelationShip(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  editRelationShipReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := performancec.EditRelationShip(req.ID, req.IndustryId, req.UserId)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

type addRelationShipReq struct {
	StockId    string `json:"stock_id" form:"stock_id" binding:"required"`
	IndustryId int    `json:"industry_id" form:"industry_id" binding:"gt=0"`
	UserId     string `json:"user_id" form:"user_id"`
}

// @Summary 业绩股票维护添加接口
// @Tags WebApi-业绩
// @Produce json
// @Param id query int true "id"
// @Param stock_id query string true "股票code"
// @Param industry_id query int true "行业id"
// @Param user_id query int false "用户id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_maintain [post]
func AddRelationShip(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  addRelationShipReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := performancec.AddRelationShip(req.IndustryId, req.StockId, req.UserId)
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 业绩趋势
// @Tags WebApi-业绩
// @Produce json
// @Param industry_search query string false "行业搜索"
// @Param report_period query string true "报告期,eg:2020-06-09"
// @Param sort_key query int false "排序字段 默认0不排序"
// @Param direction query bool false "升降序 默认false"
// @Param page_num query int true "页码,eg:1"
// @Param page_size query int true "每页展示条数,eg:10"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/performance_trend [get]
func GetPerformanceTrend(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.TrendQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := performancec.GetPerformanceTrend(req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	updateTime, err := performancec.GetPerformanceTrendUpdateTime()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"list":        data,
		"total":       cnt,
		"page_num":    req.PageNum,
		"update_time": updateTime,
	})
}

// @Summary 业绩预告统计
// @Tags WebApi-业绩
// @Produce json
// @Param search query string false "搜索"
// @Param report_period query string true "报告期,eg:2020-06-09"
// @Param sort_key query int false "排序字段 默认0不排序"
// @Param direction query bool false "升降序 默认false"
// @Param page_num query int true "页码,eg:1"
// @Param page_size query int true "每页展示条数,eg:10"
// @Param published query bool false "今年是否公布 true：是 false：否 全部：不传此字段"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/performance_forecast_statistics [get]
func GetForecastStatistics(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.ForecastStatisticsQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := performancec.GetForecastStatistics(req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	updateTime, err := performancec.GetForecastUpdateTime()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"list":        data,
		"total":       cnt,
		"page_num":    req.PageNum,
		"update_time": updateTime,
	})
}

func GetTrace(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.TraceQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := performancec.GetTrace(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List:    data,
		PageNum: req.PageNum,
		Total:   cnt,
	})
}

func GetHighLightsHistory(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.HighLightQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := performancec.GetPerformanceHighLightsHistory(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List:    data,
		PageNum: req.PageNum,
		Total:   cnt,
	})
}

func GetReportPeriodList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
	)
	data := performancec.GetReportPeriodList()
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

func GetIndustryLIst(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.IndustryUserQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := performancec.GetIndustryList(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List:    data,
		PageNum: req.PageNum,
		Total:   cnt,
	})
}

func GetIndustryDetail(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.IndustryDetailQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := performancec.GetIndustryDetail(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List:    data,
		PageNum: req.PageNum,
		Total:   cnt,
	})
}

func EditIndustry(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.IndustryEO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := performancec.EditIndustry(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func AddIndustry(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.IndustryIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := performancec.AddIndustry(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func IndustryAddCode(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.IndustryCodeAdder
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := performancec.IndustryAddCode(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func IndustryDel(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.IndustryDelQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := performancec.DelIndustry(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func IndustryDelCode(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.IndustryDelCodeQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := performancec.IndustryDelCode(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func SetIndustryHeadCode(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		req  performance.CodeEO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := performancec.SetIndustryHeadCode(&req)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}