package v1

import (
	"datacenter/models"
	"datacenter/models/calendar"
	"datacenter/models/template"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/calendar_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

// @Summary 日历列表
// @Tags WebApi-日历
// @Produce  json
// @Param page_size query int true "每页条数"
// @Param page_num query int true "页数"
// @Param type query int true "类型 1为事件类型 2为股票类型 0 为全部"
// @Param sort query int true "类型 1：创建时间降序 2：创建时间升序 默认：发布时间降序"
// @Param keyword query string true "搜索关键词"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/calendar_list [get]
func CalendarLists(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form calendar.CalendarListRequest
		err  error
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	calendarService := calendar_service.Calendar{
		PageNum:  form.PageNum,
		PageSize: form.PageSize,
		Type:     form.Type,
		Keyword:  form.Keyword,
		Sort:     form.Sort,
	}
	calendars, err := calendarService.GetCalendarLists()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_FAIL, nil)
		return
	}

	count, err := calendarService.Count()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": calendars,
		"total": count,
	})
}

// @Summary 添加日历
// @Produce  json
// @Tags WebApi-日历
// @Param publish_date body string true "展示时间"
// @Param event_date body string true "事件时间"
// @Param is_display body int false "模糊时间 1为展示 2为不展示"
// @Param type body int true "日历类型 1为事件类型 2为股票类型"
// @Param content body string true "内容"
// @Param star body int false "星级"
// @Param detail body string false "详情"
// @Param link body string false "链接"
// @Param name body string false "概念名称"
// @Param code body string false "股票代码"
// @Param user_id body string true "发布者"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/calendar_add [post]
func CalendarAdd(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form calendar.CalendarEventRequest
		user template.BUser
	)

	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := models.DB().Table("b_user").Unscoped().Where("id = ?", form.Creator).Find(&user).Error
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
		return
	}
	if user.ID < 1 {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
		return
	}
	_, err = time.Parse("2006-01-02 15:04:05", form.PublishDate)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_ADD_FAIL, nil)
		return
	}
	_, err = time.Parse("2006-01-02 15:04:05", form.EventDate)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_ADD_FAIL, nil)
		return
	}
	calendarService := calendar_service.Calendar{
		Name:        form.Name,
		Created:     time.Now().Format("2006-01-02 15:04:05"),
		PublishDate: form.PublishDate,
		EventDate:   form.EventDate,
		Content:     form.Content,
		Star:        form.Star,
		Link:        form.Link,
		Code:        form.Code,
		Detail:      form.Detail,
		Creator:     form.Creator,
		Type:        form.Type,
		IsDisplay:   form.IsDisplay,
		ManyMonths:  form.ManyMonths,
		ManyWeeks:   form.ManyWeeks,
		AddEvent: form.IsAddEvent,
	}

	err = calendarService.Add()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_ADD_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 编辑日历
// @Produce  json
// @Tags WebApi-日历
// @Param id path int true "ID"
// @Param publish_date body string true "展示时间"
// @Param event_date body string true "事件时间"
// @Param is_display body int false "模糊时间 1为展示 2为不展示"
// @Param type body int true "日历类型 1为事件类型 2为股票类型"
// @Param content body string true "内容"
// @Param star body int false "星级"
// @Param detail body string false "详情"
// @Param link body string false "链接"
// @Param name body string false "概念名称"
// @Param code body string false "股票代码"
// @Param user_id body string true "发布者"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/calendar_edit [post]
func CalendarEdit(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form calendar.CalendarEditRequest
		user template.BUser
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := models.DB().Table("b_user").Unscoped().Where("id = ?", form.Creator).Find(&user).Error
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
		return
	}
	if user.ID < 1 {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
		return
	}
	_, err = time.Parse("2006-01-02 15:04:05", form.PublishDate)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_ADD_FAIL, nil)
		return
	}
	_, err = time.Parse("2006-01-02 15:04:05", form.EventDate)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_ADD_FAIL, nil)
		return
	}
	calendarService := calendar_service.Calendar{
		ID:          form.ID,
		Name:        form.Name,
		Created:     time.Now().Format("2006-01-02 15:04:05"),
		PublishDate: form.PublishDate,
		EventDate:   form.EventDate,
		Content:     form.Content,
		Star:        form.Star,
		Link:        form.Link,
		Code:        form.Code,
		Detail:      form.Detail,
		Creator:     form.Creator,
		Type:        form.Type,
		IsDisplay:   form.IsDisplay,
		AddEvent:    form.IsAddEvent,
	}

	exists, err := calendarService.ExistCalendarByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_CALNEDAR_EDIT_FAIL, nil)
		return
	}

	if !exists {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_NOT_EXIST, nil)
		return
	}

	err = calendarService.Edit()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_CALNEDAR_EDIT_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 删除日历
// @Produce  json
// @Tags WebApi-日历
// @Param id query int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/calendar_del [get]
func CalendarDel(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form calendar.CalendarDeleteRequest
	)

	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	calendarService := calendar_service.Calendar{ID: form.ID}
	exists, err := calendarService.ExistCalendarByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_CALNEDAR_EDIT_FAIL, nil)
		return
	}
	if !exists {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_NOT_EXIST, nil)
		return
	}
	if err := calendarService.Delete(); err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DELETE_TAG_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 模糊列表
// @Produce  json
// @Tags WebApi-日历
// @Param type query int true "日历类型 1为事件类型 2为股票类型"
// @Param search_date query string true "搜索日期"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/calendar_event [get]
func CalendarListByMonth(c *gin.Context) {
	var (
		appG       = app.Gin{C: c}
		form       calendar.CalendarMonthRequest
		searchTime time.Time
		end        = ""
		start      = ""
		err        error
	)

	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	if form.SearchDate != "" {
		searchTime, err = time.Parse("2006-01-02", form.SearchDate)
		if err != nil {
			appG.Response(http.StatusOK, e.ERROR_CALENDAR_FAIL, nil)
			return
		}
		end = searchTime.AddDate(0, 1, 0).String()
		start = searchTime.String()
	}
	calendarService := calendar_service.Calendar{
		Type:      form.Type,
		IsDisplay: calendar_service.IS_DISPLAY,
		Start:     start,
		End:       end,
	}
	calendars, err := calendarService.GetCalendarMonthLists()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": calendars,
	})
}

// @Summary 事件列表
// @Produce  json
// @Tags WebApi-日历
// @Param type query int true "日历类型 1为事件类型 2为股票类型"
// @Param search_date query string true "搜索日期"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/calendar_month [get]
func CalendarByMonth(c *gin.Context) {
	var (
		appG       = app.Gin{C: c}
		form       calendar.CalendarMonthRequest
		searchTime time.Time
		end        = ""
		start      = ""
		err        error
	)

	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	if form.SearchDate != "" {
		searchTime, err = time.Parse("2006-01-02", form.SearchDate)
		if err != nil {
			appG.Response(http.StatusOK, e.ERROR_CALENDAR_FAIL, nil)
			return
		}
		end = searchTime.AddDate(0, 1, 0).String()
		start = searchTime.String()
	}
	calendarService := calendar_service.Calendar{
		Type:      form.Type,
		IsDisplay: calendar_service.NOT_DISPLAY,
		Start:     start,
		End:       end,
	}
	calendars, err := calendarService.GetCalendarMonthLists()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_FAIL, nil)
		return
	}
	lists := calendar_service.AssbleCalendarList(calendars)
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": lists,
	})
}
