package v1

import (
	//"datacenter/models/con"
	"datacenter/models/ften"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

func NewConceptList(c *gin.Context) {
	appG := app.Gin{}
	appG.C = c
	var form ften.NewConceptListRequest
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, count := ften.GetNewConceptList(form)
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": data,
		"total": count,
	})
}

func AllConceptList(c *gin.Context) {
	appG := app.Gin{}
	appG.C = c
	var form ften.AllConceptRequest
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, count := ften.GetAllConceptList(form)
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": data,
		"total": count,
	})
}
