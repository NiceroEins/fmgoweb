package v1

import (
	"datacenter/models/changelog"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/changelog_service"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 日志列表
// @Tags WebApi-版本更新日志
// @Produce  json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/changelog_list [get]
func ChangelogLists(c *gin.Context) {
	appG := app.Gin{C: c}
	changelogService := changelog_service.ChangeLog{}
	changelogs, err := changelogService.GetChangelogLists()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CHANGELOG_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": changelogs,
	})
}

// @Summary 添加版本更新日志
// @Produce  json
// @Tags WebApi-版本更新日志
// @Param user_id body string true "用户ID"
// @Param content body string true "内容"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/changelog_add [post]
func ChangelogAdd(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form changelog.ChangeLogAddRequest
	)

	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	changelogService := changelog_service.ChangeLog{
		Content:         form.Content,
		UserID:          form.UserID,
	}

	err := changelogService.Add()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CHANGELOG_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 编辑日志
// @Produce  json
// @Tags WebApi-版本更新日志
// @Param id path int true "ID"
// @Param content body string true "内容"
// @Param user_id body string true "用户ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/changelog_edit [post]
func ChangelogEdit(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form changelog.CalendarEditRequest
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	changelogService := changelog_service.ChangeLog{
		ID: form.ID,
		Content:         form.Content,
		UserID:          form.UserID,
	}

	exists, err := changelogService.ExistChangelogByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_CHANGELOG_FAIL, nil)
		return
	}

	if !exists {
		appG.Response(http.StatusOK, e.ERROR_CHANGELOG_NOT_EXIST, nil)
		return
	}

	err = changelogService.Edit()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_CALNEDAR_EDIT_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 删除日志
// @Produce  json
// @Tags WebApi-版本更新日志
// @Param id query int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/changelog_del [get]
func ChangelogDel(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form changelog.CalendarDeleteRequest
	)

	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	changelogService := changelog_service.ChangeLog{ID: form.ID}
	exists, err := changelogService.ExistChangelogByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_CHANGELOG_FAIL, nil)
		return
	}
	if !exists {
		appG.Response(http.StatusOK, e.ERROR_CHANGELOG_NOT_EXIST, nil)
		return
	}
	if err := changelogService.Delete(); err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_CHANGELOG_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}