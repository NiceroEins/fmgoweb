package v1

import (
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/pkg/logging"
	"datacenter/pkg/oss"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
)

// @Summary 文件服务-oss回调
// @Tags FileService
// @Produce json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fs [post]
func FsCallback(c *gin.Context) {
	appG := app.Gin{C: c}
	// Get PublicKey bytes
	bytePublicKey, err := oss.GetPublicKey(c.Request)
	if err != nil {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err.Error())
		return
	}

	// Get Authorization bytes : decode from Base64String
	byteAuthorization, err := oss.GetAuthorization(c.Request)
	if err != nil {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err.Error())
		return
	}

	// Get MD5 bytes from Newly Constructed Authorization String.
	byteMD5, err := oss.GetMD5FromNewAuthString(c.Request)
	if err != nil {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err.Error())
		return
	}

	// VerifySignature and response to client
	if !oss.VerifySignature(bytePublicKey, byteMD5, byteAuthorization) {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err.Error())
		return
	}

	// do something you want according to callback_body ...
	bodyContent, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err.Error())
		return
	}
	defer func() {
		_ = c.Request.Body.Close()
	}()

	//r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyContent))
	body := make(map[string]interface{})
	err = json.Unmarshal(bodyContent, &body)
	if err != nil {
		logging.Error("system.fsCallback Errors: ", err.Error())
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, body)
}

// @Summary 文件服务-获取上传token
// @Tags FileService
// @Produce json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fs [get]
func GetPolicyToken(c *gin.Context) {
	appG := app.Gin{C: c}
	response := oss.GetPolicyToken()
	appG.Response(http.StatusOK, e.SUCCESS, response)
}
