package v1

import (
	"datacenter/controller/relpay"
	"datacenter/models/replay"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"strings"
)

// @Summary 我的复盘
// @Produce  json
// @Tags WebApi
// @Param datetime query string true "日期"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/replay_list [get]
func GetTotalReplayList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form replay.ReplayQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, _, err := relpay.GetTotalReplayList(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

// @Summary 我的复盘
// @Produce  json
// @Tags WebApi
// @Param code query string false "股票搜索"
// @Param reason query string false "涨停原因搜索"
// @Param highlight query string false "亮点搜索"
// @Param type query string false "类型搜索"
// @Param datetime query string true "日期"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/my_replay_list [get]
func GetMyReplayList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form replay.ReplayQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	form.Adder, _ = strconv.Atoi(appG.C.GetHeader("UID"))
	data, _, err := relpay.GetMyReplayList(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

// @Summary 添加我的复盘
// @Produce  json
// @Tags WebApi
// @Param code query string true "股票code"
// @Param reason query string false "涨停原因"
// @Param highlight query string false "亮点"
// @Param type query string true "类型"
// @Param datetime query string true "日期"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/add_my_replay [post]
func CreateMyReplay(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form replay.ReplayIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	form.Adder, _ = strconv.Atoi(appG.C.GetHeader("UID"))
	err := relpay.CreateMyReplay(form)
	if err != nil {
		if strings.Contains(err.Error(), "Error 1062: Duplicate entry") {
			appG.Response(http.StatusOK, e.INVALID_PARAMS, "数据重复")
			return
		}
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 编辑我的复盘
// @Produce  json
// @Tags WebApi
// @Param id query string true "id"
// @Param reason query string false "涨停原因"
// @Param highlight query string false "亮点"
// @Param type query string false "类型"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_my_replay [post]
func EditMyReplay(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form replay.ReplayEO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	form.Adder, _ = strconv.Atoi(appG.C.GetHeader("UID"))
	err := relpay.EditMyReplay(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

type DelReq struct {
	Id int `json:"id" form:"id" binding:"gt=0"`
}

// @Summary 删除我的复盘
// @Produce  json
// @Tags WebApi
// @Param id query string true "id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/del_my_replay [post]
func DelMyReplay(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form DelReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := relpay.DelMyReplay(form.Id)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 导入复盘
// @Produce  json
// @Tags WebApi
// @Param file formData file true "excel文件"
// @Param datetime formData string true "日期"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/import_replay [post]
func ImportReplay(c *gin.Context) {
	var (
		appG     = app.Gin{C: c}
		fileType []string
	)
	file, header, err := appG.C.Request.FormFile("file")
	if header != nil {
		fileType = strings.Split(header.Filename, ".")
	}
	if fileType == nil || (fileType[len(fileType)-1] != "xlsx" && fileType[len(fileType)-1] != "xls") {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, "非法文件类型")
		return
	}
	datetime := appG.C.PostForm("datetime")
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, err.Error())
		return
	}
	uid := appG.C.GetHeader("UID")
	total, data, err := relpay.UploadExcel(file, uid, datetime)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"total":     total,
		"fail":      len(data),
		"fail_list": data,
	})
}

func GetReplayStatistics(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form replay.ReplayQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := relpay.GetReplayStatistics(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}
