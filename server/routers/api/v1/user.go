package v1

import (
	"datacenter/models/admin"
	"datacenter/models/template"
	"datacenter/models/user"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/crud_service"
	"datacenter/service/excel_service"
	user_service "datacenter/service/user_service"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strconv"
)

type AddUserRequest struct {
	UserName  string `json:"user_name" form:"user_name" binding:"required"` //账号(数据库中用户名)
	RealName  string `json:"real_name" form:"real_name" binding:"required"` //姓名(数据库中真实姓名)
	Mobile    string `json:"mobile" form:"mobile"`                          //手机号
	Privilege string `json:"privilege" form:"privilege" binding:"required"` //权限,eg:1,2,3,4
}

type EditUserRequest struct {
	Id int `json:"id" form:"id" binding:"required"` //用户id
	AddUserRequest
}

// @Summary 新增账号
// @Produce  json
// @Tags WebApi-User
// @Param user_name query string true "账号"
// @Param real_name query string true "姓名"
// @Param mobile query string false "联系电话"
// @Param privilege query string true "权限:eg: 1,2,3"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/add_user [post]
func AddUser(c *gin.Context) {
	appG := app.Gin{C: c}

	var addUserReq AddUserRequest

	if err := appG.ParseRequest(&addUserReq); err != nil {
		return
	}
	fmt.Printf("请求结构体:%#v\n", addUserReq)

	success, err := user.AddUser(addUserReq.UserName, addUserReq.RealName, addUserReq.Mobile, addUserReq.Privilege)

	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}

type GetUserListRequest struct {
	UserName string `form:"user_name" json:"user_name"`
	RealName string `form:"real_name" json:"real_name"`
	Mobile   string `form:"mobile" json:"mobile" `
	Page     int    `form:"page" json:"page" binding:"required"`
	PageSize int    `form:"page_size" json:"page_size"  binding:"required"`
}

type GetUserListResponse struct {
	Data  []template.BUser `json:"data"`
	Count int              `json:"count"`
}

// @Summary 账号列表
// @Produce  json
// @Tags WebApi-User
// @Param user_name query string false "账号"
// @Param real_name query string false "姓名"
// @Param mobile query string false "联系电话"
// @Param page query string true "页码"
// @Param page_size query string true "条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/get_user_list [get]
func GetUserList(c *gin.Context) {
	appG := app.Gin{C: c}

	var g_u_l_req GetUserListRequest
	var g_u_l_res GetUserListResponse
	var err error
	if err := appG.ParseRequest(&g_u_l_req); err != nil {
		return
	}
	fmt.Printf("请求结构体:%#v\n", g_u_l_req)

	g_u_l_res.Data, g_u_l_res.Count, err = user.GetUserList(g_u_l_req.UserName, g_u_l_req.RealName, g_u_l_req.Mobile, g_u_l_req.Page, g_u_l_req.PageSize)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, g_u_l_res)
	return
}

// @Summary 编辑
// @Produce  json
// @Tags WebApi-User
// @Param user_id query int true "用户id"
// @Param real_name query string true "姓名"
// @Param mobile query string false "联系电话"
// @Param privilege query string true "权限:eg: 1,2,3,一个都不选 就是空"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_user [post]
func EditUser(c *gin.Context) {
	appG := app.Gin{C: c}
	var editUserReq user_service.EditUserRequest
	if err := appG.ParseRequest(&editUserReq); err != nil {
		return
	}

	success, err := user.EditUser(editUserReq)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}

// @Summary 删除用户
// @Produce  json
// @Tags WebApi-User
// @Param user_id query int true "用户id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/del_user [post]
func DelUser(c *gin.Context) {
	appG := app.Gin{C: c}

	var d_u_req user_service.EditPwdRequest

	if err := appG.ParseRequest(&d_u_req); err != nil {
		return
	}
	success, err := user.DelUser(d_u_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}

// @Summary 密码重置
// @Produce  json
// @Tags WebApi-User
// @Param user_id query int true "账号"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_pwd [post]
func EditPwd(c *gin.Context) {
	appG := app.Gin{C: c}

	var e_p_req user_service.EditPwdRequest

	if err := appG.ParseRequest(&e_p_req); err != nil {
		return
	}

	success, err := user.EditPwd(e_p_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}

// @Summary 账号 停用/启用
// @Produce  json
// @Tags WebApi-User
// @Param user_id query int true "用户id"
// @Param status query string true "用户状态(normal(正常)/banned(停用))"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_status [post]
func EditStatus(c *gin.Context) {
	appG := app.Gin{C: c}

	var editStatusReq user_service.EditStatusRequest
	if err := appG.ParseRequest(&editStatusReq); err != nil {
		return
	}
	success, err := user.EditStatus(editStatusReq)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}

// @Summary 点击头像,修改密码
// @Produce  json
// @Tags WebApi-User
// @Param user_id query int true "用户id"
// @Param old_pwd query string true "旧密码"
// @Param new_pwd query string true "新密码,只传一个,前端比对完之后,传过来一个"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_user_pwd [post]
func EditUserPwd(c *gin.Context) {
	appG := app.Gin{C: c}

	var e_u_p_req user_service.EditUserPwdRequest

	if err := appG.ParseRequest(&e_u_p_req); err != nil {
		return
	}

	success, err := user.EditUserPwd(e_u_p_req)

	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}

// @Summary 获取用户配置信息
// @Produce  json
// @Tags WebApi-User
// @Param user_id query string false "用户id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/profile [get]
func GetUserProfile(c *gin.Context) {
	appG := app.Gin{C: c}
	req := struct {
		UserID string `json:"user_id" form:"user_id"`
	}{}
	if err := appG.ParseRequest(&req); err != nil {
		return
	}
	uid := c.GetHeader("UID")
	if uid == "" {
		uid = req.UserID
	}
	profile, err := user.QueryProfile(uid)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, profile)
	return
}

// @Summary 修改配置文件
// @Produce  json
// @Tags WebApi-User
// @Param data body user.Profile true "上报数据，json格式"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/profile [post]
func UpdateUserProfile(c *gin.Context) {
	appG := app.Gin{C: c}
	var req user.Profile

	if err := appG.ParseRequest(&req); err != nil {
		return
	}
	uid := c.GetHeader("UID")
	if uid != "" {
		req.UserID = uid
	}
	err := user.UpdateProfile(&req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
	return
}

func AddWxUser(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.WxLoginQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	res,err:=admin.AddUserBase(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, res)
}

func GetWxUser(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.QuestionLogQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	vos,err:=admin.GetDatiUserByOpenid(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, vos)
}

func GetDatiUsers(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.DatiUsersQuery
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	vos,cnt,err:=admin.GetDatiUsers(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{List: vos,Total: cnt})
}

type KLineQO struct {
	Symbol string `json:"symbol" form:"symbol" binding:"required"`
	Interval string `json:"interval" form:"interval" binding:"required"`
	Limit int `json:"limit" form:"limit" binding:"required"`
	StartTime int64 `json:"start_time" form:"start_time"`
	EndTime int64 `json:"end_time" form:"end_time"`
}

func ExportKLine(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form KLineQO
		vos  []interface{}
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	url:="https://api.binance.com/api/v3/klines"
	params:=fmt.Sprintf("?symbol=%s&interval=%s&limit=%d",form.Symbol,form.Interval,form.Limit)
	if form.StartTime!=0{
		params=params+fmt.Sprintf("&startTime=%d",form.StartTime)
	}
	if form.EndTime!=0{
		params=params+fmt.Sprintf("&endTime=%d",form.EndTime)
	}
	resp,err:=http.Get(url+params)
	defer func() {
		if resp != nil {
			resp.Body.Close()
		}
	}()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err)
		return
	}
	bytes,err:=ioutil.ReadAll(resp.Body)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err)
		return
	}
	var result [][]interface{}
	err = json.Unmarshal(bytes,&result)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err)
		return
	}
	if len(result)>1{
		for _,v:=range result{
			m:=make(map[string]interface{})
			resultJson := fmt.Sprintf(
				`{"A":"%v","B":%v,"C":%v,"D":%v,"E":%v,"F":%v,"G":%v}`,
				/*time.Unix(int64(v[0].(float64))/1e3,0).Format(util.YMDHMS+".000")*/strconv.FormatFloat(v[0].(float64),'f',-1,64), v[1], v[2], v[3], v[4], v[5], v[7])
			json.Unmarshal([]byte(resultJson),&m)
			vos= append(vos, m)
		}
	}
	title := []string{"timestamp", "open", "high", "low", "close", "volume", "turnover"}
	err = excel_service.HandlerDownload(c, "KLine"+form.Symbol+form.Interval, vos, title)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func DatiAdminLogout(c *gin.Context)  {
	var (
		appG = app.Gin{C: c}
	)
	appG.Response(http.StatusOK,e.SUCCESS,nil)
}