package v1

import (
	"datacenter/models/user"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type GetPrivilegeListRequest struct {
	UserId int `json:"user_id" form:"user_id"`
}

// @Summary 获取权限列表,不传user_id,展示所有权限,传user_id,展示当前用户的所有权限
// @Produce  json
// @Tags WebApi
// @Param user_id query int false "用户id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/get_privilege_list [get]
func GetPrivilegeList(c *gin.Context) {
	appG := app.Gin{C: c}
	var g_p_l_req GetPrivilegeListRequest
	if err := appG.ParseRequest(&g_p_l_req); err != nil {
		return
	}
	//fmt.Printf("请求结构体:%#v\n", g_p_l_req)
	uid := c.GetHeader("UID")

	res, err := user.GetPrivilegeListV2(uid, strconv.Itoa(g_p_l_req.UserId))
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, res)
	return
}
