package v1

import (
	"datacenter/models/template"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/excel_service"
	"datacenter/service/research_report_service"
	"github.com/gin-gonic/gin"
	"net/http"
)

type GetResearchReportResponse struct {
	Data  []map[string]interface{} `json:"data"`
	Count int                      `json:"count"`
}

// @Summary 研报筛选
// @Produce  json
// @Tags WebApi-研报平台
// @Param organization query string false "机构名称"
// @Param search query string false "股票代码或名称"
// @Param key_word query string false "关键词"
// @Param date_begin query string false "开始日期"
// @Param date_end query string false "结束日期"
// @Param content query string false "研报内容"
// @Param level query string false "相关评级(不限传空,首次推荐,买入,增持,中性,减持,卖出,上调评级,下调评级)"
// @Param type query string false "类型(不限传空,重点券商,公司研究,行业分析)"
// @Param sort query string false "排序,格式 1_asc,上涨空间(1)预测净利润(2),预测eps(3)预测pe(4),日期(5)"
// @Param page query int true "页码"
// @Param page_size query int true "条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/get_research_report [get]
func GetResearchReport(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		g_r_r_req research_report_service.GetResearchReportRequest
		g_r_r_res GetResearchReportResponse
		err       error
	)
	if err := appG.ParseRequest(&g_r_r_req); err != nil {
		return
	}
	g_r_r_res.Data, g_r_r_res.Count, err = template.GetResearchReport(g_r_r_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, g_r_r_res)
	return
}

// @Summary 研报推荐按钮
// @Produce  json
// @Tags WebApi-研报平台
// @Param id query int true "id"
// @Param recommend query string true "推荐: 0 不推荐,1 推荐"
// @Param user_id query int true "用户id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_research_report_recommend [post]
func EditResearchReportRecommend(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		e_r_r_req research_report_service.EditResearchReportRecommendRequest
		err       error
	)
	if err := appG.ParseRequest(&e_r_r_req); err != nil {
		return
	}
	success, err := template.EditResearchReportRecommend(e_r_r_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}

// @Summary 研报导出
// @Produce  json
// @Tags WebApi-研报平台
// @Param organization query string false "机构名称"
// @Param search query string false "股票代码或名称"
// @Param key_word query string false "关键词"
// @Param date_begin query string false "开始日期"
// @Param date_end query string false "结束日期"
// @Param content query string false "研报内容"
// @Param level query string false "相关评级(不限传空,首次推荐,买入,增持,中性,减持,卖出,上调评级,下调评级)"
// @Param type query string false "类型(不限传空,重点券商,公司研究,行业分析)"
// @Param sort query string false "排序,格式 1_asc,上涨空间(1)预测净利润(2),预测eps(3)预测pe(4),日期(5)"
// @Param page query int true "页码"
// @Param page_size query int true "条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/export_research_report [get]
func ExportResearchReport(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		g_r_r_req research_report_service.GetResearchReportRequest
		g_r_r_res GetResearchReportResponse
		err       error
	)
	if err := appG.ParseRequest(&g_r_r_req); err != nil {
		return
	}
	g_r_r_res.Data, _, err = template.GetResearchReport(g_r_r_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	array := make([]map[string]interface{}, 0)
	var title []string
	for i := 0; i < len(g_r_r_res.Data); i++ {
		var c map[string]interface{}
		c, title = research_report_service.Map(g_r_r_res.Data[i])
		array = append(array, c)
	}

	var data []interface{}
	for _, v := range array {
		//if len(title) == 0 {
		//	for k, _ := range v {
		//		title = append(title, k)
		//	}
		//}
		ch := 'A'
		s := make(map[string]interface{})
		for _, t := range title {
			s[string(ch)] = v[t]
			ch = ch + 1
		}
		data = append(data, v)
	}
	excel_service.HandlerDownload(c, "研报", data, title)
	appG.Response(http.StatusOK, e.SUCCESS, g_r_r_res)
	return
}
