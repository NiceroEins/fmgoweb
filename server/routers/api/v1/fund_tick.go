package v1

import (
    "datacenter/models/fund"
    "datacenter/pkg/app"
    "datacenter/pkg/e"
    "datacenter/service/fund_service"
    "github.com/gin-gonic/gin"
    "github.com/unknwon/com"
    "net/http"
    "time"
)

// @Summary 净值维护列表
// @Produce  json
// @Tags WebApi-净值维护
// @Param fund_id path int true "基金ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_tick_list [get]
func FundTickList(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        form fund.FundTickRequest
        err  error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    tickService := fund_service.FundTick{
        FundID: form.FundID,
    }
    result, err := tickService.GetLists()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_TICK_FAIL, nil)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": result,
    })
}

// @Summary 净值维护添加
// @Produce  json
// @Tags WebApi-净值维护
// @Param value_date body string true "记录时间"
// @Param value body string true "累计净值"
// @Param unit_value body string true "单位净值"
// @Param status body int true "是否分红 1：是；0否"
// @Param fund_id body int true "基金ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_tick_add [post]
// @Tags Test
func FundTickAdd(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form fund.FundTickAddRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    valueDate,err := time.ParseInLocation("2006-01-02", form.ValueDate, time.Local)
    if err !=  nil {
        appG.Response(httpCode, e.ERROR_TICK_FAIL, nil)
        return
    }
    tickService := fund_service.FundTick{
        Created: time.Now(),
        FundID:      form.FundID,
        ValueDate:   valueDate,
        Value:       form.Value,
        Status:      form.Status,
        UnitValue:   form.UnitValue,
    }
    exists, existErr := tickService.ExistByFundId()
    if existErr != nil {
        appG.Response(http.StatusOK, e.ERROR_TICK_FAIL, nil)
        return
    }
    if exists > 0 {
        appG.Response(http.StatusOK, e.ERROR_TICK_EXISTS, nil)
        return
    }
    existsFund, fundErr :=  fund_service.ExistByFund(form.FundID)
    if fundErr != nil {
        appG.Response(http.StatusOK, e.ERROR_TICK_FAIL, nil)
        return
    }
    if !existsFund {
        appG.Response(http.StatusOK, e.ERROR_FUND_NOT_EXISTS, nil)
        return
    }
    err = tickService.Add()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_TICK_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 净值维护删除
// @Produce  json
// @Tags WebApi-净值维护
// @Param id path int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_tick_del [get]
// @Tags Test
func FundTickDel(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form = fund.FundTickDelByIDRequest{ID: com.StrTo(c.Param("id")).MustInt()}
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    tickService := fund_service.FundTick{
        ID:         form.ID,
    }
    exists, err := tickService.ExistByID()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_TICK_FAIL, nil)
        return
    }

    if !exists {
        appG.Response(http.StatusOK, e.ERROR_TICK_NOT_EXISTS, nil)
        return
    }

    err = tickService.Del()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_TICK_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 净值维护编辑
// @Produce  json
// @Tags WebApi-净值维护
// @Param id path int true "ID"
// @Param value_date body string true "记录时间"
// @Param value body string true "累计净值"
// @Param unit_value body string true "单位净值"
// @Param status body int true "是否分红"
// @Param fund_id body int true "基金ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_tick_edit [post]
// @Tags Test
func FundTickEdit(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form  fund.FundTickEditRequest
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode,  e.ERROR_TICK_FAIL, nil)
        return
    }
    valueDate,err := time.ParseInLocation("2006-01-02", form.ValueDate, time.Local)
    if err !=  nil {
        appG.Response(httpCode, errCode, nil)
        return
    }
    tickService := fund_service.FundTick{
        ID:          form.ID,
        FundID:      form.FundID,
        ValueDate:   valueDate,
        Value:       form.Value,
        Status:      form.Status,
        UnitValue:   form.UnitValue,
    }
    exists, existErr := tickService.ExistByFundId()
    if existErr != nil {
        appG.Response(http.StatusOK, e.ERROR_TICK_FAIL, nil)
        return
    }
    if exists > 1 {
        appG.Response(http.StatusOK, e.ERROR_TICK_EXISTS, nil)
        return
    }
    existsFund, fundErr := fund_service.ExistByFund(form.FundID)
    if fundErr != nil {
        appG.Response(http.StatusOK, e.ERROR_TICK_FAIL, nil)
        return
    }
    if !existsFund {
        appG.Response(http.StatusOK, e.ERROR_FUND_NOT_EXISTS, nil)
        return
    }
    existsFund, existErr = tickService.ExistByID()
    if existErr != nil {
        appG.Response(http.StatusOK, e.ERROR_TICK_FAIL, nil)
        return
    }
    if !existsFund {
        appG.Response(http.StatusOK, e.ERROR_FUND_NOT_EXISTS, nil)
        return
    }
    err = tickService.Edit()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_TICK_FAIL, nil)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, nil)
}
