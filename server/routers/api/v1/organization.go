package v1

import (
	"datacenter/models/admin"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/crud_service"
	"github.com/gin-gonic/gin"
	"net/http"
)

func AddOrganization(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.OrganizationIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	id,err:=admin.AddOrganization(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, id)
}

func EditOrganization(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.OrganizationIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err:=admin.EditOrganization(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func DelOrganization(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.OrganizationIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err:=admin.DelOrganization(form.Id)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func GetOrganizations(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.OrganizationQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	vos,cnt,err:=admin.GetOrganizations(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List: vos,
		Total: cnt,
	})
}

func GetOrganizationsAll(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
	)
	vos,err:=admin.GetOrganizationsAll()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, vos)
}

func ImportOrganizations(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.OrgIPOArr
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err:=admin.ImportOrganizations(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func AddUserOrganization(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.DatiUserInfoIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err:=admin.AddUserOrganization(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}