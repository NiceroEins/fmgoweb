package v1

import (
    "datacenter/models/fund"
    "datacenter/pkg/app"
    "datacenter/pkg/e"
    "datacenter/service/fund_service"
    "github.com/gin-gonic/gin"
    "github.com/unknwon/com"
    "net/http"
    "time"
)

// @Summary 基金公告列表
// @Produce  json
// @Tags WebApi-基金公告
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_notice_list [get]
func FundNoticeList(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        err  error
    )
    noticeService := fund_service.FundNotice{}
    result, err := noticeService.GetLists()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_NOTICE_FAIL, nil)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": result,
    })
}

// @Summary 基金公告添加
// @Produce  json
// @Tags WebApi-基金公告
// @Param title body string true "标题"
// @Param author body int true "作者id"
// @Param fund_id body int true "基金ID"
// @Param content body string true "内容"
// @Param publish_date body string true "展示时间"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_notice_add [post]
// @Tags Test
func FundNoticeAdd(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form fund.FundNoticeAddRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    publishDate,err := time.ParseInLocation("2006-01-02 15:04:05", form.PublishDate, time.Local)
    if err !=  nil {
        appG.Response(httpCode, e.ERROR_NOTICE_FAIL, nil)
        return
    }
    noticeService := fund_service.FundNotice{
        Created:     time.Now(),
        FundID:      form.FundID,
        Content:     form.Content,
        Title:       form.Title,
        Author:      form.Author,
        PublishDate: publishDate,
    }
    existsFund, fundErr := noticeService.ExistByFund()
    if fundErr != nil {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_FAIL, nil)
        return
    }
    if !existsFund {
        appG.Response(http.StatusOK, e.ERROR_FUND_NOT_EXISTS, nil)
        return
    }
    err = noticeService.Add()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 基金公告删除
// @Produce  json
// @Tags WebApi-基金公告
// @Param id path int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_notice_del [get]
// @Tags Test
func FundNoticeDel(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form = fund.FundNoticeDelByIDRequest{ID: com.StrTo(c.Param("id")).MustInt()}
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    noticeService := fund_service.FundNotice{
        ID:         form.ID,
    }
    exists, err := noticeService.Exist()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_FAIL, nil)
        return
    }

    if !exists {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_NOT_EXISTS, nil)
        return
    }

    err = noticeService.Del()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 基金公告编辑
// @Produce  json
// @Tags WebApi-基金公告
// @Param id path int true "ID"
// @Param title body string true "标题"
// @Param author body int true "作者ID"
// @Param fund_id body int true "基金ID"
// @Param content body string true "内容"
// @Param publish_date body string true "展示时间"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_notice_edit [post]
// @Tags Test
func FundNoticeEdit(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form  fund.FundNoticeEditRequest
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    publishDate,err := time.ParseInLocation("2006-01-02 15:04:05", form.PublishDate, time.Local)
    if err !=  nil {
        appG.Response(httpCode, e.ERROR_NOTICE_FAIL, nil)
        return
    }
    noticeService := fund_service.FundNotice{
        ID:              form.ID,
        FundID:      form.FundID,
        Content:     form.Content,
        Title:       form.Title,
        Author:      form.Author,
        PublishDate: publishDate,
    }
    exists, existErr := noticeService.ExistByID()
    if existErr != nil {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_FAIL, nil)
        return
    }
    if !exists {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_NOT_EXISTS, nil)
        return
    }
    existsFund, fundErr := noticeService.ExistByFund()
    if fundErr != nil {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_FAIL, nil)
        return
    }
    if !existsFund {
        appG.Response(http.StatusOK, e.ERROR_FUND_NOT_EXISTS, nil)
        return
    }
    existsFund, existErr = noticeService.Exist()
    if existErr != nil {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_FAIL, nil)
        return
    }
    if !exists {
        appG.Response(http.StatusOK, e.ERROR_NOTICE_NOT_EXISTS, nil)
        return
    }
    err = noticeService.Edit()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_NOTICE_FAIL, nil)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, nil)
}
