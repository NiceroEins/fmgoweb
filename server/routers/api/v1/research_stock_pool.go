package v1

import (
	"datacenter/models/stock"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
	"net/http"
	"strconv"
)

// @Summary 研究股池列表
// @Produce  json
// @Tags WebApi-研究股池
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param sort_key query string false "排序字段名"
// @Param user_id query string false "用户名"
// @Param search query string false "股票名称或代码"
// @Param keyword query string false "关键词"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/research/stock_pool_manage [get]
func ReasearchStockPoolManage(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form stock.ReasearchStockPoolReq
		err  error
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	result, cnt, err := stock.GetResearchManageList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	ret := make(map[string]interface{})
	ret["lists"] = result
	ret["total"] = cnt
	appG.Response(http.StatusOK, e.SUCCESS, ret)
}

// @Summary 研究股池添加
// @Produce  json
// @Tags WebApi-研究股池
// @Param code body string true "code"
// @Param name body string true "name"
// @Param content body string true "内容"
// @Param pool_type body int true "股池类型"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/research/stock_pool_add [post]
// @Tags Test
func ReasearchAddStockPool(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form stock.ReasearchStockPoolAdd
	)
	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	form.UserId,_ = strconv.Atoi(c.GetHeader("UID"))
	exist,err := stock.GetDuplicationRecord(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	if !exist {
		appG.Response(http.StatusOK, e.ERROR_STOCK_POOL_EXISTS, nil)
		return
	}
	err = stock.AddReasearchStock(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 研究股池删除
// @Produce  json
// @Tags WebApi-研究股池
// @Param id path int true "id"
// @Param del_status path int true "0代表未删除；1代表删除;2代表剔除"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/research/stock_pool_del [post]
// @Tags Test
func ReasearchDelStockPool(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = stock.ReasearchStockPoolDelRep{Id: com.StrTo(c.Param("id")).MustInt()}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	err := stock.GetDuplicationRecordDel(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	err = stock.DelReasearchStock(&form)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 研究股池编辑
// @Produce  json
// @Tags WebApi-运营股池
// @Param id path int true "ID"
// @Param content body int false "内容"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/research/stock_pool_edit [post]
// @Tags Test
func ReasearchEditStockPool(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = stock.ReasearchStockPoolEditReq{}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	exist,err := stock.GetDuplicationRecordEdit(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	if !exist {
		appG.Response(http.StatusOK, e.ERROR_STOCK_POOL_EXISTS, nil)
		return
	}
	err = stock.EditReasearchStock(&form)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 研究股池内容历史
// @Produce  json
// @Tags WebApi-运营股池
// @Param object_id path int true "对象ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/research/stock_pool_history [get]
// @Tags Test
func ReasearchEditStockPoolHistory(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = stock.ReasearchStockPoolHistoryReq{}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}

	ret,cnt,err := stock.GetReseachHistoryContent(&form)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{"lists":ret,"total":cnt})
}