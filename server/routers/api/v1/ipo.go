package v1

import (
	"datacenter/models/ipo"
	"datacenter/models/template"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/ipo_service"
	"github.com/gin-gonic/gin"
	"net/http"
)

type IpoListResponse struct {
	Data  []template.UIpoEquity `json:"data"`
	Count int                   `json:"count"`
}

// @Summary ipo股权
// @Produce  json
// @Tags WebApi-IPO股权
// @Param order query string true "asc,desc 上会时间排序"
// @Param page query int true "页码"
// @Param page_size query int true "页数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/ipo_list [get]
func IpoList(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		i_l_req ipo_service.IpoListRequest
		i_l_res IpoListResponse
		err     error
	)
	if err := appG.ParseRequest(&i_l_req); err != nil {
		return
	}
	i_l_res.Data, i_l_res.Count, err = template.QueryIPO(i_l_req.Page, i_l_req.PageSize)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, i_l_res)
	return
}

// @Summary ipo股权树
// @Produce  json
// @Tags WebApi-IPO股权
// @Param name query string true "公司全称"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/ipo_relationship [get]
func IpoRelationShip(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		ipoRelationshipRequest  ipo.IpoRelationshipRequest
		ipoRelationshipResponse ipo.IpoRelationshipResponse
		err     error
	)
	if err := appG.ParseRequest(&ipoRelationshipRequest); err != nil {
		return
	}
	ipoRelationshipResponse, err = ipo.QueryIPOEquityRelationship(ipoRelationshipRequest.Name)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, ipoRelationshipResponse)
	return
}
