package v1

import (
	"datacenter/models/stock"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
	"net/http"
	"strconv"
)

// @Summary 纪要列表
// @Produce  json
// @Tags WebApi-纪要列表
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/research/summary_list [get]
func SummaryList(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		err  error
		form stock.SummaryListReq
	)
	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	form.UserId = c.GetHeader("UID")
	result,cnt, err := stock.SummaryList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_FUND_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": result,
		"total":cnt,
	})
}

// @Summary 纪要添加
// @Produce  json
// @Tags WebApi-纪要列表
// @Param content body string true "内容"
// @Param title body string true "标题"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/research/summary_add [post]
// @Tags Test
func SummaryAdd(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form stock.SummaryAddReq
	)
	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	form.UserId,_ = strconv.Atoi(c.GetHeader("UID"))
	err := stock.CreateStockSummary(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 纪要列表删除
// @Produce  json
// @Tags WebApi-纪要列表
// @Param id path int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/research/summary_del [get]
// @Tags Test
func SummaryDel(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = stock.SummaryDelReq{ID: com.StrTo(c.Param("id")).MustInt()}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	err := stock.DelStockSummary(&form)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 纪要编辑
// @Produce  json
// @Tags WebApi-纪要列表
// @Param content body string true "内容"
// @Param title body string true "标题"
// @Param id path int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/research/summary_edit [post]
// @Tags Test
func SummaryEdit(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  stock.SummaryAddReq
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	err := stock.UpdateStockSummary(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}


