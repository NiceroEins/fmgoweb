package v1

import (
	"datacenter/models/spider"
	"datacenter/models/template"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/keyword_service"
	"github.com/gin-gonic/gin"
	"net/http"
)

type KeywordListResponse struct {
	Data             []template.UKeyword `json:"data"`
	Count            int                 `json:"count"`
	MaintenanceCount int                 `json:"MaintenanceCount"` //关键词待维护数量
}

// @Summary 关键词列表
// @Produce  json
// @Tags WebApi-关键词
// @Param keyword query string false "关键词"
// @Param category query string false "分类"
// @Param star query int false "重要等级,1就是 1星，2就是2星,以此类推"
// @Param creator_id query int false "创建者id"
// @Param status query string false "状态,normal/banned"
// @Param case query string false "应用场景:normal(即时新闻),search(百度搜索),interaction(互动)"
// @Param page query int true "页码"
// @Param page_size query int true "页数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/keyword_list [get]
func KeywordList(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		k_l_req keyword_service.KeywordListRequest
		k_l_res KeywordListResponse
		err     error
	)
	if err := appG.ParseRequest(&k_l_req); err != nil {
		return
	}
	k_l_res.Data, k_l_res.Count, k_l_res.MaintenanceCount, err = template.KeywordList(k_l_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, k_l_res)
	return
}

// @Summary 关键词列表
// @Produce  json
// @Tags WebApi-关键词
// @Param keyword query string false "关键词"
// @Param category query string false "分类"
// @Param star query int false "重要等级,1就是 1星，2就是2星,以此类推"
// @Param creator_id query int true "创建者id"
// @Param status query string false "状态,normal/banned"
// @Param case query string false "应用场景:normal(即时新闻),search(百度搜索),interaction(互动)"
// @Param page query int true "页码"
// @Param page_size query int true "页数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/keyword_list_person [get]
func KeywordListPerson(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		k_l_req keyword_service.KeywordListRequest
		k_l_res KeywordListResponse
		err     error
	)
	if err := appG.ParseRequest(&k_l_req); err != nil {
		return
	}
	k_l_res.Data, k_l_res.Count, k_l_res.MaintenanceCount, err = template.KeywordListPersion(k_l_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, k_l_res)
	return
}

// @Summary 新增关键词
// @Produce  json
// @Tags WebApi-关键词
// @Param keyword query string true "关键词"
// @Param category query int true "分类"
// @Param star query int true "重要等级"
// @Param creator_id query int true "创建者id"
// @Param status query string true "状态,normal"
// @Param case query string true "应用场景"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/add_keyword [post]
func AddKeyword(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		g_r_r_req keyword_service.AddKeywordRequest
		success   bool
		err       error
	)
	if err := appG.ParseRequest(&g_r_r_req); err != nil {
		return
	}
	success, err = template.AddKeyword(g_r_r_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	//uid := c.GetHeader("UID")
	spider.RefreshKeyword("")
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}

// @Summary 编辑关键词
// @Produce  json
// @Tags WebApi-关键词
// @Param id query int true "id"
// @Param keyword query string true "关键词"
// @Param category query int true "分类"
// @Param star query int true "重要等级"
// @Param creator_id query int true "创建者id"
// @Param status query string true "状态,normal"
// @Param case query string true "应用场景"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_keyword [post]
func EditKeyword(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		g_r_r_req keyword_service.AddKeywordRequest
		success   bool
		err       error
	)
	if err := appG.ParseRequest(&g_r_r_req); err != nil {
		return
	}
	success, err = template.EditKeyword(g_r_r_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	//uid := c.GetHeader("UID")
	spider.RefreshKeyword("")
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}

// @Summary 删除关键词
// @Produce  json
// @Tags WebApi-关键词
// @Param id query int true "id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/del_keyword [post]
func DelKeyword(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		k_s_d_req keyword_service.DelKeywordRequest
		success   bool
		err       error
	)
	if err := appG.ParseRequest(&k_s_d_req); err != nil {
		return
	}
	success, err = template.DelKeyword(k_s_d_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	//uid := c.GetHeader("UID")
	spider.RefreshKeyword("")
	appG.Response(http.StatusOK, e.SUCCESS, success)
	return
}
