package v1

import (
	"datacenter/models/industry"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 市场事件
// @Produce  json
// @Tags WebApi-市场事件
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param start query string false "开始时间"
// @Param end query string false "结束时间"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/industry_event_list [get]
func IndustryEvent(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form industry.IndustryEventsReq
		err  error
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	result,count, err := industry.GetIndustryEvents(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": result,
		"total": count,
	})
}

// @Summary 市场事件
// @Produce  json
// @Tags WebApi-市场事件
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param start query string false "开始时间"
// @Param end query string false "结束时间"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/industry_calendar_event [get]
func CalendarEvent(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form industry.CalendarEventsReq
	err  error
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	result,count, err := industry.GetCalendarEvents(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": result,
		"total": count,
	})
}

// @Summary 编辑影响股票
// @Tags WebApi
// @Produce json
// @Param category_id body int true "数据项category_id"
// @Param data_id body int true "当前记录id"
// @Param data body array true "影响股票数组(code[必填]/name[选填])"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/events/edit_stock [post]
func EditIndustryStock(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req industry.EditIndustryStockReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}
	err := industry.EditStock(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}