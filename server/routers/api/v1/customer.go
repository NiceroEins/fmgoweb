package v1

import (
    "datacenter/models/fund"
    "datacenter/pkg/app"
    "datacenter/pkg/e"
    "datacenter/pkg/util"
    "datacenter/service/fund_service"
    "github.com/gin-gonic/gin"
    "github.com/unknwon/com"
    "net/http"
    "time"
)

// @Summary 用户列表
// @Produce  json
// @Tags WebApi-基金用户
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param realname query string false "姓名"
// @Param username query string false "登录账号"
// @Param id_card_no query string false "证件号码"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/customer_list [get]
func ListCustomer(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        form fund.CustomerRequest
        err  error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    customerService := fund_service.Customer{
        PageNum:  form.PageNum,
        PageSize: form.PageSize,
        UserName: form.UserName,
        Realname: form.Realname,
        IdCardNo: form.IdCardNo,
    }
    result, err := customerService.GetLists()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }

    count, err := customerService.Count()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": result,
        "total": count,
    })
}

// @Summary 基金用户添加
// @Produce  json
// @Tags WebApi-基金用户
// @Param realname body string true "姓名"
// @Param id_card_no body string true "证件号码"
// @Param username body string true "登陆账号"
// @Param type body string true "身份证"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/customer_add [post]
// @Tags Test
func AddCustomer(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form fund.CustomerAddRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    customerService := fund_service.Customer{
        Realname:        form.Realname,
        Created:         time.Now().Format(util.YMDHMS),
        IdCardNo:        form.IdCardNo,
        UserName:        form.UserName,
    }

    checkName, numErr := customerService.ExistByName()
    if numErr != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }
    if checkName {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_EXISTS, nil)
        return
    }
    err := customerService.Add()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 基金用户删除
// @Produce  json
// @Tags WebApi-基金用户
// @Param id path int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/customer_del [get]
// @Tags Test
func DelCustomer(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form = fund.CustomerDelByIDRequest{ID: com.StrTo(c.Param("id")).MustInt()}
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    customerService := fund_service.Customer{
        ID:         form.ID,
    }

    _, err := customerService.Exist()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }

    err = customerService.Del()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 基金用户编辑
// @Produce  json
// @Tags WebApi-基金用户
// @Param id path int true "ID"
// @Param realname body string true "姓名"
// @Param id_card_no body string true "证件号码"
// @Param username body string true "登陆账号"
// @Param type body string true "身份证"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/customer_edit [post]
// @Tags Test
func EditCustomer(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form  fund.CustomerEditRequest
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    customerService := fund_service.Customer{
        ID:              form.ID,
        Realname:        form.Realname,
        IdCardNo:        form.IdCardNo,
        UserName:        form.UserName,
    }

    exists, err := customerService.ExistByNameAndNo()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }

    if exists {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_EXISTS, nil)
        return
    }

    err = customerService.Edit()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 用户列表
// @Produce  json
// @Tags WebApi-基金用户
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/customer_names [get]
func CustomerNamesList(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form  fund.CustomerNameRequest
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    customerService := fund_service.Customer{
        Realname:        form.Realname,
    }

    result, err := customerService.GetNames()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": result,
    })
}

// @Summary 重置密码
// @Produce  json
// @Tags WebApi-基金用户
// @Param id path int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/customer_reset_pwd [get]
func CustomerResetPwd(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form = fund.CustomerDelByIDRequest{ID: com.StrTo(c.Param("id")).MustInt()}
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    customerService := fund_service.Customer{
        ID:         form.ID,
    }

    ret, err := customerService.Exist()
    if !ret  {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }
    err = customerService.ResetPwd()


    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}



// @Summary 登陆,登陆成功返回token
// @Produce  json
// @Tags WebApi-基金页面接口
// @Param username query string true "登录账号,eg : admin"
// @Param pwd query string true "密码,eg : b9844afdf93df0eeb83b320c9c66f0ff"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v2/login [post]
func CustomerLogin(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form fund.LoginRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    customerService := fund_service.Customer{
        UserName:        form.Username,
        LoginPwd:        form.Pwd,
    }
    frontModel, err := customerService.Check()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_USER_NOT_EXIST, err.Error())
        return
    }
    if frontModel.ID < 1  {
        appG.Response(http.StatusOK, e.ERROR_USER_NOT_EXIST, err.Error())
        return
    }
    //生成token
    token, err := util.GenerateToken(frontModel.ID, frontModel.UserName, frontModel.LoginPwd,"front")
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "token":                   token,
        "user_id":                 frontModel.ID,
        "user_name":               frontModel.UserName,
    })
}

// @Summary 修改密码
// @Produce  json
// @Tags WebApi-基金页面接口
// @Param username query string true "用户登录账号"
// @Param old_pwd query string true "旧密码"
// @Param new_pwd query string true "新密码,只传一个,前端比对完之后,传过来一个"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v2/customer_edit_pwd [post]
func CustomerEditPwd(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form fund.EditCustomerPwdRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    customerFront := fund_service.Customer{
        UserName:        form.Username,
        LoginPwd:        form.NewPwd,
        OldPwd:          form.OldPwd,
    }
    err := customerFront.EditPwd()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, err.Error())
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, nil)
    return
}
