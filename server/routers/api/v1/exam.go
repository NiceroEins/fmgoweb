package v1

import (
	"datacenter/models/exam"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 考核评审/详情
// @Produce  json
// @Tags WebApi-考核
// @Param id query int false "数据id"
// @Param begin query string false "开始日期，YY-MM-DD"
// @Param end query string false "结束日期"
// @Param owner_id query int false "用户uid"
// @Param status query string false "状态 未处理-normal 已处理graded"
// @Param reason query string false "推荐原因"
// @Param p query int true "页码"
// @Param n query int true "单页条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/exam_review [get]
func ExamReview(c *gin.Context) {
	appG := app.Gin{C: c}
	var req exam.GetReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}
	resp, err := exam.Get(req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, resp)
	return
}

// @Summary 我的考核/考核成绩
// @Produce  json
// @Tags WebApi-考核
// @Param year query int false "年份"
// @Param month query int false "月份"
// @Param owner_id query int false "用户uid"
// @Param p query int true "页码"
// @Param n query int true "单页条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/exam_summary [get]
func ExamSummary(c *gin.Context) {
	appG := app.Gin{C: c}
	var req exam.SummaryReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}
	resp, err := exam.Sum(req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, resp)
	return
}

// @Summary 考核评星
// @Produce  json
// @Tags WebApi-考核
// @Param id query int true "条目id"
// @Param star query int true "星级"
// @Param comment query string false "点评"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/exam_set [post]
func ExamSet(c *gin.Context) {
	appG := app.Gin{C: c}
	var req exam.SetReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}
	err := exam.SetStar(req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
	return
}

//// @Summary 编辑关键词
//// @Produce  json
//// @Tags WebApi-关键词
//// @Param id query int true "id"
//// @Param keyword query string true "关键词"
//// @Param category query int true "分类"
//// @Param star query int true "重要等级"
//// @Param creator_id query int true "创建者id"
//// @Param status query string true "状态,normal"
//// @Param case query string true "应用场景"
//// @Success 200 {object} app.Response
//// @Failure 500 {object} app.Response
//// @Router /api/v1/edit_keyword [post]
//func EditKeyword(c *gin.Context) {
//	appG := app.Gin{C: c}
//	var req struct {
//		Id   int `json:"id"`
//		Star int `json:"star"`
//	}
//	if err := appG.ParseRequest(&req); err != nil {
//		return
//	}
//	err := exam.SetStar(req.Id, req.Star)
//	if err != nil {
//		appG.Response(200, e.ERROR, err.Error())
//		return
//	}
//	appG.Response(http.StatusOK, e.SUCCESS, nil)
//	return
//}

// @Summary 考核条目删除
// @Produce  json
// @Tags WebApi-考核
// @Param id query int true "条目id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/exam_del [post]
func ExamDel(c *gin.Context) {
	appG := app.Gin{C: c}
	var req struct {
		Id int `json:"id" form:"id"`
	}
	if err := appG.ParseRequest(&req); err != nil {
		return
	}
	err := exam.Del(req.Id)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
	return
}

// @Summary 考核姓名
// @Produce  json
// @Tags WebApi-考核
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/exam_name [get]
func ExamName(c *gin.Context) {
	appG := app.Gin{C: c}

	data := exam.GetName()
	appG.Response(http.StatusOK, e.SUCCESS, data)
	return
}
