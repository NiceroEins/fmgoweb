package v1

import (
	quantization2 "datacenter/controller/quantization"
	"datacenter/models/quantization"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/excel_service"

	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 回测分析
// @Tags Quantization
// @Produce json
// @Param type query array true "创新高类型, 涨停block/百元100/次新股sub-new/平台突破platform/板块industry"
// @Param min_price query number true "最小股价"
// @Param max_price query number false "最大股价"
// @Param min_amount query number true "最小成交量"
// @Param max_amount query number false "最大成交量"
// @Param min_market_value query number true "最小市值"
// @Param max_market_value query number false "最大市值"
// @Param rsi query array false "基本面"
// @Param begin query string false "开始日期，2006-01-02T15:04:05+08:00"
// @Param end query string false "结束日期"
// @Param period query array false "回测持股周期数组"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/quantization/analyze [post]
func Analyze(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form quantization.HighestReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := quantization2.Analyze(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

// @Summary 回测详情
// @Tags Quantization
// @Produce json
// @Param type query array true "创新高类型, 涨停block/百元100/次新股sub-new/平台突破platform/板块industry"
// @Param min_price query number true "最小股价"
// @Param max_price query number false "最大股价"
// @Param min_amount query number true "最小成交量"
// @Param max_amount query number false "最大成交量"
// @Param min_market_value query number true "最小市值"
// @Param max_market_value query number false "最大市值"
// @Param rsi query array false "基本面"
// @Param begin query string false "开始日期，2006-01-02T15:04:05+08:00"
// @Param end query string false "结束日期"
// @Param period query array false "回测持股周期数组"
// @Param sort query string false "排序字段"
// @Param order query string false "ASC升序/DESC降序"
// @Param p query int true "页码"
// @Param n query int true "单页条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/quantization/detail [post]
func Detail(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form quantization.HighestDetailReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := quantization2.Detail(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

// @Summary 回测详情导出
// @Tags Quantization
// @Produce json
// @Param key query string true "下载key"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/quantization/export [get]
func Export(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form struct {
			Key string `json:"key" form:"key"`
		}
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := quantization2.Export(form.Key)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	//array := make([]map[string]interface{}, 0)
	//var title []string
	//quantization_service.Map()
	var val []interface{}

	for _, v := range data {
		val = append(val, quantization.HighestDetailExport{
			Code:       v.Code,
			Name:       v.Name,
			TimeBegin:  v.TimeBegin,
			TimeEnd:    v.TimeEnd,
			PriceBegin: v.PriceBegin,
			PriceEnd:   v.PriceEnd,
			Change:     v.Change,
		})
	}
	title := []string{
		"股票代码",
		"股票简称",
		"起始时间",
		"终止时间",
		"起始价",
		"终止价",
		"区间涨幅",
	}

	excel_service.HandlerDownload(c, "回测", val, title)
	appG.Response(http.StatusOK, e.SUCCESS, nil)
	return
}

// @Summary QA问答回测
// @Tags Quantization
// @Produce json
// @Param type query string true "回测类型，question/提问买入，answer/回答买入"
// @Param begin query string false "开始日期，2006-01-02T15:04:05+08:00"
// @Param end query string false "结束日期"
// @Param sort query string false "排序字段"
// @Param order query string false "ASC升序/DESC降序"
// @Param p query int true "页码"
// @Param n query int true "单页条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/quantization/qa_analyze [post]
func QAAnalyze(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form quantization.QAReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := quantization2.QAAnalyze(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

// @Summary QA问答回测详情
// @Tags Quantization
// @Produce json
// @Param type query string true "回测类型，question/提问买入，answer/回答买入"
// @Param begin query string false "开始日期，2006-01-02T15:04:05+08:00"
// @Param end query string false "结束日期"
// @Param sort query string false "排序字段"
// @Param order query string false "ASC升序/DESC降序"
// @Param author query string true "作者"
// @Param p query int true "页码"
// @Param n query int true "单页条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/quantization/qa_detail [post]
func QADetail(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form quantization.QADetailReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := quantization2.QADetail(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

// @Summary 回测分时图
// @Tags Quantization
// @Produce json
// @Param type query array true "创新高类型, 涨停block/百元100/次新股sub-new/平台突破platform/板块industry"
// @Param min_price query number true "最小股价"
// @Param max_price query number false "最大股价"
// @Param min_amount query number true "最小成交量"
// @Param max_amount query number false "最大成交量"
// @Param min_market_value query number true "最小市值"
// @Param max_market_value query number false "最大市值"
// @Param rsi query array false "基本面"
// @Param begin query string false "开始日期，2006-01-02T15:04:05+08:00"
// @Param end query string false "结束日期"
// @Param period query array false "回测持股周期数组"
// @Param start_id query number false "查询起始序号，初始为0"
// @Param max_position query number true "最大仓位,如10"
// @Param principal query number true "本金，如1000000"
// @Param stop_profit query number true "止盈，如0.03"
// @Param stop_loss query number true "止损，如0.03"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/quantization/benchmark [post]
func Benchmark(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form quantization.HighestBenchmarkReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := quantization2.BenchmarkDetail(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}
