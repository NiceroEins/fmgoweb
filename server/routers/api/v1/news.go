package v1

import (
	"datacenter/controller/news"
	"datacenter/models/spider"
	"datacenter/models/statistics"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"time"

	"net/http"
)

type NewsRequest struct {
	Keyword   string `form:"keyword" json:"keyword" binding:"omitempty"`
	Type      string `form:"type" json:"type" binding:"omitempty"`
	Num       int    `form:"n" json:"n" binding:"gt=0,lt=200"`
	Offset    int    `form:"offset" json:"offset" binding:"gte=0"`
	Direction string `form:"direction" json:"direction" binding:"oneof=up down"`
	Duplicate int    `form:"duplicate" json:"duplicate" binding:"gte=0,lte=1"`
}

type NewsResponse struct {
	Data []spider.NewsEx `json:"data"`
}

// @Summary 拿新闻
// @Tags WebApi
// @Produce json
// @Param keyword query string false "关键词清单，多个关键词以逗号（中英文皆可）分隔"
// @Param type query string false "查询类型，新闻=news / 淘股吧=tgb / 微博=wb / 互动=qa / 互动提问=q / 微信公众号标题及内容=wechat-detail，多选时用英文逗号分隔，为空时查询全部类型"
// @Param n query int true "必填，最大查询条数"
// @Param offset query int true "必填，当前查询偏移量（首次查询为0）"
// @Param direction query string true "必填，查询方向，up/down"
// @Param duplicate query int false "是否显示重复新闻，0-否/1-是"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/news [get]
func GetNews(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var newsRequest NewsRequest
	var newsResponse NewsResponse
	if err := appG.ParseRequest(&newsRequest); err != nil {
		return
	}
	uid := c.GetHeader("UID")
	policy := &spider.NewsPolicy{}
	//keywords := news.MakeKeywords(newsRequest.Keyword)
	//newsType := news.MakeNewsType(newsRequest.Type)
	policy.Default(uid).SetKeyword(newsRequest.Keyword).SetType(newsRequest.Type).Rule("roll").SetEntry("roll").Do(uid)
	raw, _, err := spider.FetchNews(policy, false, newsRequest.Offset, 0, newsRequest.Num, newsRequest.Direction)
	if len(raw) > 0 {
		q, n := 0, 0
		for _, v := range raw {
			if v.News.Type == "qa" || v.News.Type == "q" {
				q++
			} else {
				n++
			}
		}
		if n > 0 {
			err = statistics.UpdateTotalNum(statistics.NewsTotalKey, uid, n)
		}
		if q > 0 {
			err = statistics.UpdateTotalNum(statistics.QaTotalKey, uid, q)
		}
	}

	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}
	newsResponse.Data = news.Assemble(raw, policy, uid)

	appG.Response(http.StatusOK, e.SUCCESS, newsResponse)
}

type SearchRequest struct {
	Keyword   string    `form:"keyword" json:"keyword" binding:"omitempty"`
	Type      string    `form:"type" json:"type" binding:"required"`
	Num       int       `form:"n" json:"n" binding:"gt=0,lte=200"`
	Page      int       `form:"p" json:"p" binding:"gt=0"`
	Begin     time.Time `form:"begin" json:"begin" binding:"required,ltefield=End"`
	End       time.Time `form:"end" json:"end" binding:"required,gtefield=Begin"`
	Duplicate int       `form:"duplicate" json:"duplicate" binding:"gte=0,lte=1"`
}

type SearchResponse struct {
	Data  []spider.NewsEx `json:"data"`
	Total int             `json:"total"`
	Page  int             `json:"page"`
}

// @Summary 搜索新闻、互动
// @Tags WebApi
// @Produce json
// @Param keyword query string false "关键词清单，多个关键词以空格分隔"
// @Param type query string true "必填，查询类型，新闻=news / 淘股吧=tgb / 微博=wb / 互动=qa / 互动提问=q / 栏目=source / 无关新闻=unrelated / 微信公众号标题及内容=wechat-detail / 微信订阅号名称=wechat-source，多选时用空格分隔"
// @Param n query int true "必填，单页条数，不大于50"
// @Param p query int true "必填，页码，不小于1"
// @Param begin query string true "必填，查询起始时间，rfc3339格式"
// @Param end query string true "必填，查询截止时间，2019-04-27T09:08:29+08:00"
// @Param duplicate query int false "是否显示重复新闻，0-否/1-是"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/search [get]
func SearchNews(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req SearchRequest
	var resp SearchResponse
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	//keywords := news.MakeKeywords(newsRequest.Keyword)
	//newsType := news.MakeNewsType(newsRequest.Type)
	uid := c.GetHeader("UID")
	policy := &spider.NewsPolicy{}
	policy = policy.Default(uid).SetSep(" ").SetKeyword(req.Keyword).SetType(req.Type).SetEntry("search")
	if req.Duplicate == 0 {
		policy = policy.Rule("search")
	} else {
		policy = policy.Rule("all")
	}
	policy.Do(uid)
	offset := req.Num * (req.Page - 1)

	raw, cnt, err := spider.FetchNews(policy, true, 0, offset, req.Num, "", req.Begin.Format("2006-01-02 15:04:05"), req.End.Format("2006-01-02 15:04:05"))
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	resp.Data = news.Assemble(raw, policy, uid)
	resp.Page = req.Page
	resp.Total = cnt

	appG.Response(http.StatusOK, e.SUCCESS, resp)
}
