package v1

import (
	"datacenter/controller/report"
	"datacenter/controller/statistics"
	"datacenter/controller/task"
	"datacenter/models/spider"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/pkg/gredis"
	"strconv"

	//"datacenter/pkg/util"

	//"datacenter/pkg/logging"
	//"datacenter/pkg/util"

	//"fmt"
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
	"net/http"
	"regexp"
	//"reflect"
)

// @Summary 任务状态监控
// @Tags Spider
// @Produce json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/monitor [get]
func GetMonitor(c *gin.Context) {
	appG := app.Gin{C: c}

	ret := task.Monitor()
	appG.Response(http.StatusOK, e.SUCCESS, ret)
}

// @Summary 拿个任务
// @Tags Spider
// @Produce json
// @Param type query string false "任务类型，normal/strong/q/qa/qa-hdy/special/forecast(预告)/announcement(公告)/disclosure(披露时间)/iwc(i问财)/wb(微博)/wb-detail(微博详情)/以及以上任务的-copy"
// @Param n query int true "任务个数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/task [get]
func GetTask(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}
	taskType := com.StrTo(c.Query("type")).String()
	valid := validation.Validation{}
	//valid.Match(taskType, regexp.MustCompile("normal|strong|qa|special|forecast|announcement|disclosure|iwc|qa-hdy|wb|wb-detail|normal-copy|special-copy"), "type")
	//
	//if taskType != "" && valid.HasErrors() {
	//	app.MarkErrors(valid.Errors)
	//	appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
	//	return
	//}

	taskNum := com.StrTo(c.Query("n")).MustInt()
	valid.Min(taskNum, 1, "n").Message("n必须大于0")

	if valid.HasErrors() {
		app.MarkErrors(valid.Errors)
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
		return
	}

	task := task.GetTask(taskNum, taskType)
	statistics.SendTask(task.Len())
	appG.Response(http.StatusOK, e.SUCCESS, task)
}

// @Summary 手动写入任务(谨慎使用，无法撤回。使用前确认参数无误)
// @Tags Spider
// @Produce json
// @Param data body spider.Seed true "任务数据，json格式"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/task [post]
func SetTask(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form spider.Seed
	)

	//将传入参数赋值，验证并且赋值给form变量
	if err := appG.ParseRequest(&form); err != nil {
		return
	}
	//httpCode, errCode, msg := app.BindAndCheck(c, &form)
	//if errCode != e.SUCCESS {
	//	appG.Response(httpCode, errCode, msg)
	//	return
	//}
	if bEnqueued := task.TryEnqueue(&form, form.Mode); !bEnqueued {
		appG.Response(http.StatusOK, e.ERROR, "task list exceeds limit")
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 查看日志
// @Tags Spider
// @Produce json
// @Param type query string true "日志类型，seed/report/task"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/report [get]
func GetReport(c *gin.Context) {
	appG := app.Gin{C: c}
	//参数校验
	reportType := com.StrTo(c.Query("type")).String()
	valid := validation.Validation{}
	valid.Match(reportType, regexp.MustCompile("seed|report|task"), "type")

	if reportType != "" && valid.HasErrors() {
		app.MarkErrors(valid.Errors)
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
		return
	}

	switch reportType {
	case "seed":
		seeds, _ := spider.GetReport()
		appG.Response(http.StatusOK, e.SUCCESS, seeds)
	case "report":
		_, reports := spider.GetReport()
		appG.Response(http.StatusOK, e.SUCCESS, reports)
	case "task":
		appG.Response(http.StatusOK, e.SUCCESS, spider.GetLog(100))
	default:
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
	}

}

// @Summary 发送报告
// @Tags Spider
// @Produce json
// @Param data body spider.Report true "上报数据，json格式"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/report [post]
func PostReport(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form spider.Report
	)

	//将传入参数赋值，验证并且赋值给form变量
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}

	if id, err := report.ProcReport(&form); err != nil {
		//ipo去重错误,返回200
		if err.Error() == e.GetMsg(e.ERROR_IPO_ERR_MSG) {
			appG.Response(http.StatusOK, e.SUCCESS, err.Error())
			return
		}
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	} else {
		appG.Response(http.StatusOK, e.SUCCESS, id)
	}
}

//实时计算:今天,近一小时,实时(近10s)
//生成任务个数,发任务个数,收到爬虫数据(postreport),入库(mysql):实际写入

// @Summary 实时监控状态
// @Tags Spider
// @Produce json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/realtimeinfo [get]
func GetRealTimeInfo(c *gin.Context) {
	appG := app.Gin{C: c}

	//统计今天 00：00 --当前时间的数据;直接查redis
	day_product_task_num, _ := gredis.Get("day_product_task_num")
	day_send_task_num, _ := gredis.Get("day_send_task_num")
	day_receive_data_num, _ := gredis.Get("day_receive_data_num")
	day_store_mysql_num, _ := gredis.Get("day_store_mysql_num")
	//fmt.Println(string(day_product_task_num),string(day_send_task_num),string(day_receive_data_num),string(day_store_mysql_num))

	//统计近一个小时数据:当前时间%3600=当前incr哪个key，建立3600个string,过期时间3599s  todo
	hour_product_task_num := gredis.KeysValueCount("hour_product_task_num:*")
	hour_send_task_num := gredis.KeysValueCount("hour_send_task_num:*")
	hour_receive_data_num := gredis.KeysValueCount("hour_receive_data_num:*")
	hour_store_mysql_num := gredis.KeysValueCount("hour_store_mysql_num:*")

	////统计近10s数据:当前时间%10=当前incr哪个key，建立10个string,过期时间9s  todo
	second_product_task_num := gredis.KeysValueCount("second_product_task_num:*")
	second_send_task_num := gredis.KeysValueCount("second_send_task_num:*")
	second_receive_data_num := gredis.KeysValueCount("second_receive_data_num:*")
	second_store_mysql_num := gredis.KeysValueCount("second_store_mysql_num:*")
	//fmt.Println(second_product_task_num)
	resp := spider.RealTimeInfo{}

	resp.Day.Day_product_task_num = string(day_product_task_num)
	resp.Day.Day_send_task_num = string(day_send_task_num)
	resp.Day.Day_receive_data_num = string(day_receive_data_num)
	resp.Day.Day_store_mysql_num = string(day_store_mysql_num)

	resp.Hour.Hour_product_task_num = strconv.Itoa(hour_product_task_num)
	resp.Hour.Hour_send_task_num = strconv.Itoa(hour_send_task_num)
	resp.Hour.Hour_receive_data_num = strconv.Itoa(hour_receive_data_num)
	resp.Hour.Hour_store_mysql_num = strconv.Itoa(hour_store_mysql_num)

	resp.Second.Second_product_task_num = strconv.Itoa(second_product_task_num / 10)
	resp.Second.Second_send_task_num = strconv.Itoa(second_send_task_num / 10)
	resp.Second.Second_receive_data_num = strconv.Itoa(second_receive_data_num / 10)
	resp.Second.Second_store_mysql_num = strconv.Itoa(second_store_mysql_num / 10)

	appG.Response(200, e.SUCCESS, resp)
}

// @Summary 拿代理地址
// @Tags Spider
// @Produce json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/proxy [get]
func GetProxy(c *gin.Context) {
	appG := app.Gin{C: c}

	proxy := spider.GetProxy()
	appG.Response(http.StatusOK, e.SUCCESS, proxy)
}
