package v1

import (
	"datacenter/models/event"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 报告事件
// @Tags WebApi
// @Produce json
// @Param data body event.Event true "任务数据，json格式"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/event [post]
func PushEvent(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form event.Event
	)

	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}

	form.UserID = c.GetHeader("UID")
	if err := event.CreateEvent(&form); err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

type PushResponse struct {
	Data    []event.PushData `json:"data"`
	Total   int              `json:"total"`
	PageNum int              `json:"n"`
}

// @Summary 推送查询
// @Tags WebApi
// @Produce json
// @Param begin query string true "开始时间"
// @Param end query string true "结束时间"
// @Param size query int true "每页条数"
// @Param n query int true "页码"
// @Param b_mine query bool true "我的推荐库"
// @Param from query string false "推荐人"
// @Param type query string false "推荐类型"
// @Param content query string false "内容"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/push [get]
func GetPushData(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form event.PushRequest
	)

	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	uid := c.GetHeader("UID")
	data, cnt := event.QueryPushData(&form, uid)
	ret := PushResponse{
		Data:    data,
		Total:   cnt,
		PageNum: form.PageNum,
	}

	appG.Response(http.StatusOK, e.SUCCESS, ret)
}
