package v1

import (
    "datacenter/models/stock"
    "datacenter/models/template"
    "datacenter/pkg/app"
    "datacenter/pkg/e"
    "datacenter/service/excel_service"
    "datacenter/service/stock_pool_service"
    "github.com/gin-gonic/gin"
    "github.com/unknwon/com"
    "net/http"
    "time"
)

type StockPoolListResponse struct {
    Data       []stock.StockPoolVO `json:"data"`
    Total      int                 `json:"total"`
    UpdateTime string              `json:"update_time"`
}

/*func StockPoolList(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		s_p_l_req stock_pool_service.StockPoolListRequest
		s_p_l_res StockPoolListResponse
		err       error
	)
	if err := appG.ParseRequest(&s_p_l_req); err != nil {
		return
	}
	s_p_l_res.Data, s_p_l_res.Count, err = template.StockPoolList(s_p_l_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, s_p_l_res)
	return
}*/

// @Summary 精选股池列表
// @Produce  json
// @Tags WebApi-精选股池
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param sort_key query string false "排序字段名"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Param type query string false "类型筛选"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_pool_list [get]
func StockPoolGetList(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        form stock.StockPoolQO
        vos  []stock.StockPoolVO
        err  error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    data, cnt, err := stock.GetList(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, err.Error())
        return
    }
    for _, v := range data {
        p := v.DTO2VO()
        vos = append(vos, *p)
    }
    ret := StockPoolListResponse{
        Data:       vos,
        Total:      cnt,
        UpdateTime: time.Now().Format("2006-01-02 15:04:05"),
    }
    appG.Response(http.StatusOK, e.SUCCESS, ret)
}

// @Summary 精选股池类型列表
// @Produce  json
// @Tags WebApi-精选股池
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_pool_type [get]
func GetAllStockPoolType(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        vos []stock.StockPoolTypeVO
        err error
    )
    data, err := stock.GetAllStockPoolType()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, err.Error())
        return
    }
    for _, v := range data {
        p := v.DTO2VO()
        vos = append(vos, *p)
    }
    appG.Response(http.StatusOK, e.SUCCESS, vos)
}
// @Summary 精选股池导出
// @Produce  json
// @Tags WebApi-精选股池
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param type query string false "类型筛选"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_pool_export [get]
func ExportStockPool(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        vos  []interface{}
        err  error
    )
    typeName :=c.Request.FormValue("type")
    data, _, err := stock.GetAll(typeName)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, nil)
        return
    }
    for _, v := range data {
        p := v.DTO2EPO()
        vos = append(vos, *p)
    }
    title := []string{"股票代码", "股票名称", "现价", "涨跌幅(%)", "成交额", "流通市值", "总市值", "换手率(%)", "行业分类", "类型", "至今涨幅(%)", "调入日期", "人气排名", "排名变化"}
    err = excel_service.HandlerDownload(c, "精选股票池", vos, title)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, err)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, nil)
}


// @Summary 运营股池列表
// @Produce  json
// @Tags WebApi-运营股池
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param sort_key query string false "排序字段名"
// @Param user_id query string false "用户名"
// @Param search query string false "股票名称或代码"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_pool_manage [get]
func StockPoolGetManage(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        form stock.StockPoolRequest
        err  error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    result, cnt, err := stock.GetManageList(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, nil)
        return
    }
    userInfo,userErr := stock.GetUserInfo()
    if userErr != nil {
        appG.Response(http.StatusOK, e.ERROR, nil)
        return
    }
    ret := stock.StockPoolManageResponse{
        Data:       result,
        Total:      cnt,
        UserInfo: userInfo,
        UpdateTime: time.Now().Format("2006-01-02 15:04:05"),
    }
    appG.Response(http.StatusOK, e.SUCCESS, ret)
}

// @Summary 运营股池添加
// @Produce  json
// @Tags WebApi-运营股池
// @Param code body string true "Code"
// @Param user_id body int false "UserId"
// @Param recommend_reason body int false "RecommendReason"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_pool_add [post]
// @Tags Test
func AddStockPool(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form stock_pool_service.StockRecommondForm
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    stockService := stock_pool_service.StockPoolManage{
        Code:            form.Code,
        RecommendReason: form.RecommendReason,
        UserId:          form.UserId,
        Status:          0,
    }

    checkNum, numErr := stockService.CheckByCode()
    if numErr != nil {
        appG.Response(http.StatusOK, e.ERROR_RECOMMOND_STOCK, nil)
        return
    }
    if checkNum {
        appG.Response(http.StatusOK, e.ERROR_RECOMMOND_STOCK_NUM, nil)
        return
    }
    exists, err := stockService.ExistByCode()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_RECOMMOND_STOCK, nil)
        return
    }
    if exists {
        appG.Response(http.StatusOK, e.ERROR_RECOMMOND_STOCK_EXIST, nil)
        return
    }
    err = stockService.Add()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_RECOMMOND_STOCK, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 运营股池删除
// @Produce  json
// @Tags WebApi-运营股池
// @Param id path int true "ID"
// @Param del_reason body string true "DelReason"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_pool_del [post]
// @Tags Test
func DelStockPool(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form = stock_pool_service.DelRecommondForm{ID: com.StrTo(c.Param("id")).MustInt()}
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    stockService := stock_pool_service.StockPoolManage{
        Id:         form.ID,
        DelReason:  form.DelReason,
        Status: template.DELETE_STATUS,
    }

    exists, err := stockService.ExistByID()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_RECOMMOND_STOCK_DEL, nil)
        return
    }

    if !exists {
        appG.Response(http.StatusOK, e.ERROR_RECOMMOND_STOCK_EXIST, nil)
        return
    }

    err = stockService.Del()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_RECOMMOND_STOCK_DEL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 运营股池编辑
// @Produce  json
// @Tags WebApi-运营股池
// @Param id path int true "ID"
// @Param recommend_reason body int false "RecommendReason"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_pool_edit [post]
// @Tags Test
func EditStockPool(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form = stock_pool_service.EditRecommondForm{ID: com.StrTo(c.Param("id")).MustInt()}
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    stockService := stock_pool_service.StockPoolManage{
        Id:         form.ID,
        RecommendReason:  form.RecommendReason,
    }

    exists, err := stockService.ExistByID()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_RECOMMOND_STOCK_DEL, nil)
        return
    }

    if !exists {
        appG.Response(http.StatusOK, e.ERROR_RECOMMOND_STOCK_EXIST, nil)
        return
    }

    err = stockService.Edit()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_RECOMMOND_STOCK_DEL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}