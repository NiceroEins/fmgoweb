package v1

import (
	"datacenter/models/auction"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/auction_service"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 拍卖列表
// @Tags WebApi-拍卖
// @Produce  json
// @Param page_size query int true "每页条数"
// @Param page_num query int true "页数"
// @Param title query string false "标题"
// @Param platform query string false "网站平台"
// @Param start query string false "开始时间"
// @Param end query string false "结束时间"
// @Param max_id query string false "最大id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/auction_list [get]
func AuctionLists(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form auction.AuctionListRequest
		err  error
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	userID :=  c.GetHeader("UID")
	auctionService := auction_service.Auction{
		PageNum:  form.PageNum,
		PageSize: form.PageSize,
		Start:    form.Start,
		End:      form.End,
		Platform: form.Platform,
		MaxID:    form.MaxID,
		Title:    form.Title,
		Status:   form.Status,
		UserID:   userID,
	}
	auctionList, err := auctionService.GetAuctionLists()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_FAIL, nil)
		return
	}

	count, err := auctionService.Count()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": auctionList,
		"total": count,
	})
}


// @Summary 拍卖已读
// @Tags WebApi-拍卖
// @Produce  json
// @Param id body int true "当前记录id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/auction_read [post]
func AuctionDeal(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form auction.AuctionDealRequest
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	userID :=  c.GetHeader("UID")
	eventModel := auction_service.CommonEvent{
		UserID: userID,
		ObjectID: form.Id,
		ObjectType:"auction",
		EventType:"click",
	}
	if err := auction_service.CreateEvent(&eventModel); err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}