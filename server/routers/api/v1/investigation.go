package v1

import (
	"datacenter/controller/storage"
	"datacenter/models/spider"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/crud_service"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetInvestigationSearchList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form spider.InvestigationSearchQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := storage.GetInvestigationSearchList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		PageNum: form.PageNum,
		List:    data,
		Total:   cnt,
	})
}

func GetInvestigationFilteredList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form spider.InvestigationFilteredQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := storage.GetInvestigationFilteredList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		PageNum: form.PageNum,
		List:    data,
		Total:   cnt,
	})
}
