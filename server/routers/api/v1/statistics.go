package v1

import (
	"datacenter/controller/statistics"
	"datacenter/models/seed"
	statistics2 "datacenter/models/statistics"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/crud_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

type StatNewsReq struct {
	TBegin time.Time `json:"begin" form:"begin" validate:"required,gt=0"`
	TEnd   time.Time `json:"end" form:"end" validate:"required,gt=0"`
	Limit  int       `json:"limit" form:"limit" binding:"lte=20,gt=0"`
	Field  string    `json:"field" form:"field"`
}

type StatNewsResp struct {
	TBegin time.Time            `json:"begin"`
	TEnd   time.Time            `json:"end"`
	Limit  int                  `json:"limit"`
	Data   *statistics.StatData `json:"data"`
}

// @Summary 统计平台-新闻统计api
// @Tags Statistics
// @Produce json
// @Param begin query string true "必填，查询起始时间，rfc3339格式"
// @Param end query string true "必填，查询截止时间，2019-04-27T09:08:29+08:00"
// @Param limit query int true "必填，显示条数，0<limit≤20"
// @Param field query string false "查询数据域，逗号分隔，新闻:news_all/news_show,互动作者:author,互动股票:object_all/object_q/object_qa,互动关键词:keyword_all/keyword_new,处理新闻:proc"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/statistics_news [get]
func StatNews(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form StatNewsReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}

	var resp = StatNewsResp{
		TBegin: form.TBegin,
		TEnd:   form.TEnd,
		Limit:  form.Limit,
		Data: statistics.StatNews(statistics.StatReq{
			Begin: form.TBegin,
			End:   form.TEnd,
			Field: form.Field,
			Limit: form.Limit,
		}),
	}

	appG.Response(http.StatusOK, e.SUCCESS, resp)
}

// @Summary 新闻源统计
// @Tags Statistics
// @Produce json
// @Param date_time query string true "日期(2020-02-02)"
// @Param page_size query int true "每页条数"
// @Param page_num query int true "页码"
// @Param err query bool false "是否异常"
// @Param catch_path query string false "栏目名称"
// @Param sort_param query int false "排序字段 0-最后更新日期 1-平均 2-昨天 3-今天"
// @Param direction query int false "升降序 0-降序 1-升序"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/statistics_seed [get]
func GetSeedStatisticsList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = statistics.SeedStatisticsQO{}
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := form.GetList()
	var ret = make([]crud_service.VO, 0)
	for _, v := range data {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	rep := crud_service.GetResponse(ret, cnt, form.PageNum)
	appG.Response(http.StatusOK, e.SUCCESS, rep)
}

// @Summary 新闻源统计折线图
// @Tags Statistics
// @Produce json
// @Param date_time query string true "日期(2020-02-02)"
// @Param id query int true "新闻源id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/statistics_seed_map [get]
func GetSeedStatisticsMap(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = statistics.SingleSeedStatisticsQO{}
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := form.GetOne()
	var ret = make([]crud_service.VO, 0)
	for _, v := range data {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, ret)
}

// @Summary 用户统计
// @Tags Statistics
// @Produce json
// @Param date_time query string true "日期 例：2020-01-01"
// @Param page_size query int true "每页条数"
// @Param page_num query int true "页码"
// @Param name query string false "姓名"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/statistics_user [get]
func GetUserStatistics(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form statistics.UserStatisticsQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := statistics.GetUserStatistics(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.GetResponse(data, cnt, form.PageNum))
}

// @Summary 个人处理详情统计
// @Tags Statistics
// @Produce json
// @Param id query int true "用户id"
// @Param date_time query string true "日期 例：2020-01-01"
// @Param page_size query int true "每页条数"
// @Param page_num query int true "页码"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/statistics_person [get]
func GetPersonStatistics(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form statistics.PersonStatisticsQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := statistics.GetPersonStatistics(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.GetResponse(data, cnt, form.PageNum))
}

// @Summary 统计页异常新闻源重置
// @Tags Statistics
// @Produce json
// @Param id query int true "新闻源id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/reset_seed [get]
func ResetSeed(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form struct {
			ID int `json:"id" form:"id" binding:"required"`
		}
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err := seed.ResetSeed(form.ID)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func GetPrivilegeStatisticsList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form statistics2.PrivilegeStatisticListQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := statistics.GetPrivilegeStatisticsList(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

func GetPrivilegeStatisticsUserList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form statistics2.PrivilegeStatisticUserListQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := statistics.GetPrivilegeStatisticsUserList(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

func GetUserPrivilegeStatisticsTotal(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form statistics2.UserPrivilegeStatisticTotalQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := statistics.GetUserPrivilegeStatisticsTotal(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

func GetUserPrivilegeStatisticsDetail(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form statistics2.UserPrivilegeStatisticQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := statistics.GetUserPrivilegeStatisticsDetail(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}

func GetUserPrivilegeStatisticsMap(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form statistics2.UserPrivilegeStatisticMapQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, err := statistics.GetUserPrivilegeStatisticsMap(form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, data)
}
