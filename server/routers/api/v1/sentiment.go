package v1

import (
	"datacenter/models/sentiment"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 舆情监控
// @Tags WebApi
// @Produce json
// @Param id query int true "方案id"
// @Param n query int true "必填，单页条数，不大于50"
// @Param p query int true "必填，页码，不小于1"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/sentiment_news [get]
func SentimentNews(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req sentiment.MonitoringReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	resp, err := sentiment.News(req)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, resp)
}

// @Summary 舆情监测方案
// @Tags WebApi
// @Produce json
// @Param id query int true "方案id"
// @Param profile query string false "关键词清单"
// @Param type query string true "必填，类型add/edit/query/delete"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/sentiment_edit [get]
func SentimentEdit(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req sentiment.MonitoringCaseReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	userID := c.GetHeader("UID")
	resp, err := sentiment.ProcProfile(&req, userID)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, resp)
}

// @Summary 舆情纠错
// @Tags WebApi
// @Produce json
// @Param feed_id query int true "新闻id"
// @Param sentiment query int false "情感分析(1-利好/2-利空/3-未知)"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/sentiment_correct [get]
func SentimentCorrect(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var req sentiment.MonitoringCorrectionReq
	if err := appG.ParseRequest(&req); err != nil {
		return
	}

	userID := c.GetHeader("UID")
	err := sentiment.Correct(&req, userID)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}
