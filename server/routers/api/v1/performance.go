package v1

import (
	"datacenter/models/performance"
	"datacenter/models/template"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/performance_service"
	"datacenter/service/user_service"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
)

/*--------------------------------------------下面是业绩计算页面------------------------------------------------*/
//请求数据结构体
type PerformanceRequest struct {
	Sort1  string `form:"sort1" json:"sort1" `   //披露时间升降序
	Sort2  string `form:"sort2" json:"sort2" `   //预测比例升降序
	Sort3  string `form:"sort3" json:"sort3" `   //预测净利润升降序
	Sort4  string `form:"sort4" json:"sort4" `   //本期预告升降序
	Sort5  string `form:"sort5" json:"sort5" `   //净利润升降序
	Sort6  string `form:"sort6" json:"sort6" `   //净利润同比升降序
	Sort7  string `form:"sort7" json:"sort7" `   //营业总收入升降序
	Sort8  string `form:"sort8" json:"sort8" `   //营业总收入同比升降序
	Sort9  string `form:"sort9" json:"sort9" `   //扣非净利润升降序
	Sort10 string `form:"sort10" json:"sort10" ` //券商预测升降序

	//报告期
	ReportPeriod string `form:"report_period" json:"report_period" binding:"required"`
	//搜索:根据股票名称,代码,行业内容搜索

	Search string `form:"search" json:"search" `
	//类型(模糊搜索,数据格式 eg:1_2_3)
	Type string `form:"type" json:"type" `
	//重要程度
	Important string `form:"important" json:"important" `
	//姓名id
	UserId string `form:"user_id" json:"user_id" `
	//是否推荐
	Recommend string `form:"recommend" json:"recommend" `

	//披露时间(开始时间)
	DisclosureStartTime string `form:"disclosure_start_time" json:"disclosure_start_time,omitempty" `
	//披露时间(结束时间)
	DisclosureEndTime string `form:"disclosure_end_time" json:"disclosure_end_time,omitempty"`

	//页码
	Page int `form:"page" json:"page" binding:"gte=1"`
	//每页展示条数
	PageSize int `form:"page_size" json:"page_size" binding:"gte=1"`
}

//返回数据结构体
type PerformanceResponse struct {
	Data  []performance_service.GetPerformanceMysql `json:"data"`
	Count int                                       `json:"count"`
}

//数据库返回数据绑定结构体

// @Summary 业绩计算页面
// @Tags WebApi
// @Produce json
// @Param report_period query string true "报告期,eg:2020-06-09"
// @Param search query string false "搜索条件,输入内容后端会自动识别,eg:000001,当没有搜索条件输入时,内容为空字符串"
// @Param type query string false "类型(下拉框),全部:空字符串,妖股:1,业绩增长:2"
// @Param important query string false "重要程度,全部:空字符串,非常重要:1,重要:2"
// @Param user_id query int false "用户名(此字段传递用户id,eg:1),全部:空字符串"
// @Param recommend query string false "是否推荐,是:1,否:2"
// @Param disclosure_start_time query string false "披露开始时间,eg:2020-06-08"
// @Param disclosure_end_time query string false "披露结束时间,eg:2020-06-10"
// @Param sort1 query string false "披露时间排序开关,asc(升序) 或者 desc(降序)"
// @Param sort2 query string flase "预测比例排序开关,asc(升序) 或者 desc(降序)"
// @Param sort3 query string false "预测净利润排序开关,asc(升序) 或者 desc(降序)"
// @Param sort4 query string false "本期预告排序开关,asc(升序) 或者 desc(降序)"
// @Param sort5 query string false "净利润排序开关,asc(升序) 或者 desc(降序)"
// @Param sort6 query string false "净利润同比排序开关,asc(升序) 或者 desc(降序)"
// @Param sort7 query string false "营业总收入排序开关,asc(升序) 或者 desc(降序)"
// @Param sort8 query string false "营业总收入同比排序开关,asc(升序) 或者 desc(降序)"
// @Param sort9 query string false "扣非净利润排序开关,asc(升序) 或者 desc(降序)"
// @Param page query int true "页码,eg:1"
// @Param page_size query int true "每页展示条数,eg:10"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/performance [get]
func Performance(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	emptyArray := []string{}

	var (
		err  error
		pReq PerformanceRequest
		pRsp PerformanceResponse
	)

	//绑定请求数据到结构体
	if err = appG.ParseRequest(&pReq); err != nil {
		return
	}
	//fmt.Printf("请求结构体:%#v\n", pReq)

	//从model获取返回数据
	pRsp.Data, pRsp.Count, err = template.FetchPerformance(
		pReq.Sort1,
		pReq.Sort2,
		pReq.Sort3,
		pReq.Sort4,
		pReq.Sort5,
		pReq.Sort6,
		pReq.Sort7,
		pReq.Sort8,
		pReq.Sort9,
		pReq.Sort10,
		pReq.Search,
		pReq.ReportPeriod,
		pReq.Type,
		pReq.Important,
		pReq.UserId,
		pReq.Recommend,
		pReq.DisclosureStartTime,
		pReq.DisclosureEndTime,
		pReq.Page,
		pReq.PageSize,
	)
	if err != nil && err != gorm.ErrRecordNotFound {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	if len(pRsp.Data) == 0 {
		appG.Response(200, e.SUCCESS, emptyArray)
		return
	} else {
		appG.Response(200, e.SUCCESS, pRsp)
	}
}

//根据系统时间,返回4个报告期
//过了截止时间,展示下一个报告期
//看后面的时间,来决定前面的展示
//下拉框:上面第一个是当前的,下面4个是最近的4个，历史的，剩下的n个是 爬虫那边抓取的,按照报告期将序排序从数据库中获取
// @Summary 获取报告期
// @Tags WebApi-业绩
// @Produce json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/get_report_period [get]
func GetReportPeriod(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		resMap     []string
		dateSwitch string
	)
	nowDate := util.GenDateFormat()
	//当前时间所属的报告期
	dateSwitch = template.GetRecentReportPeriod(nowDate)
	fmt.Println("报告期:", dateSwitch)

	//现在已经算出 当前日期所属的报告期,之后再从数据库里面查在这个报告期之前的n个报告期
	res, err := template.GetReportPeriod(dateSwitch)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	fmt.Printf("res:%#v\n", res)

	resMap = append(resMap, dateSwitch)

	for _, v := range res {
		resMap = append(resMap, v.ReportPeriod[0:10])
	}

	appG.Response(200, e.SUCCESS, resMap)
}

// @Summary 获取用户信息接口
// @Tags WebApi-业绩
// @Produce json
// @Param user_name query string false "姓名,为空返回所有用户,不为空,返回指定用户"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/get_user_info [get]
func GetUserInfo(c *gin.Context) {
	var u_s_g_u_req user_service.GetUserInfoRequest
	var err error
	appG := app.Gin{C: c}
	if err = appG.ParseRequest(&u_s_g_u_req); err != nil {
		return
	}
	res := template.GetUserInfo(u_s_g_u_req)
	appG.Response(200, e.SUCCESS, res)
}

type GetEditPerformanceRequest struct {
	Code           string `form:"code" json:"code" binding:"required"`                   //股票代码
	Name           string `form:"name" json:"name" binding:"required"`                   //股票名称
	ReportPeriod   string `form:"report_period" json:"report_period" binding:"required"` //报告期 date
	DisclosureTime string `form:"disclosure_time" json:"disclosure_time"`                //披露时间 date
}

type GetEditPerformanceResponse struct {
	Data template.EditGetPerformance `json:"data"`
}

type PostPerformanceEditRequest struct {
	Id        int    `json:"id"`
	Code      string `form:"code" json:"code" binding:"required"` //股票代码
	Type      string `form:"type" json:"type"`                    //类型,多选下拉框,eg:1_2_3
	Important string `form:"important" json:"important" `         //重要程度,1 非常重要,2 重要
	Recommend string `form:"recommend" json:"recommend" `         //是否推荐,1 是,2否

	ReportPeriod   string `form:"report_period" json:"report_period"  binding:"required"` //报告期
	DisclosureTime string ` form:"disclosure_time" json:"disclosure_time"`                //披露时间

	ForecastProportion string `form:"forecast_proportion" json:"forecast_proportion"` //预测比例

	ForecastNetProfit    string `form:"forecast_net_profit" json:"forecast_net_profit"`      //预测净利润
	BrokerProportion     string ` form:"broker_proportion" json:"broker_proportion" `        //券商预测
	BrokerInterpretation string `form:"broker_interpretation" json:"broker_interpretation" ` //券商解读
	HighLights           string `form:"high_lights" json:"high_lights"`                      //预测内容
}

type PostPerformanceEditResponse struct {
	Data bool `json:"data"`
}

// @Summary 业绩编辑修改(post):更新数据
// @Tags WebApi-业绩
// @Produce json
// @Param code query string true "股票代码,eg:000001"
// @Param type query string false "类型,前端为多选,所以:1 妖股,2 业绩增长,提交格式为:1_2(每个数字之间使用下划线拼接)"
// @Param important query string false "重要程度:1 非常重要， 2重要 3 不重要"
// @Param recommend query string false "是否推荐:  1 是, 2 否"
// @Param forecast_proportion query string true "预测比例"
// @Param forecast_net_profit query string true "预测净利润"
// @Param broker_proportion query string true "券商预测"
// @Param broker_interpretation query string true "券商解读"
// @Param high_lights query string true "亮点"
// @Param report_period query string true "报告期,eg:2020-06-30"
// @Param disclosure_time query string true "披露时间,eg:2020-06-11"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/post_edit_performance [post]
func PostPerformanceEdit(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		err   error
		p_req PostPerformanceEditRequest
		p_res PostPerformanceEditResponse
	)

	if err = appG.ParseRequest(&p_req); err != nil {
		return
	}
	fmt.Printf("请求结构体:p_req:%#v\n", p_req)
	p_res.Data, err = template.PostEditPerformance(
		p_req.Id,
		p_req.Code,
		p_req.Type,
		p_req.Important,
		p_req.Recommend,
		p_req.ForecastProportion,
		p_req.ForecastNetProfit,
		p_req.HighLights,
		p_req.ReportPeriod,
		p_req.DisclosureTime,
		p_req.BrokerProportion,
		p_req.BrokerInterpretation,
	)
	if err != nil {
		fmt.Println(err)
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(200, e.SUCCESS, p_res)

}

/*--------------------------------------------下面是股票维护页面------------------------------------------------*/
type SaveStockMaintainRequest struct {
	Code     string `json:"code" form:"code" binding:"required"`         //股票代码
	Name     string `form:"name" json:"name" binding:"required"`         //股票名称
	Industry string `form:"industry" json:"industry" binding:"required"` //行业名称
	UserName string `form:"user_name" json:"user_name"`                  //用户名

}
type SaveStockMaintainResponse struct {
	Data bool `json:"data"`
}

// @Summary 股票维护页面:添加数据
// @Tags WebApi-业绩
// @Produce json
// @Param code query string true "股票代码"
// @Param name query string true "股票名称"
// @Param industry query string true "行业名称"
// @Param user_name query string false "用户名,默认空"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_maintain [post]
func SaveStockMaintain(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		err       error
		s_s_m_req SaveStockMaintainRequest
		s_s_m_res SaveStockMaintainResponse
	)

	if err = appG.ParseRequest(&s_s_m_req); err != nil {
		return
	}
	s_s_m_res.Data, err = template.SaveStockMaintain(s_s_m_req.Name, s_s_m_req.Industry, s_s_m_req.UserName)
	if err != nil && err != gorm.ErrRecordNotFound {
		fmt.Println(err)
		appG.Response(200, e.ERROR, err.Error())
		return
	}

	appG.Response(200, e.SUCCESS, s_s_m_res)

}

//股票行业查询
type StockMaintainRequest struct {
	Info     string `form:"info" json:"info"`                              //股票信息
	Industry string `form:"industry" json:"industry" `                     //行业名称
	UserId   string `form:"user_id" json:"user_id"`                        //用户id
	Page     int    `form:"page" json:"page" binding:"required"`           //页码
	PageSize int    `form:"page_size" json:"page_size" binding:"required"` //每页显示条数
}

type StockMaintainResponse struct {
	Data  []template.SearchStockMaintain `json:"data"`
	Count int                            `json:"count"`
}

// @Summary 股票维护-查询页面
// @Tags WebApi-业绩
// @Produce json
// @Param info query string false "股票信息,eg:000001，默认空"
// @Param industry query string false "行业名称，默认空"
// @Param user_id query string false "用户id,全部为空"
// @Param page query string true "页码"
// @Param page_size query string true "每页显示条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/search_stock_maintain [get]
func SearchStockMaintain(c *gin.Context) {
	appG := app.Gin{C: c}

	var (
		err     error
		s_m_req StockMaintainRequest
		s_m_res StockMaintainResponse
	)

	if err = appG.ParseRequest(&s_m_req); err != nil {
		return
	}
	fmt.Printf("请求结构体:%#v\n", s_m_req)

	s_m_res.Data, s_m_res.Count, err = template.SearchStockMaintainData(s_m_req.Info, s_m_req.Industry, s_m_req.UserId, s_m_req.Page, s_m_req.PageSize)
	if err != nil && err != gorm.ErrRecordNotFound {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(200, e.SUCCESS, s_m_res)
}

type GetStockMaintainRequest struct {
	Id int `json:"id" form:"id" binding:"required"`
}

type EditStockRequest struct {
	Code     string `form:"code" json:"code"  binding:"required"`
	Id       int    `form :"id" json:"id" binding:"required"`
	Industry string `form:"industry" json:"industry" binding:"required"`
	UserId   string `form:"user_id" json:"user_id" `
	UserName string `form:"user_name" json:"user_name"`
}

// @Summary 股票维护:修改(只修改行业名称和姓名)
// @Tags WebApi-业绩
// @Produce json
// @Param info query string true "id"
// @Param industry query string true "行业名称"
// @Param code query string true "股票代码"
// @Param user_id query string false "用户id"
// @Param user_name query string false "用户名"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/edit_stock_maintain [post]
func EditStockMaintain(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		err     error
		e_s_req EditStockRequest
	)

	if err = appG.ParseRequest(&e_s_req); err != nil {
		return
	}
	fmt.Printf("请求结构体:%#v\n", e_s_req)

	success, err := template.EditStockMaintain(e_s_req.Code, e_s_req.Id, e_s_req.UserId, e_s_req.Industry, e_s_req.UserName)
	if err != nil {
		fmt.Println(err)
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(200, e.SUCCESS, success)
}

type DelStockRequest struct {
	Id   int    `json:"id" form:"id" binding:"required"`     //行业id
	Code string `json:"code" form:"code" binding:"required"` //股票代码
}

//(删除u_stock_industry_relationship中的关系,并且删除u_industry中的对应数据)
// @Summary 股票维护:删除
// @Tags WebApi-业绩
// @Produce json
// @Param id query string true "id"
// @Param code query string true "股票代码"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/del_stock_maintain [post]
func DelStockMaintain(c *gin.Context) {
	appG := app.Gin{C: c}

	var (
		d_s_req DelStockRequest
	)

	if err := appG.ParseRequest(&d_s_req); err != nil {
		return
	}

	success, err := template.DelStockMaintain(d_s_req.Id, d_s_req.Code)

	if err != nil {
		fmt.Println(err)
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(200, e.SUCCESS, success)

}

type ImportExcel struct {
	Code     string `json:"code"`
	Name     string `json:"name"`
	Industry string `json:"industry"`
	UserName string `json:"user_name"`
}

// @Summary 股票维护:导入excel
// @Tags WebApi-业绩
// @Produce json
// @Param file query string true "excel文件"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/import_stock_maintain [post]
func ImportStockMaintain(c *gin.Context) {

	appG := app.Gin{C: c}

	file, _, err := c.Request.FormFile("file")
	if err != nil {
		logging.Warn(err)
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	fmt.Println("上传excel测试")
	fmt.Println(file)

	sucNum, failNum, err := template.Import(file)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}

	resMap := make(map[string]interface{})
	resMap["sucNum"] = sucNum
	resMap["failNum"] = failNum

	appG.Response(http.StatusOK, e.SUCCESS, resMap)

}

// @Summary 业绩计算导入excel
// @Produce  json
// @Tags WebApi
// @Param file query string true "文件"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/import_performance_excel [post]
func ImportPerformanceExcel(c *gin.Context) {

	appG := app.Gin{C: c}

	file, _, err := c.Request.FormFile("file")
	if err != nil {
		logging.Warn(err)
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	err = template.Import2(file)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
	}

	appG.Response(http.StatusOK, e.SUCCESS, "")
}

/*--------------------------------------------下面行业维护页面------------------------------------------------*/
type SaveIndustryRequest struct {
	Industry               string `json:"industry" form:"industry" binding:"required"`
	IndustryInterpretation string `json:"industry_interpretation" form:"industry"`
	//UserName               string `json:"user_name" form:"user_name" binding:"required"`
}

// @Summary 行业维护:添加
// @Tags WebApi-业绩
// @Produce json
// @Param industry query string true "行业名称"
// @Param industry_interpretation query string false "行业解读"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/save_industry [post]
func SaveIndustry(c *gin.Context) {
	appG := app.Gin{C: c}

	var (
		s_i_req SaveIndustryRequest
	)

	if err := appG.ParseRequest(&s_i_req); err != nil {
		return
	}
	success, err := template.SaveIndustry(s_i_req.Industry, s_i_req.IndustryInterpretation)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(200, e.SUCCESS, success)

}

type SearchIndustryRequest struct {
	Industry string `json:"industry" form:"industry"`
	Page     int    `json:"page" form:"page" binding:"required"`
	PageSize int    `json:"page_size" form:"page_size" binding:"required"`
}
type SearchIndustryResponse struct {
	Data  []template.UPerformanceIndustry `json:"data"`
	Count int                             `json:"count"` //总条数
}

// @Summary 行业维护:查询
// @Tags WebApi-业绩
// @Produce json
// @Param industry query string false "行业名称"
// @Param page query string true "页码"
// @Param page_size query string true "每页条数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/search_industry [get]
func SearchIndustry(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		s_i_req SearchIndustryRequest
		s_i_res SearchIndustryResponse
		err     error
	)

	if err := appG.ParseRequest(&s_i_req); err != nil {
		return
	}
	s_i_res.Data, s_i_res.Count, err = template.SearchIndustry(s_i_req.Industry, s_i_req.Page, s_i_req.PageSize)
	if err != nil {
		fmt.Println(err)
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(200, e.SUCCESS, s_i_res)
}

type GetEditIndustryRequest struct {
	Id int `json:"id" form:"id" binding:"required"`
}
type GetEditIndustryResponse struct {
	Data template.GetEditIndustrMysql `json:"data"`
}

type PostEditIndustryRequest struct {
	Id                     int    `json:"id" form:"id" binding:"required"`
	IndustryInterpretation string `json:"industry_interpretation" form:"industry_interpretation"`
}

// @Summary 行业维护:修改(post)
// @Tags WebApi-业绩
// @Produce json
// @Param id query string true "id"
// @Param industry_interpretation query string false "行业解读"
// @Param industry_interpretation query string false "行业解读"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/post_edit_industry [post]
func PostEditIndustry(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		g_e_i_req PostEditIndustryRequest
	)
	if err := appG.ParseRequest(&g_e_i_req); err != nil {
		return
	}
	success, err := template.PostEditIndustry(g_e_i_req.Id, g_e_i_req.IndustryInterpretation)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(200, e.SUCCESS, success)
}

// @Summary 行业维护:删除
// @Tags WebApi-业绩
// @Produce json
// @Param id query string true "id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/del_industry [post]
func DelIndustry(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		g_e_i_req GetEditIndustryRequest
	)

	if err := appG.ParseRequest(&g_e_i_req); err != nil {
		return
	}
	success, err := template.DelEditIndustry(g_e_i_req.Id)
	if err != nil {
		fmt.Println(err)
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(200, e.SUCCESS, success)
}

/*--------------------------------------------下面行业跟踪页面------------------------------------------------*/

type SearchIndustryTrackResponse struct {
	Data  []util.SearchIndustryTrackMysql `json:"data"`
	Count int                             `json:"count"`
}

// @Summary 行业跟踪:搜索
// @Tags WebApi-业绩
// @Produce json
// @Param sort query string true "默认为空,刚进页面为空,eg:披露时间升序,sort=1_asc,降序 sort=1_desc,你如你想按照别的字段排序,eg sort=2_asc,sort=22_asc"
// @Param is_first query string true "只有刚进页面的时候为true,其余时候全部为false"
// @Param search query string false "搜索条件,输入内容后端会自动识别,eg:000001"
// @Param report_period query string true "报告期 eg:2020-06-30"
// @Param forecast_increase_day query int true "预测天数"
// @Param announcement_increase_day query int true "公告天数"
// @Param user_id query string false "用户id,eg:1"
// @Param page query int true "页码,eg:1"
// @Param page_size query int true "每页展示条数,eg:10"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/search_industry_track [get]
func SearchIndustryTrack(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		s_i_t_req performance_service.SearchIndustryTrackRequest
		s_i_p_req performance.SearchIndustryTrackRequest
		s_i_t_res SearchIndustryTrackResponse
		err       error
	)

	if err := appG.ParseRequest(&s_i_t_req); err != nil {
		return
	}
	if err := appG.ParseRequest(&s_i_p_req); err != nil {
		return
	}

	s_i_t_res.Data, s_i_t_res.Count, err = template.SearchIndustryTrack(s_i_t_req)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}
	appG.Response(200, e.SUCCESS, s_i_t_res)
}

type GetEditIndustryTrackRequest struct {
	Code string `json:"code" form:"code" binding:"required"`
}
type GetEditIndustryTrackResponse struct {
	Data template.UPerformanceEdit `json:"data"`
}

type PostEditIndustryTrackRequest struct {
	Code                 string `json:"code" form:"code" binding:"required"`
	IndividualShareTrack string `json:"individual_share_track" form:"individual_share_track"`
	ReportPeriod         string `json:"report_period" form:"report_period" binding:"required"`
}

type StockSearchRequest struct {
	Search string `json:"search" form:"search" binding:"required"`
}

// @Summary 股票,股票代码模糊搜索
// @Tags WebApi-业绩
// @Produce json
// @Param search query string true "search内容"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/stock_search [get]
func StockSearch(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		s_s_req StockSearchRequest
		err     error
	)

	if err = appG.ParseRequest(&s_s_req); err != nil {
		return
	}
	res := template.StockSearch(s_s_req.Search)
	if err != nil && err != gorm.ErrRecordNotFound {
		appG.Response(200, e.ERROR, err.Error())
		return
	}

	appG.Response(200, e.SUCCESS, res)
}

// @Summary 行业跟踪(post):修改
// @Tags WebApi-业绩
// @Produce json
// @Param code query string true "股票代码"
// @Param individual_share_track query string false "行业跟踪修改内容"
// @Param ReportPeriod query string true "行业跟踪修改内容"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/post_edit_industry_track [post]
func PostEditIndustryTrack(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		e_i_t_req PostEditIndustryTrackRequest
	)

	httpCode, errCode := app.BindAndValid(c, &e_i_t_req)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	success, err := template.PostEditIndustryTrack(e_i_t_req.Code, e_i_t_req.IndividualShareTrack,e_i_t_req.ReportPeriod)
	if err != nil {
		appG.Response(200, e.ERROR, err.Error())
		return
	}

	appG.Response(200, e.SUCCESS, success)
}
