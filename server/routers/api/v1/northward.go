package v1

import (
	northward2 "datacenter/controller/northward"
	"datacenter/models/northward"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

func NorthWardList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form northward.NorthWardQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	data, cnt, err := northward2.GetList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"list":     data,
		"total":    cnt,
		"page_num": form.PageNum,
	})
}

func NorthWardLatestTime(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
	)
	time, err := northward2.GetLatestTime()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, time)
}
