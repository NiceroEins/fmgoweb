package v1

import (
	"datacenter/controller/storage"
	"datacenter/models/admin"
	"datacenter/models/user"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/pkg/gredis"
	"datacenter/pkg/util"
	"fmt"
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type LoginRequest struct {
	Username string `json:"username" form:"username" valid:"Required; MaxSize(50)"`
	Pwd      string ` json:"pwd" form:"pwd" valid:"Required; MaxSize(50)"`
}

type LoginReq struct {
	Username string `json:"username" form:"username" binding:"required"`
	Password string `json:"password" form:"password" binding:"required"`
}

// @Summary 登陆,登陆成功返回token,一个账号同时只能一个人登录
// @Produce  json
// @Tags WebApi
// @Param username query string true "用户名,eg : admin"
// @Param pwd query string true "密码,eg : b9844afdf93df0eeb83b320c9c66f0ff"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/login [post]
func Login(c *gin.Context) {
	appG := app.Gin{C: c}
	var loginReq LoginRequest
	//var friday_dev_mgr_apitoken string
	valid := validation.Validation{}
	if err := appG.ParseRequest(&loginReq); err != nil {
		return
	}
	fmt.Printf("a:%#v\n", loginReq)
	ok, _ := valid.Valid(&loginReq)

	if !ok {
		app.MarkErrors(valid.Errors)
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}

	userId, realName, isExist, err := user.Check(loginReq.Username, loginReq.Pwd, c.ClientIP())
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}

	if !isExist {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	//生成token
	token, err := util.GenerateToken(userId, loginReq.Username, loginReq.Pwd, "admin")
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}

	//登陆成功,返回token,用户id,用户真实姓名
	//同时登陆老系统,并返回老系统的api_token
	//oldWebUrl := "https://friday.54ace.com/mgrapi/login"
	postData := make(map[string]string)
	postData["mobile"] = loginReq.Username
	postData["pwd"] = loginReq.Pwd
	//str := curl.SendWithFormData(oldWebUrl, &postData)
	//if str != "" {
	//	friday_dev_mgr_apitoken = str[25:57]
	//}

	//if _friday_dev_mgr_apitoken == "" {
	//	appG.Response(http.StatusOK, e.ERROR, "老系统登录失败")
	//	return
	//}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"token":    token,
		"user_id":  userId,
		"realname": realName,
		//"friday_dev_mgr_apitoken": friday_dev_mgr_apitoken,
	})
}

//type LogoutRequest struct {
//	Token string `json:"token" form:"token"`
//}

// @Summary 退出登录
// @Produce  json
// @Tags WebApi
// @Param token query string true "token"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router  /api/v1/logout [post]
func Logout(c *gin.Context) {
	appG := app.Gin{C: c}
	//var l_o_req LogoutRequest
	token := c.GetHeader("Token")
	if token == "" {
		return
	}
	//if err := appG.ParseRequest(&l_o_req); err != nil {
	//	return
	//}
	err := gredis.Set("logoutlist:"+token, 1, 60*60*24*7)
	if err != nil {
		appG.Response(200, e.ERROR, fmt.Errorf("退出登录失败"))
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, "退出登录成功")
	return
}

func TestRedis(c *gin.Context) {
	//err := gredis.Set("ipo:"+strconv.Itoa(1),0,60*60*24)
	//if err !=nil{
	//	fmt.Println(err)
	//}

	//redisNum := gredis.IncrBy("ipo:"+strconv.Itoa(1), 1)
	//fmt.Println(redisNum)

	//gredis.SAdd("ipo:third_req_times_"+strconv.Itoa(1), strconv.Itoa(789))

	a := storage.IpoPushCondition(1)
	fmt.Println(a)

}

func LoginDati(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  LoginReq
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	userId,userName,err:=admin.GetUserInfoByNameAndPwd(form.Username,form.Password)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	token, err := util.GenerateToken(userId, form.Username, form.Password, "admin")
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"accessToken":    token,
		"user_id":  userId,
		"user_name": userName,
		//"friday_dev_mgr_apitoken": friday_dev_mgr_apitoken,
	})


}

func GetAdminUserInfo(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
	)
	uidStr := appG.C.GetHeader("UID")
	uid,err:=strconv.Atoi(uidStr)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	uservo,err:=admin.GetUserInfoById(uid)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"user":uservo,
		//"friday_dev_mgr_apitoken": friday_dev_mgr_apitoken,
	})


}
