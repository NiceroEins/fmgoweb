package v1

import (
    "datacenter/models/fund"
    "datacenter/models/template"
    "datacenter/pkg/app"
    "datacenter/pkg/e"
    "datacenter/pkg/logging"
    "datacenter/service/fund_service"
    "github.com/gin-gonic/gin"
    "github.com/unknwon/com"
    "net/http"
    "strconv"
    "time"
)

// @Summary 基金分红列表
// @Produce  json
// @Tags WebApi-基金分红
// @Param page_num query int true "页码"
// @Param page_size query int true "条数"
// @Param user_id query string false "用户姓名"
// @Param fund_id query int false "基金id"
// @Param bonus_type query string false "分红方式 默认全部为‘’"
// @Param register_date query string false "登记日期"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_bonus_list [get]
func FundBonusList(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        form fund.BonusFundRequest
        err        error
        searchDate time.Time
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    if form.RegisterDate  != ""{
        searchDate,err = time.Parse("2006-01-02",form.RegisterDate)
        if err != nil {
            appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
            return
        }
    }
    bonusService := fund_service.FundBonus{
        PageNum:  form.PageNum,
        PageSize: form.PageSize,
        UserName: form.UserName,
        FundID: form.FundID,
        BonusType: form.BonusType,
        RegisterDate: searchDate,
    }
    result, err := bonusService.GetLists()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_BONUS_FAIL, nil)
        return
    }

    count, err := bonusService.Count()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_BONUS_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": result,
        "total": count,
    })
}

// @Summary 基金分红添加
// @Produce  json
// @Tags WebApi-基金分红
// @Param fund_id body int true "基金id"
// @Param user_id body int true "用户id"
// @Param invest_num body string true "再投资份额"
// @Param register_date body string true "登记时间"
// @Param unit_bonus body string true "每单位分红"
// @Param cash_bonus body string true "现金红利"
// @Param invest_money body string true "再投资金额"
// @Param invest_unit_value body string true "再投资单位净值"
// @Param bonus_type body string true "分红方式 1代表现金分红 2代表红利再投资"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_bonus_add [post]
// @Tags Test
func FundBonusAdd(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form fund.BonusFundAddRequest
        err  error
        registerDate time.Time
        number int
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    if form.RegisterDate  != ""{
        registerDate,err = time.Parse("2006-01-02",form.RegisterDate)
        if err != nil {
            appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
            return
        }
    }

    checkNumFormate,err := template.CheckNumParms(form.InvesteMoney,form.InvestNum,form.UnitBonus,form.CashBonus,form.InvesteUnitValue)
    bonusService := fund_service.FundBonus{
        Created: time.Now(),
        FundID: form.FundID,
        UserID: form.UserID,
        InvesteMoney: checkNumFormate["investeMoney"],
        BonusType: form.BonusType,
        InvestNum: checkNumFormate["investNum"],
        UnitBonus: checkNumFormate["unitBonus"],
        CashBonus: checkNumFormate["cashBonus"],
        InvesteUnitValue: checkNumFormate["cnvesteUnitValue"],
        RegisterDate: registerDate,
    }
    _,err = fund_service.ExistByFund(form.FundID)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_NOT_EXISTS, nil)
        return
    }
    _,err = fund_service.ExistByCustomer(form.UserID)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_NOT_EXISTS, nil)
        return
    }
    number,err = fund_service.ExistParams(form.UserID,form.FundID,registerDate)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_NOT_EXISTS, nil)
        return
    }
    if number > 0 {
        appG.Response(http.StatusOK, e.ERROR_BONUS_DUPLICATION, nil)
        return
    }
    err = bonusService.Add()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_BONUS_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 基金分红删除
// @Produce  json
// @Tags WebApi-基金分红
// @Param id path int true "ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_bonus_del [get]
// @Tags Test
func FundBonusDel(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form = fund.BonusDelByIDRequest{ID: com.StrTo(c.Param("id")).MustInt()}
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    bonusService := fund_service.FundBonus{
        ID:         form.ID,
    }
    _, err := bonusService.Exist()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_BONUS_NOT_EXISTS, nil)
        return
    }

    err = bonusService.Del()
    if err != nil {
        appG.Response(http.StatusInternalServerError, e.ERROR_BONUS_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 基金分红编辑
// @Produce  json
// @Tags WebApi-基金分红
// @Param id path int true "ID"
// @Param fund_id body int true "基金id"
// @Param user_id body int true "用户id"
// @Param invest_num body string true "再投资份额"
// @Param register_date body string true "登记时间"
// @Param unit_bonus body string true "每单位分红"
// @Param cash_bonus body string true "现金红利"
// @Param invest_money body string true "再投资金额"
// @Param invest_unit_value body string true "再投资单位净值"
// @Param bonus_type body string true "分红方式 1代表现金分红 2代表红利再投资"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_bonus_edit [post]
// @Tags Test
func FundBonusEdit(c *gin.Context) {
    var (
        appG = app.Gin{C: c}
        form  fund.BonusFundEditRequest
        err  error
        registerDate time.Time
        number int
    )

    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }
    if form.RegisterDate  != ""{
        registerDate,err = time.Parse("2006-01-02",form.RegisterDate)
        if err != nil {
            appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
            return
        }
    }
    form.UserID,_ = strconv.Atoi(c.GetHeader("UID"))
    //investeMoneyStr,investNumStr,unitBonusStr,cashBonusStr,investeUnitValueStr
    checkNumFormate,err := template.CheckNumParms(form.InvesteMoney,form.InvestNum,form.UnitBonus,form.CashBonus,form.InvesteUnitValue)
    if err != nil {
        appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
        return
    }
    bonusService := fund_service.FundBonus{
        ID:     form.ID,
        FundID: form.FundID,
        UserID: form.UserID,
        InvesteMoney: checkNumFormate["investeMoney"],
        BonusType: form.BonusType,
        InvestNum: checkNumFormate["investNum"],
        UnitBonus: checkNumFormate["unitBonus"],
        CashBonus: checkNumFormate["cashBonus"],
        InvesteUnitValue: checkNumFormate["investeUnitValue"],
        RegisterDate: registerDate,
    }

    _,err = fund_service.ExistByFund(form.FundID)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_FUND_NOT_EXISTS, nil)
        return
    }
    _,err = fund_service.ExistByCustomer(form.UserID)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_NOT_EXISTS, nil)
        return
    }
    _, err = bonusService.Exist()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_BONUS_NOT_EXISTS, nil)
        return
    }
    number,err = fund_service.ExistParams(form.UserID,form.FundID,registerDate)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_CUSTOMER_NOT_EXISTS, nil)
        return
    }
    if number > 1 {
        appG.Response(http.StatusOK, e.ERROR_BONUS_DUPLICATION, nil)
        return
    }
    err = bonusService.Edit()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_BONUS_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, nil)
}


// @Summary 基金分红导入
// @Produce  json
// @Tags WebApi-基金分红
// @Param file body string true "Excel File"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/fund_bonus_import [post]
// @Tags Test
func FundBonusImport(c *gin.Context) {
    appG := app.Gin{C: c}
    file, _, err := c.Request.FormFile("file")
    var (
        failRaws  = 0
        successRaws  = 0
    )
    if err != nil {
        logging.Info(err)
        appG.Response(http.StatusInternalServerError, e.ERROR, nil)
        return
    }

    bonusService := fund_service.FundBonus{}
    failRaws,successRaws,err = bonusService.Import(file)
    if err != nil {
        logging.Info(err)
        appG.Response(http.StatusOK, e.ERROR_BONUS_FAIL, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "failRaws": failRaws,
        "successRaws": successRaws,
    })
}

// @Summary 获取分红公告内容
// @Produce  json
// @Tags WebApi-基金页面接口
// @Param bonus_id query int true "分红公告ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v2/customer_bonus_notice [get]
func FundBonusNoticeById(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        err  error
        form fund.FundBonusRequest
    )
    httpCode, errCode := app.BindAndValid(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, nil)
        return
    }

    fundBonusNoticeService := fund_service.FundBonus{
        ID: form.BonusID,
    }
    ret , err := fund_service.ExistByID(form.BonusID)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_USER_BONUS_NOTICE_FAIL, nil)
        return
    }
    if !ret {
        appG.Response(http.StatusOK, e.ERROR_BONUS_NOT_EXISTS, nil)
        return
    }
    notice,err := fundBonusNoticeService.GetFundNotice()
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR_USER_BONUS_NOTICE_FAIL, nil)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "notice": notice,
    })
}

