package v1

import (
	"datacenter/models/admin"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/pkg/setting"
	"datacenter/service/crud_service"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"time"
)

func AddQuestion(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.QuestionIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err:=admin.AddQuestion(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func EditQuestion(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.QuestionIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err:=admin.EditQuestion(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func DelQuestion(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.QuestionIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err:=admin.DelQuestion(form.Id)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func GetQuestions(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.QuestionQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	vos,cnt,err:=admin.GetQuestions(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List: vos,
		Total: cnt,
	})
}

func GetRandQuestions(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.UserBaseIO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	vos,done,err:=admin.GetRandQuestions(form.Openid)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"list":vos,
		"done":done,
	})
}

func GetQuestionLog(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.QuestionLogQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	vos,err:=admin.GetUserQuestionLog(form.Openid)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List: vos,
	})
}

func GetQuestionLogDetail(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.QuestionLogQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	vos,err:=admin.GetUserQuestionLogDetail(form.Openid,form.Date)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, crud_service.Response{
		List: vos,
	})
}

func SetQuestionLog(c *gin.Context)  {
	var (
		appG = app.Gin{C: c}
		form  admin.Answers
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	vos,err:=admin.SetQuestionLog(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, vos)
}

func DelQuestionLog(c *gin.Context)  {
	var (
		appG = app.Gin{C: c}
		form  admin.QuestionLogQO
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err:=admin.DelUserQuestionLog(form.Openid)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

//单张图片上传
func UploadHandler(c *gin.Context) {
	var appG = app.Gin{
		C: c,
	}
	file, err := appG.C.FormFile("file")
	appid:=appG.C.PostForm("app_id")
	if err != nil {
		appG.Response(http.StatusOK,e.INVALID_PARAMS,"文件接收失败")
		return
	}

	path:=""
	fileName:=strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond),10)+file.Filename
	if file !=nil {
		path =setting.AppSetting.ImageSavePath+fileName
		if err := appG.C.SaveUploadedFile(file, path); err != nil {
			appG.Response(http.StatusOK,e.INVALID_PARAMS,err)
			return
		}
		fmt.Println(appid)
	}else{
		appG.Response(http.StatusOK,e.INVALID_PARAMS,"文件上传失败")
		return
	}

	appG.Response(http.StatusOK,e.SUCCESS,"/admin/img/"+fileName)
}

func ImportQuestions(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form  admin.QuestionsArr
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err:=admin.ImportQuestions(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}
