package v1

import (
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/pkg/websocket"
	"github.com/gin-gonic/gin"
	"net/http"
)

type WSRequest struct {
	UID     string `form:"uid" json:"uid" validate:"required gt=0"`
	Token   string `form:"token" json:"token" validate:"gt=0 lte=32"`
	Channel string `form:"channel" json:"channel" validate:"oneof=recommend news"`
}

// @Summary ws接口-推荐
// @Tags WebSocket
// @Produce json
// @Param uid query string true "必填，发起人uid"
// @Param token query string true "必填，token"
// @Param channel query string true "必填，连接频道，recommend（推荐）/news（新闻）,新闻频道无需回包确认"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /ws [get]
func WSConnect(c *gin.Context) {
	//参数校验
	appG := app.Gin{C: c}

	var wsRequest WSRequest

	httpCode, errCode, msg := app.BindAndCheck(c, &wsRequest)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}

	uid := c.GetHeader("UID")
	if wsRequest.UID != "" {
		uid = wsRequest.UID
	}
	if uid == "" {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, "invalid uid")
		return
	}

	channel := c.GetHeader("Channel")
	if wsRequest.Channel != "" {
		channel = wsRequest.Channel
	}
	if channel == "" {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, "invalid channel")
		return
	}

	ws := websocket.GetInstance()
	ws.ServeHTTP(uid, func(from, to string) map[string]interface{} {
		return map[string]interface{}{
			"uid": c.GetHeader("UID"),
		}
	}, appG.C.Writer, appG.C.Request, c.ClientIP(), channel)

}
