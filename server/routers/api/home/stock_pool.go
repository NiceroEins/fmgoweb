package home

import (
	"datacenter/models/home"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"time"
)

type StockPoolListResponse struct {
	Data       []*home.StockResponse `json:"data"`
	Total      int                 `json:"total"`
	UpdateTime string              `json:"update_time"`
}

// @Summary 股池列表
// @Produce  json
// @Tags WebApi-个人中心
// @Param page_size query int true "页码"
// @Param max_id query int false "最大id"
// @Param min_id query int false "最大id"
// @Param sort_key query string false "排序字段名"
// @Param user_id query string false "用户id"
// @Param type query string false "类型"
// @Param description query string false "情况说明"
// @Param industry query string false "所属行业"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/stock_pool_manage [get]
func StockPoolGetList(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form home.StockPoolRequest
		err  error
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}

	form.UserID,_=strconv.Atoi(appG.C.GetHeader("UID"))
	data, cnt, err := home.GetStockPoolList(&form)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, err.Error())
		return
	}
	ret := StockPoolListResponse{
		Data:       data,
		Total:      cnt,
		UpdateTime: time.Now().Format("2006-01-02 15:04:05"),
	}
	appG.Response(http.StatusOK, e.SUCCESS, ret)
}
// @Summary 股池添加
// @Produce  json
// @Tags WebApi-个人中心
// @Param code body string true "股票ID"
// @Param user_id body int true "用户ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/stock_pool_add [post]
func AddStockPool(c *gin.Context)  {
	var (
		appG = app.Gin{C: c}
		form home.StockAddRequest
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	stockModel := home.StockPool{
		Code:form.Code,
		UserID:form.UserId,
		Type:form.Type,
	}
	exists,err := stockModel.ExistByCodeAndUserID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_HOME_STOCK_POOL_FAIL, nil)
		return
	}
	if exists {
		appG.Response(http.StatusOK, e.ERROR_HOME_STOCK_POOL_EXIST, nil)
		return
	}
	err = stockModel.Add()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_HOME_STOCK_POOL_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}
// @Summary 股池删除
// @Produce  json
// @Tags WebApi-个人中心
// @Param id path int true "记录ID"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/stock_pool_del [get]
func DelStockPool(c *gin.Context)  {
	var (
		appG = app.Gin{C: c}
		form home.StockDelRequest
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	stockModel := home.StockPool{
		Id: form.ID,
	}
	exists,err := stockModel.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_HOME_STOCK_POOL_FAIL, nil)
		return
	}
	if !exists {
		appG.Response(http.StatusOK, e.ERROR_HOME_STOCK_POOL_NOT_EXIST, nil)
		return
	}
	err = stockModel.Del()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_HOME_STOCK_POOL_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}
// @Summary 行业列表
// @Produce  json
// @Tags WebApi-个人中心
// @Param industry_name query string true "行业名称 模糊搜索"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/industry_names [get]
func GetIndustryNames(c *gin.Context)  {
	var (
		appG = app.Gin{C: c}
		form home.StockNamesRequest
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	maps := make(map[string]interface{})
	maps["industry_name"] = form.IndustryName
	ret,err := home.GetIndustryNames(maps)
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_HOME_STOCK_POOL_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, ret)
}