package home

import (
	"datacenter/models/home"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"github.com/gin-gonic/gin"
	"net/http"
)
type EventAddRequest struct {
	ID               int    `json:"id" form:"id"`
	UserId           int    `json:"user_id" form:"user_id"`
	ObjectId         int    `json:"object_id" form:"object_id"`
	EventStatus      int    `json:"event_status" form:"event_status"`
	EventStr         string `json:"event_str" form:"event_str"`
	ObjectType       string `json:"object_type" form:"object_type"`
	EventType        string `json:"event_type" form:"event_type"`
	PictureLink      string `json:"picture_link" form:"picture_link"`
}

type HistoryEventRequest struct {
	UserId           int       `json:"user_id" form:"user_id" binding:"required"`
	Code             string    `json:"code" form:"code" binding:"required"`
	PageSize         int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum          int       `json:"page_num" form:"page_num" binding:"required,gte=1"`
}

// @Summary 点评添加/更新
// @Produce  json
// @Tags WebApi-个人中心
// @Param user_id body int true "用户ID"
// @Param object_id body int true "对象ID"
// @Param event_status body int true "处理状态 事件状态，0未处理，1已处理"
// @Param event_str body string true "点评内容"
// @Param object_type body string true "对象类型 公告为announcement,研报为research 股池为 stock_pool "
// @Param event_type body string true "事件类型 例如 click"
// @Param picture_link body string true "图片链接 为json"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/event [post]
func EventContent(c *gin.Context)  {
	var (
		appG = app.Gin{C: c}
		form EventAddRequest
		err error
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	userEventModel := home.StockUserEvent{
		UserID:       form.UserId,
		ObjectID:     form.ObjectId,
		EventStatus:  form.EventStatus,
		EventStr:     form.EventStr,
		ObjectType:   form.ObjectType,
		EventType:    form.EventType,
		PictureLink:  form.PictureLink,
	}
    userEvent,err := userEventModel.Exists()
	if err != nil  {
		appG.Response(http.StatusOK, e.ERROR_HOME_EVENT_EXIST, nil)
		return
	}
	if userEvent.ID > 0  {
		userEventModel.ID = userEvent.ID
		err = userEventModel.Update()
	} else {
		err = userEventModel.Add()
	}
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_HOME_EVENT_EXIST, nil)
		return
	}
	go home.PushMsg(form.UserId)
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func Start(c *gin.Context)  {
	var (
		appG = app.Gin{C: c}
		form EventAddRequest
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}

	home.PushMsg(form.UserId)
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}


// @Summary 当前股票历史点评
// @Produce  json
// @Tags WebApi-个人中心
// @Param user_id body int true "用户ID"
// @Param code body string true "股票代码"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/history_events [get]
func GetHistoryEvent(c *gin.Context)  {
	var (
		appG = app.Gin{C: c}
		form HistoryEventRequest
		err error
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	userEventModel := home.HistoryEvent{
		UserID:       form.UserId,
		Code:         form.Code,
	}
	userEvent,err := userEventModel.GetHistory()
	if err != nil  {
		appG.Response(http.StatusOK, e.ERROR_HOME_EVENT_EXIST, nil)
		return
	}
	count, err := userEventModel.Count()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR_CALENDAR_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": userEvent,
		"total": count,
	})
}

