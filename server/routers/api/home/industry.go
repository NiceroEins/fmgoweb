package home

import (
    "datacenter/models/home"
    homeController "datacenter/controller/home"
    "datacenter/pkg/app"
    "datacenter/pkg/e"
    "github.com/gin-gonic/gin"
    "net/http"
    "strconv"
)

// @Summary 行业监控 - 行业列表
// @Produce  json
// @Tags WebApi-个人中心
// @Param page_size query int true "页数量"
// @Param page_num query int true "页数"
// @Param sort_key query string false "排序字段"
// @Param direction query bool false "true 升序 false 降序 默认false"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/industry_monitor [get]
func IndustryMonitor(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        form home.IndustryListReq
        err  error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    form.UserId,_ =  strconv.Atoi(c.GetHeader("UID"))
    industrys,count, err := homeController.GetIndustryMonitorList(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, err.Error())
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": industrys,
        "count":count,
    })
}

// @Summary 行业监控 - 行业折线图
// @Produce  json
// @Tags WebApi-个人中心
// @Param industry_name query string true "行业名称"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/industry_pic [get]
func IndustryPic(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        form home.IndustryPicReq
        err  error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    industrys,price,extral,err := homeController.GetIndustryPic(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": industrys,
        "price":price,
        "extral":extral,
    })
}

// @Summary 行业监控 - 行业折线图
// @Produce  json
// @Tags WebApi-个人中心
// @Param industry_name query string true "行业名称"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/industry_pic_minute [get]
func IndustryPicPerMin(c *gin.Context) {
    appG := app.Gin{C: c}
    var (
        form home.IndustryPicReq
        err  error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    industrys,price,extral,err := homeController.GetIndustryPicPerMin(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, nil)
        return
    }

    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": industrys,
        "price":price,
        "extral":extral,
    })
}
// @Summary 行业监控 - 行业股票图
// @Produce  json
// @Tags WebApi-个人中心
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/industry_industry_stock [get]
func GetIndustryStock(c *gin.Context)  {
    appG := app.Gin{C: c}
    var (
        form home.IndustryStockReq
        err  error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    industrys,count, err := homeController.GetIndustyStock(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, nil)
        return
    }
    head,err := homeController.GetTop(&form)
    if err != nil {
        appG.Response(http.StatusOK, e.ERROR, nil)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": industrys,
        "count":count,
        "head":head,
    })
}

// @Summary 当前股票历史点评
// @Produce  json
// @Tags WebApi-个人中心
// @Param user_id body int true "用户ID"
// @Param code body string true "股票代码"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/history_events [get]
func GetIndustryHistoryEvent(c *gin.Context)  {
    var (
        appG = app.Gin{C: c}
        form home.IndustryStockReq
        err error
    )
    httpCode, errCode, msg := app.BindAndCheck(c, &form)
    if errCode != e.SUCCESS {
        appG.Response(httpCode, errCode, msg)
        return
    }
    form.UserId,_ =  strconv.Atoi(c.GetHeader("UID"))
    userEvent,count,err := homeController.GetIndustryHistoryEvent(&form)
    if err != nil  {
        appG.Response(http.StatusOK, e.ERROR_HOME_EVENT_EXIST, nil)
        return
    }
    appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
        "lists": userEvent,
        "total": count,
    })
}
