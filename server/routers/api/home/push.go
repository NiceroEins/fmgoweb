package home

import (
	"datacenter/models"
	"datacenter/models/home"
	"datacenter/models/template"
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/service/home_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"time"
)

// @Summary 推送内容 - 股池推送
// @Produce  json
// @Tags WebApi-个人中心
// @Param max_id query int false "最大id"
// @Param min_id query int false "最大id"
// @Param code_name query string false "股票名称"
// @Param start query string false "开始时间"
// @Param end query string false "结束时间"
// @Param search_type query string true "搜索类型 推荐为recommend 点评为‘’"
// @Param push_type query string true "推送类型 stock_pool：股票 announcement：公告 research：研报 "
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/stock_pool_push [get]
func StockPushList(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form home.StockPushRequest
		err  error
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	if form.Sart != "" {
		_,err = time.Parse("2006-01-02",form.Sart)
		if err != nil {
			appG.Response(http.StatusOK, e.ERROR_HOME_PUSH_STOCK_FAIL, nil)
			return
		}
	}
	if form.End != "" {
		_,err = time.Parse("2006-01-02",form.End)
		if err != nil {
			appG.Response(http.StatusOK, e.ERROR_HOME_PUSH_STOCK_FAIL, nil)
			return
		}
	}
	pushStockRequest := home_service.StockPush{
		UserId:   form.UserID,
		CodeName: form.CodeName,
		Start:    form.Sart,
		End:      form.End,
		SearchType: form.SearchType,
		PageNum:    form.PageNum,
		PageSize:   form.PageSize,
	}
	result := make(map[string]interface{})
	switch form.PushType {
	case "stock_pool":
		ret,_ := pushStockRequest.GetStockPushLists()
		result["data"] = ret
		count,_ := pushStockRequest.GetStockPushListsCount()
		result["total"] = count
	case "announcement":
		ret,_ := pushStockRequest.GetAnnouncementPushLists()
		result["data"] = ret
		count,_ := pushStockRequest.GetAnnouncementPushListsCount()
		result["total"] = count
	case "research":
		ret,_ := pushStockRequest.GetReportPushLists()
		result["data"] = ret
		count,_ := pushStockRequest.GetReportPushListsCount()
		result["total"] = count
	}
	if len(result) > 0{
		appG.Response(http.StatusOK, e.SUCCESS, result)
		return
	}
	appG.Response(http.StatusOK, e.ERROR_HOME_PUSH_STOCK_FAIL, nil)
}

// @Summary 行业列表 - 行业管理
// @Produce  json
// @Tags WebApi-个人中心
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/user_industrys [get]
func IndustryNamesUser(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		err  error
	)
	industyService := home_service.IndustryNamesUser{}
	industrys, err := industyService.GetLists()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": industrys,
	})
}

// @Summary 行业添加 - 行业管理
// @Produce  json
// @Tags WebApi-个人中心
// @Param user_id body int false "用户id"
// @Param industry_names body int false "行业名称 用英文逗号隔开"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/user_industrys_edit [post]
func IndustryNamesUserEdit(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		err  error
		form      home.IndustryNamesEditRequest
		user      template.BUser
		industyAddService home_service.IndustryNameAdd
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err = models.DB().Table("b_user").Where("id = ?", form.UserId).Find(&user).Error
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
		return
	}
	industyService := home_service.IndustryNamesUser{
		UserId: form.UserId,
		UserName: user.Realname,
	}

	err = industyService.DelIndustry()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	storeData := []home_service.IndustryNameAdd{}
	for _,value := range form.IndustryNames {
		industyAddService = home_service.IndustryNameAdd{
			UserId: form.UserId,
			UserName: user.Realname,
			FirstIndustry: value.FirstIndustry,
			SecondIndustry: value.SecondIndustry,
			IndustryID: value.IndustryID,
		}
		_, err := industyAddService.AddIndustry()
		storeData = append(storeData,industyAddService)
		if err != nil {
			logging.Info("home push.IndustryNamesUserEdit edit error",err)
		}
		continue
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}
// @Summary 行业删除 - 行业管理
// @Produce  json
// @Tags WebApi-个人中心
// @Param user_id query int true "用户id"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/user_industrys_del [get]
func IndustryNamesUserDel(c *gin.Context)  {
	appG := app.Gin{C: c}
	var (
		err  error
		form      home.IndustryNamesDelRequest
		user      template.BUser
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	err = models.DB().Table("b_user").Unscoped().Where("id = ?", form.UserId).Find(&user).Error
	if err != nil {
		appG.Response(http.StatusOK, e.INVALID_PARAMS, nil)
		return
	}
	industyService := home_service.IndustryNamesUser{
		UserId: form.UserId,
	}
	// 这里的添加逻辑是先删 再加
	err = industyService.DelIndustry()
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	rediskey := "my_industry_list:" + strconv.Itoa(form.UserId)
	gredis.Delete(rediskey)
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

// @Summary 工作统计 - 统计管理
// @Produce  json
// @Tags WebApi-个人中心
// @Param date_time query string true "查询时间"
// @Param user_id query string true "用户id"
// @Param page_size query int true "每页条数"
// @Param page_num query int true "页数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/statistics/work [get]
func HomeStatistics(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form home.StaticsticRequest
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	sta := home_service.Staticstic{
		SearchStaticsticDate: form.SearchDate,
		PageSize: form.PageSize,
		PageNum:  form.PageNum,
		UserID:   form.UserID,
	}
	ret := sta.GetStatisticData()
	count := sta.GetStatisticsCount()
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
		"total": count,
	})
}

// @Summary 工作统计详情 - 统计管理
// @Produce  json
// @Tags WebApi-个人中心
// @Param date_time query string true "查询时间"
// @Param user_id query int true "用户id"
// @Param page_size query int true "每页条数"
// @Param page_num query int true "页数"
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/statistics/detail [get]
func HomeStatisticsDetail(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		form home.StaticsticRequest
	)
	httpCode, errCode, msg := app.BindAndCheck(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, msg)
		return
	}
	sta := home_service.Staticstic{
		SearchStaticsticDate: form.SearchDate,
		PageSize: form.PageSize,
		PageNum:  form.PageNum,
		UserID:   form.UserID,
	}
	ret := sta.GetStatisticsDetail()
	count := sta.GetStatisticsDetailCount()
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": ret,
		"total": count,
	})
}

