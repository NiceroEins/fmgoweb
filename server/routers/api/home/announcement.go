package home

import (
	"datacenter/pkg/app"
	"datacenter/pkg/e"
	"datacenter/service/home_service"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 公告类别 - 公告
// @Produce  json
// @Tags WebApi-个人中心
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /api/v1/home/annoucement_list [get]
func AnnouncementTypeNames(c *gin.Context) {
	appG := app.Gin{C: c}
	var (
		err error
	)
	annoucementService := home_service.AnnouncementType{}
	announcement, err := annoucementService.GetAnnouncementType()
	ret := []string{}
	for _, value := range announcement {
		ret = append(ret, value.Type)
	}
	if err != nil {
		appG.Response(http.StatusOK, e.ERROR, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, ret)
}
