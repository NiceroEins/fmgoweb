package message

import (
	"datacenter/models/message"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/pkg/websocket"
	"encoding/json"
	"time"
)

type MsgData struct {
	Type string `json:"type"`
	Msg  string `json:"msg"`
}

func Push(msg string, msgType message.MessageType, ext ...string) {
	//ws := websocket.GetInstance()
	//ws.SendToHub(msg)
	var md5 string
	conn := gredis.Clone(setting.RedisSetting.MessageDB)
	if len(ext) > 0 {
		md5 = ext[0]
	} else {
		md5 = util.EncodeMD5(msg)
	}
	data, _ := json.Marshal(MsgData{
		Type: msgType.String(),
		Msg:  msg,
	})
	if res, err := conn.HGet("message:static:system", md5); err != nil || res == nil {
		_ = conn.PushList("message:list:system", string(data))
		_ = conn.HSet("message:static:system", md5, string(data))
		_ = conn.HSet("message:static:"+time.Now().Format("20060102"), msg, time.Now().Format("2006-01-02 15:04:05"))
		//conn.PushList("message:static:system", msg)
	}
	// 投递到备份库里面 供预发布使用
	connbak := gredis.Bakup(setting.RedisbakSetting.MessageDB)
	if res, err := connbak.HGet("message:static:system", md5); err != nil || res == nil {
		_ = connbak.PushList("message:list:system", string(data))
		_ = connbak.HSet("message:static:system", md5, string(data))
		_ = connbak.HSet("message:static:"+time.Now().Format("20060102"), msg, time.Now().Format("2006-01-02 15:04:05"))
	}
}

func Pull() {
	conn := gredis.Clone(setting.RedisSetting.MessageDB)
	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	for {
		<-ticker.C
		fetchMsg(conn)
	}
}

func fetchMsg(conn *gredis.RedisConn) {
	data, err := conn.PopList("message:list:system")
	if err == nil && len(data) > 0 {
		ws := websocket.GetInstance()
		for _, v := range data {
			var msg MsgData
			if err := json.Unmarshal([]byte(v), &msg); err == nil {
				msgData := message.ParseMsg(message.MessageType(msg.Type), "system", "", msg.Msg, true)
				ws.SysSend(msgData)
			} else {
				logging.Error("message.fetchMsg() Json unmarshal Errors: ", err.Error())
			}

		}
	}
}

func Setup() {
	if setting.ServerSetting.Type == "web" {
		go Pull()
	}
}
