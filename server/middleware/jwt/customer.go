package jwt

import (
	"datacenter/models/template"
	"datacenter/pkg/gredis"
	"datacenter/pkg/setting"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"

	"datacenter/pkg/e"
	"datacenter/pkg/util"
)

//登陆中间件
func Customer() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			token   string
			uid     string
			code    int
			data    interface{}
			value   []byte
		)
		if setting.ServerSetting.RunMode != "debug" {
			code = e.SUCCESS
			token = c.Query("token")
			if token == "" {
				token = c.GetHeader("Token")
			}
			uid = c.Query("uid")
			if uid == "" {
				uid = c.GetHeader("UID")
			}
			if token == "" || uid == "" {
				code = e.INVALID_PARAMS
			} else {
				data := make(map[string]interface{})
				data["id"] = uid
				//查看账号是否已禁用,和删除
				ret, err := template.ExistCustomer(data)
				if err != nil{
					code = e.ERROR_AUTH_CHECK_TOKEN_FAIL
				}else {
					if !ret {
						c.JSON(http.StatusUnauthorized, gin.H{
							"code": e.ERROR_PLZ_LOGIN,
							"msg":  e.GetMsg(e.ERROR_PLZ_LOGIN),
							"data": "账号已被删除",
						})
						c.Abort()
						return
					}
				}
				// 修改账号后redis里的值置为空
				value, _ = gredis.Get("loginfront:" + uid)
				if string(value) == "" || strings.Trim(string(value), "\"") != token {
					//前端直接跳到登录页面
					c.JSON(http.StatusUnauthorized, gin.H{
						"code": e.ERROR_PLZ_LOGIN,
						"msg":  e.GetMsg(e.ERROR_PLZ_LOGIN),
						"data": data,
					})
					c.Abort()
					return
				}
				//验证token
				_, err = util.ParseToken(token)
				if err != nil {
					switch err.(*jwt.ValidationError).Errors {
					case jwt.ValidationErrorExpired:
						code = e.ERROR_AUTH_CHECK_TOKEN_TIMEOUT
					default:
						code = e.ERROR_AUTH_CHECK_TOKEN_FAIL
					}
				}
			}

			if code != e.SUCCESS {
				c.JSON(http.StatusUnauthorized, gin.H{
					"code": code,
					"msg":  e.GetMsg(code),
					"data": data,
				})

				c.Abort()
				return
			}
		}
	}
}
