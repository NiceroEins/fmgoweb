package jwt

import (
	"datacenter/models/user"
	"datacenter/pkg/gredis"
	"datacenter/pkg/setting"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"

	"datacenter/pkg/e"
	"datacenter/pkg/util"
)

//登陆中间件
func JWT() gin.HandlerFunc {
	//return func(c *gin.Context) {
	//	c.Next()
	//}
	return func(c *gin.Context) {
		var (
			token   string
			uid     string
			code    int
			bAccess = true
			data    interface{}
			value   []byte
			err     error
		)
		if setting.ServerSetting.RunMode != "debug" {
			code = e.SUCCESS
			token = c.Query("token")
			if token == "" {
				token = c.GetHeader("Token")
			}
			uid = c.Query("uid")
			if uid == "" {
				uid = c.GetHeader("UID")
			}
			if token == "" || uid == "" {
				code = e.INVALID_PARAMS
			} else {
				//查看账号是否已禁用,和删除
				status, deleted_at := user.UserStatus(uid)
				if status == "banned" {
					c.JSON(http.StatusUnauthorized, gin.H{
						"code": e.ERROR_PLZ_LOGIN,
						"msg":  e.GetMsg(e.ERROR_PLZ_LOGIN),
						"data": "账号已经停用",
					})
					c.Abort()
					return
				}
				if deleted_at != nil {
					c.JSON(http.StatusUnauthorized, gin.H{
						"code": e.ERROR_PLZ_LOGIN,
						"msg":  e.GetMsg(e.ERROR_PLZ_LOGIN),
						"data": "账号已被删除",
					})
					c.Abort()
					return
				}

				//查看token是否在登出中
				value, _ = gredis.Get("logoutlist:" + token)
				fmt.Println("value=", string(value))
				if string(value) == "1" {
					//前端直接跳到登录页面
					c.JSON(http.StatusUnauthorized, gin.H{
						"code": e.ERROR_PLZ_LOGIN,
						"msg":  e.GetMsg(e.ERROR_PLZ_LOGIN),
						"data": data,
					})
					c.Abort()
					return
				}
				//一个账号同时只能一个人登录
				value, _ = gredis.Get("loginlist:" + uid)
				if strings.Trim(string(value), "\"") != token {
					//前端直接跳到登录页面
					c.JSON(http.StatusUnauthorized, gin.H{
						"code": e.ERROR_PLZ_LOGIN,
						"msg":  e.GetMsg(e.ERROR_PLZ_LOGIN),
						"data": data,
					})
					c.Abort()
					return
				}
				//验证token
				_, err = util.ParseToken(token)
				if err != nil {
					switch err.(*jwt.ValidationError).Errors {
					case jwt.ValidationErrorExpired:
						code = e.ERROR_AUTH_CHECK_TOKEN_TIMEOUT
					default:
						code = e.ERROR_AUTH_CHECK_TOKEN_FAIL
					}
				}
			}

			if code != e.SUCCESS {
				c.JSON(http.StatusUnauthorized, gin.H{
					"code": code,
					"msg":  e.GetMsg(code),
					"data": data,
				})

				c.Abort()
				return
			}

			//权限验证
			url := c.Request.URL.Path
			accessedUrlMap := user.GetPrivileges(uid)
			for k, v := range accessedUrlMap {
				b, ok := v.(int)
				if url == k && (!ok || b == 0) {
					bAccess = false
				}
			}
		}
		if setting.ServerSetting.RunMode != "debug" {
			if bAccess {
				c.Next()
			} else {
				c.JSON(http.StatusUnauthorized, gin.H{
					"code": 403,
					"msg":  "",
					"data": "您无权访问此页面",
				})
				c.Abort()
				return
			}
		} else {
			c.Next()
		}
	}
}
