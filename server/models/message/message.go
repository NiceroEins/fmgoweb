package message

import (
	"datacenter/models"
	"datacenter/models/event"
	"datacenter/models/user"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"errors"
	"strings"
	"sync/atomic"
)

type MessageType string

const (
	Message_Null      MessageType = "null"      //不正确消息类型
	Message_Broadcast MessageType = "broadcast" //系统广播
	Message_Notify    MessageType = "notify"    //通知消息
	Message_Chat      MessageType = "chat"      //聊天消息
	//Message_Concern            MessageType = "concern"            //关注
	//Message_Undo_Concern       MessageType = "unconcern"          //取消关注
	//Message_Like               MessageType = "like"               //点赞
	//Message_Undo_Like          MessageType = "unlike"             //取消点赞
	//Message_Bookmark           MessageType = "bookmark"           //收藏
	//Message_Undo_Bookmark      MessageType = "unbooked"           //取消收藏
	//Message_Comment            MessageType = "comment"            //评论
	//Message_Reply              MessageType = "reply"              //回复
	//Message_Block              MessageType = "block"              //拉黑
	//Message_Undo_Block         MessageType = "unblock"            //取消拉黑
	//Message_Pay                MessageType = "pay"                //支付
	Message_Confirm    MessageType = "confirm"    //客户端确认
	Message_System     MessageType = "system"     //系统提示
	Message_User_Event MessageType = "user_event" //用户事件

	Message_Recommend             MessageType = "recommend"               //推荐
	Message_Performance           MessageType = "performance"             //业绩推荐
	Message_Strongly_Recommend    MessageType = "highly_recommend"        //强烈推荐
	Message_Message               MessageType = "message"                 //顶部推送
	Message_Report                MessageType = "report"                  //研报推送
	Message_Roll                  MessageType = "roll"                    //滚动新闻
	Message_Tick                  MessageType = "tick"                    //实时行情（收）
	Message_Tick_Copy             MessageType = "tick_copy"               //实时行情（转发）
	Message_Stock                 MessageType = "stock"                   //行情推送
	Message_System_Report         MessageType = "system_report"           //研报系统推荐
	Message_System_Performance    MessageType = "system_performance"      //业绩系统推荐
	Message_System_IPO            MessageType = "system_ipo"              //ipo系统推荐
	Message_Performance_Stock     MessageType = "performance_stock"       //业绩行情推送
	Message_User_Center           MessageType = "home"                    // 用户中心推送
	Message_Home_Report           MessageType = "home_report"             // 用户中心研报推送
	Message_Home_Announcement     MessageType = "home_announcement"       // 用户中心公告推送
	Message_Home_Investigation    MessageType = "home_investigation"      //用户中心调研纪要推送
	Message_Personal_Stock        MessageType = "personal_stock"          //个人行情
	Message_Home_Stock_Pool       MessageType = "home_stock_pool"         // 用户中心股池推送
	Message_Auction               MessageType = "auction"                 // 拍卖信息推送
	Message_Stock_Pool            MessageType = "stock_pool"              //用户中心 股池推荐
	Message_Futures_Info          MessageType = "futures_info"            //用户中心 股池推荐
	Message_Home_Performance      MessageType = "home_performance"        //个人中心业绩
	Message_Home_Industry_Monitor MessageType = "home_industry_monitor"   //用户中心 行情监控
	Message_Industry_Monitor      MessageType = "system_industry_monitor" //用户中心 行情监控
	Message_Public_Fund           MessageType = "system_public_fund"      //用户中心 公募基金
	Message_Event_Recommend       MessageType = "event_recommend"         //事件推荐
)

var RuleTable = map[MessageType]string{
	Message_Recommend:             "/ws/news",
	Message_Strongly_Recommend:    "/ws/news",
	Message_System_Report:         "/ws/sys_report",
	Message_Performance:           "/ws/performance",
	Message_Message:               "/ws/message",
	Message_Roll:                  "/ws/roll",
	Message_Stock:                 "/ws/stock",
	Message_Report:                "/ws/report",
	Message_System_IPO:            "/ws/sys_ipo",
	Message_System_Performance:    "/ws/sys_performance",
	Message_Performance_Stock:     "/ws/sys_performance",
	Message_Home_Announcement:     "/ws/home_announcement",
	Message_Home_Investigation:    "/ws/home_investigation",
	Message_Home_Report:           "/ws/report",
	Message_Home_Stock_Pool:       "/ws/home_stock_pool",
	Message_Auction:               "/ws/auction",
	Message_Futures_Info:          "/ws/futures_info",
	Message_Home_Industry_Monitor: "/ws/home_industry_monitor",
	Message_Public_Fund:           "/ws/system_public_fund",
	Message_Event_Recommend:       "/ws/event_recommend",
}

func (m MessageType) String() string {
	return string(m)
}

type Message struct {
	id          int64
	msg         string
	from        string
	to          string
	bStore      bool
	bFilter     bool
	delay       int
	messageType MessageType
	channel     string
}

type Counter struct {
	n int64
}
type User struct {
	UserID string `json:"user_id" gorm:"column:user_id"`
}
type SystemMessage string

const (
	System_Concern_Non      SystemMessage = "-1" //未关注对方，对方未关注你
	System_Concern_DestOnly SystemMessage = "0"  //未关注对方，对方已关注你
	System_Concern_SrcOnly  SystemMessage = "1"  //已关注对方，对方未关注你
	System_Concern_Each     SystemMessage = "2"  //互相关注
	System_Ban_Src          SystemMessage = "3"  //我方已屏蔽对方信息
	System_Ban_Dest         SystemMessage = "4"  //对方已屏蔽我方信息
)

func (s SystemMessage) String() string {
	return string(s)
}

type Rule struct {
	Operation   string //操作符
	Key         Param  //字段名
	Value       int
	Intercepted bool            //对满足条件的消息是否拦截
	RetCode     []SystemMessage //返回值
}

type Param string

const (
	WsSendBeforeReply Param = "send_before_reply" // 有回复前发送条数
	WsSendCount       Param = "send_count"        // 发送条数
)

func (p Param) String() string {
	return string(p)
}

func (msg *Message) Id() int64 {
	return msg.id
}

func (msg *Message) From() string {
	return msg.from
}

func (msg *Message) Msg() string {
	return msg.msg
}

func (msg *Message) To() string {
	return msg.to
}

func (msg *Message) Type() MessageType {
	return msg.messageType
}

func (msg *Message) Delay() int {
	return msg.delay
}

// 系统消息
func (msg *Message) IsSystemMsg() bool {
	return msg.Type() != Message_Chat
}

// 消息是否有效
func (msg *Message) Available() bool {
	if msg.Type() == Message_Null || msg.Type() == "" {
		return false
	}
	if !msg.IsSystemMsg() && len(msg.Msg()) == 0 {
		return false
	}
	return true
}

func (msg *Message) SetId(id int64) {
	atomic.StoreInt64(&msg.id, id)
}

func (msg *Message) SetMsg(content string) {
	msg.msg = content
}

func (msg *Message) SetDelay(delay int) {
	msg.delay = delay
}

func (msg *Message) SetStore(bStore bool) {
	msg.bStore = bStore
}

func (msg *Message) GenerateKey(key Param) string {
	return key.String() + "_" + msg.From() + "_" + msg.To()
}

func (msg *Message) GenerateKeyEx(src, dest string, key Param) string {
	return key.String() + "_" + src + "_" + dest
}

func (msg *Message) System() string {
	return "system"
}

// 是否需要保存
func (msg *Message) NeedStore() bool {
	return msg.bStore
}

// 是否需要使用筛选器
func (msg *Message) NeedFilter() bool {
	return msg.bFilter
}

// 消息是否指定管道
func (msg *Message) Channel() string {
	return msg.channel
}

func (msg *Message) SetChannel(channel string) {
	msg.channel = channel
}

func UnmarshalMsg(msg []byte) *Message {
	ret := &Message{}
	m := unmarshal(msg)

	bTick := false
	if m == nil {
		bTick = true
	} else {
		_, bTick = util.GetString(m, "event")
	}

	if bTick {
		// 行情
		return &Message{
			id:          0,
			msg:         string(msg),
			from:        "tick",
			to:          "stock",
			bStore:      false,
			bFilter:     false,
			messageType: Message_Tick,
		}
	}

	id, _ := util.GetInt(m, "id")
	if id != 0 {
		ret.id = int64(id)
	}

	ret.from, _ = util.GetString(m, "from")
	ret.to, _ = util.GetString(m, "to")
	t, _ := util.GetString(m, "type")
	ret.messageType = MessageType(t)
	ret.msg, _ = util.GetString(m, "msg")

	var ok bool
	ret.bFilter, ok = util.GetBool(m, "filter")
	if !ok {
		// 默认使用筛选器
		ret.bFilter = true
	}

	ret.bStore, ok = util.GetBool(m, "store")
	if !ok {
		// 默认保存
		ret.bStore = true
	}

	ret.channel, ok = util.GetString(m, "channel")
	if !ok {
		ret.channel = ""
	}

	return ret
}

func MarshalMsg(msg *Message) []byte {
	ret, _ := json.Marshal(map[string]interface{}{
		"id":      msg.id,
		"from":    msg.from,
		"to":      msg.to,
		"type":    msg.messageType,
		"msg":     msg.msg,
		"channel": msg.channel,
	})
	return ret
}

func ParseMsg(messageType MessageType, from, to, content string, bStore bool) *Message {
	return &Message{
		msg:         content,
		from:        from,
		to:          to,
		messageType: messageType,
		bStore:      bStore,
		bFilter:     true,
	}
}

func unmarshal(msg []byte) map[string]interface{} {
	m := map[string]interface{}{}
	err := json.Unmarshal(msg, &m)
	if err != nil {
		return nil
	}
	return m
}

func NewCounter() *Counter {
	return &Counter{
		n: 1,
	}
}

func (c *Counter) Add(n int) int64 {
	ret := atomic.AddInt64(&c.n, int64(n))
	return ret
}

func (c *Counter) Step() int64 {
	return c.Add(1)
}

func (c *Counter) Get() int64 {
	return atomic.LoadInt64(&c.n)
}

func (c *Counter) Reset() {
	atomic.StoreInt64(&c.n, 0)
}

/*
func (msg *Message) FilterOld(condition map[string]interface{}) []*Message {
	ret := make([]*Message, 0)
	intercepted := false

	logging.Debug("condition: " + util.Map2str(condition))
	for _, v := range condition {
		rule, ok := v.(Rule)
		if !ok {
			logging.Error("websocket.service.Filter() invalid condition")
			continue
		}
		//cmpValue, _ := xx.GetInt(record, rule.Key)
		ri := gredis.Clone(3)
		var cv int64
		var err error
		if cv, err = ri.GetInt(msg.GenerateKey(rule.Key)); err != nil {
			logging.Debug("message.Filter() Errors: " + err.Error())
		}
		cmpValue := int(cv)
		var cr bool
		switch rule.Operation {
		case "gt":
			cr = cmpValue > rule.Value
		case "gte":
			cr = cmpValue >= rule.Value
		case "lt":
			cr = cmpValue < rule.Value
		case "lte":
			cr = cmpValue <= rule.Value
		case "e":
			cr = cmpValue == rule.Value
		case "ne":
			cr = cmpValue != rule.Value
		default:
			logging.Error("websocket.service.Filter() unknown operation: " + rule.Operation)
			continue
		}
		if cr {
			if rule.Intercepted {
				intercepted = true
			}
			for _, v := range rule.RetCode {
				ret = append(ret, &Message{
					from:        msg.To(),
					to:          msg.From(),
					messageType: Message_System,
					msg:         string(v),
				})
			}
		}
	}
	if !intercepted {
		ret = append(ret, msg)
	}
	return ret
}*/

func (msg *Message) Filter(condition map[string]interface{}) []*Message {
	ret := make([]*Message, 0)
	if msg.To() != "" {
		ret = append(ret, msg)
		return ret
	}
	if !msg.isPushAvailable() {
		return ret
	}
	logging.Info("msg: ", string(MarshalMsg(msg)))
	// insert push data
	if msg.Channel() == "" {
		if msg.Type().String() != "auction" {
			_ = event.InsertPushData(message2Push(msg))
		}
		if strings.HasPrefix(msg.Type().String(), "home_") {
			// 推送逻辑 用户中心的只推送给自己 11-22
			ret = append(ret, ParseMsg(msg.Type(), msg.From(), msg.From(), msg.Msg(), false))
			// var (
			// 	users []User
			// )
			// // 解析消息内容 获取用户id
			// code, err := getCodeByMessage(msg)
			// if err != nil {
			// 	return ret
			// }
			// maps := make(map[string]interface{})
			// maps["code"] = code
			//
			// users, err = GetUserIdByCode(maps)
			// logging.Info("message users : ", users)
			// if err != nil {
			// 	logging.Info("Filter err:", err)
			// 	return ret
			// }
			// if len(users) < 1 {
			// 	ret = append(ret, msg)
			// 	return ret
			// }
			// for _, v := range users {
			// 	if v.UserID == msg.From() {
			// 		continue
			// 	}
			// 	ret = append(ret, &Message{
			// 		msg:         msg.Msg(),
			// 		from:        msg.From(),
			// 		to:          v.UserID,
			// 		messageType: msg.Type(),
			// 		bStore:      msg.bStore,
			// 	})
			// }
			// ret = append(ret, ParseMsg(msg.Type(), msg.From(), msg.From(), msg.Msg(), false))
		}
		userList := user.GetUserHavePrivilege(RuleTable[msg.Type()] + "/pull")
		for _, v := range userList {
			ret = append(ret, &Message{
				msg:         msg.Msg(),
				from:        msg.From(),
				to:          v.UserID,
				messageType: msg.Type(),
				bStore:      msg.bStore,
				delay:       v.Delay,
			})
		}
	} else {
		ret = append(ret, msg)
	}
	return ret
}

func RemoveDup(msg []*Message) []*Message {
	ret := make([]*Message, 0)
	hashList := make([]string, 0)
	for i := 0; i < len(msg); i++ {
		hash := util.EncodeMD5(string(MarshalMsg(msg[i])))
		if !util.ContainsAny(hashList, hash) {
			ret = append(ret, msg[i])
			hashList = append(hashList, hash)
		}
	}
	return ret
}

func (msg *Message) isPushAvailable() bool {
	// check send
	if msg.From() == "" {
		return false
	}

	// 新闻类及系统推送，不检查推送权限
	if msg.From() == "system" || msg.Type() == Message_System || msg.Type() == Message_System_Report || msg.Type() == Message_Roll || msg.Type() == Message_Stock || msg.Type() == Message_System_IPO || msg.Type() == Message_System_Performance || msg.Type() == Message_Home_Announcement || msg.Type() == Message_Home_Report || msg.Type() == Message_Home_Stock_Pool || msg.Type() == Message_Performance || msg.Type() == Message_Home_Industry_Monitor || msg.Type() == Message_Home_Performance || msg.Type() == Message_Event_Recommend {
		return true
	}

	pf := user.GetPrivileges(msg.From())
	if v, ok := pf[RuleTable[msg.Type()]+"/push"]; !ok || v == nil || v.(int) == 0 {
		return false
	}

	//// check receive
	//pt := models.GetPrivileges(msg.To())
	//if v, ok := pt[RuleTable[msg.Type()] + "/pull"]; !ok || v == nil || v.(int) == 0 {
	//	return false
	//}

	return true
}

func message2Push(msg *Message) *event.Push {
	return &event.Push{
		FromID:  msg.From(),
		ToID:    &msg.to,
		Content: &msg.msg,
		Type:    msg.Type().String(),
	}
}

func ParseChanelMsg(messageType MessageType, from, to, content, channel string, bStore bool) *Message {
	return &Message{
		msg:         content,
		from:        from,
		to:          to,
		messageType: messageType,
		bStore:      bStore,
		bFilter:     true,
		channel:     channel,
	}
}

// 期货进推送库
func (msg *Message) FilterFutures() []*Message {
	ret := make([]*Message, 0)
	if msg.To() != "" {
		ret = append(ret, msg)
		return ret
	}
	if !msg.isPushAvailable() {
		return ret
	}
	logging.Info("msg: ", string(MarshalMsg(msg)))
	_ = event.InsertPushData(message2Push(msg))
	userList := user.GetUserHavePrivilege(RuleTable[msg.Type()] + "/pull")
	for _, v := range userList {
		ret = append(ret, &Message{
			msg:         msg.Msg(),
			from:        msg.From(),
			to:          v.UserID,
			messageType: msg.Type(),
			bStore:      msg.bStore,
			delay:       v.Delay,
		})
	}
	return ret
}

func (msg *Message) FilterHomeReport() []*Message {
	ret := make([]*Message, 0)
	if msg.To() != "" {
		return []*Message{msg}
	}
	var (
		users []User
		err   error
	)
	// insert push data
	// _ = event.InsertPushData(message2Push(msg))
	// 解析消息内容 获取用户id
	var MessageStr struct {
		Code     string `json:"code"`
		Industry string `json:"industry"`
	}
	message := MessageStr
	err = json.Unmarshal([]byte(msg.msg), &message)
	if err != nil {
		logging.Info("FilterHomeReport err:", err)
		return ret
	}
	if message.Code == "" && message.Industry == "" {
		logging.Info("FilterHomeReport err:", err)
		return ret
	}
	maps := make(map[string]interface{})
	if message.Code != "" {
		maps["code"] = message.Code
		users, err = GetUserIdByCode(maps)
	} else if message.Industry != "" {
		maps["industry"] = message.Industry
		users, err = GetUserIdByCode(maps)
	}
	if err != nil {
		logging.Info("FilterHomeReport err:", err)
		return ret
	}
	for _, v := range users {
		if v.UserID == msg.From() {
			continue
		}
		ret = append(ret, &Message{
			msg:         msg.Msg(),
			from:        msg.From(),
			to:          v.UserID,
			messageType: msg.Type(),
			bStore:      msg.bStore,
		})
	}
	ret = append(ret, ParseMsg(msg.Type(), msg.From(), msg.From(), msg.Msg(), false))
	return ret
}

func (msg *Message) FilterHomeStockPool() []*Message {
	ret := make([]*Message, 0)
	if msg.To() != "" {
		return []*Message{msg}
	}
	var (
		users []User
		err   error
	)
	if msg.From() == "system" {
		return []*Message{msg}
	}
	// insert push data
	// _ = event.InsertPushData(message2Push(msg))
	// 解析消息内容 获取用户id
	var MessageStr struct {
		Code string `json:"code"`
	}
	message := MessageStr
	err = json.Unmarshal([]byte(msg.msg), &message)
	if err != nil {
		logging.Info("FilterHomeStockPool err:", err)
		return ret
	}
	if message.Code == "" {
		logging.Info("FilterHomeStockPool err:", err)
		return ret
	}
	maps := make(map[string]interface{})

	if message.Code != "" {
		maps["code"] = message.Code
		users, err = GetUserIdByCode(maps)
	}
	if err != nil {
		logging.Info("FilterHomeStockPool err:", err)
		return ret
	}

	// tmp := &Message{
	// 	msg:         msg.Msg(),
	// 	from:        msg.From(),
	// 	to:          msg.From(),
	// 	messageType: msg.Type(),
	// 	bStore:      msg.bStore,
	// }
	// msg.to = strconv.Itoa(userID)
	// if msg.To() != msg.From() {
	// 	ret = append(ret, tmp)
	// }

	for _, v := range users {
		if v.UserID == msg.From() {
			continue
		}
		ret = append(ret, &Message{
			msg:         msg.Msg(),
			from:        msg.From(),
			to:          v.UserID,
			messageType: msg.Type(),
			bStore:      msg.bStore,
		})
	}
	ret = append(ret, ParseMsg(msg.Type(), msg.From(), msg.From(), msg.Msg(), false))
	return ret
}

func (msg *Message) FilterHomeAnnouncement() []*Message {
	ret := make([]*Message, 0)
	if msg.To() != "" {
		return ret
	}
	var (
		users []User
		err   error
	)
	if msg.From() == "system" {
		return []*Message{msg}
	}
	// 解析消息内容 获取用户id
	var MessageStr struct {
		Source string `json:"source"`
		Code   string `json:"code" gorm:"column:id"`
	}
	message := MessageStr
	err = json.Unmarshal([]byte(msg.msg), &message)
	if err != nil {
		logging.Info("FilterHomeAnnouncement err:", err)
		return ret
	}
	if message.Source == "" {
		logging.Info("FilterHomeAnnouncement err:", err)
		return ret
	}
	err = models.DB().Table("b_stock").Select("id").Where("name = ? ", message.Source).First(&message).Error
	logging.Info("code name is ", message.Code)
	if err != nil {
		logging.Info("FilterHomeAnnouncement err:", err)
		return ret
	}
	maps := make(map[string]interface{})
	maps["code"] = message.Code
	users, err = GetUserIdByCode(maps)
	logging.Info("users name is ", users)
	if err != nil {
		logging.Info("FilterHomeAnnouncement err:", err)
		return ret
	}
	for _, v := range users {
		if v.UserID == msg.From() {
			continue
		}
		ret = append(ret, &Message{
			msg:         msg.Msg(),
			from:        msg.From(),
			to:          v.UserID,
			messageType: msg.Type(),
			bStore:      msg.bStore,
		})
	}
	ret = append(ret, ParseMsg(msg.Type(), msg.From(), msg.From(), msg.Msg(), false))
	return ret
}

func (msg *Message) HomeFilter() []*Message {
	ret := make([]*Message, 0)
	if msg.To() != "" {
		ret = append(ret, msg)
		return ret
	}
	ret = append(ret, msg)
	return ret
}

func (msg *Message) HomePersonStockFilter() []*Message {
	ret := make([]*Message, 0)
	if msg.To() != "" {
		ret = append(ret, msg)
		return ret
	}
	if !msg.isPushAvailable() {
		return ret
	}
	logging.Info("msg: ", string(MarshalMsg(msg)))
	if msg.Channel() == "" {
		_ = event.InsertPushData(message2Push(msg))
		userList := user.GetUserHavePrivilege(RuleTable[msg.Type()] + "/pull")
		for _, v := range userList {
			if v.UserID == msg.From() {
				continue
			}
			ret = append(ret, &Message{
				msg:         msg.Msg(),
				from:        msg.From(),
				to:          v.UserID,
				messageType: msg.Type(),
				bStore:      msg.bStore,
				delay:       v.Delay,
			})
		}
	} else {
		ret = append(ret, msg)
	}
	return ret
}

// 根据code 获取 user_id
func GetUserIdByCode(data map[string]interface{}) ([]User, error) {
	userModel := []User{}
	err := models.DB().Table("u_stock_industry").
		Select("u_second_industry_user.user_id").
		Joins("left join u_second_industry_user on u_stock_industry.second_industry = u_second_industry_user.second_industry").
		Where("u_second_industry_user.deleted_at is null").
		Where(data).Find(&userModel).Error
	if err != nil {
		return []User{}, err
	}
	return userModel, nil
}

func getCodeByMessage(msg *Message) (string, error) {
	// 解析消息内容 获取用户id
	var MessageStr struct {
		Source string `json:"source"`
		Code   string `json:"code" gorm:"column:id"`
	}
	message := MessageStr
	err := json.Unmarshal([]byte(msg.msg), &message)
	if err != nil {
		logging.Info("FilterHomeAnnouncement err:", err)
		return "", err
	}
	if message.Code != "" {
		return message.Code, nil
	}
	if message.Source != "" {
		err = models.DB().Table("b_stock").Select("id").Where("name = ? ", message.Source).First(&message).Error
		if err != nil {
			logging.Info("FilterHomeAnnouncement err:", err)
			return "", err
		}
		return message.Code, nil
	}
	return "", errors.New("get code fail")
}
