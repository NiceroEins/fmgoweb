package changelog
type ChangeLogListRequest struct {
	PageSize  int    `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum   int    `json:"page_num" form:"page_num" binding:"required,gte=1"`
}
type ChangeLogAddRequest struct {
	Content        string     `json:"content" form:"content" binding:"required,max=500"`
	UserID         int        `json:"user_id" form:"user_id" binding:"required"`
}
type CalendarDeleteRequest struct {
	ID    int     `json:"id" form:"id"  binding:"required"`
}
type CalendarEditRequest struct {
	CalendarDeleteRequest
	ChangeLogAddRequest
}
