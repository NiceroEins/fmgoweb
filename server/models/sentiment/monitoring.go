package sentiment

import (
	bytes2 "bytes"
	"datacenter/controller/news"
	"datacenter/models"
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"sort"
	"strings"
	"time"
)

type MonitoringReq struct {
	ID int `json:"id" form:"id"`
	//PageNum  int    `json:"p" form:"p"`
	//PageSize int    `json:"n" form:"n"`
	Period   string `json:"period" form:"period"`
	Property string `json:"property,omitempty" form:"property,omitempty"`
	Sort     string `json:"sort,omitempty" form:"sort,omitempty"`
	SeedName string `json:"seed_name,omitempty" form:"seed_name,omitempty"`
}

type MonitoringCaseReq struct {
	ID      int    `json:"id,omitempty" form:"id,omitempty"`
	Name    string `json:"name,omitempty" form:"name,omitempty"`
	Profile string `json:"profile,omitempty" form:"profile,omitempty"`
	Exclude string `json:"exclude,omitempty" form:"exclude,omitempty"`
	Type    string `json:"type" form:"type"`
}

type MonitoringNews struct {
	spider.NewsEx
	Similar int `json:"similar"`
	//LastID  int `json:"-"`
}

type MonitoringCaseResp struct {
	Data []MonitoringNews `json:"data"`
}

type MonitoringCorrectionReq struct {
	FeedID       int `json:"feed_id" form:"feed_id"`
	SentimentOld int `json:"sentiment" form:"sentiment"`
}

type MonitoringCorrection struct {
	FeedID       int
	SentimentOld int
	SentimentNew int
	UserID       string
}

type Profile struct {
	models.Simple
	UserID  string
	Name    string
	Profile string
	Exclude string
	LastID  int
}

func (Profile) TableName() string {
	return "b_sentiment_profile"
}

func (p *Profile) Add() error {
	return models.DB().Table("b_sentiment_profile").Create(p).Error
}

func (p *Profile) Edit(q *Profile) error {
	err := models.DB().Table("b_sentiment_profile").
		Where("id = ?", p.ID).
		Assign(q).FirstOrCreate(&Profile{}).Error
	return err
}

func QueryByID(id int) *Profile {
	var p Profile
	if err := models.DB().Table("b_sentiment_profile").
		Where("id = ?", id).
		First(&p).Error; err != nil {
		return nil
	}
	return &p
}

func Query(userID string) []Profile {
	var ret []Profile
	models.DB().Table("b_sentiment_profile").
		Where("user_id = ?", userID).
		Find(&ret)
	return ret
}

func (p *Profile) Delete() {
	models.DB().Table("b_sentiment_profile").Delete(p)
}

func ProcProfile(mcr *MonitoringCaseReq, userID string) ([]Profile, error) {
	switch mcr.Type {
	case "add":
		return nil, NewProfile(mcr, userID)
	case "edit":
		return nil, EditProfile(mcr)
	case "query":
		return Query(userID), nil
	case "delete":
		DeleteProfile(mcr, userID)
		return nil, nil
	}
	return nil, nil
}

func NewProfile(mcr *MonitoringCaseReq, userID string) error {
	p := Profile{
		UserID:  userID,
		Name:    mcr.Name,
		Profile: mcr.Profile,
		Exclude: mcr.Exclude,
	}
	return p.Add()
}

func EditProfile(mcr *MonitoringCaseReq) error {
	var p Profile
	p.ID = mcr.ID
	deleteCache(mcr.ID)
	return p.Edit(&Profile{
		Name:    mcr.Name,
		Profile: mcr.Profile,
		Exclude: mcr.Exclude,
	})
}

func DeleteProfile(mcr *MonitoringCaseReq, userID string) {
	var p Profile
	p.ID = mcr.ID
	p.UserID = userID
	p.Delete()
	deleteCache(mcr.ID)
}

func do(mr *MonitoringReq, pf *Profile) ([]MonitoringNews, error) {
	var tsBegin, direction string
	var t time.Time
	var idx int
	var bCache, ok bool = false, false
	var data []spider.NewsEx
	if data, ok = loadCache(mr.ID); ok && len(data) > 0 {
		for i := 0; i < len(data); i++ {
			if data[i].ID > idx {
				idx = data[i].ID
			}
		}
		direction = "up"
		bCache = true
	}
	//offset := mr.PageSize * (mr.PageNum - 1)
	policy := &spider.NewsPolicy{}
	profile := strings.Replace(pf.Profile, "|", " or ", -1)
	profile = strings.Replace(profile, "+", " and ", -1)
	policy = policy.Default(pf.UserID)
	if mr.SeedName != "" {
		policy = policy.SetKeyword(mr.SeedName).SetType("source")
	} else {
		policy = policy.SetKeyword(profile).SetType("news")
	}
	policy = policy.SetDup(1).SetNLPDup(1)
	policy.SetEntry("sentiment").Do(pf.UserID)

	switch mr.Period {
	case "today":
		t = time.Now().Truncate(24 * time.Hour).Add(-8 * time.Hour)
		tsBegin = t.Format(util.YMDHMS)
	case "24hour":
		t = time.Now().Add(-24 * time.Hour)
		tsBegin = t.Format(util.YMDHMS)
	case "2day":
		t = time.Now().Add(-24 * time.Hour).Truncate(24 * time.Hour).Add(-8 * time.Hour)
		tsBegin = t.Format(util.YMDHMS)
	default:
		return []MonitoringNews{}, errors.New("unknown type of period")
	}
	raw, _, err := spider.FetchNews(policy, false, idx, 0, 2500, direction, tsBegin, time.Now().Format(util.YMDHMS))
	if bCache {
		data = append(data, raw...)
	} else {
		data = raw
	}
	key := generateKey(mr)
	logging.Info("sentiment.do() set cache: ", key, mr.ID, idx)
	insertCache(mr.ID, key)
	cache.Store(key, data)
	data = news.Assemble(data, policy, pf.UserID, "no-event")
	return assemble(data, t, mr), err
}

func assemble(data []spider.NewsEx, t time.Time, mr *MonitoringReq) []MonitoringNews {
	var ret []spider.NewsEx
	for _, v := range data {
		b := false
		//var cnt int64
		//models.DB().Table("b_sentiment_correction").Where("feed_id = ?", v.ID).Count(&cnt)
		if v.SentimentNew > 0 {
			v.SentimentAnalysis = 0
		}
		switch mr.Property {
		case "all":
			b = true
		case "good":
			b = v.SentimentAnalysis == 1
		case "bad":
			b = v.SentimentAnalysis == 2
		case "neutral":
			b = v.SentimentAnalysis == 3
		}
		if b {
			ret = append(ret, v)
		}
	}

	return similar(ret, t, time.Now())
}

func News(mr MonitoringReq) (MonitoringCaseResp, error) {
	var ret MonitoringCaseResp
	if pf := QueryByID(mr.ID); pf == nil {
		return ret, errors.New("empty record")
	} else {
		data, _ := do(&mr, pf)
		switch mr.Sort {
		case "time_desc":
			sort.Slice(data, func(i, j int) bool {
				return util.Compare(reflect.ValueOf(data[j]).FieldByName("CreatedAt"), reflect.ValueOf(data[i]).FieldByName("CreatedAt"))
			})
		case "time_asc":
			sort.Slice(data, func(i, j int) bool {
				return util.Compare(reflect.ValueOf(data[i]).FieldByName("CreatedAt"), reflect.ValueOf(data[j]).FieldByName("CreatedAt"))
			})
		case "similar":
			sort.Slice(data, func(i, j int) bool {
				return util.Compare(reflect.ValueOf(data[j]).FieldByName("Similar"), reflect.ValueOf(data[i]).FieldByName("Similar"))
			})
		}

		ret.Data = data

		return ret, nil
	}
}

func Correct(req *MonitoringCorrectionReq, userID string) error {
	mc := MonitoringCorrection{
		FeedID:       req.FeedID,
		SentimentOld: req.SentimentOld,
		UserID:       userID,
	}
	switch mc.SentimentOld {
	case 1:
		mc.SentimentNew = 2
	case 2:
		mc.SentimentNew = 1
	}
	if err := models.DB().Table("b_sentiment_correction").
		Where("feed_id = ?", req.FeedID).
		Assign(mc).FirstOrCreate(&MonitoringCorrection{}).Error; err != nil {
		return err
	}
	return nil
}

func similar(news []spider.NewsEx, tsBegin time.Time, tsEnd time.Time) []MonitoringNews {
	data := make([]MonitoringNews, 0)
	ret := make([]MonitoringNews, 0)
	var inParams struct {
		NewsIDs   []int `json:"news_ids"`
		StartTime int64 `json:"start_time"`
		EndTime   int64 `json:"end_time"`
	}
	for _, v := range news {
		inParams.NewsIDs = append(inParams.NewsIDs, v.ID)
		data = append(data, MonitoringNews{
			NewsEx: v,
		})
	}
	inParams.StartTime = tsBegin.Unix()
	inParams.EndTime = time.Now().Unix()
	in, _ := json.Marshal(inParams)
	client := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	urlString := fmt.Sprintf("%s/get_same_news_nums", setting.NLPSetting.SimilarHost)
	req, err := http.NewRequest("POST", urlString, bytes2.NewReader(in))
	if err != nil {
		return ret
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json;charset=UTF-8")
	resp, err := client.Do(req)
	if err != nil {
		return data
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ret
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	var cst struct {
		Code int    `json:"code"`
		Msg  string `json:"msg"`
		Data []struct {
			NewsID int   `json:"news_id"`
			Nums   int   `json:"nums"`
			IDS    []int `json:"ids"`
		} `json:"data"`
	}
	if err = json.Unmarshal(bytes, &cst); err != nil {
		return ret
	}

	for i := 0; i < len(data); i++ {
		for j := 0; j < len(cst.Data); j++ {
			if data[i].ID == cst.Data[j].NewsID {
				for k := 0; k < len(cst.Data[j].IDS); k++ {
					for l := 0; l < len(ret); l++ {
						if cst.Data[j].IDS[k] != cst.Data[j].NewsID && cst.Data[j].IDS[k] == ret[l].ID {
							ret = append(ret[:l], ret[l+1:]...)
						}
					}
				}
				data[i].Similar = cst.Data[j].Nums
				// fix
				if data[i].Similar == 0 {
					data[i].Similar = 1
				}
				ret = append(ret, data[i])
				break
			}
		}
	}

	return ret
}
