package sentiment

import (
	"datacenter/models/spider"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"sync"
)

var cache sync.Map

func generateKey(hr interface{}) string {
	b, _ := json.Marshal(hr)
	hash := fmt.Sprintf("%d", util.BKDRHash(string(b)))
	return hash
}

func load(hash string) ([]spider.NewsEx, bool) {
	var ret []spider.NewsEx
	ih, ok := cache.Load(hash)
	if !ok {
		return ret, false
	}

	ret, ok = ih.([]spider.NewsEx)
	return ret, ok
}

func loadCache(id int) ([]spider.NewsEx, bool) {
	hs, ok := cache.Load(id)
	if !ok {
		return []spider.NewsEx{}, false
	}
	hash := hs.(string)
	return load(hash)
}

func insertCache(id int, hash string) {
	ih, ok := cache.Load(id)
	if !ok {
		return
	}
	list, _ := ih.([]string)
	list = append(list, hash)
	cache.Store(id, list)
}

func deleteCache(id int) {
	cache.Delete(id)
}
