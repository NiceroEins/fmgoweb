package public_fund

import (
    "datacenter/models"
    "datacenter/pkg/logging"
    "datacenter/pkg/util"
    "github.com/jinzhu/gorm"
    "github.com/shopspring/decimal"
    "reflect"
    "sort"
    "strconv"
    "sync"
    "time"
)

// 基金经理姓名搜索
type FundNameSearchReq struct {
    FundName        string    `json:"fund_name" form:"name" binding:"required"`
}
type FundNameListResp struct {
    FundId          string       `json:"fund_id" gorm:"column:id"`
    FundName        string       `json:"fund_name" gorm:"column:fund_name"`
    FundCode        string       `json:"fund_code" gorm:"column:code"`
}
type FundScaleResp struct {
    FundName                  string                  `json:"fund_name" gorm:"column:name"`    // 基金名称
    NowFundScale              decimal.NullDecimal         `json:"now_fund_scale"`                  // 当前报告期基金规模
    IncreaseFundScale         decimal.NullDecimal         `json:"increase_fund_scale"`             // 当前报告期新增规模
    LastFundScale             decimal.NullDecimal         `json:"last_fund_scale"`                 // 上一个报告期新增规模
    YearFundScale             decimal.NullDecimal         `json:"year_fund_scale"`                 // 去年最后一个报告期规模
    YearIncreaseFundScale     decimal.NullDecimal         `json:"year_increase_fund_scale"`        //今年累计新增报告期规模
    Rate                      decimal.NullDecimal         `json:"rate"`                            // 环比增长
    YearRate                  decimal.NullDecimal         `json:"year_rate"`                       // 累计增长率
    ReportDate                string                      `json:"report_date"`                     // 当前报告期
    // 当前报告期
    NetRetioShares            decimal.NullDecimal         `json:"net_retio_shares" gorm:"column:net_retio_shares"` // 股票占净比
    NetRetioBond              decimal.NullDecimal         `json:"net_retio_bond" gorm:"column:net_retio_bond"`     // 债券占净比
    NetRetioCash              decimal.NullDecimal         `json:"net_retio_cash" gorm:"column:net_retio_cash"`     // 现金占净比
    // 上一个报告期
    LastNetRetioShares        decimal.NullDecimal         `json:"last_net_retio_shares"`   // 股票占净比
    LastNetRetioBond          decimal.NullDecimal         `json:"last_net_retio_bond"`     // 债券占净比
    LastNetRetioCash          decimal.NullDecimal         `json:"last_net_retio_cash"`     // 现金占净比
    // 对应比例
    NetRetioSharesRate        decimal.NullDecimal     `json:"net_retio_shares_rate"`   // 股票占净比环比
    NetRetioBondRate          decimal.NullDecimal     `json:"net_retio_bond_rate"`     // 债券占净比环比
    NetRetioCashRate          decimal.NullDecimal     `json:"net_retio_cash_rate"`     // 现金占净比环比
}
type FundScaleList struct {
    FundCode                  string                  `json:"fund_code" gorm:"column:code"`             // 基金代码
    ReportDate                time.Time               `json:"report_date" gorm:"column:scale_date"`     // 报告期
    Scale                     decimal.Decimal         `json:"scale" gorm:"column:scale"`                // 基金规模
}
type FundAssets struct {
    FundCode                  string                  `json:"fund_code" gorm:"column:code"`             // 基金代码
    ReportDate                time.Time               `json:"report_date" gorm:"column:report_date"`     // 报告期
    NetAssets                 decimal.Decimal         `json:"net_assets" gorm:"column:net_assets"`             // 基金净资产
    NetRetioShares            decimal.Decimal         `json:"net_retio_shares" gorm:"column:net_retio_shares"` // 股票占净比
    NetRetioBond              decimal.Decimal         `json:"net_retio_bond" gorm:"column:net_retio_bond"`     // 债券占净比
    NetRetioCash              decimal.Decimal         `json:"net_retio_cash" gorm:"column:net_retio_cash"`     // 现金占净比
}
// 基金
type FundReq struct {
    SortKey     string    `json:"sort_key" form:"sort_key"`
    Direction   bool      `json:"direction" form:"direction"`
    DateTime    time.Time `json:"report_date" form:"report_date" time_format:"2006-01-02"`
    FundCode    string    `json:"fund_code" form:"fund_code" binding:"required,gte=1"`
    PageSize    int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
    PageNum     int       `json:"page_num" form:"page_num" binding:"required,gte=1"`
    IsNew       string    `json:"is_new" form:"is_new"`
}
// 获取一个基金的股票信息
type FundSigle struct {
   FundCode                string                `json:"fund_code" gorm:"column:fund_code"`
   StockCode               string                `json:"stock_code" gorm:"column:stock_code"`
   ReportDate              time.Time             `json:"report_date" gorm:"column:report_date"`
   StockName               string                `json:"stock_name" gorm:"column:stock_name"`
   NetWorthPropotion       decimal.Decimal       `json:"net_worth_propotion" gorm:"column:net_worth_propotion"`
   HeldNum                 decimal.Decimal       `json:"held_num" gorm:"column:held_num"`
}
// 基金页面
type FundList struct {
    ID                     int                   `json:"-" gorm:"column:id"`
    StockCode              string                `json:"stock_code" gorm:"column:stock_code"`
    FundCode               string                `json:"fund_code" gorm:"column:fund_code"`
    StockName              string                `json:"stock_name" gorm:"column:stock_name"`
    HeldNum                decimal.Decimal       `json:"held_num" gorm:"column:held_num"`
    NetWorthPropotion      decimal.Decimal       `json:"networth_propotion" gorm:"column:networth_propotion"`
    IsNew                  bool                  `json:"is_new"`                         // 是否是新股
    IsImportant            bool                  `json:"is_important" gorm:"column:is_important"`  // 是否是重点基金

    // 统计数据
    LastHeldNum                decimal.Decimal       `json:"last_held_num"`
    LastNetWorthPropotion      decimal.Decimal       `json:"last_networth_propotion"`
    IncreaseHeldNum            decimal.Decimal       `json:"increase_held_num"`
    YearIncreaseHeldNum        decimal.Decimal       `json:"year_increase_held_num"`
    YearHeldNum                decimal.Decimal       `json:"year_held_num"`
    Rate                       decimal.NullDecimal   `json:"rate"`       // 当前报告期环比
    YearRate                   decimal.NullDecimal   `json:"year_rate"`  //今年累计
    TotalMarketValue           decimal.NullDecimal   `json:"total_market_value"`  // 总市值
    TotalMarketValueIncrease   decimal.NullDecimal   `json:"total_market_value_increase"`         // 当前总市值
    ReportPeriod               time.Time             `json:"report_date" gorm:"column:report_date"`  // 报告期
    IncreaseNetWorthPropotion  decimal.Decimal       `json:"increase_networth_propotion"`    // 占净值比增加
}
type ReportPeriod struct {
    ReportPeriodList   []string    `json:"report_period_list"`
}
func GetFundNameList(req *FundNameSearchReq) ([]FundNameListResp,error)  {
    fund := []FundNameListResp{}
    err := models.DB().Table("p_public_fund_info").
        Select("code as id,name as fund_name").
        Where("name like ? or code like ?  ", "%"+req.FundName+"%","%"+req.FundName+"%").Find(&fund).Error
    if err != nil && err != gorm.ErrRecordNotFound{
        return nil,err
    }
    if err == gorm.ErrRecordNotFound {
        return []FundNameListResp{},nil
    }
    return fund,nil
}

func GetFundScaleList(req *FundReq) (FundScaleResp,error) {
    // 获取基金名称
    fundName := FundNameListResp{}
    ret := FundScaleResp{}
    err := models.DB().Table("p_public_fund_info").Select("id,name as fund_name,code").
        Where("code = ? or name like ?",req.FundCode,"%"+req.FundCode+"%").First(&fundName).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return FundScaleResp{},err
    }
    if err == gorm.ErrRecordNotFound {
        return FundScaleResp{},nil
    }
    req.FundCode = fundName.FundCode
    ret.FundName = fundName.FundName
    ret.ReportDate = req.DateTime.Format("2006年01月02日")
    // 获取当前报告期的规模
    nowReport,err := GetScaleByReportDate(req.DateTime.Format(util.YMD),req.FundCode)
    if err != nil {
        return FundScaleResp{},nil
    }
    ret.NowFundScale.Decimal = nowReport.NetAssets.Round(2)
    ret.NowFundScale.Valid   = true
    ret.NetRetioBond.Decimal = nowReport.NetRetioBond.Round(2)
    ret.NetRetioBond.Valid   = true
    ret.NetRetioCash.Decimal = nowReport.NetRetioCash.Round(2)
    ret.NetRetioCash.Valid   = true
    ret.NetRetioShares.Decimal = nowReport.NetRetioShares.Round(2)
    ret.NetRetioShares.Valid   = true
    // 获取上一个报告期数据
    lastReportPeriod,_ := GetReportPeriod(req.DateTime,-1)
    lastReport,err := GetScaleByReportDate(lastReportPeriod,req.FundCode)
    if err != nil {
        ret.LastFundScale.Decimal = nowReport.NetAssets.Round(2)
        ret.LastFundScale.Valid   = true
    }else {
        ret.IncreaseFundScale.Decimal = nowReport.NetAssets.Sub(lastReport.NetAssets).Round(2)
        ret.IncreaseFundScale.Valid   = true
        ret.LastFundScale.Decimal = lastReport.NetAssets.Round(2)
        ret.LastFundScale.Valid   = true
        if !lastReport.NetAssets.IsZero() {
            ret.Rate.Decimal = (nowReport.NetAssets.Sub(lastReport.NetAssets)).Div(lastReport.NetAssets).Mul(decimal.NewFromInt(100)).Round(2)
            ret.Rate.Valid = true
        }
        if !lastReport.NetRetioShares.IsZero() {
            // 债券占比
            ret.NetRetioSharesRate.Decimal = (ret.NetRetioShares.Decimal.Sub(lastReport.NetRetioShares)).Div(lastReport.NetRetioShares).Mul(decimal.NewFromInt(100)).Round(2)
            ret.NetRetioSharesRate.Valid = true
        }
        if !lastReport.NetRetioCash.IsZero() {
            // 现金占比
            ret.NetRetioCashRate.Decimal = ret.NetRetioCash.Decimal.Sub(lastReport.NetRetioCash).Div(lastReport.NetRetioCash).Mul(decimal.NewFromInt(100)).Round(2)
            ret.NetRetioCashRate.Valid = true
        }
        if !lastReport.NetRetioBond.IsZero() {
            // 现金占比
            ret.NetRetioBondRate.Decimal = ret.NetRetioBond.Decimal.Sub(lastReport.NetRetioBond).Div(lastReport.NetRetioBond).Mul(decimal.NewFromInt(100)).Round(2)
            ret.NetRetioBondRate.Valid = true
        }
        ret.LastNetRetioBond.Decimal = lastReport.NetRetioBond.Round(2)
        ret.LastNetRetioBond.Valid = true
        ret.LastNetRetioCash.Decimal = lastReport.NetRetioCash.Round(2)
        ret.LastNetRetioCash.Valid = true
        ret.LastNetRetioShares.Decimal = lastReport.NetRetioShares.Round(2)
        ret.LastNetRetioShares.Valid = true
    }
    // 获取上一年的最后一个报告期数据
    lastYearReportPeriod := strconv.Itoa(req.DateTime.AddDate(-1,0,0).Year()) + "-12-31"
    lastYearReport,err := GetScaleByReportDate(lastYearReportPeriod,req.FundCode)
    if err != nil {
        ret.YearIncreaseFundScale.Decimal = nowReport.NetAssets
        ret.YearIncreaseFundScale.Valid = true
    }else {
        ret.YearIncreaseFundScale.Decimal = nowReport.NetAssets.Sub(lastYearReport.NetAssets).Round(2)
        ret.YearIncreaseFundScale.Valid = true
        if !lastYearReport.NetAssets.IsZero() {
            ret.YearRate.Decimal = ret.YearIncreaseFundScale.Decimal.Div(lastYearReport.NetAssets).Mul(decimal.NewFromInt(100)).Round(2)
            ret.YearRate.Valid = true
        }
        return ret,nil

    }
    return ret,nil
}

func GetFundList(req *FundReq) ([]FundList,int,error) {
    // 获取当前所有股票的信息
    fund := []FundList{}
    cnt := 0
    err := models.DB().Table("p_public_fund_position").
        Select("p_public_fund_position.*,p_public_fund_info.is_important").
        Joins("left join p_public_fund_info on p_public_fund_info.`code` = p_public_fund_position.fund_code ").
        Where("p_public_fund_position.networth_propotion > 0.05").
        Where("report_date = ? ",req.DateTime.Format(util.YMD)).
        Where("fund_code = ? or p_public_fund_info.name like ?",req.FundCode,"%"+req.FundCode+"%").Find(&fund).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return fund,cnt,err
    }
    if err == gorm.ErrRecordNotFound {
        return fund,cnt,nil
    }
    // 拼接数据
    wg := sync.WaitGroup{}
    ret := []FundList{}
    wg.Add(len(fund))
    for i,_ := range fund {
        go func(idx int) {
            v := &fund[idx]
            err = GetFundStockData(v)
            if err != nil {
                logging.Info("get total year err:",err)
                return
            }
            if !v.LastHeldNum.IsZero(){
                v.Rate.Valid = true
                v.Rate.Decimal = v.IncreaseHeldNum.Div(v.LastHeldNum).Mul(decimal.NewFromFloat(100)).Round(2)
            }
            if !v.YearHeldNum.IsZero() {
                v.YearRate.Valid = true
                v.YearRate.Decimal = v.YearIncreaseHeldNum.Div(v.YearHeldNum).Mul(decimal.NewFromFloat(100)).Round(2)
            }
            err = GetStockPrice(v)
            if err != nil {
                logging.Info("get total_market_value err:",err)
                return
            }
            ret = append(ret,*v)
            wg.Done()
        }(i)
    }
    wg.Wait()
    if req.IsNew != "all" && req.IsNew != ""{
        isNew := false
        if req.IsNew == "1" {
            isNew = true
        }
        newData := []FundList{}
        for _,value := range ret {
            if value.IsNew == isNew {
                newData = append(newData,value)
            }
        }
        ret = newData
    }
    if req.SortKey != "" {
        sort.Slice(ret, func(i, j int) bool {
            v := reflect.ValueOf(ret[i])
            u := reflect.ValueOf(ret[j])
            for k := 0; k < v.NumField(); k++ {
                if reflect.TypeOf(ret[i]).Field(k).Tag.Get("json") == req.SortKey {
                    return req.Direction == util.Compare(v.Field(k), u.Field(k))
                }
            }
            return false
        })
    }
    lists := []FundList{}
    if req.PageSize > 0 && req.PageNum > 0 {
        start := req.PageSize * (req.PageNum - 1)
        end   := req.PageSize * (req.PageNum)
        if req.PageNum * req.PageSize > len(ret)  {
            end = len(ret)
        }
        lists = ret[start:end]
    }
    return lists,len(ret),err
}

// 根据报告期获取基金规模
func GetScaleByReportDate(report ,code string) (FundAssets,error) {
    var err error
    ret := FundAssets{}
    err = models.DB().Table("p_public_fund_assets").
        Where("code = ? ",code).Where("report_date = ? ",report).First(&ret).Error
    if err != nil && err != gorm.ErrRecordNotFound{
        return FundAssets{},err
    }
    if err == gorm.ErrRecordNotFound {
        return FundAssets{},nil
    }
    type NetAssets struct {
        NetAssets                 decimal.Decimal         `json:"net_assets" gorm:"column:net_assets"`             // 基金净资产
    }
    net := NetAssets{}
    err = models.DB().Table("p_public_fund_scale_change").
        Where("code = ? ",code).Where("report_date = ? ",report).First(&net).Error
    if err != nil && err != gorm.ErrRecordNotFound{
        return FundAssets{},err
    }
    if err == gorm.ErrRecordNotFound {
        return FundAssets{},nil
    }
    ret.NetAssets = net.NetAssets
    return ret,nil
}

// 根据基金 报告期，股票代码 获取环比信息
func GetStockDataByReportDate(reportPeriod,stockCode,fundCode string) (FundList,error) {
    ret := FundList{}
    err := models.DB().Table("p_public_fund_position").
        Select("stock_code,stock_name,held_num,networth_propotion").
        Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_position.fund_code").
        Where("p_public_fund_position.networth_propotion > 0.05").
        Where("stock_code = ? ",stockCode).
        Where("fund_code = ? ",fundCode).
        Where("report_date = ? ",reportPeriod).
        First(&ret).Error
    if err != nil && err != gorm.ErrRecordNotFound{
        return ret,err
    }
    if err == gorm.ErrRecordNotFound {
        return FundList{},nil
    }
    return ret,nil
}
func GetFundStockData(ret *FundList) error  {
    // 获取上一年的最后一个报告期数据
    lastYearReportPeriod := strconv.Itoa(ret.ReportPeriod.AddDate(-1,0,0).Year()) + "-12-31"
    lastYearData,err  := GetStockDataByReportDate(lastYearReportPeriod,ret.StockCode,ret.FundCode)
    if err != nil {
        return err
    }
    // 获取当前报告期的
    reportPeriod := ret.ReportPeriod.Format(util.YMD)
    data,err := GetStockDataByReportDate(reportPeriod,ret.StockCode,ret.FundCode)
    if err != nil {
        return err
    }
    // 获取上一个报告期数据
    lastReportPeriod,_ := GetReportPeriod(ret.ReportPeriod,-1)
    lastReportPeriodData,err := GetStockDataByReportDate(lastReportPeriod,ret.StockCode,ret.FundCode)
    if err != nil {
        return err
    }
    if !lastReportPeriodData.HeldNum.IsZero() {
        ret.IsNew = false
    }else {
        ret.IsNew = true
    }
    ret.YearHeldNum = lastYearData.HeldNum
    ret.HeldNum  = data.HeldNum
    ret.NetWorthPropotion = data.NetWorthPropotion
    ret.LastHeldNum = lastReportPeriodData.HeldNum
    ret.LastNetWorthPropotion = lastReportPeriodData.NetWorthPropotion
    ret.IncreaseHeldNum = data.HeldNum.Sub(lastReportPeriodData.HeldNum)
    ret.YearIncreaseHeldNum = data.HeldNum.Sub(lastYearData.HeldNum)
    ret.IncreaseNetWorthPropotion = data.NetWorthPropotion.Sub(lastReportPeriodData.NetWorthPropotion)
    return nil
}

func GetStockPrice(ret *FundList) error {
    var (
        price decimal.NullDecimal
        err   error
    )
    price,err = getCurrentPrice(ret.StockCode)
    if err != nil && err != gorm.ErrRecordNotFound{
        logging.Info("get current price err ",err)
        return err
    }
    if err == gorm.ErrRecordNotFound {
        return nil
    }
    if !price.Decimal.IsZero() {
        ret.TotalMarketValue.Decimal = price.Decimal.Mul(ret.HeldNum)
        ret.TotalMarketValue.Valid = true
        ret.TotalMarketValueIncrease.Decimal = price.Decimal.Mul(ret.IncreaseHeldNum)
        ret.TotalMarketValueIncrease.Valid = true
    }
    return nil
}

func GetReportPeriodList() (ReportPeriod,error){
    ret := ReportPeriod{}
    now,_ := GetReportPeriod(time.Now(),0)
    last,_ := GetReportPeriod(time.Now(),-1)
    lastTwo,_ := GetReportPeriod(time.Now(),-2)
    lastThree,_ := GetReportPeriod(time.Now(),-3)
    lastFast,_ := GetReportPeriod(time.Now(),-4)
    ret.ReportPeriodList = append(ret.ReportPeriodList,now,last,lastTwo,lastThree,lastFast)
    return ret,nil
}

