package public_fund

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/util"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"time"
)

// 基金
type FundNewReq struct {
	SortKey     string    `json:"sort_key" form:"sort_key"`
	Direction   bool      `json:"direction" form:"direction"`
	DateTime    time.Time `json:"report_date" form:"report_date" time_format:"2006-01-02"`
	PageSize    int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum     int       `json:"page_num" form:"page_num" binding:"required,gte=1"`
}
// 基金页面
type FundNewListResp struct {
	ID                     int                   `json:"-" gorm:"column:id"`
	FundCode               string                `json:"fund_code" gorm:"column:fund_code"`
	FundName               string                `json:"fund_name" gorm:"column:fund_name"`
	Scale                  decimal.Decimal       `json:"scale" gorm:"column:scale"`
	FundManager            string                `json:"fund_manager" gorm:"column:fund_manager"`
	ManagerID              string                `json:"manager_id" gorm:"column:manager_id"`
	CreatedAt              time.Time             `json:"created_at" gorm:"column:created_at"`     // 披露时间
	IsImportant            bool                  `json:"is_important" gorm:"column:is_important"`   // 是否是重点基金
	IsChange               bool                  `json:"is_change"`                                // 是否是重点基金
}

func GetFundNewList(req *FundNewReq) ([]FundNewListResp,error) {
    dbs := models.DB().Table("p_public_fund_position").
		Select("p_public_fund_position.fund_code,p_public_fund_info.name as fund_name,p_public_fund_info.scale,p_public_fund_manager.name as fund_manager,p_public_fund_relationship.manager_id," +
			"p_public_fund_position.created_at,p_public_fund_info.is_important").
		Joins("left join p_public_fund_info on p_public_fund_info.`code` = p_public_fund_position.fund_code ").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.`code` = p_public_fund_position.fund_code ").
		Joins("left join p_public_fund_manager on p_public_fund_manager.id = p_public_fund_relationship.manager_id ")
	if !req.DateTime.IsZero() {
		dbs = dbs.Where("p_public_fund_position.report_date = ? ", req.DateTime)
	}
	fundNewList := []FundNewListResp{}
	err := dbs.Group("p_public_fund_position.fund_code").Find(&fundNewList).Error
	if err != nil {
		return []FundNewListResp{},err
	}
	if req.SortKey != "" {
		sort.Slice(fundNewList, func(i, j int) bool {
			v := reflect.ValueOf(fundNewList[i])
			u := reflect.ValueOf(fundNewList[j])
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(fundNewList[i]).Field(k).Tag.Get("json") == req.SortKey {
					return req.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	} else {
		sort.Slice(fundNewList, func(i, j int) bool {
			return !fundNewList[i].CreatedAt.Before(fundNewList[j].CreatedAt)
		})
	}
	lists := []FundNewListResp{}
	if req.PageSize > 0 && req.PageNum > 0 {
		start := req.PageSize * (req.PageNum - 1)
		end   := req.PageSize * (req.PageNum)
		if req.PageNum * req.PageSize > len(fundNewList)  {
			end = len(fundNewList)
		}
		lists = fundNewList[start:end]
	}
	nowReportPeriod,_ := util.ReportPeriod(-1)
	redisKey := nowReportPeriod + "public_fund_push"
	for i,v := range lists {
		if gredis.HExists(redisKey,v.FundCode) {
			lists[i].IsChange = true
		}
	}
	return lists,nil
}

func GetFundNewListCount(req *FundNewReq) (int,error) {
	dbs := models.DB().Table("p_public_fund_position").
		Select("p_public_fund_position.fund_code,p_public_fund_info.name as fund_name,p_public_fund_info.scale,p_public_fund_manager.name as fund_manager,p_public_fund_position.created_at,p_public_fund_relationship.manager_id").
		Joins("left join p_public_fund_info on p_public_fund_info.`code` = p_public_fund_position.fund_code ").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.`code` = p_public_fund_position.fund_code ").
		Joins("left join p_public_fund_manager on p_public_fund_manager.id = p_public_fund_relationship.manager_id ")
	if !req.DateTime.IsZero() {
		dbs = dbs.Where("p_public_fund_position.report_date = ? ", req.DateTime)
	}
	fundNewList := []FundNewListResp{}
	cnt := 0
	err := dbs.Group("p_public_fund_position.fund_code").Order("p_public_fund_position.created_at desc").Find(&fundNewList).Error
	if err != nil {
		return cnt,err
	}
	return len(fundNewList),nil
}