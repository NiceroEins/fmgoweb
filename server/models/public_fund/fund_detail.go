package public_fund

import (
    "datacenter/models"
    "datacenter/pkg/logging"
    "datacenter/pkg/util"
    "github.com/jinzhu/gorm"
    "github.com/shopspring/decimal"
    "reflect"
    "sort"
    "strconv"
    "sync"
    "time"
)
type FundStockDetailResp struct {
    List            []FundStockDetail             `json:"list"`
    Fund            FundAmountDetail              `json:"fund"`
}
type FundAmountDetail struct {
    MonthFundNum            int                 `json:"month_fund_num"`          // 本月新成立基金数量
    FundNum                 int                 `json:"fund_num"`                // 包含挡墙股票基金数量
    StockName               string              `json:"stock_name"`              // 股票名称
    FundScaleDetail         decimal.Decimal     `json:"fund_scale_detail"`       // 规模
}
type FundStockDetail struct {
    ID               int              `json:"id" gorm:"column:id"`
    FundCode         string           `json:"fund_code" gorm:"column:fund_code"`
    StockCode        string           `json:"stock_code" gorm:"column:stock_code"`
    FundName         string           `json:"fund_name" gorm:"column:fund_name"`
    ManagerName      string           `json:"manager_name" gorm:"column:manager_name"`
    FundManager      int              `json:"fund_manager"`
    FundRelationShip int              `json:"fund_relation_ship"`
    FundRelationIds  []string         `json:"fund_relation_ids"`
    ReportDate       time.Time        `json:"report_date" gorm:"column:report_date"`
    FundRelations    []FundWorth      `json:"fund_relations"`
    AvgGrowthRate    decimal.Decimal  `json:"avg_growth_rate"`
    MaxGrowthRate    decimal.Decimal  `json:"max_growth_rate"`
    MaxGrowthRateName string          `json:"max_growth_rate_name"`
    Children         []ChildrenDetail `json:"children"`
    TradeDate        time.Time        `json:"-"`
    AvgYearGrowthRate decimal.NullDecimal  `json:"avg_year_growth_rate"`
}
type ChildrenDetail struct {
    ID               int              `json:"id" gorm:"column:id"`
    FundCode         string           `json:"fund_code" gorm:"column:fund_code"`
    StockCode        string           `json:"stock_code" gorm:"column:stock_code"`
    FundName         string           `json:"fund_name" gorm:"column:fund_name"`
    ManagerName      string           `json:"manager_name" gorm:"column:manager_name"`
    FundManager      int              `json:"fund_manager"`
    FundRelationShip int              `json:"fund_relation_ship"`
    FundRelationIds  []string         `json:"fund_relation_ids"`
    ReportDate       time.Time        `json:"report_date" gorm:"column:report_date"`
    FundRelations    []FundWorth      `json:"fund_relations"`
    AvgGrowthRate    decimal.Decimal  `json:"avg_growth_rate"`
    MaxGrowthRate    decimal.Decimal  `json:"max_growth_rate"`
    MaxGrowthRateName string          `json:"max_growth_rate_name"`
    AvgYearGrowthRate decimal.NullDecimal  `json:"avg_year_growth_rate"`
}
type FundStockDetailReq struct {
    SortKey     string `json:"sort_key" form:"sort_key"`
    Direction   bool   `json:"direction" form:"direction"`
    PageSize    int    `json:"page_size" form:"page_size" binding:"required,gte=1"`
    PageNum     int    `json:"page_num" form:"page_num" binding:"required,gte=1"`
    StockCode   string `json:"stock_code" form:"stock_code" binding:"required"`
    DateTime    time.Time `json:"date" form:"date" time_format:"2006-01-02"`
}
type FundWorth struct {
    Code                     string                          `json:"code" gorm:"column:code"`
    Name                     string                          `json:"name" gorm:"column:name"`
    GrowthRateEndYear        decimal.NullDecimal             `json:"growth_rate_end_year" gorm:"column:growth_rate_end_year"`
    GrowthRateWeek           decimal.NullDecimal             `json:"growth_rate_week" gorm:"column:growth_rate_week"`
    GrowthRateOneYear        decimal.NullDecimal             `json:"growth_rate_one_year" gorm:"column:growth_rate_one_year"`
}
type MaxFundName struct {
    MaxGrowthRate            decimal.Decimal                  `json:"max_growth_rate" gorm:"column:max_growth_rate"`
    MaxGrowthRateName        string                           `json:"max_growth_rate_name" gorm:"column:max_growth_rate_name"`
}
func GetFundStockDetail(req *FundStockDetailReq) (FundStockDetailResp,int,error){
    fund := []FundStock{}
    ret := FundStockDetailResp{}
    cnt := 0
    dbs :=  models.DB().Table("p_public_fund_info").
        Select("p_public_fund_info.*,p_public_fund_relationship.manager_id as fund_manager").
        Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_info.code").
        Where("establish_date > 0 ")
    if !req.DateTime.IsZero() {
        start := req.DateTime.Format("2006-01")+"-01"
        end := req.DateTime.AddDate(0,1,0).Format("2006-01")+"-01"
        dbs = dbs.Where("establish_date >= ? ",start).Where("establish_date < ? ",end)
    } else {
        now := "2020-12-01"
        dbs = dbs.Where("establish_date >= ? ",now)
    }
    err := dbs.Find(&fund).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return FundStockDetailResp{},cnt,err
    }
    if err == gorm.ErrRecordNotFound {
        return FundStockDetailResp{},cnt,nil
    }
    num := []string{}
    fundNum := []string{}
    // 获取新成立的基金对应的股票
    for _,value := range fund {
        if !util.ContainsAny(num,strconv.Itoa(value.FundManager)) && value.FundManager > 0  {
            num = append(num,strconv.Itoa(value.FundManager))
        }
        if !util.ContainsAny(fundNum,strconv.Itoa(value.ID)) {
            fundNum = append(fundNum,strconv.Itoa(value.ID))
        }
    }
    ret.Fund.MonthFundNum = len(fundNum)
    stock := []FundStockDetail{}
    nowReportDate,_ := GetReportPeriod(req.DateTime,-1)
    err = models.DB().Table("p_public_fund_position").
        Select("p_public_fund_position.*,p_public_fund_relationship.manager_id,p_public_fund_info.name as fund_name," +
            "p_public_fund_manager.name as manager_name,p_public_fund_relationship.manager_id as fund_manager").
        Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_position.fund_code").
        Joins("left join p_public_fund_manager on p_public_fund_manager.id = p_public_fund_relationship.manager_id").
        Joins("left join p_public_fund_info on p_public_fund_info.code = p_public_fund_position.fund_code").
        Where("p_public_fund_relationship.manager_id in (?)",num).
        Where("p_public_fund_position.stock_code = ? ",req.StockCode).
        Where("p_public_fund_position.report_date = ? ",nowReportDate).
        Group("p_public_fund_position.fund_code,p_public_fund_relationship.manager_id").Find(&stock).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return FundStockDetailResp{},cnt,err
    }
    if err == gorm.ErrRecordNotFound {
        return FundStockDetailResp{},cnt,nil
    }
    fundCodes := []string{}
    lastTradeDate,_ := GetLastTradeDate()
    for index,v := range stock {
        if !util.ContainsAny(fundCodes,v.FundCode) {
            fundCodes  = append(fundCodes,v.FundCode)
        }
        stock[index].TradeDate = lastTradeDate
    }
    // 获取基金对应的规模
    data := FundInfo{}
    err = models.DB().Table("p_public_fund_info").
        Select("sum(scale) as scale").
        Where("code in (?) ",fundCodes).
        Find(&data).Error
    if err != nil {
        return FundStockDetailResp{},cnt,nil
    }
    // 获取当前股票名称
    nameData := FundPositionResp{}
    err = models.DB().Table("p_public_fund_position").
        Select("stock_code,stock_name").
        Where("stock_code = ? ",req.StockCode).
        First(&nameData).Error
    if err != nil {
        return FundStockDetailResp{},cnt,nil
    }
    ret.Fund.StockName = nameData.Name
    ret.Fund.FundScaleDetail = data.Scale
    ret.Fund.FundNum = len(stock)
    stocks := []FundStockDetail{}
    // 拼接数据
    wg := sync.WaitGroup{}
    wg.Add(len(stock))
    for i,_ := range stock {
        go func(idx int) {
            v := &stock[idx]
            GetFundDetailData(v)
            stocks = append(stocks,*v)
            wg.Done()
        }(i)
    }
    wg.Wait()
    sort.Slice(stocks, func(i, j int) bool {
        return stocks[i].FundManager > stocks[j].FundManager
    })
    tmpStock := make(map[int]FundStockDetail,0)
    preManager := 0
    for _,value := range stocks {
        if preManager < 1 || preManager != value.FundManager {
            preManager = value.FundManager
            tmpStock[value.FundManager] = value
            continue
        }
        if preManager == value.FundManager {
            child := ChildrenDetail{
                value.ID,
                value.FundCode,
                value.StockCode,
                value.FundName,
                value.ManagerName,
                value.FundManager,
                value.FundRelationShip,
                value.FundRelationIds,
                value.ReportDate,
                value.FundRelations,
                value.AvgGrowthRate,
                value.MaxGrowthRate,
                value.MaxGrowthRateName,
                value.AvgYearGrowthRate,
            }
            tmp := tmpStock[value.FundManager]
            tmp.Children = append(tmpStock[value.FundManager].Children,child)
            tmpStock[value.FundManager] = tmp
        }
    }
    tmp := []FundStockDetail{}
    for _,value := range tmpStock{
        tmp = append(tmp,value)
    }
    stocks = tmp
    if req.SortKey != "" {
        sort.Slice(stocks, func(i, j int) bool {
            v := reflect.ValueOf(stocks[i])
            u := reflect.ValueOf(stocks[j])
            for k := 0; k < v.NumField(); k++ {
                if reflect.TypeOf(stocks[i]).Field(k).Tag.Get("json") == req.SortKey {
                    return req.Direction == util.Compare(v.Field(k), u.Field(k))
                }
            }
            return false
        })
    }
    lists := []FundStockDetail{}
    if req.PageSize > 0 && req.PageNum > 0 {
        start := req.PageSize * (req.PageNum - 1)
        end   := req.PageSize * (req.PageNum)
        if req.PageNum * req.PageSize > len(stocks)  {
            end = len(stocks)
        }
        lists = stocks[start:end]
    }
    ret.List = lists
    // 根据基金经理和股票 统计关联基金和平均持有
    return ret,len(stocks),nil
}

// 通过基金经理人数 获取关联其他基金数量 和平均持有股数
func GetFundDetailData(fund *FundStockDetail) {
    var (
        err   error
    )
    funds := []string{}
    // 获取一只股票对应的基金经理人数
    history := []HistoryList{}
    // 获取当前这个基金经理的基金中包含这只股票的数据
    err = models.DB().Raw("select p_public_fund_relationship.manager_id, " +
        "p_public_fund_position.held_num,p_public_fund_position.networth_propotion,p_public_fund_position.stock_code as code,p_public_fund_position.fund_code as fund_code " +
        "from p_public_fund_relationship " +
        "left join p_public_fund_position on p_public_fund_position.fund_code = p_public_fund_relationship.code " +
        "where p_public_fund_position.stock_code = ? and p_public_fund_position.report_date = ? and p_public_fund_relationship.manager_id = ? " +
        " ORDER BY p_public_fund_position.report_date desc",fund.StockCode,fund.ReportDate,fund.FundManager).Find(&history).Error
    if err != nil {
        return
    }
    relation := []FundWorth{}
    sumGrowthRate := decimal.Decimal{}
    count := 0
    for _,v := range history {
        if !util.ContainsAny(funds,v.FundCode) {
            detail := FundWorth{}
            err := models.DB().Table("p_public_fund_worth").
                Where("p_public_fund_worth.worth_date = ? ",fund.TradeDate).
                Where("code = ? ",v.FundCode).Order("id desc").First(&detail).Error
            if err != nil {
                continue
            }
            funds  = append(funds,v.FundCode)
            fund.FundRelationIds = append(fund.FundRelationIds,v.FundCode)
            // 获取详情
            relation = append(relation,detail)
            count ++
            // 获取平均年收益
            sumGrowthRate = sumGrowthRate.Add(detail.GrowthRateEndYear.Decimal)
            if v.FundCode == fund.FundCode {
                if !detail.GrowthRateOneYear.Decimal.IsZero() {
                    fund.AvgYearGrowthRate.Decimal = detail.GrowthRateOneYear.Decimal
                    fund.AvgYearGrowthRate.Valid   = true
                }
            }
        }
    }
    sort.Slice(relation, func(i, j int) bool {
        return relation[i].GrowthRateEndYear.Decimal.Sub(relation[j].GrowthRateEndYear.Decimal).Sign() > 0
    })

    fund.FundRelations = relation
    if count > 0 {
        fund.AvgGrowthRate = sumGrowthRate.Div(decimal.NewFromInt(int64(count))).Round(2)
    }
    fund.FundRelationShip = len(funds)
    highest := []MaxFundName{}
    // 获取当前这个基金经理的基金中包含这只股票的数据
    err = models.DB().Raw("select growth_rate_end_year as max_growth_rate,p_public_fund_worth.name as max_growth_rate_name from p_public_fund_relationship " +
        "left join p_public_fund_worth on p_public_fund_relationship.code = p_public_fund_worth.code " +
        "where p_public_fund_relationship.manager_id = ?  " +
        "and p_public_fund_worth.worth_date = ? " +
        "ORDER BY growth_rate_end_year desc ",fund.FundManager,fund.TradeDate).Find(&highest).Error
    if err != nil {
        return
    }
    maxGrowthRate := decimal.Decimal{}
    maxGrowthRateName := ""
    for _,value := range highest {
        if maxGrowthRate.IsZero() {
            maxGrowthRate = value.MaxGrowthRate.Round(2)
            maxGrowthRateName = value.MaxGrowthRateName
        } else {
            if value.MaxGrowthRate.Sub(maxGrowthRate).Sign() > 0 {
                maxGrowthRate = value.MaxGrowthRate
                maxGrowthRateName = value.MaxGrowthRateName
            }
        }
    }
    fund.MaxGrowthRate = maxGrowthRate
    fund.MaxGrowthRateName = maxGrowthRateName
}

func GetLastTradeDate() (time.Time,error) {
    now := time.Now().Format(util.YMD)
    type Date struct {
        TradeDate     time.Time        `gorm:"column:trade_date"`
    }
    date := Date{}
    err := models.DB().Table("b_trade_date").
        Where("trade_date < ?",now).Order("trade_date desc").First(&date).Error
    // 获取当前时间
    if err != nil {
        logging.Info("get trade date err",err)
        return time.Time{},err
    }
    return date.TradeDate,nil
}