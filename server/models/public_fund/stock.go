package public_fund

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type FundPosition struct {
	FundCode              string      `json:"fund_code" gorm:"column:fund_code"`
	StockCode             string      `json:"stock_code" gorm:"column:stock_code"`
	ReportDate            time.Time   `json:"report_date" gorm:"column:report_date"`
	StockName             string      `json:"stock_name" gorm:"column:stock_name"`
	NetworthPropotion     string      `json:"networth_propotion" gorm:"column:networth_propotion"`
	HeldNum               decimal.Decimal      `json:"held_num" gorm:"column:held_num"`
}
type FundStockManageAmount struct {
	FundNum            int            `json:"fund_num"`
	FundManagerNum     int            `json:"fund_manager_num"`
	FundRelationShip   int            `json:"fund_relation_ship"`
}

type FundStock struct {
	ID               int              `json:"id" gorm:"column:id"`
	StockCode        string           `json:"stock_code"`
	StockName        string           `json:"stock_name"`
	Tmp              string           `json:"tmp"`
	FundManager      int              `json:"fund_manager"`
	FundRelationShip int              `json:"fund_relation_ship"`
	FundAmount       decimal.Decimal  `json:"fund_amount"`
	FundManagerIds   []string         `json:"fund_manager_ids"`
	FundRelationIds  []string         `json:"fund_relation_ids"`
	FundAmountList   []decimal.Decimal `json:"fund_amount_list"`
	FundPrice        decimal.NullDecimal  `json:"fund_price"`
	ReportDate       time.Time            `json:"report_date" gorm:"column:report_date"`
	StockScale       decimal.Decimal      `json:"stock_scale"`
	NetworthPropotion    decimal.Decimal  `json:"stock_increase"`
}
type FundStockRsp struct {
	List            []FundStock             `json:"list"`
	Fund            FundStockManageAmount   `json:"fund"`
}
type FundHistoryReq struct {
	SortKey     string `json:"sort_key" form:"sort_key"`
	Direction   bool   `json:"direction" form:"direction"`
	PageSize    int    `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum     int    `json:"page_num" form:"page_num" binding:"required,gte=1"`
	DateTime    time.Time `json:"date" form:"date" time_format:"2006-01-02"`
}
type HistoryList struct {
	MangerId              string               `json:"manager_id" gorm:"column:manager_id"`
	StockCode             string               `json:"code" gorm:"column:code"`
	FundCode              string               `json:"fund_code" gorm:"column:fund_code"`
	HeldNum               decimal.Decimal      `json:"held_num" gorm:"column:held_num"`
	NetworthPropotion     decimal.Decimal      `json:"networth_propotion" gorm:"column:networth_propotion"`
}
func(FundPosition) TableName() string {
	return "p_public_fund_position"
}

func GetFundStock(req *FundHistoryReq) (FundStockRsp,int,error){
	fund := []FundStock{}
	ret := FundStockRsp{}
	cnt := 0
	dbs := models.DB().Table("p_public_fund_info").
		Select("p_public_fund_info.*,p_public_fund_relationship.manager_id as fund_manager").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_info.code").
		Where("establish_date > 0 ")
	if !req.DateTime.IsZero() {
		start := req.DateTime.Format("2006-01")+"-01"
		end   := req.DateTime.AddDate(0,1,0).Format("2006-01")+"-01"
		dbs = dbs.Where("establish_date >= ? ",start)
		dbs = dbs.Where("establish_date < ? ",end)
	}else {
		now := "2020-12-01"
		dbs = dbs.Where("establish_date >= ? ",now)
	}
	err := dbs.Find(&fund).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return FundStockRsp{},cnt,err
	}
	if err == gorm.ErrRecordNotFound {
		return FundStockRsp{},cnt,nil
	}
	num := []string{}
	fundNum := []string{}
	// 获取新成立的基金对应的股票
	for _,value := range fund {
		if !util.ContainsAny(num,strconv.Itoa(value.FundManager)) && value.FundManager > 0  {
			num = append(num,strconv.Itoa(value.FundManager))
		}
		if !util.ContainsAny(fundNum,strconv.Itoa(value.ID)) {
			fundNum = append(fundNum,strconv.Itoa(value.ID))
		}
	}
	// 获取所有关联基金
	relationship := 0
	err = models.DB().Table("p_public_fund_relationship").
		Select("distinct manager_id").
		Where("manager_id in (?)",num).Group("code").Count(&relationship).Error
	if err != nil {
		return FundStockRsp{},cnt,err
	}
	ret.Fund.FundRelationShip = relationship
	ret.Fund.FundNum = len(fundNum)
	ret.Fund.FundManagerNum = len(num)
	stock := []FundStock{}
	nowReportDate,_ := GetReportPeriod(time.Now(),-1)
	err = models.DB().Table("p_public_fund_position").
		Select("/*+NO_PARALLEL()*/ p_public_fund_position.*, group_concat(distinct p_public_fund_relationship.manager_id) as tmp," +
			"p_public_fund_relationship.manager_id as fund_manager").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_position.fund_code").
		Where("p_public_fund_relationship.manager_id in (?)",num).
		Where("p_public_fund_position.report_date = ? ",nowReportDate).
		Group("p_public_fund_position.stock_code").Find(&stock).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return FundStockRsp{},cnt,err
	}
	if err == gorm.ErrRecordNotFound {
		return FundStockRsp{},cnt,nil
	}
	stocks := []FundStock{}
	// 拼接数据
	wg := sync.WaitGroup{}
	wg.Add(len(stock))
	for i,_ := range stock {
		go func(idx int) {
			v := &stock[idx]
			if v.Tmp != "" {
				v.FundManagerIds = strings.Split(v.Tmp,",")
				GetFundNumByID(v)
				stocks = append(stocks,*v)
			}
			wg.Done()
		}(i)
	}
	wg.Wait()
	if req.SortKey != "" {
		sort.Slice(stocks, func(i, j int) bool {
			v := reflect.ValueOf(stocks[i])
			u := reflect.ValueOf(stocks[j])
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(stocks[i]).Field(k).Tag.Get("json") == req.SortKey {
					return req.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	}
	if len(stocks) < 1 {
		return FundStockRsp{},cnt,nil
	}
	lists := []FundStock{}
	if req.PageSize > 0 && req.PageNum > 0 {
		start := req.PageSize * (req.PageNum - 1)
		end   := req.PageSize * (req.PageNum)
	    if req.PageNum * req.PageSize > len(stocks)  {
			end = len(stocks) - 1
		}
		lists = stocks[start:end]
	}
	ret.List = lists
    // 根据基金经理和股票 统计关联基金和平均持有
	return ret,len(stocks),nil
}
// 通过基金经理人数 获取关联其他基金数量 和平均持有股数
func GetFundNumByID(fund *FundStock) {
	funds := []string{}
	totalHeldNum := decimal.NewFromFloat(0.00)
	totalNetworthPropotion := decimal.NewFromFloat(0.00)
	// 获取一只股票对应的基金经理人数
	for _,value := range fund.FundManagerIds {
		history := []HistoryList{}
		// 获取当前这个基金经理的基金中包含这只股票的数据
		err := models.DB().Raw("select p_public_fund_relationship.manager_id, " +
			"p_public_fund_position.held_num,p_public_fund_position.networth_propotion,p_public_fund_position.stock_code as code,p_public_fund_position.fund_code as fund_code " +
			"from p_public_fund_relationship " +
			"left join p_public_fund_position on p_public_fund_position.fund_code = p_public_fund_relationship.code " +
			"where p_public_fund_position.stock_code = ? and p_public_fund_relationship.manager_id = ? and p_public_fund_position.report_date = ?" +
			" ORDER BY p_public_fund_position.report_date desc",fund.StockCode,value,fund.ReportDate).Find(&history).Error
		if err != nil {
			continue
		}
		heldNum := decimal.NewFromFloat(0.00)
		tmp := []string{}
		for _,v := range history {
			tmp = append(tmp,v.FundCode)
			if !util.ContainsAny(funds,v.FundCode) {
				funds  = append(funds,v.FundCode)
				fund.FundRelationIds = append(fund.FundRelationIds,v.FundCode)
				totalNetworthPropotion = totalNetworthPropotion.Add(v.NetworthPropotion)
			}
			heldNum = heldNum.Add(v.HeldNum)
		}
		if len(tmp) > 0 {
			fund.FundAmountList = append(fund.FundAmountList,heldNum.Div(decimal.NewFromInt(int64(len(tmp)))))
		}
		totalHeldNum = totalHeldNum.Add(heldNum.Div(decimal.NewFromInt(int64(len(tmp)))))
	}
	fund.FundRelationShip = len(funds)
	fund.FundAmount = totalHeldNum
	fund.FundManager = len(fund.FundManagerIds)
	fund.NetworthPropotion =  totalNetworthPropotion.Div(decimal.NewFromInt(int64(len(funds)))).Round(2)
	var (
		price decimal.NullDecimal
		err   error
		scale decimal.Decimal
	)
	scale,err = GetFundScale(fund.FundManagerIds,fund.ReportDate)
	if err != nil {
		logging.Info("get stock scale err ",err)
		return
	}
	fund.StockScale = scale
	price,err = getCurrentPrice(fund.StockCode)
	if err != nil {
		logging.Info("get current price err ",err)
		return
	}
	if !price.Decimal.IsZero() {
		fund.FundPrice.Decimal = price.Decimal.Mul(totalHeldNum)
		fund.FundPrice.Valid = true
	}
}

// 获取当前股票的基金经理的本月买基金的总规模
func GetFundScale(managerIds []string,date time.Time) (decimal.Decimal,error) {
	var retScale  decimal.Decimal
	if len(managerIds) < 1 {
		logging.Info("get manager_ids err ",managerIds)
		return decimal.Decimal{},nil
	}
	ret := FundInfo{}
	now := ""
	if !date.IsZero() {
		now = date.Format("2006-01")+"-01"
	}else {
		now = time.Now().Format("2006-01")+"-01"
	}
	err := models.DB().Table("p_public_fund_info").
		Select("sum(scale) as scale").
		Joins("left join p_public_fund_relationship on `p_public_fund_relationship`.code = p_public_fund_info.code ").
		Where("p_public_fund_info.establish_date > 0").
		Where("p_public_fund_info.establish_date >= ?",now).
		Where("scale_date > 0 ").Where("p_public_fund_relationship.manager_id in (?)",managerIds).Find(&ret).Error
	if err != nil {
		return retScale,err
	}
	return ret.Scale,nil
}


