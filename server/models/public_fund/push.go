package public_fund

import (
	"datacenter/models"
	"datacenter/models/message"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/pkg/websocket"
	"encoding/json"
	"fmt"
	"github.com/shopspring/decimal"
	"time"
)

type PushFund struct {
	Name              string    `json:"name" gorm:"column:name"`
	Code              string    `json:"code" gorm:"column:code"`
	SubscriptionBegin time.Time `json:"subscription_date_begin" gorm:"column:subscription_date_begin"`
	ManagerId         string    `json:"manager_id" gorm:"column:manager_id"`
	ManagerName       string    `json:"manager_name" gorm:"manager_name"`
}

func PushFundList() {
	PushStarFundManager()
	//PushImportantFund()
}
func PushStarFundManager() {
	// 获取新进的基金
	fund := []PushFund{}
	end := time.Now().Format(util.YMDHM)
	start := time.Now().Add(-time.Second * 60).Format(util.YMDHM)
	err := models.DB().Table("p_public_fund_info").
		Select("p_public_fund_info.name,p_public_fund_info.code,"+
			"p_public_fund_info.subscription_date_begin,p_public_fund_manager.name as manager_name").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_info.code").
		Joins("left join p_public_fund_manager on p_public_fund_manager.id = p_public_fund_relationship.manager_id").
		Where("p_public_fund_info.created_at >= ? ", start).
		Where("p_public_fund_manager.is_star = 1").
		Where("p_public_fund_info.created_at <= ? ", end).Find(&fund).Error
	if err != nil {
		return
	}
	redisKey := "public_fund_manager"
	for _, value := range fund {
		if gredis.HExists(redisKey, value.Code) {
			return
		}
		if !value.SubscriptionBegin.IsZero() {
			m := map[string]interface{}{
				"type":      "public_fund",
				"content":   fmt.Sprintf("%v基金经理人%v开始新发售%v", value.ManagerName, value.SubscriptionBegin.Format("2006年01月02日"), value.Name),
				"push_time": time.Now(),
			}
			ws := websocket.GetInstance()
			bt, _ := json.Marshal(m)
			msg := message.ParseMsg(message.Message_Public_Fund, "system", "", string(bt), true)
			ws.SysSend(msg)
			gredis.HSet(redisKey, value.Code, time.Now())
			gredis.Expire(redisKey, 9*3600)
		}
	}
}

// 重点基金发行报告期推送
func PushImportantFund() {
	// 获取新进的基金
	fund := []PushFund{}
	end := time.Now().Format(util.YMDHM)
	start := time.Now().Add(-time.Second * 60).Format(util.YMDHM)
	err := models.DB().Table("p_public_fund_position").
		Select("p_public_fund_info.name,p_public_fund_info.code").
		Joins("left join p_public_fund_info on p_public_fund_info.code = p_public_fund_position.fund_code").
		Where("p_public_fund_position.created_at >= ? ", start).
		Where("p_public_fund_info.is_important = 1").
		Where("p_public_fund_position.created_at <= ? ", end).Group("p_public_fund_position.fund_code").Find(&fund).Error
	if err != nil {
		return
	}
	redisKey := "public_fund_important"
	for _,value := range fund {
		if gredis.HExists(redisKey,value.Code) {
			return
		}
		m := map[string]interface{}{
			"type":      "public_fund",
			"content":   fmt.Sprintf("%v基金今日公布持仓数据", value.Name),
			"push_time": time.Now(),
		}
		ws := websocket.GetInstance()
		bt, _ := json.Marshal(m)
		msg := message.ParseMsg(message.Message_Stock, "system", "", string(bt), true)
		ws.SysSend(msg)
		gredis.HSet(redisKey, value.Code, time.Now())
		gredis.Expire(redisKey, 9*3600)
	}
}

func FundChange()  {
	// 获取基金的净值
	type FundWorth struct {
		WorthDate         time.Time            `json:"worth_date" gorm:"cloumn:worth_date"`
		UnitNetWorth      decimal.Decimal      `json:"unit_net_worth" gorm:"column:unit_net_worth"`
		GrowthRateDay     decimal.Decimal      `json:"growth_rate_day" gorm:"column:growth_rate_day"`
		FundCode          string               `json:"fund_code" gorm:"column:code"`
	}
	type FundPosition struct {
		FundCode              string                 `json:"fund_code" gorm:"column:fund_code"`
		StockCode             string                 `json:"stock_code" gorm:"column:stock_code"`
		NetworthPropotion     decimal.Decimal        `json:"networth_propotion" gorm:"column:networth_propotion"`
	}
	type StockTick struct {
		StockCode             string                 `json:"stock_code" gorm:"column:stock_code"`
		TradeDate             time.Time              `json:"trade_date" gorm:"column:trade_date"`
		PctChg                decimal.Decimal        `json:"pct_chg" gorm:"column:pct_chg"`
	}
	type HKStockTick struct {
		StockCode             string                 `json:"stock_code" gorm:"column:stock_code"`
		TradeDate             time.Time              `json:"trade_date" gorm:"column:trade_date"`
		PctChg                decimal.Decimal        `json:"pct_chg" gorm:"column:pctchange"`
	}
	// 获取规模排名前100的基金
	type FundInfo struct {
		Name                  string                 `json:"name" gorm:"column:name"`
		Code                  string                 `json:"code" gorm:"column:code"`
		Scale                 string                `json:"scale" gorm:"column:scale"`
		NetRedioShares        decimal.Decimal        `json:"net_retio_shares" gorm:"column:net_retio_shares"`
	}
	info := []FundInfo{}
	// 获取当前报告期
	nowReportPeriod,_ := util.ReportPeriod(-1)
	err := models.Data().Table("p_public_fund_info").
		Select("p_public_fund_info.*,p_public_fund_assets.net_retio_shares as net_retio_shares").
		Joins("left join p_public_fund_assets on p_public_fund_info.code = p_public_fund_assets.code").
		Where("p_public_fund_info.scale > 8000000000 and p_public_fund_assets.net_retio_shares > 90 ").
		Where("p_public_fund_assets.report_date = ?",nowReportPeriod).
		Group("p_public_fund_info.code").Find(&info).Error
	if err != nil {
		return
	}
	now := time.Now().AddDate(0,0,-1)
	redisKey := nowReportPeriod + "public_fund_push"
	for _,v := range info {
		// 获取当天的净值
		worth := FundWorth{}
		err = models.Data().Table("p_public_fund_worth").Where("code = ? ",v.Code).
			Where("worth_date = ? ",now.Format(util.YMD)).First(&worth).Error
		if err != nil {
			continue
		}
		position := []FundPosition{}
		// 获取当前基金的重仓股
		err = models.Data().Table("p_public_fund_position").
			Where("fund_code = ? ",worth.FundCode).Where("report_date = ? ",nowReportPeriod).
			Find(&position).Error
		divValue := decimal.NewFromFloat(0.00)
		allP := decimal.NewFromFloat(0.00)
		ten := decimal.NewFromFloat(0.00)
		leftP := decimal.NewFromFloat(0.00)
		leftHK := decimal.NewFromFloat(0.00)
		count := 0
		// 获取每支股票的涨跌幅
		for _,v := range position {
			// 获取每天的涨跌幅
			if len(v.StockCode) > 5 {
				count ++
				pct := StockTick{}
				err = models.Data().Table("p_stock_tick").
					Where("stock_code = ? ",v.StockCode).Where("trade_date = ? ",now.Format(util.YMD)).
					First(&pct).Error
				allP = allP.Add(v.NetworthPropotion)
				divValue = divValue.Add(pct.PctChg.Mul(v.NetworthPropotion).Div(decimal.NewFromFloat(100.00)))
			}else {
				pct := HKStockTick{}
				err = models.Data().Table("p_hk_stock_tick").
					Where("code = ? ",v.StockCode).Where("time = ? ",now.Format(util.YMD)).
					First(&pct).Error
				allP = allP.Add(v.NetworthPropotion)
				divValue = divValue.Add(pct.PctChg.Mul(v.NetworthPropotion).Div(decimal.NewFromFloat(100.00)))
			}
		}
		pctIndex := StockTick{}
		err = models.Data().Table("p_stock_tick").
			Where("ts_code = '000300.XSHG' ").Where("trade_date = ? ",now.Format(util.YMD)).
			First(&pctIndex).Error
		if count < 10 {
			hkIndex := HKStockTick{}
			err = models.Data().Table("p_hk_stock_tick").Select("avg(pctchange) as pct_chg").
				Where("time = ? ",now.Format(util.YMD)).
				First(&hkIndex).Error
			leftHK = decimal.NewFromFloat(100).Sub(allP).
				Mul(decimal.NewFromInt(int64(10-count)).Div(decimal.NewFromInt(10))).
				Mul(hkIndex.PctChg).Div(decimal.NewFromFloat(100.00))
		}
		leftP = decimal.NewFromFloat(100).Sub(allP).
			Mul(decimal.NewFromInt(int64(count)).Div(decimal.NewFromInt(10))).
			Mul(pctIndex.PctChg).Div(decimal.NewFromFloat(100.00))
		ten = divValue
		divValue = divValue.Add(leftP).Add(leftHK)
		divValue = divValue.Mul(v.NetRedioShares).Div(decimal.NewFromFloat(100))
		if worth.GrowthRateDay.IsZero() {
			continue
		}
		rateChange := worth.GrowthRateDay.Sub(divValue).Abs().Div(worth.GrowthRateDay).Round(4)
		if rateChange.Sub(decimal.NewFromFloat(2.00)).Sign() > 0 || rateChange.Sub(decimal.NewFromFloat(-2.00)).Sign() < 0{
			if gredis.HExists(redisKey, v.Code) {
				return
			}
			m := map[string]interface{}{
				"type":      "system_public_fund",
				"dialect":   fmt.Sprintf("%v基金疑似调仓", v.Name),
				"content":   fmt.Sprintf("%v日%v基金净值涨幅数据%v%%,十大持仓股预测涨幅%v%%,其他部分预测计算涨幅%v%%", now.Format(util.YMD), v.Name,worth.GrowthRateDay.Round(3),ten.Round(3),leftP.Round(3)),
			}
			ws := websocket.GetInstance()
			bt, _ := json.Marshal(m)
			msg := message.ParseMsg(message.Message_Public_Fund, "system", "", string(bt), true)
			ws.SysSend(msg)
			gredis.HSet(redisKey, v.Code, time.Now().Format(util.YMDHMS))
			gredis.Expire(redisKey, 100*24*3600)
			logging.Info(fmt.Sprintf("日期为：%v,当前涨跌幅%v,十大持仓股%v,港股指数涨跌幅%v,沪深300涨跌幅%v,当前涨跌幅比例%v,计算得到涨跌幅%v,涨跌幅偏差%v,其他比例%v,沪深300涨幅%v",now.Format(util.YMD), worth.GrowthRateDay.Round(4),ten.Round(4),leftHK.Round(4),leftP.Round(4),worth.GrowthRateDay.Sub(divValue).Abs().Div(worth.GrowthRateDay).Round(4),divValue.Round(4),worth.GrowthRateDay.Sub(divValue).Round(4),decimal.NewFromFloat(100).Sub(allP).Round(4),pctIndex.PctChg.Round(4)))
		}
	}
}