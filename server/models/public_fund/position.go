package public_fund

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strconv"
	"sync"
	"time"
)

// 基金持仓
type FundPositionReq struct {
	SortKey   string    `json:"sort_key" form:"sort_key"`
	Direction bool      `json:"direction" form:"direction"`
	DateTime  time.Time `json:"report_date" form:"report_date" time_format:"2006-01-02"`
	Code      string    `json:"code" form:"code"`
	PageSize  int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum   int       `json:"page_num" form:"page_num" binding:"required,gte=1"`
	IsNew     string    `json:"is_new" form:"is_new"`
}

type FundPositionResp struct {
	Code                     string              `json:"code" gorm:"column:stock_code"`
	Name                     string              `json:"name" gorm:"column:stock_name"`
	IsNew                    bool                `json:"is_new"`                                              // 是否是新股
	FundNums                 int                 `json:"fund_nums" gorm:"column:fund_nums"`                   // 最新报告期基金数量
	IncreaseFundNums         int                 `json:"increase_fund_nums"`                                  // 新增基金数量
	PreQuarterFundNums       int                 `json:"pre_quarter_fund_nums"`                               // 上一个报告期的基金数量
	YearIncreaseFundNums     int                 `json:"year_increase_fund_nums"`                             // 今年累计增加数量
	YearFundNum              int                 `json:"year_fund_num"`                                       // 去年最后一个报告期基金数量
	HeldNum                  int                 `json:"held_num" gorm:"column:held_num"`                     // 最新报告期累计持股
	IncreaseHeldNum          int                 `json:"increase_held_num"`                                   // 新增持股数量
	YearIncreaseHeldNum      int                 `json:"year_increase_held_num" `                             // 今年累计新增持股数量
	PreQuarterHeldNum        int                 `json:"pre_quarter_held_num"`                                // 上一个报告期持股数量
	YearHeldNum              int                 `json:"year_held_num"`                                       // 去年最后一个报告期持股数量
	ReportPeriod             time.Time           `json:"report_date" gorm:"column:report_date"`               // 报告期
	NetworthPropotion        decimal.Decimal     `json:"networth_propotion" gorm:"column:networth_propotion"` //占净值比
	FundRate                 decimal.NullDecimal `json:"fund_rate"`                                           // 基金数量环比
	FundRateTotal            decimal.NullDecimal `json:"fund_rate_total"`                                     // 基金数量今年累计环比
	HeldNumRate              decimal.NullDecimal `json:"held_num_rate"`                                       // 持股数量环比
	TotalHeldNumRate         decimal.NullDecimal `json:"total_fund_rate"`                                     // 持股数量累计环比
	TotalMarketValue         decimal.NullDecimal `json:"total_market_value"`                                  // 总市值
	TotalMarketValueIncrease decimal.NullDecimal `json:"total_market_value_increase"`                         // 当前总市值

	NowQuarterHeldNum  int `json:"now_quarter_held_num" gorm:"column:now_quarter_held_num"`    // 当前报告期持股数量
	NowQuarterFundNums int `json:"now_quarter_fund_nums"  gorm:"column:now_quarter_fund_nums"` // 当前报告期基金数量
}

type Position struct {
	ID    int    `json:"id" gorm:"column:id"`
	Code  string `json:"code" gorm:"column:stock_code"`
	IsNew int    `json:"is_new" gorm:"column:is_new"`
}

type StockTick struct {
	TradeDate *time.Time          `json:"trade_date" gorm:"column:trade_date"`
	Close     decimal.NullDecimal `json:"close" gorm:"column:close"`
}

func (StockTick) TableName() string {
	return "p_stock_tick"
}

var cache sync.Map

func GetStockPosition(req *FundPositionReq) ([]FundPositionResp, int, error) {
	var err error
	position := []FundPositionResp{}
	ret := []FundPositionResp{}
	key := generateKey(req.DateTime.String() + req.Code)
	itf, ok := cache.Load(key)
	if ok {
		tmp, ok := itf.([]FundPositionResp)
		if ok {
			lastTimeStr, ok := cache.Load(key + "_time")
			if ok {
				lastTime, err := time.ParseInLocation(util.YMDHMS, lastTimeStr.(string), time.Local)
				if err == nil && time.Now().Sub(lastTime.Add(time.Minute*10)).Minutes() < 0 {
					ret = tmp
				}
			}
		}
	}
	if len(ret) > 1 {
		if req.IsNew != "all" && req.IsNew != "" {
			isNew := false
			if req.IsNew == "1" {
				isNew = true
			}
			newData := []FundPositionResp{}
			for _, value := range ret {
				if value.IsNew == isNew {
					newData = append(newData, value)
				}
			}
			ret = newData
		}
		if req.SortKey != "" {
			sort.Slice(ret, func(i, j int) bool {
				v := reflect.ValueOf(ret[i])
				u := reflect.ValueOf(ret[j])
				for k := 0; k < v.NumField(); k++ {
					if reflect.TypeOf(ret[i]).Field(k).Tag.Get("json") == req.SortKey {
						return req.Direction == util.Compare(v.Field(k), u.Field(k))
					}
				}
				return false
			})
		}
		lists := []FundPositionResp{}
		if req.PageSize > 0 && req.PageNum > 0 {
			start := req.PageSize * (req.PageNum - 1)
			end := req.PageSize * (req.PageNum)
			if req.PageNum*req.PageSize > len(ret) {
				end = len(ret)
			}
			lists = ret[start:end]
		}
		return lists, len(ret), err
	}
	cnt := 0
	dbs := models.DB().Table("p_public_fund_position").
		Select("stock_code,stock_name,report_date").
		Where("p_public_fund_position.networth_propotion > 0.05")
	if req.Code != "" {
		dbs = dbs.Where("stock_code like ? or stock_name like ? ", "%"+req.Code+"%", "%"+req.Code+"%")
	}
	if !req.DateTime.IsZero() {
		dbs = dbs.Where("report_date = ? ", req.DateTime)
	}
	err = dbs.Group("stock_code").Order("id desc").Find(&position).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return position, cnt, nil
	}
	// 拼接数据
	wg := sync.WaitGroup{}
	wg.Add(len(position))
	t := time.Now()
	for i, _ := range position {
		go func(idx int) {
			v := &position[idx]
			err = GetTotalYear(v)
			if err != nil {
				logging.Info("get total year err:", err)
				return
			}
			v.IncreaseFundNums = v.NowQuarterFundNums - v.PreQuarterFundNums
			v.IncreaseHeldNum = v.NowQuarterHeldNum - v.PreQuarterHeldNum
			if v.PreQuarterFundNums > 0 {
				v.FundRate.Valid = true
				v.FundRate.Decimal = decimal.NewFromInt(int64(v.IncreaseFundNums)).Div(decimal.NewFromInt(int64(v.PreQuarterFundNums))).Mul(decimal.NewFromFloat(100)).Round(2)
			}
			if v.PreQuarterHeldNum > 0 {
				v.HeldNumRate.Valid = true
				v.HeldNumRate.Decimal = decimal.NewFromInt(int64(v.IncreaseHeldNum)).Div(decimal.NewFromInt(int64(v.PreQuarterHeldNum))).Mul(decimal.NewFromFloat(100)).Round(2)
			}
			if v.YearHeldNum > 0 {
				v.FundRateTotal.Valid = true
				v.FundRateTotal.Decimal = decimal.NewFromInt(int64(v.YearIncreaseFundNums)).Div(decimal.NewFromInt(int64(v.YearFundNum))).Mul(decimal.NewFromFloat(100)).Round(2)
			}
			if v.YearHeldNum > 0 {
				v.TotalHeldNumRate.Valid = true
				v.TotalHeldNumRate.Decimal = decimal.NewFromInt(int64(v.YearIncreaseHeldNum)).Div(decimal.NewFromInt(int64(v.YearHeldNum))).Mul(decimal.NewFromFloat(100)).Round(2)
			}
			err = getTotalMarketValue(v)
			if err != nil {
				logging.Info("get total_market_value err:", err)
				return
			}
			ret = append(ret, *v)
			wg.Done()
		}(i)
	}
	wg.Wait()
	if len(ret) < 1 {
		return position, cnt, nil
	}
	logging.Info(time.Now().Sub(t))
	// store
	cache.Store(key, ret)
	cache.Store(key+"_time", time.Now().Format(util.YMDHMS))
	if req.IsNew != "all" && req.IsNew != "" {
		isNew := false
		if req.IsNew == "1" {
			isNew = true
		}
		newData := []FundPositionResp{}
		for _, value := range ret {
			if value.IsNew == isNew {
				newData = append(newData, value)
			}
		}
		ret = newData
	}
	if req.SortKey != "" {
		sort.Slice(ret, func(i, j int) bool {
			v := reflect.ValueOf(ret[i])
			u := reflect.ValueOf(ret[j])
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(ret[i]).Field(k).Tag.Get("json") == req.SortKey {
					return req.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	}
	lists := []FundPositionResp{}
	if req.PageSize > 0 && req.PageNum > 0 {
		start := req.PageSize * (req.PageNum - 1)
		end := req.PageSize * (req.PageNum)
		if req.PageNum*req.PageSize > len(ret) {
			end = len(ret)
		}
		lists = ret[start:end]
	}
	return lists, len(ret), err
}

func GetReportPeriod(date time.Time, offset int) (string, int) {
	reportList := []string{"03-31", "06-30", "09-30", "12-31"}
	reportStartList := []string{"01-01", "04-01", "05-01", "09-01", "11-01"}
	//date:=time.Date(2020,8,31,0,0,1,0,time.Local)
	var reportPeriod string
	q := 0
	for i, r := range reportStartList {
		if date.Format(util.MD) >= r {
			i = i - 1
			if (i+offset)%4 >= 0 {
				q = (i + offset) % 4
				reportPeriod = strconv.Itoa(date.Year()+(i+offset)/4) + "-" + reportList[q]
			} else {
				q = (i+offset)%4 + 4
				reportPeriod = strconv.Itoa(date.Year()+(i+offset-4)/4) + "-" + reportList[q]
			}
		}

	}
	return reportPeriod, q + 1
}

func GetReportPeriodData(date, code string) (FundPositionResp, error) {
	ret := FundPositionResp{}
	err := models.DB().Table("p_public_fund_position").
		Select("stock_code,stock_name,count(fund_code) as fund_nums,sum(held_num) as held_num,avg(networth_propotion) as networth_propotion").
		Where("p_public_fund_position.networth_propotion > 0.05").
		Where("stock_code = ? ", code).
		Where("report_date = ? ", date).
		First(&ret).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return FundPositionResp{}, err
	}
	if err == gorm.ErrRecordNotFound {
		return FundPositionResp{}, nil
	}
	return ret, nil
}

func GetTotalYear(ret *FundPositionResp) error {
	// 获取上一年的最后一个报告期数据
	lastYearReportPeriod := strconv.Itoa(ret.ReportPeriod.AddDate(-1, 0, 0).Year()) + "-12-31"
	lastYearData, err := GetReportPeriodData(lastYearReportPeriod, ret.Code)
	if err != nil {
		return err
	}
	// 获取当前报告期的
	reportPeriod := ret.ReportPeriod.Format(util.YMD)
	data, err := GetReportPeriodData(reportPeriod, ret.Code)
	if err != nil {
		return err
	}
	// 获取上一个报告期数据
	lastReportPeriod, _ := GetReportPeriod(ret.ReportPeriod, -1)
	lastReportPeriodData, err := GetReportPeriodData(lastReportPeriod, ret.Code)
	if err != nil {
		return err
	}
	ret.PreQuarterFundNums = lastReportPeriodData.FundNums
	if lastReportPeriodData.FundNums > 0 {
		ret.IsNew = false
	} else {
		ret.IsNew = true
	}
	ret.PreQuarterHeldNum = lastReportPeriodData.HeldNum
	ret.FundNums = data.FundNums
	ret.HeldNum = data.HeldNum
	ret.NowQuarterFundNums = data.FundNums
	ret.NowQuarterHeldNum = data.HeldNum
	ret.YearFundNum = lastYearData.FundNums
	ret.YearHeldNum = lastYearData.HeldNum
	ret.YearIncreaseFundNums = data.FundNums - lastYearData.FundNums
	ret.YearIncreaseHeldNum = data.HeldNum - lastYearData.HeldNum
	return nil
}

func getTotalMarketValue(ret *FundPositionResp) error {
	var (
		err error
	)
	price := StockTick{}
	err = models.DB().Table("p_stock_tick").
		Where("stock_code = ? ", ret.Code).
		Where("trade_date <= ? ", time.Now().Format(util.YMD)).
		Order("trade_date desc").Limit(1).First(&price).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("stock_code", ret.Code)
		logging.Info("getTotalMarketValue ", err)
		return err
	}
	if err == gorm.ErrRecordNotFound {
		return nil
	}
	ret.TotalMarketValue.Decimal = price.Close.Decimal.Mul(decimal.NewFromInt(int64(ret.NowQuarterHeldNum)))
	ret.TotalMarketValue.Valid = true
	ret.TotalMarketValueIncrease.Decimal = price.Close.Decimal.Mul(decimal.NewFromInt(int64(ret.IncreaseHeldNum)))
	ret.TotalMarketValueIncrease.Valid = true
	return nil
}

func getRedisValue(key string, field string) string {
	value, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString(key, field)
	if err != nil {
		logging.Info("redis get "+key+" "+field+" value err:", err.Error())
		return ""
	}
	return value
}

func GetPresentReportData(code string) (FundPositionResp, error) {
	present := FundPositionResp{}
	// 获取这个股票当前最新的报告期
	err := models.DB().Table("p_public_fund_position").
		Where("stock_code = ? ", code).
		Order("report_date desc").
		First(&present).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return present, err
	}
	if err == gorm.ErrRecordNotFound {
		return FundPositionResp{}, nil
	}
	return present, nil
}

func getCurrentPrice(code string) (decimal.NullDecimal, error) {
	var (
		err error
	)
	price := StockTick{}
	err = models.Data().Table("p_stock_tick").
		Where("stock_code = ? ", code).
		Where("trade_date <= ? ", time.Now().Format(util.YMD)).
		Order("trade_date desc").Limit(1).First(&price).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return decimal.NullDecimal{}, err
	}
	if err == gorm.ErrRecordNotFound {
		return decimal.NullDecimal{}, nil
	}
	return price.Close, nil

}
func generateKey(hr interface{}) string {
	b, _ := json.Marshal(hr)
	hash := fmt.Sprintf("%d", util.BKDRHash(string(b)))
	return hash
}
