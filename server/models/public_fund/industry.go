package public_fund

import (
	"datacenter/models"
	"datacenter/pkg/util"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"time"
)

// 基金
type FundIndustryReq struct {
	SortKey     string    `json:"sort_key" form:"sort_key"`
	FundCode    string    `json:"fund_code" form:"fund_code" binding:"required"`
	Direction   bool      `json:"direction" form:"direction"`
	DateTime    time.Time `json:"report_date" form:"report_date" time_format:"2006-01-02"`
}
type FundIndustryResp struct {
    FundCode         string               `json:"fund_code" gorm:"column:code"`
    ReportDate       time.Time            `json:"report_date" gorm:"column:report_date"`
    Category         string               `json:"category" gorm:"column:category"`
	NetPropotion     decimal.Decimal      `json:"net_propotion" gorm:"column:net_propotion"`
	Market           decimal.Decimal      `json:"market" gorm:"clomun:market"`
    LastReportMarket decimal.Decimal      `json:"last_report_market" gorm:"column:last_report_market"`
    IncreaseMarket   decimal.NullDecimal  `json:"increase_market"`
    Rate             decimal.NullDecimal  `json:"rate" gorm:"column:rate"`
	IsNew            bool                 `json:"is_new"`                         // 是否是新进行业
}
type Industry struct {
	FundCode         string               `json:"fund_code" gorm:"column:fund_code"`
	ReportDate       time.Time            `json:"report_date" gorm:"column:report_date"`
	Category         string               `json:"category" gorm:"column:category"`
	NetPropotion     decimal.Decimal      `json:"net_propotion" gorm:"column:net_propotion"`
	Market           decimal.Decimal      `json:"market" gorm:"clomun:market"`
}

type FundRankReq struct {
	SortKey     string    `json:"sort_key" form:"sort_key"`
	Direction   bool      `json:"direction" form:"direction"`
	DateTime    time.Time `json:"report_date" form:"report_date" time_format:"2006-01-02"`
	PageSize    int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum     int       `json:"page_num" form:"page_num" binding:"required,gte=1"`
}

type FundRankResp struct {
	FundCode       string               `json:"fund_code" gorm:"column:code"`
	ReportDate     time.Time            `json:"report_date" gorm:"column:report_date"`
	FundName       string               `json:"fund_name" gorm:"column:fund_name"`
	Purchase       decimal.NullDecimal         `json:"purchase" gorm:"column:purchase"`        // 期间申购
	Redeem         decimal.NullDecimal         `json:"redeem" gorm:"column:redeem"`            // 期间赎回
	TotalShare     decimal.NullDecimal         `json:"total_share" gorm:"column:total_share"`  // 期末总份额
	NetAssets      decimal.NullDecimal         `json:"net_assets" gorm:"column:net_assets"`    // 期末净资产
	Rate           decimal.NullDecimal         `json:"rate"`
	LastReportNetAssets  decimal.Decimal       `json:"last_report_net_assets" gorm:"column:last_report_net_assets"`  // 上个报告期总资产
	IncreaseNetAssets    decimal.NullDecimal   `json:"increase_net_assets"`
	IsImportant    bool                        `json:"is_important" gorm:"column:is_important"`
}

type FundScaleChange struct {
	FundCode       string               `json:"fund_code" gorm:"column:fund_code"`
	ReportDate     time.Time            `json:"report_date" gorm:"column:report_date"`
	Purchase       decimal.Decimal      `json:"purchase" gorm:"column:purchase"`        // 期间申购
	Redeem         decimal.Decimal      `json:"redeem" gorm:"column:redeem"`            // 期间赎回
	TotalShare     decimal.Decimal      `json:"total_share" gorm:"column:total_share"`  // 期末总份额
	NetAssets      decimal.Decimal      `json:"net_assets" gorm:"column:net_assets"`    // 期末净资产
}
func GetIndustryListByFund(req *FundIndustryReq) ([]FundIndustryResp,error) {
	ret := []FundIndustryResp{}
	dbs := models.DB().Table("p_public_fund_industry").
		Joins("left join p_public_fund_info on p_public_fund_info.code = p_public_fund_industry.code").
		Where("report_date = ?",req.DateTime.Format(util.YMD)).
		Where("p_public_fund_info.code = ? or p_public_fund_info.name like ?",req.FundCode,"%"+req.FundCode+"%")
	if req.SortKey != "" {
		if req.Direction {
			dbs = dbs.Order(req.SortKey + " asc ")
		}else {
			dbs = dbs.Order(req.SortKey + " desc ")
		}
	}
	err := dbs.Find(&ret).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return ret,err
	}
	if err == gorm.ErrRecordNotFound {
		return ret,nil
	}
	for index,value := range ret {
		lastReportPeriod,_ := GetReportPeriod(value.ReportDate,-1)
		last,err := getIndustryByReportDate(lastReportPeriod ,value.Category,value.FundCode)
		if err != nil {
			continue
		}
		if !last.Market.IsZero() {
			ret[index].IsNew = false
		}else {
			ret[index].IsNew = true
		}
		ret[index].Market = value.Market.Round(2)
		if !last.Market.IsZero() {
			ret[index].LastReportMarket = last.Market.Round(2)
			ret[index].IncreaseMarket.Decimal = value.Market.Sub(last.Market).Round(2)
			ret[index].Rate.Decimal = value.Market.Sub(last.Market).Div(last.Market).Mul(decimal.NewFromFloat(100.00)).Round(2)
			ret[index].Rate.Valid = true
			ret[index].IncreaseMarket.Valid = true
		}
	}
	return ret,nil
}

// 根据报告期获取基金规模
func getIndustryByReportDate(report ,category,fundCode string) (Industry,error) {
	var err error
	ret := Industry{}
	err = models.DB().Table("p_public_fund_industry").
		Where("code = ? ",fundCode).Where("report_date = ? ",report).
		Where("category = ? ",category).First(&ret).Error
	if err != nil && err != gorm.ErrRecordNotFound{
		return Industry{},err
	}
	if err == gorm.ErrRecordNotFound {
		return Industry{},nil
	}
	return ret,nil
}

func GetFundRankList(req *FundRankReq) ([]FundRankResp,int,error)   {
	ret := []FundRankResp{}
	cnt := 0
	dbs := models.DB().Table("p_public_fund_scale_change").
		Select("p_public_fund_scale_change.*,p_public_fund_info.name as fund_name,p_public_fund_info.is_important as is_important").
		Joins("left join p_public_fund_info on p_public_fund_scale_change.code = p_public_fund_info.code ").
		Where("report_date = ?",req.DateTime.Format(util.YMD))
	err := dbs.Find(&ret).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return ret,cnt,err
	}
	if err == gorm.ErrRecordNotFound {
		return ret,cnt,nil
	}
	for index,value := range ret {
		lastReportPeriod,_ := GetReportPeriod(value.ReportDate,-1)
		last,err := getScaleChangeByReportDate(lastReportPeriod,value.FundCode)
		if err != nil {
			continue
		}
		ret[index].NetAssets.Decimal = value.NetAssets.Decimal.Round(2)
		ret[index].Rate.Valid = true
		if !last.NetAssets.IsZero() {
			ret[index].LastReportNetAssets = last.NetAssets.Round(2)
			ret[index].Rate.Decimal = value.NetAssets.Decimal.Sub(last.NetAssets).Div(last.NetAssets).Mul(decimal.NewFromFloat(100.00)).Round(2)
			ret[index].Rate.Valid = true
			ret[index].IncreaseNetAssets.Valid = true
			ret[index].IncreaseNetAssets.Decimal = value.NetAssets.Decimal.Sub(last.NetAssets).Round(2)
		}
	}
	if req.SortKey != "" {
		sort.Slice(ret, func(i, j int) bool {
			v := reflect.ValueOf(ret[i])
			u := reflect.ValueOf(ret[j])
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(ret[i]).Field(k).Tag.Get("json") == req.SortKey {
					return req.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	}
	lists := []FundRankResp{}
	if req.PageSize > 0 && req.PageNum > 0 {
		start := req.PageSize * (req.PageNum - 1)
		end   := req.PageSize * (req.PageNum)
		if req.PageNum * req.PageSize > len(ret)  {
			end = len(ret)
		}
		if req.PageSize * (req.PageNum - 1) > len(ret) {
			start = 0
		}
		lists = ret[start:end]
	}
	return lists,len(ret),err
}

// 根据报告期获取基金净资产
func getScaleChangeByReportDate(report ,fundCode string) (FundScaleChange,error) {
	var err error
	ret := FundScaleChange{}
	err = models.DB().Table("p_public_fund_scale_change").
		Where("report_date = ? ",report).
		Where("code = ? ",fundCode).First(&ret).Error
	if err != nil && err != gorm.ErrRecordNotFound{
		return FundScaleChange{},err
	}
	if err == gorm.ErrRecordNotFound {
		return FundScaleChange{},nil
	}
	return ret,nil
}



