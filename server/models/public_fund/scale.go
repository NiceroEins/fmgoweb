package public_fund

import (
	"datacenter/models"
	"datacenter/models/message"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/pkg/websocket"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"sort"
	"time"
)

type Scale struct {
	TotalScale    decimal.NullDecimal     `json:"-" gorm:"column:total_scale"`
	Date          time.Time               `json:"-"`
	DateStr       string                  `json:"-" gorm:"column:establish_date"`
	Month         string                  `json:"month"`
	Count         int                     `json:"count" gorm:"column:count"`
	OnSale        int                     `json:"on_sale"`
	Scale         string                  `json:"total_scale"`
}
type FundNum struct {
	TotalNum     int                      `json:"total_num"`
	MonthNum     int                      `json:"month_num"`
	Rate         decimal.Decimal          `json:"rate"`
	PreMonthNum  int                      `json:"pre_month_num"`
}
type FundScale struct {
	TotalScale        decimal.Decimal          `json:"total_scale"`
	Rate              decimal.Decimal          `json:"rate"`
	MonthScale        decimal.Decimal          `json:"month_scale"`
	PreMonthScale    decimal.Decimal           `json:"pre_month_scale"`
}

type ScaleResp struct {
	List           []Scale                   `json:"list"`
	FundNum        FundNum                   `json:"fund_num"`
	FundScale      FundScale                 `json:"fund_scale"`
	MaxFundNum     int                       `json:"max_fund_num"`
	MaxFundScale   decimal.Decimal           `json:"max_fund_scale"`
	TradeDate      string                    `json:"trade_date"`
}
type FundInfo struct {
	Name                   string                      `json:"name" gorm:"column:name"`
	Code                   string                      `json:"code" gorm:"column:code"`
	SubscriptionBegin      time.Time                   `json:"subscription_begin" gorm:"column:subscription_begin"`
	SubscriptionEnd        time.Time                   `json:"subscription_end" gorm:"column:subscription_end"`
	EstablishDate          time.Time                   `json:"establish_date" gorm:"column:establish_date"`
	ApplyStatus            string                      `json:"apply_status" gorm:"column:apply_status"`
	Scale                  decimal.Decimal             `json:"scale" gorm:"column:scale"`
	ScaleDate              time.Time                   `json:"scale_date" gorm:"column:scale_date"`
}

func(FundInfo) TableName() string {
	return "p_public_fund_info"
}

func GetScaleList() (ScaleResp,error) {
	ret := ScaleResp{}
	var (
		total   []Scale
		err     error
	)
	total = GetTotal()
	if len(total) < 1 {
		return ret,errors.New("total data is empty")
	}
	ret.List,ret.MaxFundNum,ret.MaxFundScale,err = GetScalePicList(total)
	if err != nil {
		return ret,err
	}
	ret.FundScale,err = GetScaleQuator(total)
	if err != nil {
		return ret,err
	}
	ret.FundNum = GetFundNumStastic(total)
	// 获取交易日
	tradeDate,err := GetLatestTradeDate()
	if err != nil {
		logging.Info("get trade_date err",err)
	}
	ret.TradeDate = tradeDate.Format("2006年01月02日")
	return ret,nil
}

func GetTotal() []Scale {
	// 获取全部数据
	scale := []Scale{}
	err := models.DB().Raw("select * from ( " +
		"select count(*) as count ,sum(scale) as total_scale ,left(establish_date,7) as establish_date from p_public_fund_info where establish_date > 0 " +
		" GROUP BY left(establish_date,7) " +
		") as a order by establish_date desc limit 12 ").Find(&scale).Error
	if err != nil {
		logging.Info("get scale list err :",err)
		return scale
	}
	now := time.Now().Format("2006-01")
	scaleData := []Scale{}
	for index,value := range scale {
		if index == 0 && value.DateStr != now {
			nowDate,_ := time.Parse("2006-01",now)
			sca := Scale{
				Count: 0,
				Date: nowDate,
				DateStr: now,
			}
			scaleData = append(scaleData,sca)
		}
		value.Date,_ = time.Parse("2006-01",value.DateStr)
		if len(scaleData) < 12 {
			scaleData = append(scaleData,value)
		}
	}
	return scaleData
}

func GetScalePicList(tmp []Scale) ([]Scale,int,decimal.Decimal,error) {
	now := time.Now().Format("2006-01")+"-01"
	ret := []Scale{}
	next := time.Now().AddDate(0,1,0).Format("2006-01")+"-01"
	count := 0
	var maxNum int
	var maxScale decimal.Decimal
	err := models.DB().Table("p_public_fund_info").
		Select("count(*) as count").
		Where("subscription_date_end >= ?",now).
		Where("subscription_date_end < ? ",next).Count(&count).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("get onsale err :",err)
		return ret,maxNum,maxScale,err
	}
	for _,value := range tmp {
		tmpMonth := value.Date.Format("2006-01")+"-01"
		if tmpMonth == now {
			value.OnSale = count
		}
		if maxNum == 0 {
			maxNum = value.Count
		} else {
			if maxNum - value.Count  <  0 {
				maxNum = value.Count
			}
		}
		if maxScale.IsZero() {
			maxScale = value.TotalScale.Decimal
		} else {
			if maxScale.Sub(value.TotalScale.Decimal).Sign() < 0{
				maxScale = value.TotalScale.Decimal
			}
		}
		value.Month = value.Date.Format("2006年01月")
		if value.TotalScale.Decimal.IsZero() {
			value.Scale = "0"
		}else {
			value.Scale = value.TotalScale.Decimal.String()
		}
		ret = append(ret,value)
	}
	sort.Slice(ret, func(i, j int) bool {
		return ret[i].Date.Before(ret[j].Date)
	})
	return ret,maxNum,maxScale,nil
}

func GetFundNumStastic(tmp []Scale) FundNum {
	now := time.Now().Format("2006-01")+"-01"
	pre := time.Now().AddDate(0,-1,0).Format("2006-01")+"-01"
	ret := FundNum{}
	var (
		total, month,premonth  int
		rate                   decimal.Decimal
	)
	for _,value := range tmp {
		total += value.Count
		tmpMonth := value.Date.Format("2006-01")+"-01"
		if tmpMonth == now {
			month = value.Count
		}
		if tmpMonth == pre {
			premonth = value.Count
		}
	}
	if premonth > 0 {
		rate = decimal.NewFromInt(int64(month)).Div(decimal.NewFromInt(int64(premonth))).Sub(decimal.NewFromFloat(1.00)).Mul(decimal.NewFromInt(100)).Round(2)
	}
	ret.MonthNum = month
	ret.PreMonthNum = premonth
	ret.TotalNum = total
	ret.Rate = rate
	return ret
}

func GetScaleQuator(tmp []Scale) (FundScale,error) {
	now := time.Now().Format("2006-01")+"-01"
	pre := time.Now().AddDate(0,-1,0).Format("2006-01")+"-01"
	ret := FundScale{}
	var (
		total, month,premonth  decimal.Decimal
		rate                   decimal.Decimal
	)
	for _,value := range tmp {
		total = total.Add(value.TotalScale.Decimal)
		tmpMonth := value.Date.Format("2006-01")+"-01"
		if tmpMonth == now {
			month = value.TotalScale.Decimal
		}
		if tmpMonth == pre {
			premonth = value.TotalScale.Decimal
		}
	}
	if !premonth.IsZero() {
		rate = month.Div(premonth).Sub(decimal.NewFromFloat(1.00)).Mul(decimal.NewFromInt(100)).Round(2)
	}
	ret.MonthScale = month
	ret.PreMonthScale = premonth
	ret.TotalScale = total
	ret.Rate = rate
	return ret,nil
}

func PushPublicFundData()  {
	now := time.Now().Format("2006-01")+"-01"
	type Date struct {
		TradeDate     time.Time        `gorm:"column:trade_date"`
	}
	date := Date{}
	err := models.DB().Table("b_trade_date").
		Where("trade_date >= ?",now).Order("trade_date asc").First(&date).Error
	// 获取当前时间
	if err != nil {
		logging.Info("get trade date err",err)
		return
	}
	if date.TradeDate.Format(util.YMD) != time.Now().Format(util.YMD) {
		return
	}
	pre := time.Now().AddDate(0,-1,0).Format("2006-01")+"-01"
	type Scale struct {
		Count        int                `gorm:"column:count"`
		Scale        decimal.Decimal    `gorm:"column:scale"`
	}
	monthData := Scale{}
	// 获取当前一个月的新增基金数量 和规模
	err = models.DB().Table("p_public_fund_info").
		Select("count(*) as count,sum(scale) as scale").
		Where("establish_date < ? ",now).
		Where("establish_date >= ?",pre).Find(&monthData).Error
	if err != nil {
		logging.Info("get present date err ",err)
		return
	}
	// 获取上个月的新成立的基金数量
	present := time.Now().AddDate(0,-2,0).Format("2006-01")+"-01"
	preData := Scale{}
	// 获取当前一个月的新增基金数量 和规模
	err = models.DB().Table("p_public_fund_info").
		Select("count(*) as count,sum(scale) as scale").
		Where("establish_date >= ?",present).
		Where("establish_date < ? ",pre).Find(&preData).Error
	if err != nil {
		logging.Info("get present date err ",err)
		return
	}
	if preData.Count > 0 && preData.Scale.Sign() > 0 {
		fundRate := decimal.NewFromInt(int64(monthData.Count)).Sub(decimal.NewFromInt(int64(preData.Count))).
			Div(decimal.NewFromInt(int64(preData.Count))).Mul(decimal.NewFromInt(100)).Round(2)
		scale := monthData.Scale.Div(decimal.NewFromInt(int64(100000000)))
		scaleRate := monthData.Scale.Sub(preData.Scale).Div(preData.Scale).Mul(decimal.NewFromInt(100)).Round(2)
		month := int(time.Now().AddDate(0,-1,0).Month())
		m := map[string]interface{}{
			"type":    "public_fund",
			"content": fmt.Sprintf("%v月新增%v家基金，环比%v%%，本月新增规模%v亿，环比%v%%",month,monthData.Count,fundRate,scale,scaleRate),
			"push_time":time.Now(),
		}
		push(m, message.Message_Public_Fund)
	}

}

func GetLatestTradeDate() (time.Time,error) {
	type Date struct {
		TradeDate     time.Time        `gorm:"column:trade_date"`
	}
	date := Date{}
	err := models.DB().Table("b_trade_date").
		Where("trade_date <= ?",time.Now().Format(util.YMD)).Order("trade_date desc").First(&date).Error
	// 获取当前时间
	if err != nil {
		logging.Info("get trade date err",err)
		return date.TradeDate,err
	}
	return date.TradeDate,nil
}

func push(m map[string]interface{}, msgType message.MessageType) {
	ws := websocket.GetInstance()
	bt, _ := json.Marshal(m)
	msg := message.ParseMsg(msgType, "system", "", string(bt), true)
	ws.SysSend(msg)
}