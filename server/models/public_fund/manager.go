package public_fund

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

// 基金
type FundManagerReq struct {
	SortKey   string    `json:"sort_key" form:"sort_key"`
	Direction bool      `json:"direction" form:"direction"`
	DateTime  time.Time `json:"report_date" form:"report_date" time_format:"2006-01-02"`
	ManagerID int       `json:"manager_id" form:"manager_id" binding:"required,gte=1"`
	PageSize  int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum   int       `json:"page_num" form:"page_num" binding:"required,gte=1"`
	IsNew     string    `json:"is_new" form:"is_new"`
}

// 基金经理姓名搜索
type FundManagerSearchReq struct {
	Name string `json:"name" form:"manager_name"`
}

// 获取头部基金规模和报告期
type FundManagerHeadResp struct {
	ManagerId   int             `json:"manager_id" gorm:"column:manager_id"`
	FundNum     int             `json:"fund_num" gorm:"column:fund_num"`
	ManagerName string          `json:"manager_name" gorm:"column:name"`
	ReportScale decimal.Decimal `json:"report_scale" gorm:"column:report_scale"`
	Rate        decimal.Decimal `json:"rate" gorm:"column:rate"`
}
type FundManagerStock struct {
	ReportScale decimal.Decimal `json:"report_scale" gorm:"column:report_scale"`
	FundNum     int             `json:"fund_num" gorm:"column:fund_num"`
}
type FundManagerResp struct {
	Code                        string              `json:"code" gorm:"column:stock_code"`
	Name                        string              `json:"name" gorm:"column:stock_name"`
	IsNew                       bool                `json:"is_new"`                            // 是否是新股
	FundNums                    int                 `json:"fund_nums" gorm:"column:fund_nums"` // 当前报告期的基金数量
	HeldNum                     int                 `json:"held_num" gorm:"column:held_num"`   // 当前报告期总持股
	IncreaseHeldNum             int                 `json:"increase_held_num"`                 // 新增持股数量
	PreQuarterHeldNum           int                 `json:"pre_quarter_held_num"`              // 上一个报告期持股数量
	NetworthPropotion           decimal.Decimal     `json:"avg_networth_propotion"`            // 当前报告期总持股
	IncreaseNetworthPropotion   decimal.Decimal     `json:"increase_networth_propotion"`       // 新增持股数量
	HeldNumRate                 decimal.NullDecimal `json:"held_num_rate"`                     // 持股数量环比
	TotalMarketValue            decimal.NullDecimal `json:"total_market_value"`                // 总市值
	TotalMarketValueIncrease    decimal.NullDecimal `json:"total_market_value_increase"`       // 当前总市值
	StockScale                  decimal.Decimal     `json:"stock_scale"`
	ReportPeriod                time.Time           `json:"report_date" gorm:"column:report_date"` // 报告期
	NowQuarterHeldNum           int                 `json:"now_quarter_held_num"`                  // 当前报告期持股数量
	NowQuarterFundNums          int                 `json:"now_quarter_fund_nums"`                 // 当前报告期基金数量
	Tmp                         string              `json:"tmp"`
	FundIds                     []string            `json:"fund_ids"`
	PreQuarterNetworthPropotion decimal.Decimal     `json:"pre_quarter_networth_propotion"` // 上一个报告期持股数量
	ManagerId                   int                 `json:"manager_id"`
}
type ManagerList struct {
	ManagerId      int             `json:"manager_id" gorm:"column:id"`                     // 基金经理id
	Name           string          `json:"name" gorm:"column:name"`                         // 基金经理名称
	FundName       string          `json:"fund_name" gorm:"column:fund_name"`               // 基金名称
	ManagerFundNum int             `json:"manager_fund_num" gorm:"column:manager_fund_num"` // 基金数量
	Scale          decimal.Decimal `json:"scale" gorm:"column:scale"`
}

func GetStockManager(req *FundManagerReq) ([]FundManagerResp, int, error) {
	manager := []FundManagerResp{}
	dbs := models.DB().Table("p_public_fund_position").
		Select("stock_code,stock_name,report_date,group_concat(distinct p_public_fund_position.fund_code) as tmp").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_position.fund_code").
		Where("p_public_fund_position.networth_propotion > 0.05")
	if req.ManagerID > 0 {
		dbs = dbs.Where("p_public_fund_relationship.manager_id = ? ", req.ManagerID)
	}
	if !req.DateTime.IsZero() {
		dbs = dbs.Where("p_public_fund_position.report_date = ? ", req.DateTime)
	}
	var err error
	cnt := 0
	err = dbs.Group("stock_code").Find(&manager).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return manager, cnt, nil
	}
	// 拼接数据
	wg := sync.WaitGroup{}
	ret := []FundManagerResp{}
	wg.Add(len(manager))
	for i, _ := range manager {
		go func(idx int) {
			v := &manager[idx]
			if v.Tmp != "" {
				v.FundIds = strings.Split(v.Tmp, ",")
			}
			v.ManagerId = req.ManagerID
			err = GetManagerReportPeriodData(v)
			if err != nil {
				logging.Info("get total year err:", err)
				return
			}
			v.IncreaseHeldNum = v.NowQuarterHeldNum - v.PreQuarterHeldNum
			if v.PreQuarterHeldNum > 0 {
				v.HeldNumRate.Valid = true
				v.HeldNumRate.Decimal = decimal.NewFromInt(int64(v.IncreaseHeldNum)).Div(decimal.NewFromInt(int64(v.PreQuarterHeldNum))).Mul(decimal.NewFromFloat(100)).Round(2)
			}
			v.IncreaseNetworthPropotion = v.NetworthPropotion.Sub(v.PreQuarterNetworthPropotion)
			err = GetPrice(v)
			if err != nil {
				logging.Info("get total_market_value err:", err)
				return
			}
			ret = append(ret, *v)
			wg.Done()
		}(i)
	}
	wg.Wait()
	if req.IsNew != "all" && req.IsNew != "" {
		isNew := false
		if req.IsNew == "1" {
			isNew = true
		}
		newData := []FundManagerResp{}
		for _, value := range ret {
			if value.IsNew == isNew {
				newData = append(newData, value)
			}
		}
		ret = newData
	}
	if req.SortKey != "" {
		sort.Slice(ret, func(i, j int) bool {
			v := reflect.ValueOf(ret[i])
			u := reflect.ValueOf(ret[j])
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(ret[i]).Field(k).Tag.Get("json") == req.SortKey {
					return req.Direction == compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	}
	lists := []FundManagerResp{}
	if req.PageSize > 0 && req.PageNum > 0 {
		start := req.PageSize * (req.PageNum - 1)
		end := req.PageSize * (req.PageNum)
		if req.PageNum*req.PageSize > len(ret) {
			end = len(ret)
		}
		lists = ret[start:end]
	}
	return lists, len(ret), err
}

func GetManagerReportPeriodData(ret *FundManagerResp) error {
	// 获取当前报告期的
	reportPeriod := ret.ReportPeriod.Format(util.YMD)
	data, err := GetManagerPeriodData(reportPeriod, ret.Code, ret.FundIds, ret.ManagerId)
	if err != nil {
		return err
	}
	// 获取上一个报告期数据
	lastReportPeriod, _ := GetReportPeriod(ret.ReportPeriod, -1)
	lastReportPeriodData, err := GetManagerPeriodData(lastReportPeriod, ret.Code, ret.FundIds, ret.ManagerId)
	if err != nil {
		return err
	}
	if lastReportPeriodData.FundNums > 0 {
		ret.IsNew = false
	} else {
		ret.IsNew = true
	}
	ret.PreQuarterHeldNum = lastReportPeriodData.HeldNum
	ret.PreQuarterNetworthPropotion = lastReportPeriodData.NetworthPropotion
	ret.FundNums = data.FundNums
	ret.HeldNum = data.HeldNum
	ret.NetworthPropotion = data.NetworthPropotion
	ret.NowQuarterFundNums = data.FundNums
	ret.NowQuarterHeldNum = data.HeldNum
	ret.StockScale, err = GetFundScaleByFundId(ret.FundIds)
	if err != nil {
		return err
	}
	return nil
}

func GetManagerPeriodData(date, code string, fundIds []string, managerId int) (FundPositionResp, error) {
	ret := FundPositionResp{}
	err := models.DB().Table("p_public_fund_position").
		Select("stock_code,stock_name,count(fund_code) as fund_nums,sum(held_num) as held_num,avg(networth_propotion) as networth_propotion ").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_position.fund_code").
		Where("p_public_fund_position.networth_propotion > 0.05").
		Where("p_public_fund_position.fund_code in (?) ", fundIds).
		Where("p_public_fund_relationship.manager_id = ? ", managerId).
		Where("stock_code = ? ", code).
		Where("report_date = ? ", date).
		First(&ret).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return ret, err
	}
	if err == gorm.ErrRecordNotFound {
		return FundPositionResp{}, nil
	}
	return ret, nil
}
func GetPrice(ret *FundManagerResp) error {
	var (
		price decimal.NullDecimal
		err   error
	)
	price, err = getCurrentPrice(ret.Code)
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("get current price err ", err)
		return err
	}
	if err == gorm.ErrRecordNotFound {
		return nil
	}
	if !price.Decimal.IsZero() {
		ret.TotalMarketValue.Decimal = price.Decimal.Mul(decimal.NewFromInt(int64(ret.HeldNum)))
		ret.TotalMarketValue.Valid = true
		ret.TotalMarketValueIncrease.Decimal = price.Decimal.Mul(decimal.NewFromInt(int64(ret.IncreaseHeldNum)))
		ret.TotalMarketValueIncrease.Valid = true
	}
	return nil
}

// 获取当前股票的基金经理的本月买基金的总规模
func GetFundScaleByFundId(fundIds []string) (decimal.Decimal, error) {
	var retScale decimal.Decimal
	if len(fundIds) < 1 {
		logging.Info("get manager_ids err ", fundIds)
		return decimal.Decimal{}, nil
	}
	ret := FundInfo{}
	err := models.DB().Table("p_public_fund_info").
		Select("sum(scale) as scale").
		Where("scale_date > 0 ").Where("p_public_fund_info.code in (?) ", fundIds).First(&ret).Error
	if err != nil {
		return retScale, err
	}
	return ret.Scale, nil
}

// 获取当前报告期和上一个报告期规模
func GetManagerStockScale(req *FundManagerReq) (FundManagerHeadResp, error) {
	stock := FundManagerStock{}
	// 根据manager_id获取基金经理的名字
	managerName := FundManagerHeadResp{}
	err := models.DB().Table("p_public_fund_manager").Where("id = ? ", req.ManagerID).First(&managerName).Error
	if err != nil {
		return FundManagerHeadResp{}, err
	}
	// 获取当前报告期的基金数量
	cnt := 0
	lastReportPeriod, _ := GetReportPeriod(req.DateTime, -1)
	err = models.DB().Table("p_public_fund_info").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_info.code ").
		Where("p_public_fund_relationship.manager_id = ? ", req.ManagerID).
		Where("establish_date < ? ", time.Now().Format(util.YMD)).Count(&cnt).Error
	if err != nil {
		return FundManagerHeadResp{}, err
	}
	ret := FundManagerHeadResp{}
	ret.FundNum = cnt
	ret.ManagerName = managerName.ManagerName
	ret.ManagerId = req.ManagerID
	// 获取当前报告期的规模
	err = models.DB().Table("p_public_fund_info").
		Select("sum(scale) as report_scale,count(distinct p_public_fund_info.code) as fund_num").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_info.code").
		Where("p_public_fund_relationship.manager_id = ? ", req.ManagerID).
		Where("p_public_fund_info.scale_date = ? ", req.DateTime.Format(util.YMD)).First(&stock).Error
	if err != nil {
		return FundManagerHeadResp{}, err
	}
	ret.ReportScale = stock.ReportScale
	// 获取上一个报告期的规模
	scale := FundManagerHeadResp{}
	err = models.DB().Table("p_public_fund_info").Select("sum(p_public_fund_scale.scale) as report_scale").
		Joins("left join p_public_fund_scale on p_public_fund_scale.code = p_public_fund_info.code").
		Joins("left join p_public_fund_relationship on p_public_fund_relationship.code = p_public_fund_info.code").
		Where("p_public_fund_relationship.manager_id = ? ", req.ManagerID).
		Where("p_public_fund_scale.scale_date = ? ", lastReportPeriod).
		Where("p_public_fund_info.scale_date = ? ", req.DateTime.Format(util.YMD)).First(&scale).Error
	if err != nil {
		return FundManagerHeadResp{}, err
	}
	if !scale.ReportScale.IsZero() {
		ret.Rate = stock.ReportScale.Div(scale.ReportScale).Sub(decimal.NewFromInt(1)).Mul(decimal.NewFromInt(100)).Round(2)
	}
	return ret, nil
}

// 获取基金经理人的基金排行
func GetManagerRank(req *FundManagerSearchReq) ([]ManagerList, error) {
	manager := []ManagerList{}
	err := models.DB().Table("p_public_fund_manager").
		Select("id,name").
		Where("p_public_fund_manager.name like ?", "%"+req.Name+"%").Find(&manager).Error
	if err != nil {
		return nil, err
	}
	for index, value := range manager {
		var count int
		err = models.DB().Table("p_public_fund_manager").Where("name = ? ", value.Name).Count(&count).Error
		if err != nil {
			continue
		}
		if count > 1 {
			// 获取对应基金的数量和规模最大的基金
			sigleManager := ManagerList{}
			err = models.DB().Table("p_public_fund_info").
				Select("count(*) as manager_fund_num,name as fund_name").
				Joins("left join  p_public_fund_relationship on p_public_fund_info.code = p_public_fund_relationship.code ").
				Where("p_public_fund_relationship.manager_id = ? ", value.ManagerId).
				Where("scale_date > 0").Order("scale desc ").First(&sigleManager).Error
			if err != nil {
				continue
			}
			manager[index].ManagerFundNum = sigleManager.ManagerFundNum
			manager[index].FundName = sigleManager.FundName
		}
	}
	return manager, nil
}

// 获取基金经理人的基金排行
func GetStarManagerList() ([]ManagerList, error) {
	manager := []ManagerList{}
	err := models.DB().Table("p_public_fund_manager").
		Select("id,name").
		Where("p_public_fund_manager.is_star = 1").Find(&manager).Error
	if err != nil {
		return nil, err
	}
	for index, value := range manager {
		sigleManager := ManagerList{}
		err = models.DB().Table("p_public_fund_info").
			Select("count(*) as manager_fund_num,name as fund_name,scale").
			Joins("left join  p_public_fund_relationship on p_public_fund_info.code = p_public_fund_relationship.code ").
			Where("p_public_fund_relationship.manager_id = ? ", value.ManagerId).
			Where("scale_date > 0").Order("scale desc ").First(&sigleManager).Error
		if err != nil {
			continue
		}
		manager[index].ManagerFundNum = sigleManager.ManagerFundNum
		manager[index].FundName = sigleManager.FundName
		manager[index].Scale = sigleManager.Scale
	}
	sort.Slice(manager, func(i, j int) bool {
		return manager[i].Scale.Sub(manager[j].Scale).Sign() > 0
	})
	return manager, nil
}

// to do
func compare(value1, value2 reflect.Value) bool {
	switch value1.Kind() {
	case reflect.Ptr:
		//空指针直接返回
		if value1.IsNil() || value2.IsNil() {
			return value1.IsNil()
		}
		return compare(value1.Elem(), value2.Elem())
	case reflect.Float64, reflect.Float32:
		return value1.Float() < value2.Float()
	case reflect.Int, reflect.Int64, reflect.Int32, reflect.Int16, reflect.Int8:
		return value1.Int() < value2.Int()
	case reflect.Uint, reflect.Uint64, reflect.Uint32, reflect.Uint16, reflect.Uint8:
		return value1.Uint() < value2.Uint()
	case reflect.String:
		v1, _ := strconv.ParseFloat(value1.String(), 64)
		v2, _ := strconv.ParseFloat(value2.String(), 64)
		return v1 < v2
	case reflect.Struct:
		switch value1.Type().Name() {
		case "NullDecimal":
			v1, _ := value1.Interface().(decimal.NullDecimal)
			v2, _ := value2.Interface().(decimal.NullDecimal)
			return v1.Decimal.Sub(v2.Decimal).Sign() < 0
		case "Decimal":
			v1, _ := value1.Interface().(decimal.Decimal)
			v2, _ := value2.Interface().(decimal.Decimal)
			return v1.Cmp(v2) < 0
		case "Time":
			v1, _ := value1.Interface().(time.Time)
			v2, _ := value2.Interface().(time.Time)
			return v1.Before(v2)
		default:
			return false
		}
	default:
		logging.Error("sort.compare() known type: ", value1.Kind().String())
		return false
	}
}
