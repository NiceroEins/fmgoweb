package home

import (
	"datacenter/models"
	"datacenter/models/template"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"strings"
	"time"
)

type ImgDTO struct {
	Url  string `json:"url"`
	Type string `json:"type"`
}

type AnnouncementEventQO struct {
	ID     int `json:"id" form:"id" `          //公告ID
	UserID int `json:"user_id" form:"user_id"` // 用户ID
	//ObjectType  string `json:"object_type" form:"object_type"`   // 对象类型
	EventStatus string `json:"event_status" form:"event_status"` // 事件状态
	Comment     *bool  `json:"comment" form:"comment"`
	//Direction   bool   `json:"direction" form:"direction"`
	StartTime string `json:"start_time" form:"start_time" binding:"required"`
	EndTime   string `json:"end_time" form:"end_time" binding:"required"`

	Code     string `json:"code" form:"code"`
	Type     string `json:"type" form:"type"`
	Keyword  string `json:"keyword" form:"keyword"`
	PageSize int    `json:"page_size" form:"page_size" binding:"gt=0"`
	PageNum  int    `json:"page_num" form:"page_num" binding:"gt=0"`
}

type CninfoNoticeDTO struct {
	ID          int        `gorm:"primary_key" json:"id,omitempty"`
	CreatedAt   time.Time  `json:"-"`
	UpdatedAt   time.Time  `json:"-"`
	Code        string     `json:"code"`
	Name        string     `json:"name"`
	Type        string     `json:"type"`
	Title       string     `json:"title"`
	SourceLink  string     `json:"source_link"`
	PublishDate *time.Time `json:"publish_date"`
	CatchTime   *time.Time `json:"catch_time"`
	SpiderTime  *time.Time `json:"spider_time"`
}

func (dto CninfoNoticeDTO) TableName() string {
	return "u_cninfo_notice"
}

type StockUserEvents struct {
	ID          int
	CreatedAt   time.Time `json:"-"`
	UpdatedAt   time.Time `json:"-"`
	UserID      int       // 用户ID
	ObjectID    int       // 事件ID
	EventStatus int       // 事件状态
	EventStr    string    // 点评内容
	ObjectType  string    // 对象类型
	EventType   string    // 事件类型
	PictureLink string    // 图片链接
	Description string    // 图片链接
}

type AnnouncementEventDTO struct {
	CninfoNoticeDTO
	StockUserEvents
	Head int `json:"head"`
}

type AnnouncementEventVO struct {
	ID         int    `json:"id"`
	Code       string `json:"code"`
	Name       string `json:"name"`
	Type       string `json:"type"`
	Title      string `json:"title"`
	SourceLink string `json:"source_link"`
	CreatedAt  string `json:"created_at"`
	UserId     int    `json:"user_id"`
	ObjectId   int    `json:"object_id"`
	EventStr   string `json:"event_str"` // 点评内容
	//ObjectType  string `json:"object_type"`  // 对象类型
	EventStatus int      `json:"event_status"` // 事件状态
	PictureLink []ImgDTO `json:"picture_link"` //图片链接
	Head        int      `json:"head"`
}
type AnnouncemenTypeList struct {
	Type string `json:"type" gorm:"type"`
}

func (dto AnnouncementEventDTO) DTO2VO() crud_service.VO {
	vo := AnnouncementEventVO{
		ID:         dto.CninfoNoticeDTO.ID,
		Code:       dto.Code,
		Name:       dto.Name,
		Type:       dto.Type,
		Title:      dto.Title,
		SourceLink: dto.SourceLink,
		CreatedAt:  util.TimePtr2String(&dto.CninfoNoticeDTO.CreatedAt, util.YMDHMS),
		UserId:     dto.UserID,
		ObjectId:   dto.ObjectID,
		EventStr:   dto.EventStr,
		//ObjectType:  dto.ObjectType,
		EventStatus: dto.EventStatus,
		Head:        dto.Head,
	}
	vo.PictureLink = []ImgDTO{}
	_ = json.Unmarshal([]byte(dto.PictureLink), &vo.PictureLink)
	return vo
}

func GetMyAnnouncementList(qo *AnnouncementEventQO) ([]crud_service.DTO, int, error) {
	dtos := make([]AnnouncementEventDTO, 0)
	res := make([]crud_service.DTO, 0)
	cnt := 0
	amountLimit := 300000 //3亿
	tradeDayList := make([]time.Time, 0)
	bandCodeList := make([]string, 0)
	bandZszList := make([]string, 0)
	err := models.DB().Table("b_trade_date").
		Where("trade_date<=?", qo.EndTime).
		Order("trade_date desc").Limit(11).
		Pluck("trade_date", &tradeDayList).Error
	if err != nil {
		logging.Error("GetMyAnnouncementList 获取前10交易日失败", err.Error())
	}
	if len(tradeDayList) > 1 {
		start := 0
		end := len(tradeDayList)
		if tradeDayList[0].Before(time.Now()) {
			end = end - 1
		} else {
			start = start + 1
		}
		err = models.DB().Table("p_stock_tick").
			Where("stock_code is not null").
			Where("trade_date in (?)", tradeDayList[start:end]).
			Group("stock_code").
			Having("avg(amount)<?", amountLimit).
			Pluck("stock_code", &bandCodeList).Error
		if err != nil {
			logging.Error("GetMyAnnouncementList 获取前10日日均成交额小于6000万股票失败", err.Error())
		}
		err = models.DB().Raw("select stock_code FROM p_stock_tick left join p_stock_base_info on stock_code=code where trade_date='"+tradeDayList[0].Format(util.YMD)+"' and (zgb*close)<5000000000").Pluck("stock_code", &bandZszList).Error
		bandCodeList = append(bandCodeList, bandZszList...)
	}
	aSql := "SELECT N.id,N.code,N.name,N.title,N.source_link,N.created_at,N.type, S.object_id,S.event_status,S.event_str,S.user_id,S.picture_link,I.head " +
		" FROM (SELECT GROUP_CONCAT(distinct type separator ',') as `type`,id,code,name,title,source_link,publish_time,catch_time,spider_time,created_at,deleted_at FROM u_cninfo_notice" +
		"  WHERE `u_cninfo_notice`.`deleted_at` IS NULL" + template.CheckTitleSql() + template.BandCodeSql(bandCodeList) +
		"  group by source_link, u_cninfo_notice.code) N"
	iSql := " SELECT P.id,P.code,P.name,P.title,P.source_link,P.created_at,'调研纪要' type, S.object_id,S.event_status,S.event_str,S.user_id,S.picture_link,I.head" +
		" FROM `p_investigation` P" +
		" LEFT JOIN (SELECT * FROM u_user_event WHERE object_type = 'investigation' AND user_id=%d) S   ON P.id = S.object_id" +
		" LEFT JOIN u_stock_industry I ON I.code = P.code" +
		" LEFT JOIN u_second_industry_user IU ON IU.second_industry = I.second_industry" +
		" WHERE (IU.deleted_at IS NULL)  AND (IU.user_id = %d)  "
	//" ORDER BY created_at DESC"
	if qo.UserID > 0 {
		aSql = aSql + fmt.Sprintf(
			"  LEFT JOIN (SELECT * FROM u_user_event WHERE object_type = 'announcement' and user_id = %d) S   ON N.id = S.object_id"+
				"  LEFT JOIN u_stock_industry I ON I.code = N.code"+
				"  LEFT JOIN u_second_industry_user IU ON IU.second_industry = I.second_industry"+
				" WHERE (IU.deleted_at IS NULL)  AND (IU.user_id = %d) ", qo.UserID, qo.UserID)
		iSql = fmt.Sprintf(iSql, qo.UserID, qo.UserID)
	} else {
		return nil, 0, errors.New("非法用户ID")
	}
	if qo.EventStatus != "" {
		if qo.EventStatus == "1" {
			aSql = aSql + fmt.Sprintf(" and event_status=%s", qo.EventStatus)
			iSql = fmt.Sprintf(iSql+" and event_status=%s", qo.EventStatus)
		} else {
			aSql = aSql + fmt.Sprintf(" and (event_status=%s or event_status IS NULL)", qo.EventStatus)
			iSql = fmt.Sprintf(iSql+" and (event_status=%s or event_status IS NULL)", qo.EventStatus)
		}
	}
	if qo.StartTime != "" {
		aSql = aSql + fmt.Sprintf(" and N.created_at>='%s'", qo.StartTime+" 00:00:00")
		iSql = fmt.Sprintf(iSql+" and P.created_at>='%s'", qo.StartTime+" 00:00:00")
	}
	if qo.EndTime != "" {
		aSql = aSql + fmt.Sprintf(" and N.created_at<='%s'", qo.EndTime+" 23:59:59")
		iSql = fmt.Sprintf(iSql+" and P.created_at<='%s'", qo.EndTime+" 23:59:59")
	}
	if qo.Comment != nil {
		if *qo.Comment {
			aSql = aSql + " and S.event_str!=''"
			iSql = iSql + " and S.event_str!=''"
		} else {
			aSql = aSql + " and (S.event_str='' OR S.event_str is NULL)"
			iSql = iSql + " and (S.event_str='' OR S.event_str is NULL)"
		}
	}

	if qo.Keyword != "" {
		aSql = aSql + fmt.Sprintf(" and N.title like '%s'", "%"+qo.Keyword+"%")
		iSql = fmt.Sprintf(iSql+" and P.title like '%s'", "%"+qo.Keyword+"%")
	}
	if qo.Code != "" {
		aSql = aSql + fmt.Sprintf(" and (N.name like '%s' or N.code like '%s')", "%"+qo.Code+"%", "%"+qo.Code+"%")
		iSql = fmt.Sprintf(iSql+" and (P.name like '%s' or P.code like '%s')", "%"+qo.Code+"%", "%"+qo.Code+"%")
	}
	subSql := aSql + " UNION ALL " + iSql

	if qo.Type != "全部" && qo.Type != "" {
		subSql = "SELECT * FROM (" + subSql + ") SS where (" + "type like '" + "%" + strings.ReplaceAll(qo.Type, ",", "%' or type like '%") + "%')"
	}
	err = models.DB().Raw("SELECT count(*) FROM (" + subSql + ") counttable").Count(&cnt).Error
	if err != nil {
		logging.Error("GetMyAnnouncementList() count err:" + err.Error())
		return nil, 0, err
	}
	subSql = subSql + fmt.Sprintf(" order by created_at desc,code asc limit %d offset %d ", qo.PageSize, (qo.PageNum-1)*qo.PageSize)
	err = models.DB().Raw(subSql).Find(&dtos).Error
	if err != nil {
		logging.Error("GetMyAnnouncementList() list err:" + err.Error())
		return nil, 0, err
	}
	for _, dto := range dtos {
		res = append(res, dto)
	}
	return res, cnt, nil
}

func GetMyAnnouncementCount(qo *AnnouncementEventQO) (int, int, error) {
	type countS struct {
		Deal int `json:"deal"`
		Wait int `json:"wait"`
	}
	cnt := countS{}
	aSql := "SELECT N.id,N.code,N.name,N.title,N.source_link,N.created_at,N.type, S.object_id,S.event_status,S.event_str,S.user_id,S.picture_link " +
		" FROM (SELECT GROUP_CONCAT(distinct type separator ',') as `type`,id,code,name,title,source_link,publish_time,catch_time,spider_time,created_at,deleted_at FROM u_cninfo_notice" +
		"  WHERE `u_cninfo_notice`.`deleted_at` IS NULL" +
		"  group by source_link, u_cninfo_notice.code) N" +
		"  LEFT JOIN (SELECT * FROM u_user_event WHERE object_type = 'announcement' and user_id = %d) S   ON N.id = S.object_id" +
		"  LEFT JOIN u_stock_industry I ON I.code = N.code" +
		"  LEFT JOIN u_second_industry_user IU ON IU.second_industry = I.second_industry" +
		" WHERE (IU.deleted_at IS NULL)  AND (IU.user_id = %d) "
	iSql := " SELECT P.id,P.code,P.name,P.title,P.source_link,P.created_at,'调研纪要' type, S.object_id,S.event_status,S.event_str,S.user_id,S.picture_link" +
		" FROM `p_investigation` P" +
		" LEFT JOIN (SELECT * FROM u_user_event WHERE object_type = 'investigation' AND user_id=%d) S   ON P.id = S.object_id" +
		" LEFT JOIN u_stock_industry I ON I.code = P.code" +
		" LEFT JOIN u_second_industry_user IU ON IU.second_industry = I.second_industry" +
		" WHERE (IU.deleted_at IS NULL)  AND (IU.user_id = %d)  "
	//" ORDER BY created_at DESC"
	if qo.UserID > 0 {
		aSql = fmt.Sprintf(aSql, qo.UserID, qo.UserID)
		iSql = fmt.Sprintf(iSql, qo.UserID, qo.UserID)
	} else {
		return 0, 0, errors.New("非法用户ID")
	}
	if qo.EventStatus != "" {
		if qo.EventStatus == "1" {
			aSql = fmt.Sprintf(aSql+" and event_status=%s", qo.EventStatus)
			iSql = fmt.Sprintf(iSql+" and event_status=%s", qo.EventStatus)
		} else {
			aSql = fmt.Sprintf(aSql+" and (event_status=%s or event_status IS NULL)", qo.EventStatus)
			iSql = fmt.Sprintf(iSql+" and (event_status=%s or event_status IS NULL)", qo.EventStatus)
		}
	}
	if qo.StartTime != "" {
		aSql = fmt.Sprintf(aSql+" and N.created_at>='%s'", qo.StartTime+" 00:00:00")
		iSql = fmt.Sprintf(iSql+" and P.created_at>='%s'", qo.StartTime+" 00:00:00")
	}
	if qo.EndTime != "" {
		aSql = fmt.Sprintf(aSql+" and N.created_at<='%s'", qo.EndTime+" 23:59:59")
		iSql = fmt.Sprintf(iSql+" and P.created_at<='%s'", qo.EndTime+" 23:59:59")
	}
	if qo.Comment != nil {
		if *qo.Comment {
			aSql = aSql + " and S.event_str!=''"
			iSql = iSql + " and S.event_str!=''"
		} else {
			aSql = aSql + " and (S.event_str='' OR S.event_str is NULL)"
			iSql = iSql + " and (S.event_str='' OR S.event_str is NULL)"
		}
	}

	if qo.Keyword != "" {
		aSql = fmt.Sprintf(aSql+" and N.title like '%s'", "%"+qo.Keyword+"%")
		iSql = fmt.Sprintf(iSql+" and P.title like '%s'", "%"+qo.Keyword+"%")
	}
	if qo.Code != "" {
		aSql = fmt.Sprintf(aSql+" and (N.name like '%s' or N.code like '%s')", "%"+qo.Code+"%", "%"+qo.Code+"%")
		iSql = fmt.Sprintf(iSql+" and (P.name like '%s' or P.code like '%s')", "%"+qo.Code+"%", "%"+qo.Code+"%")
	}
	subSql := aSql + " UNION ALL " + iSql

	if qo.Type != "全部" && qo.Type != "" {
		subSql = "SELECT * FROM (" + subSql + ") SS where (" + "type like '" + "%" + strings.ReplaceAll(qo.Type, ",", "%' or type like '%") + "%')"
	}
	err := models.DB().Raw("SELECT count(event_status=1) deal,count(*)-count(event_status=1) wait FROM (" + subSql + ") counttable").Find(&cnt).Error
	if err != nil {
		logging.Error("GetMyAnnouncementCount() count err:" + err.Error())
		return 0, 0, err
	}
	return cnt.Deal, cnt.Wait, nil
}

func GetAnnouncementType() ([]AnnouncemenTypeList, error) {
	var (
		err       error
		typeNames []AnnouncemenTypeList
	)
	err = models.DB().Table("u_cninfo_notice").Group("type").Find(&typeNames).Error
	if err != nil {
		return nil, err
	}
	return typeNames, nil
}
