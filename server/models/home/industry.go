package home

import (
	"bytes"
	"datacenter/models"
	"datacenter/models/message"
	"datacenter/models/template"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/pkg/websocket"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	turnoverKey     = "stock:amount"
	currentPriceKey = "stock:tick"
	lastCloseKey    = "stock:last_close"
	zgbKey          = "stock:zgb"
	ltgKey          = "stock:ltg"
	blockKey        = "stock:block"
	nameKey         = "stock:name"
	auctionBpKey    = "stock:auction:bp1"
	auctionBvKey    = "stock:auction:bv1"
)

type IndustryReq struct {
	PageSize  int    `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum   int    `json:"page_num" form:"page_num" binding:"required,gte=1"`
	UserId    int    `json:"user_id"`
	SordKey   string `json:"sort_key" form:"sort_key"`
	Direction bool   `json:"direction" form:"direction"`
}

type IndustrySingle struct {
	ID             int                 `json:"id" gorm:"column:id"`
	BlockNum       int                 `json:"block_num" gorm:"column:block_num"`
	IncreaseNum    int                 `json:"increase_num" gorm:"column:increase_num"`
	DecreaseNum    int                 `json:"decrease_num" gorm:"column:decrease_num"`
	SecondIndustry string              `json:"second_industry" gorm:"column:second_industry"`
	RecordTime     string              `json:"time" gorm:"column:time"`
	LeadStock      string              `json:"lead_stock" gorm:"column:lead_stock"`
	UpDownRange    decimal.NullDecimal `json:"up_down_range" gorm:"column:up_down_range"`
	Turnover       decimal.NullDecimal `json:"turnover" gorm:"column:turnover"`
	TotalTurnover  decimal.NullDecimal `json:"total_turnover" gorm:"column:total_turnover"`
	AmountRate     decimal.NullDecimal `json:"amount_rate" gorm:"column:amount_rate"`
	CreatedAt      time.Time           `json:"created_at" gorm:"column:created_at"`
	MinuteTime     string              `json:"minute_time" gorm:"column:minute_time"`
}
type Industry struct {
	IndustrySingle
	UserEvent
}
type UserEvent struct {
	EventsId       int               `json:"events_id"`
	EventStr       string            `json:"event_str"`
	PictureLinkArr []template.ImgDTO `json:"event_pic"`
	PictureLink    string            `json:"-" gorm:"column:picture_link"` // 图片链接
}
type IndustryListReq struct {
	IndustryName string `json:"industry_name" form:"industry_name"`
	IndustryReq
}
type IndustryPicReq struct {
	IndustryName string `json:"industry_name" form:"industry_name" binding:"required"`
}
type IndustryPic struct {
	SecondIndustry string          `json:"-" gorm:"column:second_industry"`
	Turnover       decimal.Decimal `json:"turnover" gorm:"column:turnover"`
	TotalTurnover  decimal.Decimal `json:"-" gorm:"column:total_turnover"`
	UpDownRange    decimal.Decimal `json:"up_down_range" gorm:"column:up_down_range"`
	Date           time.Time       `json:"-" gorm:"column:time"`
	UpDown         decimal.Decimal `json:"up_down"`
	Price          decimal.Decimal `json:"-"`
	RDate          string          `json:"time"`
	Type           string          `json:"type"`
	PreUpDownRange decimal.Decimal `json:"pre_up_down_range"`
	Close          decimal.Decimal `json:"close"`
	NowUpDownRange decimal.Decimal `json:"now_up_down_range"`
	NowUpDown      decimal.Decimal `json:"now_up_down"`
}
type IndustryUserEventReq struct {
	IndustryPicReq
	UserId   int `json:"user_id"`
	PageSize int `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum  int `json:"page_num" form:"page_num" binding:"required,gte=1"`
}
type IndustryStockReq struct {
	IndustryPicReq
	IndustryReq
}

func (IndustrySingle) TableName() string {
	return "u_industry_monitor"
}

type IndustryStock struct {
	Code                   string              `gorm:"column:code" json:"code"`                                         // 股票名称
	Name                   string              `gorm:"column:name" json:"name"`                                         // 股票代码
	SecondIndustry         string              `gorm:"column:second_industry" json:"column:second_industry"`            // 二级行业
	CurrentPrice           decimal.NullDecimal `gorm:"column:current_price" json:"current_price"`                       // 现价
	UpDownRange            decimal.NullDecimal `gorm:"column:up_down_range" json:"up_down_range"`                       // 涨跌幅
	UpDown                 decimal.NullDecimal `gorm:"column:up_down" json:"up_down"`                                   // 涨跌幅
	Turnover               decimal.NullDecimal `gorm:"column:turnover" json:"turnover"`                                 // 成交额
	CirculationMarketValue decimal.NullDecimal `gorm:"column:circulation_market_value" json:"circulation_market_value"` // 总市值
	TotalMarketValue       decimal.NullDecimal `gorm:"column:total_market_value" json:"total_market_value"`             // 流通市值
	TurnoverRate           decimal.NullDecimal `gorm:"column:turnover_rate" json:"turnover_rate"`                       // 换手率
	IsBlock                int                 `gorm:"column:is_block" json:"is_block"`                                 // 是否涨停
	BlockTime              time.Time
}

func (IndustryStock) TableName() string {
	return "u_industry_stock_minute"
}

// 记录股票代码 二级行业 股票名称
type IndustryNameCode struct {
	SecondIndustry string `json:"second_industry" gorm:"cloumn:second_industry"`
	Code           string `json:"code" gorm:"cloumn:code"`
	Name           string `json:"name" gorm:"cloumn:name"`
}

func (IndustryNameCode) TableName() string {
	return "u_stock_industry"
}

type Statistics struct {
	UpdatedAt       time.Time           `json:"updated_at"`
	SecondIndustry  string              `json:"second_industry" gorm:"cloumn:second_industry"`
	PrePrice        decimal.NullDecimal `json:"pre_price" gorm:"column:pre_price"`                   // 昨收价
	MaxUpDownRange  decimal.NullDecimal `gorm:"column:max_up_down_range" json:"max_up_down_range"`   // 最大涨跌幅
	MinUpDownRange  decimal.NullDecimal `gorm:"column:min_up_down_range" json:"min_up_down_range"`   // 最小涨跌幅
	UpDownRange     decimal.NullDecimal `gorm:"column:up_down_range" json:"up_down_range"`           // 当前涨跌幅
	AmountRate      decimal.NullDecimal `json:"amount_rate" gorm:"column:amount_rate"`               // 当前量比
	Turnover        decimal.NullDecimal `json:"turnover" gorm:"column:turnover"`                     // 当前成交额
	OpenUpDownRange decimal.NullDecimal `gorm:"column:open_up_down_range" json:"open_up_down_range"` // 开盘涨跌幅
}

type IndustryPrice struct {
	SecondIndustry string              `json:"second_industry" gorm:"cloumn:second_industry"`
	PrePrice       decimal.NullDecimal `json:"close" gorm:"column:close"`
	Date           string              `json:"-" gorm:"column:time"`
}

func (IndustryPrice) TableName() string {
	return "u_industry_price"
}

type IndustryUserEvent struct {
	SecondIndustry string `json:"-" gorm:"cloumn:second_industry"`
	Id             int    `json:"id" gorm:"cloumn:id"`                     // 主键ID
	UserID         int    `json:"user_id" gorm:"cloumn:user_id"`           // 用户ID
	ObjectID       int    `json:"object_id" gorm:"cloumn:object_id"`       // 事件ID
	EventStatus    int    `json:"event_status" gorm:"cloumn:event_status"` // 事件状态
	EventStr       string `json:"event_str" gorm:"cloumn:event_str"`       // 点评内容
	ObjectType     string `json:"object_type" gorm:"cloumn:object_type"`   // 对象类型
	EventType      string `json:"event_type" gorm:"cloumn:event_type"`     // 事件类型
	PictureLink    string `json:"picture_link" gorm:"cloumn:picture_link"` // 图片链接
}

func GetIndustryMonitor(req *IndustryListReq) ([]Industry, int, error) {
	// 获取当前用户的二级行业
	industryNames := template.GetUserSecondIndustry(req.UserId)
	if len(industryNames) < 1 {
		return []Industry{}, 0, nil
	}
	if req.IndustryName != "" {
		if !util.ContainsAny(industryNames, req.IndustryName) {
			return []Industry{}, 0, nil
		}
		industryNames = []string{req.IndustryName}
	}
	industryResp := []Industry{}
	industry := []Industry{}
	// 获取时间
	nowHour := time.Now().Hour()
	nowTime := time.Now()
	if nowHour < 9 {
		// 获取上一个交易日
		var yesterday []string
		date := time.Now().Format("2006-01-02")
		err := models.DB().Table("b_trade_date").Where("trade_date < ?", date).
			Order("trade_date DESC").Limit(1).Pluck("trade_date", &yesterday).Error
		if err == nil {
			nowTime, _ = time.ParseInLocation(util.YMDHMS, yesterday[0], time.Local)
		}
	}
	dbs := models.DB().Raw("select * from u_industry_monitor where id in "+
		"(select max(id) from u_industry_monitor where second_industry in (?) and created_at >= ? group by second_industry)",
		industryNames, nowTime.Format(util.YMD),
	)
	if req.SordKey != "" {
		direction := "desc"
		if req.Direction {
			direction = "asc"
		}
		dbs = dbs.Order(req.SordKey + " " + direction)
	} else {
		dbs = dbs.Order("id desc")
	}

	cnt := 0
	dbs.Find(&industry)
	cnt = len(industry)
	err := dbs.Offset((req.PageNum - 1) * req.PageSize).Limit(req.PageSize).Find(&industryResp).Error
	if err != nil {
		return []Industry{}, 0, err
	}
	events := GetIndustryUserEvent(req.UserId)
	for index, value := range industryResp {
		industryResp[index].UpDownRange.Decimal = value.UpDownRange.Decimal.Round(2)
		if events[value.SecondIndustry].Id > 0 {
			industryResp[index].EventsId = events[value.SecondIndustry].Id
			industryResp[index].EventStr = events[value.SecondIndustry].EventStr
			if events[value.SecondIndustry].PictureLink == "" {
				industryResp[index].PictureLinkArr = []template.ImgDTO{}
			} else {
				pictureArr := []template.ImgDTO{}
				_ = json.Unmarshal([]byte(value.PictureLink), &pictureArr)
				industryResp[index].PictureLinkArr = pictureArr
			}
		}
		industryResp[index].Turnover = industryResp[index].TotalTurnover
	}
	return industryResp, cnt, nil
}

// 获取每日的折线图
func GetIndustryPic(req *IndustryPicReq) ([]IndustryPic, decimal.NullDecimal, map[string]interface{}, error) {
	industryResp := []IndustryPic{}
	var (
		price      IndustryPrice
		startPrice IndustryPrice
	)
	extra := make(map[string]interface{})
	// 收盘涨幅
	dbs := models.DB().Raw("select * from u_industry_monitor where id in "+
		"(select max(id) from u_industry_monitor where second_industry = (?) group by time)",
		req.IndustryName,
	)
	err := dbs.Order("time desc").Limit(30).Find(&industryResp).Error
	if err != nil {
		return []IndustryPic{}, price.PrePrice, extra, err
	}
	// 获取开始一天数据
	startPrice, _ = GetNowClose(industryResp[len(industryResp)-1].Date, req.IndustryName)
	sort.Slice(industryResp, func(i, j int) bool {
		return industryResp[i].Date.Format(util.YMDHMS) < industryResp[j].Date.Format(util.YMDHMS)
	})
	// 获取涨跌幅的最大值 最小值
	var maxClose, minClose, maxUpDownRange, minUpDownRange, minUpDown, maxUpDown decimal.Decimal
	for index, value := range industryResp {
		price, err = GetNowClose(value.Date, req.IndustryName)
		if !price.PrePrice.Decimal.IsZero() {
			industryResp[index].Close = price.PrePrice.Decimal
		} else {
			price, err = GetClose(value.Date, req.IndustryName)
			industryResp[index].Close = price.PrePrice.Decimal.
				Mul(industryResp[index].UpDownRange.Add(decimal.NewFromFloat(100)).Div(decimal.NewFromFloat(100))).Round(2)
		}
		industryResp[index].Turnover = value.TotalTurnover
		industryResp[index].NowUpDownRange = value.UpDownRange
		industryResp[index].NowUpDown = value.UpDown
		industryResp[index].UpDownRange = industryResp[index].Close.Div(startPrice.PrePrice.Decimal).
			Sub(decimal.NewFromFloat(1.00)).Mul(decimal.NewFromFloat(100)).Round(2)
		industryResp[index].UpDown = industryResp[index].Close.Sub(startPrice.PrePrice.Decimal).Round(2)
		if maxUpDownRange.IsZero() {
			maxUpDownRange = industryResp[index].UpDownRange

		} else {
			if industryResp[index].UpDownRange.Sub(maxUpDownRange).Sign() > 0 {
				maxUpDownRange = industryResp[index].UpDownRange.Round(2)
			}
		}
		if minUpDownRange.IsZero() {
			minUpDownRange = industryResp[index].UpDownRange
		} else {
			if industryResp[index].UpDownRange.Sub(minUpDownRange).Sign() < 0 {
				minUpDownRange = industryResp[index].UpDownRange.Round(2)
			}
		}
		if maxUpDown.IsZero() {
			maxUpDown = industryResp[index].UpDown
		} else {
			if industryResp[index].UpDown.Sub(maxUpDown).Sign() > 0 {
				maxUpDown = industryResp[index].UpDown.Round(2)
			}
		}
		if minUpDown.IsZero() {
			minUpDown = industryResp[index].UpDown
		} else {
			if industryResp[index].UpDown.Sub(minUpDown).Sign() < 0 {
				minUpDown = industryResp[index].UpDown.Round(2)
			}
		}
		if maxClose.IsZero() {
			maxClose = industryResp[index].Close
		} else {
			if industryResp[index].Close.Sub(maxClose).Sign() > 0 {
				maxClose = industryResp[index].Close
			}
		}
		if minClose.IsZero() {
			minClose = industryResp[index].Close
		} else {
			if industryResp[index].Close.Sub(minClose).Sign() < 0 {
				minClose = industryResp[index].Close
			}
		}
		industryResp[index].RDate = value.Date.Format(util.YMDHMS)
	}
	for index, _ := range industryResp {
		if index == 0 {
			industryResp[index].PreUpDownRange = decimal.NewFromFloat(0)
		} else {
			industryResp[index].PreUpDownRange = industryResp[index-1].UpDownRange
		}
	}
	extra["max_close"] = maxClose
	extra["min_up_down"] = minUpDown
	extra["max_up_down"] = maxUpDown
	extra["min_close"] = minClose
	extra["min_up_down_range"] = minUpDownRange.Round(2)
	extra["max_up_down_range"] = maxUpDownRange.Round(2)
	// 获取昨收价
	return industryResp, startPrice.PrePrice, extra, nil
}

// 获取总成交额
func GetIndustryTurnover(value IndustryPic) (IndustryPic, error) {
	industryResp := IndustryPic{}
	dbs := models.DB().Table("u_industry_monitor").
		Select("total_turnover as total_turnover").
		Where("second_industry = ? ", value.SecondIndustry).Where("time <= ? ", value.Date)
	err := dbs.Order("created_at desc").First(&industryResp).Error
	if err != nil {
		return value, err
	}
	value.TotalTurnover = industryResp.TotalTurnover
	return value, nil
}

// 获取每分钟的折线图
func GetIndustryPicPerMinute(req *IndustryPicReq) ([]IndustryPic, decimal.NullDecimal, map[string]interface{}, error) {
	var (
		price IndustryPrice
	)
	industryResp := []IndustryPic{}
	extra := make(map[string]interface{})
	lastIndustry := IndustryPic{}
	err := models.DB().Table("u_industry_monitor").
		Select("created_at as time,second_industry,up_down_range,turnover").
		Where("second_industry = ? ", req.IndustryName).Order("created_at desc").First(&lastIndustry).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return []IndustryPic{}, price.PrePrice, extra, err
	}
	if err == gorm.ErrRecordNotFound {
		return []IndustryPic{}, price.PrePrice, extra, nil
	}
	// 收盘涨幅
	dbs := models.DB().Table("u_industry_monitor").
		Select("created_at as time,second_industry,up_down_range,turnover").
		Where("second_industry = ? ", req.IndustryName).Where("created_at >= ?", lastIndustry.Date.Format(util.YMD))
	err = dbs.Find(&industryResp).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return []IndustryPic{}, price.PrePrice, extra, err
	}
	if err == gorm.ErrRecordNotFound {
		return []IndustryPic{}, price.PrePrice, extra, nil
	}
	price, err = GetClose(time.Now(), req.IndustryName)
	if err != nil {
		logging.Info("get close err", err)
		return []IndustryPic{}, price.PrePrice, extra, err
	}
	// 获取涨跌幅的最大值 最小值
	var maxUpDownRange, minUpDownRange, maxUpDown, minUpDown decimal.Decimal
	industry := make(map[string]IndustryPic)
	for index, value := range industryResp {
		if index == 0 {
			industryResp[index].UpDownRange = decimal.NewFromFloat(1.00)
			industryResp[index].UpDown = decimal.NewFromFloat(0)
			industryResp[index].PreUpDownRange = decimal.NewFromFloat(0)
		} else {
			industryResp[index].UpDownRange = industryResp[index].UpDownRange.Round(2)
			industryResp[index].UpDown = price.PrePrice.Decimal.Mul(value.UpDownRange).Div(decimal.NewFromFloat(100)).Round(2)
			industryResp[index].PreUpDownRange = industryResp[index-1].UpDownRange
		}
		if maxUpDownRange.IsZero() {
			maxUpDownRange = value.UpDownRange.Round(2)
		} else {
			if value.UpDownRange.Sub(maxUpDownRange).Sign() > 0 {
				maxUpDownRange = value.UpDownRange.Round(2)
			}
		}
		industryResp[index].RDate = value.Date.Format(util.YMDHM)
		if minUpDownRange.IsZero() {
			minUpDownRange = value.UpDownRange.Round(2)
		} else {
			if value.UpDownRange.Sub(minUpDownRange).Sign() < 0 {
				minUpDownRange = value.UpDownRange.Round(2)
			}
		}
		industry[value.Date.Format(util.YMDHM)] = industryResp[index]
	}
	date := GetTimeDuration(lastIndustry.Date)
	ret := []IndustryPic{}
	for _, value := range date {
		if _, ok := industry[value]; ok {
			ret = append(ret, industry[value])
		} else {
			ret = append(ret, IndustryPic{RDate: value, Type: "all"})
		}
	}
	maxUpDown = price.PrePrice.Decimal.Mul(maxUpDownRange).Div(decimal.NewFromFloat(100)).Round(2)
	minUpDown = price.PrePrice.Decimal.Mul(minUpDownRange).Div(decimal.NewFromFloat(100)).Round(2)
	extra["max_up_down_range"] = maxUpDownRange
	extra["min_up_down_range"] = minUpDownRange
	extra["max_up_down"] = maxUpDown
	extra["min_up_down"] = minUpDown
	return ret, price.PrePrice, extra, nil
}

func GetTopData(req *IndustryStockReq) (Statistics, error) {
	lastIndustry := IndustryPic{}
	err := models.DB().Table("u_industry_monitor").
		Select("created_at as time,second_industry,up_down_range,turnover").
		Where("second_industry = ? ", req.IndustryName).Order("created_at desc").First(&lastIndustry).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return Statistics{}, err
	}
	if err == gorm.ErrRecordNotFound {
		return Statistics{}, nil
	}
	price, err := GetClose(lastIndustry.Date, req.IndustryName)
	if err != nil {
		logging.Info("get close err", err)
		return Statistics{}, err
	}
	type UpdownRange struct {
		MaxUpDownRange decimal.NullDecimal `gorm:"column:max_up_down_range" json:"max_up_down_range"`
		MinUpDownRange decimal.NullDecimal `gorm:"column:min_up_down_range" json:"min_up_down_range"`
	}
	upDownRange := UpdownRange{}
	err = models.DB().Table("u_industry_monitor").
		Select("max(up_down_range) as max_up_down_range,min(up_down_range) as min_up_down_range").
		Where("second_industry = ? ", req.IndustryName).
		Where("created_at >= ? ", lastIndustry.Date.Format(util.YMD)).First(&upDownRange).Error
	if err != nil {
		logging.Info("get up_down_range err", err)
		return Statistics{}, err
	}
	industry := IndustrySingle{}
	err = models.DB().Table("u_industry_monitor").
		Where("second_industry = ? ", req.IndustryName).
		Where("created_at >= ? ", lastIndustry.Date.Format(util.YMD)).
		Order("id desc").First(&industry).Error
	if err != nil {
		logging.Info("get turnover err", err)
		return Statistics{}, err
	}
	openIndustry := IndustrySingle{}
	err = models.DB().Table("u_industry_monitor").
		Where("second_industry = ? ", req.IndustryName).
		Where("created_at > ? ", lastIndustry.Date.Format(util.YMD)).
		Order("id asc").First(&openIndustry).Error
	if err != nil {
		logging.Info("get turnover err", err)
		return Statistics{}, err
	}
	return Statistics{
		SecondIndustry:  req.IndustryName,
		PrePrice:        price.PrePrice,
		MaxUpDownRange:  upDownRange.MaxUpDownRange,
		MinUpDownRange:  upDownRange.MinUpDownRange,
		UpDownRange:     industry.UpDownRange,
		OpenUpDownRange: openIndustry.UpDownRange,
		Turnover:        industry.TotalTurnover,
		AmountRate:      industry.AmountRate,
		UpdatedAt:       industry.CreatedAt,
	}, nil
}

func GetIndustryStock(req *IndustryStockReq) ([]IndustryStock, int, error) {
	type Industry struct {
		CreatedAt time.Time `json:"created_at" gorm:"column:created_at"`
		Code      string    `json:"code" gorm:"column:code"`
	}
	industry := []IndustryStock{}
	errI := models.DB().Table("u_stock_industry").
		Select("u_stock_industry.*,b_stock.name").Where("deleted_at is null").
		Joins("left join b_stock on b_stock.id = u_stock_industry.code").
		Where("second_industry = ?  ", req.IndustryName).Find(&industry).Error
	if errI != nil && errI == gorm.ErrRecordNotFound {
		return []IndustryStock{}, 0, errI
	}
	if errI == gorm.ErrRecordNotFound {
		return []IndustryStock{}, 0, nil
	}
	var err error
	ret := []IndustryStock{}
	minute := time.Now().Format("15:04")
	for i, _ := range industry {
		v := &industry[i]
		industyStock := IndustryStock{
			Code:           v.Code,
			SecondIndustry: v.SecondIndustry,
			Name:           v.Name,
		}
		if !util.ContainsAny(GetTickAuctionDuraion(), minute) {
			err = getValueFromRedis(&industyStock)
			if err != nil {
				logging.Info("store.StoreIndustryStockMinute() err:", err.Error())
			}
		} else {
			err = getAuctionTurnover(&industyStock)
			if err != nil {
				logging.Info("store.StoreIndustryStockMinute() err:", err.Error())
			}
		}
		ret = append(ret, industyStock)
		logging.Info(fmt.Sprintf("stock minute code:%v,record:%v", v.Code, industyStock))
	}
	if len(ret) < 1 {
		return []IndustryStock{}, 0, nil
	}
	if req.SordKey != "" {
		sort.Slice(ret, func(i, j int) bool {
			v := reflect.ValueOf(ret[i])
			u := reflect.ValueOf(ret[j])
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(ret[i]).Field(k).Tag.Get("json") == req.SordKey {
					return req.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	}
	lists := []IndustryStock{}
	if req.PageSize > 0 && req.PageNum > 0 {
		start := req.PageSize * (req.PageNum - 1)
		end := req.PageSize * (req.PageNum)
		if req.PageNum*req.PageSize > len(ret) {
			end = len(ret)
		}
		lists = ret[start:end]
	}
	return lists, len(ret), nil
}

func StoreInustryStatistics() {
	// 获取行业对应stock
	ret := GetIndustryStockCode()
	if len(ret) < 1 {
		logging.Info("store second_industry info err :", len(ret))
		return
	}
	// 获取全部数据
	res := []IndustryStock{}
	// wg := sync.WaitGroup{}
	// wg.Add(len(ret))
	// for i,_ := range ret {
	//     go func(idx int) {
	//         v := &ret[idx]
	//         industyStock := IndustryStock{
	//             Code: v.Code,
	//             Name: v.Name,
	//             SecondIndustry: v.SecondIndustry,
	//         }
	//         err := getValueFromRedis(&industyStock)
	//         if err != nil {
	//             logging.Info("store.StoreIndustryStockMinute() err:", err.Error())
	//         }
	//         res = append(res,industyStock)
	//         logging.Info(fmt.Sprintf("stock minute code:%v,record:%v",v.Code,industyStock))
	//         wg.Done()
	//     }(i)
	// }
	// wg.Wait()
	minute := time.Now().Format("15:04")
	var err error
	for i, _ := range ret {
		v := &ret[i]
		industyStock := IndustryStock{
			Code:           v.Code,
			Name:           v.Name,
			SecondIndustry: v.SecondIndustry,
		}
		if !util.ContainsAny(GetTickAuctionDuraion(), minute) {
			err = getValueFromRedis(&industyStock)
			if err != nil {
				logging.Info("store.StoreIndustryStockMinute() err:", err.Error())
			}
		} else {
			err = getAuctionTurnover(&industyStock)
			if err != nil {
				logging.Info("store.StoreIndustryStockMinute() err:", err.Error())
			}
		}
		res = append(res, industyStock)
	}
	code := []string{}
	for _, value := range res {
		code = append(code, value.Code)
	}
	retCode := []string{}
	for _, v := range ret {
		if util.ContainsAny(code, v.Code) {
			continue
		}
		retCode = append(retCode, v.Code)
		industyStock := IndustryStock{
			Code:           v.Code,
			Name:           v.Name,
			SecondIndustry: v.SecondIndustry,
		}
		if !util.ContainsAny(GetTickAuctionDuraion(), minute) {
			err = getValueFromRedis(&industyStock)
			if err != nil {
				logging.Info("store.StoreIndustryStockMinute() err:", err.Error())
			}
		} else {
			err = getAuctionTurnover(&industyStock)
			if err != nil {
				logging.Info("store.StoreIndustryStockMinute() err:", err.Error())
			}
		}
		res = append(res, industyStock)
	}
	var buffer bytes.Buffer
	sql := "insert into `u_industry_stock_minute` (`second_industry`,`code`,`name`,`up_down_range`,`up_down`,`current_price`,`total_market_value`,`circulation_market_value`,`turnover`,`turnover_rate`,`is_block`) values "
	if _, err = buffer.WriteString(sql); err != nil {
		return
	}
	for i, v := range res {
		if i == len(res)-1 {
			buffer.WriteString(fmt.Sprintf("('%v','%v','%v',%v,%v,%v,%v,%v,%v,%v,%v);",
				v.SecondIndustry, v.Code, v.Name, v.UpDownRange.Decimal, v.UpDown.Decimal, v.CurrentPrice.Decimal, v.TotalMarketValue.Decimal,
				v.CirculationMarketValue.Decimal, v.Turnover.Decimal, v.TurnoverRate.Decimal, v.IsBlock))
		} else {
			buffer.WriteString(fmt.Sprintf("('%v','%v','%v',%v,%v,%v,%v,%v,%v,%v,%v),",
				v.SecondIndustry, v.Code, v.Name, v.UpDownRange.Decimal, v.UpDown.Decimal, v.CurrentPrice.Decimal, v.TotalMarketValue.Decimal,
				v.CirculationMarketValue.Decimal, v.Turnover.Decimal, v.TurnoverRate.Decimal, v.IsBlock))
		}
	}
	err = models.DB().Exec(buffer.String()).Error
	if err != nil {
		logging.Info("save stock minute err", err)
		return
	}
	industryMap := make(map[string][]IndustryStock)
	for _, v := range res {
		industryMap[v.SecondIndustry] = append(industryMap[v.SecondIndustry], v)
	}
	wgIndustry := sync.WaitGroup{}
	wgIndustry.Add(len(res))
	for index, _ := range industryMap {
		go func(index string) {
			// 获取平均涨跌幅
			// 获取总的成交额
			// 获取涨停数量
			// 获取上涨数量
			// 获取下跌数量
			value := industryMap[index]
			industryStock := IndustrySingle{}
			avgUpDownRange := decimal.NullDecimal{}
			sumUpDownRange := decimal.NullDecimal{}
			sumTurnover := decimal.NullDecimal{}
			amountRate := decimal.Decimal{}
			blockNum := 0
			increaseNum := 0
			decreaseNum := 0
			mostChange := decimal.Decimal{}
			mostChangeName := ""
			for _, v := range value {
				sumTurnover.Valid = true
				sumTurnover.Decimal = sumTurnover.Decimal.Add(v.Turnover.Decimal)
				if v.IsBlock > 0 {
					blockNum++
				}
				if v.UpDownRange.Decimal.Sign() > 0 {
					increaseNum++
				} else {
					decreaseNum++
				}
				if mostChange.IsZero() {
					mostChange = v.UpDownRange.Decimal
					mostChangeName = v.Name
				} else {
					if v.UpDownRange.Decimal.Sub(mostChange).Sign() > 0 {
						mostChangeName = v.Name
						mostChange = v.UpDownRange.Decimal
					}
				}
			}
			for _, v := range value {
				if !sumTurnover.Decimal.IsZero() {
					sumUpDownRange.Decimal = sumUpDownRange.Decimal.Add(v.UpDownRange.Decimal.Mul(v.Turnover.Decimal).Div(sumTurnover.Decimal))
					sumUpDownRange.Valid = true
				}
			}
			avgUpDownRange.Decimal = sumUpDownRange.Decimal
			avgUpDownRange.Valid = true
			industryStock.UpDownRange = avgUpDownRange
			industryStock.TotalTurnover.Valid = true
			industryStock.TotalTurnover.Decimal = sumTurnover.Decimal
			industryStock.RecordTime = time.Now().Format(util.YMD)
			industryStock.CreatedAt = time.Now()
			industryStock.MinuteTime = time.Now().Format("15:04")
			industryStock.BlockNum = blockNum
			industryStock.DecreaseNum = decreaseNum
			industryStock.IncreaseNum = increaseNum
			industryStock.SecondIndustry = index
			industryStock.LeadStock = mostChangeName
			industry := IndustrySingle{}
			industryStock.Turnover.Valid = true
			err = models.DB().Table("u_industry_monitor").Select("turnover,total_turnover").
				Where("second_industry = ?", index).
				Where("time = ? ", time.Now().Format(util.YMD)).Order("id desc").First(&industry).Error
			if err == gorm.ErrRecordNotFound {
				industryStock.Turnover.Decimal = decimal.NewFromFloat(0.00)
			} else {
				industryStock.Turnover.Decimal = industryStock.TotalTurnover.Decimal.Sub(industry.TotalTurnover.Decimal)
			}
			amountRate = getAmountRate(index, time.Now().Format(util.YMDHM))
			if amountRate.IsZero() {
				industryStock.AmountRate = decimal.NullDecimal{Decimal: decimal.NewFromFloat(1.00), Valid: true}
			} else {
				industryStock.AmountRate.Decimal = industryStock.TotalTurnover.Decimal.Div(amountRate)
				industryStock.AmountRate.Valid = true
			}
			err = models.DB().Table("u_industry_monitor").Create(&industryStock).Error
			if err != nil {
				logging.Info("store industry stock minute err ", err)
			}
			//SendAuctionAmountRate(industryStock.AmountRate, index)
			SendAmountRate(industryStock.AmountRate, index)
			SendUpDownRange(industryStock.UpDownRange, index)
			wgIndustry.Done()
		}(index)
	}
	wgIndustry.Wait()
}

// 记录二级行业 股票 代码
func StoreSecondIndustry() []IndustryNameCode {
	stocks := []IndustryNameCode{}
	err := models.DB().Table("u_stock_industry").
		Select("u_stock_industry.code,u_stock_industry.second_industry,b_stock_names.name").
		Joins("left join b_stock_names on u_stock_industry.code = b_stock_names.code").
		Where("u_stock_industry.deleted_at is null ").Find(&stocks).Error
	if err != nil {
		logging.Info("store second_industry info err :", err)
		return stocks
	}
	redisKey := "industry_stock_cache"
	err = gredis.Set(redisKey, stocks, 12*3600)
	if err != nil {
		logging.Info("store second_industry redis err ", err)
	}
	return stocks
}

func GetIndustryStockCode() []IndustryNameCode {
	redisKey := "industry_stock_cache"
	list, _ := gredis.Get(redisKey)
	ret := []IndustryNameCode{}
	if len(list) > 2 {
		err := json.Unmarshal(list, &ret)
		if err != nil {
			return ret
		}
	} else {
		ret = StoreSecondIndustry()
	}
	return ret
}

func getValueFromRedis(dto *IndustryStock) error {
	var (
		err       error
		id        = dto.Code
		lastClose decimal.NullDecimal
		zgb       decimal.NullDecimal
		ltg       decimal.NullDecimal
	)
	err = dto.Turnover.Scan(getRedisValue(turnoverKey, id))
	if err != nil {
		dto.Turnover.Valid = false
		logging.Info("industry.getValueFromRedis() Turnover err:", err.Error())
		logging.Info("industy code ", id)
		return err
	}
	err = dto.CurrentPrice.Scan(getRedisValue(currentPriceKey, id))
	if err != nil {
		dto.CurrentPrice.Valid = false
		logging.Info("industry.getValueFromRedis() CurrentPrice err:", err.Error())
		logging.Info("industy code ", id)
		return err
	}

	err = lastClose.Scan(getRedisValue(lastCloseKey, id))
	if err != nil {
		lastClose.Valid = false
		logging.Info("industry.getValueFromRedis() lastClose err:", err.Error())
		logging.Info("industy code ", id)
		return err
	} else {
		if lastClose.Decimal.IsZero() {
			return nil
		}
		dto.UpDownRange.Decimal = dto.CurrentPrice.Decimal.Sub(lastClose.Decimal).Mul(decimal.NewFromFloat(100)).DivRound(lastClose.Decimal, 2)
		dto.UpDownRange.Valid = true
		dto.UpDown.Decimal = dto.CurrentPrice.Decimal.Sub(lastClose.Decimal)
		dto.UpDown.Valid = true
	}
	err = zgb.Scan(getRedisValue(zgbKey, id))
	if err != nil {
		zgb.Valid = false
		logging.Info("industry.getValueFromRedis() zgb err:", err.Error())
		logging.Info("industy code ", id)
		return err
	}
	err = ltg.Scan(getRedisValue(ltgKey, id))
	if err != nil {
		ltg.Valid = false
		logging.Info("industry.getValueFromRedis() ltg err:", err.Error())
		logging.Info("industy code ", id)
		return err
	}

	if gredis.Clone(setting.RedisSetting.StockDB).HExists(blockKey, id) {
		blockTimeStr := getRedisValueStr(blockKey, id)
		if blockTimeStr != "" {
			dto.BlockTime, _ = time.Parse(util.YMDHMS, blockTimeStr)
			dto.IsBlock = 1
		} else {
			dto.IsBlock = 0
		}
	} else {
		dto.IsBlock = 0
	}
	if dto.Name == "" {
		dto.Name = getRedisValueStr(nameKey, id)
	}
	dto.CirculationMarketValue.Decimal = ltg.Decimal.Mul(dto.CurrentPrice.Decimal)
	if dto.CirculationMarketValue.Decimal.IsZero() {
		logging.Info("industry.getValueFromRedis() ltg err:")
		logging.Info("industy code ", id)
		return nil
	}
	dto.CirculationMarketValue.Valid = true
	dto.TotalMarketValue.Decimal = zgb.Decimal.Mul(dto.CurrentPrice.Decimal)
	dto.TotalMarketValue.Valid = true
	dto.TurnoverRate.Decimal = dto.Turnover.Decimal.Mul(decimal.NewFromFloat(100)).DivRound(dto.CirculationMarketValue.Decimal, 2)
	dto.TurnoverRate.Valid = true
	return nil
}

// 获取集合竞价成交额
func getAuctionTurnover(dto *IndustryStock) error {
	var (
		err       error
		id        = dto.Code
		minute    string
		bv        decimal.NullDecimal
		bp        decimal.NullDecimal
		lastClose decimal.NullDecimal
		zgb       decimal.NullDecimal
		ltg       decimal.NullDecimal
	)
	minute = time.Now().Format("15:04")
	if !util.ContainsAny(GetAuctionDuraion(), minute) {
		return nil
	}

	err = dto.CurrentPrice.Scan(getRedisValue(currentPriceKey, id))
	if err != nil {
		dto.CurrentPrice.Valid = false
		logging.Info("industry.getValueFromRedis() CurrentPrice err:", err.Error())
		logging.Info("industy code ", id)
		return err
	}

	err = lastClose.Scan(getRedisValue(lastCloseKey, id))
	if err != nil {
		lastClose.Valid = false
		logging.Info("industry.getValueFromRedis() lastClose err:", err.Error())
		logging.Info("industy code ", id)
		return err
	} else {
		if lastClose.Decimal.IsZero() {
			return nil
		}
		dto.UpDownRange.Decimal = dto.CurrentPrice.Decimal.Sub(lastClose.Decimal).Mul(decimal.NewFromFloat(100)).DivRound(lastClose.Decimal, 2)
		dto.UpDownRange.Valid = true
		dto.UpDown.Decimal = dto.CurrentPrice.Decimal.Sub(lastClose.Decimal)
		dto.UpDown.Valid = true
	}
	err = zgb.Scan(getRedisValue(zgbKey, id))
	if err != nil {
		zgb.Valid = false
		logging.Info("industry.getValueFromRedis() zgb err:", err.Error())
		logging.Info("industy code ", id)
		return err
	}
	err = ltg.Scan(getRedisValue(ltgKey, id))
	if err != nil {
		ltg.Valid = false
		logging.Info("industry.getValueFromRedis() ltg err:", err.Error())
		logging.Info("industy code ", id)
		return err
	}

	if gredis.Clone(setting.RedisSetting.StockDB).HExists(blockKey, id) {
		blockTimeStr := getRedisValueStr(blockKey, id)
		if blockTimeStr != "" {
			dto.BlockTime, _ = time.Parse(util.YMDHMS, blockTimeStr)
			dto.IsBlock = 1
		} else {
			dto.IsBlock = 0
		}
	} else {
		dto.IsBlock = 0
	}
	if dto.Name == "" {
		dto.Name = getRedisValueStr(nameKey, id)
	}
	dto.CirculationMarketValue.Decimal = ltg.Decimal.Mul(dto.CurrentPrice.Decimal)
	if dto.CirculationMarketValue.Decimal.IsZero() {
		logging.Info("industry.getValueFromRedis() ltg err:")
		return nil
	}
	err = bv.Scan(getRedisValue(auctionBpKey, id))
	if err != nil {
		dto.Turnover.Valid = false
		logging.Info("industry.getValueFromRedis() Turnover err:", err.Error())
		return err
	}
	err = bp.Scan(getRedisValue(auctionBvKey, id))
	if err != nil {
		dto.Turnover.Valid = false
		logging.Info("industry.getValueFromRedis() CurrentPrice err:", err.Error())
		return err
	}
	dto.Turnover.Decimal = bv.Decimal.Mul(bp.Decimal).Mul(decimal.NewFromInt(100))
	dto.Turnover.Valid = true
	dto.CirculationMarketValue.Valid = true
	dto.TotalMarketValue.Decimal = zgb.Decimal.Mul(dto.CurrentPrice.Decimal)
	dto.TotalMarketValue.Valid = true
	dto.TurnoverRate.Decimal = dto.Turnover.Decimal.Mul(decimal.NewFromFloat(100)).DivRound(dto.CirculationMarketValue.Decimal, 2)
	dto.TurnoverRate.Valid = true
	return nil
}

func getRedisValue(key string, field string) string {
	value, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString(key, field)
	if err != nil {
		logging.Info("redis get "+key+" "+field+" value err:", err.Error())
		return ""
	}
	return value
}
func getRedisValueStr(key string, field string) string {
	value, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString(key, field)
	if err != nil {
		logging.Info("redis get "+key+" "+field+" value err:", err.Error())
		return ""
	}
	return value
}
func getAmountRate(secondIndustry, start string) decimal.Decimal {
	amount := decimal.NewFromFloat(0.00)
	if start == "" {
		return amount
	}

	nowtime := start[11:16]
	industry := IndustrySingle{}
	err := models.DB().Raw("select AVG(total_turnover) as turnover from u_industry_monitor "+
		"where second_industry = ? and minute_time = ?   order by created_at desc limit 3  ",
		secondIndustry, nowtime,
	).Find(&industry).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("get avg amount rate err", err)
		return amount
	}
	if err == gorm.ErrRecordNotFound {
		return amount
	}
	return industry.Turnover.Decimal.Round(2)
}

func getBlockCodeName(secondIndustry, start string) string {
	type SecondName struct {
		Name string `json:"name" gorm:"column:name"`
	}
	name := SecondName{}
	err := models.DB().Table("u_industry_stock_minute").Select("name").Where("second_industry = ?", secondIndustry).
		Where("name NOT LIKE '%ST%'").
		Where("created_at >= ? ", start).Order("up_down_range desc").First(&name).Error
	if err != nil {
		logging.Info("get avg amount rate err", err)
	}
	return name.Name
}

// 获取收盘价
func GetClose(recordTime time.Time, secondIndustry string) (IndustryPrice, error) {
	if recordTime.IsZero() {
		return IndustryPrice{}, errors.New("record time is zero")
	}
	price := IndustryPrice{}
	err := models.DB().Table("u_industry_price").Where("time < ? ", recordTime.Format(util.YMD)).
		Where("second_industry = ? ", secondIndustry).Order("created_at desc").First(&price).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return price, err
	}
	if err == gorm.ErrRecordNotFound {
		price.PrePrice.Decimal = decimal.NewFromFloat(1000.00)
		price.PrePrice.Valid = true
		price.SecondIndustry = secondIndustry
		return price, nil
	}
	return price, nil
}
func GetNowClose(recordTime time.Time, secondIndustry string) (IndustryPrice, error) {
	if recordTime.IsZero() {
		return IndustryPrice{}, errors.New("record time is zero")
	}
	price := IndustryPrice{}
	err := models.DB().Table("u_industry_price").Where("time = ? ", recordTime.Format(util.YMD)).
		Where("second_industry = ? ", secondIndustry).Order("created_at desc").First(&price).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return price, err
	}
	if err == gorm.ErrRecordNotFound {
		return price, nil
	}
	return price, nil
}

func UpdateIndustryPrice() {
	// 获取昨天全部的行业数据
	industryResp := []IndustrySingle{}
	err := models.DB().Table("u_industry_monitor").
		Where("created_at >=  ? ", time.Now().Format(util.YMD)+" 15:00:00").
		Where("created_at < ? ", time.Now().Format(util.YMD)+" 15:01:00").Find(&industryResp).Error
	if err != nil {
		logging.Info("get avg amount rate err", err)
		return
	}
	for _, value := range industryResp {
		price := IndustryPrice{}
		err := models.DB().Table("u_industry_price").
			Where("time < ? ", time.Now().Format(util.YMD)).
			Where("second_industry = ? ", value.SecondIndustry).Order("time desc").First(&price).Error
		if err != nil && err != gorm.ErrRecordNotFound {
			continue
		}
		if err != gorm.ErrRecordNotFound {
			price.PrePrice.Decimal = price.PrePrice.Decimal.Mul(value.UpDownRange.Decimal.Div(decimal.NewFromFloat(100)).Add(decimal.NewFromFloat(1.00)))
			price.PrePrice.Valid = true
		} else {
			price.PrePrice.Decimal = decimal.NewFromFloat(1000.00)
			price.PrePrice.Valid = true
		}
		price.SecondIndustry = value.SecondIndustry
		price.Date = time.Now().Format(util.YMD)
		err = models.DB().Table("u_industry_price").Create(price).Error
		if err != nil {
			logging.Info("save u_industry_price err", err)
			return
		}
	}
}

func GetIndustryUserEvent(userId int) map[string]IndustryUserEvent {
	events := []IndustryUserEvent{}
	dbs := models.DB().Table("u_user_event").
		Select("u_user_event.*,u_industry_monitor.second_industry").
		Joins("left join u_industry_monitor ON u_user_event.object_id = u_industry_monitor.id").
		Where("u_user_event.object_type = ? ", "industry_monitor").
		Where("u_user_event.user_id = ?", userId)
	dbs = dbs.Where("u_user_event.created_at >= ? ", time.Now().Format(util.YMD))
	err := dbs.Find(&events).Error
	if err != nil {
		logging.Info("get user event err")
		return nil
	}
	ret := make(map[string]IndustryUserEvent)
	for _, value := range events {
		ret[value.SecondIndustry] = value
	}
	return ret
}

func GetHistroyEvent(req *IndustryStockReq) ([]template.HistroyEvent, int, error) {
	var userEvent []template.HistroyEvent
	cnt := 0
	dbs := models.DB().Table("u_user_event").
		Joins("left join u_industry_monitor on u_user_event.object_id = u_industry_monitor.id").
		Where("u_user_event.object_type in (?)", []string{"industry_monitor"}).
		Where("u_user_event.event_str != ?", "")
	if req.IndustryName != "" {
		dbs = dbs.Where("u_industry_monitor.second_industry = ? ", req.IndustryName)
	}
	if req.UserId > 0 {
		dbs = dbs.Where("u_user_event.user_id = ? ", req.UserId)
	}
	dbs.Count(&cnt)
	err := dbs.Order("u_user_event.updated_at desc").Offset((req.PageNum - 1) * req.PageSize).Limit(req.PageSize).Find(&userEvent).Error
	if err != nil {
		return userEvent, cnt, err
	}
	if len(userEvent) < 1 {
		return []template.HistroyEvent{}, cnt, nil
	}
	for index, value := range userEvent {
		if value.PictureLink == "" {
			userEvent[index].PictureLinkArr = []template.ImgDTO{}
		} else {
			pictureArr := []template.ImgDTO{}
			_ = json.Unmarshal([]byte(value.PictureLink), &pictureArr)
			userEvent[index].PictureLinkArr = pictureArr
		}
	}
	return userEvent, cnt, nil
}

func StoreInustryStatisticsTest() {
	var err error
	// 获取所有的二级行业
	secondIndustryNames := []string{}
	secondIndustryCodes := make(map[string][]string)
	ret := GetIndustryStockCode()
	for _, value := range ret {
		if !util.ContainsAny(secondIndustryNames, value.SecondIndustry) {
			secondIndustryNames = append(secondIndustryNames, value.SecondIndustry)
		}
		secondIndustryCodes[value.SecondIndustry] = append(secondIndustryCodes[value.SecondIndustry], value.Code)
	}
	timeArr := []string{
		// "2020-11-02",
		// "2020-11-03",
		// "2020-11-04",
		// "2020-11-05",
		// "2020-11-06",
		// "2020-11-09",
		// "2020-11-10",
		// "2020-11-11",
		// "2020-11-12",
		// "2020-11-13",
		// "2020-11-16",
		// "2020-11-17",
		// "2020-11-18",
		// "2020-11-19",
		// "2020-11-20",
		// "2020-11-23",
		// "2020-11-24",
		// "2020-11-25",
		// "2020-11-26",
		// "2020-11-27",
		// "2020-11-30",
		// "2020-12-09",
		// "2020-12-01",
		// "2020-12-02",
		// "2020-12-03",
		// "2020-12-04",
		// "2020-12-07",
		// "2020-12-08",
		// "2020-12-09",
		// "2020-12-10",
		"2020-12-14",
		//"2020-12-14",
	}
	// wg := sync.WaitGroup{}
	// wg.Add(len(ret))
	for industry, value1 := range secondIndustryCodes {
		for _, value := range timeArr {
			industryStock := IndustrySingle{}
			pretime, _ := time.Parse(util.YMD, value)
			err = models.DB().Table("p_stock_tick").
				Select("avg(p_stock_tick.pct_chg) as up_down_range,sum(amount)*1000 as total_turnover,sum(is_block) as block_num,"+
					"sum(IF(p_stock_tick.change > 0 ,1,0)) as increase_num,"+
					"sum(IF(p_stock_tick.change < 0 ,1,0)) as decrease_num").Where("p_stock_tick.stock_code in(?)", value1).
				Where("trade_date < ?", pretime.AddDate(0, 0, 1).Format(util.YMD)).
				Where("trade_date >= ?", value).First(&industryStock).Error
			if err != nil {
				logging.Info("store industry static err ", err)
				return
			}
			// industryStock.LeadStock = ""
			// industryStock.SecondIndustry = industry
			// if getAmountRate(industry,value+ " 15:00:00").IsZero() {
			//     industryStock.AmountRate = decimal.NullDecimal{Decimal: decimal.NewFromFloat(1.00),Valid: true,}
			// }else {
			//     industryStock.AmountRate.Decimal = industryStock.Turnover.Decimal.Div(
			//         getAmountRate(industry,value+ " 15:00:00"))
			//     industryStock.AmountRate.Valid = true
			// }
			data := map[string]interface{}{
				"total_turnover": industryStock.TotalTurnover,
			}
			ic, _ := time.ParseInLocation(util.YMDHMS, value+" 15:00:36", time.Local)
			err = models.DB().Table("u_industry_monitor").Where("time = ? ", value).Where("second_industry = ?", industry).Where("created_at = ? ", ic).Updates(data).Error
			// industryStock.RecordTime = value
			// industryStock.CreatedAt,_ = time.ParseInLocation(util.YMDHMS,value+ " 15:00:00",time.Local)
			// err = models.DB().Table("u_industry_monitor").Create(&industryStock).Error
			// if err != nil {
			//     logging.Info("store industry stock minute err ",err)
			// }
		}
	}

	// for i,_ := range secondIndustryNames {
	//     go func(idx int) {
	//         industryStock := IndustrySingle{}
	//         err = models.DB().Table("p_stock_tick").
	//             Select("avg(p_stock_tick.change) as up_down_range,sum(vol)*1000 as turnover,sum(is_block) as block_num," +
	//                 "sum(IF(p_stock_tick.change > 0 ,1,0)) as increase_num," +
	//                 "sum(IF(p_stock_tick.change < 0 ,1,0)) as decrease_num").Where("p_stock_tick.stock_code in(?)",secondIndustryCodes[secondIndustryNames[idx]]).
	//             Where("trade_date < ?","2020-12-01").
	//             Where("trade_date >= ?","2020-11-30").First(&industryStock).Error
	//         if err != nil {
	//             logging.Info("store industry static err ",err)
	//             return
	//         }
	//         industryStock.LeadStock = ""
	//         industryStock.SecondIndustry = secondIndustryNames[idx]
	//         if getAmountRate(secondIndustryNames[idx],"2020-11-30 15:00:00").IsZero() {
	//             industryStock.AmountRate = decimal.NullDecimal{Decimal: decimal.NewFromFloat(1.00),Valid: true,}
	//         }else {
	//             industryStock.AmountRate.Decimal = industryStock.Turnover.Decimal.Div(
	//                 getAmountRate(secondIndustryNames[idx],"2020-11-30 15:00:00"))
	//             industryStock.AmountRate.Valid = true
	//         }
	//         industryStock.RecordTime = "2020-11-30"
	//         industryStock.CreatedAt = "2020-11-30 15:00:00"
	//         err = models.DB().Table("u_industry_monitor").Create(&industryStock).Error
	//         if err != nil {
	//             logging.Info("store industry stock minute err ",err)
	//         }
	//         wg.Done()
	//     }(i)
	//
	// }
	// wg.Wait()
}

func UpdateIndustryPriceTest() {
	timeArr := []string{
		"2020-12-09",
		"2020-12-10",
	}
	secondIndustryNames := []string{}
	secondIndustryCodes := make(map[string][]string)
	ret := GetIndustryStockCode()
	for _, value := range ret {
		if !util.ContainsAny(secondIndustryNames, value.SecondIndustry) {
			secondIndustryNames = append(secondIndustryNames, value.SecondIndustry)
		}
		secondIndustryCodes[value.SecondIndustry] = append(secondIndustryCodes[value.SecondIndustry], value.Code)
	}
	// 获取昨天全部的行业数据
	for _, value := range timeArr {
		for _, v := range secondIndustryNames {
			industryResp := IndustrySingle{}
			err := models.DB().Table("u_industry_monitor").
				Where("created_at >=  ? ", value+" 15:00:00").Where("second_industry = ? ", v).
				Where("created_at < ? ", value+" 15:01:00").Find(&industryResp).Error
			if err != nil {
				logging.Info("get avg amount rate err", err)
				continue
			}
			price := IndustryPrice{}
			err = models.DB().Table("u_industry_price").Where("time < ? ", value+" 00:00:00").
				Where("second_industry = ? ", v).Order("time desc").First(&price).Error
			if err != nil && err != gorm.ErrRecordNotFound {
				continue
			}
			if err != gorm.ErrRecordNotFound {
				price.PrePrice.Decimal = price.PrePrice.Decimal.Mul(industryResp.UpDownRange.Decimal.Div(decimal.NewFromFloat(100)).Add(decimal.NewFromFloat(1.00)))
				price.PrePrice.Valid = true
			} else {
				price.PrePrice.Decimal = decimal.NewFromFloat(1000.00)
				price.PrePrice.Valid = true
			}
			price.SecondIndustry = industryResp.SecondIndustry
			price.Date = value
			err = models.DB().Table("u_industry_price").Create(price).Error
			if err != nil {
				logging.Info("save u_industry_price err", err)
				return
			}
		}

	}
}

func SendUpDownRange(upDownRange decimal.NullDecimal, industryName string) {
	minute := time.Now().Format("15:04")
	if util.ContainsAny(GetTickAuctionDuraion(), minute) {
		return
	}
	if upDownRange.Decimal.Sub(decimal.NewFromFloat(1.5)).Sign() > 0 {
		m := map[string]interface{}{
			"type":    "system_industry_monitor",
			"content": fmt.Sprintf("%s涨幅%v%%", industryName, upDownRange.Decimal.Round(2)),
		}
		PushIndustry(m, industryName)
	} else if upDownRange.Decimal.Sub(decimal.NewFromFloat(-1.5)).Sign() < 0 {
		m := map[string]interface{}{
			"type":    "system_industry_monitor",
			"content": fmt.Sprintf("%s跌幅%v%%", industryName, upDownRange.Decimal.Abs().Round(2)),
		}
		PushIndustry(m, industryName)
	}

	return
}
func PushIndustry(m interface{}, industryName string) {
	pushAuctionKey := time.Now().Format("20060102") + "industry"
	ret, errRet := gredis.SMEMBERS(pushAuctionKey, industryName)
	if errRet != nil || ret {
		return
	}
	userModel := []User{}
	err := models.DB().Table("u_second_industry_user").
		Select("u_second_industry_user.user_id").
		Where("u_second_industry_user.second_industry = ?", industryName).
		Where("u_second_industry_user.deleted_at is null ").Find(&userModel).Error
	if err != nil {
		return
	}
	gredis.SAdd(pushAuctionKey, industryName)
	gredis.Expire(pushAuctionKey, 9*3600)
	for _, value := range userModel {
		ws := websocket.GetInstance()
		bt, _ := json.Marshal(m)
		msg := message.ParseChanelMsg(message.Message_Industry_Monitor, "system", strconv.Itoa(value.UserID), string(bt), "", true)
		ws.SysSend(msg)
	}
}
func SendAmountRate(amount decimal.NullDecimal, industryName string) {
	minute := time.Now().Format("15:04")
	if util.ContainsAny(GetTickAuctionDuraion(), minute) {
		return
	}
	if amount.Decimal.Sub(decimal.NewFromFloat(1.5)).Sign() < 0 {
		return
	}
	m := map[string]interface{}{
		"type":    "system_industry_monitor",
		"content": fmt.Sprintf("%s量能比达%v", industryName, amount.Decimal.Round(2)),
	}
	PushIndustry(m, industryName)
	return
}
func SendAuctionAmountRate(amount decimal.NullDecimal, industryName string) {
	minute := time.Now().Format("15:04")
	if !util.ContainsAny(GetAuctionDuraion(), minute) {
		return
	}
	if amount.Decimal.Sub(decimal.NewFromFloat(2.0)).Sign() < 0 {
		return
	}
	if strings.Index(industryName, "II") != -1 {
		return
	}
	m := map[string]interface{}{
		"type":    "system_industry_monitor",
		"content": fmt.Sprintf("%s今日竞价受关注", industryName),
	}
	pushAuctionKey := time.Now().Format("20060102") + "_auction_industry"
	ret, errRet := gredis.SMEMBERS(pushAuctionKey, industryName)
	if errRet != nil || ret {
		return
	}
	gredis.SAdd(pushAuctionKey, industryName)
	gredis.Expire(pushAuctionKey, 9*3600)
	ws := websocket.GetInstance()
	bt, _ := json.Marshal(m)
	msg := message.ParseMsg(message.Message_Stock, "system", "", string(bt), true)
	ws.SysSend(msg)
	logging.Info("推送集合竞价量比行业为：", industryName)
	return
}
func GetTimeDuration(minuteTime time.Time) []string {
	mstart, _ := time.Parse(util.YMDHM, minuteTime.Format(util.YMD)+" 09:29")
	mend, _ := time.Parse(util.YMDHM, minuteTime.Format(util.YMD)+" 11:31")
	astart, _ := time.Parse(util.YMDHM, minuteTime.Format(util.YMD)+" 13:00")
	aend, _ := time.Parse(util.YMDHM, minuteTime.Format(util.YMD)+" 15:01")
	ret := []string{}
	var i int
	var now time.Time
	now, _ = time.Parse(util.YMDHM, minuteTime.Format(util.YMD)+" 09:29")
	for i = 0; i <= 330; i++ {
		now = now.Add(time.Minute)
		if mend.After(now) && mstart.Before(now) ||
			aend.After(now) && astart.Before(now) {
			ret = append(ret, now.Format(util.YMDHM))
		}
	}
	return ret
}
func GetAuctionDuraion() []string {
	return []string{"09:25"}
}
func GetTickAuctionDuraion() []string {
	return []string{"09:21", "09:22", "09:23", "09:24", "09:25"}
}
func GetStockTradeDate() bool {
	count := 0
	err := models.DB().Table("b_trade_date").Where("trade_date = ?", time.Now().Format(util.YMD)).Count(&count).Error
	if err != nil {
		return false
	}
	if count > 0 {
		return true
	}
	return false
}
func GetLastData(industryName string) decimal.Decimal {
	industry := IndustryStock{}
	err := models.DB().Table("u_industry_monitor").Select("turnover,total_turnover").
		Where("second_industry = ?", industryName).
		Where("created_at < ? ", time.Now().Format(util.YMD)).Order("created_at desc").First(&industry).Error
	if err != nil {
		return decimal.NewFromFloat(0.00)
	}
	return industry.Turnover.Decimal
}
func StoreInustryStatisticsTest2() {
	// 获取所有的二级行业
	secondIndustryNames := []string{}
	ret := GetIndustryStockCode()
	for _, value := range ret {
		if value.SecondIndustry != "白色家电" && value.SecondIndustry != "渔业" {
			if !util.ContainsAny(secondIndustryNames, value.SecondIndustry) {
				secondIndustryNames = append(secondIndustryNames, value.SecondIndustry)
			}
		}
	}
	// 获取minute的创建时间集合
	indu := []IndustrySingle{}
	err := models.DB().Table("u_industry_stock_minute").Where("code = ? ", "300094").
		Where("created_at <= ? ", "2020-12-11 15:01:00").
		Where("created_at > ? ", "2020-12-11 13:03:00").Find(&indu).Error
	if err != nil {
		return
	}
	for _, value := range secondIndustryNames {
		for idx, _ := range indu {
			industryStock := IndustrySingle{}
			err := models.DB().Raw("select avg(up_down_range) as up_down_range,sum(turnover) as turnover,sum(is_block) as block_num,sum(IF(up_down_range > 0 ,1,0)) as increase_num,sum(IF(up_down_range < 0 ,1,0)) as decrease_num,second_industry from (select * from u_industry_stock_minute "+
				"where second_industry = ? and  created_at >= ? and created_at <= ? and name NOT LIKE '%ST%') as a ",
				value, indu[idx].CreatedAt.Add(-time.Second*10).Format(util.YMDHMS),
				indu[idx].CreatedAt.Add(time.Second*10).Format(util.YMDHMS),
			).Find(&industryStock).Error
			if err != nil {
				logging.Info("store industry static err ", err)
				return
			}
			if industryStock.UpDownRange.Decimal.IsZero() {
				logging.Info("store industry static err ", err)
				return
			}
			type SecondName struct {
				Name string `json:"name" gorm:"column:name"`
			}
			name := SecondName{}
			_ = models.DB().Table("u_industry_stock_minute").Select("name").Where("second_industry = ?", value).
				Where("created_at >= ? ", indu[idx].CreatedAt.Add(-time.Second*10).Format(util.YMDHMS)).
				Where("name NOT LIKE '%ST%'").
				Where("created_at < ? ", indu[idx].CreatedAt.Add(time.Second*20).Format(util.YMDHMS)).
				Order("up_down_range desc").First(&name).Error
			industryStock.LeadStock = name.Name
			industryStock.TotalTurnover.Decimal = industryStock.Turnover.Decimal
			industryStock.TotalTurnover.Valid = true
			nowtime := indu[idx].CreatedAt.Format(util.YMDHM)[11:16]
			industry1 := IndustrySingle{}
			err = models.DB().Table("u_industry_monitor").Select("turnover,total_turnover").
				Where("second_industry = ?", value).
				Where("created_at >= ? ", indu[idx].CreatedAt.Add(-time.Second*60).Format(util.YMDHM)).
				Where("created_at < ? ", indu[idx].CreatedAt.Add(time.Second).Format(util.YMDHMS)).Order("created_at desc").First(&industry1).Error
			if err == gorm.ErrRecordNotFound {
				industryStock.Turnover.Decimal = decimal.NewFromFloat(0.00)
			} else {
				industryStock.Turnover.Decimal = industryStock.Turnover.Decimal.Sub(industry1.TotalTurnover.Decimal)
			}

			industryStock.Turnover.Valid = true

			industry := IndustrySingle{}
			_ = models.DB().Raw("select AVG(turnover) as turnover from (select turnover from u_industry_monitor "+
				"where second_industry = ? and left(right(created_at,8),5) = ? and created_at < ?  order by created_at desc limit 3 ) as a ",
				value, nowtime, indu[idx].CreatedAt.Format(util.YMD),
			).Find(&industry).Error
			if industry.Turnover.Decimal.IsZero() {
				industryStock.AmountRate = decimal.NullDecimal{Decimal: decimal.NewFromFloat(1.00), Valid: true}
			} else {
				industryStock.AmountRate.Decimal = industryStock.Turnover.Decimal.Div(industry.Turnover.Decimal)
				industryStock.AmountRate.Valid = true
			}
			industryStock.CreatedAt = indu[idx].CreatedAt
			industryStock.RecordTime = indu[idx].CreatedAt.Format(util.YMD)
			err = models.DB().Table("u_industry_monitor").Create(&industryStock).Error
			if err != nil {
				logging.Info("store industry stock minute err ", err)
			}
		}
	}

}
func StoreInustryStatisticsTest3() {
	// 获取所有的二级行业
	secondIndustryNames := []string{}
	ret := GetIndustryStockCode()
	for _, value := range ret {
		if value.SecondIndustry == "其他轻工制造" {
			if !util.ContainsAny(secondIndustryNames, value.SecondIndustry) {
				secondIndustryNames = append(secondIndustryNames, value.SecondIndustry)
			}
		}

	}
	// 获取minute的创建时间集合
	indu := []IndustrySingle{}
	err := models.DB().Table("u_industry_monitor").
		Where("created_at < ? ", "2020-12-16 15:01:00").
		Where("created_at >= ? ", "2020-12-16 15:00:00").Find(&indu).Error
	if err != nil {
		return
	}
	for _, value := range indu {
		industry := IndustrySingle{}
		_ = models.DB().Raw("select AVG(turnover) as turnover from (select total_turnover as turnover from u_industry_monitor "+
			"where second_industry = ? and left(right(created_at,8),5) = ? and created_at < ?  order by created_at desc limit 3 ) as a ",
			value.SecondIndustry, "15:00", value.CreatedAt.Format(util.YMD),
		).Find(&industry).Error
		if industry.Turnover.Decimal.IsZero() {
			value.AmountRate = decimal.NullDecimal{Decimal: decimal.NewFromFloat(1.00), Valid: true}
		} else {
			value.AmountRate.Decimal = value.TotalTurnover.Decimal.Div(industry.Turnover.Decimal)
			value.AmountRate.Valid = true
		}

		data := map[string]interface{}{
			"amount_rate": value.AmountRate,
		}
		err = models.DB().Table("u_industry_monitor").Where("id = ? ", value.ID).Updates(data).Error
		if err != nil {
			logging.Info("store industry stock minute err ", err)
		}
	}
}
