package home

import (
	"datacenter/models"
	"datacenter/models/stock"
	"datacenter/models/template"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type StockPoolRequest struct {
	PageSize    int       `json:"page_size" form:"page_size" binding:"required,gt=0"`
	MaxID       int       `json:"max_id" form:"max_id"`
	MinID       int       `json:"min_id" form:"min_id"`
	UserID      int       `json:"user_id" form:"user_id"`
	SordKey     string    `json:"sort_key" form:"sort_key"`
	Direction   bool      `json:"direction" form:"direction"`
	Industry    string    `json:"industry" form:"industry"`
	Type        string    `json:"type" form:"type" binding:"required"`
	Description string    `json:"description" form:"description"`
	DateTime    time.Time `json:"date_time" form:"date_time" time_format:"2006-01-02"`
	Code        string    `json:"code" form:"code"`
}

type StockResponse struct {
	Id               int                 `json:"id"`
	Num              int                 `json:"-"`
	Code             string              `json:"code"`
	Name             string              `json:"name"`
	TransferAt       string              `json:"transfer_at"`
	Content          string              `json:"event_str"`     //点评内容
	PreEventStr      string              `json:"pre_event_str"` //历史点评内容
	IndustryName     string              `json:"industry_name"` //行业内容
	IsRecommend      bool                `json:"is_recommend"`  //是否推荐过
	Description      string              `json:"description"`   //情况说明
	TotalMarketValue string              `json:"total_market_value"`
	PictureLink      []ImgDTO            `json:"picture_link"`
	CurrentPrice     decimal.NullDecimal `json:"current_price"`
	UpDownRange      decimal.NullDecimal `json:"up_down_range"`
	Turnover         decimal.NullDecimal `json:"turnover"`
	Head             int                 `json:"head"`
}
type StockAddRequest struct {
	UserId int    `json:"user_id" form:"user_id" binding:"required"`
	Code   string `json:"code" form:"code" binding:"required"`
	Type   string `json:"type" form:"type" binding:"required"`
}
type StockDelRequest struct {
	ID int `json:"id" form:"id" binding:"required"`
}
type StockNamesRequest struct {
	IndustryName string `json:"industry_name" form:"industry_name"`
}
type StockPool struct {
	Id            int                 `json:"id" gorm:"column:id"`
	UserID        int                 `json:"user_id" gorm:"column:user_id"`
	Code          string              `json:"code" gorm:"column:code"`
	Type          string              `json:"type" gorm:"column:type"`
	Description   string              `json:"description" gorm:"column:description"` //情况说明
	TransferPrice decimal.NullDecimal `gorm:"column:transfer_price" json:"-"`
	CreatedAt     time.Time           `json:"-"`
	UpdatedAt     time.Time           `json:"-"`
	DeletedAt     *time.Time          `json:"-" sql:"index"`
}
type IndustryNames struct {
	Id           int    `json:"id" gorm:"column:-"`
	IndustryName string `json:"name" gorm:"column:actual_industry"`
	IndustryID   string `json:"industry_id" gorm:"column:industry_id"`
}

func (StockPool) TableName() string {
	return "u_user_stock"
}

func (c *StockPool) Add() error {
	return template.AddUserStockPool(c.Code, c.Type, c.Description, c.UserID)
}
func (c *StockPool) Del() error {
	if err := models.DB().Where("id = ?", c.Id).Delete(StockPool{}).Error; err != nil {
		return err
	}
	return nil
}

func (c *StockPool) ExistByID() (bool, error) {
	var stockPool StockPool
	err := models.DB().Select("id").Where("id = ?", c.Id).First(&stockPool).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	if stockPool.Id > 0 {
		return true, nil
	}
	return false, nil
}

func (c *StockPool) ExistByCodeAndUserID() (bool, error) {
	var stockPool StockPool
	dbs := models.DB().Select("id").Where("code = ?", c.Code).Where("user_id = ? ", c.UserID).
		Where("type = ? ", c.Type)
	// 行情里面的添加只验证当天的
	if c.Type == "personal_stock" {
		dbs = dbs.Where("created_at >= ?", time.Now().Format(util.YMD))
	}
	err := dbs.First(&stockPool).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	if stockPool.Id > 0 {
		return true, nil
	}
	return false, nil
}

func Formate(dto stock.StockPoolDTO) *StockResponse {
	marketPrice := dto.TotalMarketValue.Decimal.String()
	if !dto.TotalMarketValue.Valid {
		marketPrice = ""
	}
	isRecommend := false
	if dto.EventType == "recommend" {
		isRecommend = true
	}
	des := dto.Description
	if strings.HasPrefix(des, ",") {
		des = strings.TrimLeft(des, ",")
	}
	preEventStr := GetPreEventStr(dto.UserId, dto.Code, dto.ObjectType)
	pictureLink := []ImgDTO{}
	_ = json.Unmarshal([]byte(dto.PictureLink), &pictureLink)
	return &StockResponse{
		Id:               dto.ID,
		Code:             dto.Code,
		Name:             dto.Stock.Name,
		CurrentPrice:     dto.CurrentPrice,
		UpDownRange:      dto.UpDownRange,
		Turnover:         dto.Turnover,
		Content:          dto.EventStr,
		TransferAt:       dto.CreatedAt.Format("2006-01-02 15:04:05"),
		IndustryName:     dto.IndustryClassification,
		TotalMarketValue: marketPrice,
		IsRecommend:      isRecommend,
		PictureLink:      pictureLink,
		Description:      des,
		Num:              dto.Num,
		PreEventStr:      preEventStr,
		Head:             dto.Head,
	}
}

func GetStockPoolList(stockPoolRequest *StockPoolRequest) ([]*StockResponse, int, error) {
	cnt := 0
	dto := make([]stock.HomeStockPool, 0)
	result := make([]*StockResponse, 0)
	dto2 := make([]stock.StockPoolDTO, 0)
	ret := make([]stock.StockPoolDTO, 0)
	sizeRet := make([]stock.StockPoolDTO, 0)
	//tsBegin := time.Now()
	dbs := models.DB().Table("u_user_stock").
		Joins("left join b_stock on b_stock.id = u_user_stock.code").
		Joins("left join u_stock_industry on u_stock_industry.code = u_user_stock.code").
		Select("/*+NO_PARALLEL()*/ GROUP_CONCAT(distinct u_user_stock.description) description,u_user_stock.*,b_stock.name as name,u_user_stock.created_at as transfer_at," +
			"u_stock_industry.second_industry as industry_name,u_stock_industry.head").
		Where("u_user_stock.deleted_at IS NULL")

	if stockPoolRequest.Industry != "" {
		dbs = dbs.Where("u_stock_industry.second_industry like ?", "%"+stockPoolRequest.Industry+"%")
	}
	if stockPoolRequest.UserID > 0 {
		dbs = dbs.Where("u_user_stock.user_id = ?", stockPoolRequest.UserID)
	}
	if stockPoolRequest.Type != "" {
		if stockPoolRequest.Type == "personal_stock" && !stockPoolRequest.DateTime.IsZero() {
			start := stockPoolRequest.DateTime.Format("2006-01-02 00:00:00")
			end := stockPoolRequest.DateTime.AddDate(0, 0, 1).Format("2006-01-02 00:00:00")
			dbs = dbs.Where("u_user_stock.created_at >= ?", start).Where("u_user_stock.created_at < ?", end)
		}
		dbs = dbs.Where("u_user_stock.type = ?", stockPoolRequest.Type)
	}
	if stockPoolRequest.Description != "" {
		dbs = dbs.Where("u_user_stock.description in (?)", strings.Split(stockPoolRequest.Description, ","))
	}
	if stockPoolRequest.Code != "" {
		stockPoolRequest.Code = "%" + stockPoolRequest.Code + "%"
		dbs = dbs.Where("b_stock.id like ? OR b_stock.name like ?", stockPoolRequest.Code, stockPoolRequest.Code)
	}
	dbs = dbs.Group("u_user_stock.code").Order("id desc")
	err := dbs.Find(&dto).Error
	if err != nil {
		logging.Info("user_stock_pool.GetStockPoolList() err:", err.Error())
		return nil, cnt, err
	}
	codes := []int{}
	for _, d := range dto {
		codes = append(codes, d.Id)
	}
	userEvents, _ := GetEventsByIds(codes, stockPoolRequest.Type)
	for _, d := range dto {
		// 获取用户的点评内容和图片链接
		dto2 = append(dto2, stock.StockPoolDTO{
			UserId: strconv.Itoa(stockPoolRequest.UserID),
			ID:     d.Id, UserStockPool: stock.UserStockPool{
				PictureLink: userEvents[d.Id].PictureLink,
				Description: d.Description,
				EventType:   userEvents[d.Id].EventType,
				EventStr:    userEvents[d.Id].EventStr,
				ObjectType:  userEvents[d.Id].ObjectType,
			}, StockPoolRaw: stock.StockPoolRaw{
				Code:  d.Code,
				Stock: stock.StockDTO{Name: d.Name}, CreatedAt: d.TransferAt, IndustryClassification: d.IndustryName, TransferPrice: d.TransferPrice,
			},
			Head: d.Head,
		})
	}

	ret = make([]stock.StockPoolDTO, len(dto2))
	wg := sync.WaitGroup{}
	wg.Add(len(dto2))
	for i := 0; i < len(dto2); i++ {
		go func(idx int) {
			v := &dto2[idx]
			v.IgnorePopular = true
			err = stock.GetValueFromRedis(v)
			if err != nil {
				logging.Info("user_stock_pool.GetStockPoolList() err:", err.Error())
			}
			ret[idx] = *v
			wg.Done()
		}(i)
	}
	wg.Wait()
	if stockPoolRequest.SordKey != "" {
		sort.Slice(ret, func(i, j int) bool {
			v := reflect.ValueOf(ret[i].StockPoolRaw)
			u := reflect.ValueOf(ret[j].StockPoolRaw)
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(ret[i].StockPoolRaw).Field(k).Tag.Get("json") == stockPoolRequest.SordKey {
					return stockPoolRequest.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	}
	maxIndex := 0
	minIndex := 0
	for index, value := range ret {
		if stockPoolRequest.MaxID > 0 && value.ID == stockPoolRequest.MaxID {
			maxIndex = index
		}
		if stockPoolRequest.MinID > 0 && value.ID == stockPoolRequest.MinID {
			minIndex = index
		}
	}
	if stockPoolRequest.MaxID == 0 {
		if stockPoolRequest.MinID > 0 {
			maxIndex = minIndex
			minIndex = minIndex - stockPoolRequest.PageSize
			if minIndex < 0 {
				minIndex = 0
			}
			if maxIndex < 0 {
				minIndex = 0
			}
		}
		if stockPoolRequest.MinID == 0 {
			maxIndex = stockPoolRequest.PageSize
			if maxIndex > len(ret) {
				maxIndex = len(ret)
			}
		}
	}

	if stockPoolRequest.MaxID > 0 {
		minIndex = maxIndex
		maxIndex = maxIndex + stockPoolRequest.PageSize
		if maxIndex > len(ret) {
			maxIndex = len(ret)
		}
	}
	if minIndex == maxIndex {
		return result, cnt, nil
	}
	cnt = len(ret)
	sizeRet = ret[minIndex:maxIndex]
	for _, v := range sizeRet {
		result = append(result, Formate(v))
	}
	return result, cnt, nil
}

func GetIndustryNames(data map[string]interface{}) ([]map[string]string, error) {
	names := []IndustryNames{}
	ret := []map[string]string{}

	dbs := models.DB().Table("u_performance_industry").Select("industry_name as actual_industry,id as industry_id")
	if data["industry_name"].(string) != "" {
		dbs = dbs.Where("industry_name  ?","%"+data["industry_name"].(string)+"%")
	}
	err := dbs.Find(&names).Error
	if err != nil  {
		return nil, err
	}
	for index, _ := range names {
		tmp := map[string]string{}
		tmp["key"] = names[index].IndustryID
		tmp["title"] = names[index].IndustryName
		ret = append(ret, tmp)
	}
	return ret, nil
}

func GetEventsByCode(objectId int, objectType string) (template.UserEvent, error) {
	userEventModel := template.UserEvent{}
	err := models.DB().Table("u_user_event").Where("code = ? ", objectId).Where("object_type = ? ", objectType).Find(&userEventModel).Error
	if err != nil {
		logging.Info("user_stock_pool.GetIndustryNames() err:", err.Error())
		return userEventModel, err
	}
	return userEventModel, nil
}

func (s *StockPool) UpdateStockPool() error {
	var stockPool StockPool
	if s.Id < 1 {
		return errors.New("当前记录id不存在，请重试")
	}
	maps := make(map[string]interface{})
	maps["type"] = s.Type
	maps["code"] = s.Code
	maps["description"] = s.Description
	err := models.DB().Model(&stockPool).Where("id = ? ", s.Id).Update(maps)
	if err != nil {
		logging.Info("stockpool.UpdateStockPool() Errors: ", err.Error)
		return err.Error
	}
	return nil
}

func GetEventsByIds(codes []int, objectType string) (map[int]template.UserEvent, error) {
	userEventModel := []template.UserEvent{}
	ret := make(map[int]template.UserEvent)
	err := models.DB().Table("u_user_event").Where("object_id in (?) ", codes).Where("object_type = ? ", objectType).Find(&userEventModel).Error
	if err != nil {
		logging.Info("user_stock_pool.GetIndustryNames() err:", err.Error())
		return ret, err
	}
	for _, value := range userEventModel {
		ret[value.ObjectID] = value
	}
	return ret, nil
}

func GetPreEventStr(userID string, code, searchType string) string {
	var Event struct {
		EventStr string `json:"event_str" gorm:"event_str"`
	}
	event := Event
	err := models.DB().Table("u_user_event").
		Joins("left join u_user_stock on u_user_stock.id = u_user_event.object_id").
		Where("u_user_stock.code = ? ", code).
		Where("u_user_event.user_id = ? ", userID).
		Where("object_type = ? ", searchType).
		Where("u_user_event.event_str != ? ", "").
		Order("u_user_event.updated_at desc").First(&event).Error
	if err != nil {
		return ""
	}
	return event.EventStr
}
