package home

import (
	"datacenter/models"
	"datacenter/models/spider"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"encoding/json"
	"github.com/pkg/errors"
	"time"
)

type InvestigationUCDTO struct {
	spider.InvestigationDTO
	UserID      int    // 用户ID
	ObjectID    int    // 事件ID
	EventStatus int    // 事件状态
	EventStr    string // 点评内容
	ObjectType  string // 对象类型
	EventType   string // 事件类型
	PictureLink string // 图片链接
	Description string // 图片链接
}

func (dto InvestigationUCDTO) DTO2VO() crud_service.VO {
	vo := InvestigationUCVO{
		ID:         dto.InvestigationDTO.Id,
		Code:       dto.Code,
		Name:       dto.Name,
		Title:      dto.Title,
		SourceLink: dto.SourceLink,
		CreatedAt:  util.TimePtr2String(dto.InvestigationDTO.PublishTime, util.YMDHMS),
		//PublishTime: util.TimePtr2String(dto.PublishTime, util.YMDHMS),
		UserId:      dto.UserID,
		ObjectId:    dto.ObjectID,
		EventStr:    dto.EventStr,
		EventStatus: dto.EventStatus,
	}
	vo.PictureLink = []ImgDTO{}
	_ = json.Unmarshal([]byte(dto.PictureLink), &vo.PictureLink)
	return vo
}

type InvestigationUCQO struct {
	//ID          int    `json:"id" form:"id" `                    //调研ID
	PublishTime *string `json:"publish_time" form:"publish_time"`
	UserID      int     `json:"user_id" form:"user_id"`           // 用户ID
	EventStatus string  `json:"event_status" form:"event_status"` // 事件状态
	Comment     *bool   `json:"comment" form:"comment"`
	Direction   bool    `json:"direction" form:"direction"`
	StartTime   string  `json:"start_time" form:"start_time"`
	EndTime     string  `json:"end_time" form:"end_time"`
}

type InvestigationUCVO struct {
	ID          int      `json:"id"`
	Code        string   `json:"code"`
	Name        string   `json:"name"`
	Title       string   `json:"title"`
	SourceLink  string   `json:"source_link"`
	CreatedAt   string   `json:"created_at"`
	PublishTime string   `json:"publish_time"`
	UserId      int      `json:"user_id"`
	ObjectId    int      `json:"object_id"`
	EventStr    string   `json:"event_str"`    // 点评内容
	EventStatus int      `json:"event_status"` // 事件状态
	PictureLink []ImgDTO `json:"picture_link"` //图片链接
}

func GetInvestigationList(qo *InvestigationUCQO) ([]crud_service.DTO, error) {
	dtos := make([]InvestigationUCDTO, 0)
	res := make([]crud_service.DTO, 0)
	dbs := models.DB().Table("p_investigation").
		Select("*").
		Joins("LEFT JOIN (SELECT * FROM u_user_event WHERE object_type=?) S ON p_investigation.id=S.object_id", "investigation").
		Joins("LEFT JOIN u_stock_industry I ON I.code=p_investigation.code").
		Joins("LEFT JOIN u_second_industry_user IU ON IU.second_industry=I.second_industry").
		Where("IU.deleted_at IS NULL")
	if qo.Direction {
		if qo.PublishTime != nil {
			dbs = dbs.Where("p_investigation.publish_time>?", qo.PublishTime)
		}
	} else {
		if qo.PublishTime != nil {
			dbs = dbs.Where("p_investigation.publish_time<?", qo.PublishTime)
		}
	}
	if qo.UserID > 0 {
		dbs = dbs.Where("IU.user_id=?", qo.UserID)
	} else {
		return nil, errors.New("非法用户ID")
	}
	if qo.EventStatus != "" {
		if qo.EventStatus == "1" {
			dbs = dbs.Where("event_status=?", qo.EventStatus)
		} else {
			dbs = dbs.Where("event_status=? OR event_status IS NULL", qo.EventStatus)
		}
	}
	if qo.StartTime != "" {
		dbs = dbs.Where("p_investigation.created_at>=?", qo.StartTime)
	}
	if qo.EndTime != "" {
		endTime, _ := time.ParseInLocation(util.YMD, qo.EndTime, time.Local)
		qo.EndTime = endTime.AddDate(0, 0, 1).Format(util.YMD)
		dbs = dbs.Where("p_investigation.created_at<?", qo.EndTime)
	}
	if qo.Comment != nil {
		if *qo.Comment {
			dbs = dbs.Where("S.event_str!=''")
		} else {
			dbs = dbs.Where("S.event_str='' OR S.event_str is NULL")
		}
	}
	err := dbs.Limit(20).Order("p_investigation.publish_time DESC").Find(&dtos).Error
	if err != nil {
		return nil, err
	}
	for _, dto := range dtos {
		res = append(res, dto)
	}
	return res, nil
}
