package home

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"fmt"
	"github.com/shopspring/decimal"
	"strconv"
	"time"
)

func GetUserListByCode(code string) []int {
	userList := make([]int, 0)
	err := models.DB().Raw(fmt.Sprintf("select distinct user_id from u_second_industry_user where second_industry in (select second_industry from u_stock_industry where code='%s') and deleted_at is null", code)).Pluck("user_id", &userList).Error
	if err != nil {
		logging.Error("根据code"+code+"获取用户id列表失败", err.Error())
		userList = []int{}
	}
	return userList
}

func GetCodeListByUserId(userId int) []string {
	codeList := make([]string, 0)
	err := models.DB().Raw(fmt.Sprintf("select distinct code from u_stock_industry where second_industry in (select second_industry from  u_second_industry_user where user_id=%d and deleted_at is null)", userId)).Pluck("code", &codeList).Error
	if err != nil {
		logging.Error("根据user_id"+strconv.Itoa(userId)+"获取code列表失败", err.Error())
		codeList = []string{}
	}
	return codeList
}

type NetProfitChangePushDTO struct {
	ID              int64           `json:"id"`
	Code            string          `json:"code"`
	Name            string          `json:"name"`
	Organization    string          `json:"organization"`
	Author          string          `json:"author"`
	Rate            decimal.Decimal `json:"rate"`
	PublishDate     time.Time       `json:"publish_date"`
	LatestNetProfit decimal.Decimal `json:"latest_net_profit"`
	AvgNetProfit    decimal.Decimal `json:"avg_net_profit"`
	AvgValue        decimal.Decimal `json:"avg_value"`
	Year            string          `json:"year"`
	CreatedAt       time.Time       `json:"created_at"`
	UpdatedAt       time.Time       `json:"updated_at"`
	Head            int             `json:"head"`
}

type NetProfitChangePushVO struct {
	ID              int64  `json:"id"`
	Code            string `json:"code"`
	Name            string `json:"name"`
	Organization    string `json:"organization"`
	Author          string `json:"author"`
	Rate            string `json:"rate"`
	PublishDate     string `json:"publish_date"`
	LatestNetProfit string `json:"latest_net_profit"`
	AvgNetProfit    string `json:"avg_net_profit"`
	AvgValue        string `json:"avg_value"`
	Year            string `json:"year"`
	CreatedAt       string `json:"created_at"`
	UpdatedAt       string `json:"updated_at"`
	Head            int    `json:"head"`
}

type NetProfitChangePushIO struct {
	Code            string          `json:"code"`
	Name            string          `json:"name"`
	Organization    string          `json:"organization"`
	Author          string          `json:"author"`
	PublishDate     string          `json:"publish_date"`
	LatestNetProfit decimal.Decimal `json:"latest_net_profit"`
	AvgNetProfit    decimal.Decimal `json:"avg_net_profit"`
	AvgValue        decimal.Decimal `json:"avg_value"`
	Year            string          `json:"year"`
	Rate            decimal.Decimal `json:"rate"`
}

type NetProfitChangePushQO struct {
	UserId int `json:"user_id" form:"user_id"`
	models.Paginate
}

func (dto NetProfitChangePushDTO) DTO2VO() crud_service.VO {
	return NetProfitChangePushVO{
		ID:              dto.ID,
		Code:            dto.Code,
		Name:            dto.Name,
		Organization:    dto.Organization,
		Author:          dto.Author,
		Rate:            dto.Rate.String(),
		CreatedAt:       dto.CreatedAt.Format(util.YMDHMS),
		UpdatedAt:       dto.UpdatedAt.Format(util.YMDHMS),
		PublishDate:     dto.PublishDate.Format(util.YMD),
		LatestNetProfit: dto.LatestNetProfit.String(),
		AvgNetProfit:    dto.AvgNetProfit.String(),
		AvgValue:        dto.AvgValue.String(),
		Year:            dto.Year,
		Head:            dto.Head,
	}
}

func (io *NetProfitChangePushIO) Insert() bool {
	dto := NetProfitChangePushDTO{}
	err := models.DB().Table("u_netprofit_change_push").
		Where("code=?", io.Code).
		Where("organization=?", io.Organization).
		Where("publish_date=?", io.PublishDate).First(&dto).Error
	if err != nil || dto.ID == 0 {
		err = models.DB().Table("u_netprofit_change_push").Create(io).Error
		if err != nil {
			logging.Error("插入NetProfitChangePush信息失败")
			return false
		}
		return true
	}
	return false
}

func GetList(qo *NetProfitChangePushQO) ([]crud_service.DTO, int, error) {
	dtos := make([]NetProfitChangePushDTO, 0)
	res := make([]crud_service.DTO, 0)
	cnt := 0
	codeList := GetCodeListByUserId(qo.UserId)
	dbs := models.DB().Table("u_netprofit_change_push").
		Joins("LEFT JOIN u_stock_industry on u_stock_industry.code=u_netprofit_change_push.code").
		Where("u_netprofit_change_push.code in (?)", codeList).
		Order("publish_date desc,u_netprofit_change_push.code asc,year asc")
	dbs.Count(&cnt)
	err := dbs.Offset((qo.PageNum - 1) * qo.PageSize).Limit(qo.PageSize).Find(&dtos).Error
	if err != nil {
		logging.Error("获取NetProfitChangePush列表失败", err.Error())
		return nil, 0, err
	}
	for _, dto := range dtos {
		res = append(res, dto)
	}
	return res, cnt, nil
}
