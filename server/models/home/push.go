package home

import (
	"time"
)

type StockPushRequest struct {
	UserID       int       `json:"user_id" form:"user_id"`
	PageNum      int       `json:"page_num" form:"page_num" binding:"required,gte=1"`
	PageSize     int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
	CodeName     string    `json:"code_name" form:"code_name"`
	Sart         string    `json:"start" form:"start"`
	End          string    `json:"end" form:"end"`
	SearchType   string    `json:"search_type" form:"search_type"`
	PushType     string    `json:"push_type" form:"push_type"`
}

// 行业管理   添加用户
type IndustryNamesEditRequest struct {
	UserId            int                  `json:"user_id" form:"user_id" binding:"required"`
	IndustryNames     []IndustryRequest    `json:"industry_names" form:"industry_names" binding:"required"`
}
type IndustryNamesDelRequest struct {
	UserId            int                  `json:"user_id" form:"user_id" binding:"required"`
}
type IndustryRequest struct {
	SecondIndustry     string     `json:"second_industry" form:"second_industry"`
	FirstIndustry      string     `json:"first_industry" form:"first_industry"`
	UserId             int        `json:"user_id" form:"user_id" binding:"required"`
	IndustryNames      string     `json:"industry_names" form:"industry_names"`
	IndustryID         int        `json:"industry_id" form:"industry_id"`
}

// 工作统计
type StaticsticRequest struct {
	UserID        int       `json:"user_id" form:"user_id"`
	PageSize      int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum       int       `json:"page_num" form:"page_num" binding:"required,gte=1"`
	SearchDate    time.Time `json:"date_time" form:"date_time"  time_format:"2006-01-02"`
}