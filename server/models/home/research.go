package home

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"strconv"
	"strings"
	"time"
)

type ResearchEventQO struct {
	ID          int    `json:"id" form:"id" `                    //公告ID
	UserID      int    `json:"user_id" form:"user_id"`           // 用户ID
	EventStatus string `json:"event_status" form:"event_status"` // 事件状态
	Comment     *bool  `json:"comment" form:"comment"`
	Direction   bool   `json:"direction" form:"direction"`
	StartTime   string `json:"start_time" form:"start_time"`
	EndTime     string `json:"end_time" form:"end_time"`

	// 二期优化
	Organaization string `json:"organization" form:"organization"`
	Keyword       string `json:"keyword" form:"keyword"`
	Code          string `json:"code" form:"code"`
	Type          string `json:"type" form:"type" binding:"required"` // 公司研报  重点券商  行业研报
	Level         string `json:"level" form:"level"`                  // 相关评级 默认为空 首次推荐 first 买入 buy 增持 increase 中性 middle 减持 decrease 卖出 sale 上调评级 increase_level 下调评级 decrease_level
}

type ResearchReportDTO struct {
	models.Simple
	Code             string     `json:"code"`
	SeedId           int        `json:"seed_id"`
	Name             string     `json:"name"`
	Type             string     `json:"type"`
	Organization     string     `json:"organization"`
	Author           string     `json:"author"`
	Title            string     `json:"title"`
	Level            string     `json:"level"`
	LevelChange      string     `json:"level_change"`
	LevelCalc        string     `json:"level_calc"`
	Heat             int        `json:"heat"`
	Industry         string     `json:"industry"`
	SourceLink       string     `json:"source_link"`
	Abstract         string     `json:"abstract"`
	Content          string     `json:"content"`
	PublishDate      *time.Time `json:"publish_date"`
	Recommend        int        `json:"recommend"`
	RecommendUserId  string     `json:"recommend_user_id"`
	Parent           int        `json:"parent"`
	PdfFirstCoverage int        `json:"pdf_first_coverage"`
	PdfRaise         int        `json:"pdf_raise"`
	CatchTime        *time.Time `json:"catch_time"`
	SpiderTime       *time.Time `json:"spider_time"`
}

func (dto ResearchReportDTO) TableName() string {
	return "u_research_report"
}

type ResearchEventDTO struct {
	Id               int        `json:"id"`
	CreatedAt        time.Time  `json:"created_at"`
	UpdatedAt        time.Time  `json:"-"`
	Code             string     `json:"code"`
	SeedId           int        `json:"seed_id"`
	Name             string     `json:"name"`
	Type             string     `json:"type"`
	Organization     string     `json:"organization"`
	Author           string     `json:"author"`
	Title            string     `json:"title"`
	Level            string     `json:"level"`
	LevelChange      string     `json:"level_change"`
	LevelCalc        string     `json:"level_calc"`
	Heat             int        `json:"heat"`
	Industry         string     `json:"industry"`
	SourceLink       string     `json:"source_link"`
	Abstract         string     `json:"abstract"`
	Content          string     `json:"content"`
	PublishDate      *time.Time `json:"publish_date"`
	Recommend        int        `json:"recommend"`
	RecommendUserId  string     `json:"recommend_user_id"`
	Parent           int        `json:"parent"`
	PdfFirstCoverage int        `json:"pdf_first_coverage"`
	PdfRaise         int        `json:"pdf_raise"`
	CatchTime        *time.Time `json:"catch_time"`
	SpiderTime       *time.Time `json:"spider_time"`
	UserID           int        // 用户ID
	ObjectID         int        // 事件ID
	EventStatus      int        // 事件状态
	EventStr         string     // 点评内容
	ObjectType       string     // 对象类型
	EventType        string     // 事件类型
	PictureLink      string     // 图片链接

	RisingSpace              *string  `json:"rising_space"` //上涨空间
	LevelStatus              string   `json:"level_status"` //评级变动的箭头，向上或者向下

	ExpectedNetProfitRaise string  `json:"expected_net_profit_raise"` // 相同券商净利润对比
	Head                   int     `json:"head"`
}

type ResearchEventVO struct {
	Id           int      `json:"id"`
	Code         string   `json:"code"`
	Name         string   `json:"name"`
	Type         string   `json:"type"`
	Organization string   `json:"organization"`
	Author       string   `json:"author"`
	Title        string   `json:"title"`
	Level        string   `json:"level"`
	SourceLink   string   `json:"source_link"`
	Abstract     string   `json:"abstract"`
	Content      string   `json:"content"`
	CreatedAt    string   `json:"created_at"`
	UserId       int      `json:"user_id"`
	ObjectId     int      `json:"object_id"`
	EventStr     string   `json:"event_str"`    // 点评内容
	EventStatus  int      `json:"event_status"` // 事件状态
	PictureLink  []ImgDTO `json:"picture_link"` //图片链接

	RisingSpace              *string  `json:"rising_space"` //上涨空间
	PdfFirstCoverage         int     `json:"pdf_first_coverage"`
	LevelStatus              string   `json:"level_status"` //评级变动的箭头，向上或者向下
	PdfRaise                 int        `json:"pdf_raise"`
	ExpectedNetProfitRaise string  `json:"expected_net_profit_raise"` // 相同券商净利润对比
	Head                   int     `json:"head"`
}

func (dto ResearchEventDTO) DTO2VO() crud_service.VO {
	//上涨空间,内部排序
	if dto.RisingSpace != nil {
		sortFieldArray := strings.Split(*dto.RisingSpace, ",")
		//排序,string slice排序
		sortFieldFloat64Array := util.SortStrSlice(&sortFieldArray, "asc")
		//转化成字符串
		sortFieldStr := ""
		for _, v := range sortFieldFloat64Array {
			sortFieldStr += fmt.Sprintf("%.4f", v) + ","
		}
		trimSortFieldStr := strings.Trim(sortFieldStr, ",")
		dto.RisingSpace = &trimSortFieldStr
	}
	vo := ResearchEventVO{
		Id:           dto.Id,
		Code:         dto.Code,
		Name:         dto.Name,
		Type:         dto.Type,
		Organization: dto.Organization,
		Author:       dto.Author,
		Title:        dto.Title,
		Level:        dto.Level,
		SourceLink:   dto.SourceLink,
		Abstract:     dto.Abstract,
		Content:      dto.Content,
		CreatedAt:    util.TimePtr2String(&dto.CreatedAt, util.YMDHMS),
		UserId:       dto.UserID,
		ObjectId:     dto.ObjectID,
		EventStr:     dto.EventStr,
		EventStatus:  dto.EventStatus,

		RisingSpace:            dto.RisingSpace,
		PdfFirstCoverage:       dto.PdfFirstCoverage,
		LevelStatus:            dto.LevelCalc,
		PdfRaise:               dto.PdfRaise,
		ExpectedNetProfitRaise: dto.ExpectedNetProfitRaise,
		Head:                   dto.Head,
	}
	_ = json.Unmarshal([]byte(dto.PictureLink), &vo.PictureLink)
	return vo
}

func GetMyResearchList(qo *ResearchEventQO) ([]crud_service.DTO, error) {
	dto := make([]ResearchEventDTO, 0)
	res := make([]crud_service.DTO, 0)
	var err error
	// qojson, _ := json.Marshal(qo)
	// rediskey := "my_research_list:" + strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(string(qojson), ":", "_"), "\"", ""), ",", "")
	// list, err := gredis.Get(rediskey)
	// if err == nil {
	// 	if len(list) > 2 {
	// 		err = json.Unmarshal(list, &dto)
	// 		if err == nil {
	// 			for _, v := range dto {
	// 				res = append(res, v)
	// 			}
	// 		}
	// 		return res, err
	// 	}
	// }
	// dbs := models.DB().Table(fmt.Sprintf("((SELECT u_research_report.*,S.event_str,S.event_status,S.picture_link FROM `u_research_report` "+
	// 	"LEFT JOIN (SELECT * FROM u_user_event WHERE object_type='research') S ON u_research_report.id=S.object_id "+
	// 	"LEFT JOIN u_stock_industry I ON I.code=u_research_report.code AND u_research_report.code!='' "+
	// 	"LEFT JOIN u_industry_user IU ON IU.actual_industry=I.actual_industry "+
	// 	"WHERE `u_research_report`.`deleted_at` IS NULL AND ((IU.user_id=%v)) AND IU.deleted_at IS NULL "+
	// 	"GROUP BY u_research_report.code,u_research_report.organization,u_research_report.title,u_research_report.`level` ORDER BY u_research_report.id DESC LIMIT 20)"+
	// 	"UNION ALL "+
	// 	"(SELECT u_research_report.*,S.event_str,S.event_status,S.picture_link FROM `u_research_report` "+
	// 	"LEFT JOIN (SELECT * FROM u_user_event WHERE object_type='research') S ON u_research_report.id=S.object_id "+
	// 	"LEFT JOIN (SELECT * FROM u_stock_industry GROUP BY first_industry) I ON I.first_industry=u_research_report.industry AND u_research_report.code='' "+
	// 	"LEFT JOIN u_industry_user IU ON IU.actual_industry=I.actual_industry "+
	// 	"WHERE `u_research_report`.`deleted_at` IS NULL AND ((IU.user_id=%v)) AND IU.deleted_at IS NULL "+
	// 	"GROUP BY u_research_report.organization,u_research_report.title,u_research_report.`level`  ORDER BY u_research_report.id DESC LIMIT 20)"+
	// 	") as T ", qo.UserID, qo.UserID))

	dbs := models.DB().Table("u_research_report")

	if qo.Type == "公司研报" || qo.Type == "重点券商" {
		dbs = dbs.Select("u_research_report.*,S.event_str,S.event_status,S.picture_link,I.head").
			Joins("LEFT JOIN (SELECT * FROM u_user_event WHERE object_type=? and user_id = ? ) S ON u_research_report.id=S.object_id", "research", qo.UserID)
		dbs = dbs.Joins("LEFT JOIN u_stock_industry I ON I.code=u_research_report.code AND u_research_report.code!='' ").
			Joins("LEFT JOIN u_second_industry_user IU ON IU.second_industry=I.second_industry ")
		if qo.Type == "公司研报" {
			dbs = dbs.Where("u_research_report.type = ? OR u_research_report.type = ?", "个股", "重点券商")
		} else {
			dbs = dbs.Where("u_research_report.type = ?", qo.Type)
		}
		dbs = dbs.Where("IU.user_id= ? AND IU.deleted_at IS NULL ", qo.UserID)
	} else if qo.Type == "行业研报" {
		dbs = dbs.Select("u_research_report.*,S.event_str,S.event_status,S.picture_link").
			Joins("LEFT JOIN (SELECT * FROM u_user_event WHERE object_type=? and user_id = ? ) S ON u_research_report.id=S.object_id", "research", qo.UserID)
		// 获取行业查询条件
		industryWhere := GetUserFirstIndustry(qo.UserID)
		if industryWhere != "" {
			dbs = dbs.Where(industryWhere)
		}
		dbs = dbs.Where("u_research_report.code=''")
		// dbs = dbs.Joins("LEFT JOIN (SELECT * FROM u_stock_industry GROUP BY first_industry) I ON I.first_industry=u_research_report.industry AND u_research_report.code='' ").Joins("LEFT JOIN u_second_industry_user IU ON IU.first_industry=I.first_industry").Group("u_research_report.id")
	} else {
		dbs = dbs.Select("u_research_report.*,S.event_str,S.event_status,S.picture_link").
			Joins("LEFT JOIN (SELECT * FROM u_user_event WHERE object_type=? and user_id = ? ) S ON u_research_report.id=S.object_id", "research", qo.UserID)
	}
	dbs = dbs.Where("`u_research_report`.`deleted_at` IS NULL ").
		Where("`u_research_report`.`parent` = ? ", 0)
	dbs = getWhere(dbs, qo.Level)
	//.
	//Joins("LEFT JOIN (SELECT * FROM u_user_event WHERE object_type=?) S ON u_research_report.id=S.object_id", "research").
	//Joins("LEFT JOIN u_stock_industry I ON I.code=u_research_report.code").
	//Joins("LEFT JOIN u_industry_user IU ON IU.actual_industry=I.actual_industry")
	if qo.Direction {
		dbs = dbs.Where("u_research_report.id>?", qo.ID)
	} else {
		if qo.ID > 0 {
			dbs = dbs.Where("u_research_report.id<?", qo.ID)
		}
	}
	if qo.Keyword != "" {
		dbs = dbs.Where("u_research_report.title like ?", "%"+qo.Keyword+"%")
	}
	if qo.Organaization != "" {
		dbs = dbs.Where("u_research_report.organization = ?", qo.Organaization)
	}
	if qo.EventStatus != "" {
		if qo.EventStatus == "1" {
			dbs = dbs.Where("S.event_status=?", qo.EventStatus)
		} else {
			dbs = dbs.Where("S.event_status=? OR S.event_status IS NULL", qo.EventStatus)
		}
	}
	if qo.StartTime != "" {
		dbs = dbs.Where("u_research_report.created_at >= ? ", qo.StartTime)
	}
	if qo.EndTime != "" {
		endTime, _ := time.ParseInLocation(util.YMD, qo.EndTime, time.Local)
		qo.EndTime = endTime.AddDate(0, 0, 1).Format(util.YMD)
		dbs = dbs.Where("u_research_report.created_at<?", qo.EndTime)
	}
	if qo.Comment != nil {
		if *qo.Comment {
			dbs = dbs.Where("S.event_str!=''")
		} else {
			dbs = dbs.Where("S.event_str='' OR S.event_str is NULL")
		}
	}
	if qo.Code != "" {
		dbs = dbs.Where("u_research_report.name like ? or u_research_report.code like ?", qo.Code, qo.Code)
	}
	err = dbs.Limit(20).Order("u_research_report.created_at DESC").Find(&dto).Error
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		// 获取上涨空间
		reportData := ResearchEventDTO{}
		err = models.DB().Table("u_research_report_data").Where("report_id = ? ", v.Id).First(&reportData).Error
		if err == nil {
			v.RisingSpace = reportData.RisingSpace
		}
		v.ExpectedNetProfitRaise = getExpectedNetProfit(v)
		res = append(res, v)
	}
	//gredis.Set(rediskey, dto, 30)
	return res, nil
}

func getWhere(dbs *gorm.DB, level string) *gorm.DB {

	// if req.Level == "上调评级" {
	//			dbs = dbs.Where("a.level_calc = ?", "up")
	//		} else if req.Level == "下调评级" {
	//			dbs = dbs.Where("a.level_calc = ?", "down")
	//		} else if req.Level == "首次推荐" {
	//			dbs = dbs.Where("a.level_change = ?", "首次")
	//		} else {
	//			dbs = dbs.Where("a.level = ?", req.Level)
	//		}
	//相关评级 默认为空 首次推荐 first 买入 buy 增持 increase 中性 middle 减持 decrease 卖出 sale 上调评级 increase_level 下调评级 decrease_level
	switch level {
	case "first":
		dbs = dbs.Where("u_research_report.level_change = ? ", "首次")
	case "buy":
		dbs = dbs.Where("u_research_report.level = ? ", "买入")
	case "increase":
		dbs = dbs.Where("u_research_report.level = ? ", "增持")
	case "middle":
		dbs = dbs.Where("u_research_report.level = ? ", "中性")
	case "decrease":
		dbs = dbs.Where("u_research_report.level = ? ", "减持")
	case "sale":
		dbs = dbs.Where("u_research_report.level = ? ", "卖出")
	case "increase_level":
		dbs = dbs.Where("u_research_report.level_calc = ? ", "up")
	case "decrease_level":
		dbs = dbs.Where("u_research_report.level_calc = ? ", "down")
	}
	return dbs
}


// 获取当前研报 股票+机构的
func getExpectedNetProfit(ret ResearchEventDTO) string {
	// 3. 获取当前记录的最近一条
	type Report struct {
		ID                int             `json:"id" gorm:"column:report_id"`
		ExpectedNetProfit decimal.Decimal `json:"expected_net_profit" gorm:"column:expected_net_profit"`
		ExpectedYear      int             `json:"expected_year" gorm:"column:expected_year"`
	}
	// 获取当前记录的期望利润
	reportData := []Report{}
	err := models.DB().Table("u_research_report_data").Where("u_research_report_data.expected_net_profit is not null ").Where("report_id = ? ", ret.Id).Find(&reportData).Error
	if err != nil {
		return ""
	}
	reportList := []Report{}
	err = models.DB().Table("u_research_report_data").
		Joins("left join u_research_report on u_research_report_data.report_id=u_research_report.id ").
		Where("u_research_report.created_at < ? ", ret.CreatedAt).
		Where("u_research_report.code = ? ", ret.Code).
		Where("u_research_report.organization = ? ", ret.Organization).
		Where("u_research_report_data.expected_net_profit is not null ").
		Where("u_research_report_data.report_id != ? ", ret.Id).
		Where("u_research_report.parent != 1 ").
		Order("u_research_report_data.report_id desc").Limit(3).Find(&reportList).Error
	if len(reportList) < 1 {
		return ""
	}
	lastReport := make(map[int]Report)
	preID := 0
	for _, value := range reportList {
		if preID < 1 {
			preID = value.ID
			lastReport[value.ExpectedYear] = value
		}
		if value.ID != preID {
			continue
		} else {
			lastReport[value.ExpectedYear] = value
		}
	}
	retStr := ""
	nowYear := time.Now().Year()
	rate := decimal.Decimal{}
	newReport := make(map[int]Report)
	for _, value := range reportData {
		newReport[value.ExpectedYear] = value
	}

	if !util.IsEmpty(newReport[nowYear]) && !util.IsEmpty(lastReport[nowYear]) && !lastReport[nowYear].ExpectedNetProfit.IsZero() {
		rate = newReport[nowYear].ExpectedNetProfit.Sub(lastReport[nowYear].ExpectedNetProfit).Div(lastReport[nowYear].ExpectedNetProfit.Abs())
		if rate.Sub(decimal.NewFromFloat(0.10)).Sign() > 0 {
			return "up"
		}
		if rate.Sub(decimal.NewFromFloat(-0.10)).Sign() < 0 {
			return "down"
		}
	}
	if !util.IsEmpty(newReport[nowYear+1]) && !util.IsEmpty(lastReport[nowYear+1]) && !lastReport[nowYear+1].ExpectedNetProfit.IsZero() {
		nowYear = nowYear + 1
		rate = newReport[nowYear].ExpectedNetProfit.Sub(lastReport[nowYear].ExpectedNetProfit).Div(lastReport[nowYear].ExpectedNetProfit.Abs())
		if rate.Sub(decimal.NewFromFloat(0.10)).Sign() > 0 {
			return "up"
		}
		if rate.Sub(decimal.NewFromFloat(-0.10)).Sign() < 0 {
			return "down"
		}
	}
	if !util.IsEmpty(newReport[nowYear+2]) && !util.IsEmpty(newReport[nowYear+2]) && !lastReport[nowYear+2].ExpectedNetProfit.IsZero() {
		nowYear = nowYear + 2
		rate = newReport[nowYear].ExpectedNetProfit.Sub(lastReport[nowYear].ExpectedNetProfit).Div(lastReport[nowYear].ExpectedNetProfit.Abs())
		if rate.Sub(decimal.NewFromFloat(0.10)).Sign() > 0 {
			return "up"
		}
		if rate.Sub(decimal.NewFromFloat(-0.10)).Sign() < 0 {
			return "down"
		}
	}
	if !util.IsEmpty(newReport[nowYear-1]) && !util.IsEmpty(newReport[nowYear-1]) && !lastReport[nowYear].ExpectedNetProfit.IsZero() {
		nowYear = nowYear - 1
		rate = newReport[nowYear].ExpectedNetProfit.Sub(lastReport[nowYear].ExpectedNetProfit).Div(lastReport[nowYear].ExpectedNetProfit.Abs())
		if rate.Sub(decimal.NewFromFloat(0.10)).Sign() > 0 {
			return "up"
		}
		if rate.Sub(decimal.NewFromFloat(-0.10)).Sign() < 0 {
			return "down"
		}
	}
	return retStr
}
func GetUserFirstIndustry(userID int) string {
	type IndustryName struct {
		IndustryName string `json:"industry_name" gorm:"cloumn:industry_name"`
	}
	industryNames := []IndustryName{}
	names := []string{}
	err := models.DB().Table("u_second_industry_user").
		Select("distinct first_industry as industry_name").
		Where("user_id = ? ", userID).Where("deleted_at is null ").Find(&industryNames).Error
	if err != nil {
		logging.Info("userEvent.GetUserIndustry err:", err.Error)
		return ""
	}
	for _, value := range industryNames {
		if value.IndustryName != "" {
			names = append(names, value.IndustryName)
		}
	}
	type Industry struct {
		Name string `json:"name" gorm:"cloumn:name"`
		Id   int    `json:"id" gorm:"cloumn:id"`
	}
	industry := []Industry{}
	err = models.DB().Table("u_first_industry").
		Where("name in (?) ", names).Where("deleted_at is null ").Find(&industry).Error
	if err != nil {
		logging.Info("userEvent.GetUserIndustry err:", err.Error)
		return ""
	}
	where := ""
	for i, v := range industry {
		where = where + fmt.Sprintf("(u_research_report.first_ids like '%v')",
			"%#"+strconv.Itoa(v.Id)+"#%")
		if i < len(industry)-1 {
			where = where + " OR "
		}
	}
	return where
}
