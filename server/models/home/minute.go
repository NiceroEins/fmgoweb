package home

import (
	"bytes"
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"fmt"
	"github.com/shopspring/decimal"
	"sync"
	"time"
)
var (
	amounKey                  = "stock:amount"
	closeKey                  = "stock:tick"
	openKey                   = "stock:open"
	lastPriceKey              = "stock:last_close"
	highKey                   = "stock:high"
	lowKey                    = "stock:low"
	volumeKey                 = "stock:volume"
	changeKey                 = "stock:change"
)
type IndustryStockMinute struct {
	Code                   string              `gorm:"column:code" json:"code"`                               // 股票名称
	Close                  decimal.Decimal     `gorm:"column:close" json:"close"`                             // 现价
	Open                   decimal.Decimal     `gorm:"column:open" json:"open"`                               // 开盘价
	High                   decimal.Decimal     `gorm:"column:high" json:"high"`                               // 最高价
	Low                    decimal.Decimal     `gorm:"column:low" json:"low"`                                 // 最低价
	Volumne                decimal.Decimal     `gorm:"column:volume" json:"volume"`                           // 成交量
	Amount                 decimal.Decimal     `gorm:"column:amount" json:"amount"`                           // 成交额
	Change                 decimal.Decimal     `gorm:"column:change" json:"change"`                           // 涨幅
	Pctchange              decimal.Decimal     `gorm:"column:pctchange" json:"pctchange"`                     // 涨幅比例
	NowTime                string              `gorm:"column:time" json:"time"`
}
func StoreIndustryStockMinute()  {
	// 获取行业对应stock
	stocks := []IndustryNameCode{}
	err := models.DB().Table("b_stock_names").Find(&stocks).Error
	if err != nil {
		logging.Info("store second_industry info err :",err)
		return
	}
	// 获取行业对应stock
	if len(stocks) < 1 {
		logging.Info("store second_industry info err :",len(stocks))
		return
	}
	// 15:00最后一条数据延迟5s
	if time.Now().Hour() == 15 {
		time.Sleep(time.Second*5)
	}
	// 获取全部数据
	res := []IndustryStockMinute{}
	wg := sync.WaitGroup{}
	wg.Add(len(stocks))
	for i,_ := range stocks {
		go func(idx int) {
			v := &stocks[idx]
			industyStock := IndustryStockMinute{
				Code: v.Code,
				NowTime: time.Now().Format(util.YMDHM),
			}
			err := getValueFromRedisStockMinute(&industyStock)
			if err != nil {
				logging.Info("store.StoreIndustryStockMinute() err:", err.Error())
				return
			}
			res = append(res,industyStock)
			wg.Done()
		}(i)
	}
	wg.Wait()
	code := []string{}
	for _,value := range res {
		code = append(code,value.Code)
	}
	retCode := []string{}
	for _,v := range stocks {
		if util.ContainsAny(code,v.Code) {
			continue
		}
		retCode = append(retCode,v.Code)
		industyStock := IndustryStockMinute{
			Code: v.Code,
			NowTime: time.Now().Format(util.YMDHM),
		}
		err := getValueFromRedisStockMinute(&industyStock)
		if err != nil {
			logging.Info("store.StoreIndustryStockMinute() err:", err.Error())
		}
		res = append(res,industyStock)
	}
	var buffer bytes.Buffer
	sql := "insert into `u_stock_minute` (`code`,`time`,`open`,`close`,`high`,`low`,`volume`,`amount`,`change`,`pctchange`) values "
	if _, err := buffer.WriteString(sql); err != nil {
		return
	}
	for i, v := range res {
		if i == len(res)-1 {
			buffer.WriteString(fmt.Sprintf("('%v','%v','%v',%v,%v,%v,%v,%v,%v,%v);",
				 v.Code, v.NowTime,v.Open,v.Close,v.High,v.Low, v.Volumne,v.Amount,v.Change,v.Pctchange,))
		} else {
			buffer.WriteString(fmt.Sprintf("('%v','%v','%v',%v,%v,%v,%v,%v,%v,%v),",
				v.Code, v.NowTime,v.Open,v.Close,v.High,v.Low, v.Volumne,v.Amount,v.Change,v.Pctchange,))
		}
	}
	err = models.DB().Exec(buffer.String()).Error
	if err != nil {
		logging.Info("save stock minute err",err)
		return
	}
}

func getValueFromRedisStockMinute(dto *IndustryStockMinute) error {
	var (
		err       error
		id        = dto.Code
		lastClose decimal.NullDecimal
	)
	err = dto.Amount.Scan(getRedisValue(amounKey, id))  // 成交量
	if err != nil {
		logging.Info("industry.getValueFromRedis() amount err:", err.Error())
		return err
	}
	err = dto.Close.Scan(getRedisValue(closeKey, id))  // 现价
	if err != nil {
		logging.Info("industry.getValueFromRedis() close err:", err.Error())
		return err
	}
	err = lastClose.Scan(getRedisValue(lastCloseKey, id))  // 昨收价
	if err != nil {
		lastClose.Valid = false
		logging.Info("industry.getValueFromRedis() lastClose err:", err.Error())
		return err
	}
	dto.Change = dto.Close.Sub(lastClose.Decimal) // 涨跌
	err = dto.Pctchange.Scan(getRedisValue(changeKey, id)) // 涨跌幅
	if err != nil {
		logging.Info("industry.getValueFromRedis() change err:", err.Error())
		return err
	}
	err = dto.Open.Scan(getRedisValue(openKey, id))  // 开盘价
	if err != nil {
		logging.Info("industry.getValueFromRedis() open err:", err.Error())
		return err
	}
	err = dto.High.Scan(getRedisValue(highKey, id))  // 最高价
	if err != nil {
		logging.Info("industry.getValueFromRedis() high err:", err.Error())
		return err
	}
	err = dto.Low.Scan(getRedisValue(lowKey, id))  // 最低价
	if err != nil {
		logging.Info("industry.getValueFromRedis() low err:", err.Error())
		return err
	}
	err = dto.Volumne.Scan(getRedisValue(volumeKey, id))  // 最低价
	if err != nil {
		logging.Info("industry.getValueFromRedis() volume err:", err.Error())
		return err
	}
	return nil
}
