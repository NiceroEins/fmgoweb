package home

import (
	"datacenter/models"
	"datacenter/models/message"
	"datacenter/models/template"
	"datacenter/pkg/util"
	"datacenter/pkg/websocket"
	"encoding/json"
	"strconv"
)

type StockUserEvent struct {
	models.Simple
	ID          int
	UserID      int    // 用户ID
	ObjectID    int    // 事件ID
	EventStatus int    // 事件状态
	EventStr    string // 点评内容
	ObjectType  string // 对象类型
	EventType   string // 事件类型
	PictureLink string // 图片链接
	Description string // 图片链接
}

type HistoryEvent struct {
	models.Simple
	ID          int
	UserID      int           // 用户ID
	Code        string        //股票code码
	EventStr    string        // 点评内容
	PictureLink string        // 图片链接
	Description string        // 图片链接

	PageSize    int
	PageNum     int
}

func (StockUserEvent) TableName() string {
	return "u_user_event"
}

type User struct {
	UserID int `json:"user_id" gorm:"column:user_id"`
}
func push(m map[string]interface{}, userId int, msgType message.MessageType) {
	ws := websocket.GetInstance()
	bt, _ := json.Marshal(m)
	msg := message.ParseChanelMsg(msgType, "system", strconv.Itoa(userId), string(bt), "home", true)
	ws.SysSend(msg)
}

func PushMsg(userId int) {
	data := template.AssmblePushMsg(userId)
	push(map[string]interface{}{"data": data}, userId, message.Message_User_Center)
}

func PushByCode(m map[string]interface{}, code, desc string) {
	// 根据code 获取 user_id
	// 这里的逻辑是 推送给这个票对应的行业的用户 和是自选股里面的 用户
	userModel := []User{}
	err := models.DB().Table("u_stock_industry").
		Select("distinct u_second_industry_user.user_id").
		Joins("left join u_second_industry_user on u_stock_industry.second_industry = u_second_industry_user.second_industry").
		Where("u_stock_industry.code = ?", code).Where("u_second_industry_user.deleted_at is null ").Find(&userModel).Error
    userCodeModel := []User{}
	codeErr := models.DB().Table("u_user_stock").
		Select("distinct u_user_stock.user_id").
		Where("u_user_stock.code = ?", code).
		Where("u_user_stock.deleted_at is null ").
		Where("type = ? ","stock_pool").Find(&userCodeModel).Error
	if err != nil || codeErr != nil{
		return
	}
	userIds := []string{}
	for _,value := range userCodeModel {
		ws := websocket.GetInstance()
		bt, _ := json.Marshal(m)
		msg := message.ParseChanelMsg(message.Message_Stock_Pool, "system", strconv.Itoa(value.UserID), string(bt), "", true)
		ws.SysSend(msg)
		userIds = append(userIds,strconv.Itoa(value.UserID))
	}
	for _,value := range userModel {
		//ctx, ok := util.GetString(m, "content")
		sp := StockPool{
			UserID:      value.UserID,
			Code:        code,
			Type:        "personal_stock",
			Description: desc,
		}
		sp.Add()

		if util.ContainsAny(userIds,strconv.Itoa(value.UserID)) {
			continue
		}
		ws := websocket.GetInstance()
		bt, _ := json.Marshal(m)
		msg := message.ParseChanelMsg(message.Message_Personal_Stock, "system", strconv.Itoa(value.UserID), string(bt), "", true)
		ws.SysSend(msg)
	}
}

func (u *StockUserEvent) Add() error {
	event := template.UserEvent{
		UserID:      u.UserID,
		ObjectID:    u.ObjectID,
		EventStr:    u.EventStr,
		ObjectType:  u.ObjectType,
		EventStatus: u.EventStatus,
		PictureLink: u.PictureLink,
		EventType:   u.EventType,
	}
	if err := template.CreateUserEvent(&event); err != nil {
		return err
	}
	return nil
}

func (u *StockUserEvent) Update() error {
	maps := make(map[string]interface{})
	maps["user_id"] = u.UserID
	maps["object_id"] = u.ObjectID
	maps["event_str"] = u.EventStr
	maps["object_type"] = u.ObjectType
	maps["event_type"] = u.EventType
	maps["event_status"] = u.EventStatus
	maps["picture_link"] = u.PictureLink
	if err := template.UpdateUserEvent(u.ID, maps); err != nil {
		return err
	}
	return nil
}

func (u *StockUserEvent) Exists() (template.UserEvent, error) {
	maps := make(map[string]interface{})
	maps["user_id"] = u.UserID
	maps["object_id"] = u.ObjectID
	maps["object_type"] = u.ObjectType
	userEvent, err := template.GetUserEvent(maps)
	return userEvent, err
}

func (u *HistoryEvent) GetHistory()([]template.HistroyEvent, error) {
	maps := make(map[string]interface{})
	maps["user_id"] = u.UserID
	maps["code"] = u.Code
	userEvent, err := template.GetHistroyEvent(maps)
	return userEvent, err
}

func (u *HistoryEvent) Count() (int, error) {
	maps := make(map[string]interface{})
	maps["user_id"] = u.UserID
	maps["code"] = u.Code
	return template.GetHistroyEventTotal(maps)
}
