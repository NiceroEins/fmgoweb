package ipo

import (
    "datacenter/models"
    "datacenter/pkg/logging"
)

type IpoRelationshipRequest struct {
    Name string `json:"id" form:"name" binding:"required"`
}

type IpoRelationshipResponse struct {
    Data UIpoEquity
    IsDelete    bool
    Realtionship []SimpleRelationShip
}

type SimpleRelationShip struct {
    Name           string    `json:"name"`   // 公司名称
    StockRatio     float64   `json:"stock_ratio"`  // 股票占比
    Level          int       `json:"level"`  // 第几个元素
    Id             int       `json:"id"`  // 主键Id
}

type UIpoEquity struct {
    ID          int        `gorm:"primary_key;column:id;type:int(10) unsigned;not null" json:"-"`
    Name        string     `gorm:"column:company" json:"name"`
    Cid         int        `gorm:"column:cid" json:"cid"`
    Children    []*UIopEquityRelationship `json:"children"`
}

func (UIpoEquity) TableName() string {
    return "u_iop_equity"
}

type UIopEquityRelationship struct {
    ID          int       `gorm:"primary_key;column:id;type:int(10) unsigned;not null" json:"id"`
    IopEquityID int       `gorm:"column:iop_equity_id;type:int(11)" json:"iop_equity_id"`  // 主键id(u_iop_equity的主键，3 级不存)
    Name        string    `gorm:"column:name;type:varchar(255)" json:"name"`               // 名称
    StockRatio  float64   `gorm:"column:stock_ratio;type:varchar(255)" json:"stock_ratio"` // 股票占比
    Money       float64   `gorm:"column:money;type:varchar(255)" json:"money"`             // 金额(元)
    NameType    int       `gorm:"column:name_type;type:varchar(255)" json:"name_type"`     //  0 不是上市公司,1 上市公司,2股东名字,3港股
    ParentID    int       `gorm:"column:parent_id;type:int(11)" json:"-"`
    Children    []*UIopEquityRelationship `json:"children"`
}

func (UIopEquityRelationship) TableName() string {
    return "u_iop_equity_relationship"
}

func QueryIPOEquityRelationship(name string) (res IpoRelationshipResponse,err error){
    var data UIpoEquity
    var return_data IpoRelationshipResponse
    return_data.IsDelete = false
    db := models.DB()
    if err := db.Model(&UIpoEquity{}).Where("company = ?", name).First(&data).Error; err != nil {
        _ = Insert(name)
        logging.Error("QueryIPOEquityRelationship Query UIpoEquity Errors: ", err.Error())
        return return_data, err
    }
    if data.Cid  == 0 {
        return_data.IsDelete = true
        return return_data,nil
    }
    childAll := queryChild(data.ID, 1)
    realtionship := make([]SimpleRelationShip,0)
    realtionship =  append(realtionship,SimpleRelationShip{Name: data.Name,StockRatio: 0.00,Level: 0})
    for _,value := range childAll {
        if len(value.Children) == 0 && value.NameType == 6{
            realtionship =  append(realtionship,SimpleRelationShip{Name: value.Name,StockRatio: value.StockRatio,Level: 1,Id: data.ID})
        }
        value.Children = queryChild(value.ID,2)
        for _,item := range value.Children {
            if len(item.Children) == 0 && item.NameType == 6{
                realtionship =  append(realtionship,SimpleRelationShip{Name: value.Name,StockRatio: value.StockRatio,Level: 1,Id: value.ID})
                realtionship =  append(realtionship,SimpleRelationShip{Name: item.Name,StockRatio: item.StockRatio,Level: 2,Id: value.ID})
            }
            item.Children  = queryChild(item.ID,3)
            for _,child := range item.Children {
                if len(child.Children) == 0 && child.NameType == 6{
                    realtionship =  append(realtionship,SimpleRelationShip{Name: value.Name,StockRatio: value.StockRatio,Level: 1,Id: item.ID})
                    realtionship =  append(realtionship,SimpleRelationShip{Name: item.Name,StockRatio: item.StockRatio,Level: 2,Id: item.ID})
                    realtionship =  append(realtionship,SimpleRelationShip{Name: child.Name,StockRatio: child.StockRatio,Level: 3,Id: item.ID})
                }
            }
        }
    }
    data.Children = childAll
    return_data.Data = data
    return_data.Realtionship = realtionship
    return return_data,nil
}

func queryChild(id,level int) []*UIopEquityRelationship {
    var data []*UIopEquityRelationship
    db := models.DB().Model(&UIopEquityRelationship{})
    switch {
    case level == 1:
        db = db.Where("iop_equity_id = ?", id)
    case level > 1:
        db = db.Where("iop_equity_id = 0").Where("parent_id = ?", id)
    default:
        logging.Error("ipo.queryChild() Invalid level: ", level)
        return data
    }
    if err := db.Find(&data).Error; err != nil {
        logging.Error("ipo.queryChild() Query EquityRelationshipArrayStruct Errors: ", err.Error())
        return data
    }
    return data
}
