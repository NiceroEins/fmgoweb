package ipo

import (
	"datacenter/controller/task"
	"datacenter/models"
	"datacenter/models/spider"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"fmt"
	"github.com/jinzhu/gorm"
	"math/rand"
	"strconv"
	"time"
)

// 第一步：塞入redis ；
// 第二步： 监听redis，获取新增加的任务
// 第三步：完成redis里面的值的消费
const IPO_TYC_LIST = "ipo-tyc-list"
const IPO_TYC_LIST_DETAIL = "ipo-tyc-detail"
const C_TYPE_TYC = "tyc"

func Insert(name string) error {
	// 生成一个Seed 存放到 redis opt static中
	id, err := SaveUIopEquity(spider.UIopEquity{
		ID:      0,
		Company: name,
	})
	if err != nil {
		logging.Error("tycslicder.go Insert(),err", err.Error())
	}
	rand.Seed(time.Now().UnixNano())
	seedId := int(util.BKDRHash(C_TYPE_TYC + strconv.Itoa(rand.Intn(100000))))
	logging.Info("seedId", seedId)
	// 生成6位数的随机数
	_, setErr := gredis.Clone(setting.RedisSetting.SpiderDB).SAdd(task.SeedOPTKey, strconv.Itoa(seedId))                // 存opt
	err = task.SaveSeed2Redis(strconv.Itoa(seedId), *Content2IPOEquitySeed(seedId, id, IPO_TYC_LIST, name, C_TYPE_TYC)) // 存static
	if setErr != nil {
		logging.Error("增加天眼查 redis记录 "+task.SeedOPTKey+" 失败", setErr.Error())
	}
	if err != nil {
		logging.Error("增加天眼查 redis记录 "+task.SeedOPTKey+" 失败", err.Error())
	}
	return nil
}

func SaveUIopEquity(equity spider.UIopEquity) (id int, err error) {
	ipoEqutity := &spider.UIopEquity{}
	end := time.Now()
	err = models.DB().Table("u_iop_equity").Where("company = ?", equity.Company).Where("type = ?", C_TYPE_TYC).Find(&ipoEqutity).Error
	if equity.ID != 0 && err != gorm.ErrRecordNotFound {
		if end.Sub(ipoEqutity.CreatedAt).Seconds() < 60*60*24*7 {
			return equity.ID, nil
		}
		err = models.DB().Table("u_iop_equity").Where("id = ?", ipoEqutity.ID).UpdateColumn("deleted_at", end).Error
		if err != nil {
			logging.Error("tycslicder.go SaveUIopEquity(),err", err.Error())
			return equity.ID, nil
		}
	}
	if equity.ID == 0 { // 第一次搜索添加的没有主键
		if err = models.DB().Table("u_iop_equity").Omit("").Create(&equity).Error; err != nil {
			logging.Error(fmt.Sprintf("tycslicder.go SaveUIopEquity(),err : %s", err.Error()))
			return equity.ID, nil
		}
		return equity.ID, nil
	}
	if err := models.DB().Table("u_iop_equity").Where("id = ?", equity.ID).Update(&equity).Error; err != nil {
		logging.Error(fmt.Sprintf("tycslicder.go SaveUIopEquity(),err : %s,company = %v", err.Error(), equity.Company))
		return equity.ID, nil
	}
	return equity.ID, nil
}

func SaveUIopEquityRelationship(value *spider.NewsContent) error {
	var uIpoEquity spider.UIopEquity
	var uIopEquityRelationship, uIopEquityRelationshipId spider.UIopEquityRelationship
	if err := RemoveRedis(value.SeedID); err != nil {
		logging.Error(fmt.Sprintf("iop_equity.go RemoveRedis(),err : %s", err.Error()))
	}
	err := models.DB().Table("u_iop_equity_relationship").Select("id").
		Where("cid = ?", value.IpoTycDetailSearch.Cid).
		Where("parent_cid = ?", value.IpoTycDetailSearch.ParentCid).Find(&uIopEquityRelationship).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Error(fmt.Sprintf("iop_equity.go SaveUIopEquityRelationship(),err : %s,cid=%v,parent_cid=%v", err.Error(), value.IpoTycDetail.Cid, value.IpoTycDetail.ParentCid))
		return nil
	}
	if uIopEquityRelationship.ID > 0 {
		logging.Error(fmt.Sprintf("tycslider.go SaveUIopEquityRelationship(),err : u_iop_equity_relationship表中有重复,u_i_e_r.ID = %d", uIopEquityRelationship.ID))
		return nil
	}
	models.DB().Table("u_iop_equity_relationship").Select("id").Where("cid = ?", value.IpoTycDetailSearch.ParentCid).Find(&uIopEquityRelationshipId)
	models.DB().Table("u_iop_equity").Select("id").Where("cid = ?", value.IpoTycDetailSearch.ParentCid).Find(&uIpoEquity)
	//插入数据库
	uIopEquityRelationshipInsert := spider.UIopEquityRelationship{
		ParentID:    uIopEquityRelationshipId.ID,
		IopEquityID: uIpoEquity.ID,
		Name:        value.IpoTycDetailSearch.Name,
		Money:       value.IpoTycDetailSearch.Money,
		NameType:    value.IpoTycDetailSearch.NameType,
		Cid:         value.IpoTycDetailSearch.Cid,
		Link:        value.IpoTycDetailSearch.Link,
		ParentCid:   value.IpoTycDetailSearch.ParentCid,
		StockRatio:  value.IpoTycDetailSearch.StockRatio,
		Level:       value.IpoTycDetailSearch.Level,
	}
	if err := models.DB().Table("u_iop_equity_relationship").Create(&uIopEquityRelationshipInsert).Error; err != nil {
		logging.Error(fmt.Sprintf("tycslider.go SaveUIopEquityRelationship(),err : %s", err.Error()))
		return nil
	}
	return nil
}

func IpoTycDetailSearch(value *spider.NewsContent) (bool, error) {
	var count int
	err := SaveUIopEquityRelationship(value)
	if err != nil {
		return false, err
	}
	models.DB().Table("u_iop_equity").Where("cid = ?", value.IpoTycDetail.ParentCid).Count(&count)
	if value.IpoTycDetailSearch.HasNode && value.IpoTycDetailSearch.NameType == 102 && value.IpoTycDetailSearch.Cid != 0 && value.IpoTycDetailSearch.Level < 3 {
		var u_i_e spider.UIopEquityRelationship
		models.DB().Table("u_iop_equity_relationship").Select("iop_equity_id").Where("cid = ?", value.IpoTycDetailSearch.Cid).Find(&u_i_e)
		var seed *spider.Seed
		mode := IPO_TYC_LIST_DETAIL
		rand.Seed(time.Now().UnixNano())
		seedId := int(util.BKDRHash(C_TYPE_TYC + strconv.Itoa(rand.Intn(10000000))))
		_, setErr := gredis.Clone(setting.RedisSetting.SpiderDB).SAdd(task.SeedOPTKey, strconv.Itoa(seedId)) // 存opt
		if setErr != nil {
			logging.Error("增加天眼查 redis记录 "+task.SeedOPTKey+" 失败", setErr.Error())
		}
		seed = Content2IPOTycEquityTycSeed(seedId, value.IpoTycDetailSearch.Level+1, value.IpoTycDetailSearch.Cid, mode)
		_ = task.SaveSeed2Redis(strconv.Itoa(seedId), *seed)
		if seed != nil {
			return task.TryEnqueue(seed, mode), nil
		}
	}
	return true, nil
}

func ProcIpoTycList(value *spider.NewsContent) (bool, error) {
	uIopEquity := FormateEquityEntity(value)
	_, err := SaveUIopEquity(uIopEquity)
	if err != nil {
		return false, nil
	}
	if err = RemoveRedis(value.SeedID); err != nil {
		logging.Error(fmt.Sprintf("iop_equity.go RemoveRedis(),err : %s", err.Error()))
		return false, err
	}
	var seed *spider.Seed
	mode := IPO_TYC_LIST_DETAIL
	rand.Seed(time.Now().UnixNano())
	cid, _ := strconv.Atoi(value.IpoTycSearchList.Cid)
	seedId := int(util.BKDRHash(C_TYPE_TYC + strconv.Itoa(rand.Intn(100000))))
	seed = Content2IPOTycEquityTycSeed(seedId, 1, cid, mode)
	_, setErr := gredis.Clone(setting.RedisSetting.SpiderDB).SAdd(task.SeedOPTKey, strconv.Itoa(seedId)) // 存opt
	_ = task.SaveSeed2Redis(strconv.Itoa(seedId), *seed)
	// 存static
	if setErr != nil {
		return task.TryEnqueue(seed, mode), nil
	}
	return true, nil
}

func FormateEquityEntity(value *spider.NewsContent) (res spider.UIopEquity) {
	if util.IsEmpty(value) {
		return
	}
	res.ID = value.IpoTycSearchList.Id
	res.Company = value.IpoTycSearchList.Company
	res.CatchTime = value.IpoTycSearchList.CatchTime
	res.DeclareTime = value.IpoTycSearchList.DeclareTime
	res.EquityLink = value.IpoTycSearchList.EquityLink
	res.QuantityToBeIssued = value.IpoTycSearchList.QuantityToBeIssued
	res.CatchTime = value.CatchTime
	res.Cid, _ = strconv.Atoi(value.IpoTycSearchList.Cid)
	res.SpiderTime = value.SpiderTime
	res.Type = value.IpoTycSearchList.Type
	return
}

func Content2IPOTycEquityTycSeed(seedId, level, cid int, mode string) *spider.Seed {
	seed := &spider.Seed{
		ID:     seedId,
		TaskID: util.GenerateRandomString(32) + "-" + strconv.Itoa(seedId),
		Url:    "",
		Mode:   mode,
		Data: map[string]interface{}{
			"cid":   cid,
			"level": level,
		},
		CatchInterval: 3600 * 24 * 7,
		Selector:      nil,
	}
	return seed
}

func Content2IPOEquitySeed(seedId, id int, mode, company, c_type string) *spider.Seed {
	seed := &spider.Seed{
		ID:     seedId,
		TaskID: util.GenerateRandomString(32) + "-" + strconv.Itoa(seedId),
		Url:    "",
		Mode:   mode,
		Data: map[string]interface{}{
			"company": company,
			"id":      id,
			"c_type":  c_type, //公司类型
		},
		CatchInterval: 3600 * 24 * 7,
		Selector:      nil,
	}
	return seed
}

func RemoveRedis(seedId string) error {
	remErr := gredis.Clone(setting.RedisSetting.SpiderDB).SREM(task.SeedOPTKey, seedId)
	if remErr != nil {
		logging.Error("seed.DelSeed() err:", remErr.Error())
		return remErr
	}
	remErr = gredis.Clone(setting.RedisSetting.SpiderDB).HDel(task.SeedStaticKey, seedId)
	if remErr != nil {
		logging.Error("seed.DelSeed() err:", remErr.Error())
		return remErr
	}
	return nil
}
