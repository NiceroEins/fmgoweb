package spider

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"strconv"
)

type WeChatList struct {
	Title string `json:"title"`
	Link  string `json:"link"`
}

type WeChatDetail struct {
	Name      string `json:"name"`
	Title     string `json:"title"`
	TitleTime string `json:"title_time"`
	Link      string `json:"link"`
	Content   string `json:"content"`
}

func ParseWeChatDetail(content *NewsContent) *NewsContent {
	if content.WeChatDetail == (WeChatDetail{}) {
		return nil
	}
	id, err := weChatName2SeedID(content.WeChatDetail.Name)
	if err != nil {
		id = 1
	}
	content.Title = content.WeChatDetail.Title
	content.Link = content.WeChatDetail.Link
	content.Content = content.WeChatDetail.Content
	content.TitleTime = content.WeChatDetail.TitleTime
	content.SeedID = strconv.Itoa(id)

	return content
}

func weChatName2SeedID(name string) (int, error) {
	var id []int
	name = "G-" + name
	db := models.DB().Table("u_seed")
	if err := db.Where("catch_path = ? AND deleted_at IS NULL", name).Pluck("id", &id).Error; err != nil {
		logging.Error("spider.weChatName2SeedID() Query seed id Errors: ", err.Error())
		return 0, err
	}
	if id == nil || len(id) == 0 {
		return 1, nil
	}
	return id[0], nil
}
