package spider

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type ResearchList struct {
	DetailID     string  `json:"detail_id"`
	Code         string  `json:"code"`
	Name         *string `json:"name,omitempty"`
	Organization *string `json:"organization,omitempty"`
	Author       *string `json:"author,omitempty"`
	Title        *string `json:"title,omitempty"`
	Level        *string `json:"level,omitempty"`
	LevelChange  *string `json:"level_change,omitempty"`
	PublishDate  *string `json:"publish_date,omitempty"`
	Type         *string `json:"type,omitempty"`
	Industry     *string `json:"industry,omitempty"`
	Heat         *int    `json:"heat,omitempty"`
}

type ResearchDetail struct {
	ID               int                  `json:"id,omitempty"`
	Code             *string              `json:"code,omitempty"`
	Title            *string              `json:"title,omitempty"`
	Name             *string              `json:"name,omitempty"`
	Author           *string              `json:"author,omitempty"`
	Organization     *string              `json:"organization,omitempty"`
	Level            *string              `json:"level,omitempty"`
	Type             *string              `json:"type,omitempty"`
	PublishDate      *string              `json:"publish_date,omitempty"`
	SourceLink       *string              `json:"source_link,omitempty"`
	Abstract         *string              `json:"abstract,omitempty"`
	PdfFirstCoverage *int                 `json:"pdf_first_coverage,omitempty"`
	PdfRaise         *int                 `json:"pdf_raise,omitempty"`
	Data             []ResearchDetailData `json:"data,omitempty"`
}

type ResearchDetailData struct {
	TargetPrice       *string  `json:"target_price,omitempty"`
	ExpectedYear      *int     `json:"expected_year,omitempty"`
	ExpectedNetProfit *float64 `json:"expected_net_profit,omitempty"`
	EarningsPerShare  *float64 `json:"earnings_per_share,omitempty"`
	PriceEarningRatio *float64 `json:"price_earning_ratio,omitempty"`
}

type ResearchPDF struct {
	Content          *string `json:"content,omitempty"`
	ReportID         int     `json:"report_id,omitempty"`
	PdfFirstCoverage *int    `json:"pdf_first_coverage,omitempty"`
	PdfRaise         *int    `json:"pdf_raise,omitempty"`
	Data             []URRD  `json:"data,omitempty"`
}

type IpoDc struct {
	//新增东财字段
	Company                string `json:"company,omitempty"`
	DeclareTime            string `json:"declare_time,omitempty"`
	QuantityToBeIssued     string `json:"quantity_to_be_issued,omitempty"`
	ProposedListingAddress string `json:"proposed_listing_address,omitempty"`
	IpoFinishMsg           bool   `json:"ipo_finish_msg"`
}
type IpoTycList struct {
	Id           int    `json:"id,omitempty"`
	Cid          string `json:"cid,omitempty"`
	EquityLink   string `json:"equity_link,omitempty"`
	IpoFinishMsg bool   `json:"ipo_finish_msg"`
}

type IpoTycDetail struct {
	Name         string  `json:"name,omitempty"`
	Money        float64 `json:"money,omitempty"`
	NameType     int     `json:"name_type,omitempty"`
	HasNode      bool    `json:"has_node,omitempty"`
	ParentCid    int     `json:"parent_cid,omitempty"`
	Cid          int     `json:"cid,omitempty"`
	Link         string  `json:"link,omitempty"`
	StockRatio   float64 `json:"stock_ratio,omitempty"`
	IpoFinishMsg bool    `json:"ipo_finish_msg"`
	Level        int     `json:"level,omitempty"`
}

type IpoTycSearch struct {
	Id                     int    `json:"id,omitempty"`
	Cid                    string `json:"cid,omitempty"`
	EquityLink             string `json:"equity_link,omitempty"`
	Company                string `json:"company,omitempty"`
	DeclareTime            string `json:"declare_time,omitempty"`
	QuantityToBeIssued     string `json:"quantity_to_be_issued,omitempty"`
	ProposedListingAddress string `json:"proposed_listing_address,omitempty"`
	IpoFinishMsg           bool   `json:"ipo_finish_msg"`
	CatchTime              string `json:"catch_time,omitempty"`
	SpiderTime             string `json:"spider_time,omitempty"`
	Type                   string `json:"c_type,omitempty"`
}

type Reorganization struct {
	models.Simple
	Code         string `json:"code"`
	Name         string `json:"name"`
	Date         string `json:"date"`
	AffectPeriod string `json:"affect_period"`
	Object       string `json:"object"`
	CatchTime    string `json:"catch_time"`
	SpiderTime   string `json:"spider_time"`
}

func (Reorganization) TableName() string {
	return "u_reorganization"
}

type IndustryResearch struct {
	Title        string  `json:"title,omitempty"`
	Organization string  `json:"organization,omitempty"`
	SourceLink   string  `json:"source_link"`
	ID           int     `json:"id,omitempty"`
	Type         string  `json:"type,omitempty"`
	SeedID       string  `json:"seed_id,omitempty"`
	PublishDate  *string `json:"publish_date,omitempty"`
	Industry     string  `json:"industry,omitempty"`
	CatchTime    string  `json:"catch_time,omitempty"`
	SpiderTime   string  `json:"spider_time,omitempty"`
	FirstIds     *string `json:"first_ids,omitempty"`
}

func (IndustryResearch) TableName() string {
	return "u_research_report"
}

type UserEvent struct {
	models.Simple
	Id          int    `json:"id" gorm:"cloumn:id"`                     // 主键ID
	UserID      int    `json:"user_id" gorm:"cloumn:user_id"`           // 用户ID
	ObjectID    int    `json:"object_id" gorm:"cloumn:object_id"`       // 事件ID
	EventStatus int    `json:"event_status" gorm:"cloumn:event_status"` // 事件状态
	EventStr    string `json:"event_str" gorm:"cloumn:event_str"`       // 点评内容
	ObjectType  string `json:"object_type" gorm:"cloumn:object_type"`   // 对象类型
	EventType   string `json:"event_type" gorm:"cloumn:event_type"`     // 事件类型
	PictureLink string `json:"picture_link" gorm:"cloumn:picture_link"` // 图片链接
}

func (UserEvent) TableName() string {
	return "u_user_event"
}

// u_research_report
type URR struct {
	ID        int        `gorm:"primary_key" json:"id,omitempty"`
	DeletedAt *time.Time `json:"-" sql:"index"`

	DetailID         string  `json:"detail_id" gorm:"-"`
	Code             string  `json:"code"`
	Name             *string `json:"name,omitempty"`
	Organization     *string `json:"organization,omitempty"`
	Author           *string `json:"author,omitempty"`
	Title            *string `json:"title,omitempty"`
	Level            *string `json:"level,omitempty"`
	LevelChange      *string `json:"level_change,omitempty"`
	PublishDate      *string `json:"publish_date,omitempty"`
	Type             *string `json:"type,omitempty"`
	Industry         *string `json:"industry,omitempty"`
	SourceLink       *string `json:"source_link,omitempty"`
	Abstract         *string `json:"abstract,omitempty"`
	Content          *string `json:"content,omitempty"`
	SeedID           *string `json:"seed_id,omitempty"`
	CatchTime        *string `json:"catch_time,omitempty"`
	SpiderTime       *string `json:"spider_time,omitempty"`
	Parent           int     `json:"-"`
	LevelCalc        *string `json:"level_calc,omitempty"`
	PdfFirstCoverage *int    `json:"pdf_first_coverage,omitempty"`
	PdfRaise         *int    `json:"pdf_raise,omitempty"`
	Heat             *int    `json:"heat,omitempty"`
	FirstIds         *string `json:"first_ids,omitempty"`
}

func (URR) TableName() string {
	return "u_research_report"
}

// u_research_report_data
type URRD struct {
	models.Simple
	Code               string    `json:"code"`
	SeedID             *string   `json:"seed_id,omitempty"`
	Name               *string   `json:"name,omitempty"`
	ReportID           int       `json:"report_id,omitempty"`
	ExpectedNetProfit  *float64  `json:"expected_net_profit,omitempty"`
	TargetPrice        *string   `json:"target_price,omitempty"`
	RisingSpace        *string   `json:"rising_space,omitempty"`
	CompoundGrowthRate *float64  `json:"compound_growth_rate,omitempty"`
	EarningsPerShare   *float64  `json:"earnings_per_share,omitempty"`
	ForecastEPS        *string   `json:"forecast_eps,omitempty"`
	PriceEarningRatio  *float64  `json:"price_earning_ratio,omitempty"`
	ExpectedYear       *int      `json:"expected_year,omitempty"`
	PublishDate        *string   `json:"publish_date,omitempty"`
	PreClose           *float64  `json:"pre_close,omitempty"`
	RisingSpaces       []float64 `json:"-" gorm:"-"`
}

func (URRD) TableName() string {
	return "u_research_report_data"
}

func ResearchList2URR(rl *ResearchList) *URR {
	return &URR{
		DetailID:     rl.DetailID,
		Code:         rl.Code,
		Name:         rl.Name,
		Organization: rl.Organization,
		Author:       rl.Author,
		Title:        rl.Title,
		Level:        rl.Level,
		LevelChange:  rl.LevelChange,
		PublishDate:  rl.PublishDate,
		Type:         rl.Type,
		Industry:     rl.Industry,
		Heat:         rl.Heat,
	}
}

func ResearchDetail2URR(rd *ResearchDetail) *URR {
	urr := &URR{
		SourceLink: rd.SourceLink,
		Abstract:   rd.Abstract,
	}

	urr.ID = rd.ID
	if rd.Code != nil {
		urr.Code = *rd.Code
	}
	if rd.Title != nil {
		urr.Title = rd.Title
	}
	if rd.Author != nil {
		urr.Author = rd.Author
	}
	if rd.Name != nil {
		urr.Name = rd.Name
	}
	if rd.PdfFirstCoverage != nil {
		urr.PdfFirstCoverage = rd.PdfFirstCoverage
	}
	if rd.PdfRaise != nil {
		urr.PdfRaise = rd.PdfRaise
	}
	if rd.Level != nil {
		urr.Level = rd.Level
	}
	if rd.Organization != nil {
		urr.Organization = rd.Organization
	}
	if rd.Type != nil {
		urr.Type = rd.Type
	}
	if rd.PublishDate != nil {
		urr.PublishDate = rd.PublishDate
	}

	return urr
}

func ResearchDetail2URRD(rd *ResearchDetail, rdd *ResearchDetailData) *URRD {
	urrd := &URRD{
		ReportID:          rd.ID,
		TargetPrice:       rdd.TargetPrice,
		EarningsPerShare:  rdd.EarningsPerShare,
		PriceEarningRatio: rdd.PriceEarningRatio,
		ExpectedYear:      rdd.ExpectedYear,
		ExpectedNetProfit: rdd.ExpectedNetProfit,
	}
	if rd.Code != nil {
		urrd.Code = *rd.Code
	}
	if rd.Name != nil {
		urrd.Name = rd.Name
	}

	return urrd
}

func Content2ResearchSeed(content *NewsContent, id int, mode string) *Seed {
	seed := &Seed{
		ID:   util.Atoi(content.SeedID),
		Url:  content.Link,
		Mode: mode,
		Data: map[string]interface{}{
			"id":        id,
			"detail_id": content.ResearchList.DetailID,
		},
		Selector: nil,
	}
	return seed
}

// id: research_report表主键
func Content2PDFSeed(content *NewsContent, id int, mode string, preClose float64) *Seed {
	seed := &Seed{
		ID:   util.Atoi(content.SeedID),
		Url:  content.Link,
		Mode: mode,
		Data: map[string]interface{}{
			"id":          id,
			"source_link": content.ResearchDetail.SourceLink,
			"pre_close":   preClose,
		},
		Selector: nil,
	}
	return seed
}

func QueryURR(id int) *URR {
	var urr URR
	err := models.DB().Table("u_research_report").Where("id = ?", id).First(&urr).Error
	if err != nil {
		return nil
	}
	return &urr
}

func InsertURR(urr *URR) (*URR, error) {
	if urr == nil {
		return nil, nil
	}
	if bDup, err := checkDuplicateURR(urr); err == nil {
		if bDup {
			//t := 1
			urr.Parent = 1
		} else {
			if heat, err := getResearchHeat(urr); err == nil {
				urr.Heat = heat
			}
		}
	}
	if urr.Industry != nil && *urr.Industry != "" {
		ids := getFirstIdsByName(*urr.Industry)
		if len(ids) > 0 {
			firstIds := "#"
			for _, id := range ids {
				firstIds = firstIds + strconv.Itoa(id) + "#"
			}
			urr.FirstIds = &firstIds
		}
	}
	ok, levelCalc := checkPrevLevel(urr)
	if ok {
		urr.LevelCalc = &levelCalc
	}
	ret := &URR{}
	err := models.DB().Where(&URR{
		Code:         urr.Code,
		Title:        urr.Title,
		Organization: urr.Organization,
		SeedID:       urr.SeedID,
	}).Assign(urr).FirstOrCreate(ret).Error
	if urr.ID > 0 && urr.Parent == 1 {
		log, _ := json.Marshal(urr)
		logging.Error("研报判重：insert", string(log))
	}
	return ret, err
}

func CheckImportantOrg(uur *URR) bool {
	return checkImportantOrg(uur)
}

func checkImportantOrg(uur *URR) bool {
	if uur != nil && uur.Code != "" && uur.Organization != nil && uur.Author != nil {
		type auStruct struct {
			Securities string `json:"securities"`
			Author     string `json:"author"`
		}
		authors := make([]auStruct, 0)
		err := models.DB().Table("u_industry_securities_author_relationship").
			Joins("LEFT JOIN u_stock_industry SI ON u_industry_securities_author_relationship.industry_name=SI.first_industry").
			Where("code=?", uur.Code).Find(&authors).Error
		if err != nil {
			return false
		}
		for _, name := range authors {
			if strings.Contains(*uur.Author, name.Author) && *uur.Organization == name.Securities {
				return true
			}
		}
	}
	return false
}

func updatePreURR(preUrr *URR, urr URR) {
	urr.ID = 0 //gorm 更新真不好用
	sqlTitle, title := delSpecialMark(*preUrr.Title)
	err := models.DB().Table("u_research_report").
		Where("code=? AND organization=? AND parent=0 AND "+sqlTitle+"=?", preUrr.Code, *preUrr.Organization, title).
		Omit("id").
		Updates(&urr).Error
	if err != nil {
		log, _ := json.Marshal(urr)
		logging.Error("首条研报详情复制失败:", string(log))
	}
}

func getFirstIdsByName(name string) []int {
	ids := make([]int, 0)
	err := models.DB().Table("u_first_industry").Where("name=?", name).Pluck("id", &ids).Error
	if err != nil {
		logging.Error("获取u_first_industry ="+name+"ids 失败", err.Error())
	}
	if len(ids) > 0 {
		return ids
	}
	err = models.DB().Table("u_performance_industry").Where("substring_index(industry_name,'-',-1)=?", name).Pluck("first_id", &ids).Error
	if err != nil {
		logging.Error("获取u_performance_industry 二级="+name+"first_ids 失败", err.Error())
	}
	if len(ids) > 0 {
		return ids
	}
	err = models.DB().Table("u_research_report_industry").Where("industry_name=?", name).Pluck("first_id", &ids).Error
	if err != nil {
		logging.Error("获取u_research_report_industry ="+name+"first_ids 失败", err.Error())
	}
	return ids
}

func UpdateURR(urr *URR) error {
	if urr == nil {
		return nil
	}
	//if bDup, err := checkDuplicateURR(urr); err == nil {
	//	if bDup {
	//		t := 1
	//		urr.Parent = &t
	//	}
	//}
	ok, levelCalc := checkPrevLevel(urr)
	if ok {
		urr.LevelCalc = &levelCalc
	}
	if urr.Industry != nil && *urr.Industry != "" {
		ids := getFirstIdsByName(*urr.Industry)
		if len(ids) > 0 {
			firstIds := "#"
			for _, id := range ids {
				firstIds = firstIds + strconv.Itoa(id) + "#"
			}
			urr.FirstIds = &firstIds
		}
	}
	var err error
	if urr.ID == 0 {
		if bDup, err := checkDuplicateURR(urr); err == nil {
			if bDup {
				//t := 1
				urr.Parent = 1
			} else {
				if heat, err := getResearchHeat(urr); err == nil {
					urr.Heat = heat
				}
			}
		}
		err = models.DB().Table("u_research_report").Create(urr).Error
	} else {
		preUrr := URR{}
		if err = models.DB().Table("u_research_report").Where("id = ?", urr.ID).Find(&preUrr).Error; err == nil {
			if checkImportantOrg(&preUrr) {
				if urr.Type == nil {
					urr.Type = new(string)
				}
				*urr.Type = "重点券商"
			}
			if preUrr.Parent == 1 && preUrr.Title != nil && preUrr.Organization != nil && preUrr.Code != "" {
				updatePreURR(&preUrr, *urr)
			}
		}
		if urr.ID > 0 && urr.Parent == 1 {
			log, _ := json.Marshal(urr)
			logging.Error("研报判重：update", string(log))
		}
		err = models.DB().Table("u_research_report").Where("id = ?", urr.ID).Updates(urr).Error
	}
	return err
}

func InsertURRD(urrd *URRD) (*URRD, error) {
	if urrd == nil || urrd.ExpectedYear == nil {
		return urrd, nil
	}
	var urr URR
	err := models.DB().Table("u_research_report").Where("id = ?", urrd.ReportID).First(&urr).Error
	if err == nil {
		urrd.Code = urr.Code
		urrd.SeedID = urr.SeedID
		urrd.Name = urr.Name
		urrd.PublishDate = urr.PublishDate
	}
	urrd = checkPreClose(urrd)
	err = models.DB().Where(&URRD{
		ReportID:     urrd.ReportID,
		ExpectedYear: urrd.ExpectedYear,
	}).Assign(urrd).FirstOrCreate(&URRD{}).Error

	return urrd, err
}

func InsertReorganization(ro *Reorganization) error {
	err := models.DB().Where(&Reorganization{
		Code: ro.Code,
		Name: ro.Name,
	}).Assign(ro).FirstOrCreate(&Reorganization{}).Error
	//err := models.DB().Table("u_reorganization").Create(ro).Error
	return err
}

func UpdateIndustryResearch(ir *IndustryResearch) error {
	if ir == nil {
		return nil
	}
	var err error
	if ir.Industry != "" {
		ids := getFirstIdsByName(ir.Industry)
		if len(ids) > 0 {
			firstIds := "#"
			for _, id := range ids {
				firstIds = firstIds + strconv.Itoa(id) + "#"
			}
			ir.FirstIds = &firstIds
		}
	}
	if ir.ID == 0 {
		err = models.DB().Table("u_research_report").Create(ir).Error
	} else {
		ret := IndustryResearch{}
		err = models.DB().Table("u_research_report").Where(&IndustryResearch{
			ID: ir.ID,
		}).Assign(ir).FirstOrCreate(&ret).Error
	}
	return err
}

func delSpecialMark(s string) (string, string) {
	var marks = []string{
		",",
		"(",
		")",
		"（",
		"）",
		"：",
		"[",
		"]",
		"￥",
		"\"",
		"＼",
		"“",
		"”",
		"《",
		"》",
		"，",
		"。",
		" ",
		"　",
		"？",
		"+",
		"＋",
		"-",
		"—",
		";",
		"；",
		"*",
		"&",
		"、",
		".",
		":",
		"～",
		"~",
		"%",
		"^",
		"`",
		"·",
		"‘",
		"’",
		"\\'",
		"\\",
		"\t",
		"「",
		"」",
		"『",
		"』",
		"【",
		"】",
		"―",
		"/",
		"／",
		"!",
		"！",
	}
	sqlTitle := "title"
	for _, v := range marks {
		s = strings.ReplaceAll(s, v, "")
		switch v {
		case "\\":
			v = "\\\\"
			break
		case "\t":
			v = "\\t"
			break
		}
		sqlTitle = "replace(" + sqlTitle + ",'" + v + "','')"
	}
	return sqlTitle, s
}

func checkDuplicateURR(urr *URR) (bool, error) {
	n := 0
	if urr == nil || urr.Code == "" || urr.Title == nil || urr.Organization == nil {
		return false, nil
	}
	sqlTitle, title := delSpecialMark(*urr.Title)
	err := models.DB().Table("u_research_report").
		Where("code=? AND organization=? AND "+sqlTitle+"=?", urr.Code, urr.Organization, title).
		Where("id <> ?", urr.ID).
		Count(&n).Error
	if err != nil {
		return false, err
	}
	return n > 0, nil
}

func checkPrevLevel(urr *URR) (bool, string) {
	if urr == nil || urr.Code == "" || urr.Organization == nil || urr.PublishDate == nil || urr.Level == nil || *urr.Level == "" {
		return false, ""
	}
	var prev URR
	if err := models.DB().Table("u_research_report").Where(&URR{
		Code:         urr.Code,
		SeedID:       urr.SeedID,
		Organization: urr.Organization,
	}).Where("publish_date < ?", urr.PublishDate).Order("publish_date DESC").First(&prev).Error; err != nil {
		return false, ""
	}
	levels := map[string]int{
		"买入": 5,
		"增持": 4,
		"中性": 3,
		"减持": 2,
		"卖出": 1,
	}
	if prev.Level == nil || *prev.Level == "" {
		return false, ""
	}
	levelPrev, b1 := levels[*prev.Level]
	levelUrr, b2 := levels[*urr.Level]
	if !b1 || !b2 {
		return false, ""
	}
	d := levelUrr - levelPrev
	switch {
	case d > 0:
		return true, "up"
	case d < 0:
		return true, "down"
	case d == 0:
		return true, ""
	}
	return false, ""
}

// 昨收价
func checkPreClose(urrd *URRD) *URRD {
	if urrd == nil || urrd.TargetPrice == nil || urrd.PublishDate == nil {
		return urrd
	}
	var pre struct {
		Close float64
	}
	if err := models.DB().Table("p_stock_tick").Select("close").Where("stock_code = ?", urrd.Code).Where("trade_date < ?", *urrd.PublishDate).Where("close > 0").Order("trade_date DESC").First(&pre).Error; err != nil {
		logging.Error("research.checkPreClose() query close Errors: ", err.Error())
		return urrd
	}
	if pre.Close == 0 {
		logging.Error("research.checkPreClose() zero pre.Close")
		return urrd
	}
	var targetPrices []float64
	var risingSpaces []string
	tps := strings.Split(*urrd.TargetPrice, ",")
	for i := 0; i < len(tps); i++ {
		targetPrice, e := strconv.ParseFloat(tps[i], 64)
		if e != nil {
			continue
		}
		targetPrices = append(targetPrices, targetPrice)
		urrd.RisingSpaces = append(urrd.RisingSpaces, targetPrice/pre.Close-1)
		risingSpaces = append(risingSpaces, fmt.Sprintf("%.4f", targetPrice/pre.Close-1))
	}

	rs := strings.Join(risingSpaces, ",")
	urrd.PreClose = &pre.Close
	urrd.RisingSpace = &rs
	return urrd
}

// 根据code 获取 user_id
func GetUserIdByCode(data map[string]interface{}) (int, error) {
	var User struct {
		UserID int `json:"user_id" gorm:"column:user_id"`
	}
	userModel := &User
	err := models.DB().Table("u_stock_industry").
		Select("u_second_industry_user.user_id").
		Joins("left join u_second_industry_user on u_stock_industry.actual_industry = u_second_industry_user.second_industry").
		Where(data).First(&userModel).Error
	if err != nil {
		return userModel.UserID, err
	}
	return userModel.UserID, nil
}

func getResearchHeat(urr *URR) (*int, error) {
	if urr.Code == "" {
		return nil, nil
	}
	cnt := 0
	err := models.DB().Table("u_research_report").
		Where("code=?", urr.Code).
		Where("parent=0").
		Where("created_at>=?", time.Now().AddDate(0, -1, 0).Format(util.YMD)+" 00:00:00").
		Where("created_at<=?", time.Now().Format(util.YMD)+" 23:59:59").
		Count(&cnt).Error
	cnt++ //包括本身一条
	return &cnt, err
}
