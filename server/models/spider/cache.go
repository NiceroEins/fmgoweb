package spider

import (
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"sort"
	"strconv"
	"strings"
	"time"
)

// fast count
func FastCount(dbx *gorm.DB, policy *NewsPolicy, integrity []string) (int, error) {
	//sort.Strings(policy.CustomKeyword)
	sort.Strings(policy.NewsType)

	ret := 0
	val, err := gredis.HGetFloat64(makeFastCountKey(policy.NewsType), makeFastCountField(policy, integrity))
	if err != nil {
		// write cache
		retErr := dbx.Model(NewsEx{}).Count(&ret).Error
		if retErr != nil {
			return 0, retErr
		}
		retErr = gredis.HSet(makeFastCountKey(policy.NewsType), makeFastCountField(policy, integrity), ret)
		return ret, retErr
	}
	// read cache
	ret = int(val)
	return ret, nil
}

func FastIndex(policy *NewsPolicy, size int, ts []string) (int64, error) {
	keys := makeFastIndexKey(policy, size, ts)
	if ts != nil && len(ts) > 1 {
		keys = makeFastIndexKey(policy, size, []string{ts[0]})
		referred, err := gredis.Keys(keys + "*")
		if err != nil {
			return 0, err
		}
		// 逆序
		var ret []int
		for _, v := range referred {
			t := strings.Replace(v, keys+":", "", -1)
			tsRaw := util.Atoi(t)
			tsCur, err := time.ParseInLocation("2006-01-02 15:04:05", ts[1], time.Local)
			if err != nil {
				logging.Error("cache.FastIndex() Errors: ", err.Error())
				return 0, err
			}
			if tsRaw > 0 && tsCur.Unix() >= int64(tsRaw) {
				ret = append(ret, tsRaw)
			}
		}
		if len(ret) == 0 {
			return 0, nil
		}
		sort.Sort(sort.Reverse(sort.IntSlice(ret)))
		keys = fmt.Sprintf("%s:%d", keys, ret[0])
	}

	val, err := gredis.GetInt(keys)
	if err != nil {
		return 0, err
	}
	return val, nil
}

func SetFastIndex(policy *NewsPolicy, size int, ts []string, index int64) error {
	// 缓存3天
	expires := 3600 * 24 * 3
	err := gredis.Set(makeFastIndexKey(policy, size, ts), index, expires)
	return err
}

func SplitRange(timeRange []string) ([]string, []string) {
	var integrity, partial []string
	if len(timeRange) > 1 {
		tsBegin, _ := time.ParseInLocation("2006-01-02 15:04:05", timeRange[0], time.Local)
		tsZero, _ := time.ParseInLocation("2006-01-02", tsBegin.Format("2006-01-02"), time.Local)
		if tsBegin.Equal(tsZero) {
			return integrity, timeRange
		}
		tsEnd, _ := time.ParseInLocation("2006-01-02 15:04:05", timeRange[1], time.Local)
		integrity = []string{
			tsBegin.Format("2006-01-02"),
			tsEnd.Format("2006-01-02"),
		}
		partial = []string{
			tsEnd.Format("2006-01-02") + " 00:00:00",
			timeRange[1],
		}
	} else {
		return integrity, timeRange
	}
	return integrity, partial
}

func makeFastCountKey(src []string) string {
	return "cache:news:fast_count:" + strings.Join(src, "_")
}

func makeFastCountField(policy *NewsPolicy, ts []string) string {
	//var ret string
	//if src == nil || len(src) == 0 {
	//	ret += "(null)_"
	//} else {
	//	ret += strings.Join(src, "_") + "_"
	//}
	//if timeRange == nil || len(timeRange) == 0 {
	//	ret += "(null)"
	//} else {
	//	ret += strings.Join(timeRange, "_")
	//}
	bt, _ := json.Marshal(policy)
	str := string(bt)
	for _, v := range ts {
		str += "&" + v
	}

	return strconv.Itoa(int(util.BKDRHash(str)))
}

// cache:news:fast_index:[hash str]:[timestamp]
func makeFastIndexKey(policy *NewsPolicy, size int, ts []string) string {
	bt, _ := json.Marshal(policy)
	str := fmt.Sprintf("%s&%d", string(bt), size)
	if ts != nil && len(ts) > 0 {
		str += "&" + ts[0]
	}
	hash := util.BKDRHash(str)
	ret := "cache:news:fast_index:" + strconv.Itoa(int(hash))
	if ts != nil && len(ts) > 1 {
		tm, err := time.ParseInLocation("2006-01-02 15:04:05", ts[1], time.Local)
		if err != nil {
			logging.Error("cache.makeFastIndexKey() Errors: ", err.Error())
			return ret
		}
		ret += fmt.Sprintf(":%d", tm.Unix())
	}
	return ret
}
