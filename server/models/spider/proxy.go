package spider

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync/atomic"
	"time"
)

type Raw struct {
	Code    int     `json:"code"`
	Data    []Proxy `json:"data"`
	Msg     string  `json:"msg"`
	Success bool    `json:"success"`
}

type Proxy struct {
	Ip         string `json:"ip"`
	Port       int    `json:"port"`
	ExpireTime string `json:"expire_time"`
}

type ProxyNew struct {
	Ip      string `json:"ip"`
	Port    string `json:"port"`
	Timeout string `json:"timeout"`
}

//从远程地址获取数据,并存入ProxyMap,返回数据条数
func FetchProxy() int {
	v := atomic.LoadInt64(&models.Locker)
	if v > 0 || !atomic.CompareAndSwapInt64(&models.Locker, v, 1) {
		logging.Info("proxy.go FetchProxy() locked")
		return 0
	}
	defer atomic.StoreInt64(&models.Locker, 0)

	client := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	url := fmt.Sprintf("http://webapi.http.zhimacangku.com/getip?num=%d&type=2&pro=&city=0&yys=0&port=1&time=1&ts=1&ys=0&cs=0&lb=1&sb=0&pb=45&mr=2&regions=", AutoProxy())
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		logging.Error(err)
		return 0
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		logging.Error(err)
		return 0
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Error(err)
		return 0
	}
	defer func() {
		_ = resp.Body.Close()
	}()
	raw := Raw{}
	//logging.Info(string(bytes))
	err = json.Unmarshal(bytes, &raw)
	if err != nil {
		logging.Error(err)
		return 0
	}

	//fmt.Printf("raw %#v",raw)

	ret := 0
	//将 raw.Data存入redis ProxyMap中
	for _, data := range raw.Data {
		dataJson, err := json.Marshal(data)
		if err != nil {
			logging.Error("proxy.go FetchProxy():data转json格式失败:" + err.Error())
		}
		//ProxyMap中存入数据
		err = gredis.HSet("ProxyMap", data.Ip, string(dataJson))
		if err != nil {
			logging.Error("proxy.go FetchProxy():ProxyMap存入数据失败:" + err.Error())
		}
		ret++
	}
	return ret
}

func FetchProxyNew(curNum int) int {
	v := atomic.LoadInt64(&models.Locker)
	if v > 0 || !atomic.CompareAndSwapInt64(&models.Locker, v, 1) {
		logging.Info("proxy.go FetchProxyNew() locked")
		return 0
	}
	defer atomic.StoreInt64(&models.Locker, 0)

	client := &http.Client{
		Timeout: 5 * time.Second,
	}
	fetchNum := AutoProxy() - curNum
	if fetchNum <= 0 {
		return 0
	}
	url := fmt.Sprintf("http://api6.uuhttp.com:39006/index/api/return_data?mode=http&count=%d&b_time=500&return_type=2&line_break=6&ttl=1&secert=MTU2MjkwMDI3ODM6ZjAzNzM1NGRiY2FiMGEwYmNkZGE3NWMxYmZjZGJkNjE=", fetchNum)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		logging.Error(err)
		return 0
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		logging.Error(err)
		return 0
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Error(err)
		return 0
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	var raw []ProxyNew
	//logging.Info(string(bytes))
	err = json.Unmarshal(bytes, &raw)
	if err != nil {
		logging.Error(err)
		return 0
	}

	ret := 0
	//将 raw.Data存入redis ProxyMap中
	for _, data := range raw {
		dataJson, err := json.Marshal(proxyNew2Proxy(data))
		if err != nil {
			logging.Error("proxy.go FetchProxyNew():data转json格式失败:" + err.Error())
		}
		//ProxyMap中存入数据
		err = gredis.HSet("ProxyMap", data.Ip, string(dataJson))
		if err != nil {
			logging.Error("proxy.go FetchProxyNew():ProxyMap存入数据失败:" + err.Error())
		}
		ret++
	}
	return ret
}

func AutoProxy() int {
	t := time.Now()
	nMax := setting.SpiderSetting.ProxyNumPerUse
	//早8点前，晚8点后，周六日
	if t.Hour() < 7 || t.Hour() > 15 || t.Weekday() == time.Saturday || t.Weekday() == time.Sunday {
		return nMax / 3
	}
	return nMax
}

//从ProxyMap中移除数据,返回bool
func ResetProxy(proxy string) bool {
	err := gredis.HDel("ProxyMap", proxy)
	if err != nil {
		logging.Error("proxy.go ResetProxy():ProxyMap移除数据失败:" + err.Error())
	}
	return true
}

func GetProxy() []Proxy {
	var ret []Proxy
	data, err := gredis.HGetAllStringMap("ProxyMap")
	if err != nil {
		return ret
	}
	for _, v := range data {
		var proxy Proxy
		if err = json.Unmarshal([]byte(v), &proxy); err != nil {
			logging.Error("proxy.GetProxy() Unmarshal Errors: ", err)
			continue
		}
		ret = append(ret, proxy)
	}
	return ret
}

func proxyNew2Proxy(pn ProxyNew) Proxy {
	var to int
	var suffix string
	var p Proxy
	if _, err := fmt.Sscanf(pn.Timeout, "%d%s", &to, &suffix); err != nil {
		logging.Error("proxy.proxyNew2Proxy() Errors: ", err.Error())
		return p
	}
	return Proxy{
		Ip:         pn.Ip,
		Port:       util.Atoi(pn.Port),
		ExpireTime: time.Now().Add(time.Duration(to) * time.Second).Format("2006-01-02 15:04:05"),
	}
}
