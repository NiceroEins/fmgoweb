package spider

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"fmt"
	"github.com/jinzhu/gorm"
	"regexp"
	"strconv"
	"time"
)

type News struct {
	models.Simple

	SeedID      int64      `gorm:"seed_id" json:"seed_id" description:"新闻源ID"`
	Title       string     `json:"title" description:"标题（问答中为问题内容）"`
	TitleTime   string     `json:"-" description:"标题时间（问答中为提问时间）"`
	Link        string     `json:"link" description:"链接"`
	Content     string     `json:"content" description:"内容（问答中为回答内容）"`
	ContentTime string     `json:"content_time" description:"内容时间（问答中为回答时间）"`
	IndexID     int64      `gorm:"index_id" json:"-" description:"索引id（问答中为question id）"`
	Object      string     `json:"-" description:"相关对象（问答中为证券代码）"`
	Type        string     `json:"type" description:"新闻类型"`
	ParentID    int64      `json:"-" description:"父级种子ID"`
	KeywordHit  int        `json:"-" description:"关键词命中情况（0-不中/1-命中）"`
	Cata        string     `json:"-" description:"分类字段"`
	Author      string     `json:"-" description:"作者"`
	CatchTime   *time.Time `json:"-" description:"爬虫开始时间"`
	SpiderTime  *time.Time `json:"-" description:"爬虫完成时间"`
	Correlation *int       `json:"-"`
	Duration    *int64     `json:"-"`
}

type NewsContent struct {
	Title               string           `json:"title,omitempty"`
	Link                string           `json:"link,omitempty"`
	TitleTime           string           `json:"title_time,omitempty"`
	SeedID              string           `json:"seed_id"`
	Cata                string           `json:"cata,omitempty"`
	CatchTime           string           `json:"catch_time"`
	SpiderTime          string           `json:"spider_time,omitempty"`
	Type                string           `json:"type"`
	IndexId             string           `json:"index_id,omitempty"`
	Content             string           `json:"content,omitempty"`
	ContentTime         string           `form:"content_time" json:"content_time,omitempty"`
	Object              string           `form:"object" json:"object,omitempty"`
	Author              string           `form:"author" json:"author,omitempty"`
	Forecast            Forecast         `json:"forecast,omitempty"`
	Announcement        Announcement     `json:"announcement,omitempty"`
	Disclosure          Disclosure       `json:"disclosure,omitempty"`
	IWC                 IWC              `json:"iwc,omitempty"`
	HDY                 HDY              `json:"qa-hdy,omitempty"`
	WB                  WB               `json:"wb,omitempty"`
	WBDetail            WBDetail         `json:"wb-detail,omitempty"`
	ResearchList        ResearchList     `json:"research-list,omitempty"`
	ResearchDetail      ResearchDetail   `json:"research-detail,omitempty"`
	ResearchPDF         ResearchPDF      `json:"research-pdf,omitempty"`
	IpoDcList           IpoDc            `json:"ipo-dc-list,omitempty"`
	IpoTycList          IpoTycList       `json:"ipo-tyc-list,omitempty"`
	IpoTycDetail        IpoTycDetail     `json:"ipo-tyc-detail,omitempty"`
	PopularStock        PopularStock     `json:"popular-stock,omitempty"`
	WeChatList          WeChatList       `json:"wechat-list,omitempty"`
	WeChatDetail        WeChatDetail     `json:"wechat-detail,omitempty"`
	Reorganization      Reorganization   `json:"reorganization,omitempty"`
	YbDcIndustryDetail  IndustryResearch `json:"yb-dc-industry-detail,omitempty"`
	YbHbIndustry        IndustryResearch `json:"yb-hb-industry,omitempty"`
	IpoTycSearchList    IpoTycSearch     `json:"ipo-tyc-list-search,omitempty"`
	IpoTycDetailSearch  IpoTycDetail     `json:"ipo-tyc-detail-search,omitempty"`
	CninfoNoticList     CninfoNoticList  `json:"cninfo-notice-list,omitempty"`
	Twitter             Twitter          `json:"twitter,omitempty"`
	AuctionList         AuctionList      `json:"auction,omitempty"`
	FuturesInfo         FuturesInfoList  `json:"futures-info,omitempty"`
	FuturesSetprice     FuturesInfoList  `json:"futures-setprice,omitempty"`
	FutureUp            FuturesInfoList  `json:"future-up,omitempty"`
	InvestigationIO     InvestigationIO  `json:"cninfo-investigation-list,omitempty"`
	InvestigationSHIO   InvestigationIO  `json:"sh-investigation,omitempty"`
	InvestigationDetail InvestigationIO  `json:"investigation-detail,omitempty"`
	ForecastTHS         Forecast         `json:"forecast-ths,omitempty"`
}

func (News) TableName() string {
	return "u_feed"
}

type NewsEx struct {
	News
	Category          string `json:"-"`
	CatchSite         string `json:"-"`
	CatchPath         string `json:"-"`
	TitleRaw          string `json:"title_raw" gorm:"-"`
	ContentRaw        string `json:"content_raw" gorm:"-"`
	Source            string `json:"source" gorm:"-"`
	CatchLink         string `json:"source_link"`
	Name              string `json:"name"`
	Code              string `json:"code"`
	Year              string `json:"year" gorm:"-"`
	Date              string `json:"date" gorm:"-"`
	Time              string `json:"time" gorm:"-"`
	IsCopy            bool   `json:"is_copy_clicked" gorm:"-"`
	IsLink            bool   `json:"is_link_clicked" gorm:"-"`
	IsRecommend       bool   `json:"is_recommend_clicked" gorm:"-"`
	IsHighlyRecommend bool   `json:"is_highly_recommend_clicked" gorm:"-"`
	NamedEntity       string `json:"named_entity"`
	SentimentAnalysis int    `json:"sentiment"`
	SentimentNew      int    `json:"-"`
}

func (NewsEx) TableName() string {
	return "u_feed"
}

type HDY struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	TitleTime   string `json:"title_time"`
	Author      string `json:"author,omitempty"`
	QId         string `json:"q_id"`
	Content     string `json:"content,omitempty"`
	ContentTime string `form:"content_time" json:"content_time,omitempty"`
	Object      string `form:"object" json:"object,omitempty"`
}

type WB struct {
	ID  string `json:"id"`
	UId string `json:"uid"`
}

type WBDetail struct {
	Title     string `json:"title"`
	TitleTime string `json:"title_time,omitempty"`
	Link      string `json:"link,omitempty"`
	Author    string `form:"author" json:"author,omitempty"`
}

// policy: 展示策略（含关键词、类目）
// bCount: 是否需要count总数
// index: 起始偏移量（id）
// offset： 翻页时首个展示结果在结果集中的偏移量
// size: 翻页时的单页条数
// direction: 搜索方向
// ts: 可选字段，搜索时间范围
func FetchNews(policy *NewsPolicy, bCount bool, index, offset, size int, direction string, ts ...string) ([]NewsEx, int, error) {
	var news []NewsEx
	var bOptimize bool
	var maxID int
	var optimizeValue int = 800000
	var err error
	var fastIdx int64

	// 0715 optimize:
	// using STRAIGHT_JOIN
	dbs := models.DB().Select("u_seed.cata AS category, u_seed.catch_site, u_seed.catch_path, u_seed.catch_link, b_stock_names.name, b_stock_names.code, b_sentiment_correction.sentiment_new, u_feed.*").
		Joins("STRAIGHT_JOIN u_seed ON u_seed.id = u_feed.seed_id").
		Joins("LEFT JOIN b_stock_names ON u_feed.object = b_stock_names.code").
		Joins("LEFT JOIN b_sentiment_correction ON b_sentiment_correction.feed_id = u_feed.id")

	// 0604 fix: duplicate news
	if policy.Duplication == 1 {
		dbs = dbs.Where("u_feed.parent_id = 0")
	}
	// 0609 fix: bug #11
	dbs = dbs.Where("u_seed.status = ?", "normal").Where("u_seed.deleted = 0")

	switch direction {
	case "up":
		dbs = dbs.Where("u_feed.id > ?", index)
		maxID = MaxID()
		if maxID > 0 && index > maxID-optimizeValue {
			bOptimize = true
		}
	case "down":
		if index > 0 {
			dbs = dbs.Where("u_feed.id < ?", index)
		}
	default:
	}

	// 0618 add fast-index
	bFastIndex := false
	if index == 0 && offset == 0 {
		bFastIndex = true
	}

	if bFastIndex {
		fastIdx, err = FastIndex(policy, size, ts)
		if err == nil {
			if maxID == 0 {
				maxID = MaxID()
			}
			if maxID > 0 && int(fastIdx) > maxID-optimizeValue {
				bOptimize = true
			}
		}
	}

	// 0616 add time-range selection
	var integrity, partial []string
	if len(ts) > 0 {
		if len(ts) < 2 {
			ts = append(ts, time.Now().Format("2006-01-02 15:04:05"))
		}
		// fast-count
		integrity, partial = SplitRange(ts)
		startID := StartID(ts[0])
		if maxID == 0 {
			maxID = MaxID()
		}
		if maxID > 0 && startID > maxID-optimizeValue {
			bOptimize = true
			dbs = dbs.Where("u_feed.id >= ?", startID)
		}
	}

	// 0604 fix: empty keywords
	// 0612 fix again
	if len(policy.CustomKeyword) > 0 {
		var query string
		ts := []string{"u_feed.title", "u_feed.content", "u_seed.catch_path"}
		bFirst := true
		for i, k := range []string{"news", "qa", "source"} {
			//var match string
			keyWords, ok := policy.Keyword[k]
			if keyWords == nil || !ok {
				continue
			}
			if !bFirst {
				query += " OR "
			}

			var fIdx, lIdx int
			var fMatch, lMatch, nMatch string
			for k, v := range keyWords {
				cate, _ := policy.keywordCategory[v]
				switch {
				case cate == "like" || bOptimize:
					if lIdx > 0 {
						switch policy.Mode {
						case "or":
							lMatch += " OR "
						case "and":
							lMatch += " AND "
						}
					}
					lMatch += fmt.Sprintf("%s LIKE '%%%s%%'", ts[i], v)
					lIdx++
				case cate == "fulltext" && !bOptimize:
					if fIdx > 0 {
						fMatch += " "
					}
					switch policy.Mode {
					case "or":
						fMatch += v
					case "and":
						fMatch += "+" + v
					}
					fIdx++
				}
				if k > 0 {
					nMatch += " OR "
				}
				nMatch += fmt.Sprintf("b_stock_names.name LIKE '%%%s%%'", v)
			}
			if fIdx > 0 {
				query += fmt.Sprintf("(MATCH(%s) AGAINST ('%s' IN BOOLEAN MODE))", ts[i], fMatch)
				if lIdx > 0 {
					switch policy.Mode {
					case "or":
						query += " OR "
					case "and":
						query += " AND "
					}
				}
			}
			if lIdx > 0 {
				query += fmt.Sprintf("(%s)", lMatch)
			}
			if policy.entry == "search" {
				query += fmt.Sprintf(" OR (%s)", nMatch)
			}

			bFirst = false
		}
		dbs = dbs.Where(query).Where(GenerateNewsTypeQuery(policy))
	} else {
		dbs = dbs.Where(GenerateNewsPolicyQuery(policy))
	}

	// 0715 add correlation:
	if policy.NLPCorrelation == 1 {
		dbs = dbs.Where("u_seed.hidden = 1 OR u_feed.correlation > 0 OR u_feed.correlation IS NULL")
	}
	if policy.NLPDuplication == 1 {
		dbs = dbs.Where("u_feed.duplication_id = '0' OR u_feed.duplication_id IS NULL OR u_feed.duplication_id = ''")
	}

	// 0702 add additional
	if len(policy.Additional) > 0 {
		dbs = dbs.Where(policy.Additional)
	}

	// 0204 只展示3天内新闻
	dbs = dbs.Where("u_feed.content_time = '' OR DATEDIFF(u_feed.created_at,u_feed.content_time) < ? OR u_feed.type IN ('qa', 'q')", 4)

	// debug
	if setting.ServerSetting.RunMode == "debug" {
		dbs = dbs.Debug()
	}

	cnt := 0
	if bCount {
		if len(integrity) != 0 {
			if c1, err := FastCount(dbs.Scopes(createdBetween(integrity)), policy, integrity); err != nil {
				return nil, 0, err
			} else {
				cnt += c1
			}
		}

		c2 := 0
		if err := dbs.Model(NewsEx{}).Scopes(createdBetween(partial)).Count(&c2).Error; err != nil {
			return nil, 0, err
		} else {
			cnt += c2
		}
	}

	if bFastIndex && fastIdx > 0 {
		dbs = dbs.Where("u_feed.id >= ?", fastIdx)
	}
	if err := dbs.Scopes(createdBetween(ts)).Order("u_feed.id DESC").Offset(offset).Limit(size).Find(&news).Error; err != nil {
		return nil, 0, err
	} else if bFastIndex {
		// 返回数据不少于单页条数
		// 才可以使用fast-index
		if len(news) > 0 && len(ts) == 0 || size > 0 && len(news) >= size {
			_ = SetFastIndex(policy, size, ts, int64(news[len(news)-1].ID))
		}
	}

	return news, cnt, nil
}

func GenerateNewsPolicyQuery(policy *NewsPolicy) string {
	var ret string
	// 0610 add: policy news
	switch policy.Hidden {
	case "all":
		ret += "u_seed.show_in_home = 1 AND "
	case "condition":
		ret += "u_seed.show_in_home = 1 OR "
	case "non":
	}

	if len(policy.NewsType) == 0 {
		ret += "u_feed.keyword_hit = 1"
		return ret

	}

	ret += GenerateNewsTypeQuery(policy)
	return ret
}

func GenerateNewsTypeQuery(policy *NewsPolicy) string {
	var ret string
	bFirst := true
	for k, v := range policy.NewsType {
		if k > 0 {
			ret += " OR "
			//bFirst = false
		}
		//微博：208
		//淘股吧：211
		//互动提问：209  不用
		//互动回答：11  不用
		switch v {
		case "tgb":
			ret += "u_seed.cata = '#211#'"
		case "wb":
			ret += "u_seed.cata = '#208#'"
		case "qa":
			ret += "u_feed.type = 'qa'"
		case "q":
			ret += "u_feed.type = 'q'"
		case "wechat-detail", "wechat-source":
			ret += "u_feed.type = 'wechat-detail'"
		case "news":
			ret += "u_seed.cata NOT IN('#211#', '#208#') AND u_feed.type NOT IN('qa', 'q')"
		case "a":
			ret += "u_feed.type = 'qa' AND u_feed.content IS NULL"
		case "twitter":
			ret += "u_feed.type = 'twitter'"
		default:
			if !bFirst {
				ret += "0"
			}
			//ret += fmt.Sprintf("u_feed.type = '%s'", v)
		}
	}
	return ret
}

func InsertNews(news *News) error {
	news.ContentTime = CheckTitleTime(news.TitleTime)
	logging.Info("checkout content_time ", news.ContentTime, news.TitleTime)
	err := models.DB().Create(news).Error
	return err
}

func UpdateHDY(hdy *HDY) error {
	err := models.DB().Model(&News{
		Simple:  models.Simple{ID: hdy.ID},
		IndexID: int64(util.Atoi(hdy.QId)),
	}).Updates(HDY2News(hdy)).Error
	return err
}

func Content2News(content *NewsContent, parentID int64, keywordHit int) News {
	ct, _ := time.ParseInLocation("2006-01-02 15:04:05", content.CatchTime, time.Local)
	st, _ := time.ParseInLocation("2006-01-02 15:04:05", content.SpiderTime, time.Local)
	return News{
		SeedID:      int64(util.Atoi(content.SeedID)),
		Title:       content.Title,
		TitleTime:   content.TitleTime,
		Link:        content.Link,
		Content:     content.Content,
		ContentTime: content.ContentTime,
		IndexID:     int64(util.Atoi(content.IndexId)),
		Object:      content.Object,
		Type:        content.Type,
		ParentID:    parentID,
		KeywordHit:  keywordHit,
		Cata:        content.Cata,
		Author:      content.Author,
		CatchTime:   &ct,
		SpiderTime:  &st,
	}
}

func HDY2News(hdy *HDY) News {
	return News{
		TitleTime:   hdy.TitleTime,
		ContentTime: hdy.ContentTime,
	}
}

func ParseWBDetail(content *NewsContent) *NewsContent {
	if content.WBDetail == (WBDetail{}) {
		return nil
	}
	content.Title = content.WBDetail.Title
	content.TitleTime = content.WBDetail.TitleTime
	content.Link = content.WBDetail.Link
	content.Author = content.WBDetail.Author
	return content
}

func createdBetween(timeRange []string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if timeRange != nil && len(timeRange) == 2 {
			return db.Where("u_feed.created_at BETWEEN ? AND ?", timeRange[0], timeRange[1])
		} else {
			return db
		}
	}
}

// 对title_time 进行正则处理
func CheckTitleTime(str string) string {
	patternDate := `\d{4}[\/年\-.]\d{2}[\/月\-.]\d{2}`
	compileDate := regexp.MustCompile(patternDate)
	dateStr := compileDate.FindAllStringSubmatch(str, -1)
	if len(dateStr) > 0 && len(dateStr[0]) > 0 {
		if ok, _ := regexp.Match("年", []byte(dateStr[0][0])); ok {
			tmp, err := time.Parse("2006年01月02", dateStr[0][0])
			if err != nil {
				return time.Now().Format(util.YMD)
			}
			return tmp.Format(util.YMD)
		}
		tmp, err := time.Parse("2006-01-02", dateStr[0][0])
		if err != nil {
			tmp, err = time.Parse("2006.01.02", dateStr[0][0])
			if err != nil {
				tmp, err = time.Parse("2006/01/02", dateStr[0][0])
				if err != nil {
					return time.Now().Format(util.YMD)
				}
				return tmp.Format(util.YMD)
			}
			return tmp.Format(util.YMD)
		}
		return tmp.Format(util.YMD)
	}
	patternDateTime := `\d{2}[/月\-\.]\d{2}`
	compileTime := regexp.MustCompile(patternDateTime)
	timeStr := compileTime.FindAllStringSubmatch(str, -1)
	if len(timeStr) > 0 && len(timeStr[0]) > 0 {
		if ok, _ := regexp.Match("月", []byte(timeStr[0][0])); ok {
			tmp, err := time.Parse("2006年01月02", strconv.Itoa(time.Now().Year())+"年"+timeStr[0][0])
			if err != nil {
				return time.Now().Format(util.YMD)
			}
			return tmp.Format(util.YMD)
		}
		return strconv.Itoa(time.Now().Year()) + "-" + timeStr[0][0]
	}
	return time.Now().Format(util.YMD)
}

// 获取最近三天的交易日
func GetTradeDate() []string {
	var yesterday []time.Time
	var ret []string
	date := time.Now().Format("2006-01-02")
	if err := models.DB().Table("b_trade_date").Where("trade_date <= ?", date).
		Order("trade_date DESC").Limit(3).Pluck("trade_date", &yesterday).Error; err != nil {
		logging.Error("stock.blockOpen() fetching yesterday Errors: ", err.Error())
		return []string{}
	}
	if len(yesterday) < 3 {
		return []string{}
	}
	for _, value := range yesterday {
		ret = append(ret, value.Format(util.YMD))
	}
	ret = append(ret, "") //兼容之前的数据
	return ret
}

func MaxID() int {
	var ret []int
	models.DB().Select("MAX(id) AS mid").Table("u_feed").Pluck("mid", &ret)
	if len(ret) > 0 {
		return ret[0]
	}
	return 0
}

func StartID(tsBegin string) int {
	var ret []int
	models.DB().Select("MIN(id) AS mid").Table("u_feed").Where("created_at >= ?", tsBegin).Pluck("mid", &ret)
	if len(ret) > 0 {
		return ret[0]
	}
	return 0
}
