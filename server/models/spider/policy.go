package spider

import (
	"datacenter/pkg/util"
	"sort"
	"strings"
	"unicode/utf8"
)

type NewsPolicy struct {
	Hidden          string              //non-不隐藏， all-凡具备hidden属性均隐藏， condition-关联条件隐藏
	CustomKeyword   []string            //用户关键词
	Keyword         map[string][]string //关键词列表
	NewsType        []string            //新闻类型列表
	MarkWord        map[string][]string //标记关键词列表
	Additional      string              //额外项
	Mode            string              // or-或（默认）/ and-且
	NLPCorrelation  int                 //NLP二分类筛选器是否使用 0-否/1-是
	NLPDuplication  int                 //NLP去重筛选器是否使用，0-否/1-是
	Duplication     int                 //去重筛选器是否使用，0-否/1-是
	init            bool
	seps            []string          //分隔符
	trims           []string          //预处理时需清洗的特殊字符
	keywordCategory map[string]string //关键词分类(like/fulletext)
	entry           string            //入口
	method          string            // fulltext-全文索引（默认） like-关键词匹配
}

func (p *NewsPolicy) Initialize() bool {
	if p.init == false {
		p.Keyword = make(map[string][]string)
		p.MarkWord = make(map[string][]string)
		p.keywordCategory = make(map[string]string)
		p.init = true
		return false
	}
	return true
}

func (p *NewsPolicy) Default(uid string) *NewsPolicy {
	if p.Initialize() {
		return p
	}

	p.Keyword["qa"] = GetKeyword("qa", uid)
	p.Keyword["news"] = GetKeyword("news", uid)
	p.MarkWord["qa"] = GetKeyword("qa", uid)
	p.MarkWord["news"] = GetKeyword("news", uid)

	p.SetTrim("\n", "\r").SetSep(",", "，", " ", "\t").SetMode("or")
	return p
}

func (p *NewsPolicy) Rule(scene string) *NewsPolicy {
	switch scene {
	case "roll":
		//滚动新闻
		return p.SetDup(1).SetNLPCor(1).SetNLPDup(1)
	case "search":
		return p.SetDup(1).SetNLPCor(1).SetNLPDup(1)
	case "all":
		return p.SetDup(0).SetNLPCor(0).SetNLPDup(0)
	default:
		return p
	}
}

func (p *NewsPolicy) SetSep(sep ...string) *NewsPolicy {
	if len(sep) > 0 {
		p.seps = sep
	}
	return p
}

func (p *NewsPolicy) SetTrim(trim ...string) *NewsPolicy {
	if len(trim) > 0 {
		p.trims = trim
	}
	return p
}

func (p *NewsPolicy) SetMode(mode string) *NewsPolicy {
	if len(mode) > 0 {
		p.Mode = mode
	}
	return p
}

func (p *NewsPolicy) SetMethod(method string) *NewsPolicy {
	if len(method) > 0 {
		p.method = method
	}
	return p
}

func (p *NewsPolicy) SetDup(duplication int) *NewsPolicy {
	p.Duplication = duplication
	return p
}

func (p *NewsPolicy) SetNLPDup(nlpDup int) *NewsPolicy {
	p.NLPDuplication = nlpDup
	return p
}

func (p *NewsPolicy) SetNLPCor(nlpCor int) *NewsPolicy {
	p.NLPCorrelation = nlpCor
	return p
}

func (p *NewsPolicy) SetKeywords(category string, keywords []string) *NewsPolicy {
	v := util.Clear(keywords)
	sort.Strings(v)
	for _, u := range v {
		p.keywordCategory[u] = category
	}
	return p
}

func (p *NewsPolicy) GetKeywords() []string {
	var ret []string
	for k, _ := range p.keywordCategory {
		ret = append(ret, k)
	}
	return ret
}

func (p *NewsPolicy) SetEntry(entry string) *NewsPolicy {
	p.entry = entry
	return p
}

func (p *NewsPolicy) GetEntry() string {
	return p.entry
}

// 当关键词和类目均为空时，问答仅展示问答属性关键词，其它仅展示normal属性预置关键词；
// 当关键词为空，类目为问答时，展示问答所有内容，标记问答属性预置关键词；
// 当关键词为空，类目为非问答类目时，展示非问答所有内容，标记normal属性预置关键词；
// 当关键词不为空时，展示用户关键词，预置词按关键词属性一并标记。
// hidden: non-不隐藏， all-凡具备hidden属性均隐藏， condition-关联条件隐藏
func (p *NewsPolicy) Do(uid string) {
	p.Default(uid)

	keywords := p.CustomKeyword
	newsType := p.NewsType
	if len(keywords) > 0 {
		if util.ContainsAny(newsType, "source") {
			p.Keyword["source"] = keywords
			p.MarkWord["source"] = keywords
			p.Rule("all")
		}

		if util.ContainsAny(newsType, "wechat-source") {
			p.Keyword["source"] = keywords
			p.Rule("all")
			//p.MarkWord["wechat-source"] = keywords
		}
		if util.ContainsAny(newsType, "qa", "q", "wechat-detail") || len(newsType) == 0 {
			p.Keyword["qa"] = keywords
			p.MarkWord["qa"] = append(p.MarkWord["qa"], keywords...)
		} else if util.ContainsAny(newsType, "wechat-detail") {
			//微信
			p.Keyword["qa"] = keywords
			p.MarkWord["qa"] = append(GetKeyword("qa", uid), GetKeyword("news", uid)...)
			p.MarkWord["qa"] = append(p.MarkWord["qa"], keywords...)
		} else {
			delete(p.Keyword, "qa")
		}

		if util.ContainsAny(newsType, "news", "qa", "q", "tgb", "wb") || len(newsType) == 0 {
			// 用户关键词
			p.Keyword["news"] = keywords
			p.MarkWord["news"] = append(p.MarkWord["news"], keywords...)
		} else if util.ContainsAny(newsType, "wechat-detail") {
			//微信
			p.Keyword["news"] = keywords
			p.MarkWord["news"] = append(GetKeyword("news", uid), GetKeyword("qa", uid)...)
			p.MarkWord["news"] = append(p.MarkWord["news"], keywords...)
		} else {
			delete(p.Keyword, "news")
		}

		p.Hidden = "non"
	} else {
		if len(newsType) == 0 {
			p.Hidden = "condition"
		} else {
			p.Hidden = "non"
		}

	}
	p.clear()
	//return p
}

// 清洗并提取关键词至切片
func (p *NewsPolicy) SetType(newsType string) *NewsPolicy {
	p.NewsType = p.split(newsType)
	return p
}

// 清洗并提取关键词至切片
// 0609 fix: bug #7
func (p *NewsPolicy) SetKeyword(keyword string) *NewsPolicy {
	// 0702 add: correlation (unrelated)
	keywords := p.split(keyword)
	idx := specialTagIndex(keywords)
	if len(idx) > 0 {
		for k, v := range idx {
			if k > 0 {
				p.Additional += " and "
			}
			raw := strings.Replace(keywords[v], "::", "", -1)
			switch raw {
			case "unrelated":
				p.Additional += "u_feed.correlation = 0"
			case "related":
				p.Additional += "u_feed.correlation > 0"
			case "important":
				p.Additional += "u_feed.correlation = 2"
			default:
				p.Additional += "u_feed." + raw
			}
			keywords[v] = ""
		}

	}

	var like, fulltext, all []string
	for k, v := range keywords {
		if v == "且" || v == "and" {
			p.Mode = "and"
			keywords[k] = ""
		} else if v == "或" || v == "or" {
			p.Mode = "or"
			keywords[k] = ""
		} else {
			// keyword more than 1 word
			// use fulltext as default
			if v != "" && utf8.RuneCountInString(v) <= 1 || util.CountDigit(v) > 4 || util.IsLetter(v) {
				//p.method = "like"
				like = append(like, v)
			} else {
				fulltext = append(fulltext, v)
			}
			all = append(all, v)
		}

	}
	switch p.method {
	case "like":
		p.SetKeywords("like", like)
		p.SetKeywords("like", fulltext)
	case "fulltext":
		p.SetKeywords("fulltext", like)
		p.SetKeywords("fulltext", fulltext)
	default:
		p.SetKeywords("like", like)
		p.SetKeywords("fulltext", fulltext)
	}
	keywords = util.Clear(keywords)
	//p.SetKeywords("like", like)
	//p.SetKeywords("fulltext", fulltext)
	//p.SetKeywords("all", all)

	p.CustomKeyword = keywords
	return p
}

func (p *NewsPolicy) split(src string) []string {
	ret := src
	for _, v := range p.trims {
		ret = strings.Replace(ret, v, "", -1)
	}
	sep := " "
	if len(p.seps) > 0 {
		sep = p.seps[0]
	}
	for _, v := range p.seps {
		ret = strings.Replace(ret, v, sep, -1)
	}
	return util.SplitAndClear(ret, sep)
}

func (p *NewsPolicy) clear() {
	//keys := []string{"news", "qa", "source"}
	for k, v := range p.Keyword {
		p.Keyword[k] = util.Clear(v)
	}
	for k, v := range p.MarkWord {
		p.MarkWord[k] = util.Clear(v)
	}
	//for _, v := range keys {
	//	if k, ok := p.Keyword[v]; ok {
	//		p.Keyword[v] = util.Clear(k)
	//	}
	//	if k, ok := p.MarkWord[v]; ok {
	//		p.MarkWord[v] = util.Clear(k)
	//	}
	//}
}

func specialTagIndex(words []string) []int {
	ret := make([]int, 0)
	for k, v := range words {
		if strings.HasPrefix(v, "::") {
			ret = append(ret, k)
		}
	}
	return ret
}
