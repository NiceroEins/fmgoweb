package spider

import (
	"datacenter/pkg/gredis"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"gopkg.in/go-playground/validator.v9"
	//"os"
	"regexp"
	"strconv"
)

//日志
type Logs struct {
	Size int
	Data []interface{}
}

type ElemReport struct {
	ID     string `form:"id" validate:"required,max=32"`
	Result string `form:"id" validate:"required,ok|fail|empty|missing"`
}

//type ReportData struct {
//	Forecast     []Forecast     `json:"forecast,omitempty"`
//	Announcement []Announcement `json:"announcement,omitempty"`
//}

//request 结构体映射
type Report struct {
	ID        string        `form:"id" validate:"required,max=64"`
	Type      *int          `form:"type" validate:"required,gte=100"`
	Code      *int          `form:"code" validate:"required,gte=0,lt=600"`
	Timestamp *int64        `form:"timestamp" validate:"omitempty"`
	Msg       string        `form:"msg" validate:"max=500"`
	Content   []NewsContent `form:"content" validate:"omitempty"`
	//Data      ReportData    `form:"data" validate:"omitempty"`
	Selector []ElemReport `form:"selector" validate:"omitempty"`
}

//存入redis ：map全部
//var ReportSeedMap sync.Map  //seedID (int), confidence (int, 0 is the best)
//var ReportProxyMap sync.Map //proxyAddress (string), confidence (int, 0 is the best)
//var UnusedSeedMap sync.Map  //unused seed

var ReportLog Logs

func CheckReport(fl validator.FieldLevel) bool {
	ok, _ := regexp.MatchString("\\w+-\\d+", fl.Field().String())
	return ok
}

//移除seed
func RemoveSeed(seedID int, report *Report) bool {
	gredis.HDel("SeedMap", strconv.Itoa(seedID))
	gredis.HDel("ReportSeedMap", strconv.Itoa(seedID))
	//结构体转json
	reportJson, err := json.Marshal(report)
	if err != nil {
		fmt.Println("RemoveSeed 结构体转json失败:", err.Error())
	}
	gredis.HSet("UnusedSeedMap", strconv.Itoa(seedID), string(reportJson))

	return true
}

//从redis UnusedSeedMap hash 中获取 key集合,value集合
func GetReport() ([]int, []*Report) {

	seeds := make([]int, 0)
	ret := make([]*Report, 0)

	//改造,从redis中获取
	UnusedSeedMapData, err := gredis.HGetAll("UnusedSeedMap")
	if err != nil {
		fmt.Printf("GetReport UnusedSeedMap redis中获取数据失败:", err.Error())
	}

	count := 0
	for _, v := range UnusedSeedMapData {

		if count%2 == 0 {
			// redis hash key
			key, err := strconv.Atoi(string(v.([]byte)))
			fmt.Println("key :", key)
			if err != nil {
				fmt.Println("GetReport 字符串转数字失败", err.Error())
			}
			seeds = append(seeds, key)
		} else {
			//redis hash value
			r := Report{}
			err := json.Unmarshal(v.([]byte), &r)
			if err != nil {
				fmt.Println("Umarshal failed:", err)
			}
			ret = append(ret, &r)
		}
		count++
	}

	return seeds, ret
}

func GetLog(n int) []interface{} {
	end := len(ReportLog.Data)
	begin := util.Max(len(ReportLog.Data)-n+1, 0)
	return ReportLog.Data[begin:end]
}
