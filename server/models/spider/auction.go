package spider

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
)

type AuctionList struct {
	OfferCount   string    `json:"offer_count"  gorm:"column:offer_count"`
	Status       int       `json:"status"  gorm:"column:status"`
	Link         string    `json:"link"  gorm:"column:link"`
	Title        string    `json:"title"  gorm:"column:title"`
	Keyword      string    `json:"keyword"  gorm:"column:keyword"`
	Platform     string    `json:"platform"  gorm:"column:platform"`
	ShareRatio   decimal.NullDecimal    `json:"share_ratio"  gorm:"column:share_ratio"`
	SpiderTime   string    `json:"spider_time"  gorm:"column:spider_time"`
	StartTime    string    `json:"start_time,omitempty"  gorm:"column:start_time"`
}
type AuctionPush struct {
	TotalShareRatio   decimal.NullDecimal    `json:"share_ratio"  gorm:"column:share_ratio"`
}
func (AuctionList) TableName() string {
	return "u_auction"
}

func (auc *AuctionList) Save() (bool,error) {
	// 获取之前的记录
	preAuctionList := AuctionList{}
	dbs := models.DB().Table("u_auction").Where("title = ? ",auc.Title).Where("platform = ? ",auc.Platform)
	if auc.StartTime != "" {
		dbs = dbs.Where("start_time = ? ",auc.StartTime)
	}
	err := dbs.Order("id desc").First(&preAuctionList).Error
	if err != nil && err != gorm.ErrRecordNotFound  {
		logging.Info("auction save err",err)
		return false,err
	}
	// 已结束的
	if preAuctionList.Keyword != "" && preAuctionList.Status == 2 {
		logging.Info("auction save status is 2")
		return false,nil
	}
	if preAuctionList.Keyword != "" && (preAuctionList.OfferCount == auc.OfferCount && preAuctionList.Status == auc.Status ){
		return false,nil
	}
	db := models.DB()
	err = db.Where(&AuctionList{
		Title:  auc.Title,
		Platform: auc.Platform,
		StartTime: auc.StartTime,
	}).Assign(auc).FirstOrCreate(&AuctionList{}).Error
	logging.Info("auction save err",err)
	return true,err
}
