package spider

import (
	"datacenter/models"
	"datacenter/models/industry"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"time"
)

type FuturesInfoList struct {
	Name            string          `json:"name" gorm:"column:name"`
	Code            string          `json:"code"  gorm:"column:code"`
	Mechanism       string          `json:"mechanism" gorm:"column:mechanism"`
	Time            string          `json:"time" gorm:"column:time"`
	Open            decimal.Decimal `json:"open" gorm:"column:open"`
	Close           decimal.Decimal `json:"close" gorm:"column:close"`
	High            decimal.Decimal `json:"high" gorm:"column:high"`
	Low             decimal.Decimal `json:"low" gorm:"column:low"`
	Volume          decimal.Decimal `json:"volume" gorm:"column:volume"`
	Money           decimal.Decimal `json:"money" gorm:"column:money"`
	StopRange       decimal.Decimal `json:"stop_range" gorm:"column:stop_range"`
	SettlementPrice decimal.Decimal `json:"settlement_price" gorm:"column:settlement_price"`
}
type FuturesInfo struct {
	Code string    `json:"code"  gorm:"column:code"`
	Time time.Time `json:"time" gorm:"column:time"`
}

func (FuturesInfoList) TableName() string {
	return "u_futures_info"
}

const IndustryEventName = "行业事件"
const EventName = "期货异动"

func (fu *FuturesInfoList) Save() error {
	fu.Time = time.Now().Format(util.YMDHM)
	err := models.DB().Where(&FuturesInfoList{
		Code: fu.Code,
		Time: time.Now().Format(util.YMDHM),
	}).Assign(fu).FirstOrCreate(&FuturesInfoList{}).Error
	return err
}

func (fu *FuturesInfoList) SaveDetail() error {
	// 获取之前的结算价
	futuresList := FuturesInfo{}
	fu.Time = time.Now().Format(util.YMDHM)
	err := models.DB().Table("u_futures_info").Where("code = ? ", fu.Code).Where("time <= ? ", fu.Time).
		Order("time desc").First(&futuresList).Error
	if err != nil {
		logging.Info("futuresinfo pushRule err is ", err)
		return err
	}
	err = models.DB().Where(&FuturesInfoList{
		Code: fu.Code,
		Time: futuresList.Time.Format(util.YMDHM),
	}).Assign(fu).FirstOrCreate(&FuturesInfoList{}).Error
	return err
}

func (fu *FuturesInfoList) PushStopRangeRule() map[string]interface{} {
	var (
		returnData map[string]interface{}
	)
	pushFuturesInfoKey := "futuresinfo"
	var err error
	ret := checkPush(pushFuturesInfoKey, fu.Code)
	if ret {
		return returnData
	}
	// 获取之前的结算价
	futuresList := FuturesInfoList{}
	err = models.DB().Table("u_futures_info").Where("code = ? ", fu.Code).Where("time < ? ", fu.Time).
		Where("settlement_price > 0 ").
		Order("time desc").First(&futuresList).Error
	if err != nil {
		logging.Info("futuresinfo pushRule err is ", err)
		return returnData
	}
	// 上一个交易日结算价为空 就返回
	if futuresList.SettlementPrice.IsZero() {
		logging.Info("futuresinfo getSettlementPirce err is ", err)
		return returnData
	}
	// 获取涨停板 读取不为0的最后一条
	futures := FuturesInfoList{}
	err = models.DB().Table("u_futures_info").Where("code = ? ", fu.Code).Where("time <= ? ", fu.Time).Where("stop_range != ? ", 0).
		Order("id desc").First(&futures).Error
	if err != nil {
		logging.Info("futuresinfo pushRule err is ", err)
		return returnData
	}
	// 推送规则  1 > 当前涨跌幅限制 - 绝对值(（现价 - 上一日结算价）/ 上一日结算价)
	// 推送次数规则 ： 1天一个品类一次
	// 推送内容   XXX涨幅N%（XXX 为中文名）
	rate := futures.StopRange.Sub(fu.Close.Sub(futuresList.SettlementPrice).Div(futuresList.SettlementPrice).Mul(decimal.NewFromFloat(100)).Abs())
	decrease := fu.Close.Sub(futuresList.SettlementPrice).Div(futuresList.SettlementPrice).Mul(decimal.NewFromFloat(100)).Round(2)
	if rate.Sub(decimal.NewFromFloat(1)).Sign() < 0 {
		data := make(map[string]interface{})
		data["type"] = "futures_info"
		data["push_time"] = time.Now().Format("2006-01-02")
		if decrease.Sign() > 0 {
			data["content"] = fmt.Sprintf("%s上涨%v%%", fu.Name, decrease)
			if time.Now().Hour() > 8 && time.Now().Hour() < 9 {
				data["content"] = fmt.Sprintf("昨晚%s上涨%v%%", fu.Name, decrease)
			}
			_ = SaveEventData(EventName, data["content"].(string), time.Now())
			gredis.HSet(pushFuturesInfoKey, fu.Code, time.Now().Format(util.YMDHMS))
			gredis.Expire(pushFuturesInfoKey, 9*3600)
			return data
		}
	}

	return returnData
}

// 期货价格突破三个月新高
func (fu *FuturesInfoList) ThreeMonthPush() map[string]interface{} {
	var (
		returnData map[string]interface{}
		start      time.Time
	)
	pushFuturesInfoKey := "futuresinfo"
	var err error
	ret := checkPush(pushFuturesInfoKey, fu.Code)
	if ret {
		return returnData
	}

	// 获取之前的结算价
	futuresList := FuturesInfoList{}
	start, err = time.Parse("2006-01-02", fu.Time)
	if err != nil {
		logging.Info("futuresinfo pushRule fu.time err is ", err)
		return returnData
	}
	end := start.AddDate(0, -3, 0).Format("2006-01-02")
	err = models.DB().Table("u_futures_info").Select("max(high) as high").
		Where("code = ? ", fu.Code).Where("time <= ? ", start.Format("2006-01-02")).
		Where("time >= ? ", end).
		Order("id desc").First(&futuresList).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("futuresinfo pushRule err is ", err)
		return returnData
	}
	if futuresList.High.IsZero() {
		return returnData
	}
	// 推送规则 现价 - 三个月内最高价 > 0
	// 推送次数规则 ： 1天一个品类一次
	// 推送内容   XXX涨幅N%（XXX 为中文名）

	rate := fu.Close.Sub(futuresList.High)
	if rate.Sign() >= 0 {
		data := make(map[string]interface{})
		data["type"] = "futures_info"
		data["push_time"] = time.Now().Format("2006-01-02")
		data["content"] = fmt.Sprintf("%s突破3个月新高", fu.Name)
		if time.Now().Hour() > 8 && time.Now().Hour() < 9 {
			data["content"] = fmt.Sprintf("昨晚%s突破3个月新高", fu.Name)
		}
		_ = SaveEventData(EventName, data["content"].(string), time.Now())
		gredis.HSet(pushFuturesInfoKey, fu.Code, time.Now().Format(util.YMDHMS))
		gredis.Expire(pushFuturesInfoKey, 9*3600)
		return data
	}
	return returnData
}

func (fu *FuturesInfoList) PushStopRangeRuleIncrease() map[string]interface{} {
	var (
		returnData map[string]interface{}
	)
	pushFuturesInfoKey := "futuresinfo"
	var err error
	ret := checkPush(pushFuturesInfoKey, fu.Code)
	if ret {
		return returnData
	}
	// 获取之前的结算价
	futuresList := FuturesInfoList{}
	err = models.DB().Table("u_futures_info").
		Where("code = ? ", fu.Code).Where("time < ? ", fu.Time).
		Where("settlement_price > 0 ").
		Order("time desc").First(&futuresList).Error
	if err != nil {
		logging.Info("futuresinfo pushRule err is ", err)
		return returnData
	}
	// 上一个交易日结算价为空 就返回
	if futuresList.SettlementPrice.IsZero() {
		logging.Info("futuresinfo getSettlementPirce err is ", err)
		return returnData
	}
	// 获取涨停板 读取不为0的最后一条
	futures := FuturesInfoList{}
	err = models.DB().Table("u_futures_info").Where("code = ? ", fu.Code).
		Where("time <= ? ", fu.Time).Where("stop_range != ? ", 0).
		Order("id desc").First(&futures).Error
	if err != nil {
		logging.Info("futuresinfo pushRule err is ", err)
		return returnData
	}
	// 推送规则 涨幅为 5%就推送
	rate := futures.StopRange.Sub(fu.Close.Sub(futuresList.SettlementPrice).Div(futuresList.SettlementPrice).Mul(decimal.NewFromFloat(100)).Abs())
	decrease := fu.Close.Sub(futuresList.SettlementPrice).Div(futuresList.SettlementPrice).Mul(decimal.NewFromFloat(100)).Round(2)
	if rate.Sub(decimal.NewFromFloat(1)).Sign() < 0 {
		return returnData
	}
	if decrease.Sub(decimal.NewFromFloat(5.00)).Sign() > 0 {
		data := make(map[string]interface{})
		data["type"] = "futures_info"
		data["push_time"] = time.Now().Format("2006-01-02")
		data["content"] = fmt.Sprintf("%s上涨%v%%", fu.Name, decrease)
		if time.Now().Hour() > 8 && time.Now().Hour() < 9 {
			data["content"] = fmt.Sprintf("昨晚%s上涨%v%%", fu.Name, decrease)
		}
		_ = SaveEventData(EventName, data["content"].(string), time.Now())
		gredis.HSet(pushFuturesInfoKey, fu.Code, time.Now().Format(util.YMDHMS))
		gredis.Expire(pushFuturesInfoKey, 9*3600)
		return data
	}
	return returnData
}

func (fu *FuturesInfoList) PushStopRangeRuleThreeDay() map[string]interface{} {
	var (
		returnData map[string]interface{}
	)
	pushFuturesInfoKey := "futuresinfo"
	var err error
	ret := checkPush(pushFuturesInfoKey, fu.Code)
	if ret {
		return returnData
	}
	now, _ := time.Parse(util.YMDHMS, fu.Time)
	// 获取之前的结算价
	futures := []FuturesInfoList{}
	err = models.DB().Table("u_futures_info").Where("code = ? ", fu.Code).Where("right(time,8) =  ? ", now.Format("15:04")+":00").
		Order("time desc").Limit(3).Find(&futures).Error
	if err != nil {
		logging.Info("futuresinfo pushRule err is ", err)
		return returnData
	}
	if len(futures) < 3 {
		return returnData
	}
	// 计算总的涨跌幅
	var totalChange decimal.Decimal
	for _, v := range futures {
		if v.Open.IsZero() {
			continue
		}
		change := (v.Close.Sub(v.Open)).Div(v.Open)
		totalChange = totalChange.Add(change)
	}
	futuresList := futures[2]
	// 上一个交易日结算价为空 就返回
	if futuresList.SettlementPrice.IsZero() {
		logging.Info("futuresinfo getSettlementPirce err is ", err)
		return returnData
	}

	// 推送规则 3 日总涨幅达到10%
	// decrease := fu.Close.Sub(futuresList.SettlementPrice).Div(futuresList.SettlementPrice).Mul(decimal.NewFromFloat(100)).Round(2)
	if totalChange.Sub(decimal.NewFromFloat(0.10)).Sign() > 0 {
		data := make(map[string]interface{})
		data["type"] = "futures_info"
		data["push_time"] = time.Now().Format("2006-01-02")
		data["content"] = fmt.Sprintf("%s3日上涨%v%%", fu.Name, totalChange.Mul(decimal.NewFromInt(100)))
		_ = SaveEventData(EventName, data["content"].(string), time.Now())
		gredis.HSet(pushFuturesInfoKey, fu.Code, time.Now().Format(util.YMDHMS))
		gredis.Expire(pushFuturesInfoKey, 9*3600)
		return data
	}
	return returnData
}

func checkPush(redisKey, name string) bool {
	bPush := false
	tm, err := gredis.HGetString(redisKey, name)
	if err == nil && tm != "" {
		ts, e := time.ParseInLocation("2006-01-02 15:04:05", tm, time.Local)
		if e != nil {
			logging.Error("stock.checkTime() Parse time Errors: ", e.Error())
		} else {
			if time.Now().Sub(ts) < 30*time.Minute {
				bPush = true
			} else {
				bPush = false
			}
		}
	} else {
		bPush = false
	}
	return bPush
}

func (fu *FuturesInfoList) PushThreeMinute() map[string]interface{} {
	var (
		returnData map[string]interface{}
	)
	pushFuturesInfoKey := "futuresinfo"
	var err error
	ret := checkPush(pushFuturesInfoKey, fu.Code)
	if ret {
		return returnData
	}
	// 获取之前的结算价
	futuresList := FuturesInfoList{}
	err = models.DB().Table("u_futures_info").
		Where("code = ? ", fu.Code).Where("time < ? ", fu.Time).
		Where("settlement_price > 0 ").
		Order("time desc").First(&futuresList).Error
	if err != nil {
		logging.Info("futuresinfo pushRule err is ", err)
		return returnData
	}
	// 上一个交易日结算价为空 就返回
	if futuresList.SettlementPrice.IsZero() {
		logging.Info("futuresinfo getSettlementPirce err is ", err)
		return returnData
	}
	startTime, _ := time.Parse(util.YMDHM, fu.Time)
	// 获取涨停板 读取不为0的最后一条
	futures := []FuturesInfoList{}
	err = models.DB().Table("u_futures_info").Where("code = ? ", fu.Code).
		Where("time <= ? ", startTime.Format(util.YMDHM)).
		Where("time > ? ", startTime.Add(-time.Minute*5).Format(util.YMDHM)).
		Order("id asc").Limit(5).Find(&futures).Error
	if err != nil {
		logging.Info("futuresinfo pushRule err is ", err)
		return returnData
	}
	if len(futures) < 5 {
		return returnData
	}
	// 推送规则 涨幅为 5%就推送
	rate := fu.Close.Sub(futures[0].Close).Div(futuresList.SettlementPrice).Mul(decimal.NewFromFloat(100))
	if rate.Sub(decimal.NewFromFloat(3.00)).Sign() > 0 {
		data := make(map[string]interface{})
		data["type"] = "futures_info"
		data["push_time"] = time.Now().Format("2006-01-02")
		data["content"] = fmt.Sprintf("%s快速拉升", fu.Name)
		if time.Now().Hour() > 8 && time.Now().Hour() < 9 {
			data["content"] = fmt.Sprintf("昨晚%s上涨快速拉升", fu.Name)
		}
		logging.Info("获取涨幅为%5的推送内容", data)
		_ = SaveEventData(EventName, data["content"].(string), time.Now())
		gredis.HSet(pushFuturesInfoKey, fu.Code, time.Now().Format(util.YMDHMS))
		gredis.Expire(pushFuturesInfoKey, 9*3600)
		return data
	}
	return returnData
}

// 插入事件库
func SaveEventData(eventString, desc string, now time.Time) error {
	return industry.SaveEventData(eventString, desc, "", now, []string{})
}
