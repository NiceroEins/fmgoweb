package spider

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"fmt"
	"github.com/jinzhu/gorm"
	"math"
	"reflect"
	"strings"
	"time"
)

type Forecast struct {
	Code                   string   `json:"code"`
	Name                   *string  `json:"name"`
	Performance            *string  `json:"performance"`
	ExpectedNetProfitUpper *float64 `json:"expected_net_profit_upper,omitempty"`
	ExpectedNetProfitLower *float64 `json:"expected_net_profit_lower,omitempty"`
	PerformanceIncrUpper   *float64 `json:"performance_incr_upper,omitempty"`
	PerformanceIncrLower   *float64 `json:"performance_incr_lower,omitempty"`
	PerformanceReason      *string  `json:"performance_reason"`
	PublishDate            *string  `json:"publish_date"`
	ReportPeriod           string   `json:"report_period"`
}

type Announcement struct {
	Code                             string   `json:"code"`
	Name                             *string  `json:"name"`
	NetProfit                        *float64 `json:"net_profit"`
	NetProfitGrowthRate              *float64 `json:"net_profit_growth_rate"`
	NetProfitComparativeGrowth       *float64 `json:"net_profit_comparative_growth"`
	BusinessRevenue                  *float64 `json:"business_revenue"`
	BusinessRevenueGrowthRate        *float64 `json:"business_revenue_growth_rate"`
	BusinessRevenueComparativeGrowth *float64 `json:"business_revenue_comparative_growth"`
	CashFlowPerShare                 *float64 `json:"cash_flow_per_share"`
	GrossProfitMargin                *float64 `json:"gross_profit_margin"`
	PublishDate                      *string  `json:"publish_date"`
	ReportPeriod                     string   `json:"report_period"`
}

type Disclosure struct {
	Code                string  `json:"code"`
	Name                *string `json:"name"`
	ExpectedPublishDate *string `json:"expected_publish_date"`
	ReportPeriod        string  `json:"report_period"`
}

type IWC struct {
	Code                               string   `json:"code"`
	ReportPeriod                       string   `json:"report_period"`
	NetProfitAfterDed                  *float64 `json:"net_profit_after_ded"`
	NetProfitAfterDedGrowthRate        *float64 `json:"net_profit_after_ded_growth_rate"`
	NetProfitAfterDedComparativeGrowth *float64 `json:"net_profit_after_ded_comparative_growth"` // 扣非净利润环比增长率(%)
	GrossProfitMarginQuarter           *float64 `json:"gross_profit_margin_quarter"`
}

//业绩公告表
type UPA struct {
	Code                               string   `gorm:"index;column:code;type:varchar(10)" json:"code"`                                                               // 证券代码
	Name                               *string  `gorm:"column:name;type:varchar(16)" json:"name"`                                                                     // 股票名称
	NetProfit                          *float64 `gorm:"column:net_profit;type:int(16)" json:"net_profit,omitempty"`                                                   // 净利润（年度）
	NetProfitQuarter                   *float64 `gorm:"column:net_profit_quarter;type:int(16)" json:"net_profit_quarter"`                                             // 净利润（单季度）
	NetProfitGrowthRate                *float64 `gorm:"column:net_profit_growth_rate;type:float" json:"net_profit_growth_rate,omitempty"`                             // 净利润同比增长
	NetProfitGrowthRateQuarter         *float64 `gorm:"column:net_profit_growth_rate_quarter;type:float" json:"net_profit_growth_rate_quarter,omitempty"`             // 净利润同比增长(单季度)
	NetProfitComparativeGrowth         *float64 `gorm:"column:net_profit_comparative_growth;type:float" json:"net_profit_comparative_growth"`                         // 净利润环比增长
	BusinessRevenue                    *float64 `gorm:"column:business_revenue;type:int(16)" json:"business_revenue,omitempty"`                                       // 营业收入（年度）
	BusinessRevenueQuarter             *float64 `gorm:"column:business_revenue_quarter;type:int(16)" json:"business_revenue_quarter"`                                 // 营业收入（单季度）
	BusinessRevenueGrowthRate          *float64 `gorm:"column:business_revenue_growth_rate;type:float" json:"business_revenue_growth_rate,omitempty"`                 // 营业收入同比增长(%)
	BusinessRevenueGrowthRateQuarter   *float64 `gorm:"column:business_revenue_growth_rate_quarter;type:float" json:"business_revenue_growth_rate_quarter,omitempty"` // 营业收入同比增长（单季度）(%)
	BusinessRevenueComparativeGrowth   *float64 `gorm:"column:business_revenue_comparative_growth;type:float" json:"business_revenue_comparative_growth"`             // 营业收入环比增长
	CashFlowPerShare                   *float64 `gorm:"column:cash_flow_per_share;type:float" json:"cash_flow_per_share"`                                             // 每股经营现金流量
	CashFlowPerShareQuarter            *float64 `gorm:"column:cash_flow_per_share_quarter;type:float" json:"cash_flow_per_share_quarter"`                             // 每股经营现金流量（单季度）
	GrossProfitMargin                  *float64 `gorm:"column:gross_profit_margin;type:float" json:"gross_profit_margin"`                                             // 总销售毛利率(%)
	GrossProfitMarginQuarter           *float64 `gorm:"column:gross_profit_margin_quarter;type:float" json:"gross_profit_margin_quarter"`                             // 单季度销售毛利率(%)
	NetProfitAfterDed                  *float64 `gorm:"column:net_profit_after_ded;type:int(16)" json:"net_profit_after_ded,omitempty"`                               // 扣非净利润（年度）
	NetProfitAfterDedQuarter           *float64 `gorm:"column:net_profit_after_ded_quarter;type:int(16)" json:"net_profit_after_ded_quarter"`                         // 扣非净利润（单季度）
	NetProfitAfterDedGrowthRate        *float64 `gorm:"column:net_profit_after_ded_growth_rate;type:float" json:"net_profit_after_ded_growth_rate"`                   // 扣非净利润同比增长率(%)
	NetProfitAfterDedGrowthRateQuarter *float64 `gorm:"column:net_profit_after_ded_growth_rate_quarter;type:float" json:"net_profit_after_ded_growth_rate_quarter"`   // 扣非净利润同比增长率（单季度）(%)
	NetProfitAfterDedComparativeGrowth *float64 `gorm:"column:net_profit_after_ded_comparative_growth;type:float" json:"net_profit_after_ded_comparative_growth"`     // 扣非净利润环比增长率(%)
	ExpectedPublishDate                *string  `gorm:"column:expected_publish_date;type:date" json:"expected_publish_date"`                                          // 预约披露时间(时间戳(秒))
	PublishDate                        *string  `gorm:"column:publish_date;type:date" json:"publish_date"`                                                            // 公告日期
	ReportPeriod                       string   `gorm:"column:report_period;type:date" json:"report_period"`                                                          // 报告期
}

func (upa *UPA) TableName() string {
	return "u_performance_announcement"
}

// 业绩预告表
type UPF struct {
	Code                   string   `gorm:"index;column:code;type:varchar(10)" json:"code"`                                       // 证券代码
	Name                   *string  `gorm:"column:name;type:varchar(16)" json:"name"`                                             // 股票名称
	Performance            *string  `gorm:"column:performance;type:varchar(128)" json:"performance,omitempty"`                    // 业绩变动
	ExpectedNetProfitUpper *float64 `gorm:"column:expected_net_profit_upper;type:decimal(16,2)" json:"expected_net_profit_upper"` // 预计净利润上限
	ExpectedNetProfitLower *float64 `gorm:"column:expected_net_profit_lower;type:decimal(16,2)" json:"expected_net_profit_lower"` // 预计净利润下限
	PerformanceIncrUpper   *float64 `gorm:"column:performance_incr_upper;type:float" json:"performance_incr_upper"`               // 业绩变动幅度上限
	PerformanceIncrLower   *float64 `gorm:"column:performance_incr_lower;type:float" json:"performance_incr_lower"`               // 业绩变动幅度下限
	PerformanceReason      *string  `gorm:"column:performance_reason;type:varchar(512)" json:"performance_reason"`                // 业绩变动原因
	DisclosureTime         *string  `gorm:"column:disclosure_time;type:date" json:"disclosure_time"`                              // 公告日期
	DisclosureTimeHistory  *string  `gorm:"column:disclosure_time_history;type:date" json:"disclosure_time_history"`
	DisclosureChangeAt     *string  `gorm:"column:disclosure_change_at;type:datetime" json:"disclosure_change_at"`
	PublishDate            *string  `gorm:"column:publish_date;type:date" json:"publish_date"`            // 公告日期
	ReportPeriod           string   `gorm:"column:report_period;type:date;not null" json:"report_period"` // 报告期
	SourceType             string   `gorm:"column:source_type;type:varchar(10)" json:"source_type"`       // 报告期
}

func (upf *UPF) TableName() string {
	return "u_performance_forecast"
}

func Forecast2UPF(forecast Forecast) UPF {
	return UPF{
		Code:                   forecast.Code,
		Name:                   forecast.Name,
		Performance:            forecast.Performance,
		ExpectedNetProfitUpper: forecast.ExpectedNetProfitUpper,
		ExpectedNetProfitLower: forecast.ExpectedNetProfitLower,
		PerformanceIncrUpper:   forecast.PerformanceIncrUpper,
		PerformanceIncrLower:   forecast.PerformanceIncrLower,
		PerformanceReason:      forecast.PerformanceReason,
		PublishDate:            forecast.PublishDate,
		ReportPeriod:           forecast.ReportPeriod,
	}
}

func Announcement2UPA(announcement Announcement) *UPA {
	upa := UPA{
		Code:                             announcement.Code,
		Name:                             announcement.Name,
		NetProfit:                        announcement.NetProfit,
		NetProfitGrowthRate:              announcement.NetProfitGrowthRate,
		NetProfitComparativeGrowth:       announcement.NetProfitComparativeGrowth,
		BusinessRevenue:                  announcement.BusinessRevenue,
		BusinessRevenueGrowthRate:        announcement.BusinessRevenueGrowthRate,
		BusinessRevenueComparativeGrowth: announcement.BusinessRevenueComparativeGrowth,
		CashFlowPerShare:                 announcement.CashFlowPerShare,
		GrossProfitMargin:                announcement.GrossProfitMargin,
		PublishDate:                      announcement.PublishDate,
		ReportPeriod:                     announcement.ReportPeriod,
	}
	ret := calcQuarterData(&upa, "NetProfit", "BusinessRevenue", "CashFlowPerShare")
	return calcYearData(ret, "NetProfitQuarter", "NetProfitGrowthRateQuarter", "BusinessRevenueQuarter", "BusinessRevenueGrowthRateQuarter")
}

func Disclosure2UPA(disclosure Disclosure) UPA {
	return UPA{
		Code:                disclosure.Code,
		Name:                disclosure.Name,
		ExpectedPublishDate: disclosure.ExpectedPublishDate,
		ReportPeriod:        disclosure.ReportPeriod,
	}
}

func Disclosure2UPF(disclosure Disclosure) UPF {
	return UPF{
		Code:           disclosure.Code,
		Name:           disclosure.Name,
		DisclosureTime: disclosure.ExpectedPublishDate,
		ReportPeriod:   disclosure.ReportPeriod,
	}
}

func IWC2UPA(iwc IWC) *UPA {
	upa := UPA{
		Code:                               iwc.Code,
		NetProfitAfterDed:                  iwc.NetProfitAfterDed,
		NetProfitAfterDedGrowthRate:        iwc.NetProfitAfterDedGrowthRate,
		NetProfitAfterDedComparativeGrowth: iwc.NetProfitAfterDedComparativeGrowth,
		GrossProfitMarginQuarter:           iwc.GrossProfitMarginQuarter,
		ReportPeriod:                       iwc.ReportPeriod,
	}
	ret := calcQuarterData(&upa, "NetProfitAfterDed")
	return calcYearData(ret, "NetProfitAfterDedQuarter", "NetProfitAfterDedGrowthRateQuarter")
}

func InsertUPF(upf *UPF) (bool, error) {
	db := models.DB()
	var data UPF
	if err := db.Where(&UPF{
		Code:         upf.Code,
		ReportPeriod: upf.ReportPeriod,
	}).First(&data).Error; err != nil {
		err = db.Create(upf).Error
		return false, err
	}
	bPush := false
	if data.DisclosureTime != nil && upf.DisclosureTime != nil && *data.DisclosureTime != "" && string([]rune(*data.DisclosureTime)[:10]) != string([]rune(*upf.DisclosureTime)[:10]) {
		dca := time.Now().Format("2006-01-02 15:04:05")
		dth := *data.DisclosureTime
		upf.DisclosureChangeAt = &dca
		upf.DisclosureTimeHistory = &dth
		bPush = true
	}
	upf.SourceType = "dc"
	err := models.DB().Where(&UPF{
		Code:         upf.Code,
		ReportPeriod: upf.ReportPeriod,
	}).Assign(upf).FirstOrCreate(&UPF{}).Error

	return bPush, err
}

func InsertUPA(upa *UPA) error {
	err := models.DB().Where(&UPA{
		Code:         upa.Code,
		ReportPeriod: upa.ReportPeriod,
	}).Assign(upa).FirstOrCreate(&UPA{}).Error

	return err
}

func calcQuarterData(upa *UPA, fields ...string) *UPA {
	if upa == nil || len(fields) == 0 {
		return upa
	}

	ts := strings.Split(upa.ReportPeriod, " ")
	rp, err := time.ParseInLocation("2006-01-02", ts[0], time.Local)
	if err != nil {
		logging.Error("announcement.calcQuarterData() parse time Errors: ", err.Error())
		return upa
	}
	month := int(rp.Month())
	var upaPrev UPA
	var rpPrev string
	switch month {
	case 3:
	case 6:
		rpPrev = fmt.Sprintf("%d-03-31", rp.Year())
	case 9:
		rpPrev = fmt.Sprintf("%d-06-30", rp.Year())
	case 12:
		rpPrev = fmt.Sprintf("%d-09-30", rp.Year())
	default:
		logging.Error("announcement.calcQuarterData() invalid rp: ", rp)
		return upa
	}
	if len(rpPrev) > 0 {
		if err := models.DB().Model(&UPA{}).Where("code = ?", upa.Code).Where("report_period = ?", rpPrev).First(&upaPrev).Error; err != nil {
			if !gorm.IsRecordNotFoundError(err) {
				logging.Error("announcement.calcQuarterData() query upaPrev Errors: ", err.Error())
			}
			return upa
		}
	}

	for _, field := range fields {
		ue := reflect.ValueOf(upa).Elem().FieldByName(field)
		ueq := reflect.ValueOf(upa).Elem().FieldByName(field + "Quarter")
		if !ue.IsValid() || !ueq.IsValid() {
			// 任何一个参数无效，均跳过
			continue
		}
		if ue.IsNil() || !ueq.IsNil() {
			// 原值为空或目标值不为空，跳过
			continue
		}
		if month == 3 {
			ueq.Set(ue)
			continue
		} else {
			uePrev := reflect.ValueOf(&upaPrev).Elem().FieldByName(field)
			if !uePrev.IsValid() || uePrev.IsNil() {
				continue
			}
			// 相减
			value := ue.Elem().Float() - uePrev.Elem().Float()
			ueq.Set(reflect.ValueOf(&value))
		}
	}

	return upa
}

func calcYearData(upa *UPA, fields ...string) *UPA {
	if upa == nil || len(fields) == 0 || len(fields)%2 != 0 {
		return upa
	}

	ts := strings.Split(upa.ReportPeriod, " ")
	rp, err := time.ParseInLocation("2006-01-02", ts[0], time.Local)
	if err != nil {
		logging.Error("announcement.calcYearData() parse time Errors: ", err.Error())
		return upa
	}

	var upaPrev UPA
	var rpPrev string
	switch rp.Month() {
	case 3, 6, 9, 12:
		rpPrev = fmt.Sprintf("%d-%02d-%02d", rp.Year()-1, rp.Month(), rp.Day())
	default:
		logging.Error("announcement.calcYearData() invalid rp: ", rp)
		return upa
	}

	if len(rpPrev) > 0 {
		if err := models.DB().Model(&UPA{}).Where("code = ?", upa.Code).Where("report_period = ?", rpPrev).First(&upaPrev).Error; err != nil {
			if !gorm.IsRecordNotFoundError(err) {
				logging.Error("announcement.calcYearData() query upaPrev Errors: ", err.Error())
			}
			return upa
		}
	}

	for i := 0; i < len(fields); i += 2 {
		ue := reflect.ValueOf(upa).Elem().FieldByName(fields[i])
		ueq := reflect.ValueOf(upa).Elem().FieldByName(fields[i+1])
		if !ue.IsValid() || !ueq.IsValid() {
			// 任何一个参数无效，均跳过
			return upa
		}
		if ue.IsNil() || !ueq.IsNil() {
			// 原值为空或目标值不为空，跳过
			return upa
		}
		uePrev := reflect.ValueOf(&upaPrev).Elem().FieldByName(fields[i])
		if !uePrev.IsValid() || uePrev.IsNil() {
			return upa
		}

		// 计算与去年同期相比增幅
		var value float64 = 0
		if uePrev.Elem().Float() == value {
			value = ue.Elem().Float()
		} else {
			value = (ue.Elem().Float() - uePrev.Elem().Float()) / math.Abs(uePrev.Elem().Float())
		}
		ueq.Set(reflect.ValueOf(&value))
	}

	return upa
}

// 同花顺 业绩预告补充
func InsertUPFTHS(upf *UPF) (bool, error) {
	db := models.DB()
	var data UPF
	// 如果dc之前有 就不管 没有就补一条。
	err := db.Where(&UPF{
		Code:         upf.Code,
		ReportPeriod: upf.ReportPeriod,
		SourceType:   "dc",
	}).First(&data).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		err := models.DB().Where(&UPF{
			Code:         upf.Code,
			ReportPeriod: upf.ReportPeriod,
			SourceType:   "ths",
		}).Assign(upf).FirstOrCreate(&UPF{}).Error
		return false, err
	}
	if err == nil && *data.Performance == "" {
		err := models.DB().Where(&UPF{
			Code:         upf.Code,
			ReportPeriod: upf.ReportPeriod,
			SourceType:   "dc",
		}).Assign(upf).FirstOrCreate(&UPF{}).Error
		return false, err
	}
	return true, nil
}
