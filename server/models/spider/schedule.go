package spider

import (
	"datacenter/models"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"github.com/jinzhu/gorm"
)

//对应数据库中的一条数据
type USeed struct {
	//ID                 int `gorm:"primary_key" json:"id"`
	//CreatedOn          int `json:"created,omitempty"`
	//ModifiedOn         int `json:"updated"`
	models.Simple
	CatchSite          string
	CatchPath          string
	CatchLink          string
	Star               int
	Cata               string
	Type               string
	CatchInterval      int
	SelectorPageLink   string
	SelectorTitle      string
	SelectorTime       string
	SelectorSourceName string
	SelectorSourceLink string
	SelectorListTime   string
	SelectorListTitle  string
	CatchMode          string
	Status             string
	Keyword            string
	ApiLink            string
}

type ElemStatus struct {
	Name   string `json:"name"`
	Length int    `json:"length"`
	Status string `json:"status"`
}

type LaunchStatus struct {
	Task  []ElemStatus `json:"task"`
	Seed  []ElemStatus `json:"seed"`
	Proxy []ElemStatus `json:"proxy"`
}

type RealTimeInfo struct {
	Day    Day    `json:"day"`
	Hour   Hour   `json:"hour"`
	Second Second `json:"second"`
}

type Day struct {
	Day_product_task_num string `json:"day_product_task_num"`
	Day_send_task_num    string `json:"day_send_task_num"`
	Day_receive_data_num string `json:"day_receive_data_num"`
	Day_store_mysql_num  string `json:"day_store_mysql_num"`
}
type Hour struct {
	Hour_product_task_num string `json:"hour_product_task_num"`
	Hour_send_task_num    string `json:"hour_send_task_num"`
	Hour_receive_data_num string `json:"hour_receive_data_num"`
	Hour_store_mysql_num  string `json:"hour_store_mysql_num"`
}
type Second struct {
	Second_product_task_num string `json:"second_product_task_num"`
	Second_send_task_num    string `json:"second_send_task_num"`
	Second_receive_data_num string `json:"second_receive_data_num"`
	Second_store_mysql_num  string `json:"second_store_mysql_num"`
}

//var PoolSize = 50000
//var SeedMap sync.Map              //concurrent-safe map (seedID, *seed)

//从mysql:U_seed表中获取数据
func FetchUSeedsFromMysql() ([]USeed, error) {
	uSeedList := make([]USeed, 0)
	err := models.DB().Order("id DESC").Where("deleted = ? AND status = ?", 0, "normal").Limit(setting.SpiderSetting.Seed).Find(&uSeedList).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	//fmt.Printf("Fetched %d seeds\n", len(uSeedList))
	return uSeedList, nil
}

func USeed2Seed(uSeed *USeed) *Seed {
	seed := &Seed{
		ID:            uSeed.ID,
		Url:           uSeed.CatchLink,
		Source:        uSeed.CatchLink,
		Mode:          uSeed.CatchMode,
		Selector:      make([]*Selector, 0),
		CatchInterval: uSeed.CatchInterval,
		ApiLink:       uSeed.ApiLink,
	}
	selMap := map[string]string{
		"title":      uSeed.SelectorListTitle,
		"link":       uSeed.SelectorPageLink,
		"title_time": uSeed.SelectorListTime,
	}
	for k, v := range selMap {
		sel := &Selector{
			Name: k,
			Data: v,
		}
		if sel.Available() {
			seed.Selector = append(seed.Selector, sel)
		}
	}
	seed.Data = util.Struct2Map(*uSeed)
	seed.Status = uSeed.Status
	return seed
}

func Content2QASeed(content *NewsContent, id int, mode string) *Seed {
	if content.IndexId == "" {
		return nil
	}
	seed := &Seed{
		ID:   util.Atoi(content.SeedID),
		Url:  content.Link,
		Mode: mode,
		Data: map[string]interface{}{
			"id":          id,
			"question_id": content.IndexId,
		},
		Selector: nil,
	}
	return seed
}

func Content2WBSeed(content *NewsContent, mode string) *Seed {
	seed := &Seed{
		ID:   util.Atoi(content.SeedID),
		Url:  content.Link,
		Mode: mode,
		Data: map[string]interface{}{
			"id":  content.WB.ID,
			"uid": content.WB.UId,
		},
		Selector: nil,
	}
	return seed
}
