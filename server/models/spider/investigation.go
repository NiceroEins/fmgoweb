package spider

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strings"
	"time"
)

type InvestigationIO struct {
	Id                 int     `json:"id"`
	Code               *string `json:"code"`
	Name               *string `json:"name,omitempty"`
	Title              *string `json:"title,omitempty"`
	From               *string `json:"from,omitempty"`
	AliLink            *string `json:"ali_link,omitempty"`
	SourceLink         *string `json:"source_link,omitempty"`
	Content            *string `json:"content,omitempty"`
	PublishTime        *string `json:"publish_time,omitempty"`
	CompNum            *int    `json:"comp_num,omitempty"`
	InstitutionContent *string `json:"institution_content,omitempty"`
	SpiderTime         *string `json:"spider_time,omitempty"`
}

func (nc *NewsContent) InvestigationIO2Seed(mode string, io *InvestigationIO) *Seed {
	return &Seed{
		ID:   util.Atoi(nc.SeedID),
		Url:  nc.Link,
		Mode: mode,
		Data: map[string]interface{}{
			"id":          io.Id,
			"code":        io.Code,
			"title":       io.Title,
			"source_link": io.SourceLink,
		},
		Selector: nil,
	}
}

type InvestigationDTO struct {
	Id                 int                 `json:"id"`
	Code               string              `json:"code"`
	Name               string              `json:"name"`
	Title              string              `json:"title"`
	Keywords           string              `json:"keywords"`
	AliLink            string              `json:"ali_link"`
	Form               string              `json:"form"`
	SourceLink         string              `json:"source_link"`
	Content            string              `json:"content"`
	PublishTime        *time.Time          `json:"publish_time"`
	CompNum            int                 `json:"comp_num"`
	InstitutionContent string              `json:"institution_content,omitempty"`
	IncSum             decimal.NullDecimal `json:"inc_sum"`
	MaxFallback        decimal.NullDecimal `json:"max_fallback"`
	CreatedAt          *time.Time          `json:"created_at"`
	UpdatedAt          *time.Time          `json:"updated_at"`
	SpiderTime         *time.Time          `json:"spider_time"`
}

type NearlyDTO struct {
	Code        string `json:"code"`
	NearlyNum   int    `json:"nearly_num"`   //近30天调研机构总数
	NearlyCount int    `json:"nearly_count"` //近30天调研次数
}

func (dto NearlyDTO) TableName() string {
	return "p_investigation"
}

type InvestigationFilteredDTO struct {
	InvestigationDTO
	Nearly NearlyDTO `json:"Nearly" gorm:"FOREIGNKEY:Code;ASSOCIATION_FOREIGNKEY:Code"`
	Head   int       `json:"head"`
}

type InvestigationFilteredVO struct {
	Id                 int      `json:"id"`
	Code               string   `json:"code"`
	Name               string   `json:"name"`
	Title              string   `json:"title"`
	Keywords           []string `json:"keywords"`
	AliLink            string   `json:"ali_link"`
	Content            string   `json:"content"`
	PublishTime        string   `json:"publish_time"`
	CompNum            int      `json:"comp_num"`
	InstitutionContent string   `json:"institution_content,omitempty"`
	IncSum             string   `json:"inc_sum"`
	MaxFallback        string   `json:"max_fallback"`
	NearlyNum          int      `json:"nearly_num"`
	NearlyCount        int      `json:"nearly_count"`
	Head               int      `json:"head"`
}

type InvestigationFilteredQO struct {
	Code      string `json:"code" form:"code"`
	Keyword   string `json:"keyword" form:"keyword"`
	StartTime string `json:"start_time" form:"start_time"`
	EndTime   string `json:"end_time" form:"end_time"`
	PageSize  int    `json:"page_size" form:"page_size" binding:"gt=0"`
	PageNum   int    `json:"page_num" form:"page_num" binding:"gt=0"`
	SortKey   int    `json:"sort_key" form:"sort_key"`
	Direction bool   `json:"direction" form:"direction"`
}

func (dto InvestigationFilteredDTO) DTO2VO() crud_service.VO {
	vo := InvestigationFilteredVO{
		Id:   dto.Id,
		Code: dto.InvestigationDTO.Code,
		Name: dto.Name,
		//Content:     dto.Content,
		CompNum:            dto.CompNum,
		InstitutionContent: dto.InstitutionContent,
		PublishTime:        util.TimePtr2String(dto.PublishTime, util.YMDHM),
		Title:              dto.Title,
		NearlyNum:          dto.Nearly.NearlyNum,
		NearlyCount:        dto.Nearly.NearlyCount,
		AliLink:            dto.AliLink,
		IncSum:             util.NullDecimal2String(dto.IncSum),
		MaxFallback:        util.NullDecimal2String(dto.MaxFallback),
		Head:               dto.Head,
	}
	vo.Keywords = []string{}
	_ = json.Unmarshal([]byte(dto.Keywords), &vo.Keywords)
	return &vo
}

func GetInvestigationFilteredList(qo *InvestigationFilteredQO) ([]crud_service.DTO, int, error) {
	var ctx = "<span style=\"color:%s;font-weight:bold\">"
	var edx = "</span>"
	dtos := make([]InvestigationFilteredDTO, 0)
	res := make([]crud_service.DTO, 0)
	//cnt := 0
	sortArray := []string{
		"",
		"NearlyCount",
		"NearlyNum",
		"IncSum",
		"MaxFallback",
	}
	dbs := models.DB().Table("p_investigation").
		Select("p_investigation.*,u_stock_industry.head").
		Joins("LEFT JOIN u_stock_industry on u_stock_industry.code=p_investigation.code").
		Preload("Nearly", func(db *gorm.DB) *gorm.DB {
			return models.DB().
				Select("code,count(*) nearly_count,sum(comp_num) nearly_num").
				Where("publish_time<=?", time.Now()).
				Where("publish_time>=?", time.Now().AddDate(0, 0, -30)).
				Group("code")
		})
	if qo.StartTime != "" {
		dbs = dbs.Where("p_investigation.publish_time>=?", qo.StartTime)
	}
	if qo.EndTime != "" {
		endTime, _ := time.ParseInLocation(util.YMD, qo.EndTime, time.Local)
		qo.EndTime = endTime.AddDate(0, 0, 1).Format(util.YMD)
		dbs = dbs.Where("p_investigation.publish_time<?", qo.EndTime)
	}
	if qo.Keyword != "" {
		dbs = dbs.Where("title like ?", "%"+qo.Keyword+"%")
	}
	if qo.Code != "" {
		dbs = dbs.Where("p_investigation.code like ? OR p_investigation.name like ?", "%"+qo.Code+"%", "%"+qo.Code+"%")
	}
	//dbs.Count(&cnt)
	//dbs = dbs.Limit(qo.PageSize).Offset((qo.PageNum - 1) * qo.PageSize)
	err := dbs.Where("name not like '%ST%'").Order("publish_time DESC").Find(&dtos).Error
	if err != nil {
		return nil, 0, err
	}
	if qo.SortKey > 0 && qo.SortKey < len(sortArray) {
		field := "Nearly"
		if qo.SortKey > 2 {
			field = "InvestigationDTO"
		}
		sort.Slice(dtos, func(i, j int) bool {
			if util.Compare(reflect.ValueOf(dtos[i]).FieldByName(field).FieldByName(sortArray[qo.SortKey]), reflect.ValueOf(dtos[j]).FieldByName(field).FieldByName(sortArray[qo.SortKey])) {
				return qo.Direction
			} else {
				if !util.Compare(reflect.ValueOf(dtos[j]).FieldByName(field).FieldByName(sortArray[qo.SortKey]), reflect.ValueOf(dtos[i]).FieldByName(field).FieldByName(sortArray[qo.SortKey])) {
					return dtos[i].PublishTime.After(*dtos[j].PublishTime)
				} else {
					return !qo.Direction
				}
			}
			//return qo.Direction == (util.Compare(reflect.ValueOf(dtos[i]).FieldByName(field).FieldByName(sortArray[qo.SortKey]), reflect.ValueOf(dtos[j]).FieldByName(field).FieldByName(sortArray[qo.SortKey])))
		})
	}
	for _, dto := range dtos[util.Min((qo.PageNum-1)*qo.PageSize, len(dtos)):util.Min(qo.PageSize*qo.PageNum, len(dtos))] {
		if qo.Keyword != "" {
			dto.Title = strings.ReplaceAll(dto.Title, qo.Keyword, ctx+qo.Keyword+edx)
		}
		res = append(res, dto)
	}
	return res, len(dtos), nil
}

type InvestigationSearchDTO struct {
	InvestigationDTO
	ContentList []string `json:"content_list"`
}

type InvestigationSearchVO struct {
	Id                 int      `json:"id"`
	Code               string   `json:"code"`
	Name               string   `json:"name"`
	Title              string   `json:"title"`
	Content            string   `json:"content"`
	ContentList        []string `json:"content_list"`
	PublishTime        string   `json:"publish_time"`
	CompNum            int      `json:"comp_num"`
	InstitutionContent string   `json:"institution_content,omitempty"`
	AliLink            string   `json:"ali_link"`
}

func (dto InvestigationSearchDTO) DTO2VO() crud_service.VO {
	return &InvestigationSearchVO{
		Id:                 dto.Id,
		Code:               dto.Code,
		Name:               dto.Name,
		Title:              dto.Title,
		Content:            dto.Content,
		ContentList:        dto.ContentList,
		PublishTime:        util.TimePtr2String(dto.PublishTime, util.YMDHM),
		CompNum:            dto.CompNum,
		InstitutionContent: dto.InstitutionContent,
		AliLink:            dto.AliLink,
	}
}

type InvestigationSearchQO struct {
	Search    string `json:"search" form:"search"`
	StartTime string `json:"start_time" form:"start_time"`
	EndTime   string `json:"end_time" form:"end_time"`
	PageSize  int    `json:"page_size" form:"page_size" binding:"gt=0"`
	PageNum   int    `json:"page_num" form:"page_num" binding:"gt=0"`
}

func GetInvestigationSearchList(qo *InvestigationSearchQO) ([]crud_service.DTO, int, error) {
	dtos := make([]InvestigationSearchDTO, 0)
	res := make([]crud_service.DTO, 0)
	cnt := 0
	dbs := models.DB().Table("p_investigation")
	if qo.StartTime != "" {
		dbs = dbs.Where("p_investigation.publish_time>=?", qo.StartTime)
	}
	if qo.EndTime != "" {
		endTime, _ := time.ParseInLocation(util.YMD, qo.EndTime, time.Local)
		qo.EndTime = endTime.AddDate(0, 0, 1).Format(util.YMD)
		dbs = dbs.Where("p_investigation.publish_time<?", qo.EndTime)
	}
	if qo.Search != "" {
		dbs = dbs.Where("MATCH(content) AGAINST (+? IN BOOLEAN MODE)", qo.Search)
	} else {
		dbs = dbs.Order("publish_time desc")
	}
	dbs.Count(&cnt)
	dbs = dbs.Limit(qo.PageSize).Offset((qo.PageNum - 1) * qo.PageSize)
	err := dbs.Find(&dtos).Error
	if err != nil {
		logging.Error("GetInvestigationSearchList() err:" + err.Error())
		return nil, 0, err
	}
	for _, dto := range dtos {
		if qo.Search != "" {
			dto.ContentList = util.GetStringBetweenKeyword(dto.Content, qo.Search, 2, 100)
		} else {
			dto.ContentList = []string{}
		}
		res = append(res, dto)
	}
	return res, cnt, nil
}
