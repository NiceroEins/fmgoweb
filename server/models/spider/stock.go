package spider

import (
	"datacenter/models/stock"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"encoding/json"
	"github.com/shopspring/decimal"
	"strconv"
	"time"
)

type PopularStock struct {
	Code       string `json:"code"`
	Name       string `json:"name"`
	Rank       int    `json:"rank"`
	RankChange int    `json:"rank_change"`
}

func UpdatePopular(ps *PopularStock) bool {
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	content, _ := json.Marshal(ps)
	e1 := conn.HSet("stock:popular", ps.Code, string(content))
	np, _ := conn.HGetFloat64("stock:tick", ps.Code)
	if ps.Rank <= 20 || ps.RankChange >= 30 {
		logging.Info("stock.UpdatePopular() err:", "满足条件更新人气股，股票代码："+ps.Code + ",rank为"+
			strconv.Itoa(ps.Rank)+",rank_change为"+strconv.Itoa(ps.RankChange))
		tm := time.Now().Add(1830 * time.Second)
		_ = stock.CreateOrUpdateStockPool(&stock.StockPoolTypeIO{
			Code:          ps.Code,
			Name:          "人气股",
			TransferPrice: decimal.NewFromFloat(np),
			TimeoutAt:     &tm,
		})
	}
	return e1 == nil
}
