package spider

import (
	//msg "datacenter/middleware/message"
	"datacenter/models"
	"datacenter/pkg/e"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"fmt"
	"github.com/jinzhu/gorm"
	"strconv"
	"time"
)

type UIopEquity struct {
	ID                     int       `gorm:"primary_key;column:id;type:int(10) unsigned;not null" json:"id"`
	Company                string    `gorm:"column:company;type:varchar(255)" json:"company"`                                   // 公司名称
	DeclareTime            string    `gorm:"column:declare_time;type:date" json:"declare_time"`                                 // 上会日期
	EquityLink             string    `gorm:"column:equity_link;type:varchar(255)" json:"equity_link"`                           // 股权关系链接
	QuantityToBeIssued     string    `gorm:"column:quantity_to_be_issued" json:"quantity_to_be_issued"`                         // 拟发行数量
	ProposedListingAddress string    `gorm:"column:proposed_listing_address;type:varchar(255)" json:"proposed_listing_address"` // 拟上市地址
	CatchTime              string    `gorm:"column:catch_time;type:datetime" json:"catch_time"`                                 // 抓取时间
	SpiderTime             string    `gorm:"column:spider_time;type:datetime" json:"spider_time"`                               // 爬取时间
	Type                   string    `gorm:"column:type;" json:"type"`                                                          // 爬取时间
	CreatedAt              time.Time `gorm:"column:created_at;type:datetime" json:"created_at,omitempty"`                       // 创建时间
	UpdatedAt              time.Time `gorm:"column:updated_at;type:datetime" json:"updated_at,omitempty"`                       // 更新时间
	Cid                    int       `gorm:"primary_key;column:cid;type:int(10) unsigned;not null" json:"cid"`
	//EquityRelationship map[string]string `json:"-"` //(总:页面使用)股权关系(推送,股权关系链接)只要name_type=6的公司名称

	//SecondEquityRelationship map[string]string //二级(参股公司)，key 公司名称,value
	//ThirdEquityRelationship  map[string]string //三级(间接参股公司)
	EquityRelationshipArray2 []EquityRelationshipArrayStruct `json:"equity_relationship_array2"` //二级,直接参股公司
	EquityRelationshipArray3 []EquityRelationshipArrayStruct `json:"equity_relationship_array3"` //三级,间接参股公司
}

//id,parent_id,name,link,name_type
type EquityRelationshipArrayStruct struct {
	Id         int    `json:"-"`
	ParentId   int    `json:"-"`
	Name       string `json:"name"`
	Link       string `json:"link"`
	NameType   int    `json:"-"`
	ParentLink string `json:"parent_link"`
}

//二级 数据
type EquityRelationship struct {
	Id                      int                  `json:"id"`
	ParentId                int                  `json:"parent_id"`
	Company                 string               `json:"company"` //子公司名
	Link                    string               `json:"link"`    //图片链接
	ClildEquityRelationship []EquityRelationship // 三级 数据
}
type Data struct {
	Id       int    `json:"id"`
	ParentId int    `json:"parent_id"`
	Name     string `json:"name"`
	Link     string `json:"link"`
	NameType int    `json:"name_type"`
}

type UIopEquityRelationship struct {
	ID          int       `gorm:"primary_key;column:id;type:int(10) unsigned;not null" json:"-"`
	IopEquityID int       `gorm:"column:iop_equity_id;type:int(11)" json:"iop_equity_id"`  // 主键id(u_iop_equity的主键，3 级不存)
	Name        string    `gorm:"column:name;type:varchar(255)" json:"name"`               // 名称
	Link        string    `gorm:"column:link;type:varchar(255)" json:"link"`               // 链接
	StockRatio  float64   `gorm:"column:stock_ratio;type:varchar(255)" json:"stock_ratio"` // 股票占比
	Money       float64   `gorm:"column:money;type:varchar(255)" json:"money"`             // 金额(元)
	NameType    int       `gorm:"column:name_type;type:varchar(255)" json:"name_type"`     //  0 不是上市公司,1 上市公司,2股东名字,3港股
	ParentID    int       `gorm:"column:parent_id;type:int(11)" json:"parent_id"`
	ParentCid   int       `json:"parent_cid"`                         // 父级id(内关联)
	Cid         int       `gorm:"column:cid;type:int(11)" json:"cid"` // 公司在天眼查里面的id
	Level       int       `gorm:"column:level;type:int(11)" json:"level"` // 公司在天眼查里面的id
	CreatedAt   time.Time `gorm:"column:created_at;type:datetime" json:"created_at"`
	UpdatedAt   time.Time `gorm:"column:updated_at;type:datetime" json:"updated_at"`
}

type IpoWsPushMessage struct {
	Company            string   `json:"company"`               //公司
	DeclareTime        string   `json:"declare_time"`          //上市时间
	QuantityToBeIssued *float64 `json:"quantity_to_be_issued"` //拟发行数量

	SecondCompany    string   `json:"second_company"` //参股公司
	SecondStockRatio *float64 `json:"stock_ratio"`    //参股比例

	ThirdCompany    string   `json:"third_company"` //间接参股公司
	ThirdStockRatio *float64 `json:"third_company"` //间接参股比例
}

type IpoWsPushMessageJson struct {
	Type                   string `json:"type"` //类型 固定 ipo
	Recommender            string `json:"recommender"`
	Company                string `json:"company"`                  //公司
	DeclareTime            string `json:"declare_time"`             //上市时间
	QuantityToBeIssued     string `json:"quantity_to_be_issued"`    //拟发行数量
	ProposedListingAddress string `json:"proposed_listing_address"` //拟上市地址

	SecondCompany []IpoWsPushData `json:"second_company"` //参股公司
	//SecondStockRatio   float64 `json:"stock_ratio"`    //参股比例

	ThirdCompany []IpoWsPushData `json:"third_company"` //间接参股公司
	//ThirdStockRatio float64 `json:"third_company"` //间接参股比例
}
type IpoWsPushData struct {
	Id         int     `json:"-"`
	Name       string  `json:"name"`
	StockRatio float64 `json:"stock_ratio,omitempty"`
}

func DuplicationInsertUIpoEquity(value *NewsContent) (int, string, bool, error) {
	var (
		i_e_insert UIopEquity
		count      int
	)

	if err := models.DB().Table("u_iop_equity").Where("company = ?", value.IpoDcList.Company).Count(&count).Error; err != nil {
		logging.Error(fmt.Sprintf("iop_equity.go DuplicationInsertUIpoEquity(),err : %s,company = %v", err.Error(), value.IpoDcList.Company))
		return 0, "", false, err
	}
	if count > 0 {
		logging.Debug(fmt.Sprintf("iop_equity.go DuplicationInsertUIpoEquity(),err : 公司名重复"))
		return 0, "", false, fmt.Errorf("公司名重复")
	}
	i_e_insert.Company = value.IpoDcList.Company
	i_e_insert.DeclareTime = value.IpoDcList.DeclareTime

	//quantityToBeIssued := util.Atof(value.IpoDcList.QuantityToBeIssued)
	i_e_insert.QuantityToBeIssued = value.IpoDcList.QuantityToBeIssued

	i_e_insert.ProposedListingAddress = value.IpoDcList.ProposedListingAddress
	i_e_insert.CatchTime = value.CatchTime
	i_e_insert.SpiderTime = value.SpiderTime
	if err := models.DB().Table("u_iop_equity").Omit("").Create(&i_e_insert).Error; err != nil {
		logging.Error(fmt.Sprintf("iop_equity.go DuplicationInsertUIpoEquity(),err : %s", err.Error()))
		return 0, "", false, err
	}
	//redis 设置字符串计数 key 东财表主见id
	err := gredis.Set("ipo:"+strconv.Itoa(i_e_insert.ID), 0, 60*60*24)
	if err != nil {
		logging.Error(fmt.Sprintf("ipo设置东财计数器失败,id = ", i_e_insert.ID))
	}
	return i_e_insert.ID, i_e_insert.Company, true, nil
}

func UpdateUIpoEquity(value *NewsContent) (bool, error) {
	if util.IsEmpty(value.IpoTycList.Cid) && util.IsEmpty(value.IpoTycList.EquityLink) {
		logging.Error(fmt.Sprintf("iop_equity.go UpdateUIpoEquity(),err : 爬虫没有获取到该公司信息,value = %v", value))
		return false, fmt.Errorf("爬虫没有获取到该公司信息")
	}
	//更新东财表,cid,equity_link
	if err := models.DB().Table("u_iop_equity").Where("id = ?", value.IpoTycList.Id).Updates(map[string]interface{}{
		"cid":         value.IpoTycList.Cid,
		"equity_link": value.IpoTycList.EquityLink,
	}).Error; err != nil {
		logging.Error(fmt.Sprintf("iop_equity.go UpdateUIpoEquity(),err : %s", err.Error()))
		return false, err
	}
	return true, nil
}

func DuplicationInsertUIopEquityRelationship(value *NewsContent) (int, bool, error) {
	var (
		u_i_e           UIopEquity
		u_i_e_r         UIopEquityRelationship
		u_i_e_r_insert  UIopEquityRelationship
		u_i_e_r_insert2 UIopEquityRelationship
	)
	//入库判重(cid,父级cid)
	if err := models.DB().Table("u_iop_equity_relationship").Select("id").Where("cid = ?", value.IpoTycDetail.Cid).Where("parent_cid = ?", value.IpoTycDetail.ParentCid).Find(&u_i_e_r).Error; err != nil && err != gorm.ErrRecordNotFound {
		logging.Error(fmt.Sprintf("iop_equity.go DuplicationInsertUIopEquityRelationship(),err : %s,cid=%v,parent_cid=%v", err.Error(), value.IpoTycDetail.Cid, value.IpoTycDetail.ParentCid))
		return 0, false, fmt.Errorf("iop_equity.go DuplicationInsertUIopEquityRelationship(),err : 入库判重失败")
	}
	if u_i_e_r.ID > 0 {
		logging.Error(fmt.Sprintf("iop_equity.go DuplicationInsertUIopEquityRelationship(),err : u_iop_equity_relationship表中有重复,u_i_e_r.ID = %d", u_i_e_r.ID))
		return 0, false, fmt.Errorf(e.GetMsg(e.ERROR_IPO_ERR_MSG))
	}
	//通过父级cid查询到父级的主键id
	models.DB().Table("u_iop_equity_relationship").Select("id").Where("cid = ?", value.IpoTycDetail.ParentCid).Find(&u_i_e_r_insert2)

	//查询东财的父级主键id，不是二级 直接存数据库，不入队列
	models.DB().Table("u_iop_equity").Select("id").Where("cid = ?", value.IpoTycDetail.ParentCid).Find(&u_i_e)

	//插入数据库
	u_i_e_r_insert.IopEquityID = u_i_e.ID //主库主键id
	u_i_e_r_insert.Name = value.IpoTycDetail.Name
	u_i_e_r_insert.Money = value.IpoTycDetail.Money
	u_i_e_r_insert.NameType = value.IpoTycDetail.NameType
	u_i_e_r_insert.Cid = value.IpoTycDetail.Cid
	u_i_e_r_insert.Link = value.IpoTycDetail.Link
	u_i_e_r_insert.ParentCid = value.IpoTycDetail.ParentCid
	u_i_e_r_insert.StockRatio = value.IpoTycDetail.StockRatio
	u_i_e_r_insert.ParentID = u_i_e_r_insert2.ID

	if err := models.DB().Table("u_iop_equity_relationship").Create(&u_i_e_r_insert).Error; err != nil {
		logging.Error(fmt.Sprintf("iop_equity.go DuplicationInsertUIopEquityRelationship(),err : %s", err.Error()))
		return 0, false, fmt.Errorf("u_iop_equity_relationship表插入数据失败,u_i_e_r_insert = %v", u_i_e_r_insert)
	}
	return u_i_e.ID, true, nil
}

func CalcIpoWsPushMsg(ipoWsMsg *IpoWsPushMessageJson, id int) error {
	logging.Info("计算ws 推送数据")
	var (
		u_i_e        UIopEquity
		u_i_e_second []IpoWsPushData
		u_i_e_third  []IpoWsPushData
		u_i_e_r_ids  []UIopEquityRelationship
	)
	//获取一级
	err := models.DB().Table("u_iop_equity").Select("company,declare_time,quantity_to_be_issued,proposed_listing_address").Where("id = ?", id).Find(&u_i_e).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Error("CalcIpoWsPushMsg(),获取一级公司名等信息失败,err : ", err.Error())
		return err
	}
	ipoWsMsg.Type = "ipo"
	ipoWsMsg.Recommender = "系统推荐"
	if !util.IsEmpty(u_i_e.Company) {
		ipoWsMsg.Company = u_i_e.Company
	}
	if !util.IsEmpty(u_i_e.DeclareTime) {
		ipoWsMsg.DeclareTime = u_i_e.DeclareTime
	}
	if !util.IsEmpty(u_i_e.QuantityToBeIssued) {
		ipoWsMsg.QuantityToBeIssued = fmt.Sprintf("%s", u_i_e.QuantityToBeIssued)
	}
	if !util.IsEmpty(u_i_e.ProposedListingAddress) {
		ipoWsMsg.ProposedListingAddress = u_i_e.ProposedListingAddress
	}

	//获取二级
	err = models.DB().Table("u_iop_equity_relationship").Select("id,name,stock_ratio").Where("iop_equity_id = ?", id).Where("name_type = ?", "6").Find(&u_i_e_second).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Error("CalcIpoWsPushMsg(),获取二级公司名等信息失败,err : ", err.Error())
		return err
	}
	if len(u_i_e_second) > 0 {
		ipoWsMsg.SecondCompany = u_i_e_second
	}

	//所有二级id的集合,其中包含，上市的，未上市的
	err = models.DB().Table("u_iop_equity_relationship").Select("id").Where("iop_equity_id = ?", id).Find(&u_i_e_r_ids).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Error("CalcIpoWsPushMsg(),获取所有二级公司id失败,err : ", err.Error())
		return err
	}
	ids := []int{}
	for _, v := range u_i_e_r_ids {
		ids = append(ids, v.ID)
	}

	//获取三级
	err = models.DB().Table("u_iop_equity_relationship").Select("name").Where("parent_id in (?)", ids).Where("name_type = ?", "6").Find(&u_i_e_third).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Error("CalcIpoWsPushMsg(),获取二级公司名等信息失败,err : ", err.Error())
		return err
	}

	if len(u_i_e_third) > 0 {
		ipoWsMsg.ThirdCompany = u_i_e_third
	}

	return nil
}

func Content2IopEquitySeed(seedId, id int, mode, company string) *Seed {
	seed := &Seed{
		ID:     seedId,
		TaskID: util.GenerateRandomString(32) + "-" + strconv.Itoa(seedId),
		Url:    "",
		Mode:   mode,
		Data: map[string]interface{}{
			"id":      id,
			"company": company,
		},
		Selector: nil,
	}
	return seed
}

func Content2IopEquityTycSeed(seedId string, id int, cid string, mode string) *Seed {
	seedID, _ := strconv.Atoi(seedId)
	cidInt, _ := strconv.Atoi(cid)
	seed := &Seed{
		ID:     seedID,
		TaskID: util.GenerateRandomString(32) + "-" + seedId,
		Url:    "",
		Mode:   mode,
		Data: map[string]interface{}{
			"cid": cidInt,
		},
		Selector: nil,
	}
	return seed
}

func Content2IopEquityTycSeed2(seedId string, cid int, mode string) *Seed {
	seedID, _ := strconv.Atoi(seedId)
	seed := &Seed{
		ID:     seedID,
		TaskID: util.GenerateRandomString(32) + "-" + seedId,
		Url:    "",
		Mode:   mode,
		Data: map[string]interface{}{
			"cid": cid,
		},
		Selector: nil,
	}
	return seed
}

func Content2WeChatDetail(link, mode string) *Seed {
	seed := &Seed{
		ID:     1,
		TaskID: util.GenerateRandomString(32) + "-1",
		Url:    "",
		Mode:   mode,
		Data: map[string]interface{}{
			"link": link,
		},
		Selector: nil,
	}
	return seed
}
