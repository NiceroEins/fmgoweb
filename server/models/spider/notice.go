package spider

import (
	"datacenter/models"
	"time"
)

type CninfoNoticList struct {
	Code        string    `json:"code"`
	Name        string    `json:"name"`
	Type        string    `json:"type"`
	Title       string    `json:"title"`
	PublishTime time.Time `json:"publish_time,omitempty"`
	SourceLink  string    `json:"source_link,omitempty"`
}

func (CninfoNoticList) TableName() string {
	return "u_cninfo_notice"
}

func (cnl *CninfoNoticList) Save() error {
	db := models.DB()
	err := db.Where(&CninfoNoticList{
		Code:  cnl.Code,
		SourceLink: cnl.SourceLink,
		Type:  cnl.Type,
	}).Assign(cnl).FirstOrCreate(&CninfoNoticList{}).Error
	return err
}
