package spider

import (
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"time"
	//"os"
)

type Task struct {
	TaskID string  `json:"taskID"`
	Seed   []*Seed `json:"seed"`
	Proxy  *Proxy  `json:"proxy"`
}

func (t *Task) GenerateSeedID(id int) string {
	return fmt.Sprintf("%s-%d", t.TaskID, id)
}

func (t *Task) AppendSeed(seed *Seed) *Task {
	if seed.TaskID == "" {
		seed.TaskID = t.GenerateSeedID(seed.ID)
	}
	for i := 0; i < len(seed.Selector); i++ {
		seed.Selector[i].ID = seed.GenerateSelectorID()
	}
	t.Seed = append(t.Seed, seed)

	return t
}

func (t *Task) AppendSeeds(seeds []*Seed) *Task {
	for _, seed := range seeds {
		t.AppendSeed(seed)
	}
	return t
}

func (t *Task) AppendProxy(proxy *Proxy) *Task {
	t.Proxy = proxy
	return t
}

func (t *Task) SetLog() *Task {
	for _, v := range t.Seed {
		ip := "127.0.0.1"
		if t.Proxy != nil && t.Proxy.Ip == "" {
			ip = t.Proxy.Ip
		}
		if err := gredis.HSet("TaskMap", v.TaskID, ip); err != nil {
			logging.Error("task.go SetLog() Errors:", err.Error())
		}

		// store
		data, _ := json.Marshal(v)
		ts := time.Now().Format("2006010215")
		bExist := gredis.Exists("seed:list:" + ts)
		_ = gredis.HSet("seed:list:"+ts, v.TaskID, string(data))
		if !bExist {
			// 3小时过期
			gredis.Expire("seed:list:"+ts, 3600*3)
		}

		if v.Retries > 3 {
			_ = gredis.HSet("seed:retries", v.TaskID, v.Retries)
		}
	}
	return t
}

func (t *Task) Len() int {
	return len(t.Seed)
}

type Seed struct {
	ID            int                    `json:"id" validate:"gt=0"`
	TaskID        string                 `json:"ID" validate:"omitempty"`
	Url           string                 `json:"url" validate:"url"`
	Source        string                 `json:"source" validate:"omitempty"`
	Mode          string                 `json:"mode" validate:"gt=0"`
	Status        string                 `json:"-" validate:"omitempty"`
	Data          map[string]interface{} `json:"data,omitempty" validate:"omitempty"`
	Selector      []*Selector            `json:"selector,omitempty" validate:"omitempty"`
	Retries       int                    `json:"retries,omitempty" validate:"omitempty"`
	CatchInterval int                    `json:"catch_interval"`
	ApiLink       string                 `json:"api_link,omitempty" validate:"omitempty"`
}

func (s *Seed) GenerateSelectorID() string {
	return fmt.Sprintf("%s-%s", s.TaskID, util.GenerateRandomString(8))
}

func (s *Seed) AppendSelector(sel *Selector) *Seed {
	if sel.ID == "" {
		sel.ID = s.GenerateSelectorID()
	}
	s.Selector = append(s.Selector, sel)
	return s
}

func (s *Seed) AppendSelectors(sels []*Selector) *Seed {
	for _, sel := range sels {
		s.AppendSelector(sel)
	}
	return s
}

func (s *Seed) Available() bool {
	hasTitle := false
	for _, v := range s.Selector {
		if v.Name == "title" {
			hasTitle = true
		}
	}
	// status
	if s.Status == "banned" {
		return false
	}

	switch s.Mode {
	case "normal", "strong":
		return s.Url != "" && len(s.Selector) > 0 && hasTitle
	case "":
		return false
	default:
		return true
	}
}

func (s *Seed) Filter() *Seed {
	switch s.ID {
	case 322:
		s.Mode = "qa"
	case 323:
		s.Mode = "qa"
	case 3322:
		//s.Mode = "q"
		// skip this case
		s.Url = ""
	case 3323:
		// special
		s.Mode = "qa"
	}
	return s
}

type Selector struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Data string `json:"data"`
}

func (sel *Selector) Available() bool {
	return sel.Data != "" && sel.Data != "false" && sel.Name != ""
}
