package spider

import (
	"datacenter/models"
	"strings"
	"sync"
)

//var DefaultNewsKeyword []string
//var DefaultQAKeyword []string
//var DefaultImportantKeyword []string

// string, []string
var NewsKeywords sync.Map
var QAKeywords sync.Map
var StarKeywords sync.Map

type Keyword struct {
	models.Simple

	Name     string `json:"name"`
	Category string `json:"category"`
	Star     int    `json:"star"`
	Color    string `json:"color"`
	Usage    string `json:"usage"`
	Case     string `json:"case"`
	Creator  int64  `json:"creator"`
	Status   string `json:"status"`
}

func (k *Keyword) TableName() string {
	return "u_keyword"
}

func Init() {
	var company []string
	models.DB().Table("u_shares_info").Where("end_date IN (?, ?)", "None", "").Pluck("name", &company)
	StarKeywords.Store("company", company)

	RefreshKeyword("")
}

func RefreshKeyword(uid string) {
	//news, _ := fetchKeyword("news", uid, -1)
	//qa, _ := fetchKeyword("qa", uid, -1)
	var uids []string
	if uid == "" {
		//for _, v := range news {
		//	uids[v.Creator] = fmt.Sprintf("%d", v.Creator)
		//}
		//for _, v := range qa {
		//	uids[v.Creator] = fmt.Sprintf("%d", v.Creator)
		models.DB().Table("b_user").Where("deleted_at IS NULL").Where("status = ?", "normal").Pluck("id", &uids)
	} else {
		//uids[int64(util.Atoi(uid))] = uid
		uids = append(uids, uid)
	}

	cn, _ := fetchKeyword("news", "", 4)
	StarKeywords.Store("news", ConvertKeyword(cn))
	cq, _ := fetchKeyword("qa", "", 4)
	StarKeywords.Store("qa", ConvertKeyword(cq))
	ca, _ := fetchKeyword("news", "", 4)
	StarKeywords.Store("nlp", ConvertKeyword(ca))
	for _, v := range uids {
		nk, _ := fetchKeyword("news", v, -1)
		nq, _ := fetchKeyword("qa", v, -1)
		nk = append(nk, cn...)
		nq = append(nq, cq...)
		NewsKeywords.Store(v, ConvertKeyword(nk))
		QAKeywords.Store(v, ConvertKeyword(nq))
	}
}

// keywordType: news/qa/q
func fetchKeyword(keywordType, uid string, star int) ([]Keyword, error) {
	var keyword []Keyword
	dbs := models.DB().Model(&Keyword{}).Where("`status` = ?", "normal").Where("deleted_at IS NULL")
	if uid != "" {
		dbs = dbs.Where("creator = ?", uid)
	}
	dbs = dbs.Where("star >= ?", star)
	switch keywordType {
	case "qa", "q":
		dbs = dbs.Where("`case` LIKE '%interaction%'")
	case "news":
		dbs = dbs.Where("`case` LIKE '%normal%'")
	}
	err := dbs.Find(&keyword).Error
	return keyword, err
}

func FetchKeyword(keywordType string, star int) []string {
	keyword, _ := fetchKeyword(keywordType, "", star)
	return ConvertKeyword(keyword)
}

func Fetch(keywordType string) []string {
	//keyword, err := fetchKeyword(keywordType, uid, star)
	//if err != nil {
	//	return []string{}
	//}
	//return ConvertKeyword(keyword)
	keyword, ok := StarKeywords.Load(keywordType)
	if !ok {
		return []string{}
	}
	return keyword.([]string)
}

func ConvertKeyword(keyword []Keyword) []string {
	var ret []string
	for _, v := range keyword {
		ret = append(ret, v.Name)
	}
	return ret
}

func Contains(src string, keywords []string) bool {
	if src == "" {
		return false
	}
	for _, v := range keywords {
		if strings.Index(src, v) != -1 {
			return true
		}
	}
	return false
}

func GetKeyword(keywordType, uid string) []string {
	switch keywordType {
	case "qa", "q":
		v, ok := QAKeywords.Load(uid)
		if !ok {
			return Fetch("qa")
		}
		ret := append(v.([]string), Fetch("qa")...)
		return ret
	case "news":
		v, ok := NewsKeywords.Load(uid)
		if !ok {
			return Fetch("qa")
		}
		ret := append(v.([]string), Fetch("news")...)
		return ret
	default:
		return []string{}
	}
}

func BDKeyword() []string {
	var keyword []Keyword
	dbs := models.DB().Model(&Keyword{}).Where("`status` = ?", "normal")
	dbs = dbs.Where("star >= ?", 4).Where("`case` LIKE '%search%'")
	dbs.Find(&keyword)

	return ConvertKeyword(keyword)
}

func NLPKeyword() []string {
	var ret []string
	nlp, ok := StarKeywords.Load("nlp")
	if ok {
		ret = append(ret, nlp.([]string)...)
	}
	//ret = append(ret, DefaultImportantKeyword...)
	return ret
}
