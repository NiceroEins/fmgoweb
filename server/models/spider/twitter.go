package spider

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"strconv"
)

type Twitter struct {
	Author    string `json:"author"`
	Title     string `json:"title"`
	TitleTime string `json:"title_time,omitempty"`
	Link      string `json:"link,omitempty"`
}

func ParseTwitterDetail(content *NewsContent) *NewsContent {
	if content.Twitter == (Twitter{}) {
		return nil
	}
	id, err := twitterName2SeedID(content.Twitter.Author)
	if err != nil || id == 0 {
		return nil
	}
	content.Title = content.Twitter.Title
	content.Link = content.Twitter.Link
	content.Author = content.Twitter.Author
	content.TitleTime = content.Twitter.TitleTime
	content.SeedID = strconv.Itoa(id)

	return content
}

func twitterName2SeedID(name string) (int, error) {
	var id []int
	name = "T-" + name
	db := models.DB().Table("u_seed")
	if err := db.Where("(catch_path = ? OR catch_site = ?) AND deleted_at IS NULL", name, name).Pluck("id", &id).Error; err != nil {
		logging.Error("spider.twitterName2SeedID() Query seed id Errors: ", err.Error())
		return 0, err
	}
	if id == nil || len(id) == 0 {
		return 0, nil
	}
	return id[0], nil
}
