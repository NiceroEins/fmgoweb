package quantization

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"fmt"
	"strings"
	"time"
)

type Stock struct {
	Code   string
	Time   *time.Time
	Open   float64
	Close  float64
	High   float64
	Low    float64
	Amount float64
}

type DayLine struct {
	StockCode string
	TradeDate string
	Open      float64
	High      float64
	Low       float64
	Close     float64
	Block     float64
}

type Highest struct {
	Code        string
	Date        string
	Price       float64
	Pre         float64
	Description string
}

type HighestEx struct {
	Highest
	Open           float64
	High           float64
	Low            float64
	IsBlock        int
	PreClose       float64
	SecondIndustry string
	TradeID        int
	BuyPrice       float64                `gorm:"-"`
	SoldPrice      float64                `gorm:"-"`
	Types          map[string]interface{} `gorm:"-"`
	IndustryCnt    int                    `gorm:"-"`
}

func single(code string) {
	var price []float64
	var highest float64
	var curTime time.Time
	models.Data().Table("p_stock_tick").Select("MAX(high) AS high").Where("stock_code = ?", code).Where("trade_date < ?", "2019-01-01").Limit(1).Pluck("high", &price)

	if len(price) > 0 && price[0] > 0 {
		highest = price[0]
	}
	curTime = time.Date(2018, 12, 31, 0, 0, 0, 0, time.Local)

	for {
		var stock DayLine
		var err error
		var n int
		if err = models.Data().Table("p_stock_tick").Where("stock_code = ?", code).Where("high > ?", highest).Where("trade_date > ?", curTime.Format("2006-01-02")).Order("trade_date ASC").Count(&n).First(&stock).Error; err != nil || n <= 0 {
			break
		}

		if err = models.Data().Table("p_stock_highest").Create(&Highest{
			Code:  code,
			Date:  stock.TradeDate,
			Price: stock.High,
			Pre:   highest,
		}).Error; err != nil {
			logging.Error("quantization.single() create "+code+" highest Errors: ", err.Error())
			break
		}

		curTime = curTime.Add(24 * time.Hour)
		highest = stock.High
	}
}

func platform(code string) {

}

func stockCodes() []string {
	var ret []string
	models.Data().Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &ret)
	return ret
}

func GenerateHighest() {
	codes := stockCodes()
	for _, v := range codes {
		if v > "000000" {
			single(v)
		}
	}
}

type NewStock struct {
	Code string
	Date string
}

func GenerateNewStock() {
	codes := stockCodes()
	for _, v := range codes {
		var date []string
		if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", v).Where("is_block = 0").Order("trade_date ASC").Limit(1).Pluck("trade_date", &date).Error; err != nil {
			logging.Error("quantization.GenerateNewStock() Errors: ", err.Error())
			return
		}
		if len(date) > 0 && date[0] != "" {
			t := date[0]
			if strings.HasPrefix(v, "300") || strings.HasPrefix(v, "68") {
				//创业板前五日
				models.Data().Table("p_stock_tick").Where("stock_code = ?", v).Order("trade_date ASC").Limit(6).Pluck("trade_date", &date)
				if len(date) < 6 {
					t = time.Now().Format("2006-01-02")
				} else {
					t = date[5]
				}
			}
			models.Data().Table("p_stock_new").Create(&NewStock{
				Code: v,
				Date: t,
			})
		}
	}
}

type DayLineEx struct {
	DayLine
	PreClose float64
	Amount   float64
	Name     string `gorm:"-"`
}

type MinuteLine struct {
	Code  string
	Time  time.Time
	Open  float64
	Close float64
	High  float64
	Low   float64
}

type StockPlatform struct {
	Code      string
	BeginDate string
	EndDate   string
	BreakDate string
}

func GeneratePlatform() {
	var codes []string
	var maDays int = 20
	var watchPeriod int = 10
	var dailyPctg float64 = 1.08
	var maxAmplitude float64 = 1.1
	var limitLowerPctg float64 = 0.92
	var limitLowerPeriod int = 3
	var limitBreakMA int = 3

	conn := gredis.Clone(setting.RedisSetting.StockDB)

	if err := models.Data().Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("quantization.GeneratePlatform() Query codes Errors: ", err.Error())
		return
	}
	for _, v := range codes {
		var data []DayLineEx
		if err := models.Data().Table("p_stock_tick").Select("p_stock_tick.*, b_stock_names.name").Joins("LEFT JOIN b_stock_names ON b_stock_names.code = p_stock_tick.stock_code").Where("p_stock_tick.stock_code = ?", v).Order("p_stock_tick.trade_date ASC").Find(&data).Error; err != nil {
			logging.Error("stock.fetchCompanyIndustry() Errors: ", err.Error())
			continue
		}

		if strings.Index(data[0].Name, "st") != -1 || strings.Index(data[0].Name, "ST") != -1 {
			continue
		}
		fmt.Printf("Fetching %s\n", data[0].StockCode)

		//剔除证券、银行
		industry, _ := conn.HGetString("stock:industry:industry", data[0].StockCode)
		if strings.Contains(industry, "银行") || strings.Contains(industry, "证券") || strings.Contains(data[0].Name, "中国石油") || strings.Contains(data[0].Name, "中国石化") {
			continue
		}

		//// reserve
		//for k, _ := range data {
		//	if k == watchPeriod/2 {
		//		break
		//	}
		//	data[k], data[watchPeriod-k-1] = data[watchPeriod-k-1], data[k]
		//}

		db := models.Data().Table("p_stock_platform")
		for k := maDays; k < len(data); k++ {
			bPlatform := true
			curFall := 0
			breakMA := 0
			i := k
			for ; i+3 < len(data); i++ {
				if i-k >= watchPeriod {
					// 振幅大于10%
					if amplitude(data[k:i]) < maxAmplitude {
						if data[i].High > maxHigh(data[i-10:i])*1.02 {
							//突破
							db.Create(&StockPlatform{
								Code:      data[i].StockCode,
								BeginDate: data[k].TradeDate,
								EndDate:   data[i-1].TradeDate,
								BreakDate: data[i].TradeDate,
							})
							k = i
							break
						}
					}
				}
				w := data[i]
				// 当日振幅小于8%
				if w.High > w.Low*dailyPctg {
					bPlatform = false
					break
				}
				//每日成交额大于2亿（amount单位千元）
				if w.Amount < 200000 {
					bPlatform = false
					break
				}

				// 连续5个阴线
				if w.Close < w.Open {
					curFall++
					if curFall >= 5 {
						bPlatform = false
						break
					}
					if curFall > 0 && i >= limitLowerPeriod {
						if w.Close < data[i-limitLowerPeriod].Close*limitLowerPctg {
							bPlatform = false
							break
						}
					}
				} else {
					curFall = 0
				}

				//去除有过涨跌停
				if w.High > w.PreClose*1.1-0.01 || w.Low < w.PreClose*0.9+0.01 {
					bPlatform = false
					break
				}

				//每日收盘价大于20日均线
				//跌破次数小于3次
				mas := 0.0
				for j := i - maDays + 1; j <= i; j++ {
					mas += data[j].Close
				}
				ma := mas / float64(maDays)
				if w.Close < ma {
					breakMA++
					if breakMA > limitBreakMA {
						bPlatform = false
						break
					}
				}
			}

			if !bPlatform {
				//k = i
			}
		}
	}
}

func amplitude(data []DayLineEx) float64 {
	if data == nil || len(data) == 0 {
		return 0
	}
	var high float64 = 0
	var low float64 = 9999
	for _, v := range data {
		if v.High > high {
			high = v.High
		}
		if v.Low < low {
			low = v.Low
		}
	}
	return (high - low) / low
}

func maxHigh(data []DayLineEx) float64 {
	if data == nil || len(data) == 0 {
		return 0
	}
	var high float64 = 0
	for _, v := range data {
		if v.High > high {
			high = v.High
		}
	}
	return high
}

func minLow(data []DayLineEx) float64 {
	if data == nil || len(data) == 0 {
		return 0
	}
	var low float64 = 9999
	for _, v := range data {
		if v.Low < low {
			low = v.Low
		}
	}
	return low
}

func amplitudeMinute(data []MinuteLine) float64 {
	if data == nil || len(data) == 0 {
		return 0
	}
	var high float64 = 0
	var low float64 = 9999
	for _, v := range data {
		if v.High > high {
			high = v.High
		}
		if v.Low < low {
			low = v.Low
		}
	}
	return (high - low) / low
}

func maxHighMinute(data []MinuteLine) float64 {
	if data == nil || len(data) == 0 {
		return 0
	}
	var high float64 = 0
	for _, v := range data {
		if v.High > high {
			high = v.High
		}
	}
	return high
}

func minLowMinute(data []MinuteLine) float64 {
	if data == nil || len(data) == 0 {
		return 0
	}
	var low float64 = 9999
	for _, v := range data {
		if v.Low < low {
			low = v.Low
		}
	}
	return low
}
