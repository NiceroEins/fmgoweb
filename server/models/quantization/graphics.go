package quantization

import (
	"datacenter/models"
	"datacenter/pkg/util"
)

type TechnicalEx struct {
	Technical
	DayLineEx
	IsGoldX       bool    `gorm:"-"`
	IsDeadX       bool    `gorm:"-"`
	IsDIFPositive bool    `gorm:"-"`
	DerivativeDIF float64 `gorm:"-"`
	RollingDIF    int     `gorm:"-"`
	RollingX      int     `gorm:"-"`
}

func fetch(code string, day int) []TechnicalEx {
	var ret []TechnicalEx
	var tech []Technical
	var dayLine []DayLineEx
	models.Data().Table("p_stock_tick").Where("stock_code = ?", code).Where("trade_date <= ?", "2020-12-17").Order("trade_date DESC").Limit(day).Find(&dayLine)
	models.Data().Table("p_stock_technical_index").Where("code = ?", code).Where("date <= ?", "2020-12-17").Order("date DESC").Limit(day).Find(&tech)
	util.Reverse(dayLine)
	util.Reverse(tech)
	rx := 0
	for i := 0; i < len(tech) && i < len(dayLine); i++ {
		var techEx TechnicalEx
		techEx.Technical = tech[i]
		techEx.DayLineEx = dayLine[i]
		if i > 0 {
			if tech[i].DIF >= 0 {
				techEx.IsDIFPositive = true
			}
			techEx.RollingX = rx
			if tech[i-1].DIF < tech[i-1].DEA && tech[i].DIF >= tech[i].DEA {
				techEx.IsGoldX = true
				rx = 1
				techEx.RollingX = 0
			}
			if tech[i-1].DIF > tech[i-1].DEA && tech[i].DIF <= tech[i].DEA {
				techEx.IsDeadX = true
				rx = -1
				techEx.RollingX = 0
			}
			if tech[i-1].DIF > 0 {
				techEx.DerivativeDIF = (tech[i].DIF - tech[i-1].DIF) / tech[i-1].DIF
			}
		}
		ret = append(ret, techEx)
	}
	util.Reverse(ret)
	return ret
}

//佛手向上
func buddha(data []TechnicalEx) {
	//var minPeriod = 10
	//
	//for i, v := range data {
	//
	//}
}

//func calcBuddha(data []TechnicalEx) bool {
//
//}
