package quantization

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"fmt"
	"strings"
	"time"
)

type HighestReq struct {
	HighestType    []string    `json:"type" form:"type"`
	MinPrice       float64     `json:"min_price" form:"min_price"`
	MaxPrice       float64     `json:"max_price" form:"max_price"`
	MinAmount      float64     `json:"min_amount" form:"min_amount"`
	MaxAmount      float64     `json:"max_amount" form:"max_amount"`
	MinMarketValue float64     `json:"min_market_value" form:"min_market_value"`
	MaxMarketValue float64     `json:"max_market_value" form:"max_market_value"`
	RSI            []string    `json:"rsi,omitempty" form:"rsi,omitempty"`
	RsiDetail      []RSIDetail `json:"rsi_detail,omitempty" form:"rsi_detail,omitempty"`
	TimeBegin      time.Time   `json:"begin" form:"begin"`
	TimeEnd        time.Time   `json:"end" form:"end"`
	Period         []int       `json:"period" form:"period"`
	MinPeriod      int         `json:"min_period,omitempty" form:"min_period,omitempty"` //最短新高周期
}

type RSIDetail struct {
	Name string  `json:"name" form:"name"`
	Low  float64 `json:"low" form:"low"`
	High float64 `json:"high" form:"high"`
}

type HighestDetailReq struct {
	HighestReq
	Day      int    `json:"day,omitempty" form:"day,omitempty"`
	PageNum  int    `json:"p" form:"p"`
	PageSize int    `json:"n" form:"n"`
	Sort     string `json:"sort,omitempty" form:"sort,omitempty"`
	Order    string `json:"order,omitempty" form:"order,omitempty"`
}

type HighestResp struct {
	Period    int     `json:"period"`
	AvgRate   float64 `json:"avg_rate"`
	MaxRate   float64 `json:"max_rate"`
	MinRate   float64 `json:"min_rate"`
	RiseProb  float64 `json:"rise_prob"`
	Num       int     `json:"-"`
	RisingNum int     `json:"-"`
	Sum       float64 `json:"-"`
}

type HighestDetailResp struct {
	ID          int                 `json:"id"`
	Code        string              `json:"code"`
	Name        string              `json:"name"`
	TimeBegin   string              `json:"begin"`
	TimeEnd     string              `json:"end"`
	PriceBegin  float64             `json:"price_begin"`
	PriceEnd    float64             `json:"price_end"`
	Change      float64             `json:"change"`
	Day         int                 `json:"day"`
	Industry    string              `json:"industry"`
	TradeID     int                 `json:"trade_id"`
	Children    []HighestDetailResp `json:"children"`
	IndustryCnt int                 `json:"industry_cnt"`
	//Types        map[string]interface{} `json:"-"`
}

type HighestDetailExport struct {
	Code       string  `json:"A"`
	Name       string  `json:"B"`
	TimeBegin  string  `json:"C"`
	TimeEnd    string  `json:"D"`
	PriceBegin float64 `json:"E"`
	PriceEnd   float64 `json:"F"`
	Change     float64 `json:"G"`
}

func Analyze(hr *HighestReq) []HighestEx {
	var ret, data []HighestEx
	if hr == nil {
		return ret
	}

	db := models.Data().
		Table("p_stock_highest").
		Select("p_stock_highest.*, p_stock_tick.open, p_stock_tick.high, p_stock_tick.low, p_stock_tick.is_block, p_stock_tick.pre_close, u_stock_industry.second_industry, b_trade_date.id AS trade_id").
		Joins("LEFT JOIN p_stock_tick ON p_stock_highest.code = p_stock_tick.stock_code AND p_stock_highest.date = p_stock_tick.trade_date").
		Joins("LEFT JOIN p_stock_new ON p_stock_new.code = p_stock_highest.code").
		Joins("LEFT JOIN p_stock_platform ON p_stock_platform.code = p_stock_highest.code AND p_stock_highest.date = p_stock_platform.break_date").
		Joins("LEFT JOIN u_stock_industry ON u_stock_industry.code = p_stock_highest.code").
		Joins("LEFT JOIN b_trade_date ON b_trade_date.trade_date = p_stock_highest.date").
		Joins("LEFT JOIN b_trade_date BTD ON BTD.id = b_trade_date.id - 1").
		Joins("LEFT JOIN p_stock_tick PST ON PST.stock_code = p_stock_tick.stock_code AND PST.trade_date = BTD.trade_date")
	var condition string
	for _, v := range hr.HighestType {
		hType := v
		switch hType {
		case "block":
			condition += "(p_stock_tick.high = p_stock_tick.block)"
		case "100":
			//百元创新高：突破100元买入
			//condition += "(p_stock_highest.price >= 100 AND p_stock_highest.pre < 100 AND p_stock_highest.date > p_stock_new.date)"
			//新高：除一字板外买入
			condition += "(p_stock_tick.low < p_stock_tick.high)"
		case "sub-new":
			condition += "(p_stock_highest.date > p_stock_new.date AND p_stock_highest.date <= date_add(p_stock_new.date, interval 84 day))"
		case "platform":
			condition += "(p_stock_platform.break_date IS NOT NULL)"
		case "industry":
			condition += "(u_stock_industry.second_industry IS NOT NULL AND p_stock_highest.date > p_stock_new.date)"
		}

		if len(condition) > 0 {
			db = db.Where(condition)
		}
		db = db.Where("p_stock_highest.price >= ? AND p_stock_highest.price <= ?", hr.MinPrice, hr.MaxPrice).Where("PST.amount >= ? AND PST.amount <= ?", hr.MinAmount/1000, hr.MaxAmount/1000).Where("p_stock_highest.date >= ? AND p_stock_highest.date <= ?", hr.TimeBegin, hr.TimeEnd).Group("p_stock_highest.code, p_stock_highest.date")
		if err := db.Order("p_stock_highest.code ASC, p_stock_highest.date ASC").Find(&data).Error; err != nil {
			logging.Error("quantization.analyze() query Errors: ", err.Error())
		}

		conn := gredis.Clone(setting.RedisSetting.StockDB)
		m, _ := conn.HGetAllStringMap("stock:zgb")
		for k, v := range data {
			//zgb, _ := conn.HGetFloat64("stock:zgb", v.Code)
			z, ok := m[v.Code]
			if !ok {
				continue
			}
			zgb := util.Atof(z)
			if zgb*v.Price < hr.MinMarketValue || zgb*v.PreClose > hr.MaxMarketValue {
				continue
			}

			//有停牌（刚上市）
			if v.Pre == 0 {
				continue
			}

			////前几日有新高
			if hr.MinPeriod > 0 {
				if k > 0 && v.Code == data[k-1].Code && v.TradeID-data[k-1].TradeID <= hr.MinPeriod {
					continue
				}
			}

			switch hType {
			case "block":
				//剔除前10个交易日有涨停的
				if b, err := haveBlock(v.Code, v.Date, 10); err != nil || b {
					continue
				}
				//剔除近3日内创过新高的
				if k > 0 && v.Code == data[k-1].Code && v.TradeID-data[k-1].TradeID < 3 {
					continue
				}
				//涨停创新高:起始价：涨停板买入
				if strings.HasPrefix(v.Code, "300") || strings.HasPrefix(v.Code, "688") {
					continue
				} else {
					v.BuyPrice = v.High
				}
			case "100":
				if b, err := haveBlock(v.Code, v.Date, 10); err != nil || b {
					continue
				}
				//百元创新高：突破100元买入
				//v.BuyPrice = 100
				v.BuyPrice = v.Pre
			case "sub-new":
				if b, err := haveBlock(v.Code, v.Date, 10); err != nil || b {
					continue
				}
				//剔除近3日内创过新高的
				if k > 0 && v.Code == data[k-1].Code && v.TradeID-data[k-1].TradeID < 3 {
					continue
				}
				v.BuyPrice = v.Pre
			case "platform":
				//剔除近3日内创过新高的
				if k > 0 && v.Code == data[k-1].Code && v.TradeID-data[k-1].TradeID < 3 {
					continue
				}
				v.BuyPrice = v.Pre
			case "industry":
				//剔除近3日内创过新高的
				if k > 0 && v.Code == data[k-1].Code && v.TradeID-data[k-1].TradeID < 3 {
					continue
				}
				//剔除前10个交易日有涨停的
				if b, err := haveBlock(v.Code, v.Date, 10); err != nil || b {
					continue
				}

				//cnt := CountIndustry(v, data)
				//if cnt < 2 {
				//	continue
				//}
				v.BuyPrice = v.Pre
				//v.IndustryCnt = cnt
			}
			//跳空高开的创新高，以开盘价买入（剔除一字板）
			if v.Open > v.BuyPrice {
				v.BuyPrice = v.Open
			}
			if v.Low == v.High && v.IsBlock == 1 {
				continue
			}
			v.Types = make(map[string]interface{})
			v.Types[hType] = 1
			ret = append(ret, v)
		}
	}
	return ret
}

func Calc(h HighestEx, day int, data []HighestEx) *HighestDetailResp {
	var stock []struct {
		Code      string
		Close     float64
		TradeDate string
		High      float64
		Open      float64
		Low       float64
	}
	if day < 1 || h.Price <= 0 || h.Pre <= 0 {
		return nil
	}
	if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", h.Code).Where("trade_date > ?", h.Date).Order("trade_date ASC").Limit(1).Offset(day - 1).Find(&stock).Error; err != nil || len(stock) <= 0 {
		if err != nil {
			logging.Error("quantization.calc() Errors: ", err.Error())
		}
		return nil
	}
	//buyPrice := h.BuyPrice
	if h.BuyPrice <= 0 {
		logging.Error("highest.Calc() zero buyPrice: ", stock[0].Code, stock[0].TradeDate)
		return nil
	}

	switch {
	case HaveType(h.Types, "block"):
		h.SoldPrice = stock[0].Close
	case HaveType(h.Types, "100"):
		//百元新高次日达到3%止盈
		if stock[0].Open > h.BuyPrice*1.03 {
			h.SoldPrice = stock[0].Open
		} else if stock[0].High < h.BuyPrice*1.03 {
			h.SoldPrice = stock[0].Close
		} else {
			h.SoldPrice = h.BuyPrice * 1.03
		}
	case HaveType(h.Types, "industry"):
		if cnt := CountIndustry(h, data); cnt < 1 {
			return nil
		} else {
			h.IndustryCnt = cnt
		}
		h.SoldPrice = stock[0].Close
	default:
		h.SoldPrice = stock[0].Close
	}

	conn := gredis.Clone(setting.RedisSetting.StockDB)
	name, _ := conn.HGetString("stock:name", h.Code)
	change := (h.SoldPrice - h.BuyPrice) / h.BuyPrice
	ret := HighestDetailResp{
		Code:        h.Code,
		Name:        name,
		TimeBegin:   h.Date,
		TimeEnd:     stock[0].TradeDate,
		PriceBegin:  h.BuyPrice,
		PriceEnd:    stock[0].Close,
		Change:      change,
		Day:         day,
		IndustryCnt: h.IndustryCnt,
	}
	return &ret
}

func haveBlock(code, date string, day int) (bool, error) {
	var isBlock []int
	if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", code).Where("trade_date < ?", date).Order("trade_date DESC").Limit(day).Pluck("is_block", &isBlock).Error; err != nil {
		logging.Error("quantization.haveBlock() query "+code+" Errors: ", err.Error())
		return false, err
	}
	if len(isBlock) < day {
		return false, fmt.Errorf("data length < 3")
	}
	for _, v := range isBlock {
		if v == 1 {
			return true, nil
		}
	}
	return false, nil
}

func isResumed(code, date string) bool {
	return false
}

//func isNewStock(code, date string) (bool, error) {
//	if err := models.Data().Table("p_stock_tick").Where("")
//}

func HaveType(types map[string]interface{}, data string) bool {
	if types == nil {
		return false
	}
	if d, ok := util.GetInt(types, data); ok && d > 0 {
		return true
	}
	return false
}

func CountIndustry(stock HighestEx, data []HighestEx) int {
	ret := 0
	var lastCode string
	for _, v := range data {
		if stock.Code == v.Code || stock.Code == lastCode {
			continue
		}
		//if v.IndustryID > 0 && v.IndustryID == stock.IndustryID && v.TradeID > 0 && stock.TradeID > 0 {
		//	if stock.TradeID < v.TradeID+5 && stock.TradeID > v.TradeID-5 {
		//		ret++
		//		lastCode = v.Code
		//	}
		//}
		if sameIndustry(stock, v) {
			ret++
			lastCode = v.Code
		}
	}
	return ret
}

func sameIndustry(data, src HighestEx) bool {
	if src.SecondIndustry != "" && data.SecondIndustry == src.SecondIndustry && src.TradeID > 0 && data.TradeID > 0 {
		if data.TradeID < src.TradeID+5 && data.TradeID > src.TradeID-5 {
			return true
		}
	}
	return false
}

func GenerateID(code, begin, end string) int {
	return int(util.BKDRHash(code + "&" + begin + "&" + end + "&" + util.GenerateRandomString(6)))
}
