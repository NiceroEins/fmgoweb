package quantization

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"fmt"
	"strings"
	"sync"
	"time"
)

type Req struct {
	QAType    string    `json:"type" form:"type"`
	TimeBegin time.Time `json:"begin" form:"begin"`
	TimeEnd   time.Time `json:"end" form:"end"`
}

type QAReq struct {
	Req
	PageNum  int    `json:"p" form:"p"`
	PageSize int    `json:"n" form:"n"`
	Sort     string `json:"sort,omitempty" form:"sort,omitempty"`
	Order    string `json:"order,omitempty" form:"order,omitempty"`
}

type QADetailReq struct {
	QAReq
	Author  string `json:"author" form:"author"`
	Succeed string `json:"succeed" form:"succeed"`
	//PageNum  int    `json:"p" form:"p"`
	//PageSize int    `json:"n" form:"n"`
	//Sort     string `json:"sort,omitempty" form:"sort,omitempty"`
	//Order    string `json:"order,omitempty" form:"order,omitempty"`
}

type QAResp struct {
	AvgRate   float64 `json:"avg_rate"`
	MaxRate   float64 `json:"max_rate"`
	MinRate   float64 `json:"min_rate"`
	RiseProb  float64 `json:"rise_prob"`
	Num       int     `json:"count"`
	RisingNum int     `json:"-"`
	Sum       float64 `json:"-"`
	Author    string  `json:"author"`
}

type QADetailResp struct {
	Code         string  `json:"code"`
	Name         string  `json:"name"`
	TimeBegin    string  `json:"begin"`
	TimeEnd      string  `json:"end"`
	PriceBegin   float64 `json:"price_begin"`
	PriceEnd     float64 `json:"price_end"`
	Change       float64 `json:"change"`
	Title        string  `json:"title"`
	Content      string  `json:"content"`
	Author       string  `json:"author"`
	Time         string  `json:"time"`
	QuestionTime string  `json:"question_time"`
}

type QADetailExport struct {
	Code       string  `json:"A"`
	Name       string  `json:"B"`
	TimeBegin  string  `json:"C"`
	TimeEnd    string  `json:"D"`
	PriceBegin float64 `json:"E"`
	PriceEnd   float64 `json:"F"`
	Change     float64 `json:"G"`
}

type QAEx struct {
	ID        int
	CatchTime time.Time
	Author    string
	Code      string
	Name      string
	Open      float64
	Close     float64
	Close2    float64
	Close3    float64
	Open2     float64
	Title     string
	Content   string
	TitleTime string
	Date      string  `gorm:"-"`
	BuyPrice  float64 `gorm:"-"`
	SoldPrice float64 `gorm:"-"`
	QAType    string  `gorm:"-"`
}

func QAAnalyze(hr *QAReq) []QAEx {
	var ret []QAEx
	if hr == nil {
		return ret
	}

	db := models.Data().
		Table("u_feed").
		Select("u_feed.id, u_feed.title, u_feed.content, u_feed.title_time, u_feed.catch_time, u_feed.author, b_stock_names.code, b_stock_names.name, p_stock_tick.open, p_stock_tick.close, AP.close AS close2, BP.close AS close3, AP.open AS open2").
		Joins("LEFT JOIN b_stock_names ON b_stock_names.code = u_feed.object").
		Joins("LEFT JOIN p_stock_tick ON u_feed.object = p_stock_tick.stock_code AND DATE(u_feed.catch_time) = p_stock_tick.trade_date").
		Joins("LEFT JOIN b_trade_date ON b_trade_date.trade_date = p_stock_tick.trade_date").
		Joins("LEFT JOIN b_trade_date A ON A.id = b_trade_date.id + 1").
		Joins("LEFT JOIN b_trade_date B ON B.id = b_trade_date.id + 2").
		Joins("LEFT JOIN p_stock_tick AP ON AP.trade_date = A.trade_date AND u_feed.object = AP.stock_code").
		Joins("LEFT JOIN p_stock_tick BP ON BP.trade_date = B.trade_date AND u_feed.object = BP.stock_code")

	//var condition string
	//switch hr.QAType {
	//case "question":
	//	condition += "(p_stock_tick.is_block = 1 OR LEFT(p_stock_tick.stock_code, 3) = '300' AND p_stock_tick.pct_chg >= 10)"
	//case "answer":
	//	condition += "(p_stock_highest.price >= 100 AND p_stock_highest.pre < 100 AND p_stock_highest.date > p_stock_new.date)"
	//}
	//if len(condition) > 0 {
	//	//db = db.Where(condition)
	//}
	db = db.Where("u_feed.object IS NOT NULL").Where("u_feed.type = 'qa'").Where("u_feed.correlation = 1").Where("u_feed.catch_time >= ? AND u_feed.catch_time <= ?", hr.TimeBegin, hr.TimeEnd)
	if err := db.Group("b_stock_names.code, u_feed.author, DATE(u_feed.catch_time)").Order("u_feed.author, u_feed.catch_time").Find(&ret).Error; err != nil {
		logging.Error("quantization.QAAnalyze() query Errors: ", err.Error())
	}

	for i := 0; i < len(ret); i++ {
		ret[i].QAType = hr.QAType
	}
	return ret
}

func QACalc(h QAEx) *QADetailResp {
	var stock []DayLine
	if strings.Contains(h.Name, "ST") {
		return nil
	}

	var tmBegin, tmEnd string
	h.Date = h.CatchTime.Format("2006-01-02")
	tt := parseTime(h.TitleTime, h.CatchTime)
	//if h.Code == "300453" && h.CatchTime.Format("2006-01-02 15:04:05") == "2020-12-25 11:57:41" {
	//	fmt.Println(h)
	//}
	switch h.QAType {
	case "question":
		var price []float64
		if err := models.Data().Table("p_stock_minute").Where("code = ?", h.Code).Where("time >= ?", tt.Format("2006-01-02 15:04:05")).Order("time ASC").Limit(1).Pluck("open", &price).Error; err != nil || len(price) < 1 {
			return nil
		}
		h.BuyPrice = price[0]
		tmBegin = tt.Format("2006-01-02")
		if tt.YearDay() == h.CatchTime.YearDay() {
			if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", h.Code).Where("trade_date > ?", h.Date).Order("trade_date ASC").Limit(1).Find(&stock).Error; err != nil || len(stock) < 1 {
				return nil
			}
			h.SoldPrice = stock[0].Open
		} else if h.CatchTime.Hour() >= 15 {
			if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", h.Code).Where("trade_date > ?", h.Date).Order("trade_date ASC").Limit(1).Find(&stock).Error; err != nil || len(stock) < 1 {
				return nil
			}
			h.SoldPrice = stock[0].Open
		} else {
			if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", h.Code).Where("trade_date = ?", h.Date).Order("trade_date ASC").Limit(1).Find(&stock).Error; err != nil || len(stock) < 1 {
				return nil
			}
			h.SoldPrice = stock[0].Close
		}
		tmEnd = stock[0].TradeDate
	case "answer":
		//提问回答超过15天不计入
		if h.CatchTime.Sub(tt) > 15*24*time.Hour {
			return nil
		}
		if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", h.Code).Where("trade_date >= ?", h.Date).Order("trade_date ASC").Limit(3).Find(&stock).Error; err != nil || len(stock) < 3 {
			return nil
		}
		tmBegin = stock[0].TradeDate
		tmEnd = stock[1].TradeDate
		if !onTrade(h.CatchTime) {
			h.BuyPrice = stock[0].Open
			h.SoldPrice = stock[1].Open
		} else {
			if h.CatchTime.Hour() < 9 || h.CatchTime.Hour() == 9 && h.CatchTime.Minute() < 30 {
				h.BuyPrice = stock[0].Open
				h.SoldPrice = stock[1].Open
			} else if h.CatchTime.Hour() < 15 {
				var price []float64
				if err := models.Data().Table("p_stock_minute").Where("code = ?", h.Code).Where("time >= ?", h.CatchTime).Order("time ASC").Limit(1).Pluck("open", &price).Error; err != nil || len(price) < 1 {
					return nil
				}
				h.BuyPrice = price[0]
				h.SoldPrice = stock[1].Open
				tmBegin = h.Date
			} else {
				h.BuyPrice = stock[1].Open
				h.SoldPrice = stock[2].Open
				tmBegin = stock[1].TradeDate
				tmEnd = stock[2].TradeDate
			}
		}
	}

	if h.BuyPrice <= 0 || h.SoldPrice <= 0 {
		logging.Error("quantization.QACalc() zero price: ", h.Code, h.Date, h.BuyPrice, h.SoldPrice)
		return nil
	}

	change := (h.SoldPrice - h.BuyPrice) / h.BuyPrice
	ret := QADetailResp{
		Code:         h.Code,
		Name:         h.Name,
		TimeBegin:    tmBegin,
		TimeEnd:      tmEnd,
		PriceBegin:   h.BuyPrice,
		PriceEnd:     h.SoldPrice,
		Change:       change,
		Author:       h.Author,
		Title:        h.Title,
		Content:      h.Content,
		Time:         h.CatchTime.Format("2006-01-02 15:04:05"),
		QuestionTime: tt.Format("2006-01-02 15:04:05"),
	}
	return &ret
}

func parseTime(tm string, catchTime time.Time) time.Time {
	if t, err := time.ParseInLocation("2006-01-02 15:04:05", tm, time.Local); err == nil {
		return t
	}
	if strings.Contains(tm, "月") {
		var month, day, hour, minute int
		if _, err := fmt.Sscanf(tm, "%d月%d日 %d:%d", &month, &day, &hour, &minute); err == nil {
			return time.Date(catchTime.Year(), time.Month(month), day, hour, minute, 0, 0, time.Local)
		}
	}
	if strings.Contains(tm, "昨天") {
		var hour, minute int
		tLast := catchTime.Add(-24 * time.Hour)
		if _, err := fmt.Sscanf(tm, "昨天 %d:%d", &hour, &minute); err == nil {
			return time.Date(tLast.Year(), tLast.Month(), tLast.Day(), hour, minute, 0, 0, time.Local)
		}
	}
	if strings.Contains(tm, "前") {
		var n int
		var suffix string
		if _, err := fmt.Sscanf(tm, "%d%s", &n, &suffix); err == nil {
			switch suffix {
			case "天前":
				return catchTime.Add(time.Duration(n) * -24 * time.Hour)
			case "小时前":
				return catchTime.Add(time.Duration(n) * -1 * time.Hour)
			case "分前", "分钟前":
				return catchTime.Add(time.Duration(n) * -1 * time.Minute)
			case "秒前":
				return catchTime.Add(time.Duration(n) * -1 * time.Second)
			}
		}
	}
	return catchTime
}

var TradeDates []time.Time
var locker sync.RWMutex

func onTrade(tm time.Time) bool {
	if len(TradeDates) == 0 {
		locker.Lock()
		models.Data().Table("b_trade_date").Pluck("trade_date", &TradeDates)
		locker.Unlock()
	}
	locker.RLock()
	defer locker.RUnlock()
	ts := tm.Format("2006-01-02")
	for _, v := range TradeDates {
		if ts == v.Format("2006-01-02") {
			return true
		}
	}
	return false
}
