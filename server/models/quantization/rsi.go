package quantization

import (
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"datacenter/pkg/util"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strings"
	"time"
)

func reportPeriod(t time.Time) (int, int, int) {
	year := t.Year()
	var month, day int
	switch {
	case t.Month() == 4 && t.Day() >= 15 || t.Month() == 5:
		month = 3
		day = 31
	case t.Month() >= 6 && t.Month() <= 9:
		month = 6
		day = 30
	case t.Month() >= 10 && t.Month() <= 12:
		month = 9
		day = 30
	default:
		year -= 1
		month = 12
		day = 31
	}
	return year, month, day
}

func reportPeriodStr(t time.Time) string {
	year, month, day := reportPeriod(t)
	return fmt.Sprintf("%d-%02d-%02d", year, month, day)
}

//同比上期
func periodYearlyPastStr(t time.Time) string {
	year, month, day := reportPeriod(t)
	return fmt.Sprintf("%d-%02d-%02d", year-1, month, day)
}

//环比上期
func periodPastStr(t time.Time) string {
	year, month, day := reportPeriod(t)
	switch month {
	case 3:
		year -= 1
		month = 12
		day = 31
	case 6:
		month = 3
		day = 31
	case 9:
		month = 6
		day = 30
	case 12:
		month = 9
		day = 30
	}
	return fmt.Sprintf("%d-%02d-%02d", year, month, day)
}

func Ystb(code string, t time.Time, low, high float64) bool {
	var data map[string]interface{}
	err := mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"type": "营业总收入同比增长率",
		}).One(&data)
	})
	if err != nil {
		logging.Error("quantization.Ystb() query Errors: ", code, err.Error())
		return false
	}
	v, ok := util.GetString(data, reportPeriodStr(t))
	if !ok || len([]byte(v)) == 0 || []byte(v)[0] == '-' {
		return false
	}
	return true
}

func Kfjl(code string, t time.Time, low, high float64) bool {
	var data map[string]interface{}
	err := mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"type": "扣非净利润同比增长率",
		}).One(&data)
	})
	if err != nil {
		logging.Error("quantization.Kfjl() query Errors: ", code, err.Error())
		return false
	}
	v, ok := util.GetString(data, reportPeriodStr(t))
	if !ok || len([]byte(v)) == 0 || []byte(v)[0] == '-' {
		return false
	}
	return true
}

func Mllh(code string, t time.Time, low, high float64) bool {
	var data map[string]interface{}
	err := mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"type": "销售毛利率",
		}).One(&data)
	})
	if err != nil {
		logging.Error("quantization.Mllh() query Errors: ", code, err.Error())
		return false
	}

	vNow, ok := util.GetString(data, reportPeriodStr(t))
	if !ok || len([]byte(vNow)) == 0 {
		return false
	}

	vPast, ok := util.GetString(data, periodPastStr(t))
	if !ok || len([]byte(vPast)) == 0 {
		return false
	}

	return comparePctg(vNow, vPast) > 0
}

func Jyxj(code string, t time.Time, low, high float64) bool {
	var data map[string]interface{}
	err := mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"type": "每股经营现金流",
		}).One(&data)
	})
	if err != nil {
		logging.Error("quantization.Jyxj() query Errors: ", code, err.Error())
		return false
	}
	ok, vNow := util.GetNumber(data, reportPeriodStr(t))
	if !ok || vNow == 0 {
		return false
	}
	ok, vPast := util.GetNumber(data, periodYearlyPastStr(t))
	if !ok || vPast == 0 {
		return false
	}
	return vNow > vPast
}

func Yszk(code string, t time.Time, low, high float64) bool {
	var ys, ht map[string]interface{}
	err := mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"type": "预收款项",
		}).One(&ys)
	})
	if err != nil {
		logging.Error("quantization.Yszk() query yszk Errors: ", code, err.Error())
		return false
	}

	err = mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"type": "合同负债",
		}).One(&ht)
	})
	if err != nil {
		logging.Error("quantization.Yszk() query Errors: ", code, err.Error())
		return false
	}

	_, ysNow := util.GetNumber(ys, reportPeriodStr(t))
	_, htNow := util.GetNumber(ht, reportPeriodStr(t))

	_, ysPast := util.GetNumber(ys, periodYearlyPastStr(t))
	_, htPast := util.GetNumber(ht, periodYearlyPastStr(t))

	return ysNow+htNow > ysPast+htPast
}

func Jzcs(code string, t time.Time, low, high float64) bool {
	var data map[string]interface{}
	err := mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"type": "净资产收益率",
		}).One(&data)
	})
	if err != nil {
		logging.Error("quantization.Jzcs() query Errors: ", code, err.Error())
		return false
	}

	vNow, ok := util.GetString(data, reportPeriodStr(t))
	if !ok || len([]byte(vNow)) == 0 {
		return false
	}

	vPast, ok := util.GetString(data, periodPastStr(t))
	if !ok || len([]byte(vPast)) == 0 {
		return false
	}

	return comparePctg(vNow, vPast) > 0
}

// returns v1-v2 ignoring "%"
func comparePctg(v1, v2 string) float64 {
	v1 = strings.Replace(v1, "%", "", -1)
	v2 = strings.Replace(v2, "%", "", -1)
	return util.Atof(v1) - util.Atof(v2)
}
