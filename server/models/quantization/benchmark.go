package quantization

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"math"
	"sort"
	"strings"
	"sync"
	"time"
)

type HighestBenchmarkEx struct {
	HighestReq
	MaxPosition int     `json:"max_position" form:"max_position"`               //最大仓位
	Principal   float64 `json:"principal" form:"principal"`                     //本金
	DaysAverage int     `json:"days_average" form:"days_average"`               //日均线天数
	Points      float64 `json:"points,omitempty" form:"points,omitempty"`       //低于均线百分比卖出
	StopLoss    float64 `json:"stop_loss,omitempty" form:"stop_loss,omitempty"` //止损
	StopRetract float64 `json:"stop_retract" form:"stop_retract"`               //单股最大回撤
}

type HighestBenchmarkReq struct {
	HighestBenchmarkEx
	StartID int `json:"start_id,omitempty" form:"start_id,omitempty"`
	//Policy      string  `json:"policy" form:"policy"`
}

type BenchmarkDetail struct {
	Code         string     `json:"code"`
	Position     int        `json:"position"`      //仓位(手)
	BuyPrice     float64    `json:"buy_price"`     //买入价
	CurPrice     float64    `json:"cur_price"`     //现价（买入/卖出价，若无交易则为收盘价）
	Change       float64    `json:"change"`        //盈亏（比例）
	BuyDate      string     `json:"buy_date"`      //买入日期
	Value        float64    `json:"principal"`     //单个股票持仓价值
	Operation    string     `json:"operation"`     //操作（持有/买入/卖出）
	TradeTime    time.Time  `json:"trade_time"`    //交易时间
	Gain         float64    `json:"gain"`          //收入
	DayLine      DayLineAvg `json:"day_line"`      //日线
	HighestClose float64    `json:"highest_close"` //历史最高收盘价（买入起）
	unavailable  bool       `json:"-"`
}

type BenchmarkDetailList struct {
	Date      string  `json:"date"`
	Code      string  `json:"code"`
	Operation string  `json:"operation"`
	Price     float64 `json:"price"`
	Vol       int     `json:"vol"`
	Amount    float64 `json:"amount"`
	Gain      float64 `json:"gain"`
}

type HighestBenchmarkResp struct {
	ID            int               `json:"id"`
	Date          string            `json:"date"`
	Benchmark     float64           `json:"benchmark"`      //基准收益
	OverallReturn float64           `json:"overall_return"` //策略收益
	Earn          float64           `json:"earn"`
	Lose          float64           `json:"lose"`
	Position      int               `json:"position"` //持仓数量
	Detail        []BenchmarkDetail `json:"detail"`
}

type BenchmarkRetract struct {
	Date          string  `json:"date"`
	OverallReturn float64 `json:"overall_return"`
}

type HighestBenchmarkSummary struct {
	OverallReturn       float64               `json:"overall_return"`    //策略收益
	AnnualizedReturn    float64               `json:"annualized_return"` //年化收益
	Sharpe              float64               `json:"sharpe"`            //夏普比率
	RiseProb            float64               `json:"rise_prob"`         //胜率
	EarnCnt             int                   `json:"earn_cnt"`
	LoseCnt             int                   `json:"lose_cnt"`
	MaxRetract          float64               `json:"max_retract"`
	MaxBenchmarkRetract float64               `json:"max_benchmark_retract"`
	Retract             []BenchmarkRetract    `json:"retract"` //回撤点(日期)
	Detail              []BenchmarkDetailList `json:"detail"`
}

type BenchmarkDetailSummary struct {
	List    []BenchmarkDetailList
	EarnCnt int
	LoseCnt int
}

// 持仓情况
type Position struct {
	Principal float64 //剩余本金
	//PrePrincipal float64 //前日本金
	TotalValue  float64 //总净值
	PreValue    float64 //前一日净值
	MaxPosition int     //最大仓位个数
	Days        int
	Points      float64 //卖出点（相对于均线）
	StopRetract float64 //单股最大回撤
	StopLoss    float64 //止损百分比
	MinPeriod   int     //最短新高周期
	Data        []BenchmarkDetail
	Summary     BenchmarkDetailSummary
}

// 原始买入卖出数据
type Raw struct {
	Code      string
	Date      string
	Price     float64
	TradeTime time.Time
	Sell      bool
}

type DayLineAvg struct {
	DayLine
	Avg      float64
	PreClose float64
}

var cache sync.Map

func Benchmark(hr *HighestBenchmarkReq, data []HighestDetailResp) []HighestBenchmarkResp {
	var ret []HighestBenchmarkResp
	var ok bool
	if ret, ok = loadCache(hr); !ok {
		if setting.ServerSetting.RunMode == "debug" {
			calcBenchmark(hr, data)
		} else {
			go calcBenchmark(hr, data)
			time.Sleep(1 * time.Second)
		}
		ret, _ = loadCache(hr)
	}

	return ret[hr.StartID:]
}

func TradeDate(hr *HighestBenchmarkReq) []string {
	var ret []string
	var ts []time.Time
	models.Data().Table("b_trade_date").Where("trade_date >= ?", hr.TimeBegin.Format(util.YMD)).Where("trade_date <= ?", hr.TimeEnd.Format(util.YMD)).Order("trade_date").Pluck("trade_date", &ts)
	for _, v := range ts {
		ret = append(ret, v.Format(util.YMD))
	}
	return ret
}

func Summary(hr *HighestBenchmarkReq) HighestBenchmarkSummary {
	var ret HighestBenchmarkSummary
	if data, ok := loadCache(hr); ok && len(data) > 0 {
		var sum float64
		n := len(data)
		for i := 0; i < len(data); i++ {
			sum += data[i].OverallReturn
			//if data[i].Earn > 0 {
			//	ret.EarnCnt++
			//}
			//if data[i].Lose < 0 {
			//	ret.LoseCnt++
			//}
		}
		avg := sum / float64(n)
		var sq float64
		var maxRetract, maxBenchmarkRetract float64
		var idxHigh, idxLow int
		for k, v := range data {
			sq += (v.OverallReturn - avg) * (v.OverallReturn - avg)
			for i := k + 1; i < len(data); i++ {
				dt := v.OverallReturn - data[i].OverallReturn
				if dt > 0 && dt > maxRetract {
					maxRetract = dt
					idxHigh = k
					idxLow = i
				}
				dbt := v.Benchmark - data[i].Benchmark
				if dbt > 0 && dbt > maxBenchmarkRetract {
					maxBenchmarkRetract = dbt
				}
			}
		}
		ret.OverallReturn = data[n-1].OverallReturn
		ret.AnnualizedReturn = ret.OverallReturn / float64(n) * 240
		ret.MaxBenchmarkRetract = maxBenchmarkRetract
		ret.MaxRetract = maxRetract
		ret.Retract = []BenchmarkRetract{
			{
				Date:          data[idxHigh].Date,
				OverallReturn: data[idxHigh].OverallReturn,
			},
			{
				Date:          data[idxLow].Date,
				OverallReturn: data[idxLow].OverallReturn,
			},
		}
		if n > 1 && sq > 0 {
			av := math.Sqrt(250 / float64(n-1) * sq)
			ret.Sharpe = (ret.AnnualizedReturn - 0.04) / av
		}
	}
	if summary, ok := loadDetail(hr); ok {
		ret.Detail = summary.List
		ret.EarnCnt = summary.EarnCnt
		ret.LoseCnt = summary.LoseCnt
		if ret.EarnCnt+ret.LoseCnt > 0 {
			ret.RiseProb = float64(summary.EarnCnt) / float64(summary.EarnCnt+summary.LoseCnt)
		}
	}

	//for i := 0; i < len(ret.Detail); i++ {
	//	if ret.Detail[i].Gain > 0 {
	//		ret.EarnCnt++
	//	}
	//	if ret.Detail[i].Gain < 0 {
	//		ret.LoseCnt++
	//	}
	//}
	//if n := float64(len(ret.Detail)); n == 0 {
	//	ret.RiseProb = 0
	//} else {
	//	ret.RiseProb = float64(ret.EarnCnt) / n
	//}

	return ret
}

func loadCache(hr *HighestBenchmarkReq) ([]HighestBenchmarkResp, bool) {
	var ret []HighestBenchmarkResp
	ih, ok := cache.Load(generateKey(&hr.HighestBenchmarkEx))
	if !ok {
		return ret, false
	}

	ret, ok = ih.([]HighestBenchmarkResp)
	return ret, ok
}

func loadDetail(hr *HighestBenchmarkReq) (BenchmarkDetailSummary, bool) {
	var ret BenchmarkDetailSummary
	ih, ok := cache.Load(generateKey(&hr.HighestBenchmarkEx) + "_detail")
	if !ok {
		return ret, false
	}
	ret, ok = ih.(BenchmarkDetailSummary)
	return ret, ok
}

func calcBenchmark(hr *HighestBenchmarkReq, data []HighestDetailResp) []HighestBenchmarkResp {
	pos := Position{
		Principal:   hr.Principal,
		MaxPosition: hr.MaxPosition,
		Days:        hr.DaysAverage,
		Points:      1 - hr.Points,
		StopRetract: 1 - hr.StopRetract,
		StopLoss:    1 - hr.StopLoss,
		MinPeriod:   hr.MinPeriod,
		PreValue:    hr.Principal,
		TotalValue:  hr.Principal,
		Data:        make([]BenchmarkDetail, 0),
	}
	var ret []HighestBenchmarkResp
	var baseBenchmark float64
	var baseBenchmarkSlice []float64
	models.Data().Table("p_stock_tick").Where("ts_code = ?", "000300.XSHG").Where("trade_date >= ?", hr.TimeBegin.Format(util.YMD)).Order("trade_date").Limit(1).Pluck("close", &baseBenchmarkSlice)
	if len(baseBenchmarkSlice) < 1 || baseBenchmarkSlice[0] == 0 {
		return ret
	}
	baseBenchmark = baseBenchmarkSlice[0]
	for t := hr.TimeBegin; t.Format(util.YMD) != hr.TimeEnd.Add(24*time.Hour).Format(util.YMD); t = t.Add(time.Duration(1) * 24 * time.Hour) {
		ts := t.Format(util.YMD)
		if !inTradeDate(ts) {
			continue
		}
		var raw []Raw
		for i := 0; i < len(pos.Data); {
			if pos.Data[i].unavailable {
				pos.Data = append(pos.Data[:i], pos.Data[i+1:]...)
				continue
			}
			i++
		}

		// sell
		for i := 0; i < len(pos.Data); i++ {
			pos.Data[i].DayLine = dayLine(pos.Data[i].Code, ts, pos.Days)
			if pos.Data[i].DayLine.Close == 0 {
				continue
			}
			pos.Data[i].setPrice(pos.Data[i].DayLine.Close)
			// debug
			if pos.Data[i].Code == "300373" {
				fmt.Println(pos.Data[i].DayLine)
				if ts == "2020-07-06" {
					fmt.Println(ts)
				}
			}
			var soldPrice float64 = 0
			if pos.Data[i].DayLine.Low <= pos.Data[i].DayLine.Avg*(4*pos.Points/(5-pos.Points)) {
				soldPrice = pos.Data[i].DayLine.Avg * (4 * pos.Points / (5 - pos.Points))
			}
			if pos.Data[i].HighestClose > 0 && pos.StopRetract <= 0.9999 {
				sp := pos.Data[i].HighestClose * pos.StopRetract
				if pos.Data[i].DayLine.Low <= sp && sp > soldPrice {
					soldPrice = sp
				}
			}
			if pos.StopLoss <= 0.9999 {
				sp := pos.Data[i].BuyPrice * pos.StopLoss
				if pos.Data[i].DayLine.Low <= sp && sp > soldPrice {
					soldPrice = sp
				}
			}
			if soldPrice > 0 {
				r := Raw{
					Code:      pos.Data[i].Code,
					Date:      ts,
					Price:     soldPrice,
					TradeTime: time.Time{},
					Sell:      true,
				}
				if r.TradeTime, r.Price = queryTime(&r); r.TradeTime.IsZero() {
					continue
				}
				raw = append(raw, r)
				pos.Data[i].setPrice(r.Price)
			}

		}
		// buy
		for _, w := range data {
			if strings.HasPrefix(w.TimeBegin, ts) {
				//if w.TimeBegin == t.Format() {
				r := Raw{
					Code:      w.Code,
					Date:      ts,
					Price:     w.PriceBegin,
					TradeTime: time.Time{},
					Sell:      false,
				}
				if r.TradeTime, r.Price = queryTime(&r); r.TradeTime.IsZero() {
					continue
				}
				raw = append(raw, r)
			}
		}
		// combine
		pos.calcDay(raw)

		// statistics
		var value float64
		//var position int
		for i := 0; i < len(pos.Data); i++ {
			if pos.Data[i].unavailable {
				continue
			}
			if pos.Data[i].DayLine.Close == 0 {
				value += pos.Data[i].Value
			} else {
				pos.Data[i].Value = pos.Data[i].CurPrice * float64(pos.Data[i].Position) * 100
				value += pos.Data[i].Value
			}
			if pos.Data[i].BuyDate != ts && pos.Data[i].Operation != "卖出" {
				pos.Data[i].Operation = "持有"
			}
			//position++
		}

		var benchmark []float64
		models.Data().Table("p_stock_tick").Where("ts_code = ?", "000300.XSHG").Where("trade_date = ?", ts).Pluck("close", &benchmark)
		pos.TotalValue = value + pos.Principal
		change := pos.TotalValue - hr.Principal
		preChange := pos.TotalValue - pos.PreValue
		pos.PreValue = pos.TotalValue
		cRet := HighestBenchmarkResp{
			ID:            len(ret),
			Date:          ts,
			OverallReturn: change / hr.Principal,
			Earn:          math.Max(0, preChange),
			Lose:          math.Min(0, preChange),
			Position:      len(pos.Data),
		}
		if len(benchmark) > 0 {
			cRet.Benchmark = (benchmark[0] - baseBenchmark) / baseBenchmark
		}
		cRet.Detail = append(cRet.Detail, pos.Data...)
		ret = append(ret, cRet)
		cache.Store(generateKey(&hr.HighestBenchmarkEx), ret)
	}
	cache.Store(generateKey(&hr.HighestBenchmarkEx)+"_detail", pos.Summary)

	return ret
}

func (p *Position) calcDay(raw []Raw) bool {
	if raw == nil || len(raw) == 0 {
		return false
	}
	//for i := 0; i < len(raw); i++ {
	//	if raw[i].TradeTime.IsZero() {
	//		raw[i].TradeTime = queryTime(&raw[i])
	//	}
	//}
	sort.Slice(raw, func(i, j int) bool {
		if raw[i].TradeTime.IsZero() {
			return false
		}
		if raw[j].TradeTime.IsZero() {
			return true
		}
		return raw[i].TradeTime.Before(raw[j].TradeTime)
	})
	for i := 0; i < len(raw); i++ {
		if raw[i].Sell {
			if p.checkSell(raw[i]) && p.exist(raw[i]) {
				// sell
				p.sell(raw[i])
				return p.calcDay(raw[i+1:])
			}
		} else {
			if p.checkBuy(raw[i]) && !p.exist(raw[i]) {
				// buy
				p.buy(raw[i])
				return p.calcDay(raw[i+1:])
			}
		}
		//if !raw[i].Sell && p.checkBuy(raw[i]) && !p.exist(raw[i]) {
		//	// buy
		//	p.buy(raw[i])
		//	return p.calcDay(raw[i+1:])
		//} else if raw[i].Sell && p.checkSell(raw[i]) && p.exist(raw[i]) {
		//	// sell
		//	p.sell(raw[i])
		//	return p.calcDay(raw[i+1:])
		//}
	}

	return true
}

func queryTime(raw *Raw) (time.Time, float64) {
	var ret []struct {
		Time time.Time
		High float64
		Low  float64
	}
	var endTime, cond string
	//var bAmend bool
	if raw.Sell {
		endTime = raw.Date + " 15:30:00"
		cond = "low <= ?"
	} else {
		endTime = raw.Date + " 14:29:59"
		cond = "high >= ?"
	}

	models.Data().Select("time, high, low").Table("p_stock_minute").
		Where("code = ?", raw.Code).
		Where("time > ?", raw.Date).
		Where("time <= ?", endTime).
		Where(cond, raw.Price).
		Order("time ASC").Find(&ret)

	//if len(ret) == 0 {
	//	bAmend = true
	//	if raw.Sell {
	//		models.Data().Table("p_stock_minute").
	//			Where("code = ?", raw.Code).
	//			Where("time > ?", raw.Date).
	//			Where("time <= ?", endTime).
	//			Order("time ASC").Find(&ret)
	//	}
	//}
	if len(ret) > 0 {
		if raw.Sell {
			return ret[0].Time, math.Min(ret[0].High, raw.Price)
		} else {
			return ret[0].Time, math.Max(ret[0].Low, raw.Price)
		}
	} else {
		logging.Error("benchmark.queryTime() not found: ", raw.Code, raw.Date, raw.Price, raw.Sell)
		return time.Time{}, raw.Price
	}
}

func (p *Position) buy(raw Raw) {
	positionLeft := float64(p.MaxPosition - len(p.Data))
	v1 := p.PreValue / float64(p.MaxPosition) / (raw.Price * 100)
	v2 := p.Principal / positionLeft / (raw.Price * 100)
	amount := int(math.Min(v1, v2))
	bd := BenchmarkDetail{
		Code:      raw.Code,
		Position:  amount,
		BuyPrice:  raw.Price,
		CurPrice:  raw.Price,
		BuyDate:   raw.Date,
		Value:     float64(amount) * 100 * raw.Price,
		Change:    0,
		Operation: "买入",
		TradeTime: raw.TradeTime,
		DayLine:   dayLine(raw.Code, raw.Date, 0),
	}
	p.Data = append(p.Data, bd)

	bdl := BenchmarkDetailList{
		Date:      raw.TradeTime.Format(util.YMDHMS),
		Code:      raw.Code,
		Operation: "买入",
		Price:     raw.Price,
		Vol:       amount,
		Amount:    raw.Price * float64(amount) * 100,
		Gain:      0,
	}
	p.Summary.List = append(p.Summary.List, bdl)
	p.Principal -= bd.Value
}

func (p *Position) sell(raw Raw) bool {
	for i := 0; i < len(p.Data); i++ {
		if p.Data[i].Code == raw.Code {
			// sell
			p.Principal += raw.Price * 100 * float64(p.Data[i].Position)
			p.Data[i].Operation = "卖出"
			bdl := BenchmarkDetailList{
				Date:      raw.TradeTime.Format(util.YMDHMS),
				Code:      raw.Code,
				Operation: "卖出",
				Price:     raw.Price,
				Vol:       p.Data[i].Position,
				Amount:    raw.Price * float64(p.Data[i].Position) * 100,
				Gain:      (raw.Price - p.Data[i].BuyPrice) * float64(100*p.Data[i].Position),
			}
			if bdl.Gain > 0 {
				p.Summary.EarnCnt++
			} else {
				p.Summary.LoseCnt++
			}
			p.Summary.List = append(p.Summary.List, bdl)
			p.Data[i].unavailable = true
			//p.Data = append(p.Data[:i], p.Data[i+1:]...)
			return true
		}
	}
	return false
}

func (p *Position) checkBuy(raw Raw) bool {
	// 可以买入
	positionLeft := float64(p.MaxPosition - len(p.Data))
	if positionLeft > 0 && p.Principal/positionLeft >= raw.Price*100 && raw.Price > 0 && !raw.TradeTime.IsZero() {
		return true
	}
	return false
}

func (p *Position) checkSell(raw Raw) bool {
	// 可以卖出
	for _, v := range p.Data {
		if v.Code == raw.Code {
			if v.BuyDate != raw.Date {
				return true
			} else {
				return false
			}
		}
	}
	return false
}

func (p *Position) exist(raw Raw) bool {
	// 是否已在持仓中
	for _, v := range p.Data {
		if v.Code == raw.Code {
			return true
		}
	}
	return false
}

func (b *BenchmarkDetail) setPrice(price float64) {
	b.CurPrice = price
	if b.BuyPrice > 0 {
		b.Change = (b.DayLine.Close - b.BuyPrice) / b.BuyPrice
		b.Gain = (b.DayLine.Close - b.BuyPrice) * float64(b.Position) * 100
	}
	if b.DayLine.PreClose > b.HighestClose {
		b.HighestClose = b.DayLine.PreClose
	}
}

func dayLine(code, date string, days int) DayLineAvg {
	var ret DayLineAvg
	if days < 2 {
		days = 2
	}
	models.Data().Table("p_stock_tick").
		Select("AVG(p_stock_tick.close) AS avg, B.*").
		Joins("LEFT JOIN p_stock_tick B ON B.stock_code = p_stock_tick.stock_code AND B.trade_date = ?", date).
		Joins("LEFT JOIN b_trade_date C ON C.trade_date = B.trade_date").
		Joins(fmt.Sprintf("LEFT JOIN b_trade_date D ON D.id = C.id - %d", days)).
		Where("p_stock_tick.stock_code = ?", code).
		Where("p_stock_tick.trade_date > D.trade_date").
		Where("p_stock_tick.trade_date < B.trade_date").
		First(&ret)
	if days < 2 {
		ret.Avg = 0
	}
	return ret
}

func inTradeDate(date string) bool {
	var cnt int
	models.Data().Table("b_trade_date").Where("trade_date = ?", date).Count(&cnt)
	return cnt > 0
}

func generateKey(hr interface{}) string {
	b, _ := json.Marshal(hr)
	hash := fmt.Sprintf("%d", util.BKDRHash(string(b)))
	return hash
}
