package quantization

import (
	"datacenter/models"
	"datacenter/models/message"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/pkg/websocket"
	"encoding/json"
	"fmt"
	"math"
	"sync"
	"time"
)

type Technical struct {
	Code  string
	Date  string
	EMA12 float64 `gorm:"ema12"`
	EMA26 float64 `gorm:"ema26"`
	DEA   float64 `gorm:"dea"`
	DIF   float64 `gorm:"dif"`
	MACD  float64 `gorm:"macd"`
	MA    float64 `gorm:"ma"`
	MD    float64 `gorm:"md"`
	MB    float64 `gorm:"mb"`
	UP    float64 `gorm:"up"`
	DN    float64 `gorm:"dn"`
	K     float64 `gorm:"k"`
	D     float64 `gorm:"d"`
	J     float64 `gorm:"j"`
	RSI6  float64 `gorm:"rsi6"`
	RSI12 float64 `gorm:"rsi12"`
	RSI24 float64 `gorm:"rsi24"`
}

type TechnicalMinute struct {
	Code  string
	Time  time.Time `gorm:"time"`
	EMA12 float64   `gorm:"ema12"`
	EMA26 float64   `gorm:"ema26"`
	DEA   float64   `gorm:"dea"`
	DIF   float64   `gorm:"dif"`
	MACD  float64   `gorm:"macd"`
	MA    float64   `gorm:"ma"`
	MD    float64   `gorm:"md"`
	MB    float64   `gorm:"mb"`
	UP    float64   `gorm:"up"`
	DN    float64   `gorm:"dn"`
	K     float64   `gorm:"k"`
	D     float64   `gorm:"d"`
	J     float64   `gorm:"j"`
	RSI6  float64   `gorm:"rsi6"`
	RSI12 float64   `gorm:"rsi12"`
	RSI24 float64   `gorm:"rsi24"`
}

func (TechnicalMinute) TableName() string {
	return "p_stock_technical_index_minute"
}

func (Technical) TableName() string {
	return "p_stock_technical_index"
}

func MACD(code, date string) {
	var data []DayLineEx
	var ema12, ema26 float64
	var dif, dea, macd float64
	if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", code).Where("trade_date > ?", date).Order("trade_date ASC").Find(&data).Error; err != nil {
		logging.Error("quantization.EMA() Errors: ", err.Error())
		return
	}
	db := models.Data().Table("p_stock_technical_index")
	ema12 = data[0].Close
	ema26 = data[0].Close
	for i := 1; i < len(data); i++ {
		ema12 = ema12*11/13 + data[i].Close*2/13
		ema26 = ema26*25/27 + data[i].Close*2/27
		dif = ema12 - ema26
		dea = dea*8/10 + dif*2/10
		macd = (dif - dea) * 2
		if err := db.Where(&Technical{
			Code: data[i].StockCode,
			Date: data[i].TradeDate,
		}).Assign(&Technical{
			EMA12: ema12,
			EMA26: ema26,
			DIF:   dif,
			DEA:   dea,
			MACD:  macd,
		}).FirstOrCreate(&Technical{}).Error; err != nil {
			logging.Error("quantization.EMA() assign Errors: ", err.Error())
		}
	}
}

func BOLL(code, date string) {
	var data []DayLineEx
	var ma, md, mb, up, dn float64
	var N int = 20
	if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", code).Where("trade_date > ?", date).Order("trade_date ASC").Find(&data).Error; err != nil {
		logging.Error("quantization.BOLL() Errors: ", err.Error())
		return
	}
	db := models.Data().Table("p_stock_technical_index")
	for i := 0; i < len(data); i++ {
		mb = ma
		ma = calcAvg(data[max(0, i-N+1) : i+1])
		md = calcMD(data[max(0, i-N+1) : i+1])
		up = ma + 2*md
		dn = ma - 2*md
		if err := db.Where(&Technical{
			Code: data[i].StockCode,
			Date: data[i].TradeDate,
		}).Assign(&Technical{
			MA: ma,
			MB: mb,
			MD: md,
			UP: up,
			DN: dn,
		}).FirstOrCreate(&Technical{}).Error; err != nil {
			logging.Error("quantization.BOLL() assign Errors: ", err.Error())
		}
	}
}

func KDJ(code, date string) {
	var data []DayLineEx
	var rsv, j float64
	var k float64 = 50
	var d float64 = 50
	var N int = 9
	if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", code).Where("trade_date > ?", date).Order("trade_date ASC").Find(&data).Error; err != nil {
		logging.Error("quantization.BOLL() Errors: ", err.Error())
		return
	}
	//if date != "0" {
	//	var t Technical
	//	models.Data().Table("p_stock_technical_index").Where("code = ?", code).Where("date = ?", date).Find(&t)
	//	k = t.K
	//	d = t.D
	//}
	db := models.Data().Table("p_stock_technical_index")
	for i := 0; i < len(data); i++ {
		rsv = (data[i].Close - minLow(data[max(0, i-N+1):i+1])) / (maxHigh(data[max(0, i-N+1):i+1]) - minLow(data[max(0, i-N+1):i+1])) * 100
		k = k*2/3 + rsv*1/3
		d = d*2/3 + k*1/3
		j = 3*k - 2*d
		if err := db.Where(&Technical{
			Code: data[i].StockCode,
			Date: data[i].TradeDate,
		}).Assign(&Technical{
			K: k,
			D: d,
			J: j,
		}).FirstOrCreate(&Technical{}).Error; err != nil {
			logging.Error("quantization.KDJ() assign Errors: ", err.Error())
		}
	}
}

func RSI(code, date string) {
	var data []DayLineEx
	if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", code).Where("trade_date > ?", date).Order("trade_date ASC").Find(&data).Error; err != nil {
		logging.Error("quantization.BOLL() Errors: ", err.Error())
		return
	}
	db := models.Data().Table("p_stock_technical_index")
	up := make([]float64, 25)
	dn := make([]float64, 25)
	rsi := make([]float64, 25)
	for i := 0; i < len(data); i++ {
		for k := 1; k < 25; k++ {
			up[k] = calcSMA(math.Max(data[i].Close-data[i].PreClose, 0), float64(k), 1, up[k])
			dn[k] = calcSMA(math.Abs(data[i].Close-data[i].PreClose), float64(k), 1, dn[k])
			rsi[k] = up[k] / dn[k] * 100
		}
		if err := db.Where(&Technical{
			Code: data[i].StockCode,
			Date: data[i].TradeDate,
		}).Assign(&Technical{
			RSI6:  rsi[6],
			RSI12: rsi[12],
			RSI24: rsi[24],
		}).FirstOrCreate(&Technical{}).Error; err != nil {
			logging.Error("quantization.RSI() assign Errors: ", err.Error())
		}
	}
}

//func KDJMinute(code string) {
//	var data []MinuteLine
//	var rsv, j float64
//	var k float64 = 50
//	var d float64 = 50
//	var N int = 9
//	if err := models.Data().Table("p_stock_minute_month").Where("code = ?", code).Order("time ASC").Find(&data).Error; err != nil {
//		logging.Error("quantization.KDJMinute() Errors: ", err.Error())
//		return
//	}
//
//	db := models.Data().Table("p_stock_technical_index_minute")
//	for i := 0; i < len(data); i += 5 {
//		rsv = (data[i].Close - minLowMinute(data[max(0, i-N+1):i+1])) / (maxHighMinute(data[max(0, i-N+1):i+1]) - minLowMinute(data[max(0, i-N+1):i+1])) * 100
//		k = k*2/3 + rsv*1/3
//		d = d*2/3 + k*1/3
//		j = 3*k - 2*d
//		if err := db.Where(&TechnicalMinute{
//			Code: data[i].Code,
//			Time: data[i].Time,
//		}).Assign(&TechnicalMinute{
//			K: k,
//			D: d,
//			J: j,
//		}).FirstOrCreate(&TechnicalMinute{}).Error; err != nil {
//			logging.Error("quantization.KDJMinute() assign Errors: ", err.Error())
//		}
//	}
//}

//var KDJCache sync.Map            //string, []TechnicalMinute
//var RSICache sync.Map            //string, []TechnicalMinute
var MinuteLineCache sync.Map     //string, []MinuteLine
var FiveMinuteLineCache sync.Map //string, []MinuteLine
var RealTimeCache sync.Map       //string, MinuteLine
var CodesCache sync.Map

func KDJMinute(code string) {
	var data []MinuteLine
	var rsv, j float64
	var k float64 = 50
	var d float64 = 50
	var N int = 9

	data = FetchFiveMinute(code)
	if len(data) == 0 {
		return
	}

	for i := 0; i < len(data); i++ {
		cv := maxHighMinute(data[max(0, i-N+1):i+1]) - minLowMinute(data[max(0, i-N+1):i+1])
		if cv == 0 {
			rsv = 0
		} else {
			rsv = (data[i].Close - minLowMinute(data[max(0, i-N+1):i+1])) / cv * 100
		}
		k = k*2/3 + rsv*1/3
		d = d*2/3 + k*1/3
		j = 3*k - 2*d
	}
	t, _ := time.ParseInLocation("2006-01-02 15:04:05", data[len(data)-1].Time.Format("2006-01-02 15:04")+":00", time.Local)
	if err := models.Data().Table("p_stock_technical_index_minute").Where(&TechnicalMinute{
		Code: data[0].Code,
		Time: t,
	}).Assign(&TechnicalMinute{
		K: k,
		D: d,
		J: j,
	}).FirstOrCreate(&TechnicalMinute{}).Error; err != nil {
		logging.Error("quantization.KDJRealTime() assign Errors: ", err.Error())
	}

	//conn := gredis.Clone(setting.RedisSetting.StockDB)
	//if j != 0 && j < 20 {
	//	//超卖
	//	conn.HSet("stock:supercharge:sell", code, j)
	//	conn.HSet("stock:supercharge:type", code, "kdj")
	//}
	//if j > 80 {
	//	//超买
	//	conn.HSet("stock:supercharge:buy", code, j)
	//	conn.HSet("stock:supercharge:type", code, "kdj")
	//}
}

func RSIMinute(code string) {
	var data []MinuteLine

	data = FetchFiveMinute(code)
	if len(data) == 0 {
		return
	}

	db := models.DB().Table("p_stock_technical_index_minute")
	up := make([]float64, 25)
	dn := make([]float64, 25)
	rsi := make([]float64, 25)
	for i := 1; i < len(data); i++ {
		for k := 1; k < 25; k++ {
			up[k] = calcSMA(math.Max(data[i].Close-data[i-1].Close, 0), float64(k), 1, up[k])
			dn[k] = calcSMA(math.Abs(data[i].Close-data[i-1].Close), float64(k), 1, dn[k])
			rsi[k] = up[k] / dn[k] * 100
		}
	}

	t, _ := time.ParseInLocation("2006-01-02 15:04:05", data[len(data)-1].Time.Format("2006-01-02 15:04")+":00", time.Local)
	if err := db.Where(&TechnicalMinute{
		Code: data[0].Code,
		Time: t,
	}).Assign(&TechnicalMinute{
		RSI6:  rsi[6],
		RSI12: rsi[12],
		RSI24: rsi[24],
	}).FirstOrCreate(&TechnicalMinute{}).Error; err != nil {
		logging.Error("quantization.RSIMinute() assign Errors: ", data[0].Code, t, err.Error())
	}

	conn := gredis.Clone(setting.RedisSetting.StockDB)
	if rsi[6] != 0 && rsi[6] < 20 {
		//超卖
		conn.HDel("stock:supercharge:buy", code)
		conn.HSet("stock:supercharge:sell", code, rsi[6])
		conn.HSet("stock:supercharge:type", code, "rsi6")
	} else if rsi[6] > 80 {
		//超买
		conn.HDel("stock:supercharge:sell", code)
		conn.HSet("stock:supercharge:buy", code, rsi[6])
		conn.HSet("stock:supercharge:type", code, "rsi6")
	} else {
		conn.HDel("stock:supercharge:sell", code)
		conn.HDel("stock:supercharge:buy", code)
	}
}

//启动执行
func FetchFiveMinute(code string) []MinuteLine {
	var ret []MinuteLine
	if v, ok := FiveMinuteLineCache.Load(code); !ok {
		raw := FetchMinuteLine(code)
		var c MinuteLine
		for i, k := 0, 0; i < len(raw); i++ {
			var bNext bool
			if !((raw[i].Time.Hour() == 14 && raw[i].Time.Minute() == 59) || (raw[i].Time.Hour() == 11 && raw[i].Time.Minute() == 29)) {
				bNext = true
			}

			if c.Open == 0 && raw[i].Open > 0 {
				c.Open = raw[i].Open
			}
			if raw[i].Close > 0 {
				c.Close = raw[i].Close
			}
			if raw[i].High > c.High {
				c.High = raw[i].High
			}
			if (raw[i].Low < c.Low || c.Low == 0) && raw[i].Low > 0 {
				c.Low = raw[i].Low
			}

			if k%5 == 0 {
				c.Time = raw[i].Time
				c.Code = raw[i].Code
			}
			if k%5 == 4 && bNext {
				ret = append(ret, c)
				c = MinuteLine{}
				c.Low = 0
			}
			if bNext {
				k++
			}
		}
		FiveMinuteLineCache.Store(code, ret)
	} else {
		ret = v.([]MinuteLine)
	}
	return ret
}

func FetchMinuteLine(code string) []MinuteLine {
	var ret []MinuteLine
	if v, ok := MinuteLineCache.Load(code); !ok {
		if err := models.Data().Table("p_stock_minute_month").Where("code = ?", code).Where("time >= ?", time.Now().Add(-30*24*time.Hour).Format(util.YMD)).Order("time ASC").Find(&ret).Error; err != nil {
			logging.Error("quantization.FetchMinuteLine() Errors: ", err.Error())
		}
		if len(ret) > 0 {
			MinuteLineCache.Store(code, ret)
			CodesCache.Store(code, 1)
		}
	} else {
		ret = v.([]MinuteLine)
	}
	return ret
}

//接实时行情
func RealTimeStock(code string, price float64) {
	t := time.Now()
	if t.Hour() == 9 && t.Minute() < 30 {
		return
	}
	if v, ok := RealTimeCache.Load(code); !ok {
		RealTimeCache.Store(code, MinuteLine{
			Code:  code,
			Time:  time.Now(),
			Open:  price,
			Close: price,
			High:  price,
			Low:   price,
		})
	} else {
		ml := v.(MinuteLine)
		if price > ml.High {
			ml.High = price
		}
		if price < ml.Low {
			ml.Low = price
		}
		ml.Close = price
		RealTimeCache.Store(code, ml)
	}
}

// 5分钟执行
func RefreshFiveMinuteLine() {
	RealTimeCache.Range(func(k, v interface{}) bool {
		ml := v.(MinuteLine)
		fml := FetchFiveMinute(k.(string))
		fml = append(fml, ml)
		FiveMinuteLineCache.Store(k, fml)
		RealTimeCache.Delete(k)
		return true
	})
}

func (m MinuteLine) Store() {
	if err := models.Data().Table("p_stock_minute_five").Where(&MinuteLine{
		Code: m.Code,
		Time: m.Time,
	}).Assign(&m).FirstOrCreate(&Technical{}).Error; err != nil {
		//logging.Error("quantization.Store() assign Errors: ", err.Error())
	}
}

// 超买超卖
func SuperCharge() {
	t := time.Now()
	if t.Hour() == 9 && t.Minute() < 30 {
		return
	}
	RefreshFiveMinuteLine()
	CodesCache.Range(func(k, v interface{}) bool {
		code := k.(string)
		RSIMinute(code)
		KDJMinute(code)
		return true
	})
	//codes := stockCodes()
	////codes = []string{"000027"}
	//for _, v := range codes {
	//	RSIMinute(v)
	//	KDJMinute(v)
	//}
}

//
//func MinuteStock() {
//	conn := gredis.Clone(setting.RedisSetting.StockDB)
//	conn.HGetAllStringMap()
//}

func FetchAll() {
	codes := stockCodes()
	//codes = []string{"000027"}
	for _, v := range codes {
		FetchFiveMinute(v)
	}
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func calcAvg(data []DayLineEx) float64 {
	var ret float64
	for _, v := range data {
		ret += v.Close
	}
	return ret / float64(len(data))
}

func calcMD(data []DayLineEx) float64 {
	var sum float64
	ma := calcAvg(data)
	for _, v := range data {
		sum += (v.Close - ma) * (v.Close - ma)
	}
	return math.Sqrt(sum / float64(len(data)-1))
}

func calcSMA(x, n, m, y float64) float64 {
	return (m*x + (n-m)*y) / n
}

func push(code, cType string) {
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	name, _ := conn.HGetString("stock:name", code)
	if name == "" {
		return
	}
	m := map[string]interface{}{
		"type":    "pool",
		"content": fmt.Sprintf("%s %s", name, cType),
	}
	ws := websocket.GetInstance()
	bt, _ := json.Marshal(m)
	msg := message.ParseMsg(message.Message_Stock, "system", "", string(bt), true)
	ws.SysSend(msg)
}
