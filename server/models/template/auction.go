package template

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"strconv"
	"strings"
	"time"
)

type AuctionResponse struct {
	ID               int        `json:"id" gorm:"column:id"`
	Status           int        `json:"status" gorm:"column:status"`                 // 拍卖状态 0:预告中,1:开拍中,2:已结束/中止
	OfferCount       string     `json:"offer_count" gorm:"column:offer_count"`      // 报名人数
	Platform         string     `json:"platform" gorm:"column:platform"`            // 平台
	Title            string     `json:"title" gorm:"column:title"`                  // 标题
	Link             string     `json:"link" gorm:"link"`                           // 股票代码名称
	Keyword          string     `json:"keyword" gorm:"keyword"`
	ShareRatio       decimal.NullDecimal     `json:"share_ratio" gorm:"share_ratio"`  //占股比例
	UpdatedAt        time.Time  `json:"updated_at" gorm:"column:spider_time"`        // 更新时间
	SpiderTime       time.Time  `json:"spider_time" gorm:"column:created_at"`      // 抓取时间
	Start            time.Time  `json:"-" gorm:"column:start_time"`                // 开始时间
	StartTime        string     `json:"start_time"`                                 // 开始时间
	DealStatus       int        `json:"deal_status"`                                // 处理状态 0为未读 1为已读
}
func(AuctionResponse) TableName() string {
	return "u_auction"
}
func GetAuctionLists(data map[string]interface{}) ([]*AuctionResponse, error) {
	var auction []*AuctionResponse
	userID := ""

	dbs := models.DB().Table("u_auction")
	if !data["start"].(time.Time).IsZero(){
		dbs =  dbs.Where("start_time >= ?",data["start"].(time.Time).Format("2006-01-02")+" 00:00:00")
	}
	if !data["end"].(time.Time).IsZero() {
		dbs =  dbs.Where("start_time < ?",data["end"].(time.Time).Format("2006-01-02")+" 23:59:59")
	}
	if data["title"] != "" {
		dbs =  dbs.Where("title like ?","%"+ data["title"].(string)+"%")
	}
	if data["platform"] != "" {
		dbs =  dbs.Where("platform like ?","%"+ data["platform"].(string)+"%")
	}
	if data["max_id"].(int) > 0 {
		dbs =  dbs.Where("id > ?",data["max_id"].(int))
	}
	if data["status"].(int) != 3 {
		dbs =  dbs.Where("status = ?",data["status"].(int))
	}
	if data["user_id"] != "" {
		userID = data["user_id"].(string)
	}
	pageNum := data["page_num"].(int)
	pageSize := data["page_size"].(int)
	err := dbs.Offset((pageNum - 1) * pageSize).Limit(pageSize).Order("updated_at desc").Find(&auction).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	for index,_ := range auction {
		auction[index].StartTime = auction[index].Start.Format(util.YMDHMS)
		if auction[index].Start.IsZero() {
			auction[index].StartTime = ""
		}
		auction[index].Title = strings.Replace(auction[index].Title,auction[index].Keyword,`<span style="color:red">`+auction[index].Keyword+"</span>",-1)
		stockName := GetKeyword(auction[index].Keyword)
		if stockName != "" {
			auction[index].Title = strings.Replace(auction[index].Title,stockName,`<span style="color:red">`+stockName+"</span>",-1)
		}
		if userID != "" {
			auction[index].DealStatus = GetAuctionDealStatus(auction[index].ID,userID)
		}
	}
	return auction, nil
}

func GetAuctionTotal(data map[string]interface{}) (int, error) {
	var count int
	dbs := models.DB().Model(&AuctionResponse{})
	if !data["start"].(time.Time).IsZero(){
		dbs =  dbs.Where("start_time >= ?",data["start"].(time.Time).Format("2006-01-02")+" 00:00:00")
	}
	if !data["end"].(time.Time).IsZero() {
		dbs =  dbs.Where("start_time < ?",data["end"].(time.Time).Format("2006-01-02")+" 23:59:59")
	}
	if data["title"] != "" {
		dbs =  dbs.Where("title like ?","%"+ data["title"].(string)+"%")
	}
	if data["platform"].(string) != "" {
		dbs =  dbs.Where("platform like ?","%"+ data["platform"].(string)+"%")
	}
	if data["status"].(int) != 3 {
		dbs =  dbs.Where("status = ?",data["status"].(int))
	}
	err := dbs.Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}
func GetAuctionDealStatus(objectID int,userID string) int {
    var count int
    err := models.DB().Table("u_common_event").Where("object_type = ? ","auction").
    	Where("object_id = ? ",objectID).Where("user_id = ? ",userID).Count(&count).Error
    if err != nil {
    	logging.Info("auction.GetAuctionDealStatus err is",err)
    	return 0
	}
	if count > 0 {
		return 1
	}
	return 0
}

func GetKeyword(keyword string) (newKeyword string) {
	ret,err := strconv.Atoi(keyword)
	if err != nil {
		return ""
	}
	var Stock struct{
		Name  string `json:"name"`
	}
	stock := Stock
	errRet := models.DB().Table("b_stock").Where("id = ? ",ret).First(&stock).Error
	if errRet != nil {
		logging.Info("auction get stock_name fail err:",err)
		return ""
	}
	return stock.Name
}
