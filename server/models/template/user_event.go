package template

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"strconv"
	"strings"
	"time"
)

const EVENT_STATUS_WAIT = 0                          // 待处理
const EVENT_STATUS_DEAL = 1                          // 已处理
const EVENT_OBJECT_TYPE_ANNOUCEMENT = "announcement" // 对象类型为 annoucement 公告
const EVENT_OBJECT_TYPE_RESEARCH = "research"        // 对象类型为 research 研报
type UserEvent struct {
	models.Simple
	Id          int    `json:"id" gorm:"cloumn:id"`                     // 主键ID
	UserID      int    `json:"user_id" gorm:"cloumn:user_id"`           // 用户ID
	ObjectID    int    `json:"object_id" gorm:"cloumn:object_id"`       // 事件ID
	EventStatus int    `json:"event_status" gorm:"cloumn:event_status"` // 事件状态
	EventStr    string `json:"event_str" gorm:"cloumn:event_str"`       // 点评内容
	ObjectType  string `json:"object_type" gorm:"cloumn:object_type"`   // 对象类型
	EventType   string `json:"event_type" gorm:"cloumn:event_type"`     // 事件类型
	PictureLink string `json:"picture_link" gorm:"cloumn:picture_link"` // 图片链接
}
type IndustryName struct {
	IndustryName string `json:"industry_name" gorm:"cloumn:industry_name"`
}
type IndustryNameAdd struct {
	UserId         int
	UserName       string
	FirstIndustry  string
	SecondIndustry string
}
type Codes struct {
	Code string `json:"code" gorm:"cloumn:code"` // 点评内容
}
type ObjectIds struct {
	ID int `json:"id" gorm:"cloumn:id"` // 点评内容
}
type Star struct {
	Star float64 `json:"star" gorm:"cloumn:star"`
}
type HistroyEvent struct {
	Id             int      `json:"id" gorm:"cloumn:id"`               // 主键ID
	UserID         int      `json:"user_id" gorm:"cloumn:user_id"`     // 用户ID
	EventStr       string   `json:"event_str" gorm:"cloumn:event_str"` // 点评内容
	PictureLink    string   `json:"-" gorm:"cloumn:picture_link"`      // 图片链接
	PictureLinkArr []ImgDTO `json:"picture_link"`                      // 图片链接
	UpdatedAt      string   `json:"updated_at" gorm:"updated_at"`      // 处理时间\
}

// 公告和 研报详情
type StaticsticDetail struct {
	UserID      int      `json:"user_id"`
	CreatedAt   string   `json:"news_time"`
	UpdatedAt   string   `json:"opt_time"`
	Code        string   `json:"code"`
	CodeName    string   `json:"code_name"`
	Title       string   `json:"title"`
	EventStr    string   `json:"event_str"`
	PictureLink []ImgDTO `json:"picture_link"`
}
type Detail struct {
	Name      string    `json:"names" gorm:"name"`
	Title     string    `json:"title" gorm:"title"`
	Code      string    `json:"code" gorm:"code"`
	CreatedAt time.Time `json:"created_at" gorm:"spider_time"`
}

func (UserEvent) TableName() string {
	return "u_user_event"
}

// param eventType 事件类型 eventStatus 事件状态 fromDay 统计时间
func GetUserEventCount(fromDay, tableName, objectType, endDay string, industryNames, secondIndustry []string, userID int) (int, int) {
	var (
		codes      []string
		deal, wait int
		events     []int
	)
	ids := []ObjectIds{}
	dbs := models.DB().Table(tableName).
		Where("created_at > ? ", fromDay)
	codes = GetStockByIndustryNames(secondIndustry)
	if len(codes) < 1 {
		return 0, 0
	}
	if tableName == "u_research_report" {
		dbs = dbs.Where("parent = ? ", 0)
		// 获取行业查询条件
		industryWhere := GetUserFirstIndustry(userID)
		if industryWhere != "" {
			// or (industry in ( ? ) and code = '')
			tmp := fmt.Sprintf("(code in ( %v ) or (code = '' and %v ))",strings.Join(codes,","),industryWhere)
			dbs = dbs.Where(tmp)
		} else {
			dbs = dbs.Where("code in ( ? ) ", codes)
		}
	}
	if tableName == "u_cninfo_notice" || tableName == "p_investigation" {
		dbs = dbs.Where("code in ( ? ) ", codes)
	}
	if endDay != "" {
		dbs = dbs.Where("created_at <= ? ", endDay)
	}
	err := dbs.Find(&ids).Error
	if err != nil {
		logging.Info("userEvent.GetUserIndustry err:", err.Error)
		return 0, 0
	}
	for _, value := range ids {
		if value.ID > 0 {
			events = append(events, value.ID)
		}
	}
	eventIds := []ObjectIds{}
	_ = models.DB().Table("u_user_event").
		Where("event_status = ? ", 1).
		Where("user_id = ? ", userID).
		Where("object_type = ? ", objectType).
		Where("object_id in (?)", events).Find(&eventIds).Error
	deal = len(eventIds)
	wait = len(events) - deal
	return deal, wait
}

func GetUserIndustry(userID int) []string {
	names := []string{}
	industryNames := []IndustryName{}
	err := models.DB().Table("u_second_industry_user").
		Select("distinct first_industry as industry_name").
		Where("user_id = ? ", userID).Where("deleted_at is null ").Find(&industryNames).Error
	if err != nil {
		logging.Info("userEvent.GetUserIndustry err:", err.Error)
		return nil
	}
	for _, value := range industryNames {
		if value.IndustryName != "" {
			names = append(names, value.IndustryName)
		}
	}
	return names
}
func GetUserSecondIndustry(userID int) []string {
	industryNames := []IndustryName{}
	names := []string{}
	err := models.DB().Table("u_second_industry_user").
		Select("distinct second_industry as industry_name").
		Where("user_id = ? ", userID).Where("deleted_at is null ").Find(&industryNames).Error
	if err != nil {
		logging.Info("userEvent.GetUserIndustry err:", err.Error)
		return nil
	}
	for _, value := range industryNames {
		if value.IndustryName != "" {
			names = append(names, value.IndustryName)
		}
	}
	return names
}
func GetStockByIndustryNames(industryNames []string) []string {
	codes := []Codes{}
	names := []string{}
	if len(industryNames) < 1 {
		return names
	}
	err := models.DB().Table("u_stock_industry").
		Select("code").
		Where("u_stock_industry.second_industry in (?) ", industryNames).Find(&codes).Error
	if err != nil {
		logging.Info("userEvent.GetUserIndustry err:", err.Error)
		return names
	}
	for _, value := range codes {
		if value.Code != "" {
			names = append(names, value.Code)
		}
	}
	return names
}

func GetUserStar(userID int, start string) Star {
	var star Star
	err := models.DB().Table("p_stock_share").
		Select("sum(star) as star ").
		Where("created_at > ? ", start).
		Where("deleted_at is null").
		Where("p_stock_share.owner_id = ? ", userID).Scan(&star).Error
	if err != nil {
		logging.Info("userEvent.GetUserIndustry err:", err.Error)
		return Star{}
	}
	return star
}

func AssmblePushMsg(userID int) map[string]interface{} {
	// 组装数据
	data := make(map[string]interface{})
	// 获取所在的行业
	var (
		industryNames  []string
		secondIndustry []string
	)
	industryNames = GetUserIndustry(userID)
	secondIndustry = GetUserSecondIndustry(userID)
	data["industry_name"] = secondIndustry
	// 获取当天已处理的
	nowDay := time.Now().Format("2006-01-02 00:00:00")
	threeDay := time.Now().AddDate(0, 0, -3).Format("2006-01-02 00:00:00")
	monthDay := time.Date(time.Now().Year(), time.Now().Month(), 1, 0, 0, 0, 0, time.Now().Location()).Format("2006-01-02 00:00:00")
	// 获取公告数据
	// 获取3天内的公告数据
	oneCninfo := AnnouncementEventQO{
		UserID:    userID,
		StartTime: nowDay,
	}
	deal, wait, errC := GetMyAnnouncementCount(&oneCninfo)
	if errC != nil {
		logging.Info("get my announcement count err", errC)
	}
	threeCninfo := AnnouncementEventQO{
		UserID:    userID,
		StartTime: threeDay,
	}
	_, threeWait, errT := GetMyAnnouncementCount(&threeCninfo)
	if errT != nil {
		logging.Info("get my announcement count err", errT)
	}
	// 获取三天未处理
	data["annoucement_data"] = map[string]int{
		"deal":  deal,
		"wait":  wait,
		"three": threeWait,
	}
	// 获取研报数据
	researchDeal, researchWait := GetUserEventCount(nowDay, "u_research_report", "research", "", industryNames, secondIndustry, userID)
	// 获取当天未处理的
	_, threeResearchWait := GetUserEventCount(threeDay, "u_research_report", "research", "", industryNames, secondIndustry, userID)
	// 获取三天未处理
	data["research_data"] = map[string]int{
		"deal":  researchDeal,
		"wait":  researchWait,
		"three": threeResearchWait,
	}

	// 获取调研数据
	// investigationDeal, investigationWait := GetUserEventCount(nowDay, "p_investigation", "investigation", "", industryNames, secondIndustry, userID)
	// // 获取当天未处理的
	// _, threeInvestigationWait := GetUserEventCount(threeDay, "p_investigation", "investigation", "", industryNames, secondIndustry, userID)
	// 获取三天未处理 调研纪要和公告一起获取
	data["investigation_data"] = map[string]int{
		"deal":  0,
		"wait":  0,
		"three": 0,
	}

	// 获取今日得星
	star := GetUserStar(userID, nowDay)
	monthStar := GetUserStar(userID, monthDay)
	data["star"] = map[string]float64{
		"today_star": star.Star,
		"month_star": monthStar.Star,
	}
	return data
}

func CreateUserEvent(event *UserEvent) error {
	if err := models.DB().Table("u_user_event").Create(&event).Error; err != nil {
		logging.Error("userEvent.CreateUserEvent() Errors: ", err.Error())
		return err
	}
	return nil
}

func UpdateUserEvent(id int, data interface{}) error {
	var userEvent UserEvent
	err := models.DB().Model(&userEvent).Where("id = ? ", id).Update(data)
	if err != nil {
		logging.Info("userEvent.UpdateUserEvent() Errors: ", err.Error)
		return err.Error
	}
	return nil
}

func GetUserEvent(data interface{}) (UserEvent, error) {
	var userEvent UserEvent
	err := models.DB().Model(&userEvent).Where(data).First(&userEvent).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return userEvent, err
	}
	if userEvent.Id > 0 {
		return userEvent, nil
	}
	return userEvent, nil
}

// 根据code 获取 user_id
func GetUserIdByCode(data map[string]interface{}) (int, error) {
	var User struct {
		UserID int `json:"user_id" gorm:"column:user_id"`
	}
	userModel := &User
	err := models.DB().Table("u_stock_industry").
		Select("u_second_industry_user.user_id").
		Joins("left join u_second_industry_user on u_stock_industry.second_industry = u_second_industry_user.second_industry").
		Where("u_second_industry_user.deleted_at is null ").
		Where(data).First(&userModel).Error
	if err != nil {
		return userModel.UserID, err
	}
	return userModel.UserID, nil
}

func GetHistroyEvent(data map[string]interface{}) ([]HistroyEvent, error) {
	var userEvent []HistroyEvent
	dbs := models.DB().Table("u_user_event").
		Joins("left join u_user_stock on u_user_event.object_id = u_user_stock.id").
		Where("u_user_event.object_type in (?)", []string{"stock_pool", "personal_stock"}).
		Where("u_user_event.event_str != ?", "")
	if data["code"].(string) != "" {
		dbs = dbs.Where("u_user_stock.code = ? ", data["code"].(string))
	}
	if data["user_id"].(int) > 0 {
		dbs = dbs.Where("u_user_event.user_id = ? ", data["user_id"].(int))
	}
	err := dbs.Order("u_user_event.updated_at desc").Find(&userEvent).Error
	if err != nil {
		return userEvent, err
	}
	if len(userEvent) < 1 {
		return []HistroyEvent{}, nil
	}
	for index, value := range userEvent {
		if value.PictureLink == "" {
			userEvent[index].PictureLinkArr = []ImgDTO{}
		} else {
			pictureArr := []ImgDTO{}
			_ = json.Unmarshal([]byte(value.PictureLink), &pictureArr)
			userEvent[index].PictureLinkArr = pictureArr
		}
	}
	return userEvent, nil
}

func GetHistroyEventTotal(data map[string]interface{}) (int, error) {
	var count int
	dbs := models.DB().Table("u_user_event").
		Joins("left join u_user_stock on u_user_event.object_id = u_user_stock.id").
		Where("u_user_event.object_type in (?)", []string{"stock_pool", "personal_stock"}).
		Where("u_user_event.event_str != ?", "")
	if data["code"].(string) != "" {
		dbs = dbs.Where("u_user_stock.code = ? ", data["code"].(string))
	}
	if data["user_id"].(int) > 0 {
		dbs = dbs.Where("u_user_event.user_id = ? ", data["user_id"].(int))
	}
	err := dbs.Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}

// 获取用户最后一条记录
func GetUserEventLastRecord(data map[string]interface{}) UserEvent {
	var userEvent UserEvent
	dbs := models.DB().Model(&userEvent)
	if data["user_id"].(int) > 0 {
		dbs = dbs.Where("user_id = ? ", data["user_id"])
	}
	if data["event_status"] != "" {
		dbs = dbs.Where("event_status = ? ", data["event_status"])
	}
	if data["updated_start"] != "" {
		dbs = dbs.Where("updated_at > ? ", data["updated_start"])
	}
	if data["updated_end"] != "" {
		dbs = dbs.Where("updated_at <= ? ", data["updated_end"])
	}
	err := dbs.Order("updated_at desc").First(&userEvent).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return userEvent
	}
	if userEvent.Id > 0 {
		return userEvent
	}
	return userEvent
}

// 获取用户研报和公告信息
func GetUserStatistic(data map[string]interface{}) []StaticsticDetail {
	var (
		userEvent []UserEvent
		details   []StaticsticDetail
		ret       Detail
	)
	dbs := models.DB().Model(&userEvent)
	if data["user_id"].(int) > 0 {
		dbs = dbs.Where("user_id = ? ", data["user_id"])
	}
	if data["start"] != "" {
		dbs = dbs.Where("updated_at > ? ", data["start"])
	}
	if data["end"] != "" {
		dbs = dbs.Where("updated_at <= ? ", data["end"])
	}
	err := dbs.Where("event_status = ? ", 1).Limit(data["page_size"]).
		Offset(data["page_size"].(int) * (data["page_num"].(int) - 1)).Order("updated_at desc").Find(&userEvent).Error
	if err != nil {
		return []StaticsticDetail{}
	}
	if len(userEvent) < 1 {
		return []StaticsticDetail{}
	}
	for _, value := range userEvent {
		detail := &StaticsticDetail{}
		detail.UpdatedAt = value.UpdatedAt.Format("2006-01-02 15:04:05")
		if value.ObjectType == EVENT_OBJECT_TYPE_ANNOUCEMENT {
			ret = GetAnnouncementDetailById(value.ObjectID)
		} else if value.ObjectType == EVENT_OBJECT_TYPE_RESEARCH {
			ret = GetReportDetailById(value.ObjectID)
		}
		detail.Title = ret.Title
		detail.Code = ret.Code
		detail.CodeName = ret.Name
		detail.UserID = value.UserID
		detail.EventStr = value.EventStr
		detail.CreatedAt = ret.CreatedAt.Format("2006-01-02 15:04:05")
		_ = json.Unmarshal([]byte(value.PictureLink), &detail.PictureLink)
		details = append(details, *detail)
	}
	return details
}

// 获取用户研报和公告信息
func GetUserStatisticCount(data map[string]interface{}) int {
	var (
		count int
	)
	dbs := models.DB().Table("u_user_event")
	if data["user_id"].(int) > 0 {
		dbs = dbs.Where("user_id = ? ", data["user_id"])
	}
	if data["start"] != "" {
		dbs = dbs.Where("updated_at > ? ", data["start"])
	}
	if data["end"] != "" {
		dbs = dbs.Where("updated_at <= ? ", data["end"])
	}
	err := dbs.Where("event_status = ? ", 1).Order("updated_at desc").Count(&count).Error
	if err != nil {
		return 0
	}
	return count
}

func GetAnnouncementDetailById(id int) Detail {
	var announcement Detail
	err := models.DB().Table("u_cninfo_notice").Where("id = ? ", id).Order("id desc").First(&announcement).Error
	if err != nil {
		return Detail{}
	}
	return announcement
}

func GetReportDetailById(id int) Detail {
	var report Detail
	err := models.DB().Table("u_research_report").Where("id = ? ", id).Order("id desc").First(&report).Error
	if err != nil {
		return Detail{}
	}
	return report
}

type AnnouncementEventQO struct {
	ID     int `json:"id" form:"id" `          //公告ID
	UserID int `json:"user_id" form:"user_id"` // 用户ID
	//ObjectType  string `json:"object_type" form:"object_type"`   // 对象类型
	EventStatus string `json:"event_status" form:"event_status"` // 事件状态
	Comment     *bool  `json:"comment" form:"comment"`
	//Direction   bool   `json:"direction" form:"direction"`
	StartTime string `json:"start_time" form:"start_time" binding:"required"`
	EndTime   string `json:"end_time" form:"end_time" binding:"required"`

	Code     string `json:"code" form:"code"`
	Type     string `json:"type" form:"type"`
	Keyword  string `json:"keyword" form:"keyword"`
	PageSize int    `json:"page_size" form:"page_size" binding:"gt=0"`
	PageNum  int    `json:"page_num" form:"page_num" binding:"gt=0"`
}

func GetMyAnnouncementCount(qo *AnnouncementEventQO) (int, int, error) {
	type countS struct {
		Deal int `json:"deal"`
		Wait int `json:"wait"`
	}
	cnt := countS{}
	amountLimit := 300000 //3亿
	qo.EndTime = time.Now().Format(util.YMD)
	tradeDayList := make([]time.Time, 0)
	bandCodeList := make([]string, 0)
	bandZszList := make([]string, 0)
	err := models.DB().Table("b_trade_date").
		Where("trade_date<=?", qo.EndTime).
		Order("trade_date desc").Limit(11).
		Pluck("trade_date", &tradeDayList).Error
	if err != nil {
		logging.Error("GetMyAnnouncementList 获取前10交易日失败", err.Error())
	}
	if len(tradeDayList) > 1 {
		start := 0
		end := len(tradeDayList)
		if tradeDayList[0].Before(time.Now()) {
			end = end - 1
		} else {
			start = start + 1
		}
		err = models.DB().Table("p_stock_tick").
			Where("stock_code is not null").
			Where("trade_date in (?)", tradeDayList[start:end]).
			Group("stock_code").
			Having("avg(amount)<?", amountLimit).
			Pluck("stock_code", &bandCodeList).Error
		if err != nil {
			logging.Error("GetMyAnnouncementList 获取前10日日均成交额小于6000万股票失败", err.Error())
		}
		err = models.DB().Raw("select stock_code FROM p_stock_tick left join p_stock_base_info on stock_code=code where trade_date='"+tradeDayList[0].Format(util.YMD)+"' and (zgb*close)<5000000000").Pluck("stock_code", &bandZszList).Error
		bandCodeList = append(bandCodeList, bandZszList...)
	}
	aSql := "SELECT N.id,N.code,N.name,N.title,N.source_link,N.created_at,N.type, S.object_id,S.event_status,S.event_str,S.user_id,S.picture_link " +
		" FROM (SELECT GROUP_CONCAT(distinct type separator ',') as `type`,id,code,name,title,source_link,publish_time,catch_time,spider_time,created_at,deleted_at FROM u_cninfo_notice" +
		"  WHERE `u_cninfo_notice`.`deleted_at` IS NULL" + CheckTitleSql() + BandCodeSql(bandCodeList) +
		"  group by source_link, u_cninfo_notice.code) N"
	iSql := " SELECT P.id,P.code,P.name,P.title,P.source_link,P.created_at,'调研纪要' type, S.object_id,S.event_status,S.event_str,S.user_id,S.picture_link" +
		" FROM `p_investigation` P" +
		" LEFT JOIN (SELECT * FROM u_user_event WHERE object_type = 'investigation' AND user_id=%d) S   ON P.id = S.object_id" +
		" LEFT JOIN u_stock_industry I ON I.code = P.code" +
		" LEFT JOIN u_second_industry_user IU ON IU.second_industry = I.second_industry" +
		" WHERE (IU.deleted_at IS NULL)  AND (IU.user_id = %d)  "
	//" ORDER BY created_at DESC"
	if qo.UserID > 0 {
		aSql = aSql + fmt.Sprintf(
			"  LEFT JOIN (SELECT * FROM u_user_event WHERE object_type = 'announcement' and user_id = %d) S   ON N.id = S.object_id"+
				"  LEFT JOIN u_stock_industry I ON I.code = N.code"+
				"  LEFT JOIN u_second_industry_user IU ON IU.second_industry = I.second_industry"+
				" WHERE (IU.deleted_at IS NULL)  AND (IU.user_id = %d) ", qo.UserID, qo.UserID)
		iSql = fmt.Sprintf(iSql, qo.UserID, qo.UserID)
	} else {
		return 0, 0, errors.New("非法用户ID")
	}
	if qo.EventStatus != "" {
		if qo.EventStatus == "1" {
			aSql = aSql + fmt.Sprintf(" and event_status=%s", qo.EventStatus)
			iSql = fmt.Sprintf(iSql+" and event_status=%s", qo.EventStatus)
		} else {
			aSql = aSql + fmt.Sprintf(" and (event_status=%s or event_status IS NULL)", qo.EventStatus)
			iSql = fmt.Sprintf(iSql+" and (event_status=%s or event_status IS NULL)", qo.EventStatus)
		}
	}
	if qo.StartTime != "" {
		aSql = aSql + fmt.Sprintf(" and N.created_at>='%s'", qo.StartTime+" 00:00:00")
		iSql = fmt.Sprintf(iSql+" and P.created_at>='%s'", qo.StartTime+" 00:00:00")
	}
	if qo.EndTime != "" {
		aSql = aSql + fmt.Sprintf(" and N.created_at<='%s'", qo.EndTime+" 23:59:59")
		iSql = fmt.Sprintf(iSql+" and P.created_at<='%s'", qo.EndTime+" 23:59:59")
	}
	if qo.Comment != nil {
		if *qo.Comment {
			aSql = aSql + " and S.event_str!=''"
			iSql = iSql + " and S.event_str!=''"
		} else {
			aSql = aSql + " and (S.event_str='' OR S.event_str is NULL)"
			iSql = iSql + " and (S.event_str='' OR S.event_str is NULL)"
		}
	}

	if qo.Keyword != "" {
		aSql = aSql + fmt.Sprintf(" and N.title like '%s'", "%"+qo.Keyword+"%")
		iSql = fmt.Sprintf(iSql+" and P.title like '%s'", "%"+qo.Keyword+"%")
	}
	if qo.Code != "" {
		aSql = aSql + fmt.Sprintf(" and (N.name like '%s' or N.code like '%s')", "%"+qo.Code+"%", "%"+qo.Code+"%")
		iSql = fmt.Sprintf(iSql+" and (P.name like '%s' or P.code like '%s')", "%"+qo.Code+"%", "%"+qo.Code+"%")
	}
	subSql := aSql + " UNION ALL " + iSql

	if qo.Type != "全部" && qo.Type != "" {
		subSql = "SELECT * FROM (" + subSql + ") SS where (" + "type like '" + "%" + strings.ReplaceAll(qo.Type, ",", "%' or type like '%") + "%')"
	}
	err = models.DB().Raw("SELECT count(event_status=1) deal,count(*)-count(event_status=1) wait FROM (" + subSql + ") counttable").Find(&cnt).Error
	if err != nil {
		logging.Error("GetMyAnnouncementCount() count err:" + err.Error())
		return 0, 0, err
	}
	return cnt.Deal, cnt.Wait, nil
}

func CheckTitleSql() string {
	keyWordList := [][]string{
		{"独立董事"},
		{"监事"},
		{"闲置募集资金"},
		{"闲置自有资金"},
		{"核查意见"},
		{"企业类型变更"},
		{"审计"},
		{"召开", "临时股东大会"},
		{"证券事务代表"},
		{"法律意见书"},
		{"议事规则"},
		{"发行结果"},
		{"股权激励", "回购注销"},
		{"三方监管协议"},
		{"注销完成"},
		{"资产评估报告"},
		{"章程"},
		{"管理制度"},
		{"登记制度"},
		{"管理办法"},
		{"会计师事务所"},
		{"质押"},
		{"高新技术企业"},
		{"短期融资券"},
		{"委托理财"},
		{"工会"},
		{"审阅报告"},
		{"信用评级报告"},
		{"财务顾问"},
		{"发行保荐书"},
		{"决策制度"},
		{"工作制度"},
		{"网上路演公告"},
		{"摘要"},
		{"合规"},
		{"保密制度"},
		{"工商设立"},
		{"授信额度"},
		{"子公司担保"},
		{"评估机构"},
		{"事先认可意见"},
		{"赎回结果"},
		{"股票期权注销完成"},
		{"现场检查报告"},
		{"现场培训报告"},
		{"受托管理事务临时报告"},
		{"提供担保"},
		{"度报告正文"},
		{"临时股东大会"},
		{"外汇套期保值"},
		{"子公司名称变更"},
		{"内部控制"},
		{"中签结果"},
		{"募集资金"},
		{"摊薄即期回报"},
		{"权益分派实施"},
		{"激励对象名单"},
		{"理财产品"},
		{"票据池"},
		{"外汇衍生品"},
		{"工作规则"},
		{"督导"},
		{"付息公告"},
		{"验资报告"},
		{"对外担保额度预计"},
		{"日常关联交易额度预计"},
		{"日常关联交易预计"},
		{"会计师"},
		{"股票交易异常波动"},
		{"银行综合授信"},
		{"内幕"},
		{"临时会议决议"},
		{"法律意见"},
		{"政府补贴"},
		{"套期保值"},
		{"结售汇"},
		{"公告词"},
		{"保荐"},
	}
	sql := ""
	for _, words := range keyWordList {
		if len(words) > 1 {
			like := "%"
			for _, word := range words {
				like = like + word + "%"
			}
			sql = sql + " and title not like '" + like + "'"
		} else {
			sql = sql + " and title not like '%" + words[0] + "%'"
		}
	}
	return sql
}

func BandCodeSql(codeList []string) string {
	sql := ""
	if len(codeList) > 0 {
		sql = sql + " and u_cninfo_notice.code not in ('" + strings.Join(codeList, "','") + "')"
	}
	return sql
}

func GetUserFirstIndustry(userID int) string {
	type IndustryName struct {
		IndustryName string `json:"industry_name" gorm:"cloumn:industry_name"`
	}
	industryNames := []IndustryName{}
	names := []string{}
	err := models.DB().Table("u_second_industry_user").
		Select("distinct first_industry as industry_name").
		Where("user_id = ? ", userID).Where("deleted_at is null ").Find(&industryNames).Error
	if err != nil {
		logging.Info("userEvent.GetUserIndustry err:", err.Error)
		return ""
	}
	for _, value := range industryNames {
		if value.IndustryName != "" {
			names = append(names, value.IndustryName)
		}
	}
	type Industry struct {
		Name string `json:"name" gorm:"cloumn:name"`
		Id   int    `json:"id" gorm:"cloumn:id"`
	}
	industry := []Industry{}
	err = models.DB().Table("u_first_industry").
		Where("name in (?) ", names).Where("deleted_at is null ").Find(&industry).Error
	if err != nil {
		logging.Info("userEvent.GetUserIndustry err:", err.Error)
		return ""
	}
	where := ""
	for i, v := range industry {
		where = where + fmt.Sprintf("(u_research_report.first_ids like '%v')",
			"%#"+strconv.Itoa(v.Id)+"#%")
		if i < len(industry)-1 {
			where = where + " OR "
		}
	}
	return where
}