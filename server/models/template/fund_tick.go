package template

import (
    "datacenter/models"
    "datacenter/pkg/logging"
    "datacenter/pkg/util"
    "github.com/jinzhu/gorm"
    "github.com/shopspring/decimal"
    "sort"
    "time"
)

type FundTick struct {
    ID                   int                  `json:"id" gorm:"column:id" `                                // 主键ID
    Status               int                 `json:"status" gorm:"column:status"`                         // 状态  0：未分红；1：分红
    FundId               int                 `json:"fund_id" gorm:"column:fund_id"`                       // 基金ID
    Created              time.Time           `json:"created" gorm:"column:created"`                       // 创建时间
    ValueDate            time.Time           `json:"value_date" gorm:"column:value_date"`                 // 记录日期
    Value                decimal.Decimal     `json:"value" gorm:"column:value"`                           // 累计净值
    UnitValue            decimal.Decimal     `json:"unit_value" gorm:"column:unit_value"`                 // 单位净值
    Deleted              int                 `json:"-" gorm:"column:deleted"`
}

func(FundTick) TableName() string {
    return "p_fund_tick"
}

func GetFundTicks(fundId,day int) ([]FundTick, error) {
    var (
        fundTicks []FundTick
        err  error
    )
    dbs := models.DB().Select("id,status,unit_value,value,created,value_date,fund_id").Where("fund_id = ? ",fundId).
        Where("deleted = 0").
        Order("value_date desc")
    if day > 0 {
        dbs = dbs.Limit(day)
        if day == 180 {
            dbs = dbs.Where("created >= ?",time.Now().AddDate(0,-6,0).Format(util.YMD))
        } else if day == 360 {
            dbs = dbs.Where("created >= ?",time.Now().AddDate(0,-12,0).Format(util.YMD))
        }
    }
    err = dbs.Find(&fundTicks).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return nil, err
    }
    if day >= 0  {
        sort.Slice(fundTicks, func(i, j int) bool {
            return fundTicks[i].ValueDate.Sub(fundTicks[j].ValueDate) < 0
        })
    }
    return fundTicks, nil
}

func AddFundTick(tick FundTick) error {
    if err := models.DB().Table("p_fund_tick").Create(&tick).Error; err != nil {
        return err
    }
    return nil
}

func EditFundTicks(index interface{}, data interface{}) error {
    if err := models.DB().Table("p_fund_tick").Model(&FundTick{}).Where(index).Updates(data).Error; err != nil {
        return err
    }
    return nil
}

func DeleteFundTicks(id int ) error {
    if err := models.DB().Table("p_fund_tick").Where("id = ? ",id).Updates(map[string]string{"deleted":"1"}).Error; err != nil {
        return err
    }
    return nil
}

func ExistRecord(fundId int, valueDate time.Time) (int,error) {
    var count int
    err := models.DB().Table("p_fund_tick").Select("id").Where("deleted = 0").Where("fund_id = ?",fundId).Where("value_date = ? ",valueDate).Count(&count).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return count, err
    }
    return count, nil
}

func ExistRecordById(id int) (bool,error) {
    var tick FundTick
    err := models.DB().Table("p_fund_tick").Select("id").Where("deleted = 0").Where("id = ?",id).First(&tick).Error
    if err != nil  {
        return false, err
    }

    if tick.ID > 0 {
        return true, nil
    }

    return false, nil
}

func GetLastRecordByFundId(fundId int) (ret FundTick,err error){
    var tick FundTick
    err = models.DB().Where("fund_id = ?",fundId).Where("deleted = 0").Order("value_date desc").First(&tick).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        logging.Info("get last record fund err :",err)
        return FundTick{}, err
    }
    if tick.ID > 0 {
        return tick, nil
    }

    return FundTick{}, nil
}

func GetSpecialUnitValue(ticks []FundTick) (maxUnitValue,minUnitValu decimal.Decimal) {
    var (
        preMaxValue decimal.Decimal
        preMinUnitValue decimal.Decimal
    )
    for _,value := range ticks {
        if preMaxValue.Sub(value.Value).Sign() < 0  {
            preMaxValue = value.Value
        }
        if preMinUnitValue.IsZero()  {
            preMinUnitValue = value.UnitValue
        }else {
            if preMinUnitValue.Sub(value.UnitValue).Sign()  >  0  {
                preMinUnitValue = value.UnitValue
            }
        }
    }
    return preMaxValue,preMinUnitValue
}

func GetLastCashRecordByFundId(data map[string]interface{}) (ret []FundTick,err error){
    var tick []FundTick
    dbs := models.DB()
    if data["fund_id"].(int) > 0 {
        dbs = dbs.Where("fund_id = ? ",data["fund_id"])
    }
    if data["status"].(int) > 0 {
        dbs = dbs.Where("status = ? ",data["status"])
    }
    if data["day"].(int) > 0 {
        dbs = dbs.Limit(data["day"])
    }
    err = dbs.Where("deleted = 0").Order("value_date desc").Find(&tick).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return []FundTick{}, err
    }
    if len(tick) > 0 {
        return tick, nil
    }

    return []FundTick{}, nil
}