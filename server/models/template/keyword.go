package template

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/keyword_service"
	"fmt"
	"github.com/jinzhu/gorm"
	"time"
)

type UKeyword struct {
	models.Base
	Name         string `gorm:"column:name;type:varchar(500)" json:"name"`         // 名称
	Category     string `gorm:"column:category;type:varchar(100)" json:"category"` // 分类
	Star         int    `gorm:"column:star;type:int(5)" json:"star"`               // 重要等级
	Color        string `gorm:"column:color;type:varchar(16)" json:"color"`        // 提醒颜色
	Usage        string `gorm:"column:usage;type:varchar(16)" json:"usage"`        // 用途
	Case         string `gorm:"column:case;type:varchar(128)" json:"case"`         // 场景
	Creator      int    `gorm:"column:creator;type:bigint(20)" json:"creator"`     // 创建者ID
	CreatorName  string `json:"creator_name"`                                      //创建者名称
	Status       string `gorm:"column:status;type:varchar(50)" json:"status"`      // 状态
	PromptStatus int    `json:"prompt_status"`                                     //提示状态
	EditTimes    int    `json:"edit_times"`                                        //修改次数
}

func (u *UKeyword) AfterFind() (err error) {
	var (
		b_u BUser
	)
	if !util.IsEmpty(u.Creator) {
		models.DB().Table("b_user").Select("realname").Where("id = ?", u.Creator).Find(&b_u)
		u.CreatorName = b_u.Realname
	}
	return
}

type UKeywordEditHistory struct {
	ID        int       `gorm:"primary_key;column:id;type:int(10) unsigned;not null" json:"-"`
	KeywordID int       `gorm:"column:keyword_id;type:int(11)" json:"keyword_id"`  // 对应u_keyword表的主键id
	CreatedAt time.Time `gorm:"column:created_at;type:datetime" json:"created_at"` // 创建时间,也就是u_keyword关键词的修改时间
}

func KeywordList(req keyword_service.KeywordListRequest) ([]UKeyword, int, int, error) {
	var (
		k_l_m            []UKeyword
		count            int
		maintenanceCount int
		err              error
	)

	dbs := models.DB().Table("u_keyword")
	if !util.IsEmpty(req.Keyword) {
		dbs = dbs.Where("name like ?", "%"+req.Keyword+"%")
	}
	//个人关键词
	if !util.IsEmpty(req.CreatorId) {
		dbs = dbs.Where("creator = ?", req.CreatorId)
		dbs = dbs.Order("updated_at,prompt_status desc")
	} else {
		dbs = dbs.Order("updated_at desc")
	}

	if !util.IsEmpty(req.Category) {
		dbs = dbs.Where("category = ?", req.Category)
	}
	if !util.IsEmpty(req.Star) {
		dbs = dbs.Where("star = ?", req.Star)
	}
	if !util.IsEmpty(req.Status) {
		dbs = dbs.Where("status = ?", req.Status)
	}
	if !util.IsEmpty(req.Case) {
		dbs = dbs.Where("`case` like ?", "%"+req.Case+"%")
	}
	dbs.Where("deleted_at is null").Count(&count)
	dbs = dbs.Scopes(PageAndPageSize(req.Page.Page, req.Page.PageSize))
	err = dbs.Find(&k_l_m).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Error("keyword.KeywordList() err:", err.Error())
		return k_l_m, 0, 0, err
	}
	return k_l_m, count, maintenanceCount, err
}

func KeywordListPersion(req keyword_service.KeywordListRequest) ([]UKeyword, int, int, error) {
	var (
		k_l_m            []UKeyword
		u_k              []UKeyword
		count            int
		maintenanceCount int
		u_k_e_h          []UKeywordEditHistory
		err              error
	)
	//当前时间秒数
	timeNow := util.GenDateFormat2()
	timeNowSecond, _ := util.GenBeginEndTimeThree(timeNow) //当天时间的00:00:00

	//个人关键词,先把所有的计算一遍,之后在进行分页查询
	models.DB().Table("u_keyword").Select("id,edit_times,prompt_status,created_at").Where("creator = ?", req.CreatorId).Where("deleted_at is null").Where("status = ?", "normal").Find(&u_k)
	if !util.IsEmpty(u_k) {
		for _, v := range u_k {
			switch {
			case v.EditTimes >= 2:
				//修改超过2次,不提示
				models.DB().Table("u_keyword").Where("id = ?", v.ID).Updates(map[string]interface{}{
					"prompt_status": 0,
				})
			case v.EditTimes == 1:
				//获取其更新时间(u_keyword_edit_history),并且判断当前时间和这个时间是否为第7天,是第7天则提示,往后一直提示,不是则不提示
				models.DB().Table("u_keyword_edit_history").Where("keyword_id = ?", v.ID).Find(&u_k_e_h)
				if !util.IsEmpty(u_k_e_h) {
					updateDate := util.ConvTimeToStdTime2(u_k_e_h[0].CreatedAt) //修改时间转日期,年月日
					updateSecond, _ := util.GenBeginEndTimeThree(updateDate)    //当天时间的00:00:00
					if timeNowSecond-updateSecond >= 60*60*24*7 {
						//大于等于7天,提示
						models.DB().Table("u_keyword").Where("id = ?", v.ID).Updates(map[string]interface{}{
							"prompt_status": 1,
						})
					} else {
						models.DB().Table("u_keyword").Where("id = ?", v.ID).Updates(map[string]interface{}{
							"prompt_status": 0,
						})
					}
				}
			case v.EditTimes == 0:
				//一次都没有修改过,正好7天,开始提示,一直不修改就一直提示,直到修改2次之后,就不提示了
				updateDate := util.ConvTimeToStdTime2(v.CreatedAt)       //修改时间转日期,年月日
				updateSecond, _ := util.GenBeginEndTimeThree(updateDate) //当天时间的00:00:00
				if timeNowSecond-updateSecond >= 60*60*24*7 {
					//大于等于7天
					models.DB().Table("u_keyword").Where("id = ?", v.ID).Updates(map[string]interface{}{
						"prompt_status": 1,
					})
				}
			}
		}
	}

	//统计 需要提示的条数
	models.DB().Table("u_keyword").Where("creator = ?", req.CreatorId).Where("deleted_at is null").Where("status = ?", "normal").Where("prompt_status = ?", 1).Count(&maintenanceCount)

	dbs := models.DB().Table("u_keyword")
	if !util.IsEmpty(req.Keyword) {
		dbs = dbs.Where("name like ?", "%"+req.Keyword+"%")
	}
	if !util.IsEmpty(req.CreatorId) {
		dbs = dbs.Where("creator = ?", req.CreatorId)
	}

	if !util.IsEmpty(req.Category) {
		dbs = dbs.Where("category = ?", req.Category)
	}
	if !util.IsEmpty(req.Star) {
		dbs = dbs.Where("star = ?", req.Star)
	}
	if !util.IsEmpty(req.Status) {
		dbs = dbs.Where("status = ?", req.Status)
	}
	if !util.IsEmpty(req.Case) {
		dbs = dbs.Where("`case` like ?", "%"+req.Case+"%")
	}
	dbs.Where("deleted_at is null").Count(&count)
	//内存分页
	dbs = dbs.Scopes(PageAndPageSize(req.Page.Page, req.Page.PageSize)).Order("prompt_status desc,updated_at desc")
	err = dbs.Find(&k_l_m).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Error("keyword.KeywordList() err:", err.Error())
		return k_l_m, 0, 0, err
	}
	return k_l_m, count, maintenanceCount, err
}
func AddKeyword(req keyword_service.AddKeywordRequest) (bool, error) {
	var (
		u_k UKeyword
		err error
	)
	u_k.Name = req.Keyword
	u_k.Category = req.Category
	u_k.Star = req.Star
	u_k.Case = req.Case
	u_k.Creator = req.CreatorId
	u_k.Status = req.Status
	u_k.Color = "red"
	u_k.Usage = "person"
	u_k.EditTimes = 0

	if countKeyword(req) > 0 {
		return false, fmt.Errorf("存在该关键词，请勿重复添加")
	}
	err = models.DB().Table("u_keyword").Omit("creator_name").Create(&u_k).Error
	if err != nil {
		logging.Error("keyword.AddKeyword() err:", err.Error())
		return false, err
	}
	return true, nil
}

func countKeyword(req keyword_service.AddKeywordRequest) int {
	var id []int
	err := models.DB().Table("u_keyword").Where("name = ?", req.Keyword).Where("deleted_at IS NULL").Pluck("id", &id).Error
	if err != nil {
		return 0
	}
	return len(id)
}

func EditKeyword(req keyword_service.AddKeywordRequest) (bool, error) {
	var (
		u_k_e_h UKeywordEditHistory
		err     error
	)
	err = models.DB().Table("u_keyword").Where("id = ?", req.Id).Updates(map[string]interface{}{
		"name":       req.Keyword,
		"category":   req.Category,
		"star":       req.Star,
		"case":       req.Case,
		"creator":    req.CreatorId,
		"status":     req.Status,
		"color":      "red",
		"usage":      "person",
		"edit_times": gorm.Expr("edit_times + ? ", 1), //修改次数,每修改一次+1,关键词提示用,>=2次不提示,不然就一直提示
	}).Error
	if err != nil {
		logging.Error("keyword.EditKeyword() err:", err.Error())
		return false, err
	}

	u_k_e_h.KeywordID = req.Id
	u_k_e_h.CreatedAt, _ = util.GenDateFormatTime()
	err = models.DB().Table("u_keyword_edit_history").Create(&u_k_e_h).Error
	if err != nil {
		logging.Error("keyword.EditKeyword() err:", err.Error())
		return false, err
	}

	models.DB().Table("u_keyword").Where("id = ?", req.Id).Updates(map[string]interface{}{
		"prompt_status": 0,
	})
	return true, nil
}

func DelKeyword(req keyword_service.DelKeywordRequest) (bool, error) {
	var (
		err error
	)
	err = models.DB().Table("u_keyword").Where("id = ?", req.Id).Updates(map[string]interface{}{
		"deleted_at": util.GenDateFormat(),
	}).Error
	if err != nil {
		logging.Error("keyword.DelKeyword() err:", err.Error())
		return false, err
	}
	return true, nil
}
