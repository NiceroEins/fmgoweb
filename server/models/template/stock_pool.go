package template

import (
	"datacenter/models"
	"datacenter/models/stock"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"time"
)

const RECOMMEND_STATUS = 0;
const DELETE_STATUS = 1;

//精选股池表
type BStockPool struct {
	models.Base
	Code                   string    `gorm:"column:code;type:varchar(10)" json:"code"`                                           // 股票代码
	Name                   string    `gorm:"column:name;type:varchar(255)" json:"name"`                                          // 股票名称
	CurrentPrice           *float64  `gorm:"column:current_price;type:decimal(10,2)" json:"current_price"`                       // 当前价格
	UpDownRange            *float64  `gorm:"column:up_down_range;type:decimal(10,2)" json:"up_down_range"`                       // 涨跌幅
	GrowthRate             *float64  `gorm:"column:growth_rate;type:decimal(18,2)" json:"growth_rate"`                           // 涨速
	Turnover               *float64  `gorm:"column:turnover;type:decimal(18,2)" json:"turnover"`                                 // 成交额
	CirculationMarketValue *float64  `gorm:"column:circulation_market_value;type:decimal(18,2)" json:"circulation_market_value"` // 流通市值
	TotalMarketValue       *float64  `gorm:"column:total_market_value;type:decimal(18,2)" json:"total_market_value"`             // 总市值
	TurnoverRate           *float64  `gorm:"column:turnover_rate;type:decimal(10,2)" json:"turnover_rate"`                       // 换手率
	PERatio                *float64  `gorm:"column:p_e_ratio;type:decimal(10,2)" json:"p_e_ratio"`                               // 市盈率
	IndustryClassification string    `gorm:"column:industry_classification;type:varchar(255)" json:"industry_classification"`    // 行业分类
	Type                   string    `gorm:"column:type;type:varchar(255)" json:"type"`                                          // 类型
	CurrentIncrease        *float64  `gorm:"column:current_increase;type:decimal(10,2)" json:"current_increase"`                 // 至今涨幅
	TransferAt             time.Time `gorm:"column:transfer_at;type:datetime" json:"transfer_at"`                                // 调入日期
}

type StockPoolManage struct {
	Id                         int                `gorm:"column:id"`                               //个人股票管理表
	Code                       string             `gorm:"column:code"`                             //股票代码
	UserId                     string             `gorm:"column:user_id"`                          //用户Id
	RecommendReason            string             `gorm:"column:recommend_reason"`                 //推荐理由
	DelReason                  string             `gorm:"column:del_reason"`                       //删除理由
	Status                     int                `gorm:"column:status"`                           //状态
	TransferPrice              decimal.NullDecimal `gorm:"column:transfer_price" json:"-"`         // 调入价格
	IndexTransferPrice         decimal.NullDecimal `gorm:"column:index_transfer_price" json:"-"`   // 调入指数
}

type StockPool struct {
	models.Simple
	Id                     int                 `json:"id" gorm:"column:id"`
	UserID                 int                 `json:"user_id" gorm:"column:user_id"`
	Code                   string              `json:"code" gorm:"column:code"`
	Type                   string              `json:"type" gorm:"column:type"`
	Description            string              `json:"description" gorm:"column:description"`          //情况说明
	TransferPrice       decimal.NullDecimal    `gorm:"column:transfer_price" json:"-"`
}
func(StockPool) TableName() string {
	return "u_user_stock"
}
// func StockPoolList(s_p_l_req stock_pool_service.StockPoolListRequest) ([]BStockPool, int, error) {
// 	var (
// 		res   []BStockPool
// 		count int
// 		err   error
// 	)
// 	err = models.DB().Table("b_stock_pool").Count(&count).Scopes(PageAndPageSize(s_p_l_req.Page, s_p_l_req.PageSize)).Find(&res).Error
// 	if err != nil && err != gorm.ErrRecordNotFound {
// 		logging.Error(fmt.Sprintf("stock_pool.go StockPoolList(),err = ", err.Error()))
// 		return res, 0, err
// 	}
// 	return res, count, err
// }


func ExistStockRecommondByCode(code,user_id string,status int) (bool, error) {
	var stock StockPoolManage
	err := models.DB().Table("b_stock_pool_manage").Select("id").Where("code = ?", code).
		Where("user_id = ? ",user_id).
		Where("status = ?",status).First(&stock).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("stock_pool.ExistStockRecommondByCode() ,Errors::",err.Error())
		return false, err
	}

	if stock.Id > 0 {
		return true, nil
	}
	return false, nil
}

// ExistTagByID determines whether a Tag exists based on the ID
func ExistStockRecommondByID(id int) (bool, error) {
	var stock StockPoolManage
	err := models.DB().Table("b_stock_pool_manage").Select("id").Where("id = ?", id).First(&stock).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	if stock.Id > 0 {
		return true, nil
	}

	return false, nil
}

func StockRecommondByCodeNum(user_id string,status int) (bool, error) {
	var num int
	models.DB().Table("b_stock_pool_manage").Select("id").Where("user_id = ? ",user_id).Where("status = ?",status).Count(&num)
	if num >= 5 {
		return true, nil
	}
	return false, nil
}

// AddTag Add a Tag
func AddStockRecommend(code,user_id,reason string,status int) error {
	stockManage := StockPoolManage{
		Code:             code,
		UserId:           user_id,
		RecommendReason:  reason,
		Status:           status,
	}
	currentPriceKey   := "stock:tick"
	err := stockManage.TransferPrice.Scan(GetRedisValue(currentPriceKey, code))
	if err != nil {
		logging.Info("stock_pool.AddStockRecommend() ,Errors::",err.Error())
		stockManage.TransferPrice.Valid = false
		return err
	}
	if stockManage.TransferPrice.Decimal.IsZero() {
		return errors.New("添加的股票已退市")
	}
	currentPriceIndexKey := "stock:ts_tick"
	err = stockManage.IndexTransferPrice.Scan(GetRedisValue(currentPriceIndexKey, stock.GetIndexCode(code)))
	if err != nil {
		stockManage.IndexTransferPrice.Valid = false
		logging.Info("stock_pool.AddStockRecommend() ,Errors::",err.Error())
		return err
	}
	if err := models.DB().Table("b_stock_pool_manage").Create(&stockManage).Error; err != nil {
		return err
	}
	return nil
}

// EditTag modify a single tag
func EditStockRecommend(id int, data interface{}) error {
	if err := models.DB().Table("b_stock_pool_manage").Where("id = ?", id).Updates(data).Error; err != nil {
		return err
	}

	return nil
}

func GetRedisValue(key string, field string) string {
	value, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString(key, field)
	if err != nil {
		logging.Error("redis get "+field+" value err:", err.Error())
		return ""
	}
	return value
}

func AddUserStockPool(code, typeName,description string,userId int) error {
	stockPool := StockPool{
		Code: code,
		UserID: userId,
		Type: typeName,
		Description: description,
	}
	if err := models.DB().Table("u_user_stock").Create(&stockPool).Error; err != nil {
		return err
	}
	return nil
}