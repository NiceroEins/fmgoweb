package template

import (
    "datacenter/models"
    "datacenter/pkg/gredis"
    "datacenter/pkg/logging"
    "github.com/jinzhu/gorm"
    "strconv"
)

type Customer struct {
    ID                   int        `json:"id" gorm:"column:id" `                            // 主键ID
    Realname             string     `json:"realname" gorm:"column:realname"`                 // 姓名
    IdCardNo             string     `json:"id_card_no" gorm:"column:id_card_no"`             // 身份证号
    UserName             string     `json:"username" gorm:"column:username"`                 // 登录账号
    Created              string     `json:"created" gorm:"column:created"`                   // 创建时间
    Type                 string     `json:"type" gorm:"column:type"`                         // 证件类型
    LoginPwd             string     `json:"login_pwd" gorm:"column:login_pwd"`               // 证件类型
    Deleted              int        `json:"-" gorm:"column:deleted"`
}

type CustomerNames struct {
    ID                   int        `json:"id" gorm:"column:id" `                                // 主键ID
    Realname             string     `json:"realname" gorm:"column:realname"`                     // 姓名
    OwnerId              string     `json:"owner_id" gorm:"column:owner_id"`                     // 登录账号
    IdCardNo             string     `json:"id_card_no" gorm:"column:id_card_no"`                 // 身份证号
}

func(Customer) TableName() string {
    return "p_customer"
}

func GetCustomers(pageNum int, pageSize int,customer Customer) ([]Customer, error) {
    var (
        customers []Customer
        err  error
    )
    if pageSize > 0 && pageNum >= 0 {
        dbs := models.DB().Select("id,realname,id_card_no,username,created,type")
        if customer.Realname != "" {
            dbs = dbs.Where("realname like ?", "%"+customer.Realname+"%")
        }
        if customer.UserName != "" {
            dbs = dbs.Where("username like ?", "%"+customer.UserName+"%")
        }
        if customer.IdCardNo != "" {
            dbs = dbs.Where("id_card_no like ?", "%"+customer.IdCardNo+"%")
        }
        err = dbs.Where("p_customer.deleted = 0").Order("created desc").Offset((pageNum - 1)*pageSize).Limit(pageSize).Find(&customers).Error
    } else {
        err = models.DB().Where("p_customer.deleted = 0").Find(&customers).Error
    }

    if err != nil && err != gorm.ErrRecordNotFound {
        return nil, err
    }

    return customers, nil
}

func AddCustomer(customer Customer) error {

    if err := models.DB().Table("p_customer").Create(&customer).Error; err != nil {
        return err
    }

    return nil
}

func GetCustomerTotal(customer Customer) (int, error) {
    var count int
    dbs := models.DB().Table("p_customer").Select("id,realname,id_card_no,username,created,type")
    if customer.Realname != "" {
        dbs = dbs.Where("realname like ?", "%"+customer.Realname+"%")
    }
    if customer.UserName != "" {
        dbs = dbs.Where("username like ?", "%"+customer.UserName+"%")
    }
    if customer.IdCardNo != "" {
        dbs = dbs.Where("id_card_no like ?", "%"+customer.IdCardNo+"%")
    }
    if err := dbs.Where("p_customer.deleted = 0").Count(&count).Error; err != nil {
        return 0, err
    }
    return count, nil
}

func EditCustomer(id int, data interface{}) error {
    // 获取删除的行的用户id
    customer := Customer{}
    err := models.DB().Table("p_customer").Where("id = ? ",id).First(&customer).Error
    if err != nil {
        return err
    }
    tx := models.DB().Begin()
    err = tx.Table("p_customer_fund").Where("owner_id = ? ",customer.ID).Update(map[string]interface{}{"deleted":1}).Error
    if err != nil {
        logging.Info("delete fund_customer err",err)
        tx.Rollback()
        return err
    }
    err = tx.Table("p_customer").Where("id = ? ",id).Update(map[string]interface{}{"deleted":1}).Error
    if err != nil {
        logging.Info("delete fund_notice err",err)
        tx.Rollback()
        return err
    }
    err = tx.Table("p_fund_bonus").Where("user_id = ? ",id).Delete(&FundBonus{}).Error
    if err != nil {
        logging.Info("delete fund_notice err",err)
        tx.Rollback()
        return err
    }
    tx.Commit()
    return nil
}

func ExistCustomer(data interface{}) (bool, error) {
    var customer Customer
    err := models.DB().Select("id").Where("p_customer.deleted = 0").Where(data).First(&customer).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return false, err
    }

    if customer.ID > 0 {
        return true, nil
    }

    return false, nil
}

func DeleteCustomer(maps interface{}) error {
    if err := models.DB().Where(maps).Delete(&Customer{}).Error; err != nil {
        return err
    }
    return nil
}

func ExistCustomerByName(realname,idCardNo string) (bool, error) {
    var customer Customer
    err := models.DB().Select("id").Where("realname = ? ",realname).
        Where("right(id_card_no, 6) = ? ",idCardNo[len(idCardNo)-6:]).Where("p_customer.deleted = 0").
        First(&customer).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return false, err
    }

    if customer.ID > 0 {
        return true, nil
    }

    return false, nil
}
func ExistCustomerByNameAndId(maps interface{}) (bool, error) {
    var customer Customer
    err := models.DB().Where("p_customer.deleted = 0").Where(maps).First(&customer).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return false, err
    }

    if customer.ID > 0 {
        return true, nil
    }

    return false, nil
}

func GetCustomersNames(realname string) (funds []CustomerNames,err error) {
    ret := []CustomerNames{}
    err = models.DB().Table("p_customer").Where("realname like ? ","%"+realname+"%").
        Select("realname,id,owner_id,id_card_no").Where("p_customer.deleted = 0").Find(&ret).Error
    for index,customer := range ret {
        data := make(map[string]interface{})
        data["realname"] = customer.Realname
        num,err := GetCustomerCount(data)
        if err != nil {
            logging.Error("template.GetCustomersNames() err:", err.Error())
            continue
        }
        if num < 2 {
            ret[index].IdCardNo = ""
        }
    }
    if err != nil && err != gorm.ErrRecordNotFound {
        logging.Error("template.GetCustomersNames() err:", err.Error())
        return nil,err
    }
    return ret,nil
}

func GetCustomer(data interface{}) (Customer, error) {
    var customer Customer
    err := models.DB().Where(data).Where("p_customer.deleted = 0").First(&customer).Error
    if err != nil{
        return Customer{}, err
    }

    return customer, nil
}

func Check(data interface{}) (Customer, error) {
    var customer Customer
    err := models.DB().Where("p_customer.deleted = 0").Where(data).First(&customer).Error
    if err != nil {
        return Customer{}, err
    }
    return customer, nil
}

func EditPwd(userId int,data interface{}) error {
    if err := models.DB().Model(&Customer{}).Where("id = ? ",userId).Update(data).Error; err != nil {
        return err
    }
    gredis.Set("loginfront:"+strconv.Itoa(userId), "", 60*60)
    return nil
}

func GetCustomerCount(data interface{}) (int, error) {
    var count int
    if err := models.DB().Model(&Customer{}).Where("p_customer.deleted = 0").Where(data).Count(&count).Error; err != nil {
        return 0, err
    }
    return count, nil
}