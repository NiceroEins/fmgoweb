package template

import (
    "datacenter/models"
    "github.com/jinzhu/gorm"
    "github.com/shopspring/decimal"
    "time"
)

type FundBonusResponse struct {
    FundBonus
    Realname             string     `json:"realname" gorm:"column:realname"`                 // 姓名
    FundName             string     `json:"fund_name" gorm:"column:fund_name"`               // 基金名称
}
type FundBonusContent struct {
    ID                   int             `json:"id" gorm:"column:id" `                            // 主键ID
    FundID               int             `json:"-" gorm:"column:fund_id"`                        // 基金id
    FundName             string          `json:"fund_name" gorm:"column:fund_name"`              // 基金名称
    RegisterDate         time.Time       `json:"register_date" gorm:"column:register_date"`       // 登记时间
}
type FundBonus struct {
    ID                   int             `json:"id" gorm:"column:id" `                            // 主键ID
    UserID               int             `json:"user_id" gorm:"column:user_id" `                  // 用户ID
    FundID               int             `json:"fund_id" gorm:"column:fund_id"`                   // 基金id
    BonusType            int             `json:"bonus_type" gorm:"column:bonus_type"`             // 分红方式
    RegisterDate         time.Time       `json:"register_date" gorm:"column:register_date"`       // 登记时间
    Updated              time.Time       `json:"updated" gorm:"column:updated"`                   // 更新时间
    Created              time.Time       `json:"-" gorm:"column:created"`                         // 创建时间
    InvestNum            decimal.Decimal `json:"investe_num" gorm:"column:investe_num"`           // 再投资份额
    UnitBonus            decimal.Decimal `json:"unit_bonus" gorm:"column:unit_bonus"`             // 每单位分红
    CashBonus            decimal.Decimal `json:"cash_bonus" gorm:"column:cash_bonus"`             // 现金红利
    InvesteMoney          decimal.Decimal `json:"investe_money" gorm:"column:investe_money"`        // 再投资金额
    InvesteUnitValue      decimal.Decimal `json:"investe_unit_value" gorm:"column:investe_unit_value"`  // 再投资单位净值
}

func(FundBonus) TableName() string {
    return "p_fund_bonus"
}

func GetFundBonus(pageNum int, pageSize int,data interface{},username string,registerDate time.Time) ([]FundBonusResponse, error) {
    var (
        fundBonus []FundBonusResponse
        err  error
    )
    dbs := models.DB().Table("p_fund_bonus").Select("p_fund_bonus.*,p_customer.realname,p_fund.name as fund_name").
        Joins("LEFT JOIN p_customer ON p_customer.id = p_fund_bonus.user_id").
        Joins("LEFT JOIN p_fund ON p_fund.id = p_fund_bonus.fund_id").
        Where("p_customer.deleted = 0").
        Where("p_fund.deleted = 0").
        Where(data)

    if username != "" {
        dbs = dbs.Where("p_customer.realname like ?", "%"+username+"%")
    }
    if !registerDate.IsZero() {
        dbs = dbs.Where("p_fund_bonus.register_date >= ?", registerDate)
    }
    dbs = dbs.Order("p_fund_bonus.created desc").Find(&fundBonus).Offset(pageNum).Limit(pageSize)
    if dbs.Error != nil && err != gorm.ErrRecordNotFound {
        return nil, dbs.Error
    }

    return fundBonus, nil
}

func AddFundBonus(bonus FundBonus) error {
    if err := models.DB().Table("p_fund_bonus").Create(&bonus).Error; err != nil {
        return err
    }

    return nil
}

func GetFundBonusTotal(data interface{},username string) (int, error) {
    var count int
    dbs := models.DB().Table("p_fund_bonus").Select("p_fund_bonus.*,p_customer.realname,p_fund.name as fund_name").
        Joins("LEFT JOIN p_customer ON p_customer.id = p_fund_bonus.user_id").
        Joins("LEFT JOIN p_fund ON p_fund.id = p_fund_bonus.fund_id").
        Where("p_customer.deleted = 0").
        Where("p_fund.deleted = 0").
        Where(data)

    if username != "" {
        dbs = dbs.Where("p_customer.realname like ?", "%"+username+"%")
    }

    dbs = dbs.Count(&count)
    if err := dbs.Error; err != nil {
        return 0, err
    }
    return count, nil
}

func EditFundBonus(id int, data interface{}) error {
    if err := models.DB().Model(&FundBonus{}).Where("id = ? ",id).Updates(data).Error; err != nil {
        return err
    }
    return nil
}

func ExistFundBonus(data interface{}) (bool, error) {
    var bonus FundBonus
    err := models.DB().Select("id").Where(data).First(&bonus).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return false, err
    }

    if bonus.ID > 0 {
        return true, nil
    }

    return false, nil
}

func DeleteFundBonus(id int) error {
    if err := models.DB().Where("id = ? ",id).Delete(&FundBonus{}).Error; err != nil {
        return err
    }
    return nil
}
func GetFundNoticeContent(data interface{}) ([]FundBonusContent, error)  {
    var (
        fundBonus []FundBonusContent
        err  error
    )
    dbs := models.DB().Table("p_fund_bonus").Select("p_fund_bonus.*,p_customer.realname,p_fund.name as fund_name").
        Joins("LEFT JOIN p_customer ON p_customer.id = p_fund_bonus.user_id").
        Joins("LEFT JOIN p_fund ON p_fund.id = p_fund_bonus.fund_id").
        Where("p_customer.deleted = 0").
        Where("p_fund.deleted = 0").
        Where(data)
    dbs = dbs.Order("register_date desc").Find(&fundBonus)
    if dbs.Error != nil && err != gorm.ErrRecordNotFound {
        return nil, dbs.Error
    }
    return fundBonus, nil
}
// 去重
func CheckDuplicateData(userId,fundId int,registerDate time.Time) (int, error) {
    var bonusCount int
    err := models.DB().Model(FundBonus{}).Where("user_id = ?",userId).
        Where("fund_id = ?",fundId).
        Where("register_date = ?",registerDate.Format("2006-01-02")).Count(&bonusCount).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return bonusCount, err
    }
    return bonusCount, nil
}

func CheckNumParms(investeMoneyStr,investNumStr,unitBonusStr,cashBonusStr,investeUnitValueStr string) (map[string]decimal.Decimal,error) {
    maps := make(map[string]decimal.Decimal)
    investeMoney,err := decimal.NewFromString(investeMoneyStr)
    if investeMoneyStr != "" && err != nil {
        return maps,err
    }
    maps["investeMoney"] = investeMoney
    investNum,err := decimal.NewFromString(investNumStr)
    if investNumStr != "" && err != nil {
        return maps,err
    }
    maps["investNum"] = investNum
    unitBonus,err := decimal.NewFromString(unitBonusStr)
    if unitBonusStr != "" && err != nil {
        return maps,err
    }
    maps["unitBonus"] = unitBonus
    cashBonus,err := decimal.NewFromString(cashBonusStr)
    if cashBonusStr != "" && err != nil {
        return maps,err
    }
    maps["cashBonus"] = cashBonus
    investeUnitValue,err := decimal.NewFromString(investeUnitValueStr)
    if investeUnitValueStr != "" && err != nil {
        return maps,err
    }
    maps["investeUnitValue"] = investeUnitValue
    return maps,nil
}

func GetFundBonusByID(data interface{}) (FundBonusResponse, error) {
    var (
        fundBonus FundBonusResponse
        err  error
    )
    dbs := models.DB().Table("p_fund_bonus").Select("p_fund_bonus.*,p_customer.realname,p_fund.name as fund_name").
        Joins("LEFT JOIN p_customer ON p_customer.id = p_fund_bonus.user_id").
        Joins("LEFT JOIN p_fund ON p_fund.id = p_fund_bonus.fund_id").
        Where("p_customer.deleted = 0").
        Where("p_fund.deleted = 0").
        Where(data)
    dbs = dbs.Find(&fundBonus)
    if dbs.Error != nil && err != gorm.ErrRecordNotFound {
        return fundBonus, dbs.Error
    }

    return fundBonus, nil
}

