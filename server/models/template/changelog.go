package template
import (
	"datacenter/models"
	"github.com/jinzhu/gorm"
	"time"
)

type ChangelogResponse struct {
	ID                 int        `json:"id" gorm:"column:id"`
	DeletedAt          time.Time  `json:"-" gorm:"column:deleted_at"`                         // 删除时间
	ChangelogAdd
}
type ChangelogAdd struct {
	Content            string     `json:"content" gorm:"column:content"`                      // 内容
	Created            time.Time  `json:"created" gorm:"column:created_at"`                   // 创建时间
	UserID             int        `json:"-" gorm:"column:user_id"`                            // 用户id
}

func(ChangelogResponse) TableName() string {
	return "b_changelog"
}

func GetChangelogLists() ([]*ChangelogResponse, error) {
	var changelog []*ChangelogResponse
	err := models.DB().Table("b_changelog").Order("created_at desc").Find(&changelog).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	return changelog, nil
}

func GetChangelogTotal() (int, error) {
	var count int
	err := models.DB().Model(&ChangelogResponse{}).Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}

func AddChangelog(changelog ChangelogAdd) error {
	if err := models.DB().Table("b_changelog").Create(&changelog).Error; err != nil {
		return err
	}

	return nil
}

func EditChangelog(id int, data interface{}) error {
	if err := models.DB().Model(&ChangelogResponse{}).Where("id = ?", id).Updates(data).Error; err != nil {
		return err
	}

	return nil
}

func DeleteChangelog(id int) error {
	if err := models.DB().Where("id = ?", id).Delete(ChangelogResponse{}).Error; err != nil {
		return err
	}

	return nil
}
