package template

import (
	"datacenter/models"
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/ipo_service"
)

func IpoList(req ipo_service.IpoListRequest) ([]spider.UIopEquity, int, error) {
	var (
		u_i_e []spider.UIopEquity
		count int
	)
	//查询东财表(一级)
	if err := models.DB().Table("u_iop_equity").Select("id,company,declare_time,equity_link,quantity_to_be_issued,proposed_listing_address,created_at,updated_at").Count(&count).Scopes(PageAndPageSize(req.Page, req.PageSize)).Order("declare_time " + req.Order).Find(&u_i_e).Error; err != nil {
		return u_i_e, 0, err
	}
	//计算 股权关系
	//for k, v := range u_i_e {
	//	//获取一级下面所有二级的名字,及图片链接
	//	var secondData []spider.Data
	//	models.DB().Table("u_iop_equity_relationship").Where("iop_equity_id = ?", v.ID).Select("id,parent_id,name,link,name_type").Find(&secondData)
	//	if len(secondData) == 0 {
	//		continue
	//	}
	//
	//	//赋值
	//	for _, value := range secondData {
	//		//赋值一级下面所有二级
	//		var secondEquityRelationship spider.EquityRelationship
	//		secondEquityRelationship.Id = value.Id
	//		secondEquityRelationship.ParentId = value.ParentId
	//		secondEquityRelationship.Company = value.Name
	//		secondEquityRelationship.Link = value.Link
	//
	//		u_i_e[k].EquityRelationship = append(u_i_e[k].EquityRelationship, secondEquityRelationship)
	//
	//		//查出二级下面的三级数据
	//		for _, vv := range u_i_e[j].EquityRelationship {
	//			//查出二级下面所属的三级
	//			var thirdData []spider.Data
	//			models.DB().Table("u_iop_equity_relationship").Where("parent_id= ?", vv.Id).Where("name_type = ?", 6).Select("id,parent_id,name,link,name_type").Find(&thirdData)
	//			if len(thirdData) != 0 {
	//				for _, vvv := range thirdData {
	//					var thirdEquityRelationship spider.EquityRelationship
	//					thirdEquityRelationship.Id = vvv.Id
	//					thirdEquityRelationship.ParentId = vvv.ParentId
	//					thirdEquityRelationship.Company = vvv.Name
	//					thirdEquityRelationship.Link = vvv.Link
	//
	//					u_i_e[k].EquityRelationship[i].ClildEquityRelationship = append(u_i_e[k].EquityRelationship[i].ClildEquityRelationship, thirdEquityRelationship)
	//				}
	//			}
	//		}
	//	}
	//
	//	//获取每个二级下面所有三级的名字,及图片链接
	//}
	//获取一级公司下面所属二级,三级公司名,链接,(如果二级name_type=6,加入),(如果二级name_type!=6,但是其三级有6的,加入)
	//fetchIpoEquityRelationship(&u_i_e)

	fetchIpoEquityRelationshipSecond(&u_i_e) //算二级
	fetchIpoEquityRelationshipThird(&u_i_e)  //算三级

	//for k, v := range u_i_e {
	//	a := util.RemoveZero(v.EquityRelationshipArray2)
	//}

	for k, v := range u_i_e {
		newSlice := make([]spider.EquityRelationshipArrayStruct, 0)
		for kk, vv := range v.EquityRelationshipArray2 {
			if !util.IsEmpty(vv.Name) || !util.IsEmpty(vv.Link) {
				//u_i_e[k].EquityRelationshipArray2 = append(u_i_e[k].EquityRelationshipArray2[0:1])
				newSlice = append(u_i_e[k].EquityRelationshipArray2[kk : kk+1])
			}
		}
		u_i_e[k].EquityRelationshipArray2 = newSlice
	}

	//fetchIpoEquityRelationship(&u_i_e)
	//handleEquityRelationshipData(&u_i_e)
	return u_i_e, count, nil
}

func fetchIpoEquityRelationshipSecond(u_i_e *[]spider.UIopEquity) {
	for k, v := range *u_i_e {
		//查询它下面所有的二级
		var secondData []spider.Data

		models.DB().Table("u_iop_equity_relationship").Where("iop_equity_id = ?", v.ID).Select("id,parent_id,name,link,name_type").Find(&secondData)

		if len(secondData) == 0 {
			continue
		} else {
			//把所有二级存入
			for _, vv := range secondData {
				var secondDataStruct spider.EquityRelationshipArrayStruct
				secondDataStruct.Id = vv.Id
				secondDataStruct.Link = vv.Link
				secondDataStruct.Name = vv.Name
				secondDataStruct.NameType = vv.NameType
				secondDataStruct.ParentLink = ""
				secondDataStruct.ParentId = vv.ParentId
				if vv.NameType != 6 {
					//不是上市公司,不展示名字,链接展不展示看其三级
					secondDataStruct.Name = ""
				}
				(*u_i_e)[k].EquityRelationshipArray2 = append((*u_i_e)[k].EquityRelationshipArray2, secondDataStruct)
			}
		}
	}
}

func fetchIpoEquityRelationshipThird(u_i_e *[]spider.UIopEquity) {
	for k, v := range *u_i_e {
		for kk, vv := range (*u_i_e)[k].EquityRelationshipArray2 {
			var thirdData []spider.Data
			var emptyStruct spider.EquityRelationshipArrayStruct
			var count int
			models.DB().Table("u_iop_equity_relationship").Where("parent_id= ?", vv.Id).Where("name_type = ?", 6).Select("id,parent_id,name,link,name_type").Count(&count).Find(&thirdData)
			if count == 0 {
				//二级下面无三级,判断二级是否为上市公司,是：去掉其链接，不是则去掉该二级
				if vv.NameType == 6 {
					//二级是上市公司,上市公司没有儿子
					(*u_i_e)[k].EquityRelationshipArray2[kk].Link = ""
				} else {
					//二级不是上市公司,并且没有儿子,去掉该二级
					(*u_i_e)[k].EquityRelationshipArray2[kk] = emptyStruct
					if (len((*u_i_e)[k].EquityRelationshipArray2))-1 == kk {
						//最后一个元素
						(*u_i_e)[k].EquityRelationshipArray2 = append((*u_i_e)[k].EquityRelationshipArray2[0:kk])
					} else {
						(*u_i_e)[k].EquityRelationshipArray2 = append((*u_i_e)[k].EquityRelationshipArray2[0:kk], (*u_i_e)[k].EquityRelationshipArray2[kk:]...)
					}
				}
			} else {
				//二级下面有三级

				for _, vvv := range thirdData {
					var thirdDataStruct spider.EquityRelationshipArrayStruct
					thirdDataStruct.Id = vvv.Id
					thirdDataStruct.Link = vvv.Link //自身链接
					thirdDataStruct.Name = vvv.Name
					thirdDataStruct.NameType = vvv.NameType

					thirdDataStruct.ParentLink = v.EquityLink //一级链接

					thirdDataStruct.ParentId = vvv.ParentId
					if vvv.NameType == 6 && vv.NameType != 6 {
						//三级为上市公司,其所属二级不是上市公司,去掉二级链接
						(*u_i_e)[k].EquityRelationshipArray2[kk].Link = ""
					}
					(*u_i_e)[k].EquityRelationshipArray3 = append((*u_i_e)[k].EquityRelationshipArray3, thirdDataStruct)
				}

			}
		}
	}
}

//func fetchIpoEquityRelationship(u_i_e *[]spider.UIopEquity) {
//	//遍历每一个1级
//	for k, v := range *u_i_e {
//		//查询它下面所有的二级
//		var secondData []spider.Data
//		models.DB().Table("u_iop_equity_relationship").Where("iop_equity_id = ?", v.ID).Select("id,parent_id,name,link,name_type").Find(&secondData)
//		if len(secondData) == 0 {
//			continue
//		}
//		//遍历每一个二级,查到其所属的三级,之后如果这个二级下面有符合条件的三级,就把二级的数据
//		for _, vv := range secondData {
//			//将二级名称加入
//			(*u_i_e)[k].EquityRelationship[vv.Name] = ""
//			var thirdData []spider.Data
//			var count int
//			models.DB().Table("u_iop_equity_relationship").Where("parent_id= ?", vv.Id).Where("name_type = ?", 6).Select("id,parent_id,name,link,name_type").Count(&count).Find(&thirdData)
//			if count != 0 {
//				for _, vvv := range thirdData {
//					if !util.IsEmpty(vvv.Name) && !util.IsEmpty(vvv.Link) {
//						//将三级的公司名加入进去
//						(*u_i_e)[k].EquityRelationship[vvv.Name] = ""
//						//将该三级所属二级的链接加进去
//						(*u_i_e)[k].EquityRelationship[vv.Name] = vv.Link
//					}
//				}
//			} else {
//				//二级下面没有三级,并且 二级 的name_type不是6，去掉
//				if vv.NameType != "6" {
//					delete((*u_i_e)[k].EquityRelationship, vv.Name)
//				} else {
//					//是上市公司,没有三级,将顶级图加入
//					//将二级的链接加入
//					//(*u_i_e)[k].EquityRelationship[vv.Name] = vv.Link
//					//(*u_i_e)[k].EquityRelationship[vv.Name] = v.EquityLink
//				}
//			}
//		}
//	}
//}

//处理返回数据格式
//func handleEquityRelationshipData(u_i_e *[]spider.UIopEquity) {
//	for i := 0; i < len(*u_i_e); i++ {
//		for k, v := range (*u_i_e)[i].EquityRelationship {
//			var data spider.EquityRelationshipArrayStruct
//			data.Name = k
//			data.Link = v
//			(*u_i_e)[i].EquityRelationshipArray = append((*u_i_e)[i].EquityRelationshipArray, data)
//		}
//	}
//}

// -----------------------------------------new-----------------------------------
type UIpoEquity struct {
	models.Simple
	Company                 string                          `gorm:"column:company;type:varchar(255)" json:"company"`                                   // 公司名称
	DeclareTime             string                          `gorm:"column:declare_time;type:date" json:"declare_time"`                                 // 上会日期
	EquityLink              string                          `gorm:"column:equity_link;type:varchar(255)" json:"equity_link"`                           // 股权关系链接
	QuantityToBeIssued      string                          `gorm:"column:quantity_to_be_issued" json:"quantity_to_be_issued"`                         // 拟发行数量
	ProposedListingAddress  string                          `gorm:"column:proposed_listing_address;type:varchar(255)" json:"proposed_listing_address"` // 拟上市地址
	EquityRelationshipArray []EquityRelationshipArrayStruct `json:"equity_relationship_array"`                                                         //参股公司
	StockRatio              string                          `json:"pctg"`                                                                              //参股比例
}

func (UIpoEquity) TableName() string {
	return "u_iop_equity"
}

//id,parent_id,name,link,name_type
type EquityRelationshipArrayStruct struct {
	models.Simple
	ParentId   int    `json:"-"`
	Name       string `json:"name"`
	Link       string `json:"link"`
	NameType   int    `json:"-"`
	ParentLink string `json:"parent_link"`
}

func (EquityRelationshipArrayStruct) TableName() string {
	return "u_iop_equity_relationship"
}

func QueryIPO(pageID, pageSize int) ([]UIpoEquity, int, error) {
	var data []UIpoEquity
	var cnt int
	db := models.DB()
	if err := db.Model(&UIpoEquity{}).Count(&cnt).Scopes(PageAndPageSize(pageID, pageSize)).Order("declare_time DESC").Find(&data).Error; err != nil {
		logging.Error("ipo.QueryIPO() Query UIpoEquity Errors: ", err.Error())
		return data, 0, err
	}
	for i := 0; i < len(data); i++ {
		u := &data[i]
		appendChild(u)
	}

	return data, cnt, nil
}

func appendChild(u *UIpoEquity) {
	childAll := queryChild(u.ID, 1, false)
	childShow := queryChild(u.ID, 1, true)
	u.EquityRelationshipArray = append(u.EquityRelationshipArray, childShow...)
	for _, v := range childAll {
		child := queryChild(v.ID, 2, true)
		for _, w := range child {
			w.ParentLink = v.Link
			u.EquityRelationshipArray = append(u.EquityRelationshipArray, w)
		}
	}
}

func queryChild(id, level int, bShow bool) []EquityRelationshipArrayStruct {
	var data []EquityRelationshipArrayStruct
	db := models.DB().Model(&EquityRelationshipArrayStruct{})
	switch {
	case level == 1:
		db = db.Where("iop_equity_id = ?", id)
	case level > 1:
		db = db.Where("iop_equity_id = 0").Where("parent_id = ?", id)
	default:
		logging.Error("ipo.queryChild() Invalid level: ", level)
		return data
	}
	if bShow {
		db = db.Where("name_type = ?", 6)
	}
	if err := db.Find(&data).Error; err != nil {
		logging.Error("ipo.queryChild() Query EquityRelationshipArrayStruct Errors: ", err.Error())
		return data
	}
	return data
}

func QueryIPOByID(id int) *UIpoEquity {
	var data UIpoEquity
	db := models.DB()
	if err := db.Model(&UIpoEquity{}).Where("id = ?", id).First(&data).Error; err != nil {
		logging.Error("template.QueryIPOByID() Query UIpoEquity Errors: ", err.Error())
		return nil
	}
	appendChild(&data)
	return &data
}

func queryCompany(name string) string {
	var company struct {
		StockName string
		Name      string
	}
	if err := models.DB().Table("p_public_company").Where("name = ?", name).First(&company).Error; err != nil {
		logging.Error("template.QueryCompany() Query p_public_company Errors: ", err.Error())
		return name
	}
	var ret string
	if company.StockName == "" {
		ret = name
	} else {
		ret = company.StockName
	}
	return ret
}

func AssembleIPO(data *UIpoEquity) *UIpoEquity {
	if data == nil {
		return data
	}
	data.Company = queryCompany(data.Company)
	for i := 0; i < len(data.EquityRelationshipArray); i++ {
		data.EquityRelationshipArray[i].Name = queryCompany(data.EquityRelationshipArray[i].Name)
	}
	return data
}




