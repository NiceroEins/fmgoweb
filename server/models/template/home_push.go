package template

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"sync"
)

type PushResponse struct {
	ID                 int        `json:"id" gorm:"column:id"`
	UserID             int        `json:"user_id" gorm:"column:user_id"`               // 发布者用户id
	Code               string     `json:"code"    gorm:"column:code"`                  // 股票内容
	CodeName           string     `json:"code_name" gorm:"column:code_name"`           // 股票代码名称
	UserName           string     `json:"user_name" gorm:"column:user_name"`           // 用户名称
	EventStr           string     `json:"event_str" gorm:"column:event_str"`           // 点评内容
	PictureLink        string     `json:"-" gorm:"column:picture_link"`                // 图片链接
	Updated            string     `json:"updated_at" gorm:"column:updated_at"`         // 更新时间
	PictureLinkArr     []ImgDTO   `json:"picture_link"`
	CreatedTime        string     `json:"spider_time" gorm:"column:created_time"`
	ObjectType         string     `json:"object_type" gorm:"object_type"`
	ObjectId           int        `json:"object_id" gorm:"object_id"`
	Industry           string     `json:"industry"`
}
type ImgDTO struct {
	Url  string `json:"url"`
	Type string `json:"type"`
}
type PushAnnouncementResponse struct {
	PushResponse
	Title       string     `json:"title"    gorm:"column:title"`                         // 股票内容
	SourceLink  string     `json:"source_link"    gorm:"column:source_link"`            // 公告链接

}

type PushReportResponse struct {
	PushResponse
	Organization       string     `json:"organization" gorm:"column:organization"`                  // 机构名称
	Author             string     `json:"author" gorm:"column:author"`                              // 作者
	Title              string     `json:"title" gorm:"column:title"`                                // 评级
	Level              string     `json:"level" gorm:"column:level"`                                // 观点
	SourceLink         string     `json:"source_link"  gorm:"column:source_link"`                   // 研报链接
}

func GetStockPushLists(data map[string]interface{}) ([]*PushResponse, error) {
	var (
		pushResponse []*PushResponse
		page_num   int
		page_size  int
	)
	dbs := models.DB().Table("u_user_event").
		Select("u_user_event.*,b_user.realname as user_name,u_user_stock.code,u_user_stock.user_id,b_stock.name as code_name,u_user_stock.created_at as created_time").
		Joins("LEFT JOIN b_user on u_user_event.user_id = b_user.id ").
		Joins("LEFT JOIN u_user_stock on u_user_stock.id = u_user_event.object_id ").
	    Joins("LEFT JOIN b_stock on b_stock.id = u_user_stock.code ")
	dbs = getWheres(data,dbs)
	if data["code_name"].(string) != "" {
		dbs =  dbs.Where("b_stock.name like ? or b_stock.id = ? ","%"+ data["code_name"].(string) + "%",data["code_name"])
	}
	if data["page_num"].(int) > 0 {
		page_num = data["page_num"].(int)
	}
	if data["page_size"].(int) > 0 {
		page_size = data["page_size"].(int)
	}
	err := dbs.Where("u_user_event.object_type in (?)",[]string{"stock_pool","personal_stock","industry_monitor"}).
		Order("u_user_event.updated_at desc").
		Limit(page_size).Offset((page_num - 1)*page_size).Find(&pushResponse).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	for index,value:=range pushResponse {
		if value.PictureLink == "" {
			pushResponse[index].PictureLinkArr = []ImgDTO{}
		}else {
			pictureArr := []ImgDTO{}
			_ = json.Unmarshal([]byte(value.PictureLink), &pictureArr)
			pushResponse[index].PictureLinkArr = pictureArr
		}
		if value.ObjectType == "industry_monitor" {
			// 获取行业名称
			pushResponse[index].Industry = GetIndustryNameById(value.ObjectId)
			pushResponse[index].Code = ""
			pushResponse[index].CodeName = ""
		}
	}
	return pushResponse, nil
}
func GetStockPushListsCount(data map[string]interface{}) (int, error) {
	var (
		count      int
	)
	dbs := models.DB().Table("u_user_event").
		Select("u_user_event.*,b_user.realname as user_name,u_user_stock.code,u_user_stock.user_id,b_stock.name as code_name").
		Joins("LEFT JOIN b_user on u_user_event.user_id = b_user.id ").
		Joins("LEFT JOIN u_user_stock on u_user_stock.id = u_user_event.object_id ").
		Joins("LEFT JOIN b_stock on b_stock.id = u_user_stock.code ")
	dbs = getWheres(data,dbs)
	if data["code_name"].(string) != "" {
		dbs =  dbs.Where("b_stock.name like ? or b_stock.id = ? ","%"+ data["code_name"].(string) + "%",data["code_name"])
	}
	err := dbs.Where("u_user_event.object_type in (?)",[]string{"stock_pool","personal_stock","industry_monitor"}).
		Order("u_user_event.updated_at desc").Count(&count).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return 0, err
	}
	return count,err
}
func GetAnnouncementPushLists(data map[string]interface{}) ([]*PushAnnouncementResponse, error) {
	var (
		pushResponse []*PushAnnouncementResponse
		page_num     int
		page_size    int
	)
	if data["page_num"].(int) > 0 {
		page_num = data["page_num"].(int)
	}
	if data["page_size"].(int) > 0 {
		page_size = data["page_size"].(int)
	}
	aSubSql := fmt.Sprintf("select u_user_event.*,b_user.realname as user_name,u_cninfo_notice.code,u_cninfo_notice.name as code_name,u_cninfo_notice.title,u_cninfo_notice.source_link,u_cninfo_notice.created_at as created_time from u_user_event "+
		" LEFT JOIN b_user on u_user_event.user_id = b_user.id"+
		" LEFT JOIN u_cninfo_notice on u_cninfo_notice.id = u_user_event.object_id"+
		" where u_user_event.object_type='%s'", "announcement")
	iSubSql := fmt.Sprintf("select u_user_event.*,b_user.realname as user_name,p_investigation.code,p_investigation.name as code_name,p_investigation.title,p_investigation.source_link,p_investigation.created_at as created_time from u_user_event "+
		" LEFT JOIN b_user on u_user_event.user_id = b_user.id"+
		" LEFT JOIN p_investigation on p_investigation.id = u_user_event.object_id"+
		" where u_user_event.object_type='%s'", "investigation")
	if data["code_name"].(string) != "" {
		aSubSql = aSubSql + fmt.Sprintf(" and (u_cninfo_notice.name like '%s' or u_cninfo_notice.code like '%s' )", "%"+data["code_name"].(string)+"%", "%"+data["code_name"].(string)+"%")
		iSubSql = iSubSql + fmt.Sprintf(" and (p_investigation.name like '%s' or p_investigation.code like '%s' )", "%"+data["code_name"].(string)+"%", "%"+data["code_name"].(string)+"%")
	}
	if data["user_id"].(int) > 0 {
		aSubSql = aSubSql + fmt.Sprintf(" and u_user_event.user_id = %d", data["user_id"])
		iSubSql = iSubSql + fmt.Sprintf(" and u_user_event.user_id = %d", data["user_id"])
	}
	if data["start"].(string) != "" {
		aSubSql = aSubSql + fmt.Sprintf(" and u_user_event.updated_at >= '%s'", data["start"].(string)+" 00:00:00")
		iSubSql = iSubSql + fmt.Sprintf(" and u_user_event.updated_at >= '%s'", data["start"].(string)+" 00:00:00")
	}
	if data["end"].(string) != "" {
		aSubSql = aSubSql + fmt.Sprintf(" and u_user_event.updated_at <= '%s'", data["end"].(string)+" 23:59:59")
		iSubSql = iSubSql + fmt.Sprintf(" and u_user_event.updated_at <= '%s'", data["end"].(string)+" 23:59:59")
	}
	if data["search_type"].(string) == "recommend" {
		aSubSql = aSubSql + fmt.Sprintf(" and u_user_event.event_type = '%s'", data["search_type"])
		iSubSql = iSubSql + fmt.Sprintf(" and u_user_event.event_type = '%s'", data["search_type"])
	} else {
		aSubSql = aSubSql + fmt.Sprintf(" and (u_user_event.event_str != '%s' or u_user_event.picture_link != '%s')", "", "[]")
		iSubSql = iSubSql + fmt.Sprintf(" and (u_user_event.event_str != '%s' or u_user_event.picture_link != '%s')", "", "[]")
	}
	subSql := aSubSql + " UNION ALL " + iSubSql + fmt.Sprintf(" order by updated_at desc limit %d offset %d", page_size, (page_num-1)*page_size)
	err := models.DB().Raw(subSql).Find(&pushResponse).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	for index, value := range pushResponse {
		if value.PictureLink == "" {
			pushResponse[index].PictureLinkArr = []ImgDTO{}
		} else {
			pictureArr := []ImgDTO{}
			_ = json.Unmarshal([]byte(value.PictureLink), &pictureArr)
			pushResponse[index].PictureLinkArr = pictureArr
		}
	}
	return pushResponse, nil
}
func GetAnnouncementPushListsCount(data map[string]interface{}) (int, error) {
	var (
		count int
	)
	aSubSql := fmt.Sprintf("select u_user_event.*,b_user.realname as user_name,u_cninfo_notice.code,u_cninfo_notice.name as code_name,u_cninfo_notice.title,u_cninfo_notice.source_link,u_cninfo_notice.created_at as created_time from u_user_event "+
		" LEFT JOIN b_user on u_user_event.user_id = b_user.id"+
		" LEFT JOIN u_cninfo_notice on u_cninfo_notice.id = u_user_event.object_id"+
		" where u_user_event.object_type='%s'", "announcement")
	iSubSql := fmt.Sprintf("select u_user_event.*,b_user.realname as user_name,p_investigation.code,p_investigation.name as code_name,p_investigation.title,p_investigation.source_link,p_investigation.created_at as created_time from u_user_event "+
		" LEFT JOIN b_user on u_user_event.user_id = b_user.id"+
		" LEFT JOIN p_investigation on p_investigation.id = u_user_event.object_id"+
		" where u_user_event.object_type='%s'", "investigation")
	if data["code_name"].(string) != "" {
		aSubSql = aSubSql + fmt.Sprintf(" and (u_cninfo_notice.name like '%s' or u_cninfo_notice.code like '%s' )", "%"+data["code_name"].(string)+"%", "%"+data["code_name"].(string)+"%")
		iSubSql = iSubSql + fmt.Sprintf(" and (p_investigation.name like '%s' or p_investigation.code like '%s' )", "%"+data["code_name"].(string)+"%", "%"+data["code_name"].(string)+"%")
	}
	if data["user_id"].(int) > 0 {
		aSubSql = aSubSql + fmt.Sprintf(" and u_user_event.user_id = %d", data["user_id"])
		iSubSql = iSubSql + fmt.Sprintf(" and u_user_event.user_id = %d", data["user_id"])
	}
	if data["start"].(string) != "" {
		aSubSql = aSubSql + fmt.Sprintf(" and u_user_event.updated_at >= '%s'", data["start"].(string)+" 00:00:00")
		iSubSql = iSubSql + fmt.Sprintf(" and u_user_event.updated_at >= '%s'", data["start"].(string)+" 00:00:00")
	}
	if data["end"].(string) != "" {
		aSubSql = aSubSql + fmt.Sprintf(" and u_user_event.updated_at <= '%s'", data["end"].(string)+" 23:59:59")
		iSubSql = iSubSql + fmt.Sprintf(" and u_user_event.updated_at <= '%s'", data["end"].(string)+" 23:59:59")
	}
	if data["search_type"].(string) == "recommend" {
		aSubSql = aSubSql + fmt.Sprintf(" and u_user_event.event_type = '%s'", data["search_type"])
		iSubSql = iSubSql + fmt.Sprintf(" and u_user_event.event_type = '%s'", data["search_type"])
	} else {
		aSubSql = aSubSql + fmt.Sprintf(" and (u_user_event.event_str != '%s' or u_user_event.picture_link != '%s')", "", "[]")
		iSubSql = iSubSql + fmt.Sprintf(" and (u_user_event.event_str != '%s' or u_user_event.picture_link != '%s')", "", "[]")
	}
	subSql := "select count(id) from (" + aSubSql + " UNION ALL " + iSubSql + ") counttable"
	err := models.DB().Raw(subSql).Count(&count).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return 0, err
	}
	return count, nil
}
func GetReportPushLists(data map[string]interface{}) ([]*PushReportResponse, error) {
	var (
		pushResponse []*PushReportResponse
		page_num   int
		page_size  int
	)
	dbs := models.DB().Table("u_user_event").
		Select("u_user_event.*,b_user.realname as user_name,u_research_report.code," +
			"u_research_report.name as code_name,u_research_report.title,u_research_report.organization,u_research_report.author,u_research_report.level,u_research_report.source_link,u_research_report.created_at as created_time").
		Joins("LEFT JOIN b_user on u_user_event.user_id = b_user.id ").
		Joins("LEFT JOIN u_research_report on u_research_report.id = u_user_event.object_id ")
	dbs = getWheres(data,dbs)
	if data["code_name"].(string) != "" {
		dbs =  dbs.Where("u_research_report.name like ? or u_research_report.code = ? ","%"+ data["code_name"].(string) + "%",data["code_name"])
	}
	if data["page_num"].(int) > 0 {
		page_num = data["page_num"].(int)
	}
	if data["page_size"].(int) > 0 {
		page_size = data["page_size"].(int)
	}
	err := dbs.Where("u_user_event.object_type = ?","research").Order("u_user_event.updated_at desc").
		Limit(page_size).Offset((page_num - 1)*page_size).Find(&pushResponse).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	for index,value:=range pushResponse {
		if value.PictureLink == "" {
			pushResponse[index].PictureLinkArr = []ImgDTO{}
		}else {
			pictureArr := []ImgDTO{}
			_ = json.Unmarshal([]byte(value.PictureLink), &pictureArr)
			pushResponse[index].PictureLinkArr = pictureArr
		}
	}
	return pushResponse, nil
}
func GetReportPushListsCount(data map[string]interface{}) (int, error) {
	var (
		count      int
	)
	dbs := models.DB().Table("u_user_event").
		Select("u_user_event.*,b_user.realname as user_name,u_research_report.code," +
			"u_research_report.name as code_name,u_research_report.title,u_research_report.organization,u_research_report.author,u_research_report.level,u_research_report.source_link").
		Joins("LEFT JOIN b_user on u_user_event.user_id = b_user.id ").
		Joins("LEFT JOIN u_research_report on u_research_report.id = u_user_event.object_id ")
	dbs = getWheres(data,dbs)
	if data["code_name"].(string) != "" {
		dbs =  dbs.Where("u_research_report.name like ? or u_research_report.code = ? ","%"+ data["code_name"].(string) + "%",data["code_name"])
	}
	err := dbs.Where("u_user_event.object_type = ?","research").Order("u_user_event.updated_at desc").Count(&count).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return 0, err
	}
	return count, nil
}
func getWheres(data map[string]interface{},dbs *gorm.DB) *gorm.DB {
	if data["user_id"].(int) > 0 {
		dbs =  dbs.Where("u_user_event.user_id = ?",data["user_id"])
	}
	if data["start"].(string) != "" {
		dbs =  dbs.Where("u_user_event.updated_at >=  ?",data["start"].(string) + " 00:00:00")
	}
	if data["end"].(string) != "" {
		dbs =  dbs.Where("u_user_event.updated_at <=  ?",data["end"].(string) + " 23:59:59")
	}
	if data["search_type"].(string) == "recommend" {
		dbs =  dbs.Where("u_user_event.event_type = ? ",data["search_type"])
	}else {
		dbs =  dbs.Where("u_user_event.event_str != ? or u_user_event.picture_link != ? ","","[]")
	}
	return dbs
}

type IndustryNamesUser struct {
	models.Simple
	UserID             int        `json:"user_id" gorm:"column:user_id"`               // 发布者用户id
	UserName           string     `json:"user_name" gorm:"column:user_name"`           // 用户名称
}
type SecondIndustryName struct {
	FirstIndustry      string     `json:"first_industry"`       // 一级行业
	SecondIndustry     string     `json:"second_industry"`     // 二级行业
	IndustryId         int        `json:"industry_id"`          // 行业id
}
type IndustryResponse struct {
	IndustryNamesUser
	SecondIndustryName []SecondIndustryName `json:"second_industry"`
}
type IndustryNamesAdd struct {
	models.Simple
	UserID             int        `json:"user_id" gorm:"column:user_id"`                     // 发布者用户id
	UserName           string     `json:"user_name" gorm:"column:user_name"`                 // 用户名称
	FirstIndustry      string     `json:"first_industry" gorm:"column:first_industry"`       // 一级行业
	SecondIndustry     string     `json:"second_industry" gorm:"column:second_industry"`     // 二级行业
	IndustryId         int        `json:"industry_id" gorm:"column:industry_id"`             // 行业id
}
func(IndustryNamesUser) TableName() string {
	return "u_second_industry_user"
}
func GetIndustryNamesLists() ([]IndustryResponse, error) {
	var (
		industyNames []IndustryNamesAdd
		responseIndustry IndustryResponse
		secondIndustry SecondIndustryName
	)
	err := models.DB().Table("u_second_industry_user").Find(&industyNames).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	industry := []IndustryResponse{}
	userSecondIndustry := make(map[int][]SecondIndustryName)
	users := make(map[int]string)
	for _,value := range industyNames {
		secondIndustry.FirstIndustry = value.FirstIndustry
		secondIndustry.SecondIndustry = value.SecondIndustry
		secondIndustry.IndustryId = value.IndustryId
		userSecondIndustry[value.UserID] = append(userSecondIndustry[value.UserID],secondIndustry)
		users[value.UserID] = value.UserName
	}
	for index,value := range userSecondIndustry {
		responseIndustry.UserName = users[index]
		responseIndustry.UserID = index
		responseIndustry.SecondIndustryName = value
		industry = append(industry,responseIndustry)
	}
	return industry, nil
}
func AddIndustyNames(user IndustryNamesAdd) error {
	if err := models.DB().Table("u_second_industry_user").Create(&user).Error; err != nil {
		return err
	}

	return nil
}
func DelIndustyNames(userID int) error {
	if err := models.DB().Where("user_id = ?", userID).Delete(IndustryNamesUser{}).Error; err != nil {
		return err
	}
	return nil
}
func GetIndustryNames(name string) (IndustryNamesUser,error) {
	var industyNames IndustryNamesUser
	err := models.DB().Table("u_second_industry_user").
		Where("second_industry = ? ",name).
		Where("deleted_at is null  ").
		Order("id desc").
		First(&industyNames).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return industyNames, err
	}
	return industyNames, nil
}

// 工作统计
type Staticstic struct {
	UserID         int           `json:"user_id"`
	AnnounceDeal   int           `json:"announce_deal"`
	AnnounceWait   int           `json:"announce_wait"`
	ReportDeal     int           `json:"report_deal"`
	ReportWait     int           `json:"report_wait"`
	UserName       string		 `json:"realname"`
	LastDealTime   string 		 `json:"last_deal_time"`
}

func GetWorkStaticstic(data map[string]interface{}) []Staticstic {
	var (
		industyNames []*IndustryNamesUser
		staticstic   []Staticstic
		err error
	)
	dbs  := models.DB().Table("u_industry_user").Select("GROUP_CONCAT(actual_industry) as actual_industry,u_industry_user.*")
	if data["user_id"].(int) > 0 {
		dbs = dbs.Where("user_id = ? ",data["user_id"])
	}
	err = dbs.Group("user_id").Limit(data["page_size"]).
		Offset(data["page_size"].(int) * (data["page_num"].(int) - 1) ).Order("id desc").Find(&industyNames).Error
	if err != nil {
		logging.Info("GetWorkStaticstic err is ",err)
		return nil
	}
	wg := sync.WaitGroup{}
	wg.Add(len(industyNames))
	for index,_ := range industyNames {
		go func(idx int) {
			staticstic= append(staticstic,genWorkStaicsData(*industyNames[idx],data["start"].(string),data["end"].(string)))
			wg.Done()
		}(index)
	}
	wg.Wait()
	return staticstic
}

func genWorkStaicsData(industry IndustryNamesUser,searchDateFrom,searchDateEnd string) Staticstic {
	sta := &Staticstic{}
	// 公告
	industryNames := GetUserIndustry(industry.UserID)
	secondIndustry := GetUserSecondIndustry(industry.UserID)
	sta.AnnounceDeal,sta.AnnounceWait = GetUserEventCount( searchDateFrom,"u_cninfo_notice","announcement",searchDateEnd,industryNames,secondIndustry,industry.UserID)
	// 研报
	sta.ReportDeal,sta.ReportWait = GetUserEventCount( searchDateFrom,"u_research_report","research",searchDateEnd,industryNames,secondIndustry,industry.UserID)
	// 获取最后处理时间
	maps := make(map[string]interface{})
	maps["user_id"] = industry.UserID
	maps["event_status"] = 1
	maps["updated_start"] = searchDateFrom
	maps["updated_end"] =  searchDateEnd
	userEvent := GetUserEventLastRecord(maps)
	lasttime := userEvent.UpdatedAt.Format("2006-01-02 15:04:05")
	if userEvent.UpdatedAt.IsZero() {
		lasttime = ""
	}
	sta.LastDealTime = lasttime
	sta.UserName = industry.UserName
	sta.UserID = industry.UserID
	return *sta
}

func GetStatiscticCount(data map[string]interface{}) int {
	var (
		count    int
		err error
	)
	dbs  := models.DB().Table("u_industry_user").Select("GROUP_CONCAT(actual_industry) as actual_industry,u_industry_user.*")
	if data["user_id"].(int) > 0 {
		dbs = dbs.Where("user_id = ? ",data["user_id"])
	}
	err = dbs.Group("user_id").Count(&count).Error
	if err != nil {
		logging.Info("GetStatiscticCount err is ",err)
		return count
	}
	return count
}

func GetIndustryNameById(id int) string {
	data := IndustryName{}
	err := models.DB().Table("u_industry_monitor").Select("second_industry as industry_name").Where("id = ? ",id).First(&data).Error
	if err != nil {
		logging.Info("get industry name by id err",err)
		return ""
	}
	return data.IndustryName
}