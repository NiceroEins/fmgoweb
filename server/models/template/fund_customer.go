package template

import (
    "datacenter/models"
    "datacenter/pkg/logging"
    "github.com/jinzhu/gorm"
    "github.com/shopspring/decimal"
    "time"
)

type FundCustomerResponse struct {
    FundCustomer
    Realname             string     `json:"realname" gorm:"column:realname"`                 // 姓名
    FundName             string     `json:"fund_name" gorm:"column:fund_name"`               // 基金名称
}

type FundCustomer struct {
    FundCustomerAdd
    ID                   int             `json:"id" gorm:"column:id" `                            // 主键ID
}

type FundCustomerAdd struct {
    UserID               int             `json:"user_id" gorm:"column:owner_id" `                 // 用户ID
    FundID               int             `json:"fund_id" gorm:"column:fund_id"`                   // 基金id
    Created              string          `json:"-" gorm:"column:created"`                         // 创建时间
    Num                  string          `json:"num" gorm:"column:num"`                           // 份额
    Updated              string          `json:"updated" gorm:"column:updated"`                   // 更新时间
    Deleted              int             `json:"-" gorm:"column:deleted"`
}

type FundCustomerList struct {
    FundID               int                      `json:"fund_id" gorm:"column:fund_id"`                   // 基金id
    Updated              string                   `json:"updated" gorm:"column:updated"`                   // 更新时间
    FundName             string                   `json:"fund_name" gorm:"column:fund_name"`               // 基金名称
    Num                  decimal.Decimal          `json:"num" gorm:"column:num"`                           // 份额
    UnitValue            decimal.Decimal          `json:"unit_value"`                                      // 单位净值
}

type FundCustomerImport struct {
    UserID               int
    FundID               int
    Num                  decimal.Decimal
}
func(FundCustomer) TableName() string {
    return "p_customer_fund"
}

func GetFundCustomers(pageNum int, pageSize int,data interface{},realname string) ([]FundCustomerResponse, error) {
    var (
        fundCustomers []FundCustomerResponse
        err  error
    )
    if pageSize > 0 && pageNum >= 0 {
        err = models.DB().Select("p_customer_fund.*,p_customer.realname,p_fund.name as fund_name,p_customer.id_card_no").
            Joins("LEFT JOIN p_customer ON p_customer.id = p_customer_fund.owner_id").
            Joins("LEFT JOIN p_fund ON p_fund.id = p_customer_fund.fund_id").Where(data).
            Where("realname like ?", "%"+realname+"%").
            Where("p_customer.deleted = 0").
            Where("p_fund.deleted = 0").
            Where("p_customer_fund.deleted = 0").
            Order("p_customer_fund.updated desc").Offset((pageNum - 1)*pageSize).Limit(pageSize).Find(&fundCustomers).Error
    } else {
        err = models.DB().Select("p_customer_fund.*,p_customer.realname,p_fund.name as fund_name,p_customer.id_card_no").
            Joins("LEFT JOIN p_customer ON p_customer.id = p_customer_fund.owner_id").
            Joins("LEFT JOIN p_fund ON p_fund.id = p_customer_fund.fund_id").
            Where("realname like ?", "%"+realname+"%").
            Where("p_customer.deleted = 0").
            Where("p_fund.deleted = 0").
            Where("p_customer_fund.deleted = 0").
            Find(&fundCustomers).Error
    }

    if err != nil && err != gorm.ErrRecordNotFound {
        return nil, err
    }

    return fundCustomers, nil
}

func AddFundCustomer(customer FundCustomerAdd) error {
    if err := models.DB().Table("p_customer_fund").Create(&customer).Error; err != nil {
        return err
    }

    return nil
}

func GetFundCustomerTotal(data interface{},realname string) (int, error) {
    var count int
    if err := models.DB().Model(&FundCustomer{}).Where(data).
        Joins("LEFT JOIN p_customer ON p_customer.id = p_customer_fund.owner_id").
        Joins("LEFT JOIN p_fund ON p_fund.id = p_customer_fund.fund_id").
        Where("p_customer.deleted = 0").
        Where("p_fund.deleted = 0").
        Where("p_customer_fund.deleted = 0").
        Where("realname like ?", "%"+realname+"%").Count(&count).Error; err != nil {
        return 0, err
    }
    return count, nil
}

func EditFundCustomer(id int, data interface{}) error {
    if err := models.DB().Model(&FundCustomer{}).Where("id = ? ",id).Updates(data).Error; err != nil {
        return err
    }
    return nil
}

func ExistFundCustomer(data interface{}) (bool, error) {
    var customer FundCustomer
    err := models.DB().Select("id").Where("p_customer_fund.deleted = 0").Where(data).First(&customer).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return false, err
    }

    if customer.ID > 0 {
        return true, nil
    }

    return false, nil
}

func DeleteFundCustomer(id int) error {
    if err := models.DB().Where("id = ? ",id).Update(map[string]interface{}{"deleted":1}).Error; err != nil {
        return err
    }
    return nil
}

func GetFundIdByName(name string) (int,error) {
    var fundId struct{
        FundID int `json:"fund_id" gorm:"column:fund_id"`
    }
    err := models.DB().Table("p_fund").Where("p_fund.deleted = 0").Select("id as fund_id").Where("name = ? ",name).
        First(&fundId).Error
    if err != nil{
        return 0, err
    }
    return fundId.FundID, nil
}

func GetCustomerIdByName(name string) (int,error) {
    var User struct{
        ID int  `json:"id" gorm:"column:id"`
    }
    err := models.DB().Table("p_customer").Where("p_customer.deleted = 0").Select("id").Where("realname = ? ",name).
        First(&User).Error
    if err != nil{
        return 0, err
    }
    return User.ID, nil
}

func CheckFundCostmer(fundId,userId int) (bool,error) {
    var num int
    err := models.DB().Table("p_customer_fund").Where("p_customer_fund.deleted = 0").Select("id").Where("fund_id = ? ",fundId).
    Where("owner_id = ? ",userId).Count(&num).Error
    if err != nil{
        return false, err
    }
    return num > 1, nil
}

func FindFundCostmerByName(user_id,fund_id int ) (FundCustomer,error) {
    var customer FundCustomer
    if err := models.DB().Table("p_customer_fund").Where("owner_id = ? ",user_id).
        Where("p_customer_fund.deleted = 0").
        Where("fund_id = ? ",fund_id).Find(&customer).Error; err != nil && err != gorm.ErrRecordNotFound  {
        return customer,err
    }
    if customer.ID > 0{
        return customer,nil
    }
    return customer,nil
}

func GetCustomerCountByName(name string) (int,error){
    var count int
    if err := models.DB().Table("p_customer").Where("p_customer.deleted = 0").Where("realname = ? ",name).Count(&count).Error; err != nil {
        return 0, err
    }
    return count, nil
}

func GetCustomerByNameAndOwnerId(name string,id string) (int,error) {
    var User struct{
        ID int
    }
    err := models.DB().Table("p_customer").Where("p_customer.deleted = 0").Select("id as user_id").
        Where("realname = ? ",name).Where("id = ? ",id).
        First(&User).Error
    if err != nil{
        return 0, err
    }
    return User.ID, nil
}

func GetCustomerFundList(userID,fundID int)  ([]FundCustomerList, string,error)  {
    var (
        fundCustomers []FundCustomerList
        err  error
        regiseteDate time.Time
    )
    dbs := models.DB().Table("p_customer_fund").Select("p_customer_fund.fund_id,p_customer.id,p_customer.realname,p_fund.name as fund_name,p_customer_fund.num").
        Joins("LEFT JOIN p_customer ON p_customer.id = p_customer_fund.owner_id").
        Joins("LEFT JOIN p_fund ON p_fund.id = p_customer_fund.fund_id").
        Where("p_customer.deleted = 0").
        Where("p_fund.deleted = 0").
        Where("p_customer_fund.deleted = 0").
        Where("p_customer.id = ?", userID)
    if fundID > 0 {
        dbs = dbs.Where("p_customer_fund.fund_id = ? ",fundID)
    }
    err = dbs.Find(&fundCustomers).Error
    if err != nil {
        return nil, "",err
    }
    for index,fund := range fundCustomers {
        fundTick,err := GetLastRecordByFundId(fund.FundID)
        logging.Info("get last record fund err :",err)
        if err != nil {
            continue
        }
        // 获取注册时间
        if !regiseteDate.IsZero() {
            if fundTick.ValueDate.Sub(regiseteDate) > 0{
                regiseteDate = fundTick.Created
            }
        }else {
            regiseteDate = fundTick.Created
        }
        fundCustomers[index].UnitValue = fundTick.UnitValue
    }
    logging.Info("get last record fund err :",fundCustomers)
    ret := ""
    if !regiseteDate.IsZero() {
        ret = regiseteDate.Format("2006-01-02")
    }
    return fundCustomers, ret,nil
}
