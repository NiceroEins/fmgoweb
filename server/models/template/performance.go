package template

import (
	"datacenter/models"
	"datacenter/pkg/parser"
	"datacenter/pkg/util"
	"datacenter/service/performance_service"
	"datacenter/service/user_service"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/jinzhu/gorm"
	"io"
	"strconv"
	"strings"
	"time"
)

//type GetPerformanceMysql1 struct {
//	models.Base
//	//股票信息
//	Name string `json:"name"`
//	//股票代码
//	Code string `json:"code"`
//	//披露时间
//	ExpectedPublishDate string `json:"expected_publish_date"`
//
//	//预测比例(上下限)
//	ForecastProportionLower string `json:"forecast_proportion_lower"`
//	ForecastProportionUpper string `json:"forecast_proportion_upper"`
//
//	//预测净利润
//	ForecastNetProfit string `json:"forecast_net_profit"`
//
//	//亮点
//	HighLights string `json:"high_lights"`
//	//亮点截取100个字,后面 + ...
//	HighLightsSub100 string `json:"high_lights_sub_100"`
//
//	//本期预告(业绩变动幅度上限)
//	PerformanceIncrUpper string `json:"performance_incr_upper"`
//	//本期预告(业绩变动幅度下限)
//	PerformanceIncrLower string `json:"performance_incr_lower"`
//
//	//净利润
//	NetProfit string `json:"net_profit"`
//	//净利润同比
//	NetProfitGrowthRate string `json:"net_profit_growth_rate"`
//
//	//营业总收入
//	BusinessRevenue string `json:"business_revenue"`
//
//	//营业总收入同比
//	BusinessRevenueGrowthRate string `json:"business_revenue_growth_rate"`
//	//扣非净利润
//	NetProfitAfterDed string `json:"net_profit_after_ded"`
//
//	//重要程度
//	Important string `json:"important"`
//	//姓名
//	UserName string `json:"user_name"`
//	//展真实姓名
//	RealName string `json:"real_name"`
//	//行业
//	Industry string `json:"industry"`
//	//行业解读
//	IndustryInterpretation string `json:"industry_interpretation"`
//	//业绩变动原因
//	PerformanceReason string `json:"performance_reason"`
//	//类型
//	Type      string `json:"type"`
//	Recommend string `json:"recommend"`
//}

//业绩公告表
type UPerformanceAnnouncement struct {
	ID                                 int64     `gorm:"primary_key;column:id;type:bigint(20) unsigned;not null"`
	Code                               string    `gorm:"index;column:code;type:varchar(10)" json:"code"`                                                           // 证券代码
	Name                               string    `gorm:"column:name;type:varchar(16)" json:"name"`                                                                 // 股票名称
	NetProfit                          string    `gorm:"column:net_profit;type:int(16)" json:"net_profit,omitempty"`                                               // 净利润（年度）
	NetProfitQuarter                   float64   `gorm:"column:net_profit_quarter;type:int(16)" json:"net_profit_quarter"`                                         // 净利润（单季度）
	NetProfitGrowthRate                string    `gorm:"column:net_profit_growth_rate;type:float" json:"net_profit_growth_rate,omitempty"`                         // 净利润同比增长
	NetProfitComparativeGrowth         float32   `gorm:"column:net_profit_comparative_growth;type:float" json:"net_profit_comparative_growth"`                     // 净利润环比增长
	BusinessRevenue                    string    `gorm:"column:business_revenue;type:int(16)" json:"business_revenue,omitempty"`                                   // 营业收入（年度）
	BusinessRevenueQuarter             float64   `gorm:"column:business_revenue_quarter;type:int(16)" json:"business_revenue_quarter"`                             // 营业收入（单季度）
	BusinessRevenueGrowthRate          string    `gorm:"column:business_revenue_growth_rate;type:float" json:"business_revenue_growth_rate,omitempty"`             // 营业收入同比增长(%)
	BusinessRevenueComparativeGrowth   float32   `gorm:"column:business_revenue_comparative_growth;type:float" json:"business_revenue_comparative_growth"`         // 营业收入环比增长
	CashFlowPerShare                   float64   `gorm:"column:cash_flow_per_share;type:float" json:"cash_flow_per_share"`                                         // 每股经营现金流量
	GrossProfitMargin                  float32   `gorm:"column:gross_profit_margin;type:float" json:"gross_profit_margin"`                                         // 总销售毛利率(%)
	GrossProfitMarginQuarter           float32   `gorm:"column:gross_profit_margin_quarter;type:float" json:"gross_profit_margin_quarter"`                         // 单季度销售毛利率(%)
	NetProfitAfterDed                  string    `gorm:"column:net_profit_after_ded;type:int(16)" json:"net_profit_after_ded,omitempty"`                           // 扣非净利润（年度）
	NetProfitAfterDedQuarter           float64   `gorm:"column:net_profit_after_ded_quarter;type:int(16)" json:"net_profit_after_ded_quarter"`                     // 扣非净利润（单季度）
	NetProfitAfterDedGrowthRate        float32   `gorm:"column:net_profit_after_ded_growth_rate;type:float" json:"net_profit_after_ded_growth_rate"`               // 扣非净利润同比增长率(%)
	NetProfitAfterDedComparativeGrowth float32   `gorm:"column:net_profit_after_ded_comparative_growth;type:float" json:"net_profit_after_ded_comparative_growth"` // 扣非净利润环比增长率(%)
	ExpectedPublishDate                time.Time `gorm:"column:expected_publish_date;type:datetime" json:"expected_publish_date"`                                  // 预约披露时间(时间戳(秒))

	PublishDate  time.Time `gorm:"column:publish_date;type:date" json:"publish_date"`   // 公告日期
	ReportPeriod string    `gorm:"column:report_period;type:date" json:"report_period"` // 报告期
	CreatedAt    time.Time `gorm:"column:created_at;type:datetime" json:"created_at"`   // 创建时间(时间戳(秒))
	UpdatedAt    time.Time `gorm:"column:updated_at;type:datetime" json:"updated_at"`   // 更新时间(时间戳(秒))
	DeletedAt    time.Time `gorm:"column:deleted_at;type:datetime" json:"deleted_at"`   // 删除时间(时间戳(秒))
}

//业绩预告表
type UPerformanceForecast struct {
	ID                     int64  `gorm:"primary_key;column:id;type:bigint(20) unsigned;not null" json:"-"`
	Code                   string `gorm:"index;column:code;type:varchar(10)" json:"code"`                                 // 证券代码
	Name                   string `gorm:"column:name;type:varchar(16)" json:"name"`                                       // 股票名称
	Performance            string `gorm:"column:performance;type:varchar(128)" json:"performance,omitempty"`              // 业绩变动
	ExpectedNetProfitUpper int    `gorm:"column:expected_net_profit_upper;type:int(16)" json:"expected_net_profit_upper"` // 预计净利润上限
	ExpectedNetProfitLower int    `gorm:"column:expected_net_profit_lower;type:int(16)" json:"expected_net_profit_lower"` // 预计净利润下限
	PerformanceIncrUpper   string `gorm:"column:performance_incr_upper;type:float" json:"performance_incr_upper"`         // 业绩变动幅度上限
	PerformanceIncrLower   string `gorm:"column:performance_incr_lower;type:float" json:"performance_incr_lower"`         // 业绩变动幅度下限
	PerformanceReason      string `gorm:"column:performance_reason;type:varchar(512)" json:"performance_reason"`          // 业绩变动原因

	PublishDate  string    `gorm:"column:publish_date;type:date" json:"publish_date"`            // 公告日期
	ReportPeriod string    `gorm:"column:report_period;type:date;not null" json:"report_period"` // 报告期
	CreatedAt    time.Time `gorm:"column:created_at;type:datetime" json:"created_at"`            // 创建时间
	UpdatedAt    time.Time `gorm:"column:updated_at;type:datetime" json:"updated_at"`            // 更新时间
	DeletedAt    time.Time `gorm:"column:deleted_at;type:datetime" json:"deleted_at"`            // 删除时间

	DisclosureTime     string `gorm:"column:disclosure_time;type:date;not null" json:"disclosure_time"`                 // 披露时间
	ForecastProportion string `gorm:"column:forecast_proportion;type:varchar(255);not null" json:"forecast_proportion"` // 预测比例
	ForecastNetProfit  string `gorm:"column:forecast_net_profit;type:varchar(255);not null" json:"forecast_net_profit"` // 预测净利润
	HighLights         string `gorm:"column:high_lights;type:varchar(255);not null" json:"high_lights"`                 // 亮点
}

// UPerformanceEdit 业绩解读,和预告表是垂直拆分出来的表(一个是爬虫输入的,一个是手动输入的)
type UPerformanceEdit struct {
	models.Base
	Code                 string `gorm:"unique_index:IDX_UNIQUE;index;column:code;type:varchar(10)" json:"code"`                     // 证券代码(股票code)
	ForecastProportion   string `gorm:"column:forecast_proportion;type:varchar(255)" json:"forecast_proportion"`                    // 预测比例
	BrokerProportion     string `gorm:"column:broker_proportion;type:varchar(255)" json:"broker_proportion"`                        // 券商预测
	BrokerInterpretation string `gorm:"column:broker_interpretation;type:varchar(1024)" json:"broker_interpretation"`               // 券商解读
	ForecastNetProfit    string `gorm:"column:forecast_net_profit;type:bigint(20)" json:"forecast_net_profit"`                      // 预测净利润
	HighLights           string `gorm:"column:high_lights;type:varchar(1024)" json:"high_lights"`                                   // 亮点
	ReportPeriod         string `gorm:"unique_index:IDX_UNIQUE;index;column:report_period;type:date;not null" json:"report_period"` // 报告期
	IndividualTrack      string `gorm:"column:individual_track;" json:"individual_track"`                                           // 个股追踪
}

type UPerformanceEdit2 struct {
	models.Base
	Code                 string `gorm:"unique_index:IDX_UNIQUE;index;column:code;type:varchar(10)" json:"code"`                     // 证券代码(股票code)
	ForecastProportion   string `gorm:"column:forecast_proportion;type:varchar(255)" json:"forecast_proportion"`                    // 预测比例
	BrokerProportion     string `gorm:"column:broker_proportion;type:varchar(255)" json:"broker_proportion"`                        // 券商预测
	BrokerInterpretation string `gorm:"column:broker_interpretation;type:varchar(1024)" json:"broker_interpretation"`               // 券商解读
	ForecastNetProfit    string `gorm:"column:forecast_net_profit;type:bigint(20)" json:"forecast_net_profit"`                      // 预测净利润
	HighLights           string `gorm:"column:high_lights;type:varchar(1024)" json:"high_lights"`                                   // 亮点
	ReportPeriod         string `gorm:"unique_index:IDX_UNIQUE;index;column:report_period;type:date;not null" json:"report_period"` // 报告期
}

//用户表
type BUser struct {
	models.Base
	Username      string `gorm:"column:username;type:varchar(100)" json:"username"`                       // 用户名
	Pwd           string `gorm:"column:pwd;type:varchar(100)" json:"pwd,omitempty"`                       // 密码
	Mobile        string `gorm:"column:mobile;type:varchar(100)" json:"mobile"`                           // 手机号
	Realname      string `gorm:"column:realname;type:varchar(100)" json:"realname"`                       // 姓名
	Nickname      string `gorm:"column:nickname;type:varchar(100)" json:"nickname,omitempty"`             // 用户名
	Avatar        string `gorm:"column:avatar;type:varchar(1000)" json:"avatar,omitempty"`                // 头像
	Gender        string `gorm:"column:gender;type:varchar(100)" json:"gender,omitempty"`                 // 性别
	Role          string `gorm:"column:role;type:varchar(2000)" json:"role,omitempty"`                    // 角色
	Type          string `gorm:"column:type;type:varchar(100)" json:"type,omitempty"`                     // 账号类型
	Rights        string `gorm:"column:rights;type:varchar(2000)" json:"rights,omitempty"`                // 权限
	Menus         string `gorm:"column:menus;type:text" json:"menus,omitempty"`                           // 菜单权限
	Memo          string `gorm:"column:memo;type:varchar(2000)" json:"memo,omitempty"`                    // 备注
	Source        string `gorm:"column:source;type:varchar(200)" json:"source,omitempty"`                 // 来源
	Fip           string `gorm:"column:fip;type:varchar(200)" json:"fip,omitempty"`                       // 首次IP
	Lip           string `gorm:"column:lip;type:varchar(200)" json:"lip,omitempty"`                       // 最后IP
	MobileAddress string `gorm:"column:mobile_address;type:varchar(200)" json:"mobile_address,omitempty"` // 地址(手机号)
	IPAddress     string `gorm:"column:ip_address;type:varchar(200)" json:"ip_address"`                   // 地址(ip)
	LicenseType   string `gorm:"column:license_type;type:varchar(255)" json:"license_type,omitempty"`     // 证件类型
	LicenseNo     string `gorm:"column:license_no;type:varchar(255)" json:"license_no,omitempty"`         // 证件编号
	LoginPwd      string `gorm:"column:login_pwd;type:varchar(255)" json:"login_pwd,omitempty"`           // 登录密码
	Creator       int64  `gorm:"column:creator;type:bigint(20)" json:"creator,omitempty"`                 // 创建者ID
	Status        string `gorm:"column:status;type:varchar(100)" json:"status"`                           // 状态（normal/banned）

	LastLoginTime *time.Time `gorm:"column:last_login_time;type:datetime" json:"last_login_time"` // 最后登陆时间
}

//行业表
type UPerformanceIndustry struct {
	models.Base
	IndustryName           string `gorm:"column:industry_name;type:varchar(255)" json:"industry_name"`                     // 行业名称
	IndustryInterpretation string `gorm:"column:industry_interpretation;type:varchar(255)" json:"industry_interpretation"` // 行业解读
	Industry               string `gorm:"column:industry;type:varchar(255);not null" json:"industry"`                      // 行业内容
}

//股票表
type BStock struct {
	ID                  string    `gorm:"primary_key;column:id;type:varchar(20);not null" json:"id"`
	Name                string    `gorm:"column:name;type:varchar(255)" json:"name"`
	SseID               int64     `gorm:"column:sse_id;type:bigint(20)" json:"sse_id"`                             // 上证E互动uid
	AdjFactor           float64   `gorm:"column:adj_factor;type:decimal(10,3)" json:"adj_factor"`                  // 复权因子
	TsCode              string    `gorm:"column:ts_code;type:varchar(20)" json:"ts_code"`                          // TS_CODE
	ConceptNum          int       `gorm:"column:concept_num;type:int(11)" json:"concept_num"`                      // 概念数
	ConceptUpdated      time.Time `gorm:"column:concept_updated;type:datetime" json:"concept_updated"`             // 概念更新时间
	IsB                 int8      `gorm:"column:is_b;type:tinyint(4)" json:"is_b"`                                 // B股
	PubDate             time.Time `gorm:"column:pub_date;type:date" json:"pub_date"`                               // 发行日期
	PubPrice            float64   `gorm:"column:pub_price;type:decimal(10,2)" json:"pub_price"`                    // 发行价
	BaseDate            time.Time `gorm:"column:base_date;type:date" json:"base_date"`                             // 基准日
	TickNum             int       `gorm:"column:tick_num;type:int(11)" json:"tick_num"`                            // 交易日数
	TickNumDate         time.Time `gorm:"column:tick_num_date;type:date" json:"tick_num_date"`                     // 交易日统计日期
	NoNeedTrack         int8      `gorm:"column:no_need_track;type:tinyint(4)" json:"no_need_track"`               // 不追踪
	PerformanceSyncTime time.Time `gorm:"column:performance_sync_time;type:datetime" json:"performance_sync_time"` // 业绩展示处理时间
	ListStatus          string    `gorm:"column:list_status;type:varchar(20)" json:"list_status"`                  // 上市状态
	ReportUpdated       time.Time `gorm:"column:report_updated;type:datetime" json:"report_updated"`               // 业绩更新时间
	StartDate           time.Time `gorm:"column:start_date;type:date" json:"start_date"`                           // 上市日期
	EndDate             time.Time `gorm:"column:end_date;type:date" json:"end_date"`                               // 退市日期
	PyName              string    `gorm:"column:py_name;type:varchar(20)" json:"py_name"`                          // 拼音
	Creator             int64     `gorm:"column:creator;type:bigint(20)" json:"creator"`                           // 创建者ID
	Created             time.Time `gorm:"column:created;type:timestamp" json:"created"`                            // 创建时间
	Updated             time.Time `gorm:"column:updated;type:timestamp" json:"updated"`                            // 更新时间
	Status              string    `gorm:"column:status;type:varchar(50)" json:"status"`                            // 状态
	Deleted             bool      `gorm:"column:deleted;type:tinyint(1)" json:"deleted"`                           // 是否已删除
	Jqdata              bool      `gorm:"column:jqdata;type:tinyint(1)" json:"jqdata"`                             // jqdata是否有数据
}

type UStockIndustryCode struct {
	ID       int `gorm:"primary_key;column:id;type:int(11) unsigned;not null" json:"-"`
	StockID  int `gorm:"column:stock_id;type:int(11);not null" json:"stock_id"` // 股票表主键id
	Industry int `gorm:"column:industry;type:int(11);not null" json:"industry"` // 行业表主键id
}

// UStockIndustryRelationship 股票,行业关系表,多对多
type UStockIndustryRelationship struct {
	models.Base
	StockID              int    `gorm:"column:stock_id;type:int(11)" json:"stock_id"`                                  // 股票表主键id
	IndustryID           int    `gorm:"column:industry_id;type:int(11)" json:"industry_id"`                            // 编辑表主键id
	UserID               string `gorm:"column:user_id;type:varchar(255)" json:"user_id"`                               // 股票 行业关联的用户id
	IndividualShareTrack string `gorm:"column:individual_share_track;type:varchar(255)" json:"individual_share_track"` // 个股跟踪
	Recommend            string `gorm:"column:recommend;type:varchar(255)" json:"recommend"`                           // 是否推荐
	Important            string `gorm:"column:important;type:varchar(255)" json:"important"`                           // 重要程度
	Type                 string `gorm:"column:type;type:varchar(255)" json:"type"`                                     // 类型
}

// PStockDate 日线
type PStockDate struct {
	ID           int64     `gorm:"primary_key;column:id;type:bigint(20) unsigned;not null" json:"-"`
	StockCode    string    `gorm:"index:HASH;column:stock_code;type:varchar(20)" json:"stock_code"` // 股票代码
	TsCode       string    `gorm:"column:ts_code;type:varchar(20)" json:"ts_code"`                  // 全代码
	TradeDate    time.Time `gorm:"index:HASH;column:trade_date;type:date" json:"trade_date"`        // 交易日
	Open         float64   `gorm:"column:open;type:decimal(10,2)" json:"open"`                      // 开
	High         float64   `gorm:"column:high;type:decimal(10,2)" json:"high"`                      // 高
	Low          float64   `gorm:"column:low;type:decimal(10,2)" json:"low"`                        // 低
	Close        float64   `gorm:"column:close;type:decimal(10,2)" json:"close"`                    // 收
	PreClose     float64   `gorm:"column:pre_close;type:decimal(10,2)" json:"pre_close"`            // 昨收
	Change       float64   `gorm:"column:change;type:decimal(10,2)" json:"change"`                  // 涨幅
	PctChg       float64   `gorm:"column:pct_chg;type:decimal(10,2)" json:"pct_chg"`                // 百分比涨幅
	Block        float64   `gorm:"column:block;type:decimal(10,2)" json:"block"`                    // 打板价
	BlockPct     float64   `gorm:"column:block_pct;type:decimal(10,2)" json:"block_pct"`            // 打板涨幅
	IsBlock      int8      `gorm:"column:is_block;type:tinyint(4)" json:"is_block"`                 // 板
	BlockNum     int8      `gorm:"column:block_num;type:tinyint(4)" json:"block_num"`               // 连板
	Vol          float64   `gorm:"column:vol;type:decimal(20,2)" json:"vol"`                        // 成交量(手)
	Amount       float64   `gorm:"column:amount;type:decimal(20,3)" json:"amount"`                  // 成交额(千元)
	AdjFactor    float64   `gorm:"column:adj_factor;type:decimal(10,3)" json:"adj_factor"`          // 复权因子
	PreAdjFactor float64   `gorm:"column:pre_adj_factor;type:decimal(10,3)" json:"pre_adj_factor"`  // 昨天复权因子
	IsSt         int8      `gorm:"column:is_st;type:tinyint(4)" json:"is_st"`                       // ST
	Hash         string    `gorm:"column:hash;type:varchar(200)" json:"hash"`                       // hash
	OwnerID      int64     `gorm:"column:owner_id;type:bigint(20)" json:"owner_id"`                 // 所有者
	UpdatedAt    time.Time `gorm:"column:updated_at;type:datetime" json:"updated_at"`               // 更新时间
	CreatedAt    time.Time `gorm:"column:created_at;type:datetime" json:"created_at"`               // 创建时间
	Creator      int64     `gorm:"column:creator;type:bigint(20)" json:"creator"`                   // 创建人
	Status       string    `gorm:"column:status;type:varchar(50)" json:"status"`                    // 状态
	DeletedAt    time.Time `gorm:"column:deleted_at;type:datetime" json:"deleted_at"`               // 删除时间
}

/*--------------------------------------------下面是业绩计算页面------------------------------------------------*/
//获取历史报告期,降序
func GetReportPeriod(date string) (res []UPerformanceAnnouncement, err error) {
	err = models.DB().Table("u_performance_announcement").Select("report_period").Where("report_period < ?", date).Group("report_period").Order("report_period DESC").Find(&res).Error
	if err != nil {
		return nil, err
	}
	return
}

type GetUserInfoResp struct {
	Id       int    `json:"id"`
	Realname string `json:"realname"`
}

//获取用户列表接口
func GetUserInfo(req user_service.GetUserInfoRequest) (b_u []GetUserInfoResp) {
	if util.IsEmpty(req.UserName) {
		models.DB().Table("b_user").Select("id,realname").Find(&b_u)
	} else {
		models.DB().Table("b_user").Select("id,realname").Where("realname like ?", "%"+req.UserName+"%").Find(&b_u)
	}
	return
}

type Result struct {
	Code         string `json:"code"`
	Name         string `json:"name"`
	ReportPeriod string `json:"report_period"`
}

type ResultCount struct {
	Code         string `json:"code"`
	ReportPeriod string `json:"report_period"`
}

//业绩计算页面接口
func FetchPerformance(sort1, sort2, sort3, sort4, sort5, sort6, sort7, sort8, sort9, sort10, search, reportPeriod, typeName, important, userId, recommend,
	disclosureStartTime, disclosureEndTime string, page int, page_size int) ([]performance_service.GetPerformanceMysql, int, error) {

	var (
		err        error
		searchTYpe int
	)

	res := make([]util.GetPerformanceMysql, 0)
	resDeal := make([]performance_service.GetPerformanceMysql, 0)

	typeNameArray := strings.Split(typeName, "_")
	//判断search内容
	searchTYpe, err = SearchType(search)
	if err != nil {
		return nil, 0, err
	}

	//一次性查出搜索符合条件的数据,之后 按照特殊规则排序,切片分页
	//如果没有,在判断是否是股票名称:去b_stock查,如果有,则为股票名称,没有则为行业内容,三个都不是，直接返回空数据
	//查询预告表,公告表两个表中所有符合报告期数据
	dbs := models.DB().Table("u_performance_announcement as a")
	dbs = dbs.Joins(" left join u_performance_forecast as b on a.code = b.code and a.report_period = b.report_period")
	dbs = dbs.Joins("left join u_stock_industry_relationship as c on b.code = c.stock_id")
	dbs = dbs.Joins("left join u_performance_industry as d on c.industry_id = d.id")
	dbs = dbs.Joins("left join b_user as e on c.user_id = e.id")
	dbs = dbs.Joins("left join b_stock as f on a.code = f.id")
	dbs = dbs.Joins("left join u_performance_edit as g on a.code = g.code and a.report_period = g.report_period")
	selectStr := "a.code,a.net_profit,a.net_profit_growth_rate,a.business_revenue,a.business_revenue_growth_rate,a.net_profit_after_ded,b.disclosure_time,b.disclosure_time_history,b.disclosure_change_at,g.forecast_proportion,g.forecast_net_profit,g.high_lights,g.broker_proportion,g.broker_interpretation,b.performance_incr_upper,b.performance_incr_lower,b.performance_reason,c.important,c.user_id,c.recommend,c.type,d.industry_interpretation,d.industry_name,c.id,e.realname,f.name"
	dbs = dbs.Select(selectStr)
	//报告期
	dbs = dbs.Where("a.report_period = ?", reportPeriod)
	switch searchTYpe {
	case 0:
	//无内容搜索
	case 1:
		dbs = dbs.Where("a.code = ?", search) //股票代码
	case 2:
		dbs = dbs.Where("a.name = ?", search) //股票名称
	case 3:
		dbs = dbs.Where("d.industry_name like ?", "%"+search+"%") //行业内容
	default:
		//无内容搜索
	}

	//类型
	if typeName != "" {
		switch len(typeNameArray) {
		case 1:
			dbs = dbs.Where("c.type like ?", "%_"+typeNameArray[0]+"_%")
		case 2:
			dbs = dbs.Where("c.type like ? or c.type like ?", "%_"+typeNameArray[0]+"_%", "%_"+typeNameArray[1]+"_%")
		case 3:
			dbs = dbs.Where("c.type like ? or c.type like ? or c.type like ?", "%_"+typeNameArray[0]+"_%", "%_"+typeNameArray[1]+"_%", "%_"+typeNameArray[2]+"_%")
		case 4:
			dbs = dbs.Where("c.type like ? or c.type like ? or c.type like ? or c.type like ?", "%_"+typeNameArray[0]+"_%", "%_"+typeNameArray[1]+"_%", "%_"+typeNameArray[2]+"_%", "%_"+typeNameArray[3]+"_%")
		case 5:
			dbs = dbs.Where("c.type like ? or c.type like ? or c.type like ? or c.type like ? or c.type like ?", "%_"+typeNameArray[0]+"_%", "%_"+typeNameArray[1]+"_%", "%_"+typeNameArray[2]+"_%", "%_"+typeNameArray[3]+"_%", "%_"+typeNameArray[4]+"_%")
		}
	}
	//重要程度
	if important != "" {
		dbs = dbs.Where("c.important = ?", important)
	}
	//用户名id,空字符串全部
	if userId != "" {
		dbs = dbs.Where("c.user_id = ?", userId)
	}
	//是否推荐
	if recommend != "" {
		dbs = dbs.Where("c.recommend = ?", recommend)
	}
	//披露时间
	if disclosureStartTime != "" && disclosureEndTime != "" {
		dbs = dbs.Where("b.disclosure_time between ? and ?", disclosureStartTime, disclosureEndTime) //披露时间
	}

	//去除 2,4,8,9开头的股票显示
	dbs = dbs.Where("a.code not like ?", "2%")
	dbs = dbs.Where("a.code not like ?", "4%")
	dbs = dbs.Where("a.code not like ?", "8%")
	dbs = dbs.Where("a.code not like ?", "9%")
	dbs = dbs.Where("a.deleted_at is null")

	timeNow := util.GenDateFormat2()

	//排序
	//预测比例排序
	if sort2 != "" {
		dbs.Unscoped().Group("a.code").Find(&res)

		for _, v := range res {
			resOne := GenReturnData(v)
			resDeal = append(resDeal, resOne)
		}

		resDealBySort2, _ := util.StructSort3(resDeal, sort2)
		//总条数
		total := len(resDealBySort2)
		//切片分页
		startIndex := (page - 1) * page_size //开始位置
		endIndex := startIndex + page_size   //结束位置

		if endIndex <= total {
			//返回部分
			return resDealBySort2[startIndex:endIndex], total, nil
		} else {
			//返回后面全部
			return resDealBySort2[startIndex:], total, nil
		}
	}

	//券商预测排序
	if sort10 != "" {
		dbs.Unscoped().Group("a.code").Find(&res)

		for _, v := range res {
			resOne := GenReturnData(v)
			resDeal = append(resDeal, resOne)
		}

		resDealBySort2, _ := util.StructSort5(resDeal, sort10)
		//总条数
		total := len(resDealBySort2)
		//切片分页
		startIndex := (page - 1) * page_size //开始位置
		endIndex := startIndex + page_size   //结束位置

		if endIndex <= total {
			//返回部分
			return resDealBySort2[startIndex:endIndex], total, nil
		} else {
			//返回后面全部
			return resDealBySort2[startIndex:], total, nil
		}
	}

	//预测净利润排序
	if sort3 != "" {
		dbs.Unscoped().Group("a.code").Find(&res)

		for _, v := range res {
			resOne := GenReturnData(v)
			resDeal = append(resDeal, resOne)
		}

		resDealBySort2, _ := util.StructSort6(resDeal, sort3)
		//总条数
		total := len(resDealBySort2)
		//切片分页
		startIndex := (page - 1) * page_size //开始位置
		endIndex := startIndex + page_size   //结束位置

		if endIndex <= total {
			//返回部分
			return resDealBySort2[startIndex:endIndex], total, nil
		} else {
			//返回后面全部
			return resDealBySort2[startIndex:], total, nil
		}
	}

	//本期预告排序
	if sort4 != "" {
		orderStr := "b.performance_incr_upper " + sort4
		dbs = dbs.Order(orderStr)
		dbs.Unscoped().Group("a.code").Find(&res)
	}
	//净利润排序排序
	if sort5 != "" {
		orderStr := "a.net_profit " + sort5
		dbs = dbs.Order(orderStr)
		dbs.Unscoped().Group("a.code").Find(&res)
	}
	//净利润同比排序
	if sort6 != "" {
		orderStr := "a.net_profit_growth_rate " + sort6
		dbs = dbs.Order(orderStr)
		dbs.Unscoped().Group("a.code").Find(&res)
	}
	//营业总收入排序
	if sort7 != "" {
		orderStr := " a.business_revenue " + sort7
		dbs = dbs.Order(orderStr)
		dbs.Unscoped().Group("a.code").Find(&res)
	}
	//营业总收入同比排序
	if sort8 != "" {
		orderStr := " a.business_revenue_growth_rate " + sort8
		dbs = dbs.Order(orderStr)
		dbs.Unscoped().Group("a.code").Find(&res)
	}
	//扣非净利润排序
	if sort9 != "" {
		orderStr := " a.net_profit_after_ded " + sort9
		dbs = dbs.Order(orderStr)
		dbs.Unscoped().Group("a.code").Find(&res)
	}
	//当前时间
	//披露时间升降序-直接数据库排序
	if sort1 != "" {
		if sort1 == "asc" {
			dbs.Unscoped().Order("b.disclosure_time asc").Group("a.code").Where("b.disclosure_time is not null").Find(&res)

		} else {
			dbs.Unscoped().Order("b.disclosure_time desc").Group("a.code").Find(&res)
		}
	}

	//刚进页面,默认披露时间排序规则
	if sort1 == "" && sort2 == "" && sort3 == "" && sort4 == "" && sort5 == "" && sort6 == "" && sort7 == "" && sort8 == "" && sort9 == "" {
		var (
			res1 []util.GetPerformanceMysql
			res2 []util.GetPerformanceMysql
			res3 []util.GetPerformanceMysql
		)
		//披露时间大于当前日期的在最上面升序，下面的小于当前日期降序,空的在最后面
		dbs.Unscoped().Where("b.disclosure_time >= ?", timeNow).Order("b.disclosure_time asc").Group("a.code").Find(&res1)
		dbs.Unscoped().Where("b.disclosure_time is null").Group("a.code").Find(&res2)
		dbs.Unscoped().Where("b.disclosure_time < ?", timeNow).Order("b.disclosure_time desc").Group("a.code").Find(&res3)
		res = append(res, res1...)
		res = append(res, res3...)
		res = append(res, res2...)
	}

	fmt.Printf("res:%#v\n", res) //总返回结果

	//处理返回结果
	for _, v := range res {
		//有的股票名称为空,需要去查询股票表获取股票名称
		resOne := GenReturnData(v)
		resDeal = append(resDeal, resOne)
	}

	//总条数
	total := len(resDeal)
	//切片分页
	startIndex := (page - 1) * page_size //开始位置
	endIndex := startIndex + page_size   //结束位置

	if endIndex <= total {
		//返回部分
		return resDeal[startIndex:endIndex], total, nil
	} else {
		//返回后面全部
		return resDeal[startIndex:], total, nil
	}
}

//GetPerformanceMysql结构体赋值:生成返回数据结构体
func GenReturnData(src util.GetPerformanceMysql) performance_service.GetPerformanceMysql {

	var (
		NetProfitStr                 *float64
		netProfitGrowthRateStr       *float64
		BusinessRevenueStr           *float64
		businessRevenueGrowthRateStr *float64
		netProfitAfterDedStr         *float64
		//b_u                          BUser
		important      string
		disclosureTime string
		//DisclosureTimeHistory string
		//DisclosureChangeAt    string
		//预测比例
		ForecastProportionLowerStr *float64
		ForecastProportionUpperStr *float64
		//预测净利润
		forecastNetProfitStrUpper *float64
		forecastNetProfitStrLower *float64

		//券商预测
		BrokerProportionLowerStr *float64
		BrokerProportionUpperStr *float64
	)

	//披露时间,只要日期
	if src.DisclosureTime != "" {
		disclosureTime = src.DisclosureTime[0:10]
	}
	//if src.DisclosureTimeHistory != "" {
	//	DisclosureTimeHistory = nil
	//} else {
	//	DisclosureTimeHistory = src.DisclosureTimeHistory
	//}
	//if src.DisclosureChangeAt == "" {
	//	DisclosureChangeAt = nil
	//} else {
	//	DisclosureChangeAt
	//}
	//预测比例
	if src.ForecastProportion != nil && *src.ForecastProportion != "" {
		forecastProportionArray := strings.Split(*src.ForecastProportion, "~")
		if len(forecastProportionArray) == 2 {
			middle1 := util.Atof(forecastProportionArray[0])
			middle2 := util.Atof(forecastProportionArray[1])
			ForecastProportionLowerStr = &middle1
			ForecastProportionUpperStr = &middle2
		} else if len(forecastProportionArray) == 1 {
			middle := util.Atof(*src.ForecastProportion)
			ForecastProportionUpperStr = &middle
		}
	}
	//券商预测
	if src.BrokerProportion != nil && *src.BrokerProportion != "" {
		brokerProportionArray := strings.Split(*src.BrokerProportion, "~")
		if len(brokerProportionArray) == 2 {
			//去除掉百分号
			floatStr1 := strings.Replace(brokerProportionArray[0], "%", "", -1)
			floatStr2 := strings.Replace(brokerProportionArray[1], "%", "", -1)

			middle1 := util.Atof(floatStr1)
			middle2 := util.Atof(floatStr2)

			BrokerProportionLowerStr = &middle1
			BrokerProportionUpperStr = &middle2
		} else if len(brokerProportionArray) == 1 {
			floatStr1 := strings.Replace(brokerProportionArray[0], "%", "", -1)
			middle := util.Atof(floatStr1)
			BrokerProportionUpperStr = &middle
		}
	}

	//预测净利润
	if src.ForecastNetProfit != nil && *src.ForecastNetProfit != "" {
		forecastNetProfitArray := strings.Split(*src.ForecastNetProfit, "~")
		if len(forecastNetProfitArray) == 2 {
			middle1 := util.Atof(forecastNetProfitArray[0])
			middle2 := util.Atof(forecastNetProfitArray[1])
			forecastNetProfitStrLower = &middle1
			forecastNetProfitStrUpper = &middle2
		} else if len(forecastNetProfitArray) == 1 {
			middle := util.Atof(*src.ForecastNetProfit)
			forecastNetProfitStrUpper = &middle
		}
	}

	if src.NetProfit != nil {
		middle := util.DecimalThree(*src.NetProfit / 100000000)
		NetProfitStr = &middle
	}

	if src.BusinessRevenue != nil {
		middle := util.DecimalThree(*src.BusinessRevenue / 100000000)
		BusinessRevenueStr = &middle
	}

	if src.BusinessRevenueGrowthRate != nil {
		businessRevenueGrowthRateStr = src.BusinessRevenueGrowthRate

	}
	if src.NetProfitAfterDed != nil {
		middle := *src.NetProfitAfterDed / 100000000
		netProfitAfterDedStr = &middle
	}

	if src.NetProfitGrowthRate != nil {
		netProfitGrowthRateStr = src.NetProfitGrowthRate
	}

	//重要程度转字符串
	if src.Important == "2" {
		important = "非常重要"
	}
	if src.Important == "1" {
		important = "重要"
	}
	if src.Important == "0" {
		important = "不重要"
	}

	//类型 1_2转文字,妖股_业绩增长
	typeSlice := strings.Split(src.Type, "_")
	typeStr := ""

	for _, v := range typeSlice {
		if v == "1" {
			typeStr += "妖股,"
		}
		if v == "2" {
			typeStr += "高比例,"
		}
		if v == "3" {
			typeStr += "业绩增长,"
		}
		if v == "4" {
			typeStr += "并购重组,"
		}
		if v == "5" {
			typeStr += "业绩反转,"
		}

	}
	typeStr = strings.TrimRight(typeStr, ",")

	if src.PerformanceIncrUpper != nil {
		src.PerformanceIncrUpper = src.PerformanceIncrUpper
	}

	if src.PerformanceIncrLower != nil {
		src.PerformanceIncrLower = src.PerformanceIncrLower
	}
	//本期预告
	var resOne = performance_service.GetPerformanceMysql{
		ID:                    src.ID,
		Name:                  src.Name,
		Code:                  src.Code,
		ExpectedPublishDate:   &disclosureTime,
		DisclosureTimeHistory: src.DisclosureTimeHistory,
		DisclosureChangeAt:    src.DisclosureChangeAt,

		ForecastProportionLower: ForecastProportionLowerStr,
		ForecastProportionUpper: ForecastProportionUpperStr,

		//券商预测
		BrokerProportionLower: BrokerProportionLowerStr,
		BrokerProportionUpper: BrokerProportionUpperStr,

		//券商解读
		BrokerInterpretation:       src.BrokerInterpretation,
		BrokerInterpretationSub100: util.Sub50ChineseCharacters(src.BrokerInterpretation),

		ForecastNetProfitLower: forecastNetProfitStrLower,
		ForecastNetProfitUpper: forecastNetProfitStrUpper,
		HighLights:             src.HighLights,
		HighLightsSub100:       util.Sub100ChineseCharacters(src.HighLights),

		PerformanceIncrUpper: src.PerformanceIncrUpper,
		PerformanceIncrLower: src.PerformanceIncrLower,

		NetProfit: NetProfitStr,

		NetProfitGrowthRate: netProfitGrowthRateStr,

		BusinessRevenue:           BusinessRevenueStr,
		BusinessRevenueGrowthRate: businessRevenueGrowthRateStr,

		NetProfitAfterDed: netProfitAfterDedStr,

		Important:              important,
		RealName:               src.Realname,
		Type:                   typeStr,
		Recommend:              src.Recommend,
		PerformanceReason:      src.PerformanceReason,
		IndustryInterpretation: src.IndustryInterpretation,
		Industry:               src.IndustryName,
	}
	return resOne
}

//判断业绩计算：search内容类型,是股票代码,还是股票名称,还是行业名称
/**
0 为 无条件
1 为 股票代码
2 为 股票名称
3 为 行业名称
*/
func SearchType(search string) (int, error) {
	var (
		b_s   BStock
		u_p_i UPerformanceIndustry
	)
	if search != "" {
		if parser.IsNum(search) {
			//判断search是否为股票代码
			models.DB().Where("id = ?", search).Select("id").Find(&b_s)
			if b_s.ID == search {
				//search为股票代码
				return 1, nil
			} else {
				return 0, fmt.Errorf("错误的股票代码")
			}
		} else {
			//判断是否为股票名称
			models.DB().Where("name = ?", search).Select("id,name").Find(&b_s)
			if b_s.Name == search {
				//股票名称
				return 2, nil
			} else {
				models.DB().Where("industry_name like ?", search).Select("id").Find(&u_p_i)
				if u_p_i.ID > 0 {
					//行业名称
					return 3, nil
				} else {
					return 0, fmt.Errorf("不知道的搜索条件内容")
				}
			}
		}
	} else {
		//无search条件
		return 0, nil
	}
}

//业绩编辑修改:返回数据结构体
type EditGetPerformance struct {
	Code               string  ` json:"code" `              //股票代码
	Name               string  ` json:"name" `              //股票名称
	ReportPeriod       string  `json:"report_period" `      //报告期 date
	DisclosureTime     string  ` json:"disclosure_time" `   //披露时间 date
	Type               string  `json:"type"`                //类型,eg:1_2_3
	Important          string  `json:"important"`           //重要程度
	Recommend          int8    `json:"recommend"`           //是否推荐,1 是，2 否
	ForecastProportion string  `json:"forecast_proportion"` //预测比例
	ForecastNetProfit  float64 `json:"forecast_net_profit"` //预测净利润
	HighLights         string  `json:"high_lights"`         //亮点
}

func PostEditPerformance(id int, code, typeName string, important, recommend, forecast_proportion, forecast_net_profit, high_lights, report_period,
	disclosure_time, broker_proportion, broker_interpretation string) (bool, error) {
	var err error
	var u_p_e UPerformanceEdit
	if id == 0 {
		//如果关系表中没有股票行业的对应关系,那么直接新插入一条(关系表中)
		timeNow, _ := util.GenDateFormatTime()
		var u_s_i_r UStockIndustryRelationship
		u_s_i_r.StockID, _ = strconv.Atoi(code)
		u_s_i_r.Type = typeName
		u_s_i_r.Recommend = recommend
		u_s_i_r.Important = important
		u_s_i_r.CreatedAt = timeNow
		u_s_i_r.UpdatedAt = timeNow
		models.DB().Table("u_stock_industry_relationship").Create(&u_s_i_r)
	} else {
		//更新u_stock_industry_relationship
		dbs := models.DB().Table("u_stock_industry_relationship").Select("type,important,recommend")
		dbs = dbs.Where("stock_id = ?", code)
		err = dbs.Updates(map[string]interface{}{
			"type":      "_" + typeName + "_",
			"important": important,
			"recommend": recommend,
		}).Error
		if err != nil {
			return false, err
		}
	}

	//更新u_p_f表
	//预测比例
	forecast_proportionArray := strings.Split(forecast_proportion, "~")
	if len(forecast_proportionArray) == 1 {
		forecast_proportion = forecast_proportionArray[0]
	}
	if len(forecast_proportionArray) == 2 {
		forecast_proportion = forecast_proportionArray[0] + "~" + forecast_proportionArray[1]
	}
	//预测净利润
	forecast_net_profitArray := strings.Split(forecast_net_profit, "~")
	if len(forecast_net_profitArray) == 1 {
		forecast_net_profit = forecast_net_profitArray[0]
	}
	if len(forecast_net_profitArray) == 2 {
		forecast_net_profit = forecast_net_profitArray[0] + "~" + forecast_net_profitArray[1]
	}
	//券商预测
	broker_proportionArray := strings.Split(broker_proportion, "~")
	if len(broker_proportionArray) == 1 {
		broker_proportion = broker_proportionArray[0]
	}
	if len(broker_proportionArray) == 2 {
		broker_proportion = broker_proportionArray[0] + "~" + broker_proportionArray[1]
	}

	//u_p_e表中没有则新增，有则更新
	models.DB().Table("u_performance_edit").Select("id").Where("code = ?", code).Where("report_period = ?", report_period).Find(&u_p_e)

	if u_p_e.ID > 0 {
		//更新u_p_e表
		dbs := models.DB().Table("u_performance_edit").Select("forecast_proportion,forecast_net_profit,broker_proportion,broker_interpretation,high_lights")
		dbs = dbs.Where("code = ?", code)
		dbs = dbs.Where("report_period = ?", report_period)
		err = dbs.Updates(map[string]interface{}{
			"forecast_proportion":   forecast_proportion,
			"forecast_net_profit":   forecast_net_profit,
			"broker_proportion":     broker_proportion,
			"broker_interpretation": broker_interpretation,
			"high_lights":           high_lights,
		}).Error
	} else {

		//插入
		u_p_e.Code = code
		u_p_e.ReportPeriod = report_period
		u_p_e.ForecastProportion = forecast_proportion
		u_p_e.ForecastNetProfit = forecast_net_profit
		u_p_e.BrokerProportion = broker_proportion
		u_p_e.BrokerInterpretation = broker_interpretation
		u_p_e.HighLights = high_lights

		err = models.DB().Table("u_performance_edit").Create(&u_p_e).Error
	}

	if err != nil {
		return false, err
	}
	return true, nil
}

func Import2(r io.Reader) (err error) {
	xlsx, err := excelize.OpenReader(r)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	rows := xlsx.GetRows("Sheet1")
	for irow, row := range rows {
		if irow > 0 {
			var data []string
			for _, cell := range row {
				data = append(data, cell)
			}
			ImportPerformanceExcel(data[0], data[1], data[2], data[3])
		}
	}
	return nil
}

func ImportPerformanceExcel(name, forecast_proportion, forecast_net_profit, high_lights string) {
	var b_s BStock
	var u_p_e UPerformanceEdit2
	name = strings.Trim(name, "")
	forecast_proportion = strings.Trim(forecast_proportion, "")
	forecast_net_profit = strings.Trim(forecast_net_profit, "")
	high_lights = strings.Trim(high_lights, "")
	models.DB().Select("id,name").Where("name = ?", name).Find(&b_s)
	u_p_e.Code = b_s.ID
	u_p_e.ForecastProportion = forecast_proportion
	u_p_e.ForecastNetProfit = forecast_net_profit
	u_p_e.HighLights = high_lights
	u_p_e.ReportPeriod = "2020-06-30"
	models.DB().Table("u_performance_edit").Create(&u_p_e)
}

/*--------------------------------------------下面是股票维护页面------------------------------------------------*/

type StockSearchResponse struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

//股票模糊搜索
func StockSearch(search string) (res []StockSearchResponse) {
	//models.DB().Table("b_stock").Select("id,name").Where("id like ?", "%"+search+"%").Or("name like ?", "%"+search+"%").Find(&res)
	// 换到新的数据表 原来的b_stock没在维护
	models.DB().Table("b_stock_names").Select("code as id,name").
		Where("code like ?", "%"+search+"%").Or("name like ?", "%"+search+"%").Where("deleted_at is null").Find(&res)
	return
}

//(1)保存数据到编辑表，股票行业关系表
func SaveStockMaintain(name, industry, user_name string) (res bool, err error) {
	var (
		b_s     BStock
		u_i_r_s UStockIndustryRelationship
		b_u     BUser
		u_p_i   UPerformanceIndustry
	)
	//先查后增加,一个股票可以关联多个行业,一个行业也可以关联多个股票
	name = strings.Trim(name, "")
	industry = strings.Trim(industry, "")
	user_name = strings.Trim(user_name, "")

	models.DB().Select("id,name").Where("name = ?", name).Find(&b_s)
	if b_s.ID == "" {
		return false, fmt.Errorf(name + ":股票不存在(错别字)")
	}

	//根据用户名 查用户
	models.DB().Table("b_user").Select("id").Where("realname = ?", user_name).Find(&b_u)

	//取行业id
	err = models.DB().Table("u_performance_industry").Select("id,industry_name").Where("industry_name = ?", industry).Find(&u_p_i).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		//数据库中没有此条记录,则添加到行业表
		//不存在,先插入行业表,再插入关系表
		u_p_i.IndustryName = industry
		timeFormat, _ := util.GenDateFormatTime()
		u_p_i.CreatedAt = timeFormat
		u_p_i.UpdatedAt = timeFormat
		models.DB().Table("u_performance_industry").Create(&u_p_i)
	}

	models.DB().Table("u_performance_industry").Select("id,industry_name").Where("industry_name = ?", industry).Find(&u_p_i)
	models.DB().Table("u_stock_industry_relationship").Where("stock_id = ?", b_s.ID).Where("industry_id = ?", u_p_i.ID).Find(&u_i_r_s)
	if u_i_r_s.ID > 0 {
		//util.Tracefile2("股票行业关系已经存在")
		return false, fmt.Errorf("股票行业关系已经存在")
	}

	//插入关系表
	u_i_r_s.StockID, err = strconv.Atoi(b_s.ID)
	if err != nil {
		return false, err
	}
	if b_s.ID != "" {
		u_i_r_s.Important = strconv.Itoa(2)
	}
	u_i_r_s.IndustryID = int(u_p_i.ID)
	u_i_r_s.UserID = strconv.Itoa(int(b_u.ID))
	//插入关系表u_stock_industry_relationship
	err = models.DB().Table("u_stock_industry_relationship").Create(&u_i_r_s).Error
	if err != nil {
		return false, err
	}
	return true, nil
}

//返回mysql数据结构体
type SearchStockMaintain struct {
	Id           int    `json:"id"`   //行业id
	Code         string `json:"code"` //股票代码
	Name         string `json:"name"` //股票名称
	IndustryId   int    `json:"industry_id"`
	IndustryName string `json:"industry_name"` //行业名称
	UserId       string `json:"user_id"`
	UserName     string `json:"user_name"`  //用户名
	UpdatedAt    string `json:"updated_at"` //更新时间
}

type IndustryIds struct {
	IndustryId string `json:"industry_id"`
}

func SearchStockMaintainData(info, industry, user_id string, page, page_size int) (res []SearchStockMaintain, total int, err error) {
	var (
		b_s BStock
	)

	dbs := models.DB().Table("u_stock_industry_relationship as a").Joins("join b_stock as b on b.id = a.stock_id")
	dbs = dbs.Joins("left join u_performance_industry as c on a.industry_id = c.id")
	dbs = dbs.Joins("left join b_user as d on a.user_id = d.id")
	dbs = dbs.Select("b.id as code ,b.name,c.industry_name,a.industry_id,a.user_id,a.updated_at,a.id as id,d.realname as user_name")

	//判断info字段输入的是股票代码还是股票名称
	if info != "" {
		//股票code或代码
		err = models.DB().Select("id,name").Where("id = ?", info).Or("name = ?", info).Find(&b_s).Error
		fmt.Printf("b_s:%#v\n", b_s)
		if err != nil {
			return nil, 0, err
		}
		dbs = dbs.Where("b.id = ?", b_s.ID)
	}
	//行业名称条件判断
	if industry != "" {
		dbs = dbs.Where("c.industry_name like ?", "%"+industry+"%")
	}
	//姓名条件判断
	if user_id != "" {
		dbs = dbs.Where("a.user_id = ?", user_id)
	}
	dbs = dbs.Order("updated_at desc")
	dbs.Count(&total)
	err = dbs.Limit(page_size).Offset((page - 1) * page_size).Find(&res).Error
	fmt.Printf("res:%#v\n", res)
	if err != nil {
		return nil, 0, err
	}
	return
}

type GetStockMaintainResponse struct {
	ID           int    `gorm:"primary_key;column:id;type:int(10) unsigned;not null" json:"id"`
	IndustryName string `gorm:"column:industry_name;type:varchar(255)" json:"industry_name"` // 行业名称
	UserName     string `gorm:"column:user_name;type:varchar(255)" json:"user_name"`         // 用户名
	UserId       string `gorm:"column:user_id;type:varchar(255)" json:"user_id"`             // 用户名 	// 删除时间
}

//股票行业修改(POST)
func EditStockMaintain(code string, id int, user_id string, industry_name, user_name string) (bool, error) {
	var u_p_i UPerformanceIndustry
	var u_i_r_s UStockIndustryRelationship

	models.DB().Table("u_performance_industry").Where("industry_name = ?", industry_name).Find(&u_p_i)
	models.DB().Table("u_stock_industry_relationship").Where("id != ?", id).Where("stock_id = ?", code).Where("industry_id = ?", u_p_i.ID).Find(&u_i_r_s)

	if u_i_r_s.ID > 0 {
		return false, fmt.Errorf("股票行业关系已经存在")
	}

	timeNow := util.GenDateFormat()
	err := models.DB().Table("u_performance_industry").Where("industry_name = ?", industry_name).Find(&u_p_i).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		//没有找到,则创建新的
		u_p_i.IndustryName = industry_name
		models.DB().Table("u_performance_industry").Create(&u_p_i)
	}

	dbs := models.DB().Table("u_stock_industry_relationship").Where("id = ?", id)
	err = dbs.Updates(map[string]interface{}{
		"industry_id": u_p_i.ID,
		"user_id":     user_id,
		"updated_at":  timeNow,
	}).Error
	if err != nil {
		return false, fmt.Errorf("股票行业唯一")
	}
	return true, nil
}

//股票行业删除
func DelStockMaintain(id int, code string) (bool, error) {
	var err error
	//删除u_stock_industry_relationship中对应关系,并删除u_industry中对应数据
	tx := models.DB().Begin()
	err = tx.Exec("delete from u_stock_industry_relationship where id = ?", id).Error
	if err != nil {
		tx.Rollback()
		return false, err
	}
	//err = tx.Exec("delete from u_performance_edit where id = ?", id).Error
	//if err != nil {
	//	tx.Rollback()
	//	return false, err
	//}
	tx.Commit()
	return true, nil
}

//导入excel
func Import(r io.Reader) (sucNum, failNum int, err error) {
	var (
		success bool
	)
	xlsx, err := excelize.OpenReader(r)
	if err != nil {
		fmt.Println(err.Error())
		return 0, 0, err
	}
	rows := xlsx.GetRows("Sheet1")
	for irow, row := range rows {
		if irow > 0 {
			var data []string
			for _, cell := range row {
				data = append(data, cell)
			}
			success, err = SaveStockMaintain(data[0], data[1], data[2])
			if err != nil {
				fmt.Printf("err:", err.Error())
			}
			if success {
				sucNum++
			} else {
				failNum++
			}
		}
	}
	return sucNum, failNum, nil
}

/*--------------------------------------------下面是行业维护页面------------------------------------------------*/

func SaveIndustry(industry_name, industry_interpretation string) (bool, error) {
	//先查后增加,保证行业名称唯一
	var (
		u_p_i UPerformanceIndustry
		err   error
	)

	models.DB().Table("u_performance_industry").Select("id").Where("industry_name = ?", industry_name).Find(&u_p_i)
	if u_p_i.ID > 0 {
		return false, fmt.Errorf("行业名称重复")
	} else {

		dateTime, _ := util.GenDateFormatTime()
		u_p_i.IndustryName = industry_name
		u_p_i.IndustryInterpretation = industry_interpretation
		u_p_i.CreatedAt = dateTime
		u_p_i.UpdatedAt = dateTime
		if err != nil {
			return false, err
		}

		err = models.DB().Table("u_performance_industry").Create(&u_p_i).Error
		if err != nil {
			return false, err
		}
		return true, nil
	}
}

func SearchIndustry(industry string, page, page_size int) ([]UPerformanceIndustry, int, error) {
	var (
		res   []UPerformanceIndustry
		count int
		err   error
	)

	dbs := models.DB().Table("u_performance_industry").Unscoped().Select("id,industry_name,industry_interpretation,updated_at")
	if industry != "" {
		dbs = dbs.Where("industry_name like ?", "%"+industry+"%")
	}
	dbs.Count(&count)
	dbs = dbs.Order("updated_at desc").Offset((page - 1) * page_size).Limit(page_size)
	err = dbs.Find(&res).Error
	if err != nil {
		return nil, 0, err
	}

	return res, count, nil
}

type GetEditIndustrMysql struct {
	Id                     int    `json:"id"`
	IndustryName           string `json:"industry_name"`
	IndustryInterpretation string `json:"industry_interpretation"`
}

//行业修改:post
func PostEditIndustry(id int, industry_interpretation string) (bool, error) {

	//先查后增加,保证行业名称唯一
	err := models.DB().Table("u_performance_industry").Where("id = ?", id).Update(map[string]interface{}{
		"industry_interpretation": industry_interpretation,
	}).Error
	if err != nil {
		return false, err
	}
	return true, nil

}

//行业维护:删除
func DelEditIndustry(id int) (bool, error) {
	err := models.DB().Exec("delete from u_stock_industry_relationship where industry_id = ?", id).Error
	if err != nil {
		return false, err
	}

	err = models.DB().Exec("delete from u_performance_industry where id = ?", id).Error
	if err != nil {
		return false, err
	}

	return true, err
}

/*--------------------------------------------下面是行业跟踪页面------------------------------------------------*/

//定义:净利润/净利润同比/营业总收入/营业总收入同比/扣非净利润/扣非净利润同比/总销售毛利率/单季度销售毛利率 结构体
type NetProfitQuarter struct {
	Code         string
	ReportPeriod string
	//NetProfit                   *float64
	NetProfitQuarter *float64
	//NetProfitGrowthRate *float64
	NetProfitGrowthRateQuarter *float64

	//BusinessRevenue             *float64
	BusinessRevenueQuarter *float64
	//BusinessRevenueGrowthRate   *float64
	BusinessRevenueGrowthRateQuarter *float64

	NetProfitAfterDedQuarter *float64
	//NetProfitAfterDedGrowthRate *float64
	NetProfitAfterDedGrowthRateQuarter *float64

	GrossProfitMargin        *float64
	GrossProfitMarginQuarter *float64
	//CashFlowPerShare            *float64
	CashFlowPerShareQuarter *float64
}

//type NetProfitQuarter struct {
//	ReportPeriod                string
//	NetProfit                   decimal.Decimal
//	NetProfitQuarter           decimal.Decimal
//	NetProfitGrowthRate         decimal.Decimal
//	BusinessRevenue             decimal.Decimal
//	BusinessRevenueQuarter     decimal.Decimal
//	NetProfitAfterDedQuarter   decimal.Decimal
//	NetProfitAfterDedGrowthRate decimal.Decimal
//	GrossProfitMargin           decimal.Decimal
//	GrossProfitMarginQuarter    decimal.Decimal
//	CashFlowPerShare            decimal.Decimal
//}

//type NetProfitQuarter struct {
//	ReportPeriod                string
//	NetProfit                   string
//	NetProfitQuarter            string
//	NetProfitGrowthRate         string
//	BusinessRevenue             string
//	BusinessRevenueQuarter      string
//	NetProfitAfterDedQuarter    string
//	NetProfitAfterDedGrowthRate string
//	GrossProfitMargin           string
//	GrossProfitMarginQuarter    string
//	CashFlowPerShare            string
//}

//行业跟踪:搜索
func SearchIndustryTrack(p performance_service.SearchIndustryTrackRequest) (res []util.SearchIndustryTrackMysql, total int, err error) {

	var (
		b_s     BStock
		s_i_t_m util.SearchIndustryTrackMysql
		//p_s_d       []PStockDate
		u_p_a_u_p_e          []util.UpaUpe
		isDisclosureTimeSort bool
		u_p_a_u_p_e1         []util.UpaUpe
		u_p_a_u_p_e2         []util.UpaUpe
		u_p_a_u_p_e3         []util.UpaUpe

		n_p_q_total []NetProfitQuarter

		//最近4个报告期
		//recentReportPeriod []UPerformanceAnnouncement

		resSult []util.SearchIndustryTrackMysql
	)

	year, _ := strconv.Atoi(p.ReportPeriod[0:4]) //年(报告期)
	date := p.ReportPeriod[5:10]                 //月-日(报告期)
	searchDate := make([]string, 0)              // <当前报告期最近的4个报告期
	switch date {
	case "03-31":
		//eg:2020-03-31 最近四个报告期,2019-12-31(q4),2019-09-30(q3),2019-06-30(q2),2019-03-31(q1)
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-03-31") //q1
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-06-30") //q2
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-09-30") //q3
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-12-31") //q4
	case "06-30":
		//eg:2020-06-30 最近四个报告期,2020-03-31(q1),2019-12-31(q4),2019-09-30(q3),2019-06-30(q2)
		searchDate = append(searchDate, strconv.Itoa(year)+"-03-31")   //q1
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-06-30") //q2
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-09-30") //q3
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-12-31") //q4
	case "09-30":
		//eg:2020-09-30 最近四个报告期,2020-06-30(q2),2020-03-31(q1),2019-12-31(q4),2019-09-30(q3)
		searchDate = append(searchDate, strconv.Itoa(year)+"-03-31")   //q1
		searchDate = append(searchDate, strconv.Itoa(year)+"-06-30")   //q2
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-09-30") //q3
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-12-31") //q4
	case "12-31":
		//eg:2020-12-31 最近四个报告期,2020-09-30(q3),2020-06-30(q2),2020-03-31(q1),2019-12-31(q4)
		searchDate = append(searchDate, strconv.Itoa(year)+"-03-31")   //q1
		searchDate = append(searchDate, strconv.Itoa(year)+"-06-30")   //q2
		searchDate = append(searchDate, strconv.Itoa(year)+"-09-30")   //q3
		searchDate = append(searchDate, strconv.Itoa(year-1)+"-12-31") //q4
	}

	//公告表,预告表,股票行业关系表,行业表，联查
	dbs := models.DB().Table("u_performance_announcement as a").Joins("left join u_performance_forecast as b on a.code = b.code and a.report_period = b.report_period")
	dbs = dbs.Joins("left join u_stock_industry_relationship as c on b.code = c.stock_id")
	dbs = dbs.Joins(" left join u_performance_industry as d on c.industry_id = d.id")
	dbs = dbs.Joins("left join b_user as e on c.user_id = e.id")

	dbs = dbs.Select("a.code,a.name,a.publish_date as a_publish_date ,b.disclosure_time,c.individual_share_track,e.realname,b.performance_incr_upper,b.performance_incr_lower,b.publish_date as f_publish_date,a.net_profit,c.id,d.industry_name")
	//这些字段,包括本期预告(预告表里面的业绩变动幅度上下限),预告时间(预告表里面的公告日期),预告涨幅?(预留,需要计算),本期公告(年度净利润),公告涨幅(需要计算),行业名称,姓名,股票行业关系表的id(后续修改用)

	//输入框条件筛选
	if p.Search != "" {
		if parser.IsNum(p.Search) {
			//纯数字:查看search内容是否是股票代码
			models.DB().Where("id = ?", p.Search).Select("id").Find(&b_s)
			fmt.Printf("b_s:%#v\n", b_s)
			if b_s.ID == p.Search {
				fmt.Println("search内容为股票code")
				dbs = dbs.Where("a.code = ?", p.Search) //公告表中符合code的
			}
		} else {
			//判断search是否是股票名称
			models.DB().Where("name = ?", p.Search).Select("name").Find(&b_s)
			if b_s.Name == p.Search {
				//search 为股票名称
				fmt.Println("search内容为股票名称")
				dbs = dbs.Where("a.name = ?", p.Search) //公告表中符合code的
			} else {
				//判断search是否是行业内容
				err = models.DB().Table("u_performance_industry").Where("industry_name = ?", p.Search).Select("id").Find(&b_s).Error
				if err != nil {
					//没查到,非法内容
					return nil, 0, fmt.Errorf("没有此行业名称")
				}
				//查询行业名称
				dbs = dbs.Where("d.industry_name like ?", "%"+p.Search+"%")
			}
		}
	}
	//报告期条件筛选,
	dbs = dbs.Where("a.report_period = ?", p.ReportPeriod)
	//姓名条件筛选
	if p.UserId != "" {
		//查看指定用户
		dbs = dbs.Where("c.user_id = ?", p.UserId)
	}

	//披露时间排序-数据库排序
	timeNow := util.GenDateFormat2()
	if p.Sort != "" && (p.Sort == "1_asc" || p.Sort == "1_desc" && p.IsFirst == "false") {
		if p.Sort == "1_asc" {
			dbs = dbs.Order("b.disclosure_time asc").Where("b.disclosure_time is not null").Group("a.code").Find(&u_p_a_u_p_e)
		} else {
			dbs = dbs.Order("b.disclosure_time desc").Group("a.code").Find(&u_p_a_u_p_e)
		}
		isDisclosureTimeSort = true
		goto HandleResponseData

	}
	//披露时间默认排序,第一次进入页面情况
	if p.IsFirst == "true" {
		dbs.Where("b.disclosure_time >= ?", timeNow).Order("b.disclosure_time asc").Group("a.code").Find(&u_p_a_u_p_e1)
		dbs.Where("b.disclosure_time is null").Group("a.code").Find(&u_p_a_u_p_e2)
		dbs.Where("b.disclosure_time < ?", timeNow).Group("a.code").Find(&u_p_a_u_p_e3)
		u_p_a_u_p_e = append(u_p_a_u_p_e, u_p_a_u_p_e1...)
		u_p_a_u_p_e = append(u_p_a_u_p_e, u_p_a_u_p_e3...)
		u_p_a_u_p_e = append(u_p_a_u_p_e, u_p_a_u_p_e2...)

		isDisclosureTimeSort = true
		goto HandleResponseData
	}

	//上面筛选出符合条件结果集,下面查询缺少的数据

	//整理返回数据 todo后续优化
HandleResponseData:
	if !isDisclosureTimeSort {
		dbs.Group("a.code").Find(&u_p_a_u_p_e)
	}
	codeArray := make([]string, 0)

	for _, v := range u_p_a_u_p_e {
		codeArray = append(codeArray, v.Code)
	}
	selectStr := "code,net_profit_quarter,net_profit_growth_rate_quarter,business_revenue_growth_rate_quarter,business_revenue_quarter,net_profit_after_ded_growth_rate_quarter,net_profit_after_ded_quarter,gross_profit_margin,gross_profit_margin_quarter,cash_flow_per_share_quarter,report_period"
	dbs = models.DB().Table("u_performance_announcement").Select(selectStr).Where("code in (?)", codeArray).Where("deleted_at is null").Where("report_period in (?)", searchDate).Find(&n_p_q_total)

	for _, v := range u_p_a_u_p_e {
		//组织返回结构体数据
		s_i_t_m.Id = v.Id                                       //股票行业关系表id
		s_i_t_m.Code = v.Code                                   //股票代码
		s_i_t_m.Name = v.Name                                   //股票名称
		s_i_t_m.RealName = v.Realname                           //用户姓名
		s_i_t_m.ExpectedPublishDate = v.DisclosureTime          //披露时间
		s_i_t_m.DisclosureTimeHistory = v.DisclosureTimeHistory //披露时间历史
		s_i_t_m.DisclosureChangeAt = v.DisclosureChangeAt       //披露时间更新时间
		s_i_t_m.IndividualShareTrack = v.IndividualShareTrack   //个股跟踪
		//本期预告(预告表里面的业绩变动幅度上下限)
		if v.PerformanceIncrUpper != nil {
			s_i_t_m.PerformanceIncrUpper = v.PerformanceIncrUpper
		} else {
			s_i_t_m.PerformanceIncrUpper = nil
		}
		if v.PerformanceIncrLower != nil {
			s_i_t_m.PerformanceIncrLower = v.PerformanceIncrLower
		} else {
			s_i_t_m.PerformanceIncrLower = nil
		}

		//预告时间(预告表里面的公告日期)
		s_i_t_m.ForecastPublishDate = v.FPublishDate

		//本期公告
		if v.NetProfit != nil {
			middle := util.DecimalThree(*v.NetProfit)
			s_i_t_m.NetProfitYear = &middle
		} else {
			s_i_t_m.NetProfitYear = nil
		}

		s_i_t_m.IndustryName = v.IndustryName //行业名称
		counter := 0
		for _, vv := range n_p_q_total {
			//如果找到了4个,那么直接停止向下遍历
			//q1
			if v.Code == vv.Code && vv.ReportPeriod[0:10] == searchDate[0] {
				var (
					middle1 *float64
					middle2 *float64
					middle3 *float64
					middle4 *float64
				)
				s_i_t_m.NetProfit1 = vv.NetProfitQuarter
				if vv.NetProfitGrowthRateQuarter != nil {
					middlePtr := *vv.NetProfitGrowthRateQuarter * 100
					middle1 = &middlePtr
				}
				s_i_t_m.NetProfitGrowthRate1 = middle1

				s_i_t_m.BusinessRevenue1 = vv.BusinessRevenueQuarter
				if vv.BusinessRevenueGrowthRateQuarter != nil {
					middlePtr := *vv.BusinessRevenueGrowthRateQuarter * 100
					middle2 = &middlePtr
				}
				s_i_t_m.BusinessRevenueGrowthRate1 = middle2

				s_i_t_m.NetProfitAfterDedQuarter1 = vv.NetProfitAfterDedQuarter
				if vv.NetProfitAfterDedGrowthRateQuarter != nil {
					middlePtr := *vv.NetProfitAfterDedGrowthRateQuarter * 100
					middle3 = &middlePtr
				}
				s_i_t_m.NetProfitAfterDedGrowthRate1 = middle3
				s_i_t_m.CashFlowPerShare1 = vv.CashFlowPerShareQuarter
				if vv.GrossProfitMarginQuarter != nil {
					middlePtr := *vv.GrossProfitMarginQuarter * 100
					middle4 = &middlePtr
				}
				s_i_t_m.GrossProfitMarginQuarter1 = middle4
				counter++
			}
			//q2
			if v.Code == vv.Code && vv.ReportPeriod[0:10] == searchDate[1] {

				var (
					middle1 *float64
					middle2 *float64
					middle3 *float64
					middle4 *float64
				)
				s_i_t_m.NetProfit2 = vv.NetProfitQuarter
				if vv.NetProfitGrowthRateQuarter != nil {
					middlePtr := *vv.NetProfitGrowthRateQuarter * 100
					middle1 = &middlePtr
				}
				s_i_t_m.NetProfitGrowthRate2 = middle1
				s_i_t_m.BusinessRevenue2 = vv.BusinessRevenueQuarter
				if vv.BusinessRevenueGrowthRateQuarter != nil {
					middlePtr := *vv.BusinessRevenueGrowthRateQuarter * 100
					middle2 = &middlePtr
				}
				s_i_t_m.BusinessRevenueGrowthRate2 = middle2

				s_i_t_m.NetProfitAfterDedQuarter2 = vv.NetProfitAfterDedQuarter
				if vv.NetProfitAfterDedGrowthRateQuarter != nil {
					middlePtr := *vv.NetProfitAfterDedGrowthRateQuarter * 100
					middle3 = &middlePtr
				}
				s_i_t_m.NetProfitAfterDedGrowthRate2 = middle3
				s_i_t_m.CashFlowPerShare2 = vv.CashFlowPerShareQuarter
				if vv.GrossProfitMarginQuarter != nil {
					middlePtr := *vv.GrossProfitMarginQuarter * 100
					middle4 = &middlePtr
				}
				s_i_t_m.GrossProfitMarginQuarter2 = middle4
				counter++
			}
			//q3
			if v.Code == vv.Code && vv.ReportPeriod[0:10] == searchDate[2] {
				var (
					middle1 *float64
					middle2 *float64
					middle3 *float64
					middle4 *float64
				)
				s_i_t_m.NetProfit3 = vv.NetProfitQuarter
				if vv.NetProfitGrowthRateQuarter != nil {
					middlePtr := *vv.NetProfitGrowthRateQuarter * 100
					middle1 = &middlePtr
				}
				s_i_t_m.NetProfitGrowthRate3 = middle1
				s_i_t_m.BusinessRevenue3 = vv.BusinessRevenueQuarter
				if vv.BusinessRevenueGrowthRateQuarter != nil {
					middlePtr := *vv.BusinessRevenueGrowthRateQuarter * 100
					middle2 = &middlePtr
				}
				s_i_t_m.BusinessRevenueGrowthRate3 = middle2

				s_i_t_m.NetProfitAfterDedQuarter3 = vv.NetProfitAfterDedQuarter
				if vv.NetProfitAfterDedGrowthRateQuarter != nil {
					middlePtr := *vv.NetProfitAfterDedGrowthRateQuarter * 100
					middle3 = &middlePtr
				}
				s_i_t_m.NetProfitAfterDedGrowthRate3 = middle3
				s_i_t_m.CashFlowPerShare3 = vv.CashFlowPerShareQuarter
				if vv.GrossProfitMarginQuarter != nil {
					middlePtr := *vv.GrossProfitMarginQuarter * 100
					middle4 = &middlePtr
				}
				s_i_t_m.GrossProfitMarginQuarter3 = middle4
				counter++
			}
			//q4
			if v.Code == vv.Code && vv.ReportPeriod[0:10] == searchDate[3] {
				var (
					middle1 *float64
					middle2 *float64
					middle3 *float64
					middle4 *float64
				)
				s_i_t_m.NetProfit4 = vv.NetProfitQuarter
				if vv.NetProfitGrowthRateQuarter != nil {
					middlePtr := *vv.NetProfitGrowthRateQuarter * 100
					middle1 = &middlePtr
				}
				s_i_t_m.NetProfitGrowthRate4 = middle1
				s_i_t_m.BusinessRevenue4 = vv.BusinessRevenueQuarter
				if vv.BusinessRevenueGrowthRateQuarter != nil {
					middlePtr := *vv.BusinessRevenueGrowthRateQuarter * 100
					middle2 = &middlePtr
				}
				s_i_t_m.BusinessRevenueGrowthRate4 = middle2

				s_i_t_m.NetProfitAfterDedQuarter4 = vv.NetProfitAfterDedQuarter
				if vv.NetProfitAfterDedGrowthRateQuarter != nil {
					middlePtr := *vv.NetProfitAfterDedGrowthRateQuarter * 100
					middle3 = &middlePtr
				}
				s_i_t_m.NetProfitAfterDedGrowthRate4 = middle3
				s_i_t_m.CashFlowPerShare4 = vv.CashFlowPerShareQuarter
				if vv.GrossProfitMarginQuarter != nil {
					middlePtr := *vv.GrossProfitMarginQuarter * 100
					middle4 = &middlePtr
				}
				s_i_t_m.GrossProfitMarginQuarter4 = middle4
				counter++
			}
			if counter > 4 {
				break
			}
		}
		res = append(res, s_i_t_m)
	}
	//所有数据字段排序,这个地方其他字段排序,需要全部查询
	if isDisclosureTimeSort {
		resSult = res
	} else {
		resSult, _ = util.StructSort4(res, p.Sort)
	}

	//总条数
	total = len(resSult)
	//切片分页
	startIndex := (p.Page - 1) * p.PageSize //开始位置
	endIndex := startIndex + p.PageSize     //结束位置
	if endIndex <= total {
		//返回部分
		return resSult[startIndex:endIndex], total, nil
	} else {
		//返回后面全部
		return resSult[startIndex:], total, nil
	}
	//return
}

//行业跟踪:修改
func PostEditIndustryTrack(code string, individual_track string, report_period string) (bool, error) {
	//u_p_e表中没有则新增，有则更新
	uPerformanceEdit := UPerformanceEdit{}
	err := models.DB().Table("u_performance_edit").Select("id").Where("code = ?", code).Where("report_period = ?", report_period).Find(&uPerformanceEdit).Error
	if uPerformanceEdit.ID > 0 {
		err = models.DB().Table("u_performance_edit").Where("code = ?", code).Where("report_period = ?", report_period).Update(map[string]interface{}{
			"individual_track": individual_track,
		}).Error
	} else {
		uPerformanceEdit.Code = code
		uPerformanceEdit.ReportPeriod = report_period
		uPerformanceEdit.IndividualTrack = individual_track
		err = models.DB().Table("u_performance_edit").Create(&uPerformanceEdit).Error
	}
	if err != nil {
		return false, err
	}
	return true, nil
}

//获取当前时间所属的报告期
func GetRecentReportPeriod(nowDate string) string {
	var dateSwitch string
	//当前时间(年月日)

	year := nowDate[0:4]
	date := nowDate[5:10]
	basic, _ := strconv.Atoi(year) //年

	dateSlice := strings.Split(date, "-")
	//fmt.Println(dateSlice)
	dateStr := dateSlice[0] + dateSlice[1]
	//fmt.Println(dateStr)

	dateInt, _ := strconv.Atoi(dateStr)
	dateInt = basic + dateInt //年（2020） + 日期（0617）
	//fmt.Println(dateInt)

	//fmt.Println("year", year) //2020
	//fmt.Println("date", date) // 06-17
	//fmt.Println("baisc", basic)

	//03-31  报告期截止为(04-30)；//2020年 04-16 ~ 04-30 展示 03-31报告期
	//06-30  报告期截止为(08-31)；//2020年 05-01 ~ 08-31 展示 06-30报告期
	//09-30  报告期截止为(10-31)；//2020年 09-01 ~ 10-31 展示 09-30报告期
	//12-31  报告期截止为(04-15) //2020年 11-01 ~ 2021年 04-15 展示 2020年 12-31报告期

	//算出当前日期属于哪个报告期
	switch {
	case dateInt >= basic+101 && dateInt <= basic+415:
		dateSwitch = strconv.Itoa(basic-1) + "-12-31"
	case dateInt >= basic+416 && dateInt <= basic+430:
		dateSwitch = year + "-03-31"
	case dateInt >= basic+501 && dateInt <= basic+831:
		dateSwitch = year + "-06-30"
	case dateInt >= basic+901 && dateInt <= basic+1031:
		dateSwitch = year + "-09-30"
	case dateInt >= basic+1101 && dateInt <= basic+1231:
		dateSwitch = year + "-12-31"
	}

	return dateSwitch
}
