package template

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/research_report_service"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"strconv"
	"strings"
	"time"
)

// UResearchReport 研报表
type UResearchReport struct {
	models.Base
	Code            string    `gorm:"index;column:code;type:varchar(10);not null" json:"code"`  // 证券代码
	SeedID          int       `gorm:"column:seed_id;type:int(20)" json:"seed_id"`               // 源id
	Name            string    `gorm:"column:name;type:varchar(16)" json:"name"`                 // 股票名称
	Type            string    `gorm:"column:type;type:varchar(16)" json:"type"`                 // 研报类型
	Organization    string    `gorm:"column:organization;type:varchar(16)" json:"organization"` // 机构名称
	Author          string    `gorm:"column:author;type:varchar(16)" json:"author"`             // 作者
	Title           string    `gorm:"column:title;type:varchar(255)" json:"title"`              // 标题
	Level           string    `gorm:"column:level;type:varchar(16)" json:"level"`               // 评级
	LevelChange     string    `gorm:"column:level_change;type:varchar(16)" json:"level_change"` // 评级变动
	Industry        string    `gorm:"index;column:industry;type:varchar(16)" json:"industry"`   // 行业
	SourceLink      string    `gorm:"column:source_link;type:varchar(512)" json:"source_link"`  // pdf网站链接
	PublishDate     time.Time `gorm:"column:publish_date;type:date" json:"publish_date"`        // 发布日期
	Recommend       int       `gorm:"column:recommend;type:int(11)" json:"recommend"`           // 推荐，0 不推荐 ，1推荐
	Parent          int       `gorm:"column:parent;type:int(10)" json:"parent"`                 // 父级数据id
	CatchTime       time.Time `gorm:"column:catch_time;type:datetime" json:"catch_time"`        // 拿到任务时间
	SpiderTime      time.Time `gorm:"column:spider_time;type:datetime" json:"spider_time"`      // 抓取完成时间
	RecommendUserId string    `json:"recommend_user_id"`
}

// UResearchReportData 研报数据表
type UResearchReportData struct {
	models.Base
	Code              string  `gorm:"index;index:IDX_UNION;column:code;type:varchar(10);not null" json:"code"`  // 证券代码
	SeedID            int64   `gorm:"column:seed_id;type:bigint(20)" json:"seed_id"`                            // 源id
	Name              string  `gorm:"column:name;type:varchar(16)" json:"name"`                                 // 股票名称
	ReportID          int     `gorm:"column:report_id;type:int(10)" json:"report_id"`                           // 研报表主键id
	ExpectedNetProfit float64 `gorm:"column:expected_net_profit;type:decimal(16,2)" json:"expected_net_profit"` // 预测净利润（元）
	PreClose          float64 `gorm:"column:pre_close;type:decimal(10,2)" json:"pre_close"`                     // 昨收价(元)

	TargetPrice        float64   `gorm:"column:target_price;type:decimal(10,2)" json:"target_price"`               // 目标价格
	RisingSpace        float64   `gorm:"column:rising_space;type:float(10,4)" json:"rising_space"`                 // 上涨空间( 小数)
	CompoundGrowthRate float64   `gorm:"column:compound_growth_rate;type:float(10,2)" json:"compound_growth_rate"` // 复合增长率(小数)
	ViewPoint          string    `gorm:"column:view_point;type:varchar(255)" json:"view_point"`                    // 观点
	Abstract           string    `gorm:"column:abstract;type:varchar(255)" json:"abstract"`                        // 摘要
	EarningsPerShare   float64   `gorm:"column:earnings_per_share;type:float(10,2)" json:"earnings_per_share"`     // 每股收益
	PriceEarningRatio  float64   `gorm:"column:price_earning_ratio;type:float(10,2)" json:"price_earning_ratio"`   // 市盈率（预测pe）
	ExpectedYear       int       `gorm:"index:IDX_UNION;column:expected_year;type:int(11)" json:"expected_year"`   // 预测年度
	PublishDate        time.Time `gorm:"column:publish_date;type:date" json:"publish_date"`                        // 发布日期
}

// PPublicCompany 上市公司
type PPublicCompany struct {
	ID                    string    `gorm:"primary_key;column:id;type:varchar(20);not null" json:"-"`
	StockCode             string    `gorm:"column:stock_code;type:varchar(100)" json:"stock_code"`                           // 股票代码
	StockName             string    `gorm:"column:stock_name;type:varchar(20)" json:"stock_name"`                            // 股票名称
	Name                  string    `gorm:"column:name;type:varchar(100)" json:"name"`                                       // 公司名称
	Region                string    `gorm:"column:region;type:varchar(100)" json:"region"`                                   // 所属地域
	Industry              string    `gorm:"column:industry;type:varchar(100)" json:"industry"`                               // 所属行业
	Business              string    `gorm:"column:business;type:varchar(1000)" json:"business"`                              // 主营业务
	ProductName           string    `gorm:"column:product_name;type:varchar(1000)" json:"product_name"`                      // 产品名称
	ControlShareholder    string    `gorm:"column:control_shareholder;type:varchar(100)" json:"control_shareholder"`         // 控股股东
	ControlShareholderPct string    `gorm:"column:control_shareholder_pct;type:varchar(100)" json:"control_shareholder_pct"` // 控股股东持股比例
	ActualController      string    `gorm:"column:actual_controller;type:varchar(100)" json:"actual_controller"`             // 实际控制人
	ActualControllerPct   string    `gorm:"column:actual_controller_pct;type:varchar(100)" json:"actual_controller_pct"`     // 实际控制人持股比例
	FinalController       string    `gorm:"column:final_controller;type:varchar(100)" json:"final_controller"`               // 最终控制人
	FinalControllerPct    string    `gorm:"column:final_controller_pct;type:varchar(100)" json:"final_controller_pct"`       // 最终控制人持股比例
	LastNoticeDate        time.Time `gorm:"column:last_notice_date;type:datetime" json:"last_notice_date"`                   // 最新公告日期
	JoinCompanys          string    `gorm:"column:join_companys;type:varchar(1000)" json:"join_companys"`                    // 参股公司
	JoinRelations         string    `gorm:"column:join_relations;type:varchar(1000)" json:"join_relations"`                  // 参股关系
	JoinCompanyJSON       string    `gorm:"column:join_company_json;type:longtext" json:"join_company_json"`                 // 参股数据
	Zgb                   float64   `gorm:"column:zgb;type:decimal(15,1)" json:"zgb"`                                        // 总股本
	Ltg                   float64   `gorm:"column:ltg;type:decimal(15,1)" json:"ltg"`                                        // 流通a股
	ZgbHolder             string    `gorm:"column:zgb_holder;type:text" json:"zgb_holder"`                                   // 十大股东
	LtgHolder             string    `gorm:"column:ltg_holder;type:text" json:"ltg_holder"`                                   // 十大流通股东
	ZgbHolderSummary      string    `gorm:"column:zgb_holder_summary;type:varchar(500)" json:"zgb_holder_summary"`           // 十大股东总结
	LtgHolderSummary      string    `gorm:"column:ltg_holder_summary;type:varchar(500)" json:"ltg_holder_summary"`           // 十大流通股东总结
	ZgbHolderInfo         string    `gorm:"column:zgb_holder_info;type:text" json:"zgb_holder_info"`                         // 十大股东信息
	LtgHolderInfo         string    `gorm:"column:ltg_holder_info;type:text" json:"ltg_holder_info"`                         // 十大流通股东信息
	ZgbHolderPubDate      string    `gorm:"column:zgb_holder_pub_date;type:varchar(100)" json:"zgb_holder_pub_date"`         // 十大股东总结
	LtgHolderPubDate      string    `gorm:"column:ltg_holder_pub_date;type:varchar(100)" json:"ltg_holder_pub_date"`         // 十大流通股东总结
	Hash                  string    `gorm:"column:hash;type:varchar(200)" json:"hash"`                                       // hash
	IsB                   int8      `gorm:"column:is_b;type:tinyint(4)" json:"is_b"`                                         // B股
	OwnerID               int64     `gorm:"column:owner_id;type:bigint(20)" json:"owner_id"`                                 // 所有人
	Updated               time.Time `gorm:"column:updated;type:timestamp" json:"updated"`                                    // 更新时间
	Created               time.Time `gorm:"column:created;type:timestamp" json:"created"`                                    // 创建时间
	Creator               int64     `gorm:"column:creator;type:bigint(20)" json:"creator"`                                   // 创建人
	Status                string    `gorm:"column:status;type:varchar(50)" json:"status"`                                    // 状态
	Deleted               int8      `gorm:"column:deleted;type:tinyint(4)" json:"deleted"`                                   // 已删除
}

type UIndustrySecuritiesAuthorRelationship struct {
	ID           int    `gorm:"primary_key;column:id;type:int(10) unsigned;not null" json:"-"`
	IndustryName string `gorm:"column:industry_name;type:varchar(255)" json:"industry_name"` // 行业名称
	Securities   string `gorm:"column:securities;type:varchar(255)" json:"securities"`       // 重要机构(券商)
	Author       string `gorm:"column:author;type:varchar(255)" json:"author"`               // 重要作者
}

func PageAndPageSize(Page, PageSize int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Limit(PageSize).Offset((Page - 1) * PageSize)
	}
}

type HigtLight struct {
	TitleHigtLight   string `json:"title_higt_light"`
	ContentHighLight string `json:"content_high_light"`
}
type GetResearchReportMysql struct {
	Code         string `json:"code"`         //股票代码
	Name         string `json:"name"`         //股票名称
	Organization string `json:"organization"` //机构
	Author       string `json:"author"`       //作者
	Level        string `json:"level"`        //评级

	PreClose                 *float64 `json:"pre_close"`    //昨收价
	TargetPrice              *string  `json:"target_price"` //目标价格
	TargetPriceSorted        *float64 `json:"target_price_sorted"`
	RisingSpace              *string  `json:"rising_space"` //上涨空间
	RisingSpaceSorted        *float64 `json:"rising_space_sorted"`
	Title                    string   `json:"title"`               //观点
	ExpectedNetProfit        *string  `json:"expected_net_profit"` //预测净利润
	ExpectedNetProfitSorted  *float64 `json:"expected_net_profit_sorted"`
	ForecastEps              *string  `json:"forecast_eps"` //预测eps
	ForecastEpsSorted        *float64 `json:"forecast_eps_sorted"`
	PriceEarningRatio        *string  `json:"price_earning_ratio"`        //预测PE
	PriceEarningRatioSorted  *float64 `json:"price_earning_ratio_sorted"` //预测PE
	Heat                     int      `json:"heat"`                       //热度
	CompoundGrowthRate       *string  `json:"compound_growth_rate"`       //复合增长率
	CompoundGrowthRateSorted *float64 `json:"compound_growth_rate_sorted"`
	Content                  string   `json:"content"` //研报内容

	PublishDate string `json:"publish_date"` //日期
	Recommend   uint   `json:"recommend"`    //推荐
}

func (g *GetResearchReportMysql) AfterFind() (err error) {
	return
}

func GetResearchReport(req research_report_service.GetResearchReportRequest) ([]map[string]interface{}, int, error) {

	var (
		res       []util.GetResearchReportMysql
		response  []util.GetResearchReportMysql
		selectStr string
		resp      []map[string]interface{}
		count     int
		industry  string
		highLight HigtLight
		ctx       = "<span style=\"color:red;font-weight:bold\">"
		edx       = "</span>"
		err       error
	)

	dbs := models.DB().Table("u_research_report AS a").
		Joins("LEFT JOIN u_stock_industry ON u_stock_industry.code=a.code").
		Joins("LEFT JOIN u_research_report_data AS b ON a.id = b.report_id").
		Joins("LEFT JOIN p_public_company AS c on a.code = c.stock_code")
	//Joins("LEFT JOIN u_industry_securities_author_relationship as d on d.securities = a.organization and d.industry_name = substring_index(c.industry,'—',1)")
	//select字段
	selectStr = getSelectStr()
	dbs = dbs.Select(selectStr)
	//where条件构建
	dbs, err = dbsWhereBuild(req, &highLight, dbs)
	if err != nil {
		return resp, 0, err
	}

	orderArray := strings.Split(req.Sort, "_")
	orderNum, _ := strconv.Atoi(orderArray[0])

	//排序,如果有排序,那么需要把所有数据都查出来,内存排序
	if !util.IsEmpty(req.Sort) && orderNum < 6 {
		err = dbs.Group("a.id").Find(&res).Error
		if err != nil && err != gorm.ErrRecordNotFound {
			logging.Error("models\\research_report.go GetResearchReport() err:", err.Error())
			return resp, 0, err
		}

		//字段排序,需要每次全部取出,之后排序
		for k, _ := range res {
			//目标价(需要内部排序,小在前,大在后)
			if res[k].TargetPrice != nil {
				sortFieldArray := strings.Split(*res[k].TargetPrice, ",")
				//排序,string slice排序
				sortFieldFloat64Array := util.SortStrSlice(&sortFieldArray, "asc")
				//转化成字符串
				sortFieldStr := ""
				for _, v := range sortFieldFloat64Array {
					sortFieldStr += util.Ftoa(v) + ","
				}
				trimSortFieldStr := strings.Trim(sortFieldStr, ",")
				res[k].TargetPrice = &trimSortFieldStr
				if len(sortFieldFloat64Array) == 2 {
					res[k].TargetPriceSorted = &sortFieldFloat64Array[1]
				} else {
					res[k].TargetPriceSorted = &sortFieldFloat64Array[0]
				}
			}
			//上涨空间,内部排序
			if res[k].RisingSpace != nil {
				//sortFieldArray := strings.Split(*res[k].RisingSpace, ",")

				sortFieldArray := strings.Split(*res[k].RisingSpace, ",")
				//排序,string slice排序
				sortFieldFloat64Array := util.SortStrSlice(&sortFieldArray, "asc")
				//转化成字符串
				sortFieldStr := ""
				for _, v := range sortFieldFloat64Array {
					//sortFieldStr += util.Ftoa(v) + ","
					sortFieldStr += fmt.Sprintf("%.4f", v) + ","
				}
				trimSortFieldStr := strings.Trim(sortFieldStr, ",")
				res[k].RisingSpace = &trimSortFieldStr
				if len(sortFieldFloat64Array) == 2 {
					res[k].RisingSpaceSorted = &sortFieldFloat64Array[1]
				} else {
					res[k].RisingSpaceSorted = &sortFieldFloat64Array[0]
				}
			}
			//预测净利润
			if res[k].ExpectedNetProfit != nil {
				sortFieldArray := strings.Split(*res[k].ExpectedNetProfit, ",")
				middle := util.Atof(sortFieldArray[0])
				res[k].ExpectedNetProfitSorted = &middle
				res[k].ExpectedNetProfitRaise = getExpectedNetProfit(res[k])
			}
			//预测eps
			if res[k].ForecastEps != nil {
				sortFieldArray := strings.Split(*res[k].ForecastEps, ",")
				middle := util.Atof(sortFieldArray[0])
				res[k].ForecastEpsSorted = &middle
			}
			//预测pe
			if res[k].PriceEarningRatio != nil {
				sortFieldArray := strings.Split(*res[k].PriceEarningRatio, ",")
				middle := util.Atof(sortFieldArray[0])
				res[k].PriceEarningRatioSorted = &middle
			}

			//研报内容按照段落转成数组,返回两个关键词,每个关键词 前后各一百字,取两段
			if res[k].Content != "" && req.Content != "" {
				//关键词出现的第一段
				res[k].ContentArray = append(res[k].ContentArray, util.SubKeyWordBeforeBehind100(res[k].Content, req.Content))
				//关键词出现的第二段
				res[k].ContentArray = append(res[k].ContentArray, util.SubKeyWordBeforeBehind100Second(res[k].Content, req.Content))
			}

			middleContentArray := make([]string, 0)
			//内容关键词高亮
			for _, v := range res[k].ContentArray {
				middleContentArray = append(middleContentArray, strings.Replace(v, highLight.ContentHighLight, ctx+highLight.ContentHighLight+edx, -1))
			}
			res[k].ContentArray = middleContentArray
			//关键词搜索,标题高亮
			if !util.IsEmpty(res[k].Title) && !util.IsEmpty(highLight.TitleHigtLight) {
				//关键词高亮
				res[k].Title = strings.Replace(res[k].Title, highLight.TitleHigtLight, ctx+highLight.TitleHigtLight+edx, -1)
			}

		}
		//排序
		resSorted, _ := util.StructSort7(res, req.Sort)

		//切片分页
		count = len(resSorted)
		startIndex := (req.Page - 1) * req.PageSize //开始位置
		endIndex := startIndex + req.PageSize       //结束位置
		if endIndex <= count {
			//返回部分
			response = resSorted[startIndex:endIndex]
		} else {
			//返回后面全部
			response = resSorted[startIndex:]
		}
		//计算重点作者
		for k, v := range response {
			//处理首次推荐
			if response[k].LevelChange == "首次" {
				response[k].Level = response[k].Level + "(" + response[k].LevelChange + ")"
			}

			//重点作者
			authorArray := strings.Split(v.Author, ",")
			if !util.IsEmpty(v.Industry) {
				industry = strings.Split(v.Industry, "—")[0]
			}
			for _, vv := range authorArray {
				var u_i_s_a_r_2 UIndustrySecuritiesAuthorRelationship
				models.DB().Table("u_industry_securities_author_relationship").Select("id").Where("author = ?", vv).Where("industry_name = ?", industry).Where("securities = ?", v.Organization).Find(&u_i_s_a_r_2)
				if u_i_s_a_r_2.ID > 0 {
					response[k].ImportantAuthor = true
				}
			}

			//推荐过的用户id
			if !util.IsEmpty(v.RecommendUserId) {
				middle := strings.Split(v.RecommendUserId, ",")
				for _, vvv := range middle {
					RecommendUserIdInt, _ := strconv.Atoi(vvv)
					response[k].RecommendUserIdArray = append(response[k].RecommendUserIdArray, RecommendUserIdInt)
				}
			}
			//评级变动,箭头: 向上，向下
			response[k].LevelStatus = response[k].LevelCalc
			//返回空数组
			if util.IsEmpty(response[k].RecommendUserIdArray) {
				response[k].RecommendUserIdArray = []int{}
			}
			if response[k].TargetPrice != nil {
				middleTargetPrice := strings.Trim(*response[k].TargetPrice, ",")
				response[k].TargetPrice = &middleTargetPrice
			}
			if !util.IsEmpty(response[k].PublishDate) {
				response[k].PublishDate = response[k].PublishDate[0:10]
			}

			//处理结果-单位转换
			resp = append(resp, util.Struct2RoundedMapByTag(response[k]))
		}
		for i, _ := range resp {
			if resp[i]["target_price"] != nil {
				preClose := make([]float64, 0)
				err := models.DB().Table("p_stock_tick").Where("stock_code=?", resp[i]["code"]).Where("trade_date=?", resp[i]["publish_date"]).Pluck("pre_close", &preClose).Error
				if err != nil {
					logging.Error("研报获取昨收价报错：", err.Error())
				}
				if len(preClose) > 0 {
					resp[i]["pre_close"] = fmt.Sprintf("%2f", preClose[0])
				}
			}
		}
		return resp, count, nil
	} else {
		//发布日期排序,热度排序,每次只查20条
		if orderNum == 8 {
			//热度排序
			dbs = dbs.Order("a.heat " + orderArray[1])
		}
		if orderNum == 7 {
			//发布日期排序
			dbs = dbs.Order("a.spider_time " + orderArray[1] + ",a.created_at " + orderArray[1])
		}
		err = dbs.Group("a.id").Count(&count).Scopes(PageAndPageSize(req.Page, req.PageSize)).Find(&res).Error
		if err != nil && err != gorm.ErrRecordNotFound {
			logging.Error("models\\research_report.go GetResearchReport() err:", err.Error())
			return resp, 0, err
		}
		for k, v := range res {
			//处理首次推荐
			if res[k].LevelChange == "首次" {
				res[k].Level = res[k].Level + "(" + res[k].LevelChange + ")"
			}

			if res[k].TargetPrice != nil {
				sortFieldArray := strings.Split(*res[k].TargetPrice, ",")
				//排序,转float排序
				sortFieldFloat64Array := util.SortStrSlice(&sortFieldArray, "asc")
				//转化成字符串
				sortFieldStr := ""
				for _, v := range sortFieldFloat64Array {
					sortFieldStr += util.Ftoa(v) + ","
				}
				res[k].TargetPrice = &sortFieldStr
			}

			//研报内容按照段落转成数组,返回两个关键词,每个关键词 前后各一百字,取两段
			if res[k].Content != "" && req.Content != "" {
				//关键词出现的第一段
				res[k].ContentArray = append(res[k].ContentArray, util.SubKeyWordBeforeBehind100(res[k].Content, req.Content))

				//关键词出现的第二段
				res[k].ContentArray = append(res[k].ContentArray, util.SubKeyWordBeforeBehind100Second(res[k].Content, req.Content))

			}
			middleContentArray := make([]string, 0)
			//内容关键词高亮
			for _, v := range res[k].ContentArray {
				middleContentArray = append(middleContentArray, strings.Replace(v, highLight.ContentHighLight, ctx+highLight.ContentHighLight+edx, -1))
			}
			res[k].ContentArray = middleContentArray

			//关键词搜索,标题高亮
			if !util.IsEmpty(res[k].Title) && !util.IsEmpty(highLight.TitleHigtLight) {
				//关键词高亮
				res[k].Title = strings.Replace(res[k].Title, highLight.TitleHigtLight, ctx+highLight.TitleHigtLight+edx, -1)
			}

			//重点作者
			authorArray := strings.Split(v.Author, ",")
			if !util.IsEmpty(v.Industry) {
				industry = strings.Split(v.Industry, "—")[0]
			}
			for _, vv := range authorArray {
				var u_i_s_a_r_2 UIndustrySecuritiesAuthorRelationship
				models.DB().Table("u_industry_securities_author_relationship").Select("id").Where("author = ?", vv).Where("industry_name = ?", industry).Where("securities = ?", v.Organization).Find(&u_i_s_a_r_2)
				if u_i_s_a_r_2.ID > 0 {
					res[k].ImportantAuthor = true
				}
			}

			//推荐过的用户id
			if !util.IsEmpty(v.RecommendUserId) {
				middle := strings.Split(v.RecommendUserId, ",")
				for _, vvv := range middle {
					RecommendUserIdInt, _ := strconv.Atoi(vvv)
					res[k].RecommendUserIdArray = append(res[k].RecommendUserIdArray, RecommendUserIdInt)
				}
			}

			//评级变动,箭头: 向上，向下
			res[k].LevelStatus = res[k].LevelCalc

			if util.IsEmpty(res[k].RecommendUserIdArray) {
				res[k].RecommendUserIdArray = []int{}
			}

			if res[k].TargetPrice != nil {
				middleTargetPrice := strings.Trim(*res[k].TargetPrice, ",")
				res[k].TargetPrice = &middleTargetPrice
			}
			if !util.IsEmpty(res[k].PublishDate) {
				res[k].PublishDate = res[k].PublishDate[0:10]
			}
			//预测净利润
			if res[k].ExpectedNetProfit != nil {
				res[k].ExpectedNetProfitRaise = getExpectedNetProfit(res[k])
			}
			resp = append(resp, util.Struct2RoundedMapByTag(res[k]))
		}
		for i, _ := range resp {
			if resp[i]["target_price"] != nil {
				preClose := make([]float64, 0)
				err := models.DB().Table("p_stock_tick").Where("stock_code=?", resp[i]["code"]).Where("trade_date=?", resp[i]["publish_date"]).Pluck("pre_close", &preClose).Error
				if err != nil {
					logging.Error("研报获取昨收价报错：", err.Error())
				}
				if len(preClose) > 0 {
					resp[i]["pre_close"] = fmt.Sprintf("%.2f", preClose[0])
				}
			}
		}
		return resp, count, nil
	}
}

func getSelectStr() string {
	//selectStr := "a.id,a.code,a.name,a.organization,a.author,a.level,a.level_change,a.level_calc,a.publish_date,a.spider_time, a.recommend,a.title,a.source_link,a.recommend_user_id,a.heat,a.content,a.type,a.pdf_first_coverage,a.pdf_raise,b.pre_close,c.industry,"
	selectStr := "a.*, b.pre_close, c.industry,u_stock_industry.head,"
	selectStr += "GROUP_CONCAT( b.target_price ORDER BY b.id) AS target_price,"
	selectStr += "GROUP_CONCAT( b.rising_space ORDER BY b.id) AS rising_space,"
	selectStr += "GROUP_CONCAT( b.price_earning_ratio ORDER BY b.id) AS price_earning_ratio,"
	selectStr += "GROUP_CONCAT( b.compound_growth_rate ORDER BY b.id) AS compound_growth_rate,"
	selectStr += "GROUP_CONCAT( b.expected_net_profit ORDER BY b.id) AS expected_net_profit,"
	selectStr += "GROUP_CONCAT( b.earnings_per_share ORDER BY b.id) AS forecast_eps" //暂时这样,后续可以修改,将参数名换掉,保持一致
	return selectStr
}
func dbsWhereBuild(req research_report_service.GetResearchReportRequest, highLight *HigtLight, dbs *gorm.DB) (*gorm.DB, error) {
	//无重复
	dbs = dbs.Where("a.parent = ? or a.parent is null", 0)
	//股票代码只需要6位数  6开头，3开头，0开头的
	dbs = dbs.Where("a.code like ? or a.code like ? or a.code like ? or a.code = ?", "6%", "3%", "0%", "")
	//机构名称
	if !util.IsEmpty(req.Organization) {
		dbs = dbs.Where("a.organization LIKE ?", "%"+req.Organization+"%")
	}
	//股票代码 or 股票名称
	if !util.IsEmpty(req.Search) {
		typeNum, err := SearchType(req.Search)
		if err != nil {
			logging.Error("models\\research_report.go GetResearchReport() err:", err.Error())
			return dbs, err
		}
		if typeNum == 1 {
			//股票代码
			dbs = dbs.Where("a.code = ?", req.Search)
		} else if typeNum == 2 {
			//股票名称
			dbs = dbs.Where("a.name = ?", req.Search)
		} else {
			return dbs, fmt.Errorf("不知道的搜索内容")
		}
	}
	//关键词,搜观点,观点就是标题
	if !util.IsEmpty(req.KeyWord) {
		highLight.TitleHigtLight = req.KeyWord
		dbs = dbs.Where("a.title LIKE ?", "%"+req.KeyWord+"%")
	}
	//日期
	if !util.IsEmpty(req.DateBegin) && !util.IsEmpty(req.DateEnd) {
		dbs = dbs.Where("a.publish_date BETWEEN ? AND ?", req.DateBegin, req.DateEnd)
	}
	//相关评级
	if !util.IsEmpty(req.Level) {
		if req.Level == "上调评级" {
			dbs = dbs.Where("a.level_calc = ?", "up")
		} else if req.Level == "下调评级" {
			dbs = dbs.Where("a.level_calc = ?", "down")
		} else if req.Level == "首次推荐" {
			dbs = dbs.Where("a.level_change = ?", "首次")
		} else {
			dbs = dbs.Where("a.level = ?", req.Level)
		}
	}
	//类型
	if !util.IsEmpty(req.Type) {
		if req.Type == "公司研报" {
			dbs = dbs.Where("a.type = ? OR a.type = ?", "个股", "重点券商")
		} else {
			dbs = dbs.Where("a.type = ?", req.Type)
		}
	}
	//研报内容,全文索引检索,单个关键词
	if !util.IsEmpty(req.Content) {
		highLight.ContentHighLight = req.Content
		fullTextSearchStr := fmt.Sprintf("MATCH(%s) AGAINST ('%s' IN BOOLEAN MODE)", "content", req.Content)
		dbs = dbs.Where(fullTextSearchStr)
	}

	dbs = dbs.Where("a.deleted_at is null").Where("b.deleted_at is null")
	return dbs, nil
}
func EditResearchReportRecommend(req research_report_service.EditResearchReportRecommendRequest) (success bool, err error) {
	var u_r_r UResearchReport
	err = models.DB().Table("u_research_report").Select("recommend").Where("id = ?", req.Id).Update(map[string]interface{}{
		"recommend": req.Recommend,
	}).Error

	models.DB().Table("u_research_report").Select("recommend_user_id").Where("id = ?", req.Id).Find(&u_r_r)
	u_r_r.RecommendUserId = u_r_r.RecommendUserId + strconv.Itoa(req.UserId) + ","
	if err != nil {
		logging.Error("models\\research_report.go EditResearchReportRecommend() err:", err.Error())
		return success, err
	}
	err = models.DB().Table("u_research_report").Select("recommend_user_id").Where("id = ?", req.Id).Update(map[string]interface{}{
		"recommend_user_id": u_r_r.RecommendUserId,
	}).Error
	if err != nil {
		logging.Error("models\\research_report.go EditResearchReportRecommend() err:", err.Error())
		return success, err
	}
	success = true
	return success, nil
}

// 获取当前研报 股票+机构的
func getExpectedNetProfit(ret util.GetResearchReportMysql) string {
	// 获取逻辑
	// 1. 当前记录是否有预测净利润
	if *ret.ExpectedNetProfit == "" {
		return ""
	}
	// 3. 获取当前记录的最近一条
	type Report struct {
		ID                int             `json:"id" gorm:"column:report_id"`
		ExpectedNetProfit decimal.Decimal `json:"expected_net_profit" gorm:"column:expected_net_profit"`
		ExpectedYear      int             `json:"expected_year" gorm:"column:expected_year"`
	}
	start, _ := time.ParseInLocation(util.YMDHMS, "2021-02-04 00:00:00", time.Local)
	// 获取当前记录的期望利润
	reportData := []Report{}
	err := models.DB().Table("u_research_report_data").
		Where("u_research_report_data.created_at > ? ", start).Where("report_id = ? ", ret.Id).Find(&reportData).Error
	if err != nil {
		return ""
	}
	reportList := []Report{}
	err = models.DB().Table("u_research_report_data").
		Joins("left join u_research_report on u_research_report_data.report_id=u_research_report.id ").
		Where("u_research_report.spider_time < ? ", ret.SpiderTime).
		Where("u_research_report.code = ? ", ret.Code).
		Where("u_research_report.organization = ? ", ret.Organization).
		Where("u_research_report_data.expected_net_profit is not null ").
		Where("u_research_report_data.report_id != ? ", ret.Id).
		Where("u_research_report.parent != 1 ").
		Order("u_research_report_data.report_id desc").Limit(3).Find(&reportList).Error
	if len(reportList) < 1 {
		return ""
	}
	fmt.Println(reportList)
	// 判断3条数据是否都是一条研报数据的
	lastReport := make(map[int]Report)
	preID := 0
	for _, value := range reportList {
		if preID < 1 {
			preID = value.ID
			lastReport[value.ExpectedYear] = value
		}
		if value.ID != preID {
			continue
		} else {
			lastReport[value.ExpectedYear] = value
		}
	}
	fmt.Println(lastReport)
	retStr := ""
	nowYear := time.Now().Year()
	rate := decimal.Decimal{}
	newReport := make(map[int]Report)
	for _, value := range reportData {
		newReport[value.ExpectedYear] = value
	}
	if !util.IsEmpty(newReport[nowYear]) && !util.IsEmpty(lastReport[nowYear]) && !lastReport[nowYear].ExpectedNetProfit.IsZero() {
		rate = newReport[nowYear].ExpectedNetProfit.Sub(lastReport[nowYear].ExpectedNetProfit).Div(lastReport[nowYear].ExpectedNetProfit.Abs())
		if rate.Sub(decimal.NewFromFloat(0.10)).Sign() > 0 {
			return "up"
		}
		if rate.Sub(decimal.NewFromFloat(-0.10)).Sign() < 0 {
			return "down"
		}
	}
	if !util.IsEmpty(newReport[nowYear+1]) && !util.IsEmpty(lastReport[nowYear+1]) && !lastReport[nowYear+1].ExpectedNetProfit.IsZero() {
		nowYear = nowYear + 1
		rate = newReport[nowYear].ExpectedNetProfit.Sub(lastReport[nowYear].ExpectedNetProfit).Div(lastReport[nowYear].ExpectedNetProfit.Abs())
		if rate.Sub(decimal.NewFromFloat(0.10)).Sign() > 0 {
			return "up"
		}
		if rate.Sub(decimal.NewFromFloat(-0.10)).Sign() < 0 {
			return "down"
		}
	}
	if !util.IsEmpty(newReport[nowYear+2]) && !util.IsEmpty(newReport[nowYear+2]) && !lastReport[nowYear+2].ExpectedNetProfit.IsZero() {
		nowYear = nowYear + 2
		rate = newReport[nowYear].ExpectedNetProfit.Sub(lastReport[nowYear].ExpectedNetProfit).Div(lastReport[nowYear].ExpectedNetProfit.Abs())
		if rate.Sub(decimal.NewFromFloat(0.10)).Sign() > 0 {
			return "up"
		}
		if rate.Sub(decimal.NewFromFloat(-0.10)).Sign() < 0 {
			return "down"
		}
	}
	if !util.IsEmpty(newReport[nowYear-1]) && !util.IsEmpty(newReport[nowYear-1]) && !lastReport[nowYear].ExpectedNetProfit.IsZero() {
		nowYear = nowYear - 1
		rate = newReport[nowYear].ExpectedNetProfit.Sub(lastReport[nowYear].ExpectedNetProfit).Div(lastReport[nowYear].ExpectedNetProfit.Abs())
		if rate.Sub(decimal.NewFromFloat(0.10)).Sign() > 0 {
			return "up"
		}
		if rate.Sub(decimal.NewFromFloat(-0.10)).Sign() < 0 {
			return "down"
		}
	}
	return retStr
}
