package template

import (
	"datacenter/models"
	"github.com/jinzhu/gorm"
	"sort"
	"strings"
	"time"
)

type CalendarResponse struct {
	ID          int       `json:"id" gorm:"column:id"`
	CreatorName string    `json:"creator_name" gorm:"column:creator_name"` // 发布者名称
	CodeName    []Stock   `json:"code_name"`                               // 股票代码名称
	DeletedAt   time.Time `json:"-" gorm:"column:deleted_at"`              // 删除时间
	CalendarAdd
}
type CalendarAdd struct {
	Type        int    `json:"type" gorm:"column:type"`                 // 类型 1 为事件日历 2 为股票日历
	Star        int    `json:"star" gorm:"column:star"`                 // 星级
	IsDisplay   int    `json:"is_display" gorm:"column:is_display"`     // 模糊时间勾选
	Creator     int    `json:"creator" gorm:"column:creator"`           // 发布者
	Link        string `json:"link" gorm:"column:link"`                 // 链接
	Name        string `json:"name" gorm:"column:name"`                 // 概念名称
	Code        string `json:"code" gorm:"column:code"`                 // 股票代码
	Content     string `json:"content" gorm:"column:content"`           // 内容
	Detail      string `json:"detail" gorm:"column:detail"`             // 详情
	EventDate   string `json:"event_date" gorm:"column:event_date"`     // 事件日期
	PublishDate string `json:"publish_date" gorm:"column:publish_date"` // 发布时间
	Created     string `json:"created" gorm:"column:created_at"`        // 创建时间
	IsAddEvent  int    `json:"is_add_event" gorm:"column:is_add_event"` //是否添加到事件库
}
type CalendarMonthList struct {
	ID          int       `json:"id" gorm:"column:id"`
	Type        int       `json:"type" gorm:"column:type"`               // 类型 1 为事件日历 2 为股票日历
	IsDisplay   int       `json:"is_display" gorm:"column:is_display"`   // 模糊时间勾选
	Star        int       `json:"star" gorm:"column:star"`               // 星级
	Link        string    `json:"link" gorm:"column:link"`               // 链接
	Name        string    `json:"name" gorm:"column:name"`               // 概念名称
	Code        string    `json:"-" gorm:"column:code"`                  // 股票代码
	CodeName    []Stock   `json:"code_name" gorm:"column:code_name"`     // 股票代码名称
	Content     string    `json:"content" gorm:"column:content"`         // 内容
	Detail      string    `json:"detail" gorm:"column:detail"`           // 详情
	EventDate   time.Time `json:"-" gorm:"column:event_date"`            // 事件日期
	PublishDate time.Time `json:"event_date" gorm:"column:publish_date"` // 发布时间
}
type Stock struct {
	CodeName string `json:"name" gorm:"column:name"` // 股票代码名称
	ID       string `json:"id" gorm:"column:id"`     // 股票ID
}

const ORDERBYCREATEDDESC = 1 //创建时间降序
const ORDERBYCREATED = -1    // 创建时间升序

func (CalendarResponse) TableName() string {
	return "p_calendar"
}

func GetCalendarsLists(pageNum int, pageSize int, calendarType int, keyword string, sort int) ([]*CalendarResponse, error) {
	var calendars []*CalendarResponse
	var stock []Stock
	dbs := models.DB().Table("p_calendar").
		Select("p_calendar.*,b_user.realname as creator_name").
		Joins("LEFT JOIN b_user on p_calendar.creator = b_user.id ").
		Where("p_calendar.deleted_at is null ")
	if calendarType > 0 {
		dbs = dbs.Where("p_calendar.type = ?", calendarType)
	}
	if keyword != "" {
		dbs = dbs.Where("content like ? ", "%"+keyword+"%")
	}
	switch sort {
	case ORDERBYCREATEDDESC:
		dbs = dbs.Order("created_at desc")
	case ORDERBYCREATED:
		dbs = dbs.Order("created_at")
	default:
		dbs = dbs.Order("publish_date desc")

	}

	err := dbs.Offset((pageNum - 1) * pageSize).Limit(pageSize).Find(&calendars).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	for index, value := range calendars {
		calendars[index].EventDate = value.PublishDate
		if value.Code != "" {
			err := models.DB().Table("b_stock").Select("name,id").Where("id in (?)", strings.Split(value.Code, ",")).Find(&stock).Error
			if err != nil {
				continue
			}
			calendars[index].CodeName = stock
		}
	}
	return calendars, nil
}

func GetCalendarsTotal(calendarType int, keyword string) (int, error) {
	var count int
	dbs := models.DB().Model(&CalendarResponse{}).Where("p_calendar.deleted_at is null ")
	if calendarType > 0 {
		dbs = dbs.Where("p_calendar.type = ?", calendarType)
	}
	if keyword != "" {
		dbs = dbs.Where("content like ? ", "%"+keyword+"%")
	}
	err := dbs.Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}

func AddCalendar(calendar CalendarAdd) error {
	if err := models.DB().Table("p_calendar").Create(&calendar).Error; err != nil {
		return err
	}

	return nil
}

func EditCalendar(id int, data interface{}) error {
	if err := models.DB().Model(&CalendarResponse{}).Where("id = ?", id).Updates(data).Error; err != nil {
		return err
	}

	return nil
}

func DeleteCalendar(id int) error {
	if err := models.DB().Where("id = ?", id).Delete(CalendarResponse{}).Error; err != nil {
		return err
	}

	return nil
}

func GetCalendarsMonthLists(data interface{}, start, end string) ([]*CalendarMonthList, error) {
	var calendars []*CalendarMonthList
	var stock []Stock
	dbs := models.DB().Table("p_calendar").
		Select("p_calendar.*,b_stock.name as stock_name ").
		Joins("LEFT JOIN b_stock on b_stock.id = p_calendar.code").
		Where("p_calendar.deleted_at is null")
	if start != "" {
		dbs = dbs.Where("p_calendar.publish_date >= ? ", start)
	}
	if end != "" {
		dbs = dbs.Where("p_calendar.publish_date < ? ", end)
	}
	err := dbs.Where(data).Order("p_calendar.event_date asc").Find(&calendars).Error
	if err != nil {
		return nil, err
	}
	for index, value := range calendars {
		calendars[index].EventDate = value.PublishDate
		if value.Code != "" {
			err := models.DB().Table("b_stock").Select("name,id").Where("id in (?)", strings.Split(value.Code, ",")).Find(&stock).Error
			if err != nil {
				continue
			}
			calendars[index].CodeName = stock
		}
	}
	sort.Slice(calendars, func(i, j int) bool {
		return calendars[i].EventDate.Sub(calendars[j].EventDate) < 0
	})
	return calendars, nil
}

