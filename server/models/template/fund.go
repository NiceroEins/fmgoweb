package template

import (
    "datacenter/models"
    "datacenter/pkg/logging"
    "datacenter/pkg/util"
    "github.com/jinzhu/gorm"
    "time"
)

type Fund struct {
    ID                   int        `json:"id" gorm:"column:id" `                                // 主键ID
    Status               int        `json:"status" gorm:"column:status" `                        // 分红状态
    Name                 string     `json:"name" gorm:"column:name"`                             // 名称
    Value                string     `json:"value" gorm:"column:value"`                           // 累计净值
    Created              string     `json:"created" gorm:"column:created"`                       // 创建时间
    UnitValue            string     `json:"unit_value" gorm:"column:unit_value"`                 // 单位净值
    LastUpdateTime       string     `json:"last_update_time" gorm:"column:last_update_time"`     // 最后更新时间
    Deleted              int        `json:"-" gorm:"column:deleted"`
}

func(Fund) TableName() string {
    return "p_fund"
}

type FundNames struct {
    ID                   int        `json:"id" gorm:"column:id" `                                // 主键ID
    Name                 string     `json:"name" gorm:"column:name"`                             // 名称
}

func GetFunds() ([]Fund, error) {
    var (
        funds []Fund
        err  error
    )
    err = models.DB().Where("deleted = 0").Order("created desc").Find(&funds).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return nil, err
    }
    for index,fund := range funds {
        fundTick,tickErr := GetLastRecordByFundId(fund.ID)
        if tickErr != nil{
            logging.Info("template.GetLastRecordByFundId() ",tickErr)
            continue
        }
        if fundTick.ID  > 0 {
            funds[index].Value = fundTick.Value.String()
            funds[index].UnitValue = fundTick.UnitValue.String()
            funds[index].LastUpdateTime = fundTick.ValueDate.Format(util.YMDHMS)
        }else{
            funds[index].UnitValue = ""
            funds[index].Value = ""
            funds[index].LastUpdateTime = ""
        }
    }
    return funds, nil
}

func ExistFundByName(name string) (bool, error) {
    var fund Fund
    err := models.DB().Select("id").Where("deleted = 0").Where("name = ?", name).First(&fund).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return false, err
    }

    if fund.ID > 0 {
        return true, nil
    }

    return false, nil
}

func AddFundName(name string,created string) error {
    fund := Fund{
        Name:      name,
        Created:   created,
        Value:     "0.00",
        UnitValue: "0.00",
        LastUpdateTime: time.Now().Format(util.YMDHMS),
    }
    if err := models.DB().Create(&fund).Error; err != nil {
        return err
    }

    return nil
}

func GetFundsByName(pageNum int, pageSize int,name string) ([]Fund, error) {
    var (
        funds []Fund
        err  error
    )

    if pageSize > 0 && pageNum > 0 {
        err = models.DB().Where("name = ? ",name).Where("deleted = 0").Find(&funds).Order("id desc").Offset(pageNum).Limit(pageSize).Error
    } else {
        err = models.DB().Find(&funds).Error
    }

    if err != nil && err != gorm.ErrRecordNotFound {
        return nil, err
    }

    return funds, nil
}

func EditFund(index interface{}, data interface{}) error {
    if err := models.DB().Model(&Fund{}).Where(index).Updates(data).Error; err != nil {
        return err
    }
    return nil
}

func ExistFundByID(data interface{}) (bool, error) {
    var fund Fund
    err := models.DB().Select("id").Where("deleted = 0").Where(data).First(&fund).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return false, err
    }

    if fund.ID > 0 {
        return true, nil
    }

    return false, nil
}

func DeleteFund(maps interface{}) error {
    if err := models.DB().Where(maps).Delete(&Fund{}).Error; err != nil {
        return err
    }
    return nil
}

func GetNames() (funds []FundNames,err error) {
    ret := []FundNames{}
    err = models.DB().Table("p_fund").Where("deleted = 0").Select("distinct name,id").Find(&ret).Error

    if err != nil && err != gorm.ErrRecordNotFound {
        logging.Error("template.GetNames() err:", err.Error())
        return nil,err
    }
    return ret,nil
}

func DeleteFundAll(fundID int) error {
    tx := models.DB().Begin()
    err := tx.Table("p_fund").Where("id = ? ",fundID).Update(map[string]interface{}{"deleted":1}).Error
    if err != nil {
        logging.Info("delete fund main err",err)
        tx.Rollback()
        return err
    }
    err = tx.Table("p_customer_fund").Where("fund_id = ? ",fundID).Update(map[string]interface{}{"deleted":1}).Error
    if err != nil {
        logging.Info("delete fund_customer err",err)
        tx.Rollback()
        return err
    }
    err = tx.Table("p_notice").Where("fund_id = ? ",fundID).Update(map[string]interface{}{"deleted":1}).Error
    if err != nil {
        logging.Info("delete fund_notice err",err)
        tx.Rollback()
        return err
    }
    err = tx.Table("p_fund_bonus").Where("fund_id = ? ",fundID).Delete(&FundBonus{}).Error
    if err != nil {
        logging.Info("delete fund_bonus err",err)
        tx.Rollback()
        return err
    }
    err = tx.Table("p_fund_tick").Where("fund_id = ? ",fundID).Update(map[string]interface{}{"deleted":1}).Error
    if err != nil {
        logging.Info("delete fund_bonus err",err)
        tx.Rollback()
        return err
    }
    tx.Commit()
    return nil
}