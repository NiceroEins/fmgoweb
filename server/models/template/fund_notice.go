package template

import (
    "datacenter/models"
    "github.com/jinzhu/gorm"
    "time"
)

type FundNoticeResponse struct {
    FundNotice
    FundName             string     `json:"fund_name" gorm:"column:fund_name"`               // 基金名称
}

type FundNotice struct {
    ID                   int             `json:"id" gorm:"column:id" `                            // 主键ID
    FundID               int             `json:"fund_id" gorm:"column:fund_id"`                   // 基金id
    Title                string          `json:"title" gorm:"column:title" `                      // 标题
    Content              string          `json:"content" gorm:"column:content"`                   // 内容
    Author               string          `json:"author" gorm:"column:author"`                     // 作者
    PublishDate          time.Time       `json:"publish_date" gorm:"column:publish_date"`         // 展示时间
    Created              time.Time       `json:"created" gorm:"column:created"`                   // 创建时间
    Deleted              int        `json:"-" gorm:"column:deleted"`
}

type CustomerFundNotice struct {
    ID                   int             `json:"id" gorm:"column:id" `                            // 主键ID
    Title                string          `json:"title" gorm:"column:title" `                      // 标题
    Content              string          `json:"content" gorm:"column:content"`                   // 内容
    PublishDate          time.Time       `json:"publish_date" gorm:"column:publish_date"`         // 展示时间
}

type FundNoticeContent struct {
    ID                   int             `json:"id" gorm:"column:id" `                            // 主键ID
    Content              string          `json:"content" gorm:"column:content"`                   // 内容
}

func(FundNotice) TableName() string {
    return "p_notice"
}

func GetFundNotice() ([]FundNoticeResponse, error) {
    var (
        fundNotices []FundNoticeResponse
        err  error
    )
    err = models.DB().Select("p_notice.*,p_fund.name as fund_name,p_customer.realname").
        Joins("LEFT JOIN p_customer ON p_customer.id = p_notice.author").
        Joins("LEFT JOIN p_fund ON p_fund.id = p_notice.fund_id").
        Where("p_fund.deleted = 0").
        Where("p_notice.deleted = 0").
        Order("p_notice.publish_date desc").Find(&fundNotices).Error

    if err != nil && err != gorm.ErrRecordNotFound {
        return nil, err
    }

    return fundNotices, nil
}

func AddFundNotice(notice FundNotice) error {
    if err := models.DB().Table("p_notice").Create(&notice).Error; err != nil {
        return err
    }

    return nil
}

func EditFundNotice(id int, data interface{}) error {
    if err := models.DB().Model(&FundNotice{}).Where("id = ? ",id).Updates(data).Error; err != nil {
        return err
    }
    return nil
}

func ExistFundNotice(data interface{}) (bool, error) {
    var notice FundNotice
    err := models.DB().Select("id").Where("p_notice.deleted = 0").Where(data).First(&notice).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        return false, err
    }

    if notice.ID > 0 {
        return true, nil
    }

    return false, nil
}

func DeleteFundNotice(id int) error {
    if err := models.DB().Where("id = ? ",id).Delete(&FundNotice{}).Error; err != nil {
        return err
    }
    return nil
}


func GetNotice(data map[string]interface{}) ([]CustomerFundNotice, error) {
    var (
        fundNotices []CustomerFundNotice
        err  error
    )
    dbs := models.DB().Table("p_notice").Select("p_notice.*").Where("p_notice.deleted = 0").Where(data)
    err = dbs.Order("publish_date desc").Find(&fundNotices).Error

    if err != nil && err != gorm.ErrRecordNotFound {
        return nil, err
    }

    return fundNotices, nil
}

func GetNoticeContent(data map[string]interface{}) (FundNoticeContent, error) {
    var notice FundNoticeContent
    err := models.DB().Table("p_notice").Select("content,id").Where("p_notice.deleted = 0").Where(data).Order("id desc").First(&notice).Error
    if err != nil{
        return notice, err
    }
    return notice, nil
}
