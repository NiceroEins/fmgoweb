package replay

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type ReplayDTO struct {
	models.Simple
	Code      string     `json:"code"`
	Name      string     `json:"name"`
	Reason    string     `json:"reason"`
	Highlight string     `json:"highlight"`
	Type      string     `json:"type"`
	Adder     int        `json:"adder"`
	Datetime  *time.Time `json:"datetime"`
}

func (dto ReplayDTO) TableName() string {
	return "u_replay"
}

type ReplayVO struct {
	Id        int    `json:"id"`
	Code      string `json:"code"`
	Name      string `json:"name"`
	Reason    string `json:"reason"`
	Highlight string `json:"highlight"`
	Type      string `json:"type"`
	//Adder     int    `json:"adder"`
	Datetime  string `json:"datetime"`
	CreatedAt string `json:"created_at"`
}

type ReplayEO struct {
	Id        int    `json:"id" form:"id" binding:"gt=0"`
	Reason    string `json:"reason" form:"reason"`
	Highlight string `json:"highlight" form:"highlight"`
	Type      string `json:"type" form:"type"`
	Adder     int    `json:"-"`
}

type ReplayIO struct {
	Code      string `json:"code" form:"code" binding:"required"`
	Reason    string `json:"reason" form:"reason"`
	Highlight string `json:"highlight" form:"highlight"`
	Type      string `json:"type" form:"type" binding:"required"`
	Adder     int    `json:"adder"`
	Datetime  string `json:"datetime" form:"datetime" binding:"required"`
}

type ReplayQO struct {
	Code      string `json:"code" form:"code"`
	Reason    string `json:"reason" form:"reason"`
	Highlight string `json:"highlight" form:"highlight"`
	Type      string `json:"type" form:"type"`
	Adder     int    `json:"adder" form:"adder"`
	Datetime  string `json:"datetime" form:"datetime" binding:"required"`
}

func (dto ReplayDTO) DTO2VO() crud_service.VO {
	return ReplayVO{
		Id:        dto.Simple.ID,
		Code:      dto.Code,
		Name:      dto.Name,
		Reason:    dto.Reason,
		Highlight: dto.Highlight,
		Type:      dto.Type,
		//Adder:     dto.Adder,
		Datetime:  util.TimePtr2String(dto.Datetime, util.YMD),
		CreatedAt: util.TimePtr2String(&dto.Simple.CreatedAt, util.YMDHMS),
	}
}

/*func (io ReplayIO) IO2DTO() ReplayDTO {
	return ReplayDTO{
		Code:      io.Code,
		Reason:    io.Reason,
		Highlight: io.Highlight,
		Type:      io.Type,
		Adder:     io.Adder,
	}

}*/

func (eo ReplayEO) EO2DTO() ReplayDTO {
	return ReplayDTO{
		Simple:    models.Simple{ID: eo.Id},
		Reason:    eo.Reason,
		Highlight: eo.Highlight,
		Type:      eo.Type,
	}
}

type TotoalReplayRaw struct {
	ReplayDTO
	Price       string              `json:"price"`
	HitTimes    int                 `json:"hit_times"`
	LimitUpTime *time.Time          `json:"limit_up_time"`
	Turnover    decimal.NullDecimal `json:"turnover"`
	FAMC        string              `json:"famc"`
	Rate        string              `json:"rate"`
}

type TotoalReplayRawVO struct {
	ReplayVO
	Price       string `json:"price"`
	HitTimes    int    `json:"hit_times"`
	LimitUpTime string `json:"limit_up_time"`
	Turnover    string `json:"turnover"`
	FAMC        string `json:"famc"`
	Rate        string `json:"rate"`
}

type TotalReplayDTO struct {
	Replays []TotoalReplayRaw `json:"replays" gorm:"FOREIGNKEY:Type;ASSOCIATION_FOREIGNKEY:Type"`
	Type    string            `json:"type"`
	Count   int               `json:"count"`
}

type TotalReplayVO struct {
	Replays []TotoalReplayRawVO `json:"replays"`
	Type    string              `json:"type"`
	Count   int                 `json:"count"`
}

func (dto TotoalReplayRaw) DTO2VO() TotoalReplayRawVO {
	dto.Turnover.Decimal = dto.Turnover.Decimal.DivRound(decimal.NewFromInt32(100000), 2)
	return TotoalReplayRawVO{
		ReplayVO:    dto.ReplayDTO.DTO2VO().(ReplayVO),
		Price:       dto.Price,
		HitTimes:    dto.HitTimes,
		LimitUpTime: util.TimePtr2String(dto.LimitUpTime, util.HMS),
		Turnover:    util.NullDecimal2String(dto.Turnover),
		FAMC:        dto.FAMC,
		Rate:        dto.Rate,
	}
}

func (dto TotalReplayDTO) DTO2VO() crud_service.VO {
	vo := TotalReplayVO{
		Type:  dto.Type,
		Count: dto.Count,
	}
	for i, _ := range dto.Replays {
		vo.Replays = append(vo.Replays, dto.Replays[i].DTO2VO())
	}
	return vo
}

type ReplayStatisticsDTO struct {
	models.Simple
	IncTotal    int        `json:"inc_total"`
	Datetime    *time.Time `json:"datetime"`
	DecTotal    int        `json:"dec_total"`
	ShBlockNum  int        `json:"sh_block_num"`
	SzBlockNum  int        `json:"sz_block_num"`
	NewBlockNum int        `json:"new_block_num"`
	CyBlockNum  int        `json:"cy_block_num"`
}

type ReplayStatisticsVO struct {
	Id          int    `json:"id"`
	IncTotal    int    `json:"inc_total"`
	Datetime    string `json:"datetime"`
	DecTotal    int    `json:"dec_total"`
	ShBlockNum  int    `json:"sh_block_num"`
	SzBlockNum  int    `json:"sz_block_num"`
	NewBlockNum int    `json:"new_block_num"`
	CyBlockNum  int    `json:"cy_block_num"`
}

func (dto ReplayStatisticsDTO) DTO2VO() crud_service.VO {
	vo := ReplayStatisticsVO{
		Id:          dto.ID,
		IncTotal:    dto.IncTotal,
		DecTotal:    dto.DecTotal,
		ShBlockNum:  dto.ShBlockNum,
		SzBlockNum:  dto.SzBlockNum,
		NewBlockNum: dto.NewBlockNum,
		CyBlockNum:  dto.CyBlockNum,
		Datetime:    util.TimePtr2String(dto.Datetime, util.YMD),
	}
	return vo
}

func GetRedisValue(key string, field string) string {
	value, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString(key, field)
	if err != nil {
		logging.Error("redis get "+field+" value err:", err.Error())
		return ""
	}
	return value
}

func GetTotalReplayList(qo ReplayQO) ([]crud_service.DTO, int, error) {
	dtos := make([]TotalReplayDTO, 0)
	res := make([]crud_service.DTO, 0)
	dbs := models.DB().Table("u_replay").Select("type,COUNT(1) count").Preload("Replays", func(db *gorm.DB) *gorm.DB {
		return models.DB().Select("b_stock.name,u_replay.*,p_stock_tick.pct_chg rate,p_stock_tick.close price,p_stock_tick.amount turnover,u_replay_blocks.hit_times,u_replay_blocks.limit_up_time").
			Joins("LEFT JOIN b_stock ON b_stock.id=u_replay.code").
			Joins("LEFT JOIN p_stock_tick ON u_replay.code=p_stock_tick.stock_code AND u_replay.datetime=p_stock_tick.trade_date").
			Joins("LEFT JOIN u_replay_blocks ON u_replay_blocks.code=u_replay.code AND u_replay_blocks.datetime=u_replay.datetime").
			Where("u_replay.datetime=?", qo.Datetime)
	}).Where("datetime=? AND deleted_at IS NULL", qo.Datetime).Group("type")
	err := dbs.Find(&dtos).Error
	if err != nil {
		return nil, 0, err
	}
	var wg sync.WaitGroup
	c := make(chan crud_service.DTO)
	wg.Add(len(dtos))
	for _, d := range dtos {
		go func(dto TotalReplayDTO) {
			defer wg.Done()
			var price decimal.NullDecimal
			var ltg decimal.NullDecimal
			for i, _ := range dto.Replays {
				err := price.Scan(GetRedisValue("stock:tick", dto.Replays[i].Code))
				if err != nil {
					price.Valid = false
				}
				err = ltg.Scan(GetRedisValue("stock:ltg", dto.Replays[i].Code))
				if err != nil {
					ltg.Valid = false
				}

				dto.Replays[i].FAMC = util.NullDecimal2String(decimal.NullDecimal{price.Decimal.Mul(ltg.Decimal), price.Valid && ltg.Valid})
			}
			sort.Slice(dto.Replays, func(i, j int) bool {
				if dto.Replays[i].HitTimes > dto.Replays[j].HitTimes {
					return true
				} else {
					if dto.Replays[i].LimitUpTime != nil && dto.Replays[j].LimitUpTime != nil && dto.Replays[i].LimitUpTime.Before(*dto.Replays[j].LimitUpTime) {
						return true
					} else {
						return false
					}
				}
			})
			c <- dto
		}(d)
	}

	go func() {
		wg.Wait()
		close(c)
	}()
	for v := range c {
		res = append(res, v)
	}
	return res, 0, nil
}

func GetMyReplayList(qo ReplayQO) ([]crud_service.DTO, int, error) {
	dtos := make([]ReplayDTO, 0)
	res := make([]crud_service.DTO, 0)
	dbs := models.DB().Table("u_replay").Select("b_stock.name,u_replay.*").Joins("LEFT JOIN b_stock ON b_stock.id=u_replay.code").Where("datetime=?", qo.Datetime)
	if qo.Adder > 0 {
		dbs = dbs.Where("adder = ?", qo.Adder)
	}
	if qo.Code != "" {
		dbs = dbs.Where("code like ? OR name like ?", "%"+qo.Code+"%", "%"+qo.Code+"%")
	}
	if qo.Reason != "" {
		dbs = dbs.Where("reason like ?", "%"+qo.Reason+"%")
	}
	if qo.Type != "" {
		dbs = dbs.Where("type like ?", "%"+qo.Type+"%")
	}
	err := dbs.Find(&dtos).Error
	if err != nil {
		return nil, 0, err
	}
	for _, v := range dtos {
		res = append(res, v)
	}
	return res, 0, nil
}

func CreateMyReplay(io ReplayIO) error {
	err := models.DB().Table("u_replay").Create(&io).Error
	return err

}

func DelMyReplay(id int) error {
	err := models.DB().Exec("DELETE FROM u_replay WHERE id=?", id).Error
	return err
}

func EditMyReplay(eo ReplayEO) error {
	err := models.DB().Table("u_replay").Where("adder=? AND id=?", eo.Adder, eo.Id).Updates(map[string]interface{}{
		"reason":    eo.Reason,
		"type":      eo.Type,
		"highlight": eo.Highlight,
	}).Error
	return err
}

func ImportReplay(rows [][]string, uid, datetime string) []string {
	codeN := 0
	nameN := 1
	typeN := 4
	var failCodes []string
	var codeList []struct {
		Code string `json:"code"`
		Name string `json:"name"`
	}
	var codeListMap = make(map[string]string, 0)
	var codeNameMap = make(map[string]string, 0)
	err := models.DB().Table("b_stock").Select("DISTINCT id as code,name").Find(&codeList).Error
	if err != nil {
		logging.Error("ImportReplay 获取股票代码列表失败")
	}
	if codeList != nil {
		for _, v := range codeList {
			codeListMap[v.Code] = v.Name
			codeNameMap[v.Name] = v.Code
		}
	}
	for _, row := range rows[1:] {
		if row[typeN] == "" {
			failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+row[typeN]+" "+datetime+" 分类为空")
			continue
		}
		code := strings.Trim(strings.Split(row[codeN], ".")[0], " ")
		//row[0], row[1] = row[1], code
		if len(codeListMap) > 0 {
			if code != "" {
				if n, ok := codeListMap[code]; ok {
					if row[nameN] != "" {
						if strings.Trim(row[nameN], " ") != n {
							failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+row[typeN]+" "+datetime+" 证券代码与股票名不对映")
							continue
						}
					}
				} else {
					failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+row[typeN]+" "+datetime+" 错误证券代码")
					continue
				}
			} else {
				if row[nameN] != "" {
					if c, ok := codeNameMap[row[nameN]]; !ok {
						failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+row[typeN]+" "+datetime+" 股票名错误")
						continue
					} else {
						code = c
					}
				} else {
					failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+row[typeN]+" "+datetime+" 股票和代码缺失")
					continue
				}
			}
		}
		sql := "INSERT INTO  u_replay (code,reason,highlight,type,adder,datetime)VALUES('"
		sql = sql + code + "','" + strings.Join(row[2:], "','") + "','" + uid + "','" + datetime + "')"
		err := models.DB().Exec(sql).Error
		if err != nil {
			failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+row[typeN]+" "+datetime+" 重复")
		}
	}
	return failCodes
}

func GetReplayStatistics(qo ReplayQO) (crud_service.DTO, error) {
	dtos := ReplayStatisticsDTO{}
	dbs := models.DB().Table("u_replay_statistics").Where("datetime=?", qo.Datetime)
	err := dbs.First(&dtos).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return dtos, err
	}
	return dtos, nil
}

type BlockCountDTO struct {
	BlockCount int             `json:"block_count"`
	StockCode  string          `json:"stock_code"`
	LastDate   *time.Time      `json:"last_date"`
	Amount     decimal.Decimal `json:"amount"`
}

func CronReplayStatistics() {
	var (
		szcount  = 0
		shcount  = 0
		cycount  = 0
		newcout  = 0
		sql      = "INSERT INTO u_replay_blocks (code,limit_up_time,datetime,hit_times) VALUES"
		newCodes []struct {
			Id string `json:"id"`
		}
		newCodesMap    = make(map[string]struct{}, 0)
		now            = time.Now()
		blockCountDTOS = make([]BlockCountDTO, 0)
		blockCountMap  = make(map[string]BlockCountDTO, 0)
		limitUpTime    string
		blockSql       = "SELECT U.*,AM.amount FROM (SELECT COUNT(T.amount>0 OR NULL) block_count,S.stock_code,b_stock.`name`,MAX(T.trade_date) last_date FROM (SELECT stock_code,DATE_SUB(MIN(trade_date),INTERVAL 1 DAY) maxtradedate FROM p_stock_tick  GROUP BY stock_code HAVING maxtradedate<=? AND COUNT(1)=COUNT(is_block=1 OR NULL) UNION SELECT stock_code,MAX(trade_date) maxtradedate FROM p_stock_tick WHERE is_block=0 GROUP BY stock_code HAVING maxtradedate<?) S  LEFT JOIN b_stock ON b_stock.id=S.stock_code LEFT JOIN p_stock_tick T ON T.stock_code=S.stock_code AND T.trade_date>S.maxtradedate  AND b_stock.`name` NOT LIKE '%ST%' GROUP BY T.stock_code HAVING block_count>0) U LEFT JOIN p_stock_tick AM ON AM.stock_code=U.stock_code WHERE AM.trade_date=?"
	)
	inc, err := gredis.Clone(setting.RedisSetting.StockDB).HGetAllStringMap("stock:increase")
	if err != nil {
		logging.Error("CronReplayStatistics get inc redis:" + err.Error())
	}
	dec, err := gredis.Clone(setting.RedisSetting.StockDB).HGetAllStringMap("stock:decrease")
	if err != nil {
		logging.Error("CronReplayStatistics get dec redis:" + err.Error())
	}
	block, err := gredis.Clone(setting.RedisSetting.StockDB).HGetAllStringMap("stock:block")
	err = models.DB().Table("b_stock").Select("id").Where("start_date>=?", now.AddDate(0, -1, 0).Format(util.YMD)).Find(&newCodes).Error
	if err != nil {
		logging.Error("CronReplayStatistics get b_stock newCodes:" + err.Error())
	}
	for _, v := range newCodes {
		newCodesMap[v.Id] = struct{}{}
	}
	err = models.DB().Raw(blockSql, now.Format(util.YMD), now.Format(util.YMD), now.Format(util.YMD)).Find(&blockCountDTOS).Error
	if err != nil {
		logging.Error("CronReplayStatistics get b_stock newCodes:" + err.Error())
	}
	for _, v := range blockCountDTOS {
		blockCountMap[v.StockCode] = v
	}
	for i, v := range blockCountMap {
		//今日有开盘才计数
		if v.LastDate.After(now.AddDate(0, 0, -1)) && v.Amount.Cmp(decimal.Zero) > 0 {
			if strings.HasPrefix(i, "0") || strings.HasPrefix(i, "3") {
				szcount++
			}
			if strings.HasPrefix(i, "6") {
				shcount++
			}
			if strings.HasPrefix(i, "300") {
				cycount++
			}
			//t, err := time.ParseInLocation("2006-01-02 15:04:05.000", v, time.Local)
			/*if err != nil {
				fmt.Println(err.Error())
			}*/
			if _, ok := newCodesMap[i]; ok {
				newcout++
			}
		}

		if t, ok := block[i]; ok {
			startTime, err := time.ParseInLocation(util.YMDHMS, now.Format(util.YMD)+" 09:25:00", time.Local)
			if err != nil {
				limitUpTime = t
			} else {
				blockTime, err := time.ParseInLocation("2006-01-02 15:04:05.000", t, time.Local)
				if err != nil {
					limitUpTime = t
				} else {
					if blockTime.Before(startTime) {
						limitUpTime = now.Format(util.YMD) + " 09:25:00"
					} else {
						limitUpTime = t
					}
				}
			}

		} else {
			limitUpTime = now.Format(util.YMD) + " 09:25:00"
		}
		sql = sql + "('" + i + "'," + "'" + limitUpTime + "'," + "'" + now.Format(util.YMD) + "'," + strconv.Itoa(v.BlockCount) + "),"
	}
	if len(blockCountMap) > 0 {
		sql = strings.TrimSuffix(sql, ",")
		err = models.DB().Exec(sql).Error
		if err != nil {
			logging.Error("CronReplayStatistics :" + sql + " err:" + err.Error())
		}
	}
	dto := ReplayStatisticsDTO{
		IncTotal:    len(inc),
		DecTotal:    len(dec),
		ShBlockNum:  shcount,
		SzBlockNum:  szcount,
		CyBlockNum:  cycount,
		NewBlockNum: newcout,
		Datetime:    &now,
	}
	err = models.DB().Table("u_replay_statistics").Create(&dto).Error
	if err != nil {
		logging.Error("CronReplayStatistics insert u_replay_statistics:" + fmt.Sprintf("%v", dto) + " err:" + err.Error())
	}

}
