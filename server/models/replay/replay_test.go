package replay

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/mongo"
	"datacenter/pkg/setting"
	"testing"
)

func TestCronReplayStatistics(t *testing.T) {
	setting.Setup()
	models.Setup()
	mongo.Setup()
	gredis.Setup()
	CronReplayStatistics()
}
