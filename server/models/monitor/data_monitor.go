package monitor

import (
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type DataMonitor struct {
	Type       string `json:"type"`
	Reason     string `json:"reason"`
	Created_at string `json:"created_at"`
}

type DataMonitorListRequest struct {
	//PageSize  int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum int `json:"page_num" form:"page_num" binding:"required,gte=1"`
}

func GetDataError(page_num int) ([]DataMonitor, int) {
	var data []DataMonitor
	begin := 20*page_num - 20
	end := 20 * page_num
	//var count int
	err := mongo.GetMongoDB("shares", "Log_WrongData", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{}).All(&data)
	})
	if err != nil {
		logging.Error(" mongo获取近错误数据失败")
	}
	count := len(data)
	if begin >= count {
		return nil, count
	}
	if end > count {
		end = count
	}
	data = data[begin:end]
	return data, count
}
