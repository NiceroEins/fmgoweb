package calendar

type CalendarListRequest struct {
	PageSize int    `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum  int    `json:"page_num" form:"page_num" binding:"required,gte=1"`
	Type     int    `json:"type" form:"type" binding:"oneof=0 1 2"`
	Keyword  string `json:"keyword" form:"keyword"`
	Sort     int    `json:"sort" form:"sort"`
}
type CalendarEventRequest struct {
	IsDisplay   int    `json:"is_display" form:"is_display" binding:"oneof=1 2"`
	Type        int    `json:"type" form:"type" binding:"oneof=1 2"`
	Star        int    `json:"star" form:"star"`
	ManyMonths  int    `json:"many_months" form:"many_months"`
	ManyWeeks   int    `json:"many_weeks" form:"many_weeks"`
	Creator     int    `json:"user_id" form:"user_id" binding:"required"`
	IsAddEvent  int    `json:"is_add_event" form:"is_add_event"`
	Link        string `json:"link" form:"link"`
	Name        string `json:"name" form:"name"`
	Code        string `json:"code" form:"code"`
	Detail      string `json:"detail" form:"detail"`
	Content     string `json:"content" form:"content" binding:"required,max=500"`
	EventDate   string `json:"event_date" form:"event_date" binding:"required"`
	PublishDate string `json:"publish_date" form:"publish_date" binding:"required"`

}
type CalendarMonthRequest struct {
	Type       int    `json:"type" form:"type" binding:"oneof=0 1 2"`
	SearchDate string `json:"search_date" form:"search_date"`
}
type CalendarDeleteRequest struct {
	ID int `json:"id" form:"id"  binding:"required"`
}
type CalendarEditRequest struct {
	CalendarDeleteRequest
	CalendarEventRequest
}
