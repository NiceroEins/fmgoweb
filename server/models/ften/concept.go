package ften

import (
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"datacenter/pkg/util"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"sort"
	"strings"
	"time"
)

type NewConceptListResponse struct {
	Code   string `json:"code" form:"code"`
	Detail string `json:"detail" form:"detail"`
	Title  string `json:"title" form:"title"`
	Date   string `json:"date" form:"date"`
}

type NewConceptListRequest struct {
	PageSize int    `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum  int    `json:"page_num" form:"page_num" binding:"required,gte=1"`
	Code     string `json:"code" form:"code"`
	Analysis string `json:"analysis" form:"analysis"`
	Concept  string `json:"concept" form:"concept"`
}

type AllConceptRequest struct {
	Kw string `json:"kw" form:"kw"`
}

func get_time() (day string) {
	//获取24小时之前的时间 排除周六 周日
	now := time.Now()
	if now.Weekday() == time.Saturday || now.Weekday() == time.Sunday {
		//周六 周日 获取周五全天数据
		offset := int(time.Friday - now.Weekday())
		if offset > 0 {
			offset = -2
		}
		weekStartDate := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local).AddDate(0, 0, offset)
		day = weekStartDate.Format("2006-01-02 00:00:00")
	} else {
		if now.Weekday() == time.Monday {
			day = now.Add(-72 * time.Hour).Format("2006-01-02 15:04:05")
		} else {
			day = now.Add(-24 * time.Hour).Format("2006-01-02 15:04:05")
		}
	}

	fmt.Print(day)
	return

}

func GetAllConceptList(reqList AllConceptRequest) ([]interface{}, int) {
	//查询关键词下面所有概念
	var data []interface{}
	err := mongo.GetMongoDB("shares", "F10_conceptTheme", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{"concept_name": bson.M{"$regex": reqList.Kw, "$options": "$i"}}).Distinct("concept_name", &data)
	})
	if err != nil {
		logging.Error(" mongo获取所有概念数据失败")
	}
	fmt.Println(data)
	count := len(data)
	return data, count

}

func GetNewConceptList(reqList NewConceptListRequest) ([]map[string]interface{}, int) {
	//查询新增概念
	var data []map[string]interface{}
	var condi = bson.M{}
	var t_list []interface{}
	var count int = 0

	begin := reqList.PageSize*reqList.PageNum - reqList.PageSize
	end := reqList.PageSize * reqList.PageNum

	if reqList.Code != "" {
		condi["code"] = reqList.Code
	}
	if reqList.Concept != "" {
		//condi["title"] =bson.M{"$regex": bson.RegEx{Pattern:fmt.Sprintf("/%v/",reqList.Title),
		//	Options: "im"}}
		//condi["title"] = bson.M{"$regex":reqList.Title, "$options": "$i"}
		for _, c := range strings.Split(reqList.Concept, ",") {
			//模糊查询
			//t_list = append(t_list, bson.M{"concept_name": bson.M{"$regex": c, "$options": "$i"}})
			//指定值
			t_list = append(t_list, bson.M{"concept_name": c})

		}
		condi["$or"] = t_list

	}
	if reqList.Analysis != "" {
		condi["analysis"] = bson.M{"$regex": reqList.Analysis, "$options": "$i"}
	}
	if len(condi) == 0 {
		condi["created_time"] = bson.M{"$gte": get_time()}
	}

	fmt.Println(condi)

	//获取mongog链接
	mon_session, err := mongo.Session()
	sess := mon_session.DB("shares").C("F10_conceptTheme")
	//bs := bson.M{"$or":[]interface{}{bson.M{"title":bson.M{"$regex":"ok","$options": "$i"}},
	//	bson.M{"title":bson.M{"$regex":"零售", "$options": "$i"}}}}
	//bs := bson.D{{"$or",[]interface{}{bson.D{{"sex",1}},bson.D{{"sex",2}}}}}
	ff := sess.Find(condi).Sort("-created_time")
	//格式化count
	if t_list != nil {
		//可能为多关键字
		ff.All(&data)
		lin_dict := make(map[string][]interface{})
		for _, v := range data {
			code := v["code"].(string)
			if _, ok := lin_dict[code]; ok {
				//存在key
				lin_dict[code] = append(lin_dict[code], v)
			} else {
				lin_dict[code] = []interface{}{v}
			}
		}
		//判断长度
		var result []map[string]interface{}
		for k, v := range lin_dict {
			if len(v) == len(t_list) {
				res := make(map[string]interface{})
				res["code"] = k
				res["values"] = v
				res["created_at"] = v[0].(map[string]interface{})["created_time"]
				result = append(result, res)
			}
		}
		sort.Slice(result, func(i, j int) bool {
			v1, _ := time.Parse(util.YMDHMS, result[i]["created_at"].(string))
			v2, _ := time.Parse(util.YMDHMS, result[j]["created_at"].(string))
			return v1.After(v2)
		})
		data = result
		count = len(data)
		if begin >= count {
			return nil, count
		}
		if end > count {
			end = count
		}
		data = data[begin:end]

	} else {
		ff.Skip(begin).Limit(reqList.PageSize).All(&data)
	}
	if reqList.Analysis != "" {
		//替换搜索关键词
		for _, v := range data {
			if _, ok := v["analysis"]; ok {
				st := v["analysis"].(string)
				st = strings.Replace(st, reqList.Analysis,
					strings.Replace("<span style=\"color:red;font-weight:bold\">keyword</span>", "keyword", reqList.Analysis, -1),
					-1)
				v["analysis"] = st
			}
		}
	}
	if count == 0 {
		count, err = mon_session.DB("shares").C("F10_conceptTheme").Find(condi).Count()
		//count,err = ff.Count()
	}
	if err != nil {
		logging.Error(" mongo获取新增概念数据失败")
	}
	mon_session.Close()
	fmt.Println(data, end)
	//count := len(data)
	//fmt.Println(count)
	//fmt.Println(data)

	return data, count

}
