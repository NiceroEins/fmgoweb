package industry

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"github.com/jinzhu/gorm"
	"time"
)
const IndustryEventName = "行业事件"
const FuturesEventName = "未来事件"
const EventName = "期货异动"
// 二级分类
type EventCategory struct {
	Id                int              `gorm:"column:id"`
	FirstClassId      int              `gorm:"column:first_class_id"`
	SecondClassId     int              `gorm:"column:second_class_id"`
	Name              string           `gorm:"column:name"`
}
func(EventCategory) TableName() string {
	return "p_event_category"
}
type EventClass struct {
	Id                int            `gorm:"column:id"`
	Name              string         `gorm:"column:name"`
	Level             int            `gorm:"column:level"`
}
func(EventClass) TableName() string {
	return "p_event_class"
}
type EventData struct {
	Id                         int               `gorm:"column:id"`
	CategoryId                 int               `gorm:"column:category_id"`
	CategoryName               string            `gorm:"column:category_name"`
	Frequency                  string            `gorm:"column:frequency"`
	Year                       int               `gorm:"column:year"`
	Day                        int               `gorm:"column:day"`
	Month                      int               `gorm:"column:month"`
	Value                      string            `gorm:"column:value"`
	PublishDate                string            `gorm:"column:publish_date"`
	CreatedAt				   time.Time
	UpdatedAt                  time.Time
	DeletedAt                  *time.Time        `sql:"index"`
}
type EventStock struct {
	CategoryId                 int               `gorm:"column:category_id"`
	CategoryName               string            `gorm:"column:category_name"`
	Code                       string            `gorm:"column:code"`
	DataId                     int               `gorm:"column:data_id"`
	CreatedAt				   time.Time
	UpdatedAt                  time.Time
	DeletedAt                  *time.Time        `sql:"index"`
}
func(EventStock) TableName() string {
	return "p_event_stock"
}

// 事件库新增行业
func SaveEventClass(industryName string) error {
	// 存入一级分类  p_event_class
	var err error
	event := EventClass{}
	err = models.DB().Table("p_event_industry_class").Where("name = ? ",industryName).First(&event).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}
	if err == gorm.ErrRecordNotFound {
		eventClass := EventClass{}
		eventClass.Name = industryName
		eventClass.Level = 1
		dbs := models.DB().Table("p_event_industry_class").Create(&eventClass)
		err = dbs.Error
		if err != nil {
			return err
		}
	}
	return nil
}
// 插入事件库
func SaveEventData(eventString,desc,value string,now time.Time,stocks []string) error {
	var (
		err error
		industryName  string
	)
	// 获取一级分类
	eventClass := EventClass{}
	industryName = IndustryEventName
	if eventString == "日历事件" {
		industryName = FuturesEventName
	}
	err = models.DB().Table("p_event_industry_class").Where("name = ? ",industryName).First(&eventClass).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}
	if err == gorm.ErrRecordNotFound {
		err = SaveEventClass(industryName)
		if err != nil {
			return err
		}
	}
	eventFirstId := EventClass{}
	err = models.DB().Table("p_event_industry_class").Where("name = ? ",industryName).First(&eventFirstId).Error
	eventCategory := EventCategory{}
	err = models.DB().Table("p_event_industry_category").Where("name = ? ",eventString).
		Where("first_class_id = ? ",eventFirstId.Id).First(&eventCategory).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}
	if err == gorm.ErrRecordNotFound {
		eventCategory.Name = eventString
		eventCategory.FirstClassId = eventFirstId.Id
		eventCategory.SecondClassId = 2   // 默认是2级
		dbs := models.DB().Table("p_event_industry_category").Create(&eventCategory)
		err = dbs.Error
		if err != nil {
			return err
		}
	}
	eventData := EventData{
		CategoryId: eventCategory.Id,
		CategoryName: desc,
		Value: value,
		Frequency: "日",
		Year: now.Year(),
		Day: now.Day(),
		Month: int(now.Month()),
		PublishDate: now.Format(util.YMD),
	}
	err = models.DB().Table("p_event_industry_data").Create(&eventData).Error
	if err != nil {
		logging.Info("存入行业监控数据失败")
		return err
	}
	if len(stocks) > 0 {
		for _,stock := range stocks {
			eventStock := EventStock{
				CategoryId: eventCategory.Id,
				CategoryName: desc,
				Code: stock,
				DataId: eventData.Id,
			}
			err = models.DB().Table("p_event_industry_stock").Create(&eventStock).Error
			if err != nil {
				logging.Info("存入行业监控数据失败")
				return err
			}
		}
	}
	return nil
}