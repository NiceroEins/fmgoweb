package industry

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"strconv"
	"strings"
	"time"
)

type Stock struct {
	Code string `json:"code" form:"code"`
	Name string `json:"name" form:"name,omitempty"`
}
type IndustryEventsReq struct {
	PageNum       int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
	PageSize      int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
	EventCategory string `json:"event_category" form:"event_category"`
	StartTime     string `json:"start_time" form:"start_time"`
	EndTime       string `json:"end_time" form:"end_time"`
}
type IndustryEventsResp struct {
	EventCategory string    `json:"event_category" gorm:"column:event_category"`
	EventName     string    `json:"event_name" gorm:"column:name"`
	Stock         []Stock   `json:"stock"`
	Value         string    `json:"-" gorm:"column:value"`
	Code          string    `json:"-" gorm:"column:code"`
	Updated       string    `json:"updated"`
	Created       time.Time `json:"-" gorm:"column:created_at"`
	ID            int       `json:"id" gorm:"column:id"`
	CategoryID    int       `json:"category_id" gorm:"column:category_id"`
}
type EditIndustryStockReq struct {
	CategoryID int     `json:"category_id" form:"category_id"`
	DataID     int     `json:"data_id" form:"data_id"`
	Data       []Stock `json:"data" form:"data"`
}

const FIRST_CLASS_NAME = "行业事件"

func GetIndustryEvents(req *IndustryEventsReq) ([]IndustryEventsResp, int, error) {
	var (
		err error
		cnt int
	)
	ret := []IndustryEventsResp{}
	dbs := models.DB().Table("p_event_industry_data").
		Select("p_event_industry_data.id,p_event_industry_data.category_id,p_event_industry_data.category_name as event_category,"+
			"p_event_industry_category.name as name,p_event_industry_data.created_at,GROUP_CONCAT(DISTINCT(IF(`p_event_industry_stock`.deleted_at IS NULL,`p_event_industry_stock`.code,NULL))) AS code, GROUP_CONCAT(DISTINCT(IF(`p_event_industry_stock`.deleted_at IS NULL,b_stock_names.name,NULL))) AS value").
		Joins("LEFT JOIN p_event_industry_category ON p_event_industry_category.id = p_event_industry_data.category_id").
		Joins("LEFT JOIN p_event_industry_stock ON p_event_industry_stock.data_id = p_event_industry_data.id").
		Joins("LEFT JOIN p_event_industry_class ON p_event_industry_class.id = p_event_industry_category.first_class_id").
		Joins("LEFT JOIN b_stock_names ON b_stock_names.code = p_event_industry_stock.code").
		Where("p_event_industry_class.name = ? ", FIRST_CLASS_NAME)
	if req.EventCategory != "" {
		dbs = dbs.Where("p_event_industry_class.name = ? ", req.EventCategory)
	}
	if req.StartTime != "" {
		start, err := time.Parse(util.YMD, req.StartTime)
		if err == nil {
			dbs = dbs.Where("p_event_industry_data.created_at >= ? ", start.Format(util.YMD)+" 00:00:00")
		}
	}
	if req.EndTime != "" {
		end, err := time.Parse(util.YMD, req.EndTime)
		if err == nil {
			dbs = dbs.Where("p_event_industry_data.created_at < ? ", end.Format(util.YMD)+" 23:59:59")
		}
	}
	dbs = dbs.Group("p_event_industry_data.id")
	dbs.Count(&cnt)
	err = dbs.Order("p_event_industry_data.created_at desc").Offset((req.PageNum - 1) * req.PageSize).Limit(req.PageSize).Find(&ret).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return ret, 0, err
	}
	if err == gorm.ErrRecordNotFound {
		return ret, 0, nil
	}
	for index, _ := range ret {
		ret[index].Updated = ret[index].Created.Format(util.YMDHMS)
		ret[index].Stock = parseStock(ret[index].Code, ret[index].Value)
	}
	return ret, cnt, nil
}
func parseStock(industryCodes, industryNames string) []Stock {
	ret := make([]Stock, 0)
	codes := strings.Split(industryCodes, ",")
	names := strings.Split(industryNames, ",")
	for i := 0; i < len(codes) && i < len(names); i++ {
		ret = append(ret, Stock{
			Code: codes[i],
			Name: names[i],
		})
	}
	return ret
}

type CalendarEventsReq struct {
	PageNum       int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
	PageSize      int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
	EventCategory string `json:"event_category" form:"event_category"`
	EventContent  string `json:"event_content" form:"event_content"`
	StartTime     string `json:"start_time" form:"start_time"`
	EndTime       string `json:"end_time" form:"end_time"`
}
type CalendarEventsResp struct {
	EventCategory string              `json:"event_category" gorm:"column:event_category"`
	EventName     string              `json:"event_name" gorm:"column:name"`
	Value         string              `json:"-" gorm:"column:value"`
	Code          string              `json:"-" gorm:"column:code"`
	Updated       string              `json:"updated"`
	PublishDate   time.Time           `json:"publish_date" gorm:"column:publish_date"`
	Created       time.Time           `json:"-" gorm:"column:created_at"`
	StarStr       string              `json:"-" gorm:"column:star"`
	Stock         []Stock             `json:"stock"`
	Star          int                 `json:"star"`
	ID            int                 `json:"id" gorm:"column:id"`
	CategoryID    int                 `json:"category_id" gorm:"column:category_id"`
	SuccessRate   decimal.NullDecimal `json:"success_rate"`
}

func GetCalendarEvents(req *CalendarEventsReq) ([]CalendarEventsResp, int, error) {
	var (
		err error
		cnt int
	)
	ret := []CalendarEventsResp{}
	dbs := models.DB().Table("p_event_industry_data").
		Select("p_event_industry_data.id,p_event_industry_data.category_id,p_event_industry_data.category_name as event_category,p_event_industry_data.publish_date,p_event_industry_data.value as star," +
			"p_event_industry_category.name as name,p_event_industry_data.updated_at,GROUP_CONCAT(DISTINCT(IF(`p_event_industry_stock`.deleted_at IS NULL,`p_event_industry_stock`.code,NULL))) AS code, GROUP_CONCAT(DISTINCT(IF(`p_event_industry_stock`.deleted_at IS NULL,b_stock_names.name,NULL))) AS value").
		Joins("LEFT JOIN p_event_industry_category ON p_event_industry_category.id = p_event_industry_data.category_id").
		Joins("LEFT JOIN p_event_industry_stock ON p_event_industry_stock.data_id = p_event_industry_data.id").
		Joins("LEFT JOIN p_event_industry_class ON p_event_industry_class.id = p_event_industry_category.first_class_id").
		Joins("LEFT JOIN b_stock_names ON b_stock_names.code = p_event_industry_stock.code").
		Where("p_event_industry_data.deleted_at IS NULL").Group("p_event_industry_data.id ")
	if req.EventCategory != "" {
		dbs = dbs.Where("p_event_industry_class.name = ? ", req.EventCategory)
	} else {
		dbs = dbs.Where("p_event_industry_class.name = ? ", "未来事件")
	}
	if req.EventContent != "" {
		dbs = dbs.Where("p_event_industry_data.category_name like ? ", "%"+req.EventContent+"%")
	}
	if req.StartTime != "" {
		start, err := time.Parse(util.YMD, req.StartTime)
		if err == nil {
			dbs = dbs.Where("p_event_industry_data.publish_date >= ? ", start.Format(util.YMD)+" 00:00:00")
		}
	}
	if req.EndTime != "" {
		end, err := time.Parse(util.YMD, req.EndTime)
		if err == nil {
			dbs = dbs.Where("p_event_industry_data.publish_date < ? ", end.Format(util.YMD)+" 23:59:59")
		}
	}
	dbs.Count(&cnt)
	err = dbs.Order("p_event_industry_data.publish_date desc").Offset((req.PageNum - 1) * req.PageSize).Limit(req.PageSize).Find(&ret).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return ret, 0, err
	}
	if err == gorm.ErrRecordNotFound {
		return ret, 0, nil
	}
	for index, _ := range ret {
		ret[index].Updated = ret[index].Created.Format(util.YMDHMS)
		ret[index].Star, _ = strconv.Atoi(ret[index].StarStr)
		ret[index].Stock = parseStock(ret[index].Code, ret[index].Value)
	}
	return ret, cnt, nil
}

func EditStock(req EditIndustryStockReq) error {
	//delete
	if err := deleteStock(req.CategoryID, req.DataID); err != nil {
		return err
	}
	category, err := queryClass(req.CategoryID)
	if err != nil {
		return err
	}
	//update
	for _, u := range req.Data {
		if err := insertStock(EventStock{
			CategoryId:   req.CategoryID,
			CategoryName: category.Name,
			Code:         u.Code,
			DataId:       req.DataID,
		}); err != nil {
			return err
		}
	}
	return nil
}

func queryStock(categoryID, dataID int) ([]Stock, error) {
	var ret []Stock
	if err := models.DB().Table("p_event_industry_stock").
		Where("category_id = ?", categoryID).
		Where("data_id = ?", dataID).
		Where("deleted_at is null").
		Find(&ret).Error; err != nil {
		return ret, err
	}
	return ret, nil
}

func deleteStock(categoryID, dataID int) error {
	if err := models.DB().Table("p_event_industry_stock").
		Where("category_id = ?", categoryID).
		Where("data_id = ?", dataID).
		Delete(&EventStock{}).Error; err != nil {
		return err
	}
	return nil
}

func insertStock(stock EventStock) error {
	if err := models.DB().Table("p_event_industry_stock").
		Save(&stock).Error; err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func queryClass(id int) (EventCategory, error) {
	var ret EventCategory
	if err := models.DB().Table("p_event_industry_category").
		Where("id = ?", id).
		First(&ret).Error; err != nil {
		logging.Error("events.QueryClass() query Errors: ", err.Error())
		return ret, err
	}
	return ret, nil
}
