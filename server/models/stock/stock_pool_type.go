package stock

import (
    "datacenter/models"
    "datacenter/pkg/logging"
    "encoding/json"
    "fmt"
    "github.com/shopspring/decimal"
    "strconv"
    "time"
)

type StockPoolTypeIO struct {
    models.Simple
    Code          string          `json:"code"`
    Name          string          `json:"name"`
    TransferPrice decimal.Decimal `gorm:"type:decimal(10,2)" json:"transfer_price"`
    TimeoutAt     *time.Time      `json:"timeout_at"`
    Introduce     string          `json:"introduce"`
}

type StockPoolTypeDO struct {
    Code string `json:"code"`
    Name string `json:"name"`
}

func (io StockPoolTypeIO) TableName() string {
    return "b_stock_pool_type"
}

type StockTickDTO struct {
    Code          string          `json:"code" gorm:"column:stock_code"`
    Name          string          `json:"name" gorm:"column:name"`
    TradeDate     time.Time       `json:"trade_date" gorm:"column:trade_date"`
}

func (io StockTickDTO) TableName() string {
    return "p_stock_tick"
}

type StockPoolTypeDTO struct {
    models.Simple
    Code string `json:"code"`
    Name string `json:"name"`
    TransferPrice decimal.Decimal `gorm:"type:decimal(10,2)" json:"transfer_price"`
    TimeoutAt     *time.Time      `json:"timeout_at"`
    Introduce     string          `json:"introduce"` 
}

func (dto StockPoolTypeDTO) TableName() string {
    return "b_stock_pool_type"
}

type StockPoolTypeVO struct {
    Name string `json:"name"`
}

func (io StockPoolTypeIO) IO2DTO() *StockPoolTypeDTO {
    return &StockPoolTypeDTO{
        Simple:models.Simple{},
        Name: io.Name,
        Code: io.Code,
        TransferPrice: io.TransferPrice,
        TimeoutAt: io.TimeoutAt,
        Introduce: io.Introduce,
    }
}

func (dto StockPoolTypeDTO) DTO2VO() *StockPoolTypeVO {
    return &StockPoolTypeVO{
        Name: dto.Name,
    }
}

func GetAllStockPoolType() ([]StockPoolTypeDTO, error) {
    ret := make([]StockPoolTypeDTO, 0)
    err := models.DB().Model(StockPoolTypeDTO{}).Group("name").Order("name ASC").Find(&ret).Error
    if err != nil {
        return nil, err
    }
    return ret, nil
}

func CreateOrUpdateStockPool(io *StockPoolTypeIO) error {
    var err error
    dto := StockPoolTypeDTO{}
    models.DB().Model(StockPoolTypeDTO{}).Where(StockPoolTypeDTO{Name: io.Name, Code: io.Code}).First(&dto)
    if dto.Code != "" {
        io.Introduce = GenIntroduce(io.Code,io.Name)
        if dto.TimeoutAt == nil || dto.TimeoutAt.After(time.Now().Local()) {
            err = models.DB().Model(StockPoolTypeDTO{}).Where(StockPoolTypeDTO{Name: io.Name, Code: io.Code}).Update(StockPoolTypeDTO{TimeoutAt: io.TimeoutAt,Introduce: io.Introduce}).Error
        } else {
            createdAt := models.Simple{CreatedAt: time.Now().Local()}
            err = models.DB().Model(StockPoolTypeDTO{}).Where(StockPoolTypeDTO{Name: io.Name, Code: io.Code}).Update(StockPoolTypeDTO{TransferPrice: io.TransferPrice, TimeoutAt: io.TimeoutAt, Simple: createdAt,Introduce: io.Introduce}).Error
        }
    } else {
        io.Introduce = GenIntroduce(io.Code,io.Name)
        err = models.DB().Model(StockPoolTypeDTO{}).Create(io.IO2DTO()).Error
    }
    if err != nil {
        fmt.Println(err.Error())
        return err
    }
    return nil
}

func DelStockPool(do StockPoolTypeDO) error {
    err := models.DB().Exec("DELETE FROM b_stock_pool_type WHERE code=?", do.Code).Error
    if err != nil {
        return err
    }
    return nil
}

func GenIntroduce(code,name string) string {
    ret := ""
    switch name {
    case "突破历史新高":
        return getContinueIntroduceNewTop(code,name)
    case "涨停后换手":
        dto := StockTickDTO{}
        err := models.DB().Model(StockTickDTO{}).Where(StockTickDTO{Code: code}).
            Where("trade_date <= ?",time.Now().Format("2006-01-02")).
            Where("close = block").Order("id desc").First(&dto).Error
        if err != nil {
            logging.Info("stock.GenRemark() err:", err.Error())
            return ret
        }
        return strconv.Itoa(dto.TradeDate.Day())+"日涨停，目前处于换手调整中"
    case "小连阳突破":
        return getContinueIntroduce(code,name)
    case "人气股":
        dto := PopularDTO{}
        err := json.Unmarshal([]byte(getRedisValue(popularKey, code)), &dto)
        if err != nil {
            logging.Info("stock.GenRemark() err:", err.Error())
            return ret
        }
        if *dto.RankChange > 30 || *dto.Rank < 20{
            if *dto.RankChange < 0  {
                return "人气榜第"+strconv.Itoa(*dto.Rank)+"名  人气下降"+strconv.Itoa(*dto.RankChange*(-1))+"名"
            }
            return "人气榜第"+strconv.Itoa(*dto.Rank)+"名  人气上升"+strconv.Itoa(*dto.RankChange)+"名"
        }
        return ret
    default:
        return ret
    }
}

func getContinueIntroduce(code,name string) string  {
    ret := ""
    dto := StockPoolTypeDTO{}
    stockTickDto := StockTickDTO{}
    // 首先去股票表拿上一个工作日 然后在b_pool_type拿上一个日期对应的表数据
    err := models.DB().Model(StockPoolTypeDTO{}).Where(StockPoolTypeDTO{Name: name, Code: code}).Order("created_at desc").First(&dto).Error
    if err != nil {
        logging.Info("stock.GenRemark() err:", err.Error())
        return ret
    }
    err = models.DB().Model(StockTickDTO{}).Where(StockTickDTO{Code: code}).
        Where("trade_date <= ?",time.Now().Format("2006-01-02")).Order("created desc").First(&stockTickDto).Error
    if err != nil {
        logging.Info("stock.GenRemark() err:", err.Error())
        return ret
    }
    if dto.CreatedAt.Format("2006-01-02") == stockTickDto.TradeDate.Format("2006-01-02") {
        return "连续多日尝试突破"
    }
    return ret
}

func getContinueIntroduceNewTop(code,name string) string  {
    ret := ""
    dto := StockPoolTypeDTO{}
    stockTickDto := []StockTickDTO{}
    // 首先去股票表拿上一个工作日 然后在b_pool_type拿上一个日期对应的表数据
    err := models.DB().Model(StockPoolTypeDTO{}).Where(StockPoolTypeDTO{Name: name, Code: code}).Order("created_at desc").First(&dto).Error
    if err != nil {
        logging.Info("stock.GenRemark() err:", err.Error())
        return ret
    }
    err = models.DB().Model(StockTickDTO{}).Where(StockTickDTO{Code: code}).
        Where("trade_date <= ?",time.Now().Format("2006-01-02")).Order("created desc").Limit(2).Find(&stockTickDto).Error
    if err != nil {
        logging.Info("stock.GenRemark() err:", err.Error())
        return ret
    }
    for _,v := range stockTickDto {
        if dto.CreatedAt.Format("2006-01-02") == v.TradeDate.Format("2006-01-02") {
            return "连续多日创下新高"
        }
    }
    return ret
}

func AsycData()  {
    dto := []StockPoolTypeDTO{}
    err := models.DB().Model(StockPoolTypeDTO{}).Where("created_at >  ?","2020-09-25 00:00:00").Find(&dto).Error
    if err != nil {
        logging.Info("stock.GenRemark() err:", err.Error())
    }
    for _,v := range dto{
        err := CreateOrUpdateStockPool(&StockPoolTypeIO{
            Code: v.Code,
            Name: v.Name,
        })
        if err != nil {
            logging.Info("stock.GenRemark() err:", err.Error())
        }
    }
}