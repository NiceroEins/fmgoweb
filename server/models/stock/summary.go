package stock

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"github.com/jinzhu/gorm"
	"time"
)
type SummaryListReq struct {
	PageSize      int             `json:"page_size" form:"page_size" binding:"required,gt=0"`
	PageNum       int             `json:"page_num" form:"page_num" binding:"required,gt=0"`
	UserId        string          `json:"user_id"`
}
type SummaryListResp struct {
	ID               int             `gorm:"column:id" json:"id"`             // 主键id
	Content          string          `gorm:"column:content" json:"content"`   // 纪要内容
	Title            string          `gorm:"column:title" json:"title"`       // 纪要标题
	UserId           int             `gorm:"column:user_id" json:"user_id"`   // 用户id
	CreatedAtStr     time.Time       `gorm:"column:created_at" json:"-"`
	CreatedAt        string          `json:"created_at,omitempty" gorm:"-"`                      // 创建时间
	DeletedAt      *time.Time        `json:"-" sql:"index"`
}

func (this *SummaryListResp)TableName() string {
	return "b_stock_summary"
}
// 纪要添加编辑
type SummaryAddReq struct {
	ID             int             `gorm:"primary_key" json:"id,omitempty"`
	CreatedAt      time.Time       `json:"-"`
	UpdatedAt      time.Time       `json:"-"`
	DeletedAt      *time.Time      `json:"-" sql:"index"`
	UserId         int             `json:"user_id"`
	Content        string          `json:"content" form:"content" binding:"required"`
	Title          string          `json:"title" form:"title" binding:"required"`
}
type SummaryDelReq struct {
	ID            int              `json:"id" form:"id" binding:"required"`
}
func SummaryList(req *SummaryListReq) ([]SummaryListResp,int,error) {
	var (
		err error
		cnt int
	)
	list := []SummaryListResp{}
	dbs := models.DB().Table("b_stock_summary")
	dbs.Count(&cnt)
	err = dbs.Order("created_at desc").Offset(req.PageSize * (req.PageNum-1)).Limit(req.PageSize).Find(&list).Error
	if err != nil && err == gorm.ErrRecordNotFound{
		return list,0,err
	}
	if err == gorm.ErrRecordNotFound {
		return list,0,nil
	}
	for index,_ := range list {
		list[index].CreatedAt = list[index].CreatedAtStr.Format(util.YMDHMS)
	}
	return list,cnt,nil
}


func CreateStockSummary(req *SummaryAddReq) error {
	summary := SummaryListResp{
		UserId: req.UserId,
		Content: req.Content,
		CreatedAtStr: time.Now(),
		Title: req.Title,
	}
	if err := models.DB().Table("b_stock_summary").Create(&summary).Error; err != nil {
		logging.Error("summary.CreateStockSummary() Errors: ", err.Error())
		return err
	}
	return nil
}

func UpdateStockSummary(req *SummaryAddReq) error {
	var summaryList SummaryListResp
	err := models.DB().Table("b_stock_summary").Where("id = ? ", req.ID).First(&summaryList).Error
	if err != nil {
		logging.Info("summary.UpdateStockSummary() Errors: ", err.Error)
		return err
	}
	updateData := make(map[string]interface{})
	updateData["user_id"] = req.UserId
	updateData["content"] = req.Content
	updateData["title"] = req.Title
	err = models.DB().Table("b_stock_summary").Where("id = ? ", req.ID).Updates(updateData).Error
	if err != nil {
		logging.Info("summary.UpdateStockSummary() Errors: ", err.Error)
		return err
	}
	return nil
}

func DelStockSummary(req *SummaryDelReq) error {
	var summaryList SummaryListResp
	err := models.DB().Table("b_stock_summary").Where("id = ? ", req.ID).First(&summaryList).Error
	if err != nil {
		logging.Info("summary.UpdateStockSummary() Errors: ", err.Error)
		return err
	}
	err = models.DB().Where("id = ? ", req.ID).Delete(&SummaryListResp{}).Error
	if err != nil {
		logging.Info("summary.DelStockSummary() Errors: ", err.Error)
		return err
	}
	return nil
}