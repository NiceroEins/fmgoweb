package stock

import (
	"github.com/shopspring/decimal"
	"time"
)

type StockBaseInfoDTO struct {
	ID int64 `json:"id"`
	Code string `json:"code"`
	Name string `json:"name"`
	Zgb decimal.NullDecimal `json:"zgb"`
	Ltg decimal.NullDecimal `json:"ltg"`
	StartDate *time.Time `json:"start_date"`
	EndDate *time.Time `json:"end_date"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type StockBaseInfoVO struct {
	ID int64 `json:"id"`
	Code string `json:"code"`
	Name string `json:"name"`
	Zgb string `json:"zgb"`
	Ltg string `json:"ltg"`
	StartDate string `json:"start_date"`
	EndDate string `json:"end_date"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type StockBaseInfoIO struct {
	Code string `json:"code"`
	Name string `json:"name"`
	Zgb decimal.NullDecimal `json:"zgb"`
	Ltg decimal.NullDecimal `json:"ltg"`
	StartDate *time.Time `json:"start_date"`
	EndDate *time.Time `json:"end_date"`
}

func (io *StockBaseInfoDTO)TableName()string  {
	return "p_stock_base_info"
}

func (io *StockBaseInfoIO)TableName()string  {
	return "p_stock_base_info"
}


