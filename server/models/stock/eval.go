package stock

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"datacenter/pkg/util"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strings"
	"time"
)

func reportPeriod(t time.Time) (int, int, int) {
	year := t.Year()
	var month, day int
	switch {
	case t.Month() == 4 && t.Day() >= 15 || t.Month() == 5:
		month = 3
		day = 31
	case t.Month() >= 6 && t.Month() <= 9:
		month = 6
		day = 30
	case t.Month() >= 10 && t.Month() <= 12:
		month = 9
		day = 30
	default:
		year = year - 1
		month = 12
		day = 31
	}
	return year, month, day
}

func reportPeriodStr(t time.Time) string {
	year, month, day := reportPeriod(t)
	return fmt.Sprintf("%d-%02d-%02d", year, month, day)
}

//环比上期
func periodPastStr(t time.Time) string {
	year, month, day := reportPeriod(t)
	switch month {
	case 3:
		year -= 1
		month = 12
		day = 31
	case 6:
		month = 3
		day = 31
	case 9:
		month = 6
		day = 30
	case 12:
		month = 9
		day = 30
	}
	return fmt.Sprintf("%d-%02d-%02d", year, month, day)
}

func performance(code string) (bool, float64) {
	var np, npyr map[string]interface{}
	var ret bool
	err := mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"type": "净利润",
		}).One(&np)
	})
	if err != nil {
		logging.Error("stock.performance() query np Errors: ", err.Error())
		return ret, 0
	}

	var vNow, vPast float64
	var b1, b2 bool
	t := time.Now()
	b1, vNow = util.GetNumber(np, reportPeriodStr(t))
	b2, vPast = util.GetNumber(np, periodPastStr(t))
	if b1 && b2 {
		if vNow > vPast {
			ret = true
		}
	}

	err = mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"type": "净利润同比增长率",
		}).One(&npyr)
	})
	if err != nil {
		logging.Error("stock.performance() query npyr Errors: ", err.Error())
		return ret, 0
	}

	var v1, v2 string
	var r float64
	v1, b1 = util.GetString(npyr, reportPeriodStr(t))
	v2, b2 = util.GetString(npyr, periodPastStr(t))

	if b1 && v1 != "" {
		if r = pctg(v1); r > 0 {
			return true, r
		}
	} else if b2 && v2 != "" {
		if r = pctg(v2); r > 0 {
			return true, r
		}
	}

	return ret, r
}

//func checkPerformance(data map[string]interface{}, tp string) bool {
//	var vNow, vPast float64
//	var b1, b2 bool
//
//	switch tp {
//	case "string":
//		var v1, v2 string
//		v1, b1 = util.GetString(data, reportPeriodStr(t))
//		v2, b2 = util.GetString(data, periodPastStr(t))
//		if !b1 || !b2 {
//			return false
//		}
//		return comparePctg(v1, v2) > 0
//	case "float", "number", "double":
//		b1, vNow = util.GetNumber(data, reportPeriodStr(t))
//		b2, vPast = util.GetNumber(data, periodPastStr(t))
//		if !b1 || !b2 {
//			return false
//		}
//		return vNow > vPast
//	}
//
//	return false
//}

func research(code string) bool {
	t := time.Now().Add(-30 * 24 * time.Hour)
	var rs []string
	if err := models.DB().Table("u_research_report_data").Where("rising_space IS NOT NULL").Where("publish_date >= ?", t.Format("2006-01-02")).Limit(1).Pluck("rising_space", &rs).Error; err != nil {
		return false
	}
	if len(rs) <= 0 || rs[0] == "" {
		return false
	}
	rss := strings.Split(rs[0], ",")
	for _, v := range rss {
		rsf := util.Atof(v)
		if rsf <= 0 {
			return false
		}
	}
	return true
}

// 是否满足条件，涨停日下标（越小越近），7日涨幅
func property(code string) (bool, int, float64) {
	//conn := gredis.Clone(setting.RedisSetting.StockDB)
	//conn.HGetFloat64()
	var stock []struct {
		StockCode string
		Amount    float64
		IsBlock   int
		BlockPct  float64
		Close     float64
	}
	var ret bool
	var idx int
	var change float64
	if err := models.Data().Table("p_stock_tick").Where("stock_code = ?", code).Order("trade_date DESC").Limit(30).Find(&stock).Error; err != nil {
		return ret, idx, change
	}
	if len(stock) < 7 {
		return ret, idx, change
	}
	if stock[0].Amount+stock[1].Amount+stock[2].Amount <= 300000 {
		return ret, idx, change
	}

	change = (stock[0].Close - stock[6].Close) / stock[6].Close
	for i := 0; i < 7; i++ {
		if stock[i].IsBlock == 1 {
			ret = true
			idx = i + 1
		}
		if strings.HasPrefix(stock[i].StockCode, "3") && stock[i].BlockPct >= 9 {
			ret = true
			idx = i + 1
		}
	}

	return ret, idx, change
}

func pctg(v string) float64 {
	v = strings.Replace(v, "%", "", -1)
	return util.Atof(v)
}

type StockRate struct {
	Code        string
	Performance int
	Research    int
	Property    int
	Total       int
	Rate        float64
}

func recommendStock() {
	codes := stockCodes()

	for _, v := range codes {
		if v > "000000" {
			rate := StockRate{}
			ok, _ := performance(v)
			b, idx, ctg := property(v)

			if !b || !ok {
				rate.Total = 0
			} else {
				rate.Rate = ctg
				if idx == 0 {
					rate.Total = 1
				} else {
					rate.Total = 10 - idx
				}
				rate.Code = v
				models.Data().Table("p_stock_rate").
					Where("code = ?", v).
					Assign(&rate).
					FirstOrCreate(&StockRate{})
			}
		}
	}
}

func stockCodes() []string {
	var ret []string
	models.Data().Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Order("code").Pluck("code", &ret)
	return ret
}
