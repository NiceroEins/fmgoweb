package stock

import (
	"bytes"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"github.com/shopspring/decimal"
	"time"
)

type Tick struct {
	Date      uint32     `json:"Date"`               // 时间戳[4字节]
	Symbol    [12]byte   `json:"Symbol"`             // 产品代码[12字节]
	RawName   [16]byte   `json:"Name"`               // 产品名称[16字节]
	Price3    float32    `json:"Price3"`             // 股票为成交总笔数，期货是前一交易日结算价[4字节]
	Vol2      float32    `json:"Vol2"`               // 现量，当前最近一笔成交量[4字节]
	OpenInt   float32    `json:"Open_Int,omitempty"` // 仅期货有效，持仓（未平仓合约）[4字节]
	Price2    float32    `json:"Price2,omitempty"`   // 期货当日结算价（盘中为0，收盘后交易所才提供）[4字节]
	LastClose float32    `json:"LastClose"`          // 昨收价[4字节]
	Open      float32    `json:"Open"`               // 开盘价[4字节]
	High      float32    `json:"High"`               // 最高价[4字节]
	Low       float32    `json:"Low"`                // 最低价[4字节]
	NewPrice  float32    `json:"NewPrice"`           // 最新价[4字节]
	Volume    float32    `json:"Volume"`             // 成交量[4字节]
	Amount    float32    `json:"Amount"`             // 成交额[4字节]
	BP        [5]float32 `json:"BP"`                 //申买价（数组）[4字节]*5
	BV        [5]float32 `json:"BV"`                 //申买量（数组）[4字节]*5
	SP        [5]float32 `json:"SP"`                 //申卖价（数组）[4字节]*5
	SV        [5]float32 `json:"SV"`                 //申卖量（数组）[4字节]*5
}

var TickChan chan Tick

func Setup() {
	TickChan = make(chan Tick, 5000)
}

func SetTick(data string) {
	m := make(map[string]interface{})
	if err := json.Unmarshal([]byte(data), &m); err == nil {
		fmt.Println(data)
		//checkPing()
		return
	}

	p := split([]byte(data))
	for i := 0; i < len(p); i++ {
		if len(p[i]) <= 0 {
			continue
		}
		tick, err := decode(p[i])
		//fmt.Println(tick.NewPrice)
		if err != nil {
			logging.Error("tick.SetTick() decode Errors: ", err.Error())
			return
		}
		if tick != nil {
			//var bc []byte = tick.Symbol[0:]
			//if bc[2] == '0' || bc[2] == '6' || bc[2] == '3' {
			TickChan <- *tick
			//}
		}
	}
}

func split(b []byte) [][]byte {
	ret := make([][]byte, 0)
	data := util.ZlibUnCompress(b)
	for i := 0; i+156 < len(data); i += 156 {
		ret = append(ret, data[i:i+156])
	}
	return ret
}

func decode(data []byte) (*Tick, error) {
	buf := bytes.NewBuffer(data)
	obj := &Tick{}
	if err := binary.Read(buf, binary.LittleEndian, obj); err != nil {
		return nil, err
	}
	return obj, nil
}

var PreTenPreCloseKey = "pre_ten:pre_close"

type TickDTO struct {
	StockCode string          `json:"stock_code"`
	TsCode    string          `json:"ts_code"`
	TradeDate time.Time       `json:"trade_date"`
	Open      decimal.Decimal `json:"open"`
	High      decimal.Decimal `json:"high"`
	Low       decimal.Decimal `json:"low"`
	Close     decimal.Decimal `json:"close"`
	PreClose  decimal.Decimal `json:"pre_close"`
	Change    decimal.Decimal `json:"change"`
	PctChg    decimal.Decimal `json:"pct_chg"`
	Block     decimal.Decimal `json:"block"`
	BlockPct  decimal.Decimal `json:"block_pct"`
	IsBlock   decimal.Decimal `json:"is_block"`
	BlockNum  decimal.Decimal `json:"block_num"`
	Vol       decimal.Decimal `json:"vol"`
	Amount    decimal.Decimal `json:"amount"`
	CreatedAt time.Time       `json:"created_at"`
}
