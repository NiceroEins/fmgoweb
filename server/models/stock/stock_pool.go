package stock

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"encoding/json"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strings"
	"time"
)

var (
	turnoverKey               = "stock:amount"
	industryClassificationKey = "stock:industry:industry"
	currentPriceKey           = "stock:tick"
	lastCloseKey              = "stock:last_close"
	zgbKey                    = "stock:zgb"
	ltgKey                    = "stock:ltg"
	nameKey                   = "stock:positivity"
	popularKey                = "stock:popular"
)

type StockDTO struct {
	ID        string    `json:"id"`
	Name      string    `json:"name"`
	TradeDate time.Time `json:"trade_data"`
}

func (dto StockDTO) TableName() string {
	return "b_stock"
}

type PopularDTO struct {
	Code       string `json:"code"`
	Name       string `json:"name"`
	Rank       *int   `json:"rank"`
	RankChange *int   `json:"rank_change"`
}

type Performance struct {
	Id                   int64            `gorm:"column:id"`                     //公告表主键id
	Name                 string           `gorm:"column:name"`                   //股票名称
	Code                 string           `gorm:"column:code"`                   //股票代码
	PerformanceIncrUpper string           `gorm:"column:performance_incr_upper"` //本期预告(对应预告表里面的业绩变动幅度上限)
	PerformanceIncrLower string           `gorm:"column:performance_incr_lower"` //本期预告(对应预告表里面的业绩变动幅度下限)
	ReportPeripod        *time.Time       `gorm:"column:report_period"`          // 报告期
	NetProfitGrowthRate  *decimal.Decimal `gorm:"column:net_profit_growth_rate"` // 净利润同比增长
}

type PerformanceEdit struct {
	Code               string `json:"code" gorm:"column:code"`
	ForecastProportion string `json:"forecast_proportion" gorm:"forecast_proportion"`
	ReportPeriod       string `json:"report_period" gorm:"report_period"`
}

type ResearchReport struct {
	Id   int    `json:"id" gorm:"column:id"`
	Code string `json:"code" gorm:"column:code"` //股票代码
	Name string `json:"name" gorm:"column:name"` //股票名称
}
type UserStockPool struct {
	Num         int
	IsRecommend bool   `json:"is_recommend"`
	PictureLink string `json:"picture_link"`
	Description string `json:"description"`
	EventStr    string `json:"event_str"`
	EventType   string `json:"event_type"`
	ObjectType  string `json:"object_type"`
}
type StockPoolRaw struct {
	Code string `json:"code"`
	//Name                   string              `json:"name"`
	CurrentPrice           decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"current_price"`
	UpDownRange            decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"up_down_range"`
	Turnover               decimal.NullDecimal `gorm:"type:decimal(18,2)" json:"turnover"`
	CirculationMarketValue decimal.NullDecimal `gorm:"type:decimal(18,2)" json:"circulation_market_value"`
	TotalMarketValue       decimal.NullDecimal `gorm:"type:decimal(18,2)" json:"total_market_value"`
	TurnoverRate           decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"turnover_rate"`
	IndustryClassification string              `json:"industry_classification"`
	Type                   string              `json:"type"`
	CurrentIncrease        decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"current_increase"`
	TransferPrice          decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"transfer_price"`
	Popular                PopularDTO          `json:"popular"`
	Stock                  StockDTO            `json:"stock" gorm:"FOREIGNKEY:ID;ASSOCIATION_FOREIGNKEY:Code"`
	CreatedAt              time.Time           `json:"transfer_at"`
	Introduce              string              `json:"introduce" gorm:"column:introduces"`
	IndexNumPrice          decimal.NullDecimal `json:"indexnum_increase"`
	TransferPriceNow       decimal.NullDecimal `json:"transfer_price_now"`
}

type StockPoolDTO struct {
	//models.Simple
	ID        int        `gorm:"primary_key" json:"id,omitempty"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" sql:"index"`
	IsRearch  bool       `json:"is_rearch"`
	IsWealth  bool       `json:"is_wealth"`
	UserId    string     `gorm:"column:user_id" json:"-"`
	UserStockPool
	StockPoolRaw
	Head int `json:"head"`
	IgnorePopular bool `gorm:"-" json:"-"`
}

type StockPoolVO struct {
	Id                     int                 `json:"id"`
	Code                   string              `json:"code"`
	Name                   string              `json:"name"`
	CurrentPrice           decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"current_price"`
	UpDownRange            decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"up_down_range"`
	Turnover               decimal.NullDecimal `gorm:"type:decimal(18,2)" json:"turnover"`
	CirculationMarketValue decimal.NullDecimal `gorm:"type:decimal(18,2)" json:"circulation_market_value"`
	TotalMarketValue       decimal.NullDecimal `gorm:"type:decimal(18,2)" json:"total_market_value"`
	TurnoverRate           decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"turnover_rate"`
	IndustryClassification string              `json:"industry_classification"`
	Type                   string              `json:"type"`
	CurrentIncrease        decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"current_increase"`
	TransferAt             string              `json:"transfer_at"`
	Rank                   *int                `json:"rank"`
	RankChange             *int                `json:"rank_change"`
	IsRearch               bool                `json:"is_rearch"`
	IsWealth               bool                `json:"is_wealth"`
	Introduce              string              `json:"introduce"`
}

type StockPoolEPO struct {
	Code                   string              `json:"A"`
	Name                   string              `json:"B"`
	CurrentPrice           decimal.NullDecimal `json:"C"`
	UpDownRange            decimal.NullDecimal `json:"D"`
	Turnover               decimal.NullDecimal `json:"E"`
	CirculationMarketValue decimal.NullDecimal `json:"F"`
	TotalMarketValue       decimal.NullDecimal `json:"G"`
	TurnoverRate           decimal.NullDecimal `json:"H"`
	IndustryClassification string              `json:"I"`
	Type                   string              `json:"J"`
	CurrentIncrease        decimal.NullDecimal `json:"K"`
	TransferAt             string              `json:"L"`
	Rank                   *int                `json:"M"`
	RankChange             *int                `json:"N"`
}

type StockPoolQO struct {
	Type      string `json:"type" form:"type"`
	PageSize  int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
	PageNum   int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
	SordKey   string `json:"sord_key" form:"sort_key"`
	Direction bool   `json:"direction" form:"direction"`
}

func (dto StockPoolDTO) TableName() string {
	return "b_stock_pool_type"
}

func (dto StockPoolDTO) DTO2VO() *StockPoolVO {
	return &StockPoolVO{
		Id:                     dto.ID,
		Code:                   dto.Code,
		Name:                   dto.Stock.Name,
		CurrentPrice:           dto.CurrentPrice,
		UpDownRange:            dto.UpDownRange,
		Turnover:               dto.Turnover,
		CirculationMarketValue: dto.CirculationMarketValue,
		TotalMarketValue:       dto.TotalMarketValue,
		TurnoverRate:           dto.TurnoverRate,
		IndustryClassification: dto.IndustryClassification,
		Type:                   dto.Type,
		CurrentIncrease:        dto.CurrentIncrease,
		TransferAt:             dto.CreatedAt.Format("2006-01-02 15:04:05"),
		Rank:                   dto.Popular.Rank,
		RankChange:             dto.Popular.RankChange,
		IsRearch:               dto.IsRearch,
		IsWealth:               dto.IsWealth,
		Introduce:              dto.Introduce,
	}
}

func (dto StockPoolDTO) DTO2EPO() *StockPoolEPO {
	return &StockPoolEPO{
		Code:                   dto.Code,
		Name:                   dto.Stock.Name,
		CurrentPrice:           dto.CurrentPrice,
		UpDownRange:            dto.UpDownRange,
		Turnover:               dto.Turnover,
		CirculationMarketValue: dto.CirculationMarketValue,
		TotalMarketValue:       dto.TotalMarketValue,
		TurnoverRate:           dto.TurnoverRate,
		IndustryClassification: dto.IndustryClassification,
		Type:                   dto.Type,
		CurrentIncrease:        dto.CurrentIncrease,
		TransferAt:             dto.CreatedAt.Format("2006-01-02 15:04:05"),
		Rank:                   dto.Popular.Rank,
		RankChange:             dto.Popular.RankChange,
	}
}

func getRedisValue(key string, field string) string {
	value, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString(key, field)
	if err != nil {
		logging.Info("redis get "+key+" "+field+" value err:", err.Error())
		return ""
	}
	return value
}

func getValueFromRedis(dto *StockPoolDTO) error {
	var (
		err       error
		id        = dto.Code
		lastClose decimal.NullDecimal
		zgb       decimal.NullDecimal
		ltg       decimal.NullDecimal
	)
	//dto.Name = getRedisValue(nameKey, id)
	_ = json.Unmarshal([]byte(getRedisValue(popularKey, id)), &dto.Popular)
	err = dto.Turnover.Scan(getRedisValue(turnoverKey, id))
	if err != nil {
		dto.Turnover.Valid = false
		logging.Info("stock.getValueFromRedis() Turnover err:", err.Error())
		return err
	}
	for _, v := range strings.Split(getRedisValue(industryClassificationKey, id), " ") {
		dto.IndustryClassification = v
	}
	err = dto.CurrentPrice.Scan(getRedisValue(currentPriceKey, id))
	if err != nil {
		dto.CurrentPrice.Valid = false
		logging.Info("stock.getValueFromRedis() CurrentPrice err:", err.Error())
		return err
	} else {
		dto.CurrentIncrease.Decimal = dto.CurrentPrice.Decimal.Sub(dto.TransferPrice.Decimal).Mul(decimal.NewFromFloat(100)).DivRound(dto.TransferPrice.Decimal, 2)
		dto.CurrentIncrease.Valid = true
	}
	err = lastClose.Scan(getRedisValue(lastCloseKey, id))
	if err != nil {
		lastClose.Valid = false
		logging.Info("stock.getValueFromRedis() lastClose err:", err.Error())
	} else {
		dto.UpDownRange.Decimal = dto.CurrentPrice.Decimal.Sub(lastClose.Decimal).Mul(decimal.NewFromFloat(100)).DivRound(lastClose.Decimal, 2)
		dto.UpDownRange.Valid = true
	}
	err = zgb.Scan(getRedisValue(zgbKey, id))
	if err != nil {
		zgb.Valid = false
		logging.Info("stock.getValueFromRedis() zgb err:", err.Error())
		return err
	}
	err = ltg.Scan(getRedisValue(ltgKey, id))
	if err != nil {
		ltg.Valid = false
		logging.Info("stock.getValueFromRedis() ltg err:", err.Error())
		return err
	}
	dto.CirculationMarketValue.Decimal = ltg.Decimal.Mul(dto.CurrentPrice.Decimal)
	dto.CirculationMarketValue.Valid = true
	dto.TotalMarketValue.Decimal = zgb.Decimal.Mul(dto.CurrentPrice.Decimal)
	dto.TotalMarketValue.Valid = true
	dto.TurnoverRate.Decimal = dto.Turnover.Decimal.Mul(decimal.NewFromFloat(100)).DivRound(dto.CirculationMarketValue.Decimal, 2)
	dto.TurnoverRate.Valid = true
	return nil
}

func GetList(qo *StockPoolQO) ([]StockPoolDTO, int, error) {
	cnt := 0
	dto := make([]StockPoolRaw, 0)
	dto2 := make([]StockPoolDTO, 0)
	ret := make([]StockPoolDTO, 0)
	dbs := models.DB().Model(StockPoolDTO{}).Select("MIN(id) id,code,name,MIN(created_at) created_at,GROUP_CONCAT(name ORDER BY FIELD(name, '人气股') ASC separator '/') type,GROUP_CONCAT(introduce ORDER BY FIELD(introduce, '人气股') ASC separator '/') introduces").Where("timeout_at IS NULL OR  timeout_at>= ?", time.Now()).Group("code")
	if qo.Type != "" {
		types := strings.Split(qo.Type, ",")
		dbs = dbs.Where("code in (SELECT code FROM b_stock_pool_type WHERE name in(?) AND (timeout_at IS NULL OR  timeout_at>= ?))", types, time.Now())
	}
	dbs.Count(&cnt)
	//dto := make([]StockPoolRaw, 0)
	//ret := make([]StockPoolRaw, 0)
	err := models.DB().Raw("SELECT A.id,A.code,A.name,type,introduces,A.created_at,B.transfer_price FROM ? A LEFT JOIN b_stock_pool_type  B ON A.id=B.id", dbs.SubQuery()).Preload("Stock").
		Order("A.created_at DESC").Find(&dto).Error
	//err := dbs2.Offset(qo.PageSize * (qo.PageNum - 1)).Limit(qo.PageSize).Order("id DESC").Find(&dto).Error
	if err != nil {
		logging.Info("stock.GetList() err:", err.Error())
		return nil, cnt, err
	}
	codes := []string{}
	for i, d := range dto {
		codes = append(codes, d.Code)
		dto2 = append(dto2, StockPoolDTO{ID: i, StockPoolRaw: d})
	}
	codeWealth := GetStockWealthIconByCode(codes)
	codeRearch := GetStockRearchIconByCode(codes)
	for _, v := range dto2 {
		_ = getValueFromRedis(&v)
		if _, ok := codeWealth[v.Code]; ok {
			v.IsWealth = codeWealth[v.Code]
		}
		if _, ok := codeRearch[v.Code]; ok {
			v.IsRearch = codeRearch[v.Code]
		}
		v.Introduce = FilterFirstChar(v.Introduce)
		v.Type = FilterFirstChar(v.Type)
		ret = append(ret, v)
	}
	if qo.SordKey != "" {
		sort.Slice(ret, func(i, j int) bool {
			v := reflect.ValueOf(ret[i].StockPoolRaw)
			u := reflect.ValueOf(ret[j].StockPoolRaw)
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(ret[i].StockPoolRaw).Field(k).Tag.Get("json") == qo.SordKey {
					return qo.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	}
	return ret[qo.PageSize*(qo.PageNum-1) : util.Min(cnt, qo.PageNum*qo.PageSize)], cnt, nil
}

func GetAll(typeName string) ([]StockPoolDTO, int, error) {
	cnt := 0
	dto := make([]StockPoolRaw, 0)
	dto2 := make([]StockPoolDTO, 0)
	ret := make([]StockPoolDTO, 0)
	dbs := models.DB().Model(StockPoolDTO{}).Select("MIN(id) id,code,name,MIN(created_at) created_at,GROUP_CONCAT(name ORDER BY FIELD(name, '人气股') ASC separator '/') type").Where("timeout_at IS NULL OR  timeout_at>= ?", time.Now()).Group("code")
	if typeName != "" {
		types := strings.Split(typeName, ",")
		dbs = dbs.Where("code in (SELECT code FROM b_stock_pool_type WHERE name in(?) AND (timeout_at IS NULL OR  timeout_at>= ?))", types, time.Now())
	}
	dbs.Count(&cnt)
	//dto := make([]StockPoolRaw, 0)
	//ret := make([]StockPoolRaw, 0)
	err := models.DB().Raw("SELECT A.id,A.code,A.name,type,A.created_at,B.transfer_price FROM ? A LEFT JOIN b_stock_pool_type  B ON A.id=B.id", dbs.SubQuery()).Preload("Stock").Order("A.created_at DESC").Find(&dto).Error
	//err := dbs2.Offset(qo.PageSize * (qo.PageNum - 1)).Limit(qo.PageSize).Order("id DESC").Find(&dto).Error
	if err != nil {
		logging.Info("stock.GetList() err:", err.Error())
		return nil, cnt, err
	}
	for i, d := range dto {
		dto2 = append(dto2, StockPoolDTO{ID: i, StockPoolRaw: d})
	}
	for _, v := range dto2 {
		err = getValueFromRedis(&v)
		if err != nil {
			logging.Info("stock.GetList() err:", err.Error())
			// return nil, 0, err
		}
		ret = append(ret, v)
	}
	return ret, cnt, nil
}

func GetStockWealthIconByCode(code []string) (result map[string]bool) {
	// 1. 按照预告 50% --------->   2. 按照预测下限 50%  ----------- > 3. 按照公告 20%
	// 获取当前时间的季度日期
	single := []Performance{}
	ret := make(map[string]bool, 0)
	rate := 50
	err := models.DB().Table("u_performance_forecast").
		Where("report_period >= ?", time.Now().Format("2006-01-02")).
		Where("performance_incr_lower > ? or (performance_incr_lower is null and expected_net_profit_upper > ?)", rate, rate).
		Where("code in (?) ", code).
		Order("report_period asc").Group("code").Find(&single).Error
	if err != nil {
		logging.Info("stock.GetStockIconByCode() err:", err.Error())
		return ret
	}
	for _, v := range single {
		ret[v.Code] = true
	}
	performanceEdit := []Performance{}
	performanceEditErr := models.DB().Table("u_performance_edit").
		Select("CAST(SUBSTRING_INDEX(forecast_proportion,'~',-1) AS DECIMAL(10,2)) as forecast_proportion,code,report_period").
		Where("report_period >= ?", time.Now().Format("2006-01-02")).
		Where("forecast_proportion > ?", rate).
		Where("code in (?)  ", code).
		Order("report_period asc").Group("code").Find(&performanceEdit).Error
	if performanceEditErr != nil {
		logging.Info("stock.GetStockIconByCode() err:", performanceEditErr.Error())
		return map[string]bool{}
	}
	for _, v := range performanceEdit {
		ret[v.Code] = true
	}
	announcmentSingle := []PerformanceEdit{}
	err = models.DB().Table("u_performance_announcement").
		Where("report_period <= ?", time.Now().Format("2006-01-02")).
		Where("code in (?) ", code).
		Where("net_profit_growth_rate >= ?", 20).
		Order("report_period asc").Group("code").Find(&performanceEdit).Error
	if err != nil {
		logging.Info("stock.GetStockIconByCode() err:", err.Error())
		return ret
	}
	for _, v := range announcmentSingle {
		ret[v.Code] = true
	}
	return ret
}

func GetStockRearchIconByCode(code []string) (result map[string]bool) {
	start := time.Now().AddDate(0, 0, -7).Format("2006-01-02")
	researchReport := []ResearchReport{}
	ret := make(map[string]bool, 0)
	err := models.DB().Table("u_research_report").
		Select("id,code,name").
		Where("publish_date > ?", start).
		Where("code in(?) ", code).
		Order("publish_date desc").Group("code").Find(&researchReport).Error
	if err != nil {
		logging.Info("stock.GetStockIconByCode() err:", err.Error())
		return ret
	}
	for _, v := range researchReport {
		ret[v.Code] = true
	}
	return ret
}

func FilterFirstChar(str string) string {
	if str == "" {
		return str
	}
	if strings.Index(str, "//") == 0 {
		return str[2:]
	}
	if strings.Index(str, "/") == 0 {
		return str[1:]
	}
	if strings.Index(str, "/") == len(str)-1 {
		return str[:len(str)-1]
	}
	return str
}

func GetStockRecomendIconByCode(userID int, code string) (result bool) {
	start := time.Now().AddDate(0, 0, -5).Format("2006-01-02")
	var CodeName struct {
		Name string `json:"name" gorm:"column:name"`
	}
	err := models.DB().Table("b_stock").Where("id = ? ", code).First(&CodeName).Error
	if err != nil {
		logging.Info("GetStockRecomendIconByCode() err :", err)
	}
	var count int
	err = models.DB().Table("u_push").
		Where("created_at > ?", start).
		Where("content->'$.title' like ?", "%"+CodeName.Name+"%").
		Where(" type = ? ", "message").
		Where(" from_id = ? ", userID).
		Order("id desc").Count(&count).Error
	if err != nil {
		logging.Info("stock.GetStockRecomendIconByCode() err:", err.Error())
		return false
	}
	return count > 0
}
