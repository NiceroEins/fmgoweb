package stock

import (
	"datacenter/models"
	"fmt"
	"sync"
	"time"
)

type Minute struct {
	Code      string
	StockCode string
	Time      string
	TimeRaw   time.Time `gorm:"-"`
	Open      float64
	Close     float64
	High      float64
	Low       float64
	Volume    float64
	Amount    float64
	Change    string
	PctChange string `gorm:"column:pctchange"`
}

// ts_code []*Minute
type List struct {
	data    sync.Map
	counter int
	f       []func(data *Minute)
}

var ins List

func Init() {

}

func GetInstance() *List {
	return &ins
}

func Exec() {
	ins.Save()
}

func (l *List) Push(data *Minute) {
	go func() {
		l.push(data)
	}()
}

func (l *List) run(f func(data *Minute)) {

}

func (l *List) push(data *Minute) {
	if v, ok := l.data.Load(data.StockCode); ok {
		m, ok := v.([]*Minute)
		if ok {
			m = append(m, data)
		}
		l.data.Store(data.StockCode, m)
	} else {
		l.data.Store(data.StockCode, []*Minute{data})
	}
}

func (l *List) Pop(tsCode string) []*Minute {
	if v, ok := l.data.Load(tsCode); ok {
		m, ok := v.([]*Minute)
		if ok {
			l.data.Delete(tsCode)
			return m
		}
	}
	return []*Minute{}
}

func (l *List) PopAll() map[string][]*Minute {
	ret := make(map[string][]*Minute)
	l.data.Range(func(key, value interface{}) bool {
		tsCode := key.(string)
		v, ok := value.([]*Minute)
		if ok {
			ret[tsCode] = v
			l.data.Delete(key)
		}
		return true
	})
	return ret
}

func (l *List) Save() {
	m := l.PopAll()
	db := models.DB().Table("p_stock_minute_realtime")
	for _, value := range m {
		if len(value) <= 0 {
			continue
		}
		data := Minute{
			Code:      value[0].Code,
			StockCode: value[0].StockCode,
			Time:      value[0].TimeRaw.Format("2006-01-02 15:04:00"),
			Open:      value[0].Open,
			Close:     value[len(value)-1].Close,
			High:      value[0].High,
			Low:       value[0].Low,
			Volume:    value[len(value)-1].Volume - value[0].Volume,
			Amount:    value[len(value)-1].Amount - value[0].Amount,
			Change:    fmt.Sprintf("%.2f", value[len(value)-1].Close-value[0].Open),
		}
		if data.Open > 0 {
			data.PctChange = fmt.Sprintf("%.2f", (value[len(value)-1].Close-value[0].Open)/value[0].Open*100)
		}

		for i := 0; i < len(value); i++ {
			if value[i].High > data.High {
				data.High = value[i].High
			}
			if value[i].Low < data.Low {
				data.Low = value[i].Low
			}
		}
		db.Save(&data)
	}
}
