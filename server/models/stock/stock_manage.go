package stock

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/parser"
	"datacenter/pkg/util"
	"encoding/json"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strconv"
	"time"
)

var (
	currentPriceIndexKey = "stock:ts_tick"
)

const DELETE_STATU = 1         //个人股池删除
const RECOMMEND_STAUTS = 2     //推荐加入个人股池
const RECOMMEND_DEL_STATUS = 3 //推荐加入个人股池删除
type StockPoolManage struct {
	Id                 int                 `gorm:"column:id" json:"id"`                             //个人股票管理表
	Name               string              `gorm:"column:name" json:"-"`                            //股票名称
	Code               string              `gorm:"column:code" json:"-"`                            //股票代码
	UserId             string              `gorm:"column:user_id" json:"-"`                         //用户Id
	RecommendReason    string              `gorm:"column:recommend_reason" json:"recommend_reason"` //推荐理由
	DelReason          string              `gorm:"column:del_reason" json:"del_reason"`             //删除理由
	RealName           string              `gorm:"column:realname" json:"real_name"`                //用户昵称
	IndustryName       string              `gorm:"column:industry_name" json:"-"`                   //记录行业名称
	Status             int                 `gorm:"column:status" json:"status"`                     //状态
	TransferAt         time.Time           `gorm:"column:transfer_at" json:"-"`
	TransferPrice      decimal.NullDecimal `gorm:"column:transfer_price" json:"-"`
	CurrentPrice       decimal.NullDecimal `json:"-"`
	IndexTransferPrice decimal.NullDecimal `gorm:"column:index_transfer_price" json:"-"` // 调入指数价格
	TransferPriceNow   decimal.NullDecimal `gorm:"column:transfer_price_now" json:"-" `  // 记录当前调入价格
	IndexTransferNow   decimal.NullDecimal `gorm:"column:index_transfer_now" json:"-"`   // 记录当前调入指数价格
	DelStatus          int                 `gorm:"column:del_status" json:"del_status"`  //删除状态
	PoolType           int                 `gorm:"column:pool_type" json:"pool_type"`    //股池类型
}

type StockPoolRequest struct {
	PageSize  int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
	PageNum   int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
	SordKey   string `json:"sort_key" form:"sort_key"`
	Direction bool   `json:"direction" form:"direction"`
	UserId    string `json:"user_id" form:"user_id"`
	Search    string `json:"search" form:"search"`
}

type StockPoolManageData struct {
	StockPoolManage StockPoolManage `json:"stock_pool_manage"`
	StockPool       StockResponse   `json:"stock_pool"`
}

type StockPoolManageResponse struct {
	Total      int                   `json:"total"`
	UpdateTime string                `json:"update_time"`
	Data       []StockPoolManageData `json:"data"`
	UserInfo   []UserInfo            `json:"user_info"`
}
type Stock struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
type HomeStockPool struct {
	StockPoolManage
	Content     string `gorm:"column:content" json:"content"`           //点评内容
	PictureLink string `gorm:"column:picture_link" json:"picture_link"` //图片链接
	Description string `gorm:"column:description" json:"description"`   //情况说明
	EventType   string `gorm:"column:event_type" json:"event_type"`     //事件类型
	Head int `json:"head"`
}
type StockResponse struct {
	Id              int                 `json:"-"`
	Code            string              `json:"code"`
	Name            string              `json:"name"`
	TransferAt      string              `json:"transfer_at"`
	IndexNumPrice   decimal.Decimal     `gorm:"column:index_transfer_price" json:"indexnum_increase"`
	CurrentPrice    decimal.NullDecimal `json:"current_price"`
	UpDownRange     decimal.NullDecimal `json:"up_down_range"`
	Turnover        decimal.NullDecimal `json:"turnover"`
	CurrentIncrease decimal.NullDecimal `gorm:"type:decimal(10,2)" json:"current_increase"`
	TransferPrice   decimal.NullDecimal `gorm:"column:transfer_price" json:"-"`
}

type UserInfo struct {
	Id       string `json:"id" gorm:"column:id"`
	Realname string `json:"realname" gorm:"column:realname"`
}

func (dto StockPoolDTO) Formate() *StockResponse {
	return &StockResponse{
		Code:            dto.Code,
		Name:            dto.Stock.Name,
		CurrentPrice:    dto.CurrentPrice,
		UpDownRange:     dto.UpDownRange,
		Turnover:        dto.Turnover,
		CurrentIncrease: dto.CurrentIncrease,
		TransferAt:      dto.CreatedAt.Format("2006-01-02 15:04:05"),
		IndexNumPrice:   dto.IndexNumPrice.Decimal,
	}
}
func GetManageList(qo *StockPoolRequest) ([]StockPoolManageData, int, error) {
	cnt := 0
	dto := make([]StockPoolManage, 0)
	result := make([]StockPoolManageData, 0)
	dto2 := make([]StockPoolDTO, 0)
	ret := make([]StockPoolDTO, 0)
	tmpResult := make(map[string]StockPoolManage)
	dbs := models.DB().Table("b_stock_pool_manage").
		Joins("left join b_user on b_user.id = b_stock_pool_manage.user_id").
		Joins("left join b_stock on b_stock.id = b_stock_pool_manage.code").
		Select("b_stock_pool_manage.*,b_stock.name as name,b_user.realname,b_stock_pool_manage.created_at as transfer_at").
		Order("b_stock_pool_manage.created_at DESC")
	if qo.Search != "" {
		var stock Stock
		if parser.IsNum(qo.Search) {
			//纯数字:查看search内容是否是股票代码
			models.DB().Table("b_stock").Where("id = ?", qo.Search).Select("id").Find(&stock)
			if stock.ID == qo.Search {
				dbs = dbs.Where("b_stock_pool_manage.code = ?", qo.Search) //公告表中符合code的
			}
		} else {
			//判断search是否是股票名称
			models.DB().Table("b_stock").Where("name = ?", qo.Search).Find(&stock)
			if stock.Name == qo.Search {
				//search 为股票名称
				dbs = dbs.Where("b_stock_pool_manage.code = ?", stock.ID) //公告表中符合code的
			}
		}
	}

	if qo.UserId != "" {
		var userInfo UserInfo
		//纯数字:查看search内容是否是股票代码
		models.DB().Table("b_user").Where("id = ?", qo.UserId).Find(&userInfo)
		if userInfo.Id == qo.UserId {
			dbs = dbs.Where("b_stock_pool_manage.user_id = ?", qo.UserId) //公告表中符合code的
		}
	}
	dbs.Count(&cnt)
	err := dbs.Find(&dto).Error
	if err != nil {
		logging.Error("stock.GetManageList() err:", err.Error())
		return nil, cnt, err
	}
	for _, d := range dto {
		dto2 = append(dto2, StockPoolDTO{
			ID: d.Id, StockPoolRaw: StockPoolRaw{
				Code: d.Code, TransferPrice: d.TransferPrice,
				Stock: StockDTO{Name: d.Name}, CreatedAt: d.TransferAt, TransferPriceNow: d.TransferPriceNow,
				Type: strconv.Itoa(d.Status)}, UserId: d.UserId})
		tmpResult[d.Code+strconv.Itoa(d.Id)] = d
	}
	retLeft := make([]StockPoolDTO, 0)
	for _, v := range dto2 {
		if v.TransferPrice.Decimal.IsZero() {
			logging.Info("stock.GetManageList() err:", err.Error())
		}
		err = GetValueFromRedis(&v)
		v.IndexNumPrice.Decimal = GetIndexIncrease(v.Code, v.UserId)
		v.IndexNumPrice.Valid = true
		if err != nil {
			logging.Info("stock.GetManageList() err:", err.Error())
		}
		if v.Type == strconv.Itoa(DELETE_STATU) || v.Type == strconv.Itoa(RECOMMEND_DEL_STATUS) {
			retLeft = append(retLeft, v)
		} else {
			ret = append(ret, v)
		}
	}
	if qo.SordKey != "" {
		sort.Slice(ret, func(i, j int) bool {
			v := reflect.ValueOf(ret[i].StockPoolRaw)
			u := reflect.ValueOf(ret[j].StockPoolRaw)
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(ret[i].StockPoolRaw).Field(k).Tag.Get("json") == qo.SordKey {
					return qo.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
		sort.Slice(retLeft, func(i, j int) bool {
			v := reflect.ValueOf(retLeft[i].StockPoolRaw)
			u := reflect.ValueOf(retLeft[j].StockPoolRaw)
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(retLeft[i].StockPoolRaw).Field(k).Tag.Get("json") == qo.SordKey {
					return qo.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	}
	ret = append(ret, retLeft...)
	for _, v := range ret {
		tmp := StockPoolManageData{}
		tmp.StockPoolManage = tmpResult[v.Code+strconv.Itoa(v.ID)]
		p := v.Formate()
		tmp.StockPool = *p
		result = append(result, tmp)
	}

	if len(result) < qo.PageSize*(qo.PageNum-1) {
		return nil, cnt, err
	}
	return result[qo.PageSize*(qo.PageNum-1) : util.Min(cnt, qo.PageNum*qo.PageSize)], cnt, nil
}

func GetUserInfo() (ret []UserInfo, err error) {
	dto := []UserInfo{}
	err = models.DB().Raw("SELECT  id,realname from b_user where id in (select DISTINCT user_id from b_stock_pool_manage where type = 0)").Find(&dto).Error
	if err != nil {
		return nil, err
	}
	return dto, nil
}

func GetIndexIncrease(code, user_id string) decimal.Decimal {
	dto := StockPoolManage{}
	models.DB().Table("b_stock_pool_manage").Where("user_id = ?", user_id).Where("code = ?", code).Find(&dto)
	if dto.Status == 1 {
		dto.CurrentPrice.Decimal = dto.IndexTransferNow.Decimal
		dto.CurrentPrice.Valid = true
	} else {
		err := dto.CurrentPrice.Scan(getRedisValue(currentPriceIndexKey, GetIndexCode(code)))
		if err != nil {
			dto.CurrentPrice.Valid = false
			return decimal.Decimal{}
		}
	}
	if dto.IndexTransferPrice.Decimal.IsZero() {
		return decimal.Decimal{}
	}
	return dto.CurrentPrice.Decimal.Sub(dto.IndexTransferPrice.Decimal).Mul(decimal.NewFromFloat(100)).DivRound(dto.IndexTransferPrice.Decimal, 2)
}

// 获取指数Code码
func GetIndexCode(code string) string {
	// 000→399001   300→399006  chuangye   688→000688   6开头→000001  00开头→399005
	firstCharacter := code[0:1]
	secondCharacter := code[0:2]
	threeCharacter := code[0:3]
	codeIndex := ""
	switch {
	case threeCharacter != "000" && secondCharacter == "00":
		codeIndex = "SZ399005"
	case threeCharacter == "000" && secondCharacter == "00":
		codeIndex = "SZ399001"
	case threeCharacter == "688":
		codeIndex = "SH000688"
	case firstCharacter == "6" && threeCharacter != "688":
		codeIndex = "SH000001"
	case threeCharacter == "300":
		codeIndex = "SZ399006"
	}
	return codeIndex
}

func GetValueFromRedis(dto *StockPoolDTO) error {
	var (
		err       error
		id        = dto.Code
		lastClose decimal.NullDecimal
		zgb       decimal.NullDecimal
	)
	//dto.Name = getRedisValue(nameKey, id)
	err = dto.CurrentPrice.Scan(getRedisValue(currentPriceKey, id))
	if err != nil {
		dto.CurrentPrice.Valid = false
		return err
	}
	if dto.TransferPrice.Decimal.IsZero() {
		err = dto.TransferPrice.Scan(getRedisValue(lastCloseKey, id))
		if err != nil {
			dto.TransferPrice.Valid = false
			return err
		}
	}

	if (dto.Type == strconv.Itoa(DELETE_STATU) || dto.Type == strconv.Itoa(RECOMMEND_DEL_STATUS)) && dto.TransferPrice.Decimal.String() != "" {
		dto.CurrentPrice.Decimal = dto.TransferPriceNow.Decimal
		dto.CurrentPrice.Valid = true
	}
	err = dto.Turnover.Scan(getRedisValue(turnoverKey, id))
	if err != nil {
		dto.Turnover.Valid = false
		return err
	}
	err = lastClose.Scan(getRedisValue(lastCloseKey, id))
	if err != nil {
		lastClose.Valid = false
		return err
	}
	dto.UpDownRange.Decimal = dto.CurrentPrice.Decimal.Sub(lastClose.Decimal).Mul(decimal.NewFromFloat(100)).DivRound(lastClose.Decimal, 2)
	dto.UpDownRange.Valid = true
	dto.CurrentIncrease.Decimal = dto.CurrentPrice.Decimal.Sub(dto.TransferPrice.Decimal).Mul(decimal.NewFromFloat(100)).DivRound(dto.TransferPrice.Decimal, 2)
	dto.CurrentIncrease.Valid = true
	if !dto.IgnorePopular {
		err = json.Unmarshal([]byte(getRedisValue(popularKey, id)), &dto.Popular)
		if err != nil {
			return err
		}
	}

	err = zgb.Scan(getRedisValue(zgbKey, id))
	if err != nil {
		zgb.Valid = false
		logging.Info("stock.getValueFromRedis() zgb err:", err.Error())
		return err
	}
	dto.TotalMarketValue.Decimal = zgb.Decimal.Mul(dto.CurrentPrice.Decimal)
	dto.TotalMarketValue.Valid = true
	return nil
}
