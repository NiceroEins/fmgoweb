package stock

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/parser"
	"datacenter/pkg/util"
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"strconv"
	"time"
)

const REASEARCH_NORMAL_STAUTS = 0                              //研究股池未删除
const REASEARCH_DEL_STATUS = 1                                 //研究股池剔除
const REASEARCH_REMOVE_STATU = 2                               //研究股池删除
const REASEARCH_FIRST_LEVEL = 1                                //研究股池一级股池
const REASEARCH_SECOND_LEVEL = 2                               //研究股池二级股池
const REASEARCH_THREE_LEVEL = 3                                //研究股池三级股池
const EVENT_OBJECT_RESEARCH_STOCK_POOL = "research_stock_pool" //研究运营股市事件类型
type ReasearchStockPoolReq struct {
	PageSize  int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
	PageNum   int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
	SordKey   string `json:"sort_key" form:"sort_key"`
	Direction bool   `json:"direction" form:"direction"`
	UserId    string `json:"user_id" form:"user_id"`
	Search    string `json:"search" form:"search"`
	Keyword   string `json:"keyword" form:"keyword"`
	Level     string `json:"pool_type" form:"pool_type" binding:"required"`
}
type ReasearchStockPoolAdd struct {
	Code     string `form:"code" binding:"required" json:"code"`
	UserId   int    `form:"user_id" json:"user_id"`
	Content  string `form:"content" json:"content"`
	PoolType int    `form:"pool_type" binding:"required" json:"pool_type"`
}
type RearchStockPoolEvent struct {
	UserId       int       `json:"user_id" gorm:"user_id"`
	ObjectId     int       `json:"object_id" gorm:"object_id"`
	EventStr     string    `json:"event_str" gorm:"event_str"` // 点评内容
	CreatedAt    time.Time `json:"-" gorm:"created_at"`        // 点评时间字符串
	CreatedAtStr string    `json:"created_at"`                 // 点评时间
}
type ReasearchStockPoolEditReq struct {
	ObjectId int    `json:"object_id" form:"object_id" binding:"required"`
	Content  string `json:"content" form:"content" binding:"required"`
	UserId   int    `json:"user_id"`
}
type ReasearchStockPoolHistoryReq struct {
	ObjectId int `form:"object_id" binding:"required"`
}
type ReasearchStockPoolEditResp struct {
	ObjectId int    `gorm:"column:object_id" json:"object_id"`
	Content  string `gorm:"column:event_str" json:"content"`
}
type ReasearchStockPoolDelRep struct {
	Id        int `json:"id" form:"id" binding:"required,gt=0"`
	DelStatus int `json:"del_status" form:"del_status"` //删除状态
}
type RearchStockPool struct {
	Id                   int                       `gorm:"column:id" json:"id"`                  //个人股票管理表
	Name                 string                    `gorm:"column:name" json:"name"`              //股票名称
	Code                 string                    `gorm:"column:code" json:"code"`              //股票代码
	UserId               string                    `gorm:"column:user_id" json:"user_id"`        //用户Id
	RealName             string                    `gorm:"column:realname" json:"real_name"`     //用户昵称
	DelStatus            int                       `gorm:"column:del_status" json:"del_status"`  //删除状态
	TransferAt           time.Time                 `gorm:"column:transfer_at" json:"created_at"` //调入时间
	TransferPrice        decimal.NullDecimal       `gorm:"column:transfer_price" json:"-"`       //调入价格
	CurrentPrice         decimal.NullDecimal       `json:"-"`                                    //当前价格
	UpDownRange          decimal.NullDecimal       `json:"up_down_range"`                        //涨跌幅
	PoolType             int                       `json:"pool_type" json:"pool_type"`           //涨跌幅
	RearchStockPoolEvent `json:"stock_pool_event"` //点评内容
}
type StockUserEvent struct {
	models.Simple
	ID          int
	UserID      int    // 用户ID
	ObjectID    int    // 事件ID
	EventStatus int    // 事件状态
	EventStr    string // 点评内容
	ObjectType  string // 对象类型
	EventType   string // 事件类型
}
type ReasearchStockPoolManage struct {
	Id              int                 `gorm:"column:id"`                                       //个人股票管理表
	Code            string              `gorm:"column:code"`                                     //股票代码
	UserId          string              `gorm:"column:user_id"`                                  //用户Id
	Status          int                 `gorm:"column:status"`                                   //状态
	DelStatus       int                 `gorm:"del_status"`                                      //删除状态
	PoolType        int                 `gorm:"pool_type"`                                       //股池类型,1:一级股池;2为二级股池;3为观察股池
	TransferPrice   decimal.NullDecimal `gorm:"column:transfer_price" json:"-"`                  // 调入价格
	RecommendReason string              `gorm:"column:recommend_reason" json:"recommend_reason"` //推荐理由
}

func GetResearchManageList(req *ReasearchStockPoolReq) ([]RearchStockPool, int, error) {
	cnt := 0
	dto := make([]RearchStockPool, 0)
	ret := make([]RearchStockPool, 0)
	dbs := models.DB().Table(
		"(SELECT b_stock_pool_manage.*,b_stock.name as name,b_user.realname,b_stock_pool_manage.created_at as transfer_at," +
			"u_user_event.event_str ,u_user_event.object_id ," +
			"row_number() over(partition by b_stock_pool_manage.id order by u_user_event.id desc) as n " +
			"FROM `b_stock_pool_manage` left join b_user on b_user.id = b_stock_pool_manage.user_id  " +
			"left join b_stock on b_stock.id = b_stock_pool_manage.code " +
			"left join u_user_event on u_user_event.object_id = b_stock_pool_manage.id ORDER BY b_stock_pool_manage.created_at DESC) as a")

	if req.Search != "" {
		var stock Stock
		if parser.IsNum(req.Search) {
			//纯数字:查看search内容是否是股票代码
			models.DB().Table("b_stock").Where("id = ?", req.Search).Select("id").Find(&stock)
			if stock.ID == req.Search {
				dbs = dbs.Where("code = ?", req.Search)
			}
		} else {
			//判断search是否是股票名称
			models.DB().Table("b_stock").Where("name = ?", req.Search).Find(&stock)
			if stock.Name == req.Search {
				//search 为股票名称
				dbs = dbs.Where("code = ?", stock.ID)
			}
		}
	}
	if req.UserId != "" {
		var userInfo UserInfo
		//纯数字:查看search内容是否是股票代码
		models.DB().Table("b_user").Where("id = ?", req.UserId).Find(&userInfo)
		if userInfo.Id == req.UserId {
			dbs = dbs.Where("user_id = ?", req.UserId)
		}
	}
	if req.Keyword != "" {
		dbs = dbs.Where("event_str like ? ", "%"+req.Keyword+"%")
	}
	if req.Level != "" {
		if req.Level == "3" {
			dbs = dbs.Where("(pool_type = ? or del_status = ?) ", req.Level, REASEARCH_REMOVE_STATU)
		} else {
			dbs = dbs.Where("pool_type = ? and del_status = ?", req.Level, REASEARCH_NORMAL_STAUTS)
		}
	}
	dbs = dbs.Where("n = 1 ")
	dbs.Count(&cnt)
	err := dbs.Order("updated_at DESC").Find(&ret).Error
	if err != nil {
		logging.Error("stock.GetManageList() err:", err.Error())
		return nil, cnt, err
	}
	for index, _ := range ret {
		err = getUpDownRange(&ret[index])
		ret[index].ObjectId = ret[index].Id
		ret[index].UserId = ret[index].UserId
		if err != nil {
			logging.Info("get up_down_range err", err)
			continue
		}
	}
	if req.SordKey != "" {
		sort.Slice(dto, func(i, j int) bool {
			v := reflect.ValueOf(ret[i])
			u := reflect.ValueOf(ret[j])
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(ret[i]).Field(k).Tag.Get("json") == req.SordKey {
					return req.Direction == util.Compare(v.Field(k), u.Field(k))
				}
			}
			return false
		})
	}
	if len(ret) < req.PageSize*(req.PageNum-1) {
		return nil, cnt, err
	}
	return ret[req.PageSize*(req.PageNum-1) : util.Min(cnt, req.PageNum*req.PageSize)], cnt, nil
}

func getUpDownRange(dto *RearchStockPool) error {
	var (
		err error
		id  = dto.Code
	)
	if !dto.TransferPrice.Valid {
		return errors.New("no transfer_price")
	}
	err = dto.CurrentPrice.Scan(getRedisValue(currentPriceKey, id))
	if err != nil {
		dto.CurrentPrice.Valid = false
		return err
	}
	dto.UpDownRange.Decimal = dto.CurrentPrice.Decimal.Sub(dto.TransferPrice.Decimal).Mul(decimal.NewFromFloat(100)).DivRound(dto.TransferPrice.Decimal, 2)
	dto.UpDownRange.Valid = true
	return nil
}

func AddReasearchStock(req *ReasearchStockPoolAdd) error {
	stockManage := ReasearchStockPoolManage{
		Code:            req.Code,
		UserId:          strconv.Itoa(req.UserId),
		PoolType:        req.PoolType,
		DelStatus:       0,
		RecommendReason: "",
	}
	currentPriceKey := "stock:tick"
	err := stockManage.TransferPrice.Scan(getRedisValue(currentPriceKey, req.Code))
	if err != nil {
		logging.Info("stock_pool.AddStockRecommend() ,Errors::", err.Error())
		stockManage.TransferPrice.Valid = false
		return err
	}
	if stockManage.TransferPrice.Decimal.IsZero() {
		return errors.New("添加的股票已退市")
	}
	if err := models.DB().Table("b_stock_pool_manage").Create(&stockManage).Error; err != nil {
		return err
	}
	if req.Content != "" {
		// 添加点评内容
		event := StockUserEvent{
			UserID:     req.UserId,
			ObjectID:   stockManage.Id,
			ObjectType: EVENT_OBJECT_RESEARCH_STOCK_POOL,
			EventStr:   req.Content,
		}
		if err := models.DB().Table("u_user_event").Create(&event).Error; err != nil {
			logging.Error("userEvent.CreateUserEvent() Errors: ", err.Error())
			return err
		}
	}
	return nil
}

func GetDuplicationRecord(req *ReasearchStockPoolAdd) (bool, error) {
	stockManage := RearchStockPoolEvent{}
	err := models.DB().Table("b_stock_pool_manage").
		Where("del_status != 1").
		Where("pool_type = ? ", req.PoolType).
		Where("code = ?", req.Code).Where("user_id = ? ", req.UserId).First(&stockManage).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("stock_pool.Edit() ,Errors::", err)
		return false, err
	}
	if err == gorm.ErrRecordNotFound {
		return true, nil
	}
	return false, nil
}

func GetDuplicationRecordEdit(req *ReasearchStockPoolEditReq) (bool, error) {
	stockManage := RearchStockPoolEvent{}
	err := models.DB().Table("b_stock_pool_manage").
		Where("del_status != 1").
		Where("id = ?", req.ObjectId).First(&stockManage).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("stock_pool.Edit() ,Errors::", err)
		return false, err
	}
	if err == gorm.ErrRecordNotFound {
		return false, nil
	}
	return true, nil
}

func EditReasearchStock(req *ReasearchStockPoolEditReq) error {
	stockManage := RearchStockPoolEvent{}
	err := models.DB().Table("b_stock_pool_manage").Where("id = ?", req.ObjectId).Find(&stockManage).Error
	if err != nil {
		logging.Info("stock_pool.Edit() ,Errors::", err)
		return err
	}
	// 添加点评内容
	event := StockUserEvent{
		UserID:     req.UserId,
		ObjectID:   req.ObjectId,
		ObjectType: EVENT_OBJECT_RESEARCH_STOCK_POOL,
		EventStr:   req.Content,
	}
	if err := models.DB().Table("u_user_event").Create(&event).Error; err != nil {
		logging.Error("userEvent.CreateUserEvent() Errors: ", err.Error())
		return err
	}
	return nil
}

func GetReseachHistoryContent(req *ReasearchStockPoolHistoryReq) ([]RearchStockPoolEvent, int, error) {
	var (
		err error
		cnt int
	)
	ret := []RearchStockPoolEvent{}
	dbs := models.DB().Table("u_user_event").
		Where("object_type = ? ", EVENT_OBJECT_RESEARCH_STOCK_POOL).
		Where("object_id = ? ", req.ObjectId)
	dbs.Count(&cnt)
	err = dbs.Order("id desc").Find(&ret).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return []RearchStockPoolEvent{}, 0, err
	}
	if err == gorm.ErrRecordNotFound {
		return []RearchStockPoolEvent{}, 0, nil
	}
	for index, _ := range ret {
		ret[index].CreatedAtStr = ret[index].CreatedAt.Format(util.YMDHMS)
	}
	return ret, cnt, nil
}

func GetDuplicationRecordDel(req *ReasearchStockPoolDelRep) error {
	stockManage := RearchStockPoolEvent{}
	err := models.DB().Table("b_stock_pool_manage").Where("del_status != 1").Where("id = ?", req.Id).First(&stockManage).Error
	if err != nil {
		logging.Info("stock_pool.Edit() ,Errors::", err)
		return err
	}
	return nil
}

func DelReasearchStock(req *ReasearchStockPoolDelRep) error {
	data := make(map[string]interface{})
	stockManage := ReasearchStockPoolManage{}
	err := models.DB().Table("b_stock_pool_manage").Where("id = ?", req.Id).First(&stockManage).Error
	if err != nil {
		logging.Info("stock_pool.Edit() ,Errors::", err)
		return err
	}
	data["del_status"] = req.DelStatus
	currentPriceKey := "stock:tick"
	data["transfer_price_now"] = getRedisValue(currentPriceKey, stockManage.Code)
	data["transfer_price"] = getRedisValue(currentPriceKey, stockManage.Code)
	if req.DelStatus == REASEARCH_DEL_STATUS {
		err = models.DB().Table("b_stock_pool_manage").Where("id = ?", req.Id).Update(data).Error
	} else {
		err = models.DB().Table("b_stock_pool_manage").Where("id = ?", req.Id).Update(data).Error
	}
	if err != nil {
		return err
	}
	return nil
}

func getNewEvent(dto *RearchStockPool) error {
	ret := RearchStockPoolEvent{}
	err := models.DB().Table("u_user_event").
		Where("object_type = ? ", EVENT_OBJECT_RESEARCH_STOCK_POOL).
		Where("object_id = ? ", dto.Id).Order("id desc").First(&ret).Error
	if err != nil {
		return err
	}
	dto.UserId = strconv.Itoa(ret.UserId)
	dto.ObjectId = ret.ObjectId
	dto.EventStr = ret.EventStr
	dto.CreatedAt = ret.CreatedAt
	dto.CreatedAtStr = ret.CreatedAt.Format(util.YMDHMS)
	return nil
}
