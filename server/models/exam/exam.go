package exam

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"fmt"
	"strings"
	"time"
)

type GetReq struct {
	ID       int    `json:"id" form:"id"`
	Begin    string `json:"begin" form:"begin"`
	End      string `json:"end" form:"end"`
	OwnerID  int    `json:"owner_id,omitempty" form:"owner_id"`
	Status   string `json:"status" form:"status"`
	Reason   string `json:"reason,omitempty" form:"reason"`
	PageID   int    `json:"p" form:"p"`
	PageSize int    `json:"n" form:"n"`
}

type GetResp struct {
	Data  []Exam `json:"data"`
	Total int    `json:"total"`
}

type SummaryReq struct {
	Year     int `json:"year" form:"year"`
	Month    int `json:"month" form:"month"`
	OwnerID  int `json:"owner_id,omitempty" form:"owner_id"`
	PageID   int `json:"p" form:"p"`
	PageSize int `json:"n" form:"n"`
}

type SummaryResp struct {
	Data  []Summary `json:"data"`
	Total int       `json:"total"`
}

type Summary struct {
	Year      int     `json:"year"`
	Month     int     `json:"month"`
	Stars     float64 `json:"stars"`
	Deduct    float64 `json:"deduct"`
	OwnerID   int     `json:"owner_id"`
	OwnerName string  `json:"owner_name"`
	Cnt       int     `json:"cnt"`
	Rate      float64 `json:"rate"`
}

func (Summary) TableName() string {
	return "p_stock_share"
}

type Exam struct {
	models.Model
	Urgent    int       `json:"urgent"`
	Reason    string    `json:"reason"`
	Files     string    `json:"files"`
	Star      float64   `json:"star"`
	StarArray []float64 `json:"star_array"`
	OwnerID   int       `json:"owner_id"`
	OwnerName string    `json:"owner_name"`
	Status    string    `json:"status"`
	Comment   string    `json:"comment"`
}

func (Exam) TableName() string {
	return "p_stock_share"
}

type Name struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type SetReq struct {
	ID      int     `json:"id" form:"id" binding:"required"`
	Star    float64 `json:"star" form:"star"`
	Comment string  `json:"comment,omitempty" form:"comment"`
}

func Get(req GetReq) (*GetResp, error) {
	var ret []Exam
	var cnt int
	db := models.DB().Model(&Exam{}).Select("p_stock_share.*, b_user.realname AS owner_name").Joins("LEFT JOIN b_user ON b_user.id = p_stock_share.owner_id").Where("p_stock_share.deleted_at IS NULL").Where("b_user.realname IS NOT NULL").Where("b_user.realname <> ''")
	if req.ID != 0 {
		db = db.Where("p_stock_share.id = ?", req.ID)
	} else {
		if req.Begin != "" {
			if req.End == "" {
				req.End = time.Now().Format("2006-01-02")
			}
			db = db.Where("p_stock_share.created_at BETWEEN ? AND ?", req.Begin, req.End+" 23:59:59")
		}

		if req.OwnerID != 0 {
			db = db.Where("p_stock_share.owner_id = ?", req.OwnerID)
		}
		if req.Status != "" {
			db = db.Where("p_stock_share.status = ?", req.Status)
		}
		if req.Reason != "" {
			db = db.Where("p_stock_share.reason LIKE ?", fmt.Sprintf("%%%s%%", req.Reason))
		}
	}

	if err := db.Count(&cnt).Order("p_stock_share.status DESC, p_stock_share.created_at DESC").Limit(req.PageSize).Offset(req.PageSize * (req.PageID - 1)).Find(&ret).Error; err != nil {
		logging.Error("exam.Get() Query exam Errors: ", err.Error())
		return nil, err
	}
	for i := 0; i < len(ret); i++ {
		//unfold
		var s float64
		var p float64 = 1
		j := 0
		if ret[i].Star < 0 {
			s = -ret[i].Star
			p = -1
		} else {
			s = ret[i].Star
		}
		for ; j < int(s); j++ {
			ret[i].StarArray = append(ret[i].StarArray, p)
		}
		if d := s - float64(j); d > 0 {
			ret[i].StarArray = append(ret[i].StarArray, d*p)
		}
	}
	return &GetResp{
		Data:  ret,
		Total: cnt,
	}, nil
}

func SetStar(req SetReq) error {
	if err := models.DB().Model(&Exam{}).Where("id = ?", req.ID).Update("star", req.Star).Update("comment", req.Comment).Update("status", "graded").Error; err != nil {
		return err
	}
	return nil
}

func Del(id int) error {
	var exam Exam
	exam.ID = uint(id)
	if err := models.DB().Delete(&exam).Error; err != nil {
		return err
	}
	return nil
}

func Sum(req SummaryReq) (*SummaryResp, error) {
	var ret []Summary
	var cnt int
	var sm string
	if req.Month > 0 || (req.Year == 0 && req.Month == 0) {
		sm = "MONTH(p_stock_share.created_at)"
	} else {
		sm = "0"
	}
	db := models.DB().Model(&Summary{}).Select(fmt.Sprintf("b_user.realname AS owner_name, p_stock_share.owner_id, SUM(p_stock_share.star) AS stars, COUNT(1) AS cnt, SUM(IF(p_stock_share.star<0,p_stock_share.star,0)) AS deduct, SUM(IF(p_stock_share.star>0,1,0))/COUNT(1) AS rate, YEAR(p_stock_share.created_at) AS year, %s AS month", sm)).Joins("LEFT JOIN b_user ON b_user.id = p_stock_share.owner_id").Where("p_stock_share.deleted_at IS NULL").Where("b_user.realname IS NOT NULL").Where("b_user.realname <> ''")

	if req.Year > 0 {
		db = db.Where("YEAR(p_stock_share.created_at) = ?", req.Year)
	}
	if req.Month > 0 {
		db = db.Where("MONTH(p_stock_share.created_at) = ?", req.Month)
	}

	if req.OwnerID > 0 {
		db = db.Where("p_stock_share.owner_id = ?", req.OwnerID)
	} else {
		db = db.Where("p_stock_share.owner_id > 0")
	}

	var grp []string
	if req.OwnerID == 0 {
		grp = append(grp, "p_stock_share.owner_id")
	}
	if (req.Year > 0 && req.Month > 0) || (req.Year == 0 && req.Month == 0) {
		grp = append(grp, "YEAR(p_stock_share.created_at)", "MONTH(p_stock_share.created_at)")
	} else if req.Month == 0 {
		grp = append(grp, "YEAR(p_stock_share.created_at)")
	} else {
		grp = append(grp, "MONTH(p_stock_share.created_at)")
	}
	if len(grp) > 0 {
		db = db.Group(strings.Join(grp, ","))
	}

	if err := db.Order("year DESC, month DESC").Count(&cnt).Limit(req.PageSize).Offset(req.PageSize * (req.PageID - 1)).Find(&ret).Error; err != nil {
		logging.Error("exam.Sum() Query summary Errors: ", err.Error())
		return nil, err
	}
	return &SummaryResp{
		Data:  ret,
		Total: cnt,
	}, nil
}

func GetName() []Name {
	var ret []Name
	models.DB().Table("b_user").Select("DISTINCT(b_user.id), b_user.realname AS name").Joins("RIGHT JOIN p_stock_share ON p_stock_share.owner_id = b_user.id").Where("b_user.id > 0").Where("b_user.status = 'normal'").Where("b_user.deleted_at IS NULL").Order("name").Find(&ret)
	return ret
}
