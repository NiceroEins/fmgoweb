package admin

import (
	"datacenter/models"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"
)

type UserBaseDTO struct {
	Id        int       `json:"id"`
	Appid string `json:"appid"`
	NickName string `json:"nick_name"`
	HeadImg string `json:"head_img"`
	Mobile string `json:"mobile"`
	Openid string `json:"openid"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type UserBaseIO struct {
	Id        int       `gorm:"primary_key" json:"id" form:"id"`
	Appid string `json:"appid"`
	NickName string `json:"nick_name"`
	HeadImg string `json:"head_img"`
	Mobile string `json:"mobile"`
	Openid string `json:"openid" form:"openid" binding:"required"`
}

type UserBaseVO struct {
	Id        int     `json:"id"`
	Appid string `json:"appid"`
	NickName string `json:"nick_name"`
	HeadImg string `json:"head_img"`
	Mobile string `json:"mobile"`
	Openid string `json:"openid"`
	CreatedAt string  `json:"created_at"`
	UpdatedAt string  `json:"updated_at"`
}

type DatiUserDTO struct {
	UserBaseDTO
	OrganizationDTO
	TotalCount int `json:"total_count"`
	RightCount int `json:"right_count"`
	WrongCount int `json:"wrong_count"`
}

type DatiUserVO struct {
	UserBaseVO
	Organization string `json:"organization"`
	TotalCount int `json:"total_count"`
	RightCount int `json:"right_count"`
	WrongCount int `json:"wrong_count"`
}

func (dto UserBaseDTO) DTO2VO() crud_service.VO {
	return UserBaseVO{
		Id:        dto.Id,
		Appid: dto.Appid,
		NickName: dto.NickName,
		HeadImg: dto.HeadImg,
		Mobile: dto.Mobile,
		Openid: dto.Openid,
		CreatedAt: dto.CreatedAt.Format(util.YMDHMS),
		UpdatedAt: dto.UpdatedAt.Format(util.YMDHMS),
	}
}

func (dto DatiUserDTO)DTO2VO()crud_service.VO  {
	return DatiUserVO{
		UserBaseVO:dto.UserBaseDTO.DTO2VO().(UserBaseVO),
		Organization: dto.OrganizationDTO.DTO2VO().(OrganizationVO).Name,
		TotalCount: dto.TotalCount,
		RightCount: dto.RightCount,
		WrongCount: dto.WrongCount,
	}
}

type WxLoginQO struct {
	AppId int `json:"app_id" form:"app_id" binding:"required"`
	Code string `json:"code" form:"code" binding:"required"`
	HeadImg string `json:"head_img" form:"head_img"`
	NickName string `json:"nick_name" form:"nick_name"`
}

type appConfigDTO struct {
	Id int `json:"id"`
	ModelId int `json:"model_id"`
	Appid string `json:"appid"`
	SecretKey string `json:"secret_key"`
}

func AddUserBase(io *WxLoginQO)(map[string]interface{},error) {
	appconfig:=appConfigDTO{}
	err:=models.DB().Table("t_app_config").Where("id=?",io.AppId).First(&appconfig).Error
	if err != nil {
		return nil,err
	}
	res,err := WechatLogin(io.Code,appconfig.Appid,appconfig.SecretKey)
	if err != nil {
		return nil,err
	}
	if openid,ok:=res["openid"];ok&&openid!=""{
		id:=make([]int,0)
		err = models.DB().Table("t_user_baseinfo").Where("openid=?",openid).Pluck("id",&id).Error
		if err != nil {
			return nil,err
		}
		if len(id)>0{
			res["user_id"]=0
		}else {
			err=models.DB().Exec("INSERT IGNORE INTO t_user_baseinfo (openid,head_img,nick_name)VALUES(?,?,?)",openid,io.HeadImg,io.NickName).Error
			if err != nil {
				return nil,err
			}
			err= models.DB().Table("t_user_baseinfo").Where("openid=?",openid).Pluck("id",&id).Error
			if err != nil {
				return nil,err
			}
			if len(id)>0{
				res["user_id"]=id[0]
			}else {
				res["user_id"]=-1
				err=errors.New("用户登陆失败")
			}
		}
	}
	if err != nil {
		return nil,err
	}
	return res,nil
}

func EditUserBase(io *UserBaseIO) error {
	err := models.DB().Table("t_user_baseinfo").UpdateColumn(&io).Error
	if err != nil {
		return err
	}
	return nil
}

func DelUserBase(id int) error {
	err := models.DB().Exec(fmt.Sprintf("delete from t_user_baseinfo where id=%d", id)).Error
	if err != nil {
		return err
	}
	return nil
}
func WechatLogin(jsCode, appid, secret string) (map[string]interface{}, error) {

	Code2SessURL := "https://api.weixin.qq.com/sns/jscode2session?appid={appid}&secret={secret}&js_code={code}&grant_type=authorization_code"
	Code2SessURL = strings.Replace(Code2SessURL, "{appid}", appid, -1)
	Code2SessURL = strings.Replace(Code2SessURL, "{secret}", secret, -1)
	Code2SessURL = strings.Replace(Code2SessURL, "{code}", jsCode, -1)
	resp, err := http.Get(Code2SessURL)
	//关闭资源
	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return nil, errors.New("WechatLogin request err :" + err.Error())
	}

	var jMap map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&jMap)

	if err != nil {
		return nil, errors.New("request token response json parse err :" + err.Error())

	}
	if _,ok:=jMap["errcode"] ;!ok || jMap["errcode"] == 0 {

		return jMap, nil
	} else {
		//返回错误信息
		errcode := jMap["errcode"].(string)
		errmsg := jMap["errmsg"].(string)
		err = errors.New(errcode + ":" + errmsg)
		return nil, err
	}
}

func GetUserBases() ([]crud_service.VO, error) {
	dtos := make([]UserBaseDTO, 0)
	vos := make([]crud_service.VO, 0)
	err := models.DB().Table("t_user_baseinfo").Find(&dtos).Error
	if err != nil {
		return nil, err
	}
	for _, dto := range dtos {
		vos = append(vos, dto.DTO2VO())
	}
	return vos, nil
}

type DatiUsersQuery struct {
	Start        string `json:"start" form:"start"`
	End          string `json:"end" form:"end"`
	Organization string `json:"organization" form:"organization"`
	models.Paginate
}

func GetDatiUsers(qo *DatiUsersQuery)([]crud_service.VO,int,error)  {
	dtos := make([]DatiUserDTO, 0)
	vos := make([]crud_service.VO, 0)
	cnt:=0
	subQuery:=models.DB().Table("t_answer_log").
		Select("user_id,count(question_id) total_count,count(result='right' or null) right_count,count(result='wrong' or null) wrong_count").
		Group("user_id").SubQuery()
	dbs:=models.DB().Table("t_user_baseinfo").Select("*").
		Joins("LEFT JOIN dati_userinfo ON dati_userinfo.user_id=t_user_baseinfo.id").
		Joins("LEFT JOIN dati_organization ON dati_organization.id=dati_userinfo.organization_id").
		Joins("LEFT JOIN (?) S ON S.user_id=t_user_baseinfo.id",subQuery)
	if qo.Start != "" {
		dbs = dbs.Where("t_user_baseinfo.created_at>=?",qo.Start)
	}
	if qo.End!=""{
		dbs = dbs.Where("t_user_baseinfo.created_at<=?",qo.End+" 23:59:59")
	}
	if qo.Organization !=""{
		dbs = dbs.Where("dati_organization.name like ?","%"+qo.Organization+"%")
	}
	err := dbs.Offset((qo.PageNum-1)*qo.PageSize).Limit(qo.PageSize).Find(&dtos).Error
	dbs.Count(&cnt)
	if err != nil {
		return nil,0, err
	}
	for _, dto := range dtos {
		vos = append(vos, dto.DTO2VO())
	}
	return vos,cnt, nil
}

func GetUserIdByOpenid(openid string) (int,error) {
	id:=make([]int,0)
	err := models.DB().Table("t_user_baseinfo").Where("openid=?",openid).Pluck("id",&id).Error
	if err != nil {
		return 0,err
	}
	if len(id)>0{
		return id[0], err
	}
	return 0, errors.New("用户不存在")
}

func GetDatiUserByOpenid(io *QuestionLogQO) (crud_service.VO,error)  {
	dto:=DatiUserDTO{}
	err:=models.DB().Table("t_user_baseinfo").Select("*").
		Joins("LEFT JOIN dati_userinfo ON dati_userinfo.user_id=t_user_baseinfo.id").
		Joins("LEFT JOIN dati_organization ON dati_organization.id=dati_userinfo.organization_id").
		Where("openid=?",io.Openid).First(&dto).Error
	if err != nil {
		return nil, err
	}
	vo:=dto.DTO2VO()
	return vo, nil
}