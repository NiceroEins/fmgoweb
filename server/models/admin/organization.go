package admin

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"fmt"
	"time"
)

type OrganizationDTO struct {
	Id        int       `json:"id"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type OrganizationIO struct {
	Id   int    `gorm:"primary_key" json:"id" form:"id"`
	Name string `json:"name" form:"name"`
}

type OrganizationQO struct {
	Name string `json:"name"`
	models.Paginate
}

type OrganizationVO struct {
	Id        int    `json:"id"`
	Name      string `json:"name"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func (dto OrganizationDTO) DTO2VO() crud_service.VO {
	return OrganizationVO{
		Id:        dto.Id,
		Name:      dto.Name,
		CreatedAt: dto.CreatedAt.Format(util.YMDHMS),
		UpdatedAt: dto.UpdatedAt.Format(util.YMDHMS),
	}
}

func AddOrganization(io *OrganizationIO) (int, error) {
	err := models.DB().Table("dati_organization").Create(&io).Error
	if err != nil {
		return 0, err
	}
	return io.Id, nil
}

func EditOrganization(io *OrganizationIO) error {
	id := io.Id
	io.Id = 0
	err := models.DB().Table("dati_organization").Where("id=?", id).UpdateColumn(&io).Error
	if err != nil {
		return err
	}
	return nil
}

func DelOrganization(id int) error {
	err := models.DB().Exec(fmt.Sprintf("delete from dati_organization where id=%d", id)).Error
	if err != nil {
		return err
	}
	return nil
}

func GetOrganizations(io *OrganizationQO) ([]crud_service.VO, int, error) {
	dtos := make([]OrganizationDTO, 0)
	vos := make([]crud_service.VO, 0)
	cnt := 0
	dbs := models.DB().Table("dati_organization")
	if io.Name != "" {
		dbs = dbs.Where("name like ?", "%"+io.Name+"%")
	}
	err := dbs.Offset((io.PageNum-1)*io.PageSize).Limit(io.PageSize).Find(&dtos).Error
	dbs.Count(&cnt)
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		vos = append(vos, dto.DTO2VO())
	}
	return vos, cnt, nil
}

func GetOrganizationsAll() ([]crud_service.VO, error) {
	dtos := make([]OrganizationDTO, 0)
	vos := make([]crud_service.VO, 0)
	dbs := models.DB().Table("dati_organization")
	err := dbs.Find(&dtos).Error
	if err != nil {
		return nil, err
	}
	for _, dto := range dtos {
		vos = append(vos, dto.DTO2VO())
	}
	return vos, nil
}

type OrgIPOArr struct {
	Arr []OrganizationIPO `json:"arr" form:"arr"`
}

type OrganizationIPO struct {
	Organization string `json:"organization" form:"organization"`
}

func ImportOrganizations(io *OrgIPOArr) error {
	for _,v:=range io.Arr{
		err:=models.DB().Exec("INSERT IGNORE INTO dati_organization (name)VALUES(?)",v.Organization).Error
		if err != nil {
			logging.Error("导入单位报错",err.Error)
			return err
		}
	}
	return nil
}

type DatiUserInfoIO struct {
	UserId int `json:"user_id" form:"user_id" binding:"required"`
	OrganizationId int `json:"organization_id" form:"organization_id" binding:"required"`
}

func AddUserOrganization(io *DatiUserInfoIO) error {
	err := models.DB().Exec("INSERT IGNORE INTO dati_userinfo (user_id,organization_id)VALUES(?,?)",io.UserId,io.OrganizationId).Error
	if err != nil {
		return err
	}
	return nil
}
