package admin

import (
	"datacenter/models"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"gorm.io/datatypes"
	"math/rand"
	"time"
)

type QuestionDTO struct {
	Id        int            `json:"id"`
	ModelId   int            `json:"model_id"`
	Question  string         `json:"question"`
	Answer    string         `json:"answer"`
	Type      string         `json:"type"`
	Selects   datatypes.JSON `json:"selects"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
}

type QuestionIO struct {
	Id       int            `gorm:"primary_key" json:"id" form:"id"`
	ModelId  int            `json:"model_id" form:"model_id"`
	Question string         `json:"question" form:"question"`
	Answer   string         `json:"answer" form:"answer"`
	Type     string         `json:"type" form:"type"`
	Selects  datatypes.JSON `json:"selects" form:"selects"`
}

type Select struct {
	Select  string `json:"select" form:"select"`
	Context string `json:"context" form:"context"`
	Active bool `json:"active"`
}

type QuestionVO struct {
	Id        int      `json:"id"`
	ModelId   int      `json:"model_id"`
	Question  string   `json:"question"`
	Answer    string   `json:"answer"`
	Type      string   `json:"type"`
	Selects   []Select `json:"selects"`
	CreatedAt string   `json:"created_at"`
	UpdatedAt string   `json:"updated_at"`
}

type QuestionQO struct {
	Type string `json:"type" form:"type"`
	models.Paginate
}

func (dto QuestionDTO) DTO2VO() crud_service.VO {
	vo := QuestionVO{
		Id:        dto.Id,
		ModelId:   dto.ModelId,
		Question:  dto.Question,
		Answer:    dto.Answer,
		Type:      dto.Type,
		CreatedAt: dto.CreatedAt.Format(util.YMDHMS),
		UpdatedAt: dto.UpdatedAt.Format(util.YMDHMS),
	}
	json.Unmarshal(dto.Selects, &vo.Selects)
	if dto.Type=="trueFalse"{
		for k,_:=range vo.Selects{
			vo.Selects[k].Context=""
		}
	}
	return vo
}

type QuestionLogQO struct {
	Openid string `json:"openid" form:"openid" binding:"required"`
	Date   string `json:"date" form:"date"`
}

type QuestionLogDTO struct {
	Id         int         `json:"id"`
	Questions  QuestionDTO `json:"questions" gorm:"FOREIGNKEY:Id;ASSOCIATION_FOREIGNKEY:QuestionId"`
	UserId     int         `json:"user_id"`
	QuestionId int         `json:"question_id"`
	Answer     string      `json:"answer"`
	Result     string      `json:"result"`
	CreatedAt  time.Time   `json:"created_at"`
	UpdatedAt  time.Time   `json:"updated_at"`
}

type QuestionLogVO struct {
	Id         int        `json:"id"`
	Questions  QuestionVO `json:"questions"`
	UserId     int        `json:"user_id"`
	QuestionId int        `json:"question_id"`
	Answer     string     `json:"answer"`
	Result     string     `json:"result"`
	Date       string     `json:"date"`
	CreatedAt  string     `json:"created_at"`
	UpdatedAt  string     `json:"updated_at"`
}

func (dto *QuestionLogDTO) DTO2VO() crud_service.VO {
	vo := QuestionLogVO{
		Id:         dto.Id,
		UserId:     dto.UserId,
		QuestionId: dto.QuestionId,
		Answer:     dto.Answer,
		Result:     dto.Result,
		Date:       dto.CreatedAt.Format(util.YMD),
		CreatedAt:  dto.CreatedAt.Format(util.YMD),
		UpdatedAt:  dto.UpdatedAt.Format(util.YMD),
	}
	vo.Questions = dto.Questions.DTO2VO().(QuestionVO)
	return vo
}

func AddQuestion(io *QuestionIO) error {
	io.Selects = []byte(io.Selects)
	err := models.DB().Table("t_questions").Create(&io).Error
	if err != nil {
		return err
	}
	return nil
}

func EditQuestion(io *QuestionIO) error {
	id := io.Id
	io.Id = 0
	err := models.DB().Table("t_questions").Where("id=?", id).UpdateColumn(&io).Error
	if err != nil {
		return err
	}
	return nil
}

func DelQuestion(id int) error {
	err := models.DB().Exec(fmt.Sprintf("delete from t_questions where id=%d", id)).Error
	if err != nil {
		return err
	}
	return nil
}

func GetQuestions(io *QuestionQO) ([]crud_service.VO, int, error) {
	dtos := make([]QuestionDTO, 0)
	vos := make([]crud_service.VO, 0)
	cnt := 0
	dbs := models.DB().Table("t_questions")
	if io.Type != "" {
		dbs = dbs.Where("type=?", io.Type)
	}
	err := dbs.Offset((io.PageNum - 1) * io.PageSize).Limit(io.PageSize).Find(&dtos).Error
	dbs.Count(&cnt)
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		vos = append(vos, dto.DTO2VO())
	}
	return vos, cnt, nil
}

func randSome(db *gorm.DB, typ string, num int) ([]QuestionDTO, error) {
	dtos := make([]QuestionDTO, 0)
	cnt := 0
	if num <= 0 {
		return nil, errors.New("题目数量不足")
	}
	db.Where("type=?", typ).Count(&cnt)
	if cnt == num {
		err := db.Where("type=?", typ).Find(&dtos).Error
		if err != nil {
			return nil, err
		}
	}
	if cnt < 1 {
		return nil, errors.New("题目数量不足")
	}
	offset:=0
	if cnt>1{
		offset = rand.Intn(cnt - 1)
	}
	err := db.Where("type=?", typ).Offset(offset).Limit(num).Find(&dtos).Error
	if err != nil {
		return nil, err
	}
	return dtos, nil
}

func GetRandQuestions(openid string) ([]crud_service.VO, bool, error) {
	vos := make([]crud_service.VO, 0)
	cnt := 0
	models.DB().Table("t_answer_log").
		Joins("LEFT JOIN t_user_baseinfo on t_user_baseinfo.id=t_answer_log.user_id").
		Where("t_user_baseinfo.openid=?", openid).Count(&cnt)
	if cnt > 0 {
		return nil, true, nil
	}
	dbs := models.DB().Table("t_questions")
	dtos, err := randSome(dbs, "oneSelect", 2)
	if err != nil {
		return nil, false, err
	}
	trueFasles, err := randSome(dbs, "trueFalse", 1)
	if err != nil {
		return nil, false, err
	}
	dtos = append(dtos, trueFasles...)
	for _, dto := range dtos {
		vos = append(vos, dto.DTO2VO())
	}
	return vos, false, nil
}

func GetUserQuestionLogDetail(openid, date string) ([]crud_service.VO, error) {
	vos := make([]crud_service.VO, 0)
	dtos := make([]QuestionLogDTO, 0)
	dbs := models.DB().Table("t_answer_log").Select("*").
		Preload("Questions", func(db *gorm.DB) *gorm.DB {
			return db.Table("t_questions")
		}).Joins("LEFT JOIN t_user_baseinfo ON t_user_baseinfo.id=t_answer_log.user_id").
		Where("t_user_baseinfo.openid=?", openid).
		Where("t_answer_log.created_at between ? and ?", date+" 00:00:00", date+" 23:59:59")
	err := dbs.Find(&dtos).Error
	if err != nil {
		return nil, err
	}
	for _, dto := range dtos {
		vos = append(vos, dto.DTO2VO())
	}
	return vos, nil
}

func GetUserQuestionLog(openid string) ([]crud_service.VO, error) {
	vos := make([]crud_service.VO, 0)
	dtos := make([]QuestionLogDTO, 0)
	newDtos:=make([]QuestionLogDTO,0)
	dbs := models.DB().Table("t_answer_log").Select("*").
		Joins("LEFT JOIN t_user_baseinfo ON t_user_baseinfo.id=t_answer_log.user_id").
		Where("t_user_baseinfo.openid=?", openid).
		Group("DATE_FORMAT(t_answer_log.created_at,'%Y%m%d')").
		Order("t_answer_log.created_at desc")
	err := dbs.Find(&dtos).Error
	if err != nil {
		return nil, err
	}
	if len(dtos)>0{
		todayStart,_:=time.ParseInLocation(util.YMD,time.Now().Format(util.YMD),time.Local)
		todayEnd := todayStart.AddDate(0,0,1)
		i:=0
		for i<len(dtos){
			if todayEnd.Before(dtos[i].CreatedAt){
				newDtos= append(newDtos, dtos[i])
				i++
			}else if todayEnd.After(dtos[i].CreatedAt)&& todayStart.Before(dtos[i].CreatedAt){
				newDtos= append(newDtos, dtos[i])
				i++
			}else if todayStart.After(dtos[i].CreatedAt){
				newDtos= append(newDtos, QuestionLogDTO{
					CreatedAt: todayStart,
				})
			}
			todayEnd = todayEnd.AddDate(0,0,-1)
			todayStart = todayStart.AddDate(0,0,-1)
		}
	}
	for _, dto := range newDtos {
		vos = append(vos, dto.DTO2VO())
	}
	return vos, nil
}

type Answer struct {
	Answer string `json:"answer" form:"answer"`
	QuestionId int `json:"question_id" form:"question_id"`
	Result string `json:"result" form:"result"`
}

type Answers struct {
	Answers []Answer `json:"answers" form:"answers"`
	Openid string `json:"openid" form:"openid"`
}

type AnswerStatus struct {
	Answered bool `json:"answered"`
	IsLogin bool `json:"is_login"`
	Success bool `json:"success"`
}

func SetQuestionLog(io *Answers) (AnswerStatus,error) {
	cnt:=0
	userId:=make([]int,0)
	err:=models.DB().Table("t_user_baseinfo").Where("openid=?",io.Openid).Pluck("id",&userId).Error
	if err != nil {
		return AnswerStatus{},err
	}
	if len(userId)>0{
		models.DB().Table("t_answer_log").
			Where("user_id=?",userId[0]).
			Where("created_at between ? and ?",time.Now().Format(util.YMD)+" 00:00:00",time.Now().Format(util.YMD)+" 23:59:59").
			Count(&cnt)
		if cnt>0{
			return AnswerStatus{Answered: true,IsLogin: true}, errors.New("今日已回答")
		}else {
			tx:=models.DB().Begin()
			for _,v:=range io.Answers{
				err:=tx.Exec("INSERT INTO t_answer_log (user_id,answer,question_id,result)VALUES(?,?,?,?)",userId[0],v.Answer,v.QuestionId,v.Result).Error
				if err != nil {
					tx.Rollback()
					return AnswerStatus{}, err
				}
			}
			tx.Commit()
			return AnswerStatus{Answered: false,IsLogin: true}, nil
		}
	}else {
		return AnswerStatus{},errors.New("未登陆")
	}
}

func DelUserQuestionLog(openid string) error {
	id,err:=GetUserIdByOpenid(openid)
	err=models.DB().Exec("DELETE FROM t_answer_log where user_id=?",id).Error
	return err
}

type QuestionsArr struct {
	Arr []QuestionIPO `json:"arr" form:"arr"`
	ModelId int `json:"model_id"`
	Type string `json:"type"`
}

type QuestionIPO struct {
	Question string `json:"question" form:"question"`
	A Select `json:"a" form:"a"`
	B Select `json:"b" form:"b"`
	C Select `json:"c" form:"c"`
	Answer string `json:"answer"`
}

func ImportQuestions(io *QuestionsArr) error {
	var err error
	for _,v:=range io.Arr{
		add:=QuestionIO{
			ModelId: io.ModelId,
			Type: io.Type,
			Question: v.Question,
			Answer: v.Answer,
		}
		selects:=[]Select{v.A,v.B,v.C}
		if io.Type=="trueFalse"{
			selects=[]Select{{Select: "正确",Context: "正确"},{Select: "错误",Context: "错误"}}
		}
		add.Selects,err=json.Marshal(&selects)
		if err != nil {

		}
		err=AddQuestion(&add)
	}
	return err
}
