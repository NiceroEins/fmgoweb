package admin

import (
	"datacenter/models"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"encoding/json"
	"fmt"
	"time"
)

type AdminUser struct {
	Id int `json:"id"`
	Avatar string `json:"avatar"`
	UserName string `json:"user_name"`
	Password string `json:"password"`
	Roles []byte `json:"roles"`
	CreatedAt time.Time `json:"created_at"`
}

type AdminUserVO struct {
	Id int `json:"id"`
	Avatar string `json:"avatar"`
	Name string `json:"name"`
	Username string `json:"username"`
	Roles []string `json:"roles"`
	CreatedAt string `json:"created_at"`
}

func (dto AdminUser)DTO2VO()crud_service.VO  {
	vo:= AdminUserVO{
		Id: dto.Id,
		Avatar: dto.Avatar,
		Name: dto.UserName,
		Username: dto.UserName,
		CreatedAt: dto.CreatedAt.Format(util.YMDHMS),
	}
	err:=json.Unmarshal(dto.Roles,&vo.Roles)
	fmt.Println(err)
	return vo
}

func (au *AdminUser) TableName() string {
	return "t_admin_users"
}

func GetUserInfoByNameAndPwd(userName,pwd string)(int,string,error)  {
	au:=AdminUser{UserName: userName,Password: pwd}
	err:=models.DB().Where(au).First(&au).Error
	if err != nil {
		return 0, "", err
	}
	return au.Id,au.UserName,nil
}

func GetUserInfoById(uid int)(crud_service.VO,error)  {
	au:=AdminUser{Id: uid}
	err:=models.DB().Where(au).First(&au).Error
	if err != nil {
		return nil, err
	}
	vo:=au.DTO2VO()
	return vo,nil
}
