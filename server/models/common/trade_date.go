package common

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"fmt"
	"time"
)

type TradeDate struct {
	time.Time `json:"trade_date"`
}

func GetTradeDateList(startDate string, days int, withToday bool) ([]time.Time, bool) {
	td := make([]time.Time, 0)
	dbs := models.DB().Table("b_trade_date").Select("trade_date")
	if days > 0 {
		if withToday {
			dbs = dbs.Where("trade_date>=?", startDate)
		} else {
			dbs = dbs.Where("trade_date>?", startDate)
		}
		dbs = dbs.Order("trade_date asc").Limit(days)
	} else if days < 0 {
		if withToday {
			dbs = dbs.Where("trade_date<=?", startDate)
		} else {
			dbs = dbs.Where("trade_date<?", startDate)
		}
		dbs = dbs.Order("trade_date desc").Limit(-days)
	} else {
		return nil, false
	}
	err := dbs.Pluck("trade_date", &td).Error
	if err != nil {
		logging.Error("获取"+startDate+"前"+fmt.Sprintf("%d", days)+"个交易日失败", err.Error())
		return nil, false
	}
	/*if withToday{
		now:=time.Now()
		today:=TradeDateList{time.Date(now.Year(),now.Month(),now.Day(),0,0,0,0,time.Local)}
		*td= append(today, *td...)
	}*/
	return td, td[0].Format(util.YMD) == time.Now().Format(util.YMD)
}

func (td *TradeDate) GetDay(startDate string, day int) bool {
	t := make([]time.Time, 0)
	dbs := models.DB().Table("b_trade_date").Select("trade_date")
	if day < 0 {
		dbs = dbs.Where("trade_date<?", startDate).Order("trade_date desc").Offset(-day - 1)
	} else if day > 0 {
		dbs = dbs.Where("trade_date>?", startDate).Order("trade_date asc").Offset(day - 1)
	} else {
		dbs = dbs.Where("trade_date=?", startDate)
	}
	err := dbs.Limit(1).Pluck("trade_date", &t).Error
	if err != nil {
		logging.Error("获取"+startDate+"前1个交易日失败", err.Error())
		return false
	}
	if len(t) == 0 {
		logging.Error("获取" + startDate + "前1个交易日失败")
		return false
	}
	td.Time = t[0]
	return true
}
