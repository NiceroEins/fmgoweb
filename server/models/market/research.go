package market

import (
	"datacenter/models"
	"datacenter/pkg/util"
	"time"
)

type ResearchData struct {
	models.Simple
	Code         string
	Name         string
	Organization string
	TargetPrice  string
	RisingSpace  string
	MaxRs        float64
	PublishDate  time.Time
}

type ResearchReq struct {
	Organization string    `json:"organization" form:"organization"`
	Begin        time.Time `json:"begin,omitempty" form:"begin,omitempty"`
	End          time.Time `json:"end,omitempty" form:"end,omitempty"`
	PageSize     int       `json:"n,omitempty" form:"n,omitempty"`
	PageNum      int       `json:"p,omitempty" form:"p,omitempty"`
}

func QueryResearch(req ResearchReq) ([]ResearchData, int, error) {
	var ret []ResearchData
	var total int
	db := models.Data().Table("u_research_report").
		Select("u_research_report.*, u_research_report_data.target_price, u_research_report_data.rising_space, SUBSTRING_INDEX(u_research_report_data.rising_space,',',-1) AS max_rs").
		Joins("LEFT JOIN u_research_report_data ON u_research_report_data.report_id = u_research_report.id").Where("SUBSTRING_INDEX(u_research_report_data.rising_space,',',-1) >= 0.3").
		Where("u_research_report.publish_date > ?", time.Now().AddDate(0, -6, 0).Format(util.YMD))

	if req.Organization != "" {
		db = db.Where("u_research_report.organization = ?", req.Organization)
	}

	if !req.Begin.IsZero() {
		db = db.Where("u_research_report.publish_date >= ?", req.Begin.Format(util.YMD))
	}
	if !req.End.IsZero() {
		db = db.Where("u_research_report.publish_date <= ?", req.End.Format(util.YMD))
	}

	db = db.Group("u_research_report.code, u_research_report.publish_date").
		Order("u_research_report.publish_date DESC").Count(&total)

	//if req.PageNum > 0 && req.PageSize > 0 {
	//	db = db.Limit(req.PageSize).Offset(req.PageSize * (req.PageNum - 1))
	//}
	if err := db.Find(&ret).Error; err != nil {
		return nil, 0, err
	}
	return ret, total, nil
}

func QueryPerfDate(code string) ([]string, error) {
	tables := []string{
		"p_acem_express",
		"u_performance_forecast",
		"u_performance_announcement",
	}
	attr := []string{
		"notice_date",
		"publish_date",
		"publish_date",
	}
	var ret []string
	for i := 0; i < 3; i++ {
		var dates []string
		if err := models.Data().Table(tables[i]).
			Where("code = ?", code).
			Pluck(attr[i], &dates).Error; err != nil {
			return nil, err
		}
		ret = append(ret, dates...)
	}
	return util.RemoveDuplicateElementStr(ret), nil
}

func QueryResearchDetail() ([]ResearchData, error) {
	return nil, nil
}
