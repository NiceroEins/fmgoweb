package market

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"time"
)

type Data struct {
	models.Simple
	CategoryID   int
	CategoryName string
	ClassName    string
	Value        float64
	Unit         string
	Frequency    string
	Year         int
	Month        int
	Week         int
	Day          int
	PublishDate  time.Time
	SpiderTime   time.Time
	Code         string
	Stock        string
	RowNum       int
	Type         int
}

type Class struct {
	models.Simple
	Name     string
	Level    int
	ParentID int
}

type Category struct {
	models.Simple
	Name          string
	FirstClassID  int
	SecondClassID int
	Link          string
}

type Tick struct {
	StockCode string
	Open      float64
	PreClose  float64
	Close     float64
	High      float64
	TradeDate time.Time
	PctChg    float64
}

type Stock struct {
	models.Simple
	CategoryID   int
	CategoryName string
	Code         string
}

func (Stock) TableName() string {
	return "p_event_stock"
}

func GetInstance(id int) *Data {
	var ret Data
	if err := models.DB().Table("p_event_data").
		Where("id = ?", id).
		First(&ret).Error; err != nil {
		logging.Error("market.GetInstance() query Errors: ", err.Error())
	}
	return &ret
}

func ListClass() ([]Class, error) {
	var ret []Class
	if err := models.DB().Table("p_event_class").
		Where("level > 0").
		Order("level ASC").Find(&ret).Error; err != nil {
		logging.Error("market.ListClass() query Errors: ", err.Error())
		return ret, err
	}
	return ret, nil
}

func QueryClass(id int) (Category, error) {
	var ret Category
	if err := models.DB().Table("p_event_category").
		Where("id = ?", id).
		First(&ret).Error; err != nil {
		logging.Error("market.QueryClass() query Errors: ", err.Error())
		return ret, err
	}
	return ret, nil
}

func QueryCategory(classID int) ([]Category, error) {
	var ret []Category
	if err := models.DB().Table("p_event_category").
		Where("first_class_id = ? OR second_class_id = ?", classID, classID).
		Order("id ASC").Find(&ret).Error; err != nil {
		logging.Error("market.QueryCategory() query Errors: ", err.Error())
		return ret, err
	}
	return ret, nil
}

func QueryData(name string, classID, categoryID int) ([]Data, error) {
	var ret []Data
	db := models.DB().Table("p_event_data").
		Select("p_event_data.*, GROUP_CONCAT(DISTINCT(p_event_stock.code)) AS code, GROUP_CONCAT(DISTINCT(b_stock_names.name)) AS stock, row_number() OVER(PARTITION BY p_event_data.category_id ORDER BY p_event_data.publish_date DESC) AS row_num, p_event_class.name AS class_name, p_event_class.type AS type").
		Joins("LEFT JOIN p_event_category ON p_event_category.id = p_event_data.category_id").
		Joins("LEFT JOIN p_event_stock ON p_event_stock.category_id = p_event_data.category_id").
		Joins("LEFT JOIN b_stock_names ON b_stock_names.code = p_event_stock.code").
		Joins("LEFT JOIN p_event_class ON p_event_class.id = p_event_category.first_class_id").
		Where("p_event_stock.deleted_at IS NULL")
	if name != "" {
		db = db.Where("p_event_data.category_name LIKE ?", "%"+name+"%")
	}
	if classID > 0 {
		db = db.Where("p_event_category.first_class_id = ? OR p_event_category.second_class_id = ?", classID, classID)
	}
	if categoryID > 0 {
		db = db.Where("p_event_data.category_id = ?", categoryID)
	}

	//db = db.Group("p_event_data.category_id")
	db = db.Group("p_event_data.id")

	if err := db.Order("p_event_data.category_id ASC, p_event_data.year DESC, p_event_data.month DESC, p_event_data.week DESC, p_event_data.day DESC").
		Find(&ret).Error; err != nil {
		logging.Error("market.QueryData() query Errors: ", err.Error())
		return ret, err
	}
	return ret, nil
}

func QueryTick(code, publishDate string, day int) ([]Tick, error) {
	var ret []Tick
	var order string
	db := models.Data().Table("p_stock_tick")
	if day > 0 {
		db = db.Where("trade_date >= ?", publishDate)
		order = "stock_code ASC, trade_date ASC"
	} else {
		db = db.Where("trade_date < ?", publishDate)
		order = "stock_code ASC, trade_date DESC"
	}
	if err := db.Where("stock_code = ?", code).
		Where("amount > 0").
		Order(order).
		Limit(util.Abs(day)).
		Find(&ret).Error; err != nil {
		logging.Error("market.QueryTick() query Errors: ", err.Error())
		return ret, err
	}
	return ret, nil
}

func QueryFreq(categoryID int) (string, error) {
	var ret []string
	if err := models.DB().Table("p_event_data").
		Where("category_id = ?", categoryID).
		Limit(1).
		Pluck("frequency", &ret).Error; err != nil {
		logging.Error("market.QueryFreq() query Errors: ", err.Error())
		return "", err
	}
	if len(ret) > 0 {
		return ret[0], nil
	} else {
		return "", nil
	}
}

func QueryStock(categoryID int) ([]Stock, error) {
	var ret []Stock
	if err := models.DB().Table("p_event_stock").
		Where("category_id = ?", categoryID).
		Find(&ret).Error; err != nil {
		return ret, err
	}
	return ret, nil
}

func InsertStock(stock Stock) error {
	if err := models.DB().Table("p_event_stock").
		Save(&stock).Error; err != nil {
		return err
	}
	return nil
}

func UpdateStock(stock Stock) error {
	if err := models.DB().Table("p_event_stock").
		Assign(&stock).FirstOrCreate(&Stock{}).Error; err != nil {
		return err
	}
	return nil
}

func DeleteStock(stock Stock) error {
	if err := models.DB().Table("p_event_stock").
		//Where("category_id = ?", categoryID).
		//Where("code = ?", code).
		Delete(&stock).Error; err != nil {
		return err
	}
	return nil
}

func (md *Data) Set(value float64) error {
	if err := models.DB().Table("p_event_data").
		Where("id = ?", md.ID).
		Update("value", value).Error; err != nil {
		logging.Error("market.Set() update Errors: ", err.Error())
		return err
	}
	return nil
}
