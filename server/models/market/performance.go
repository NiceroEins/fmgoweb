package market

import (
	"bytes"
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"datacenter/pkg/util"
	"fmt"
	"github.com/jinzhu/gorm"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"time"
)

type BrokerPredict struct {
	ID             int
	StockCode      string
	Code           string
	SecName        string
	Organization   string
	LastRatingDate time.Time
	NetProY1a      *int
	NetProYe       *int
	NetProY1e      *int
	NetProY2e      *int
}

func (BrokerPredict) TableName() string {
	return "u_performance_broker_predict"
}

type PerformanceForecast struct {
	models.Simple
	Code                   string
	Name                   string
	ExpectedNetProfitUpper float64
	ExpectedNetProfitLower float64
	PublishDate            time.Time
	ReportPeriod           time.Time
}

type PerformanceAnnouncement struct {
	models.Simple
	Code             string
	Name             string
	NetProfit        float64
	NetProfitQuarter float64
	PublishDate      time.Time
	ReportPeriod     time.Time
}

type PerformanceExpress struct {
	models.Simple
	Code       string
	Name       string
	NetProfit  float64
	NoticeDate time.Time
	ReportDate time.Time
}

func (PerformanceForecast) TableName() string {
	return "u_performance_forecast"
}

type PredictStock struct {
	out bytes.Buffer
}

//func
func Predict() {
	date := "2020-12-31"
	tm, _ := time.ParseInLocation(util.YMD, date, time.Local)
	codes := stockCodes()
	var n int
	for _, code := range codes {
		ps := Init()
		ok := ps.PredictOne(code, tm)
		if ok {
			//fmt.Println(ps.out.String())
			n++
		}
		fmt.Println(ps.out.String())
	}
	fmt.Printf("统计股票总数%d, 可计算数据%d条\n\n", len(codes), n)
}

func db() *gorm.DB {
	return models.Data().LogMode(false)
}

func Init() *PredictStock {
	return &PredictStock{}
}

func (s *PredictStock) PredictOne(code string, t time.Time) bool {
	s.out.WriteString(fmt.Sprintf("------------报告期 %s ----------------\n", t.Format(util.YMD)))
	pp, ok := s.pe(code, t)
	if ok {
		s.out.WriteString(fmt.Sprintf("%s 预测最新PE对应股价 %.2f\n", code, pp))
		return true
	}
	return false
}

func (s *PredictStock) perf(code string, t time.Time) ([]float64, bool) {
	// 2019 2018 2017
	// 03 06 09 12 total count
	arr := make([][]float64, 4)
	avg := make([][]float64, 4)
	ret := make([]float64, 4)
	for i := 0; i < 4; i++ {
		arr[i] = make([]float64, 6)
		avg[i] = make([]float64, 6)
	}

	for i, year := 0, t.Year()-1; i < 3; year-- {
		if year == 2020 {
			continue
		}
		for _, rp := range []string{"03-31", "06-30", "09-30", "12-31"} {
			tn, _ := time.ParseInLocation(util.YMD, fmt.Sprintf("%d-%s", year, rp), time.Local)
			value, _, _, ok := s.queryAnnouncement(code, tn)
			if !ok {
				continue
			}
			if value < 0 {
				continue
			}
			//fmt.Printf("%s %s 公告值 %.2f\n", code, tn.Format(util.YMD), value)
			switch int(tn.Month()) {
			case 3:
				arr[i][0] = value
			case 6:
				arr[i][1] = value
			case 9:
				arr[i][2] = value
			case 12:
				arr[i][3] = value
			default:
				continue
			}
			arr[i][4] += value
			arr[i][5] += 1
		}
		i++
	}

	for j := 0; j < 4; j++ {
		if arr[j][5] != 4 || arr[j][4] < 2 {
			//数据不全或异常
			continue
		}
		// j from 0 to 4
		// representing 4 quarters
		for k := 0; k < 4; k++ {
			if arr[j][k] == 0 {
				continue
			}
			avg[k][j] = arr[j][k] / arr[j][4]
			//for l := 0; l < j; l++ {
			//	if (avg[k][j] < avg[k][l]-0.2 || avg[k][j] > avg[k][l]+0.2) && avg[k][l] != 0 {
			//		fmt.Printf("----------------------------\n%s 第%d季度 当季净利润占比分别为%.2f%% %.2f%% 剔除\n", code, k+1, avg[k][j]*100, avg[k][l]*100)
			//		return ret, false
			//	}
			//}
			avg[k][4] += arr[j][k] / arr[j][4]
			avg[k][5] += 1
		}
	}
	for l := 0; l < 4; l++ {
		if avg[l][5] <= 0 {
			s.out.WriteString(fmt.Sprintf("%s 第%d季度无可用数据（业绩全为负） 跳过\n", code, l+1))
			return ret, false
		}
		ret[l] = avg[l][4] / avg[l][5]
		for m := 0; m < 4; m++ {
			if avg[l][m] == 0 {
				continue
			}
			if avg[l][m] < ret[l]-0.2 || avg[l][m] > ret[l]+0.2 {
				s.out.WriteString(fmt.Sprintf("%s 第%d季度 当季净利润占比%.2f%% 平均值%.2f%% 剔除\n", code, l+1, avg[l][m]*100, ret[l]*100))
				return ret, false
			}
		}
	}
	s.out.WriteString(fmt.Sprintf("%s 四季度业绩占比", code))
	for n := 0; n < 4; n++ {
		s.out.WriteString(fmt.Sprintf(" %.2f%%", ret[n]*100))
	}
	s.out.WriteString(fmt.Sprintf("\n"))
	return ret, true
}

func (s *PredictStock) perfQuarter(code string, t time.Time) (float64, float64, time.Time, bool) {
	v1, s1, pd, b1 := s.perfOne(code, t)
	if !b1 {
		s.out.WriteString(code + " 当季度业绩无数据\n")
		return 0, 0, time.Time{}, false
	}
	if v1 == 0 {
		if t.Month() == 3 {
			return s1, s1, pd, b1
		}
		_, s2, _, b2 := s.queryAnnouncement(code, lastPeriod(t))
		if !b2 {
			s.out.WriteString(code + " 业绩无法计算当季度值\n")
			return 0, 0, time.Time{}, false
		}
		return s1 - s2, s1, pd, true
	}
	return v1, s1, pd, true
}

// 单季度，总
func (s *PredictStock) perfOne(code string, t time.Time) (float64, float64, time.Time, bool) {
	var quarter, total float64
	var pd time.Time
	var ok bool
	total, pd, ok = s.queryForecast(code, t)
	if !ok {
		total, pd, ok = s.queryExpress(code, t)
	}
	if !ok {
		quarter, total, pd, ok = s.queryAnnouncement(code, t)
	}
	return quarter, total, pd, ok
}

func (s *PredictStock) queryForecast(code string, t time.Time) (float64, time.Time, bool) {
	var data []PerformanceForecast
	db().Table("u_performance_forecast").
		Where("code = ?", code).
		Where("report_period = ?", t.Format(util.YMD)).
		Find(&data)
	if len(data) <= 0 {
		return 0, time.Time{}, false
	}
	v := data[0]
	if v.ExpectedNetProfitLower == 0 && v.ExpectedNetProfitUpper == 0 {
		return 0, time.Time{}, false
	}
	if v.ExpectedNetProfitLower <= 0 {
		v.ExpectedNetProfitLower = v.ExpectedNetProfitUpper
	}
	enp := (v.ExpectedNetProfitUpper + v.ExpectedNetProfitLower) / 2
	return enp, v.PublishDate, true
}

func (s *PredictStock) queryAnnouncement(code string, t time.Time) (float64, float64, time.Time, bool) {
	var data []PerformanceAnnouncement
	db().Table("u_performance_announcement").
		Where("code = ?", code).
		Where("report_period = ?", t.Format(util.YMD)).
		Find(&data)
	if len(data) <= 0 {
		return 0, 0, time.Time{}, false
	}
	v := data[0]
	if v.NetProfit == 0 || v.NetProfitQuarter == 0 {
		return 0, 0, time.Time{}, false
	}

	return v.NetProfitQuarter, v.NetProfit, v.PublishDate, true
}

func (s *PredictStock) queryExpress(code string, t time.Time) (float64, time.Time, bool) {
	var data []PerformanceExpress
	db().Table("p_acem_express").
		Where("code = ?", code).
		Where("report_date = ?", t.Format(util.YMD)).
		Find(&data)
	if len(data) <= 0 {
		return 0, time.Time{}, false
	}
	v := data[0]
	if v.NetProfit == 0 {
		return 0, time.Time{}, false
	}
	return v.NetProfit, v.NoticeDate, true
}

func (s *PredictStock) pe(code string, t time.Time) (float64, bool) {
	pq, ps, pd, ok := s.perfQuarter(code, t)
	if !ok {
		return 0, false
	}
	//_, aps, _, ok := queryAnnouncement(code, t)
	//if !ok {
	//	return 0, false
	//}
	s.out.WriteString(fmt.Sprintf("%s 发布日期 %s\n", code, pd.Format(util.YMD)))

	value, rate, ok := s.broker(code, pd, t)
	if !ok {
		return 0, false
	}
	s.out.WriteString(fmt.Sprintf("%s 券商预测值 %.0f, 平均增速 %.2f%%\n", code, value, rate*100))
	arr, ok := s.perf(code, t)
	if !ok {
		return 0, false
	}

	z, ok := s.zgb(code, t)
	if !ok {
		return 0, false
	}
	p, ok := s.price(code, pd)
	if !ok {
		return 0, false
	}

	var ne float64
	switch int(t.Month()) {
	case 3:
		if arr[0] != 0 {
			ne = ps / arr[0]
		}
	case 6:
		if arr[1] != 0 {
			ne = ps + pq/arr[1]*(arr[2]+arr[3])
		}
	case 9:
		if arr[2] != 0 {
			ne = (ps + pq/arr[2]*arr[3]) * (1 + rate)
		}
	case 12:
		ne = ps * (1 + rate)
	}
	//出之前PE
	s.out.WriteString(fmt.Sprintf("%s 总股本%.0f 当前价%.2f \n", code, z, p))
	ppe := p * z / value

	s.out.WriteString(fmt.Sprintf("%s 计算前利润 %.0f 当季利润%.0f 计算后利润 %.0f\n", code, ps, pq, ne))
	//最新PE
	npe := p * z / ne
	s.out.WriteString(fmt.Sprintf("%s 之前PE %.2f， 之后PE %.2f\n", code, ppe, npe))

	pp := p * ppe / npe
	return pp, true
}

func (s *PredictStock) zgb(code string, t time.Time) (float64, bool) {
	data := make(map[string]interface{})
	err := mongo.GetMongoDB("shares", "F10_capitalStructure", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code":       code,
			"constitute": "总股本(股)",
		}).One(&data)
	})
	if err != nil {
		logging.Error("mongo 获取总股本 Errors: ", err.Error())
		s.out.WriteString(code + " 总股本无数据\n")
		return 0, false
	}

	rps := makeReportPeriod(t)
	for _, rp := range rps {
		ok, ret := util.GetNumber(data, rp)
		if ok {
			return ret, true
		}
	}
	return 0, false
}

func (s *PredictStock) broker(code string, tPublish, tPeriod time.Time) (float64, float64, bool) {
	var ret, rate float64
	var ok bool
	ret, rate, ok = s.brokerNew(code, tPublish, tPeriod)
	//if !ok {
	//	ret, rate, ok = s.brokerOld(code, t)
	//}

	//if !ok {
	//	ret = 753000000
	//	rate = 10.1/7.53 - 1
	//	ok = true
	//}
	return ret, rate, ok
}

func (s *PredictStock) brokerOld(code string, t time.Time) (float64, float64, bool) {
	var data []BrokerPredict
	var ret, rate float64
	var n int
	db().Table("u_performance_broker_predict").
		Where("code = ?", code).
		Where("last_rating_date < ?", t.AddDate(0, 0, -1).Format(util.YMD)).
		Where("last_rating_date >= ?", t.AddDate(0, -3, 0).Format(util.YMD)).
		Find(&data)

	for _, v := range data {
		if n >= 5 {
			break
		}
		if v.NetProYe != nil && v.NetProY1e != nil {
			//fmt.Printf("券商预测 %d 万元\n", *v.NetProYe)
			ret += float64(*v.NetProYe)
			if *v.NetProYe != 0 {
				rate += float64(*v.NetProY1e)/float64(*v.NetProYe) - 1
			}
			n++
		}
	}
	if n <= 0 {
		return 0, 0, false
	}
	ret = ret / float64(n) * 10000
	rate = rate / float64(n)

	return ret, rate, true
}

func (s *PredictStock) brokerNew(code string, tPublish, tPeriod time.Time) (float64, float64, bool) {
	var data []map[string]interface{}
	err := mongo.GetMongoDB("shares", "F10_mechanismForecast", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code": code,
			"publish_date": &bson.M{
				"$gt": tPublish.AddDate(0, -3, 0).Format(util.YMD),
				"$lt": tPublish.AddDate(0, 0, -1).Format(util.YMD),
			},
		}).Sort("-publish_date").Limit(20).All(&data)
	})
	if err != nil {
		logging.Error("mongo 券商预测 Errors: ", err.Error())
		s.out.WriteString(code + " 券商无数据\n")
		return 0, 0, false
	}
	if len(data) == 0 {
		s.out.WriteString(code + " 券商无数据\n")
		return 0, 0, false
	}

	var ret, n, rate float64
	var strOutput string
	var valArr, rateArr []float64
	for i := 0; i < len(data); i++ {
		b1, v1 := util.GetNumber(data[i], strconv.Itoa(tPeriod.Year()))
		b2, v2 := util.GetNumber(data[i], strconv.Itoa(tPeriod.Year()+1))
		if b1 && b2 && v1 != 0 {
			valArr = append(valArr, v1)
			rateArr = append(rateArr, v2/v1-1)
		}
	}

	valAvg := avg(valArr[0:util.Min(5, len(valArr))], true)
	for i := 0; i < len(valArr) && i < len(rateArr) && n < 5; i++ {
		pctg := 0.2
		if len(valArr) < 5 {
			pctg = 0.3
		}
		if valArr[i] > valAvg*(1+pctg) || valArr[i] < valAvg*(1-pctg) || valArr[i] == 0 {
			valArr = append(valArr[0:i], valArr[i+1:]...)
			rateArr = append(rateArr[0:i], rateArr[i+1:]...)
			valAvg = avg(valArr[0:util.Min(5, len(valArr))], true)
			i--
			continue
		}
		strOutput += fmt.Sprintf(" %.0f", valArr[i])
		ret += valArr[i]
		rate += rateArr[i]
		n += 1
	}

	if n == 0 {
		s.out.WriteString(code + " 券商无可用数据\n")
		return 0, 0, false
	}

	s.out.WriteString(code + " 券商预测值" + strOutput + "\n")
	ret = ret / n
	rate = rate / n
	return ret, rate, true
}

func (s *PredictStock) price(code string, t time.Time) (float64, bool) {
	var arr []float64
	db().Table("p_stock_tick").
		Where("stock_code = ?", code).
		Where("trade_date < ?", t.Format(util.YMD)).
		Order("trade_date DESC").
		Pluck("close", &arr)
	if len(arr) == 0 {
		s.out.WriteString(code + " 股价无数据\n")
		return 0, false
	}
	return arr[0], true
}

func stockCodes() []string {
	var ret []string
	db().Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Order("code").Pluck("code", &ret)
	return ret
}

func makeReportPeriod(t time.Time) []string {
	var ret []string
	for i := 0; i < 3; i++ {
		year := t.Year() - i
		for month := int(t.Month()); month > 0; month -= 3 {
			var day int
			switch month {
			case 6, 9:
				day = 30
			case 3, 12:
				day = 31
			default:
				continue
			}
			ret = append(ret, fmt.Sprintf("%d-%02d-%02d", year, month, day))
		}
	}
	return ret
}

func lastPeriod(t time.Time) time.Time {
	var year, month, day int
	year = t.Year()
	month = int(t.Month() - 3)
	if month <= 0 {
		year--
		month = 12
	}
	switch month {
	case 3, 12:
		day = 31
	case 6, 9:
		day = 30
	}
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
}

func avg(data []float64, escapeZero bool) float64 {
	if len(data) == 0 {
		return 0
	}
	var ret, n float64
	for _, v := range data {
		if escapeZero && v == 0 {
			continue
		}
		ret += v
		n += 1
	}
	return ret / n
}
