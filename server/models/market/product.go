package market

import (
	"datacenter/models"
	"datacenter/models/common"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"errors"
	"fmt"
	"strings"
	"time"
)

const MINUTETP = 1 //查询当日中午开盘价 适用于百川app等
const TICKTP = 2   //查询次日开盘价  适用于bdi等

type ProductValueList struct {
	PriceDate string  `json:"price_date" form:"price_date"`
	Price     float64 `json:"price" form:"price"`
}
type ProductStock struct {
	Code string `form:"code"`
	Name string `form:"name"`
}
type Rate struct {
	Current int     `json:"current"`
	Total   int     `json:"total"`
	Rate    float64 `json:"rate"`
}

func GetCategoryById(cid int) (Category, error) {
	var cate Category
	err := models.DB().Table("p_event_category").
		Where("id = ?", cid).First(&cate).Error
	return cate, err
}

func GetProValueList(name string) []ProductValueList {
	var valueList []ProductValueList
	err := models.DB().Table("p_product_data").Select("price_date,price").
		Where("product_name=?", name).Order("price_date asc").Find(&valueList).Error
	if err != nil {
		return nil
	}
	return valueList
}

func GetBdiValueList() []ProductValueList {
	var valueList []ProductValueList
	err := models.DB().Table("p_bdi_data").Select("date as price_date,value as price").
		Order("date asc").Find(&valueList).Error
	if err != nil {
		return nil
	}
	return valueList

}

func GetCategoryDateList(cid int) ([]string, error) {
	//获取时间所有日期
	var dateList []string
	err := models.DB().Table("p_event_data").Select("publish_date").
		Where("category_id=?", cid).Order("publish_date desc").Pluck("publish_date", &dateList).Error
	return dateList, err
}

func GetCategoryStockList(cid int) ([]ProductStock, error) {
	//获取事件对应所有股票
	var stockList []ProductStock
	dbs := models.DB().Table("p_event_stock").Where("category_id=?", cid).Where("deleted_at is null")
	err := dbs.Joins("left join b_stock on p_event_stock.code=b_stock.id").
		Select("b_stock.id as code,b_stock.name as name").Find(&stockList).Error
	return stockList, err
}

func GetStockBlok(code string, date string) float64 {
	var blockList []float64
	err := models.Data().Table("p_stock_tick").Select("blok").Where("stock_code=?", code).
		Where("trade_date=?", date).Pluck("block", &blockList).Error
	if err != nil || len(blockList) != 1 {
		return 0
	}
	return blockList[0]
}

func GetProductStockPrice(code string, date string, tp int) (float64, error, string) {
	var priceList []float64
	var isTradeDate int
	//判断当天是否为交易日
	models.DB().Table("b_trade_date").Where("trade_date = ?", date).Count(&isTradeDate)
	if tp == TICKTP || isTradeDate == 0 {
		//bdi 需要次日开盘买入
		trade, _ := common.GetTradeDateList(date, 1, false)
		if trade == nil {
			logging.Error("select trade_date error")
			return 0, errors.New("trade_date not found"), date
		}
		tradeDate := trade[0].Format(util.YMD)
		price, err := GetStockPriceCompare(code, 1, date, "open")
		return price, err, tradeDate
	}

	err := models.Data().Table("p_stock_minute").Where("code=?", code).
		Where("time=?", date+" 13:00:00").Select("open").Pluck("open", &priceList).Error
	if err != nil || len(priceList) < 1 {
		return 0, err, date
	}

	return priceList[0], err, date
}

func GetStockPriceCompare(code string, days int, startDate string, st string) (float64, error) {
	//查询交易日
	var priceList []float64
	trade, _ := common.GetTradeDateList(startDate, days, false)
	if trade == nil {
		logging.Error("select trade_date error")
		return 0, errors.New("trade_date not found")
	}
	tradeDate := trade[len(trade)-1].Format(util.YMD)
	//查询价格
	err := models.Data().Table("p_stock_tick").Where("stock_code=?", code).
		Where("trade_date=?", tradeDate).Pluck(st, &priceList).Error
	if err != nil || len(priceList) < 1 {
		return 0, errors.New("price not found")
	}
	return priceList[0], nil

}

func GetStockRate(dateList []string, code string, days int, st string, tp int) string {
	var successNum = 0
	var totalNum = 0
	for _, de := range dateList {
		de := strings.Split(de, "T")[0]
		//排除当天
		if de == time.Now().Format(util.YMD) || de == time.Now().AddDate(0, 0, -1).Format(util.YMD) {
			continue
		}
		//查询当天下午价格
		buyPrice, err, de := GetProductStockPrice(code, de, tp)
		if buyPrice == 0 || err != nil {
			continue
		}
		//判断是否为涨停价
		block := GetStockBlok(code, de)
		if block == 0 || buyPrice == block {
			continue
		}

		sellPrice, err := GetStockPriceCompare(code, days, de, st)
		if sellPrice == 0 || err != nil {
			continue
		}

		if sellPrice > buyPrice {
			successNum += 1
		}
		totalNum += 1
	}
	if totalNum == 0 {
		return ""
	}
	return fmt.Sprintf("%.2f", float64(successNum)/float64(totalNum))
}

func GetProductRateById(cid int, days int, slot string, tp int) *Rate {
	rate := Rate{
		Current: 0,
		Total:   0,
	}
	//查询所有历史
	dateList, err := GetCategoryDateList(cid)
	if err != nil {
		return nil
	}
	stockList, err := GetCategoryStockList(cid)
	if err != nil || len(stockList) == 0 {
		return nil
	}
	trade, _ := common.GetTradeDateList(time.Now().Format(util.YMD), -days, false)
	if trade == nil {
		return nil
	}
	for _, date := range dateList {
		//获取当前买的价格
		//linTotal := 0
		date = strings.Split(date, "T")[0]
		//获取前交易日
		if date == time.Now().Format(util.YMD) || (date == trade[len(trade)-1].Format(util.YMD)) && time.Now().Hour() < 18 {
			continue
		}
		var linRate float64 = 0
		for _, c := range stockList {
			if find := strings.Contains(c.Name, "ST"); find {
				continue
			}
			buyPrice, err, de := GetProductStockPrice(c.Code, date, tp)
			if buyPrice == 0 || err != nil {
				continue
			}
			sellPrice, err := GetStockPriceCompare(c.Code, days, de, slot)
			if sellPrice == 0 || err != nil {
				continue
			}
			linRate += (sellPrice - buyPrice) / buyPrice
		}
		if linRate > 0 {
			rate.Current += 1
		}
		rate.Total += 1
	}
	if rate.Total == 0 {
		return nil
	}
	rate.Rate = float64(rate.Current) / float64(rate.Total)
	return &rate
}
