package nlp

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"math"
	"strconv"
	"sync"
	"sync/atomic"
)

// 判定相似的最短欧式距离
var MinDist float64 = 0.5

// 最大保留检测数
var MaxVectorSize = 40000

// id, []float64
var VectorMap sync.Map
var onProgress int64 = 0

type Duplication struct {
	Code int `json:"code"`
	Data []struct {
		DuplicatedID         string  `json:"duplicated_id"`
		Pred                 int     `json:"pred"`
		Prob                 float64 `json:"prob"`
		TextNlpID            string  `json:"text_nlp_id"`
		DuplicateInHalfHours bool    `json:"duplicate_in_half_hours"`
	} `json:"data"`
	Msg string `json:"msg"`
	//Text string  `json:"text"`
	Time float64 `json:"time,omitempty"`
}

func ParseDuplication(data string) (*Duplication, error) {
	var ret Duplication
	err := json.Unmarshal([]byte(data), &ret)
	if err != nil {
		return nil, err
	}
	return &ret, err
}

// map[id][]float64
// return id
func FindDuplicationID(id int, src []float64) (int, float64, error) {
	//data, err := gredis.HGetAllStringMap("news:vector:all")
	//if err != nil {
	//	return 0, 0, err
	//}
	//for k, v := range data {
	//	var vector []float64
	//	if e := json.Unmarshal([]byte(v), &vector); e != nil {
	//		logging.Error("duplication.FindDuplicationID() Invalid vectors: ", err.Error())
	//		return 0, 0, e
	//	}
	//	if dist, e := distance(src, vector); e != nil {
	//		return 0, 0, e
	//	} else {
	//		// dist
	//		if dist >= MinDist {
	//			return util.Atoi(k), dist, nil
	//		}
	//	}
	//}

	checkLocalVector()
	var (
		dupID int
		ret   float64
		err   error
	)
	VectorMap.Range(func(k, v interface{}) bool {
		value, ok := v.([]float64)
		if !ok {
			logging.Error("duplication.FindDuplicationID() Invalid v")
			return false
		}
		if dist, e := distance(src, value); e != nil {
			err = e
			logging.Error("duplication.FindDuplicationID() calc distance Errors: ", e.Error())
			return false
		} else {
			idx, _ := k.(int)
			if idx > 0 && idx < id-MaxVectorSize {
				VectorMap.Delete(k)
				removeDuplication(idx)
			}
			// dist
			if dist >= MinDist {
				dupID = idx
				ret = dist
				return false
			}
		}
		return true
	})

	return dupID, ret, err
}

func SetDuplication(id int, vector []float64) error {
	VectorMap.Store(id, vector)
	data, _ := json.Marshal(vector)
	return gredis.HSet("news:vector:all", strconv.Itoa(id), data)
}

func UpdateDuplication(id int, dupID, dup string, duration int64) error {
	err := models.DB().Table("u_feed").Where("id = ?", id).Updates(map[string]interface{}{
		"duplication_id": dupID,
		"duplication":    dup,
		"dup_duration":   duration,
	}).Error
	return err
}

func checkLocalVector() int {
	bEmpty := true
	VectorMap.Range(func(k, v interface{}) bool {
		bEmpty = false
		return false
	})
	if bEmpty {
		// empty VectorMap
		if maxID, err := fetchDuplication(); err != nil {
			logging.Error("duplication.checkLocalVector() Errors: ", err.Error())
		} else {
			return maxID
		}
	}
	return 0
}

// 0717: check max(id) in map
func fetchDuplication() (int, error) {
	if !atomic.CompareAndSwapInt64(&onProgress, 0, 1) {
		return 0, nil
	}
	defer atomic.StoreInt64(&onProgress, 0)
	data, err := gredis.HGetAllStringMap("news:vector:all")
	if err != nil {
		return 0, err
	}
	maxID := 0
	for k, v := range data {
		var vector []float64
		if e := json.Unmarshal([]byte(v), &vector); e != nil {
			logging.Error("duplication.fetchDuplication() Invalid vectors: ", e.Error())
			return 0, e
		}
		idx := util.Atoi(k)
		if maxID < idx {
			maxID = idx
		}
		VectorMap.Store(idx, vector)
	}
	return maxID, nil
}

func removeDuplication(id int) {
	go gredis.HDelField("news:vector:all", strconv.Itoa(id))
}

func distance(p1 []float64, p2 []float64) (float64, error) {
	if len(p1) != len(p2) {
		return 0, fmt.Errorf("duplication.distance() unequally array length")
	}
	var sum float64 = 0
	for k, v := range p1 {
		sum += (v - p2[k]) * (v - p2[k])
	}
	return 1.0 / (1.0 + math.Sqrt(sum)), nil
}
