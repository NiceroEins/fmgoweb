package nlp

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"encoding/json"
)

type Correlation struct {
	Code int `json:"code"`
	Data []struct {
		ClassId          string `json:"class_id"`
		ClassName        string `json:"class_name"`
		Probabilities    string `json:"probabilities,omitempty"`
		ProbabilitiesAll string `json:"probabilities_all,omitempty"`
	} `json:"data"`
	Msg  string  `json:"msg"`
	Text string  `json:"text"`
	Time float64 `json:"time,omitempty"`
}

func ParseCorrelation(data string) (*Correlation, error) {
	var ret Correlation
	err := json.Unmarshal([]byte(data), &ret)
	if err != nil {
		return nil, err
	}
	return &ret, err
}

func CheckCorrelation(correlation *Correlation) bool {
	if correlation == nil {
		return false
	}
	ret := false
	for _, v := range correlation.Data {
		if v.ClassId == "0" {
			ret = true
			break
		}
	}
	return ret
}

func UpdateCorrelation(id, cor int, duration int64) error {
	err := models.DB().Table("u_feed").Where("id = ?", id).Updates(map[string]interface{}{
		"correlation": cor,
		"duration":    duration,
	}).Error
	return err
}

type QACorrelation struct {
	Code int `json:"code"`
	Data []struct {
		Q           string `json:"q"`
		A           string `json:"a,omitempty"`
		AProcessed  string `json:"a_processed,omitempty"`
		QType       string `json:"q_type,omitempty"`
		TypeKeyWord string `json:"type_key_word,omitempty"`
		Pred        string `json:"pred"`
	} `json:"data"`
	Msg  string  `json:"msg"`
	Time float64 `json:"time,omitempty"`
}

func ParseQACorrelation(data []byte) (*QACorrelation, error) {
	var ret QACorrelation
	err := json.Unmarshal(data, &ret)
	if err != nil {
		logging.Error("nlp.ParseQACorrelation() Errors: ", string(data))
		return nil, err
	}
	return &ret, err
}

func CheckQACorrelation(correlation *QACorrelation) bool {
	if correlation == nil {
		return false
	}
	ret := false
	for _, v := range correlation.Data {
		if v.Pred == "有关" {
			ret = true
			break
		}
	}
	return ret
}
