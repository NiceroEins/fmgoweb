package nlp

import "encoding/json"

type Keywords struct {
	Code int `json:"code"`
	Data []struct {
		ModelNer []string  `json:"model_ner"`
	} `json:"data"`
	Msg string `json:"msg"`
	//Text string  `json:"text"`
	Time float64 `json:"time,omitempty"`
}

func ParseKeywords(data string) (*Keywords, error) {
	var ret Keywords
	err := json.Unmarshal([]byte(data), &ret)
	if err != nil {
		return nil, err
	}
	return &ret, err
}
