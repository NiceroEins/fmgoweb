package nlp

import (
	"datacenter/models"
	"encoding/json"
)

type Sentiment struct {
	Code int `json:"code"`
	Data []struct {
		ClassID   string `json:"class_id"`
		ClassName string `json:"class_name"`
		Prob      string `json:"prob"`
	} `json:"data,omitempty"`
	Msg  string  `json:"msg"`
	Time float64 `json:"time,omitempty"`
}

func ParseSentiment(data string) (*Sentiment, error) {
	var ret Sentiment
	err := json.Unmarshal([]byte(data), &ret)
	if err != nil {
		return nil, err
	}
	return &ret, err
}

func CheckSentiment(ne *Sentiment) string {
	if ne == nil || len(ne.Data) == 0 {
		return ""
	}
	if len(ne.Data) == 0 {
		return ""
	}
	//bt, err := json.Marshal(ne.Data[0])
	//if err == nil {
	//	return string(bt)
	//} else {
	//	return ""
	//}
	return ne.Data[0].ClassName
}

func UpdateSentiment(id int, sentiment string, duration int64) error {
	val := 0
	switch sentiment {
	case "利好":
		val = 1
	case "利空":
		val = 2
	case "未知":
		val = 3
	}
	err := models.DB().Table("u_feed").Where("id = ?", id).Updates(map[string]interface{}{
		"sentiment_analysis": val,
		//"sentiment_duration": duration,
	}).Error
	return err
}
