package nlp

import (
	"datacenter/models"
	"encoding/json"
)

type NamedEntity struct {
	Code int `json:"code"`
	Data []struct {
		ConceptName     []string `json:"concept_name"`
		NormalKws       []string `json:"normal_kws"`
		StockNames      []string `json:"stock_names"`
		NoRelationStock bool     `json:"no_relation_stock"`
		ConceptKeyWords []string `json:"concept_key_words"`
	} `json:"data,omitempty"`
	Msg  string  `json:"msg"`
	Time float64 `json:"time,omitempty"`
}

func ParseNamedEntity(data string) (*NamedEntity, error) {
	var ret NamedEntity
	err := json.Unmarshal([]byte(data), &ret)
	if err != nil {
		return nil, err
	}
	return &ret, err
}

func CheckNamedEntity(ne *NamedEntity) string {
	if ne == nil || len(ne.Data) == 0 {
		return ""
	}
	if len(ne.Data) == 0 {
		return ""
	}
	bt, err := json.Marshal(ne.Data[0])
	if err == nil {
		return string(bt)
	} else {
		return ""
	}
}

func UpdateNamedEntity(id int, keyword string, duration int64) error {
	err := models.DB().Table("u_feed").Where("id = ?", id).Updates(map[string]interface{}{
		"named_entity":    keyword,
		"entity_duration": duration,
	}).Error
	return err
}
