package performance

import (
	"datacenter/models"
	"datacenter/models/template"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"encoding/json"
	"fmt"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"time"
)

type ImgDTO struct {
	Url  string `json:"url"`
	Type string `json:"type"`
}

const (
	ANNOUNCEMENT = "公告"
	FORECAST     = "预告"
	EXPRESS      = "快报"
)

const (
	FROM_RECOMMEND = "recommend"
	FROM_TRACE     = "trace"
	FROM_BROKER    = "borker"
)

type typeStruct struct {
	Type  string `json:"type"`
	Table string `json:"table"`
}

var TypeMap = map[string]typeStruct{
	ANNOUNCEMENT: {
		Type:  "announcement",
		Table: "u_performance_announcement",
	},
	FORECAST: {
		Type:  "forecast",
		Table: "u_performance_forecast",
	},
	EXPRESS: {
		Type:  "express",
		Table: "p_acem_express",
	},
}

type TraceIO struct {
	Id           int                 `json:"id"`
	Code         string              `json:"code"`
	Name         string              `json:"name"`
	ObjId        int                 `json:"obj_id"`
	From         string              `json:"from"`
	Type         string              `json:"type"`
	Reason       string              `json:"reason"`
	Price        decimal.NullDecimal `json:"price"`
	IndexPrice   decimal.NullDecimal `json:"index_price"`
	ReportPeriod string              `json:"report_period"`
}

type PushTraceDTO struct {
	Id         int                 `json:"id"`
	Code       string              `json:"code"`
	Name       string              `json:"name"`
	ObjId      int                 `json:"obj_id"`
	From       string              `json:"from"`
	Type       string              `json:"type"`
	Reason     string              `json:"reason"`
	Price      decimal.NullDecimal `json:"price"`
	IndexPrice decimal.NullDecimal `json:"index_price"`
	CreatedAt  *time.Time          `json:"created_at"`
	UpdatedAt  *time.Time          `json:"updated_at"`
}

type TraceDTO struct {
	PushTraceDTO
	EventID       int64               `json:"event_id"`
	EventStr      string              `json:"event_str"`
	PictureLink   string              `json:"picture_link"`
	Lowrate       decimal.NullDecimal //同比下限 可能为空
	Uprate        decimal.NullDecimal //同比增长
	Increase      decimal.NullDecimal
	IndexIncrease decimal.NullDecimal
}

type TraceVO struct {
	Id            int                 `json:"id"`
	Code          string              `json:"code"`
	Name          string              `json:"name"`
	ObjId         int                 `json:"obj_id"`
	Type          string              `json:"type"`
	ObjType       string              `json:"obj_type"`
	Reason        string              `json:"reason"`
	EventId       int64               `json:"event_id"`
	EventStr      string              `json:"event_str"` // 点评内容
	Lowrate       decimal.NullDecimal `json:"lowrate"`   //同比下限 可能为空
	Uprate        decimal.NullDecimal `json:"uprate"`    //同比增长
	Increase      decimal.NullDecimal `json:"increase"`
	IndexIncrease decimal.NullDecimal `json:"index_increase"`
	PictureLink   []ImgDTO            `json:"picture_link"`
	CreatedAt     string              `json:"created_at"`
}

func (dto TraceDTO) DTO2VO() crud_service.VO {
	vo := TraceVO{
		Id:            dto.Id,
		Code:          dto.Code,
		Name:          dto.Name,
		ObjId:         dto.ObjId,
		Type:          "push_trace",
		ObjType:       dto.Type + "_" + dto.From,
		Reason:        dto.Reason,
		EventId:       dto.EventID,
		EventStr:      dto.EventStr,
		Lowrate:       dto.Lowrate,
		Uprate:        dto.Uprate,
		Increase:      dto.Increase,
		IndexIncrease: dto.IndexIncrease,
		CreatedAt:     util.TimePtr2String(dto.CreatedAt, util.YMD),
	}
	vo.PictureLink = []ImgDTO{}
	_ = json.Unmarshal([]byte(dto.PictureLink), &vo.PictureLink)
	return vo
}

type TraceQO struct {
	Code         string `json:"code" form:"code"`
	ReportPeriod string `json:"report_period" form:"report_period"`
	Reason       string `json:"reason" form:"reason"`
	Start        string `json:"start" form:"start"`
	End          string `json:"end" form:"end"`
	SortKey      int    `json:"sort_key" form:"sort_key"`
	Direction    bool   `json:"direction" form:"direction"`
	PageNum      int    `json:"page_num" form:"page_num" binding:"gt=0"`
	PageSize     int    `json:"page_size" form:"page_size" binding:"gt=0"`
}

func GetPerformanceTrace(qo *TraceQO) ([]crud_service.DTO, int, error) {
	sqla := "select p_push_trace.*,uue.id event_id,uue.user_id,uue.event_str,uue.picture_link,p_push_trace.report_period,null lowrate,round(upa.net_profit_growth_rate,2) uprate from p_push_trace" +
		" left join u_performance_announcement upa on p_push_trace.obj_id = upa.id " +
		" left join u_user_event uue on p_push_trace.id = uue.object_id and uue.object_type='push_trace'" +
		" where p_push_trace.type='announcement' "
	sqlf := "select p_push_trace.*,uue.id event_id,uue.user_id,uue.event_str,uue.picture_link,p_push_trace.report_period,round(upf.performance_incr_lower,2) lowrate,round(upf.performance_incr_upper,2) uprate FROM p_push_trace" +
		" left join u_performance_forecast upf on p_push_trace.obj_id = upf.id" +
		" left join u_user_event uue on p_push_trace.id = uue.object_id and uue.object_type='push_trace'" +
		" where p_push_trace.type='forecast'"
	otherSql := "select p_push_trace.*,uue.id event_id,uue.user_id,uue.event_str,uue.picture_link,p_push_trace.report_period,null lowrate,null uprate FROM p_push_trace" +
		" left join u_user_event uue on p_push_trace.id = uue.object_id and uue.object_type='push_trace'" +
		" where (p_push_trace.type!='forecast' and p_push_trace.type!='announcement')"
	dtos := make([]TraceDTO, 0)
	ret := make([]crud_service.DTO, 0)
	sortArray := []string{
		"",
		"Increase",
		"IndexIncrease",
		"CreatedAt",
	}
	if qo.Code != "" {
		sqla = sqla + " and (p_push_trace.code like '%" + qo.Code + "%' or p_push_trace.name like '%" + qo.Code + "%')"
		sqlf = sqlf + " and (p_push_trace.code like '%" + qo.Code + "%' or p_push_trace.name like '%" + qo.Code + "%')"
		otherSql = otherSql + " and (p_push_trace.code like '%" + qo.Code + "%' or p_push_trace.name like '%" + qo.Code + "%')"
	}
	if qo.ReportPeriod != "" {
		sqla = sqla + fmt.Sprintf(" and p_push_trace.report_period='%s'", qo.ReportPeriod)
		sqlf = sqlf + fmt.Sprintf(" and p_push_trace.report_period='%s'", qo.ReportPeriod)
		otherSql = otherSql + fmt.Sprintf(" and p_push_trace.report_period='%s'", qo.ReportPeriod)
	}
	if qo.Reason != "" {
		sqla = sqla + " and reason like '%" + qo.Reason + "%'"
		sqlf = sqlf + " and reason like '%" + qo.Reason + "%'"
		otherSql = otherSql + " and reason like '%" + qo.Reason + "%'"
	}

	if qo.Start != "" && qo.End != "" {
		sqla = sqla + fmt.Sprintf(" and p_push_trace.created_at>='%s 00:00:00' and p_push_trace.created_at<='%s 23:59:59'", qo.Start, qo.End)
		sqlf = sqlf + fmt.Sprintf(" and p_push_trace.created_at>='%s 00:00:00' and p_push_trace.created_at<='%s 23:59:59'", qo.Start, qo.End)
		otherSql = otherSql + fmt.Sprintf(" and p_push_trace.created_at>='%s 00:00:00' and p_push_trace.created_at<='%s 23:59:59'", qo.Start, qo.End)
	}

	sql := sqla + " UNION ALL " + sqlf + " UNION ALL " + otherSql
	err := models.DB().Raw(sql).Find(&dtos).Error
	if err != nil {
		return nil, 0, err
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	stockMap, err := conn.HGetAllStringMap("stock:tick")
	if err != nil {
		logging.Error("redis 获取tick价格失败")
	}
	indexMap := map[string]string{}
	indexMap["SH000001"], err = conn.HGetString("stock:ts_tick", "SH000001")
	if err != nil {
		logging.Error("ts_tick 上证指数获取失败")
	}
	indexMap["SZ399001"], err = conn.HGetString("stock:ts_tick", "SZ399001")
	if err != nil {
		logging.Error("ts_tick 深证指数获取失败")
	}
	indexMap["SZ399006"], err = conn.HGetString("stock:ts_tick", "SZ399006")
	if err != nil {
		logging.Error("ts_tick 创业板指数获取失败")
	}
	indexMap["SZ399005"], err = conn.HGetString("stock:ts_tick", "SZ399005")
	if err != nil {
		logging.Error("ts_tick 中小板指数获取失败")
	}
	indexMap["SH000688"], err = conn.HGetString("stock:ts_tick", "SH000688")
	if err != nil {
		logging.Error("ts_tick 科创板指数获取失败")
	}
	for i, dto := range dtos {
		nowPrice := decimal.NullDecimal{}
		nowIndex := decimal.NullDecimal{}
		if price, ok := stockMap[dto.Code]; ok {
			err := nowPrice.Decimal.Scan(price)
			if err == nil {
				nowPrice.Valid = true
			}
		}
		dtos[i].Increase = util.CalculateRate(dto.Price, nowPrice)
		dtos[i].Increase.Decimal = dtos[i].Increase.Decimal.Round(2)
		if indexPrice, ok := indexMap[util.GetIndexFromCode(dto.Code, true)]; ok {
			err := nowIndex.Decimal.Scan(indexPrice)
			if err == nil {
				nowIndex.Valid = true
			}
		}
		dtos[i].IndexIncrease = util.CalculateRate(dto.IndexPrice, nowIndex)
		dtos[i].IndexIncrease.Decimal = dtos[i].IndexIncrease.Decimal.Round(2)
		//ret = append(ret, dto)
	}
	if qo.SortKey > 0 && qo.SortKey < len(sortArray) {
		sort.Slice(dtos, func(i, j int) bool {
			return qo.Direction == util.Compare(reflect.ValueOf(dtos[i]).FieldByName(sortArray[qo.SortKey]), reflect.ValueOf(dtos[j]).FieldByName(sortArray[qo.SortKey]))
		})
	}
	for _, dto := range dtos {
		ret = append(ret, dto)
	}
	return ret[util.Min((qo.PageNum-1)*qo.PageSize, len(ret)):util.Min(qo.PageNum*qo.PageSize, len(ret))], len(ret), nil
}

type TraceContent struct {
	Code     string  `json:"code"`
	Name     string  `json:"name"`
	Reason   string  `json:"reason"`
	PfType   string  `json:"pf_type"`
	From     string  `json:"from"`
	NowPrice float32 `json:"now_price"`
}

func (tc *TraceContent) SetPerformanceTrace(reportPeriod string, withHighLight bool) bool {
	logging.Info("业绩跟踪数据录入")
	if tc == nil {
		logging.Error("业绩跟踪推送数据为空")
		return false
	}
	type objId struct {
		Id int
	}
	oid := objId{}
	if tc.Code == "" || tc.Name == "" || tc.Reason == "" {
		logging.Info("未指定个股传入")
		return false
	}
	io := TraceIO{
		Code:         tc.Code,
		Name:         tc.Name,
		Reason:       tc.Reason,
		From:         tc.From,
		ReportPeriod: reportPeriod,
	}
	dbs := models.DB()
	switch tc.PfType {
	case ANNOUNCEMENT:
		io.Type = TypeMap[ANNOUNCEMENT].Type
		dbs = dbs.Table(TypeMap[ANNOUNCEMENT].Table).Where("code=?", tc.Code).Where("report_period=?", reportPeriod)
		break
	case FORECAST:
		io.Type = TypeMap[FORECAST].Type
		dbs = dbs.Table(TypeMap[FORECAST].Table).Where("code=?", tc.Code).Where("publish_date is not NULL").Where("report_period=?", reportPeriod)
		break
	case EXPRESS:
		io.Type = TypeMap[EXPRESS].Type
		dbs = dbs.Table(TypeMap[EXPRESS].Table).Where("code=?", tc.Code).Where("report_date=?", reportPeriod)
	default:
		logging.Error("SetPerformanceTrace 输入非法类型：", tc.PfType)
		return false
	}
	err := dbs.First(&oid).Error
	if err != nil {
		logging.Error("业绩跟踪获取对应"+tc.PfType+"失败 code:"+tc.Code+" 报告期："+reportPeriod, err.Error())
	} else {
		io.ObjId = oid.Id
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	if tc.NowPrice > 0 {
		_ = io.Price.Scan(tc.NowPrice)
	} else {
		price, err := conn.HGetString("stock:tick", tc.Code)
		if err != nil {
			logging.Error("业绩跟踪获取"+tc.Code+"当前价格报错：", err.Error())
		} else {
			err := io.Price.Decimal.Scan(price)
			if err == nil {
				io.Price.Valid = true
			} else {
				logging.Error("解析"+tc.Code+"当前价"+price+"报错", err.Error())
			}
		}
	}
	indexCode := util.GetIndexFromCode(tc.Code, true)
	indexPrice, err := conn.HGetString("stock:ts_tick", indexCode)
	if err != nil {
		logging.Error("业绩跟踪获取"+tc.Code+"当前对应指数价格报错：", err.Error())
	} else {
		err := io.IndexPrice.Decimal.Scan(indexPrice)
		if err == nil {
			io.IndexPrice.Valid = true
		}
	}
	oid.Id = 0 //gorm id非空会自动加where条件 所以清空
	limit:=map[string]interface{}{
		"code":          io.Code,
		"type":          io.Type,
		"reason":        io.Reason,
		"obj_id":        io.ObjId,
		"report_period": reportPeriod,
	}
	switch tc.From {
	case FROM_BROKER:
		limit = map[string]interface{}{
			"code":          io.Code,
			"report_period": reportPeriod,
		}
		break
	case FROM_RECOMMEND:
		limit = map[string]interface{}{
			"code":          io.Code,
			"report_period": reportPeriod,
		}
	default:
		break
	}
	err = models.DB().Table("p_push_trace").Where(limit).First(&oid).Error
	if oid.Id <= 0 || err != nil {
		err = models.DB().Table("p_push_trace").Create(&io).Error
		if err != nil {
			logging.Error("业绩跟踪 插入p_push_trace失败", err.Error())
			return false
		}
		if withHighLight {
			highLight := make([]string, 0)
			err := models.DB().Table("u_performance_edit").Where("code=?", io.Code).Where("report_period=?", reportPeriod).Pluck("high_lights", &highLight).Error
			if err != nil {
				logging.Error("获取"+io.Code+" "+reportPeriod+"亮点报错", err.Error())
			} else {
				if len(highLight) > 0 {
					userEvent := template.UserEvent{
						ObjectID:   io.Id,
						EventStr:   highLight[0],
						ObjectType: "push_trace",
					}
					err = template.CreateUserEvent(&userEvent)
					if err != nil {
						logging.Error("复制"+io.Code+" "+reportPeriod+"亮点报错", err.Error())
					}
				}
			}
		}
		return true
	}
	return false
}
