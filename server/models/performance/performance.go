package performance

import (
    "datacenter/models"
    "datacenter/models/template"
    "datacenter/pkg/logging"
    "datacenter/pkg/parser"
    "github.com/shopspring/decimal"
    "strconv"
    "time"
)

type PerformanceDTO [5]*PerformanceSigle
//  股票信息 披露时间 个股追踪 本期预告 预告时间 本期公告 净利润 净利润同比 行业 姓名
type PerformanceSigle struct {
    Id                         int64              `gorm:"column:id"`               //公告表主键id
    Name                       string             `gorm:"column:name"`                       //股票名称
    Code                       string             `gorm:"column:code"`                       //股票代码
    IndividualShareTrack       string             `gorm:"column:individual_track"`     //个股跟踪
    PerformanceIncrUpper       string             `gorm:"column:performance_incr_upper"`     //本期预告(对应预告表里面的业绩变动幅度上限)
    PerformanceIncrLower       string             `gorm:"column:performance_incr_lower"`     //本期预告(对应预告表里面的业绩变动幅度下限)
    Realname                   string             `gorm:"column:realname"`                  //用户姓名
    DisclosureTimeHistory      *time.Time         `gorm:"column:disclosure_time_history"`    //披露时间历史
    DisclosureChangeAt         *time.Time         `gorm:"column:disclosure_change_at"`       //披露时间更改时间
    ExpectedPublishDate        *time.Time         `gorm:"column:disclosure_time"`            //披露时间
    ForecastPublishDate        *time.Time         `gorm:"column:f_publish_date"`            //预告时间(对应预告表里面的公告日期)
    AnnouncementPublishDate    *time.Time         `gorm:"column:a_publish_date"`          //公告时间(对应公告表里面的公告日期)
    ReportPeripod              *time.Time         `gorm:"column:report_period"`         // 报告期
    NetProfitQuarter           *decimal.Decimal   `gorm:"column:net_profit_quarter"`    //本期公告(净利润（单季度）)
    NetProfitGrowthRate        *decimal.Decimal   `gorm:"column:net_profit_growth_rate"`  // 净利润同比增长
    NetProfitGrowthRateQuarter *decimal.Decimal   `gorm:"column:net_profit_growth_rate_quarter"` // 净利润同比增长（单季度）
    NetProfitComparativeGrowth *decimal.Decimal   `gorm:"column:net_profit_comparative_growth"`  // 净利润环比增长
    BusinessRevenue            *decimal.Decimal   `gorm:"column:business_revenue"`    //营业收入
    BusinessRevenueQuarter     *decimal.Decimal   `gorm:"column:business_revenue_quarter"`    //营业收入（单季度）
    BusinessRevenueGrowthRate  *decimal.Decimal   `gorm:"column:business_revenue_growth_rate"`    //营业收入同比增长(%)
    NetProfitAfterDed          *decimal.Decimal   `gorm:"column:net_profit_after_ded"`    //扣非净利润（年度）
    NetProfitAfterDedQuarter   *decimal.Decimal   `gorm:"column:net_profit_after_ded_quarter"`    //扣非净利润（季度）
    NetProfitAfterDedGrowthRate *decimal.Decimal  `gorm:"column:net_profit_after_ded_growth_rate"`    //扣非净利润同比增长率(%)
    CashFlowPerShare           *decimal.Decimal   `gorm:"column:cash_flow_per_share"`    //每股经营现金流量
    CashFlowPerShareQuarter    *decimal.Decimal   `gorm:"column:cash_flow_per_share_quarter"`    //每股经营现金流量
    NetProfitYear              *decimal.Decimal   `gorm:"column:net_profit"`    //本期公告(年度净利润)
    GrossProfitMargin          *decimal.Decimal  `gorm:"column:gross_profit_margin"`    //总销售毛利率
    GrossProfitMarginQuarter   *decimal.Decimal  `gorm:"column:gross_profit_margin_quarter"`      //总销售毛利率单季度
    AnnouncementIncrease       *decimal.Decimal                                 // 公告涨幅
    ForecastIncrease           *decimal.Decimal                                 // 预告涨幅
    IndustryName               []IndustryNameStruct                             // 行业名称
    NetProfitAfterDedGrowthRateQuarter *decimal.Decimal  `gorm:"column:net_profit_after_ded_growth_rate_quarter"`    //扣非净利润同比增长率（单季度）(%)
    BusinessRevenueGrowthRateQuarter  *decimal.Decimal   `gorm:"column:business_revenue_growth_rate_quarter"`    //营业收入同比增长(%)

}

type SearchIndustryTrackRequest struct {
    ReportPeriod            string `json:"report_period" form:"report_period" binding:"required"`                         //报告期(公告表里面的报告期)
    IndustryName            string `json:"industry_name" form:"search_industry"`                                           //行业内容搜索(公告表+行业表里面)
    Stock                   string `json:"stock" form:"search"`                                                         //行业内容搜索(公告表+行业表里面)
    UserId                  string `json:"user_id" form:"user_id"`                                                        //用户id(行业表里面的用户id)
    Page                    int    `json:"page" form:"page" binding:"required"`
    PageSize                int    `json:"page_size" form:"page_size" binding:"required"`
    ForecastIncreaseDay     int    `json:"forecast_increase_day" form:"forecast_increase_day" binding:"required"`         //预告涨幅( 1,3,5 天)
    AnnouncementIncreaseDay int    `json:"announcement_increase_day" form:"announcement_increase_day" binding:"required"` // 公告涨幅
}

type TrackReportPeriod struct {
    ReportPeripod         *time.Time   `gorm:"column:report_period"`
}

type IndustryNameStruct struct {
    IndustryName               string   `json:"industry_name" gorm:"column:industry_name"`
    Code                       string   `json:"code" gorm:"column:code"`
    IndustryInterpretation     string   `json:"industry_interpretation" gorm:"column:industry_interpretation"`
    RealName                   string   `json:"realname" gorm:"column:realname"`
    UserId                     string   `json:"user_id" gorm:"column:user_id"`
}

type PStockDate struct {
    Close                      decimal.Decimal    `gorm:"column:close;type:decimal(10,2)" json:"close"`          // 涨幅
    Code                       string             `gorm:"column:stock_code"`                                      //股票代码
    ReportPeripod              string             `gorm:"column:report_period"`                                   // 报告期
    Number                     int                `gorm:"column:line_number"`                                     // 报告期
    PublishDate                string             `gorm:"column:publish_date"`                                     // 报告期
    Increase                   decimal.Decimal                                                                  // 报告期涨幅
}

type Codes struct {
    Code                       string             `gorm:"column:code"`
}
//行业跟踪:搜索
func SearchIndustryTrackList(p SearchIndustryTrackRequest) (res []PerformanceDTO,err error) {
    var (
        b_s     template.BStock
        performanceDTO []PerformanceDTO
        reportPeriod []*time.Time
        performacesList []PerformanceSigle
    )
    //公告表,预告表,股票行业关系表,行业表，联查
    dbs := models.DB().Table("u_performance_forecast").
        Joins("left join u_performance_announcement on u_performance_announcement.code = u_performance_forecast.code and u_performance_announcement.report_period = u_performance_forecast.report_period").
        Joins("left join u_performance_edit on u_performance_edit.code = u_performance_forecast.code and u_performance_edit.report_period = u_performance_forecast.report_period").
        Joins("left join u_stock_industry_relationship on u_performance_forecast.code = u_stock_industry_relationship.stock_id").
        Joins("left join u_performance_industry on u_stock_industry_relationship.industry_id = u_performance_industry.id").
        Select( "u_performance_forecast.*,u_performance_forecast.publish_date as f_publish_date,u_performance_announcement.publish_date as a_publish_date," +
            "u_performance_edit.individual_track,u_stock_industry_relationship.id,u_stock_industry_relationship.user_id,u_performance_announcement.* ").
        Group("u_performance_forecast.code,u_performance_forecast.report_period")
    if p.IndustryName != "" {
        //查询行业名称
        dbs = dbs.Where("u_performance_industry.industry_name like ?", "%"+p.IndustryName+"%")
    }
    if p.Stock != "" {
        if parser.IsNum(p.Stock){
            //纯数字:查看search内容是否是股票代码
            models.DB().Table("b_stock").Where("id = ?", p.Stock).Select("id").Find(&b_s)
            if b_s.ID == p.Stock {
                dbs = dbs.Where("u_performance_forecast.code = ?", p.Stock) //公告表中符合code的
            }
        } else {
            //判断search是否是股票名称
            models.DB().Where("name = ?", p.Stock).Select("name").Find(&b_s)
            if b_s.Name == p.Stock {
                //search 为股票名称
                dbs = dbs.Where("u_performance_forecast.name = ?", p.Stock) //公告表中符合code的
                p.Stock = b_s.ID
            }
        }
    }

    //报告期条件筛选,取出上年度的同比报告期到当前报告期
    year, _ := strconv.Atoi(p.ReportPeriod[0:4]) //年(报告期)
    date := p.ReportPeriod[5:10]                 //月-日(报告期)
    dbs = dbs.Where("u_performance_forecast.report_period >= ?", strconv.Itoa(year-1)+"-"+date).
        Where("LEFT(u_performance_forecast.code,1) in (0,3,6)").
        Where("u_performance_forecast.report_period <= ?", strconv.Itoa(year)+"-"+date)
    //姓名条件筛选
    if p.UserId != "" {
        codes := make([]Codes, 0)
        models.DB().Table("u_stock_industry_relationship").Select("stock_id as code").Where("user_id = ?", p.UserId).Find(&codes)
        userIds := []string{}
        for _,value := range codes {
            userIds = append(userIds,value.Code)
        }
        //查看指定用户
        dbs = dbs.Where("u_performance_forecast.code in(?)", userIds)
    }
    err = dbs.Order("u_performance_forecast.report_period desc,u_performance_forecast.publish_date desc").Find(&performacesList).Error
    if err != nil {
        logging.Error("performance.SearchIndustryTrackList err:", err.Error())
    }
    performace := make(map[string][]*PerformanceSigle)
    // 按照股票进行分组
    trackReportPeriod := &TrackReportPeriod{}
    err =  models.DB().Table("u_performance_forecast").Model(trackReportPeriod).
        Where("u_performance_forecast.report_period >= ?",strconv.Itoa(year-1)+"-"+date).Order("report_period asc").Limit("5").
        Pluck("distinct(report_period)",&reportPeriod).Error
    if err != nil {
        logging.Error("performance.SearchIndustryTrackList err:", err.Error())
    }
    for i,value := range performacesList {
        // 根据股票ID存一个map
        if value.ReportPeripod.Format("2006-01-02") == p.ReportPeriod || len(performace[value.Code]) != 0  {
            performace[value.Code] = append(performace[value.Code],&performacesList[i])
        }
    }
    industryAll := GetIndustryNameAll()
    // 对得到的Map进行排序，没有季度的进行填充
    // 获取当前报告期的公告涨幅map
    announcementIncrease := GetAnnouncementIncrease(p.ReportPeriod,p.AnnouncementIncreaseDay,"u_performance_announcement",p.Stock)
    forecastIncrease := GetAnnouncementIncrease(p.ReportPeriod,p.ForecastIncreaseDay,"u_performance_forecast",p.Stock)
    // 遍历key为code的map
    for _,value := range performace {
        // 对单个股票切片进行遍历
        performaceQuarterArr := [5]*PerformanceSigle{}
        for _,item := range value {
            // 对切片进行赋值
            for index,report :=range reportPeriod{
                if item.ReportPeripod.String() == report.String(){
                    if item.AnnouncementPublishDate != nil {
                        item.AnnouncementIncrease = GetIncreaseByCode(item.AnnouncementPublishDate,item.Code,announcementIncrease)
                    }
                    if item.ForecastPublishDate != nil {
                        item.ForecastIncrease = GetIncreaseByCode(item.ForecastPublishDate,item.Code,forecastIncrease)
                    }
                    if item.PerformanceIncrUpper == ""{
                        item.PerformanceIncrUpper = item.PerformanceIncrLower  // 如果没有上限，就用下限来排序
                    }
                    item.IndustryName = industryAll[item.Code]
                    performaceQuarterArr[index] = item
                }
            }
        }
        // 把数组append到DTO
        performanceDTO = append(performanceDTO,performaceQuarterArr)
    }
    return performanceDTO,nil
}

// 获取涨幅(公告涨幅 预告涨幅)
func GetAnnouncementIncrease(report_date string,announcement_day int,table_name,code string) (res []PStockDate) {
    pStacks := []PStockDate{}
    var err error
    if code != "" {
        err = models.DB().Raw("select temp.* from (select stock.stock_code,stock.trade_date,stock.close, anno.publish_date," +
            " ROW_NUMBER() over(partition by stock.stock_code order by stock.trade_date asc) as line_number" +
            " from p_stock_tick as stock," + table_name +" as anno " +
            "WHERE stock.stock_code = anno.`code` " +
            "and anno.report_period = ? and stock.trade_date >= anno.publish_date and stock.stock_code = ? ) as temp where line_number=1 or line_number = ?",
            report_date,code, announcement_day+1).Find(&pStacks).Error
    }else{
        err = models.DB().Raw("select temp.* from (select stock.stock_code,stock.trade_date,stock.close, anno.publish_date," +
            " ROW_NUMBER() over(partition by stock.stock_code order by stock.trade_date asc) as line_number" +
            " from p_stock_tick as stock," + table_name +" as anno " +
            "WHERE stock.stock_code = anno.`code` " +
            "and anno.report_period = ? and stock.trade_date >= anno.publish" +
            "_date ) as temp where line_number=1 or line_number = ?",
            report_date, announcement_day+1).Find(&pStacks).Error
    }
    if err != nil {
        return nil
    }
    return pStacks
}


// 获取股票的涨幅
func GetIncreaseByCode(publish_date *time.Time,code string,stock_arr []PStockDate) *decimal.Decimal {
    var preClose decimal.Decimal
    var increase  *decimal.Decimal
    for _,value := range stock_arr {
        if value.Code == code && value.PublishDate[0:10] == publish_date.Format("2006-01-02"){
            if value.Number == 1{
                preClose = value.Close
                continue
            }
            if !preClose.IsZero() {
                increase_d := value.Close.Sub(preClose).Div(preClose)
                increase = &increase_d
            }
        }
    }
    return increase
}

func GetIndustryNameAll() map[string][]IndustryNameStruct {
    industryNames := []IndustryNameStruct{}
    models.DB().Table("u_stock_industry_relationship").
        Joins("left join u_performance_industry on u_stock_industry_relationship.industry_id = u_performance_industry.id").
        Joins("left join b_user on u_stock_industry_relationship.user_id = b_user.id").
        Select("u_stock_industry_relationship.stock_id as code,u_performance_industry.industry_name,u_stock_industry_relationship.user_id,b_user.realname,u_performance_industry.industry_interpretation").
        Group("u_stock_industry_relationship.stock_id,u_performance_industry.industry_name").Find(&industryNames)
    industryNameMap := make(map[string][]IndustryNameStruct)
    for _,value := range industryNames {
        industryNameMap[value.Code] = append(industryNameMap[value.Code],value)
    }
    return industryNameMap
}

