package performance

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"strings"
	"time"
)

const DefaultIndustryId = 155
const DefaultFirstIndustry = "次新股"
const DefaultSecondIndustry = "未分配"

type IndustryEO struct {
	IndustryId     int    `json:"industry_id" form:"industry_id" binding:"required"`
	FirstIndustry  string `json:"first_industry" form:"first_industry"`
	SecondIndustry string `json:"second_industry" form:"second_industry"`
	Important      *int   `json:"important" form:"important" binding:"gte=0,lte=1"`
	UserId         int    `json:"user_id" form:"user_id"`
}

type IndustryIO struct {
	FirstIndustry  string `json:"first_industry" form:"first_industry" binding:"required"`
	SecondIndustry string `json:"second_industry" form:"second_industry" binding:"required"`
	Important      *int   `json:"important" form:"important" binding:"gte=0,lte=1"`
	UserId         int    `json:"user_id" from:"user_id"`
}

func EditIndustryAndUser(eo *IndustryEO) error {
	tx := models.DB().Begin()
	if eo.FirstIndustry != "" && eo.SecondIndustry != "" {
		firstIdList := make([]int, 0)
		err := tx.Table("u_first_industry").
			Where("name=?", eo.FirstIndustry).
			Pluck("id", &firstIdList).Error
		if err != nil {
			tx.Rollback()
			return err
		}
		if len(firstIdList) <= 0 {
			err = tx.Table("u_performance_industry").
				Where("id=?", eo.IndustryId).
				Pluck("first_id", &firstIdList).Error
			if err != nil {
				tx.Rollback()
				return err
			}
			err = tx.Table("u_first_industry").
				Where("id in (?)", firstIdList).
				Update("name", eo.FirstIndustry).Error
			if err != nil {
				tx.Rollback()
				return err
			}
		}
		err = tx.Table("u_performance_industry").
			Where("id=?", eo.IndustryId).
			Updates(map[string]interface{}{
				"industry_name": eo.FirstIndustry + "-" + eo.SecondIndustry,
				"first_id":      firstIdList[0],
				"important":     *eo.Important,
			}).Error
		if err != nil {
			tx.Rollback()
			return err
		}
		err = tx.Table("u_second_industry_user").
			Where("industry_id=?", eo.IndustryId).
			Updates(map[string]interface{}{
				"first_industry":  eo.FirstIndustry,
				"second_industry": eo.SecondIndustry,
			}).Error
		if err != nil {
			tx.Rollback()
			return err
		}
		err = tx.Table("u_stock_industry").
			Where("industry_id=?", eo.IndustryId).
			Updates(map[string]interface{}{
				"first_industry":  eo.FirstIndustry,
				"second_industry": eo.SecondIndustry,
				"actual_industry": eo.SecondIndustry,
			}).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	} else {
		return errors.New("行业信息为空")
	}
	if eo.UserId >= 0 {
		err := tx.Table("u_stock_industry_relationship").
			Where("industry_id=?", eo.IndustryId).
			Update("user_id", eo.UserId).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	tx.Commit()
	return nil
}

func AddIndustry(io *IndustryIO) error {
	ids := make([]int, 0)
	err := models.DB().Table("u_performance_industry").Where("industry_name=?", io.FirstIndustry+"-"+io.SecondIndustry).Pluck("id", &ids).Error
	if err != nil {
		return err
	}
	if len(ids) > 0 {
		return errors.New("行业已存在")
	}
	type firstIndustryDTO struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
	}
	tx := models.DB().Begin()
	first := firstIndustryDTO{Name: io.FirstIndustry}
	err = tx.Table("u_first_industry").
		Where("name=?", io.FirstIndustry).
		FirstOrCreate(&first).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	dto := IndustryDTO{IndustryName: io.FirstIndustry + "-" + io.SecondIndustry, FirstId: first.Id, Important: *io.Important}
	err = tx.Table("u_performance_industry").Create(&dto).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	rio := RelationShipDTO{
		UserId:     fmt.Sprintf("%d", io.UserId),
		IndustryId: dto.ID,
	}
	err = tx.Table("u_stock_industry_relationship").Create(&rio).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

type IndustryCodeAdder struct {
	IndustryId int    `json:"industry_id" form:"industry_id" binding:"required"`
	StockId    string `json:"stock_id" form:"stock_id" binding:"required"`
	Head       *int   `json:"head" form:"head" binding:"gte=0,lte=1"`
	UserId     int    `json:"user_id"`
}

func IndustryAddCode(io *IndustryCodeAdder) error {
	industryIds := make([]int, 0)
	industryNames := make([]string, 0)
	err := models.DB().Table("u_stock_industry_relationship").
		//Joins("inner join u_performance_industry on u_performance_industry.id=u_stock_industry_relationship.industry_id").
		Where("stock_id=?", io.StockId).
		Pluck("industry_id", &industryIds).Error
	if err != nil {
		return err
	}
	/*if len(industryIds) > 0 {
		return errors.New("该股票已被分配" + industryIds[0] + "行业")
	}*/
	for _, id := range industryIds {
		err := IndustryDelCode(&IndustryDelCodeQO{
			IndustryId: id,
			Code:       io.StockId,
		})
		if err != nil {
			logging.Error("IndustryAddCode 自动删除分配的成分股报错", err.Error())
		}
	}
	ids := make([]int, 0)
	userIds := make([]int, 0)
	err = models.DB().Table("u_stock_industry_relationship").
		Where("industry_id=? and (stock_id='' OR stock_id is null)", io.IndustryId).
		Pluck("id", &ids).Error
	err = models.DB().Table("u_stock_industry_relationship").
		Where("industry_id=? and user_id!=0", io.IndustryId).
		Pluck("user_id", &userIds).Error
	tx := models.DB().Begin()
	if len(ids) > 0 {
		if len(ids) > 1 {
			logging.Error("u_stock_industry_relationship industry_id:", io.IndustryId, "对应数量异常")
		}
		if len(userIds) > 0 {
			err = tx.Table("u_stock_industry_relationship").
				Where("id = ?", ids[0]).
				Updates(map[string]interface{}{
					"stock_id": io.StockId,
					"user_id":  userIds[0],
				}).Error
		} else {
			err = tx.Table("u_stock_industry_relationship").
				Where("id = ?", ids[0]).
				Update("stock_id", io.StockId).Error
		}
		if err != nil {
			tx.Rollback()
			return err
		}
	} else {
		if len(userIds) > 0 {
			io.UserId = userIds[0]
		}
		type adder struct {
			IndustryId int    `json:"industry_id" form:"industry_id" binding:"required"`
			StockId    string `json:"stock_id" form:"stock_id" binding:"required"`
			UserId     int    `json:"user_id"`
		}
		adderIO := adder{
			IndustryId: io.IndustryId,
			StockId:    io.StockId,
			UserId:     io.UserId,
		}
		err = tx.Table("u_stock_industry_relationship").Create(&adderIO).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	err = models.DB().Table("u_performance_industry").
		Where("id=?", io.IndustryId).
		Pluck("industry_name", &industryNames).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	if len(industryNames) <= 0 {
		tx.Rollback()
		return errors.New("行业不存在")
	}
	industrys := strings.Split(industryNames[0], "-")
	err = tx.Exec(fmt.Sprintf("insert into u_stock_industry (code,first_industry,second_industry,actual_industry,industry_id,head) VALUES('%s','%s','%s','%s',%d,%d)", io.StockId, industrys[0], industrys[1], industrys[1], io.IndustryId, *io.Head)).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

type IndustryUserDTO struct {
	IndustryId   int       `json:"industry_id"`
	IndustryName string    `json:"industry_name"`
	UserId       int       `json:"user_id"`
	UserName     string    `json:"user_name"`
	Important    int       `json:"important"`
	UpdatedAt    time.Time `json:"updated_at"`
}

type IndustryUserVO struct {
	IndustryId     int    `json:"industry_id"`
	FirstIndustry  string `json:"first_industry"`
	SecondIndustry string `json:"second_industry"`
	UserId         int    `json:"user_id"`
	UserName       string `json:"user_name"`
	UpdatedAt      string `json:"updated_at"`
	Important      int    `json:"important"`
}

func (dto IndustryUserDTO) DTO2VO() crud_service.VO {
	vo := IndustryUserVO{
		IndustryId: dto.IndustryId,
		UserId:     dto.UserId,
		UserName:   dto.UserName,
		UpdatedAt:  dto.UpdatedAt.Format(util.YMDHMS),
		Important:  dto.Important,
	}
	industryName := strings.Split(dto.IndustryName, "-")
	if len(industryName) < 2 {
		logging.Error("u_performance_industry行业名异常:" + dto.IndustryName)
		return vo
	}
	vo.FirstIndustry = industryName[0]
	vo.SecondIndustry = industryName[1]
	return vo
}

type IndustryUserQO struct {
	Search string `json:"search" form:"search"`
	models.Paginate
}

func GetIndustryList(qo *IndustryUserQO) ([]crud_service.DTO, int, error) {
	dtos := make([]IndustryUserDTO, 0)
	rets := make([]crud_service.DTO, 0)
	cnt := 0
	dbs := models.DB().Table("u_performance_industry").
		Select("u_performance_industry.industry_name,u_performance_industry.important,b_user.realname user_name,u_performance_industry.id industry_id,u_stock_industry_relationship.user_id, GREATEST(u_performance_industry.updated_at,max(u_stock_industry_relationship.updated_at)) updated_at").
		Joins("left join u_stock_industry_relationship on u_performance_industry.id=u_stock_industry_relationship.industry_id").
		Joins("left join b_user on b_user.id=u_stock_industry_relationship.user_id").
		//Where("stock_id is not null and stock_id !=''").
		Group("u_performance_industry.id")
	if qo.Search != "" {
		dbs = dbs.Where("u_performance_industry.industry_name like ?", "%"+qo.Search+"%")
	}
	err := dbs.Count(&cnt).Error
	if err != nil {
		return nil, 0, err
	}
	err = dbs.Order("GREATEST(u_performance_industry.updated_at,max(u_stock_industry_relationship.updated_at)) desc,u_performance_industry.id").
		Offset(qo.PageSize * (qo.PageNum - 1)).
		Limit(qo.PageSize).
		Find(&dtos).Error
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		rets = append(rets, dto)
	}
	return rets, cnt, nil
}

type IndustryDetailQO struct {
	IndustryId int    `json:"industry_id" form:"industry_id" binding:"gt=0"`
	Search     string `json:"search" form:"search"`
	models.Paginate
}

type stockRaw struct {
	IndustryId int    `json:"industry_id"`
	Code       string `json:"code"`
	Name       string `json:"name"`
	Head       int    `json:"head"`
}

type IndustryDetailDTO struct {
	Id           int        `json:"id"`
	IndustryName string     `json:"industry_name"`
	StockRaw     []stockRaw `json:"stock_raw" gorm:"FOREIGNKEY:IndustryId;ASSOCIATION_FOREIGNKEY:Id"`
}

type IndustryDetailVO struct {
	Id           int        `json:"id"`
	IndustryName string     `json:"industry_name"`
	StockRaw     []stockRaw `json:"stock_raw"`
}

func (dto IndustryDetailDTO) DTO2VO() crud_service.VO {
	vo := IndustryDetailVO{
		Id:           dto.Id,
		IndustryName: dto.IndustryName,
	}
	vo.StockRaw = make([]stockRaw, 0)
	vo.StockRaw = dto.StockRaw
	return vo
}

func GetIndustryDetail(qo *IndustryDetailQO) ([]crud_service.DTO, int, error) {
	dtos := make([]IndustryDetailDTO, 0)
	rets := make([]crud_service.DTO, 0)
	cnt := 0
	subSql := models.DB().Table("u_stock_industry").
		Select("u_stock_industry.industry_id,u_stock_industry.code,b_stock.name,u_stock_industry.head").
		Joins("left join b_stock on b_stock.id=u_stock_industry.code")
	if qo.Search != "" {
		subSql = subSql.Where("u_stock_industry.code like ? or b_stock.name like ?", "%"+qo.Search+"%", "%"+qo.Search+"%")
	}
	dbs := models.DB().Table("u_performance_industry").
		Preload("StockRaw", func(db *gorm.DB) *gorm.DB {
			return subSql.Offset(qo.PageSize * (qo.PageNum - 1)).
				Limit(qo.PageSize).Order("head desc,code desc")
		}).Where("id=?", qo.IndustryId)
	err := subSql.Where("industry_id=?", qo.IndustryId).Count(&cnt).Error
	err = dbs.Find(&dtos).Error
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		rets = append(rets, dto)
	}
	return rets, cnt, nil
}

type IndustryDelQO struct {
	Id int `json:"id" form:"id" binding:"gt=0"`
}

func DelIndustry(qo *IndustryDelQO) error {
	if qo.Id == DefaultIndustryId {
		return errors.New("默认行业禁止删除")
	}
	tx := models.DB().Begin()
	err := tx.Exec(fmt.Sprintf("delete from u_performance_industry where id=%d", qo.Id)).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	err = tx.Exec(fmt.Sprintf("update u_second_industry_user set deleted_at='%s' where industry_id=%d and deleted_at is null", time.Now().Format(util.YMDHMS), qo.Id)).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	err = tx.Exec(fmt.Sprintf("delete from u_stock_industry where industry_id=%d", qo.Id)).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	err = tx.Exec(fmt.Sprintf("delete from u_stock_industry_relationship where industry_id=%d", qo.Id)).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

type IndustryDelCodeQO struct {
	IndustryId int    `json:"industry_id" form:"industry_id" binding:"gt=0"`
	Code       string `json:"code" form:"code"`
}

func IndustryDelCode(qo *IndustryDelCodeQO) error {
	tx := models.DB().Begin()
	err := tx.Exec(fmt.Sprintf("update u_stock_industry_relationship set stock_id='' where industry_id=%d and stock_id='%s'", qo.IndustryId, qo.Code)).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	err = tx.Exec(fmt.Sprintf("delete from u_stock_industry where industry_id=%d and code='%s'", qo.IndustryId, qo.Code)).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

type CodeEO struct {
	Code string `json:"code" form:"code" binding:"required"`
	Head *int   `json:"head" form:"head" binding:"gte=0,lte=1"`
}

func SetIndustryHeadCode(eo *CodeEO) error {
	err := models.DB().Table("u_stock_industry").Where("code=?", eo.Code).Update("head", *eo.Head).Error
	return err
}

type CodeFullIndustryDTO struct {
	FirstIndustry  string `json:"first_industry"`
	SecondIndustry string `json:"second_industry"`
	FirstId        int    `json:"first_id"`
	SecondId       int    `json:"second_id"`
}

func GetIndustryByCode(code string) (CodeFullIndustryDTO, error) {
	dto := CodeFullIndustryDTO{}
	err := models.DB().Table("u_stock_industry").
		Select("u_stock_industry.first_industry,u_stock_industry.second_industry,u_stock_industry.industry_id second_id,u_performance_industry.first_id").
		Joins("LEFT JOIN u_performance_industry on u_performance_industry.id=u_stock_industry.industry_id").
		Where("code=?", code).First(&dto).Error
	if err != nil {
		logging.Error(code+"行业信息未获取到", err.Error())
		return CodeFullIndustryDTO{}, err
	}
	return dto, nil
}
