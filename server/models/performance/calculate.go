package performance

import (
	"datacenter/models"
	"datacenter/models/common"
	"datacenter/models/stock"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"go.mongodb.org/mongo-driver/bson"
	"gopkg.in/mgo.v2"
	"math"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type PrformanceQO struct {
	Direction      bool   `json:"direction" form:"direction"`
	ForecastDays   string `json:"forecast_days" form:"forecast_days" binding:"oneof=1 3 5"`
	AnnounceDays   string `json:"announce_days" form:"announce_days" binding:"oneof=1 3 5"`
	PageNum        int    `json:"page_num" form:"page_num"`
	PageSize       int    `json:"page_size" form:"page_size"`
	ReportPeriod   string `json:"report_period" form:"report_period" binding:"required"`
	Search         string `json:"search" form:"search"`
	SearchIndustry string `json:"search_industry" form:"search_industry"`
	SortId         int    `json:"sort_id" form:"sort_id"`
	SortKey        string `json:"sort_key" form:"sort_key"`
	UserId         string `json:"user_id" form:"user_id"`
}
type CaculateQO struct {
	Recommend              string `json:"recommend" form:"recommend"`
	Important              string `json:"important" form:"important"`
	Direction              bool   `json:"direction" form:"direction"`
	PageNum                int    `json:"page_num" form:"page_num" binding:"required,gte=1"`
	PageSize               int    `json:"page_size" form:"page_size" binding:"required,gte=1"`
	ReportPeriod           string `json:"report_period" form:"report_period" binding:"required"`
	DisclosureStartTime    string `json:"disclosure_start_time" form:"disclosure_start_time"`
	DisclosureEndTime      string `json:"disclosure_end_time" form:"disclosure_end_time"`
	Search                 string `json:"search" form:"search"`
	SearchIndustry         string `json:"search_industry" form:"search_industry"`
	SortKey                int    `json:"sort_key" form:"sort_key"`
	UserId                 int    `json:"user_id" form:"user_id"`
	Type                   string `json:"type" form:"type"`
	AmountSize             int    `json:"amount_size" form:"amount_size" binding:"oneof=0 1 2 3"`
	PreNetProfitGrowthRate int    `json:"pre_net_profit_growth_rate" form:"pre_net_profit_growth_rate" binding:"oneof=0 1 2"`
	Board                  int    `json:"board" form:"board" binding:"oneof=0 1 2 3 4 5"`
	Hurry                  string `json:"hurry" form:"hurry"`
}

type CaculateEPQO struct {
	Recommend              string `json:"recommend" form:"recommend"`
	Important              string `json:"important" form:"important"`
	Direction              bool   `json:"direction" form:"direction"`
	ReportPeriod           string `json:"report_period" form:"report_period" binding:"required"`
	DisclosureStartTime    string `json:"disclosure_start_time" form:"disclosure_start_time"`
	DisclosureEndTime      string `json:"disclosure_end_time" form:"disclosure_end_time"`
	Search                 string `json:"search" form:"search"`
	SearchIndustry         string `json:"search_industry" form:"search_industry"`
	SortKey                int    `json:"sort_key" form:"sort_key"`
	UserId                 int    `json:"user_id" form:"user_id"`
	Type                   string `json:"type" form:"type"`
	AmountSize             int    `json:"amount_size" form:"amount_size" binding:"oneof=0 1 2 3"`
	PreNetProfitGrowthRate int    `json:"pre_net_profit_growth_rate" binding:"oneof=0 1 2"`
	Board                  int    `json:"board" form:"board" binding:"oneof=0 1 2 3 4 5"`
	Hurry                  string `json:"hurry" form:"hurry"`
}

type PerformanceEditIO struct {
	Code                 string `json:"code" form:"code" binding:"required"`
	ForecastProportion   string `json:"forecast_proportion" form:"forecast_proportion"`
	BrokerProportion     string `json:"broker_proportion" form:"broker_proportion"`
	BrokerInterpretation string `json:"broker_interpretation" form:"broker_interpretation"`
	ForecastNetProfit    string `json:"forecast_net_profit" form:"forecast_net_profit"`
	HighLights           string `json:"high_lights" form:"high_lights"`
	Important            *int   `json:"important" form:"important"`
	Recommend            int    `json:"recommend" form:"recommend" binding:"required"`
	Type                 string `json:"type" form:"type"`
	UserId               int    `json:"user_id" form:"user_id" binding:"required"`
	ReportPeriod         string `json:"report_period" form:"report_period" binding:"required"`
	Hurry                int    `json:"hurry" form:"hurry" `
}

type CalculateVO struct {
	Inductry               string `json:"inductry"`
	IndustryInterpretation string `json:"industry_interpretation"`
	UserName               string `json:"user_name"`
	UserIds                string `json:"user_ids"`
	Code                   string `json:"code"`
	Name                   string `json:"name"`
	AmountAVG              string `json:"amount_avg"`
	ExpectedPublishDate    string `json:"expected_publish_date"`   //预约时间
	IndividualShareTrack   string `json:"individual_share_track"`  //个股跟踪
	DisclosureTime         string `json:"disclosure_time"`         //披露时间
	DisclosureTimeHistory  string `json:"disclosure_time_history"` //预约时间历史
	DisclosureChangeAt     string `json:"disclosure_change_at"`    //披露时间更新时间（无则为空）
	ReportPeriod           string `json:"report_period"`           //报告期
	ForecastProportion     string `json:"forecast_proportion"`     //预测比例
	ForecastNetProfit      string `json:"forecast_net_profit"`     //预测净利润
	BrokerProportion       string `json:"broker_proportion"`       //券商预测
	NextBrokerProportion   string `json:"next_broker_proportion"`  //下季度券商预测
	BrokerInterpretation   string `json:"broker_interpretation"`   //券商解读
	HighLights             string `json:"high_lights"`             //亮点

	//ExpectedNetProfitUpper string `json:"expected_net_profit_upper"` //预计净利润上限
	//ExpectedNetProfitLower string `json:"expected_net_profit_lower"` //预计净利润下限
	PerformanceReason    string `json:"performance_reason"`     //业绩变动原因
	PerformanceIncrUpper string `json:"performance_incr_upper"` //业绩变动幅度上限(行业追踪里面的本期预告)
	PerformanceIncrLower string `json:"performance_incr_lower"` //业绩变动幅度下限(行业跟踪里面的本期预告)
	Important            string `json:"important"`
	Recommend            string `json:"recommend"`
	Type                 string `json:"type"`
	Hurry                string `json:"hurry"`

	NetProfit                          string `json:"net_profit"`                              //净利润（年度）
	NetProfitGrowthRate                string `json:"net_profit_growth_rate"`                  //净利润同比增长
	NetProfitComparativeGrowth         string `json:"net_profit_comparative_growth"`           //净利润环比增长
	BusinessRevenue                    string `json:"business_revenue"`                        //营业收入（年度）
	BusinessRevenueGrowthRate          string `json:"business_revenue_growth_rate"`            //营业收入同比增长(%)
	BusinessRevenueComparativeGrowth   string `json:"business_revenue_comparative_growth"`     //营业收入环比增长
	CashFlowPerShare                   string `json:"cash_flow_per_share"`                     //每股经营现金流量
	GrossProfitMargin                  string `json:"gross_profit_margin"`                     //总销售毛利率(%)
	NetProfitAfterDed                  string `json:"net_profit_after_ded"`                    //扣非净利润（年度）
	NetProfitAfterDedGrowthRate        string `json:"net_profit_after_ded_growth_rate"`        //扣非净利润同比增长率(%)
	NetProfitAfterDedComparativeGrowth string `json:"net_profit_after_ded_comparative_growth"` //扣非净利润环比增长率(%)

	PreNetProfitGrowthRate string `json:"pre_net_profit_growth_rate"` //上季度净利润同比增长

	AOne         string `json:"a_one"`
	AThree       string `json:"a_three"`
	AFive        string `json:"a_five"`
	APreOne      string `json:"a_pre_one"`
	APreThree    string `json:"a_pre_three"`
	APreFive     string `json:"a_pre_five"`
	FOne         string `json:"f_one"`
	FThree       string `json:"f_three"`
	FFive        string `json:"f_five"`
	FPreOne      string `json:"f_pre_one"`
	FPreThree    string `json:"f_pre_three"`
	FPreFive     string `json:"f_pre_five"`
	AutoCalRate  string `json:"auto_cal_rate"`
	AutoRateRule string `json:"auto_rate_rule"`

	NearTenIncRate string `json:"near_ten_inc_rate"`
	Head           int    `json:"head"`
}

/*type TotalChangeDTO struct {
	TradeDate *time.Time `json:"trade_date"`
	Change decimal.Decimal `json:"total_close"`
	TsCode string `json:"ts_code"`
}

func (dto TotalChangeDTO)TableName() string {
	return "p_stock_tick_1"
}

type ChangeRateDTO struct {
	StockCode string `json:"stock_code"`
	TradeDate *time.Time `json:"trade_date"`
	Change decimal.Decimal `json:"change"`
	TotalChange []TotalChangeDTO `json:"total_change" gorm:"FOREIGNKEY:TradeDate;ASSOCIATION_FOREIGNKEY:TradeDate"`
}

func (dto ChangeRateDTO)TableName()string  {
	return "p_stock_tick"
}*/

type CalculateDTO struct {
	IndustryName           string              `json:"industry_name"`
	IndustryInterpretation string              `json:"industry_interpretation"`
	UserName               string              `json:"user_name"`
	UserIds                string              `json:"user_ids"`
	Code                   string              `json:"code"`
	Name                   string              `json:"name"`
	AmountAVG              decimal.NullDecimal `json:"amount_avg"`
	//Amount                 StockTickAmountAVGDTO `json:"amount" gorm:"FOREIGNKEY:Code;ASSOCIATION_FOREIGNKEY:StockCode"` //成交额
	//ChangeRates                []ChangeRateDTO `json:"change_rates" gorm:"FOREIGNKEY:StockCode;ASSOCIATION_FOREIGNKEY:Code"`
	ExpectedPublishDate   *time.Time `json:"expected_publish_date"`           //预约时间
	IndividualShareTrack  string     `json:"individual_share_track"`          //个股跟踪
	DisclosureTime        *time.Time `json:"disclosure_time"`                 //披露时间
	DisclosureTimeHistory *time.Time `json:"disclosure_time_history"`         //预约时间历史
	DisclosureChangeAt    string     `json:"disclosure_change_at"`            //披露时间更新时间（无则为空）
	ReportPeriod          *time.Time `json:"report_period"`                   //报告期
	ForecastProportion    string     `json:"forecast_proportion"`             //预测比例
	ForecastNetProfit     string     `json:"forecast_net_profit"`             //预测净利润
	BrokerProportion      string     `json:"broker_proportion"`               //券商预测
	NextBrokerProportion  string     `json:"next_broker_proportion" gorm:"-"` //下季度券商预测
	BrokerInterpretation  string     `json:"broker_interpretation"`           //券商解读
	HighLights            string     `json:"high_lights"`                     //亮点

	//ExpectedNetProfitUpper decimal.NullDecimal `json:"expected_net_profit_upper"` //预计净利润上限
	//ExpectedNetProfitLower decimal.NullDecimal `json:"expected_net_profit_lower"` //预计净利润下限
	PerformanceReason    string              `json:"performance_reason"`     //业绩变动原因
	PerformanceIncrUpper decimal.NullDecimal `json:"performance_incr_upper"` //业绩变动幅度上限(行业追踪里面的本期预告)
	PerformanceIncrLower decimal.NullDecimal `json:"performance_incr_lower"` //业绩变动幅度下限(行业跟踪里面的本期预告)
	Important            string              `json:"important"`
	Recommend            string              `json:"recommend"`
	Type                 string              `json:"type"`
	Hurry                string              `json:"hurry"`

	NetProfit                          decimal.NullDecimal `json:"net_profit"`                              //净利润（年度）
	NetProfitGrowthRate                decimal.NullDecimal `json:"net_profit_growth_rate"`                  //净利润同比增长
	NetProfitComparativeGrowth         decimal.NullDecimal `json:"net_profit_comparative_growth"`           //净利润环比增长
	BusinessRevenue                    decimal.NullDecimal `json:"business_revenue"`                        //营业收入（年度）
	BusinessRevenueGrowthRate          decimal.NullDecimal `json:"business_revenue_growth_rate"`            //营业收入同比增长(%)
	BusinessRevenueComparativeGrowth   decimal.NullDecimal `json:"business_revenue_comparative_growth"`     //营业收入环比增长
	CashFlowPerShare                   decimal.NullDecimal `json:"cash_flow_per_share"`                     //每股经营现金流量
	GrossProfitMargin                  decimal.NullDecimal `json:"gross_profit_margin"`                     //总销售毛利率(%)
	NetProfitAfterDed                  decimal.NullDecimal `json:"net_profit_after_ded"`                    //扣非净利润（年度）
	NetProfitAfterDedGrowthRate        decimal.NullDecimal `json:"net_profit_after_ded_growth_rate"`        //扣非净利润同比增长率(%)
	NetProfitAfterDedComparativeGrowth decimal.NullDecimal `json:"net_profit_after_ded_comparative_growth"` //扣非净利润环比增长率(%)

	PreNetProfitGrowthRate decimal.NullDecimal `json:"pre_net_profit_growth_rate"` //上季度净利润同比增长

	AOne         decimal.NullDecimal `json:"a_one"`
	AThree       decimal.NullDecimal `json:"a_three"`
	AFive        decimal.NullDecimal `json:"a_five"`
	APreOne      decimal.NullDecimal `json:"a_pre_one"`
	APreThree    decimal.NullDecimal `json:"a_pre_three"`
	APreFive     decimal.NullDecimal `json:"a_pre_five"`
	FOne         decimal.NullDecimal `json:"f_one"`
	FThree       decimal.NullDecimal `json:"f_three"`
	FFive        decimal.NullDecimal `json:"f_five"`
	FPreOne      decimal.NullDecimal `json:"f_pre_one"`
	FPreThree    decimal.NullDecimal `json:"f_pre_three"`
	FPreFive     decimal.NullDecimal `json:"f_pre_five"`
	AutoCalRate  decimal.NullDecimal `json:"auto_cal_rate"`
	AutoRateRule string              `json:"auto_rate_rule"`

	NearTenIncRate decimal.NullDecimal `json:"near_ten_inc_rate"`
	Head           int                 `json:"head"`
}

type CalculateEPO struct {
	Code                 string `json:"A"`
	Name                 string `json:"B"`
	DisclosureTime       string `json:"C"` //披露时间
	ForecastProportion   string `json:"D"` //预测比例
	ForecastNetProfit    string `json:"E"` //预测净利润
	HighLights           string `json:"F"` //亮点
	PerformanceIncr      string `json:"G"` //本期预告
	BrokerProportion     string `json:"H"` //券商预测
	BrokerInterpretation string `json:"I"` //券商解读
	AutoCalRate          string `json:"J"`
	AutoRateRule         string `json:"K"`
}

func (dto CalculateDTO) DTO2VO() crud_service.VO {
	return &CalculateVO{
		dto.IndustryName,
		dto.IndustryInterpretation,
		dto.UserName,
		dto.UserIds,
		dto.Code,
		dto.Name,
		util.NullDecimal2String(dto.AmountAVG),
		util.TimePtr2String(dto.ExpectedPublishDate, util.YMD),
		dto.IndividualShareTrack,
		util.TimePtr2String(dto.DisclosureTime, util.YMD),
		util.TimePtr2String(dto.DisclosureTimeHistory, util.YMD),
		dto.DisclosureChangeAt,
		util.TimePtr2String(dto.ReportPeriod, util.YMD),
		dto.ForecastProportion,
		dto.ForecastNetProfit,
		dto.BrokerProportion,
		dto.NextBrokerProportion,
		dto.BrokerInterpretation,
		dto.HighLights,
		dto.PerformanceReason,
		util.NullDecimal2String(dto.PerformanceIncrUpper),
		util.NullDecimal2String(dto.PerformanceIncrLower),
		dto.Important,
		dto.Recommend,
		dto.Type,
		dto.Hurry,
		util.NullDecimal2String(dto.NetProfit),
		util.NullDecimal2String(dto.NetProfitGrowthRate),
		util.NullDecimal2String(dto.NetProfitComparativeGrowth),
		util.NullDecimal2String(dto.BusinessRevenue),
		util.NullDecimal2String(dto.BusinessRevenueGrowthRate),
		util.NullDecimal2String(dto.BusinessRevenueComparativeGrowth),
		util.NullDecimal2String(dto.CashFlowPerShare),
		util.NullDecimal2String(dto.GrossProfitMargin),
		util.NullDecimal2String(dto.NetProfitAfterDed),
		util.NullDecimal2String(dto.NetProfitAfterDedGrowthRate),
		util.NullDecimal2String(dto.NetProfitAfterDedComparativeGrowth),
		util.NullDecimal2String(dto.PreNetProfitGrowthRate),
		util.NullDecimal2String(dto.AOne),
		util.NullDecimal2String(dto.AThree),
		util.NullDecimal2String(dto.AFive),
		util.NullDecimal2String(dto.APreOne),
		util.NullDecimal2String(dto.APreThree),
		util.NullDecimal2String(dto.APreFive),
		util.NullDecimal2String(dto.FOne),
		util.NullDecimal2String(dto.FThree),
		util.NullDecimal2String(dto.FFive),
		util.NullDecimal2String(dto.FPreOne),
		util.NullDecimal2String(dto.FPreThree),
		util.NullDecimal2String(dto.FPreFive),
		util.NullDecimal2String(dto.AutoCalRate),
		dto.AutoRateRule,
		util.NullDecimal2String(dto.NearTenIncRate),
		dto.Head,
	}
}

func (dto CalculateDTO) DTO2EPO() *CalculateEPO {
	epo := CalculateEPO{
		Code:                 dto.Code,
		Name:                 dto.Name,
		DisclosureTime:       util.TimePtr2String(dto.DisclosureTime, util.YMD),
		ForecastNetProfit:    dto.ForecastNetProfit,
		ForecastProportion:   dto.ForecastProportion,
		BrokerInterpretation: dto.BrokerInterpretation,
		BrokerProportion:     dto.BrokerProportion,
		HighLights:           dto.HighLights,
		AutoCalRate:          util.NullDecimal2String(dto.AutoCalRate),
		AutoRateRule:         dto.AutoRateRule,
	}
	if dto.PerformanceIncrLower.Valid {
		if dto.PerformanceIncrLower.Valid {
			epo.PerformanceIncr = dto.PerformanceIncrLower.Decimal.String() + "～" + dto.PerformanceIncrUpper.Decimal.String()
		} else {
			epo.PerformanceIncr = dto.PerformanceIncrUpper.Decimal.String()
		}
	}

	return &epo
}

type PerformanceEditDTO struct {
	models.Simple
	Code                 string     `json:"code"`
	ForecastProportion   string     `json:"forecast_proportion"`
	BrokerProportion     string     `json:"broker_proportion"`
	BrokerInterpretation string     `json:"broker_interpretation"`
	ForecastNetProfit    string     `json:"forecast_net_profit"`
	HighLights           string     `json:"high_lights"`
	Important            *int       `json:"important"`
	Recommend            int        `json:"recommend"`
	UserId               int        `json:"user_id"`
	ReportPeriod         *time.Time `json:"report_period"`
	Hurry                int        `json:"hurry"`
}

type PerformanceEditVO struct {
	models.Simple
	Code                 string `json:"code"`
	ForecastProportion   string `json:"forecast_proportion"`
	BrokerProportion     string `json:"broker_proportion"`
	BrokerInterpretation string `json:"broker_interpretation"`
	ForecastNetProfit    string `json:"forecast_net_profit"`
	HighLights           string `json:"high_lights"`
	Important            *int   `json:"important"`
	Recommend            int    `json:"recommend"`
	UserId               int    `json:"user_id"`
	ReportPeriod         string `json:"report_period"`
	Hurry                int    `json:"hurry"`
}

func (dto PerformanceEditDTO) DTO2VO() crud_service.VO {
	return PerformanceEditVO{
		Simple:               dto.Simple,
		Code:                 dto.Code,
		ForecastNetProfit:    dto.ForecastNetProfit,
		BrokerInterpretation: dto.BrokerInterpretation,
		ForecastProportion:   dto.ForecastProportion,
		BrokerProportion:     dto.BrokerProportion,
		HighLights:           dto.HighLights,
		Important:            dto.Important,
		Recommend:            dto.Recommend,
		UserId:               dto.UserId,
		ReportPeriod:         util.TimePtr2String(dto.ReportPeriod, util.YMD),
		Hurry:                dto.Hurry,
	}
}

type RelationShipDTO struct {
	models.Simple
	StockId    string `json:"stock_id"`
	IndustryId int    `json:"industry_id"`
	UserId     string `json:"user_id"`
}

type UserInfoVO struct {
	ID       int    `json:"id"`
	Realname string `json:"realname"`
}

type UserInfoDTO struct {
	models.Simple
	Realname string `json:"realname"`
}

func (dto UserInfoDTO) DTO2VO() crud_service.VO {
	return &UserInfoVO{
		dto.ID,
		dto.Realname,
	}
}

type IndustryVO struct {
	ID           int    `json:"id"`
	IndustryName string `json:"industry_name"`
}

type IndustryDTO struct {
	models.Simple
	IndustryName string `json:"industry_name"`
	FirstId      int    `json:"first_id"`
	Important    int    `json:"important"`
}

func (dto IndustryDTO) DTO2VO() crud_service.VO {
	return &IndustryVO{
		dto.ID,
		dto.IndustryName,
	}
}

type TrendDTO struct {
	IndustryId                       int                 `json:"industry_id"`
	IndustryName                     string              `json:"industry_name"`
	IndustryInterpretation           string              `json:"industry_interpretation"`
	Code                             string              `json:"code"`
	Name                             string              `json:"name"`
	ReportPeriod                     *time.Time          `json:"report_period"`                       //报告期
	PublishDate                      *time.Time          `json:"publish_date"`                        //公告日期
	NetProfit                        decimal.NullDecimal `json:"net_profit"`                          //净利润（年度）
	NetProfitGrowthRate              decimal.NullDecimal `json:"net_profit_growth_rate"`              //净利润同比增长
	NetProfitComparativeGrowth       decimal.NullDecimal `json:"net_profit_comparative_growth"`       //净利润环比增长
	BusinessRevenue                  decimal.NullDecimal `json:"business_revenue"`                    //营业收入（年度）
	BusinessRevenueGrowthRate        decimal.NullDecimal `json:"business_revenue_growth_rate"`        //营业收入同比增长(%)
	BusinessRevenueComparativeGrowth decimal.NullDecimal `json:"business_revenue_comparative_growth"` //营业收入环比增长
	CashFlowPerShare                 decimal.NullDecimal `json:"cash_flow_per_share"`                 //每股经营现金流量
	GrossProfitMargin                decimal.NullDecimal `json:"gross_profit_margin"`                 //总销售毛利率(%)
	GrossProfitMarginQuarter         decimal.NullDecimal `json:"gross_profit_margin_quarter"`         //单季度销售毛利率(%)
	NetProfitAfterDed                decimal.NullDecimal `json:"net_profit_after_ded"`                //扣非净利润（年度）
	NetProfitAfterDedGrowthRate      decimal.NullDecimal `json:"net_profit_after_ded_growth_rate"`    //扣非净利润同比增长率(%)
}

type StockTrend struct {
	Code string `json:"code"`
	Name string `json:"name"`
	Rate string `json:"rate"`
}
type Reasons struct {
	Name    string `json:"name"`
	Perform string `json:"perform"`
}
type TrendVO struct {
	IndustryName string `json:"industry_name"`
	//IndustryInterpretation string       `json:"industry_interpretation"`
	StockCount  int32        `json:"stock_count"`
	ReportCount int32        `json:"report_count"`
	Stocks      []StockTrend `json:"stocks"`
	Reasons     []Reasons    `json:"reasons"`
	Point       int          `json:"point"`
	//ReportPeriod           *time.Time   `json:"report_period"` //报告期
}

type TrendQO struct {
	SortKey        int    `json:"sort_key" form:"sort_key"`
	Direction      bool   `json:"direction" form:"direction"`
	IndustrySearch string `json:"industry_search" form:"industry_search"`
	ReportPeriod   string `json:"report_period" form:"report_period"`
	PageNum        int    `json:"page_num" form:"page_num"`
	PageSize       int    `json:"page_size" form:"page_size"`
}

func getRedisValue(key string, field string) string {
	value, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString(key, field)
	if err != nil {
		logging.Error("redis get "+field+" value err:", err.Error())
		return ""
	}
	return value
}

func CalculateRate(thisYear, lastYear decimal.NullDecimal) (decimal.Decimal, error) {
	if !thisYear.Valid || !lastYear.Valid || lastYear.Decimal.Cmp(decimal.Zero) == 0 {
		return decimal.Decimal{}, errors.New("wrong value to compare")
	}
	return thisYear.Decimal.Sub(lastYear.Decimal).Mul(decimal.NewFromInt(100)).DivRound(lastYear.Decimal.Abs(), 2), nil
}

func TrendCalculatePoint(beforeLast, lastYear, thisYear TrendDTO, cmpMap []stockInfo) {
	b, l, t := reflect.ValueOf(beforeLast), reflect.ValueOf(lastYear), reflect.ValueOf(thisYear)

	for i, v := range cmpMap {
		rateLastYear, errLast := CalculateRate(l.FieldByName(v.Name).Interface().(decimal.NullDecimal), b.FieldByName(v.Name).Interface().(decimal.NullDecimal))
		rateThisYear, errThis := CalculateRate(t.FieldByName(v.Name).Interface().(decimal.NullDecimal), l.FieldByName(v.Name).Interface().(decimal.NullDecimal))
		//提档或者负转正
		if errThis == nil && errLast == nil && t.FieldByName(v.Name).Interface().(decimal.NullDecimal).Decimal.Cmp(l.FieldByName(v.Name).Interface().(decimal.NullDecimal).Decimal) > 0 && ((l.FieldByName(v.Name).Interface().(decimal.NullDecimal).Decimal.Cmp(decimal.Zero) < 0 && t.FieldByName(v.Name).Interface().(decimal.NullDecimal).Decimal.Cmp(decimal.Zero) > 0) || rateThisYear.Sub(rateLastYear).Cmp(decimal.NewFromFloat32(v.RateLimit)) >= 0) {
			cmpMap[i].RateTotal = cmpMap[i].RateTotal.Add(rateThisYear.Sub(rateLastYear))
			cmpMap[i].PointTotal = cmpMap[i].PointTotal + cmpMap[i].Point
			cmpMap[i].StockCount++

		}
	}
}

func TrendCalculateRevenueAndContractPoint(unearnedRevenueMap, contractMap map[string]interface{}, bfLastYear, lastYear, thisYear string, sinfo *stockInfo) {
	var unearnedRevenue decimal.NullDecimal
	var contract decimal.NullDecimal
	var unearnedRevenuePre decimal.NullDecimal
	var contractPre decimal.NullDecimal
	_ = unearnedRevenue.Scan(unearnedRevenueMap[thisYear])
	_ = contract.Scan(contractMap[thisYear])
	if unearnedRevenue.Decimal.Add(contract.Decimal).Cmp(decimal.NewFromInt(50000000)) > 0 {
		_ = unearnedRevenuePre.Scan(unearnedRevenueMap[lastYear])
		_ = contractPre.Scan(contractMap[lastYear])
		rateThisYear, errThis := CalculateRate(decimal.NullDecimal{Decimal: unearnedRevenue.Decimal.Add(contract.Decimal), Valid: unearnedRevenue.Valid && contract.Valid}, decimal.NullDecimal{Decimal: unearnedRevenuePre.Decimal.Add(contractPre.Decimal), Valid: unearnedRevenue.Valid && contract.Valid})
		_ = unearnedRevenue.Scan(unearnedRevenueMap[bfLastYear])
		_ = contract.Scan(contractMap[bfLastYear])
		rateLastYear, errLast := CalculateRate(decimal.NullDecimal{Decimal: unearnedRevenuePre.Decimal.Add(contractPre.Decimal), Valid: unearnedRevenuePre.Valid && contractPre.Valid}, decimal.NullDecimal{Decimal: unearnedRevenue.Decimal.Add(contract.Decimal), Valid: unearnedRevenuePre.Valid && contractPre.Valid})
		//提档
		if errThis == nil && errLast == nil && rateThisYear.Cmp(decimal.Zero) > 0 && rateThisYear.Sub(rateLastYear).Cmp(decimal.NewFromFloat32(sinfo.RateLimit)) >= 0 {
			sinfo.RateTotal = sinfo.RateTotal.Add(rateThisYear.Sub(rateLastYear))
			sinfo.PointTotal = sinfo.PointTotal + sinfo.Point
			sinfo.StockCount++
		}
	}
}

func TrendCalculateTakenReturn(takeReturnMap map[string]interface{}, lastYear, thisYear string, sinfo *stockInfo) {
	var takeReturn decimal.NullDecimal
	var takeReturnPre decimal.NullDecimal
	var lastlevel, thislevel = 0, 0
	defer func() {
		if r := recover(); r != nil {
			logging.Error("Recovered in TrendCalculateTakenReturn()", takeReturnMap["code"], takeReturnMap[thisYear], takeReturnMap[lastYear], r)
		}
	}()
	_ = takeReturn.Scan(strings.Trim(takeReturnMap[thisYear].(string), "%"))
	_ = takeReturnPre.Scan(strings.Trim(takeReturnMap[lastYear].(string), "%"))
	if takeReturn.Valid && takeReturnPre.Valid {
		for i, v := range sinfo.Section {
			if takeReturnPre.Decimal.Cmp(decimal.NewFromFloat32(v)) > 0 {
				lastlevel = i
			}
			if takeReturn.Decimal.Cmp(decimal.NewFromFloat32(v)) > 0 {
				thislevel = i
			}
		}
	} else {
		return
	}
	//提档
	if takeReturn.Decimal.Sub(takeReturnPre.Decimal).Cmp(decimal.NewFromFloat32(sinfo.RateLimit)) >= 0 && thislevel > lastlevel {
		sinfo.RateTotal = sinfo.RateTotal.Add(takeReturn.Decimal.Sub(takeReturnPre.Decimal))
		sinfo.PointTotal = sinfo.PointTotal + sinfo.Point
		sinfo.StockCount++
	}
}

func TrendCalculateGrossProfit(dtos []TrendDTO, sinfo *stockInfo) {
	var totalGross decimal.Decimal
	var avglevel, thislevel = 0, 0
	for _, v := range dtos[1 : len(dtos)-1] {
		totalGross = totalGross.Add(v.GrossProfitMargin.Decimal)
	}
	avgGross := totalGross.Div(decimal.NewFromInt32(4))
	for i, v := range sinfo.Section {
		if avgGross.Cmp(decimal.NewFromFloat32(v)) > 0 {
			avglevel = i
		}
		if dtos[len(dtos)-1].GrossProfitMargin.Decimal.Cmp(decimal.NewFromFloat32(v)) > 0 {
			thislevel = i
		}
	}
	if avgGross.Cmp(dtos[len(dtos)-1].GrossProfitMargin.Decimal) < 0 && avglevel < thislevel {
		sinfo.RateTotal = sinfo.RateTotal.Add(dtos[len(dtos)-1].GrossProfitMargin.Decimal)
		sinfo.PointTotal = sinfo.PointTotal + sinfo.Point
		sinfo.StockCount++
	}
}

func StockInfoHandler(s *stockInfo, vo *TrendVO, stockCount int32, targetPersent float32) int {
	totalPoint := 0
	if s.PointTotal > 0 && decimal.NewFromInt32(s.StockCount).Div(decimal.NewFromInt32(stockCount)).Cmp(decimal.NewFromFloat32(targetPersent)) > 0 {
		totalPoint = totalPoint + s.Point
		vo.Reasons = append(vo.Reasons, Reasons{Name: s.Reasons.Name, Perform: s.Reasons.Perform + s.RateTotal.DivRound(decimal.NewFromInt32(stockCount), 2).String() + "%"})
	}
	s.StockCount = 0
	s.RateTotal = decimal.Zero
	s.PointTotal = 0
	return totalPoint
}

type stockInfo struct {
	Name       string
	RateLimit  float32
	Point      int
	RateTotal  decimal.Decimal
	PointTotal int
	StockCount int32
	Reasons    Reasons
	Section    []float32
}

type stockSet struct {
	head      int
	mid       int
	end       int
	marketCap decimal.Decimal
	goodRate  bool //业绩反转
}

type stockSets struct {
	IndustryName string
	stockSets    []stockSet
	//stockInfos                   []stockInfo
	//grossProfitMarginInfo        stockInfo
	//unearnedRevenueAndContract   stockInfo
	//takeReturnOnNetAssetsDiluted stockInfo
	goodStockCount int32
	stockCount     int32
	reportCount    int32
	pointTotal     int
}

func DTO2VO(dtos []TrendDTO, reportPeriod string) ([]TrendVO, error) {
	starttime := time.Now().UnixNano()
	vos := make([]TrendVO, 0)
	var reportCount int32 = 0
	var stockCount int32 = 0
	head := 0 //前年同期 正常情况 head+1 去年同期
	mid := 0  //去年同期
	var industryName []string
	var industryNamePre []string
	sameIndustry := true
	var industryNameFinal string
	var targetPersent float32 = 0.8 //80%个股
	var rateLimit float32 = 30.0    //30%业绩增长 30%头部市值个股
	var ratio float32 = 50.0        //50%个股
	c := make(chan TrendVO, 0)
	sets := make([]stockSet, 0)
	stockSetMap := make(map[string]*stockSets, 0)
	lastYearReportPeriodTime, err := time.ParseInLocation(util.YMD, reportPeriod, time.Local)
	if err != nil {
		return nil, err
	}
	lastYearReportPeriod := lastYearReportPeriodTime.AddDate(-1, 0, 0).Format(util.YMD)
	beforeLYearReportPeriod := lastYearReportPeriodTime.AddDate(-2, 0, 0).Format(util.YMD)
	for i, dto := range dtos {
		if i > 0 {
			now := i - 1
			if i == len(dtos)-1 {
				now = i
			}
			industryName = strings.Split(dto.IndustryName, "-")
			for x := 0; x < 2 && x < util.Max(len(industryNamePre), len(industryName)); x++ {
				if x < len(industryNamePre) {
					if x < len(industryName) {
						sameIndustry = (industryName[x] == industryNamePre[x]) && (industryName[0] == industryNamePre[0])
					} else {
						sameIndustry = false
						break
					}
				} else {
					sameIndustry = false
					break
				}
			}
			if sameIndustry {
				if dtos[now].ReportPeriod.Format(util.YMD) == reportPeriod {
					if dtos[now].PublishDate != nil {
						reportCount = reportCount + 1
					}
					if dtos[head+1].ReportPeriod.Format(util.YMD) == lastYearReportPeriod {
						mid = head + 1
					} else {
						mid = 0
					}
					sets = append(sets, stockSet{head: head, mid: mid, end: now})
				}
				if dtos[now].Code != dto.Code || now == len(dtos)-1 { //下一支股票
					stockCount = stockCount + 1
					head = i
				}
			} else { //下一个行业
				industryNameFinal = strings.Join(industryNamePre[0:util.Min(len(industryNamePre), 2)], "-")
				stockCount = stockCount + 1
				if dtos[now].ReportPeriod.Format(util.YMD) == reportPeriod {
					if dtos[now].PublishDate != nil {
						reportCount = reportCount + 1
					}
					if dtos[head+1].ReportPeriod.Format(util.YMD) == lastYearReportPeriod {
						mid = head + 1
					} else {
						mid = 0
					}
					sets = append(sets, stockSet{head: head, mid: mid, end: now})
				}
				//if stockCount > 0 {
				if stockSetMap[industryNameFinal] != nil {
					stockSetMap[industryNameFinal].stockSets = append(stockSetMap[industryNameFinal].stockSets, sets...)
					stockSetMap[industryNameFinal].stockCount = stockSetMap[industryNameFinal].stockCount + stockCount
					stockSetMap[industryNameFinal].reportCount = stockSetMap[industryNameFinal].reportCount + reportCount
				} else {
					stockSetMap[industryNameFinal] = &stockSets{IndustryName: industryNameFinal, stockSets: sets, reportCount: reportCount, stockCount: stockCount}
				}
				sets = []stockSet{}
				//}
				reportCount = 0
				stockCount = 0
				industryNamePre = industryName
				head = i
			}
			if now == len(dtos)-1 {
				//if len(sets) > 0 {
				industryNameFinal = strings.Join(industryName[0:util.Min(len(industryName), 2)], "-")
				if stockSetMap[industryNameFinal] != nil {
					stockSetMap[industryNameFinal].stockSets = append(stockSetMap[industryNameFinal].stockSets, sets...)
					stockSetMap[industryNameFinal].stockCount = stockSetMap[industryNameFinal].stockCount + stockCount
					stockSetMap[industryNameFinal].reportCount = stockSetMap[industryNameFinal].reportCount + reportCount
				} else {
					stockSetMap[industryNameFinal] = &stockSets{IndustryName: industryNameFinal, stockSets: sets, reportCount: reportCount, stockCount: stockCount}
				}
				sets = []stockSet{}
				//}
				//vos = append(vos, vo)
			}

		} else {
			industryNamePre = strings.Split(dto.IndustryName, "-")
		}
	}
	var wg sync.WaitGroup
	wg.Add(len(stockSetMap))
	for _, v := range stockSetMap {
		go func(stockSets stockSets) {
			defer wg.Done()
			if stockSets.reportCount <= 0 {
				return
			}
			vo := TrendVO{
				Stocks:  []StockTrend{},
				Reasons: []Reasons{},
			}
			var zgb decimal.Decimal
			var nowPrice decimal.Decimal
			/*var marketCap decimal.Decimal
			var marketCapTotal = decimal.Decimal{}*/
			var unearnedRevenueMap map[string]interface{}
			var contractMap map[string]interface{}
			var takeReturnMap map[string]interface{}
			var stockInfos = []stockInfo{
				{
					Name:      "BusinessRevenue",
					RateLimit: 10.0,
					Point:     1,
					Reasons: Reasons{
						Name:    "营收增长",
						Perform: "营业收入平均增长：",
					},
				},
				{
					Name:      "CashFlowPerShare",
					RateLimit: 20.0,
					Point:     1,
					Reasons: Reasons{
						Name:    "经营现金流增长",
						Perform: "经营现金流平均增长：",
					},
				},
				{
					Name:      "NetProfitAfterDed",
					RateLimit: 10.0,
					Point:     1,
					Reasons: Reasons{
						Name:    "扣非净利润增长",
						Perform: "扣非净利润平均增长：",
					},
				},
			}
			grossProfitMarginInfo := stockInfo{
				RateLimit: 10.0,
				Point:     1,
				Reasons: Reasons{
					Name:    "毛利率增长",
					Perform: "毛利率平均增长：",
				},
				Section: []float32{10, 20, 30, 40, 50, 60, 70, 80, 90, 100},
			}
			unearnedRevenueAndContract := stockInfo{
				RateLimit: 30.0,
				Point:     1,
				Reasons: Reasons{
					Name:    "预收账款+合同负债增长",
					Perform: "预收账款+合同负债平均增长：",
				},
			}
			takeReturnOnNetAssetsDiluted := stockInfo{
				RateLimit: 0.0,
				Point:     1,
				Reasons: Reasons{
					Name:    "净资产收益率增长",
					Perform: "净资产收益率平均增长：",
				},
				Section: []float32{5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100},
			}

			for i, s := range stockSets.stockSets {
				//计算总股本
				errScanzgb := zgb.Scan(getRedisValue("stock:zgb", dtos[s.end].Code))
				if errScanzgb != nil {
					fmt.Println(dtos[s.end].Code + " zgb null")
				}
				errScanPrice := nowPrice.Scan(getRedisValue("stock:tick", dtos[s.end].Code))
				if errScanPrice != nil {
					fmt.Println(dtos[s.end].Code + " tick null")
				}
				if errScanzgb == nil && errScanPrice == nil {
					stockSets.stockSets[i].marketCap = zgb.Mul(nowPrice)
				}
				//当前报告期业绩已经公布时
				if dtos[s.end].PublishDate != nil {
					//计算预收账款+合同负债
					err := mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
						return collection.Find(bson.M{
							"code": dtos[s.end].Code,
							"type": "预收款项",
						}).One(&unearnedRevenueMap)
					})
					if err != nil {
						logging.Error(dtos[s.end].Code+"预收款项获取失败：", err.Error())
					}
					err = mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
						return collection.Find(bson.M{
							"code": dtos[s.end].Code,
							"type": "合同负债",
						}).One(&contractMap)
					})
					if err != nil {
						logging.Error(dtos[s.end].Code+"合同负债获取失败：", err.Error())
					}
					TrendCalculateRevenueAndContractPoint(unearnedRevenueMap, contractMap, beforeLYearReportPeriod, lastYearReportPeriod, reportPeriod, &unearnedRevenueAndContract)

					//计算净资产收益率
					err = mongo.GetMongoDB("shares", "F10_finance", func(collection *mgo.Collection) error {
						return collection.Find(bson.M{
							"code": dtos[s.end].Code,
							"type": "净资产收益率-摊薄",
						}).One(&takeReturnMap)
					})
					if err != nil {
						logging.Error(dtos[s.end].Code+"净资产收益率-摊薄获取失败：", err.Error())
					}
					TrendCalculateTakenReturn(takeReturnMap, lastYearReportPeriod, reportPeriod, &takeReturnOnNetAssetsDiluted)
					if s.end-s.head == 5 {
						//计算毛利率
						TrendCalculateGrossProfit(dtos[s.head:s.end+1], &grossProfitMarginInfo)
					}
					if s.mid > 0 {
						//计算评分 插入股票和理由
						TrendCalculatePoint(dtos[s.head], dtos[s.mid], dtos[s.end], stockInfos)

						//计算净利润
						//行业趋势变好 判断
						goodRate, err := CalculateRate(dtos[s.end].NetProfit, dtos[s.mid].NetProfit)
						if err == nil && goodRate.Cmp(decimal.NewFromFloat32(rateLimit)) >= 0 {
							stockSets.goodStockCount++
							stockSets.stockSets[i].goodRate = true
							/*if errScanzgb == nil && errScanPrice == nil {
								marketCap = marketCap.Add(zgb.Mul(nowPrice))
							}*/
							vo.Stocks = append(vo.Stocks, StockTrend{dtos[s.end].Code, dtos[s.end].Name, goodRate.String()})
						}
					}
				}
			}
			for k, v := range stockInfos {
				//行业中80%个股满足加分
				if v.PointTotal > 0 && decimal.NewFromInt32(v.StockCount).Div(decimal.NewFromInt32(stockSets.stockCount)).Cmp(decimal.NewFromFloat32(targetPersent)) > 0 {
					stockSets.pointTotal = stockSets.pointTotal + v.Point
					vo.Reasons = append(vo.Reasons, Reasons{Name: v.Reasons.Name, Perform: v.Reasons.Perform + v.RateTotal.DivRound(decimal.NewFromInt32(v.StockCount), 2).String() + "%"})
				}
				stockInfos[k].StockCount = 0
				stockInfos[k].RateTotal = decimal.Zero
				stockInfos[k].PointTotal = 0
			}

			stockSets.pointTotal = stockSets.pointTotal + StockInfoHandler(&grossProfitMarginInfo, &vo, stockSets.stockCount, targetPersent)
			stockSets.pointTotal = stockSets.pointTotal + StockInfoHandler(&unearnedRevenueAndContract, &vo, stockSets.stockCount, targetPersent)
			stockSets.pointTotal = stockSets.pointTotal + StockInfoHandler(&takeReturnOnNetAssetsDiluted, &vo, stockSets.stockCount, targetPersent)

			//前30%股票数量
			topStockCount := int(math.Ceil(float64(stockSets.stockCount) * 0.3))
			//总市值排序
			sort.Slice(stockSets.stockSets, func(i, j int) bool {
				return stockSets.stockSets[i].marketCap.Cmp(stockSets.stockSets[i].marketCap) > 0
			})
			topGoodCount := 0
			//业绩前30%股票或者50%个股业绩增长超30%才算行业向好
			for n, c := range stockSets.stockSets {
				if !c.goodRate {
					topGoodCount = n + 1
					break
				}
			}
			if !(topGoodCount >= topStockCount || decimal.NewFromInt32(stockSets.goodStockCount).Mul(decimal.NewFromInt32(100)).Div(decimal.NewFromInt32(stockSets.stockCount)).Cmp(decimal.NewFromFloat32(ratio)) >= 0) {
				vo.Stocks = []StockTrend{}
			}
			/*if marketCapTotal.Cmp(decimal.Zero) > 0 {
				//业绩前30%股票或者50%个股业绩增长超30%才算行业向好
				if !(marketCap.Mul(decimal.NewFromInt32(100)).Div(marketCapTotal).Cmp(decimal.NewFromFloat32(rateLimit)) >= 0 ||
					decimal.NewFromInt32(stockSets.goodStockCount).Mul(decimal.NewFromInt32(100)).Div(decimal.NewFromInt32(stockSets.stockCount)).Cmp(decimal.NewFromFloat32(ratio)) >= 0) {
					vo.Stocks = []StockTrend{}
				}
			}*/
			vo.IndustryName = stockSets.IndustryName
			//vo.IndustryInterpretation = dtos[].IndustryInterpretation
			vo.StockCount = stockSets.stockCount
			vo.ReportCount = stockSets.reportCount
			vo.Point = stockSets.pointTotal
			c <- vo
			if time.Duration(time.Now().UnixNano()-starttime) >= 5*time.Second {
				//fmt.Println(stockSets.IndustryName + " take " + time.Duration(time.Now().UnixNano()-starttime).String())
				logging.Warn(stockSets.IndustryName + " take " + time.Duration(time.Now().UnixNano()-starttime).String())
			}
		}(*v)
	}
	go func() {
		wg.Wait()
		close(c)
	}()
	for v := range c {
		vos = append(vos, v)
	}
	return vos, nil
}

func GetCalculate(qo *CaculateQO) ([]crud_service.DTO, int, error) {
	dto := make([]CalculateDTO, 0)
	ret := make([]crud_service.DTO, 0)
	//cnt := 0
	preReportPeriod, _ := time.ParseInLocation(util.YMD, qo.ReportPeriod, time.Local)
	preReportPeriod = preReportPeriod.AddDate(0, 0, 1).AddDate(0, -3, -1)
	orderList := []string{
		"",
		"disclosure_time",
		"CAST(SUBSTRING_INDEX(forecast_proportion,'~',-1) AS DECIMAL(10,2))",
		"CAST(SUBSTRING_INDEX(forecast_net_profit,'~',-1) AS DECIMAL(10,2))",
		"IF(performance_incr_upper IS NULL,performance_incr_lower,performance_incr_upper)",
		"CAST(SUBSTRING_INDEX(broker_proportion,'~',-1) AS DECIMAL(10,2))", "net_profit",
		"net_profit_growth_rate",
		"business_revenue",
		"business_revenue_growth_rate",
		"net_profit_after_ded",
		"a_pre_five",
		"a_pre_three",
		"a_pre_one",
		"a_one",
		"a_three",
		"a_five",
		"f_pre_five",
		"f_pre_three",
		"f_pre_one",
		"f_one",
		"f_three",
		"f_five",
		"auto_cal_rate",
	}

	amountSizeList := []string{
		"",
		"amount_avg >500000000",
		"amount_avg BETWEEN 200000000 AND 500000000",
		"amount_avg <200000000",
	}

	preNetProfitGrowthRateList := []string{
		"",
		"PA.net_profit_growth_rate BETWEEN 30 AND 50",
		"PA.net_profit_growth_rate>50",
	}

	boardList := []string{
		"",
		"left(SR.code, 3) in ('300')", //创业板
		"left(SR.code, 2) in ('00') and left(SR.code, 3) not in ('000')", //中小板
		"left(SR.code, 3) in ('688')",                                    //科创
		"left(SR.code, 2) in ('60')",                                     //沪A
		"left(SR.code, 3) in ('000')",                                    //深A
	}

	subsql := models.DB().Table("u_stock_industry_relationship R").Select("R.stock_id,GROUP_CONCAT(industry_name separator '/') industry_name,IF(industry_interpretation='',NULL,GROUP_CONCAT(CONCAT(industry_name,':',industry_interpretation) separator '/')) industry_interpretation,GROUP_CONCAT(DISTINCT realname separator '/') user_name,CONCAT('#',GROUP_CONCAT(DISTINCT user_id separator '#,#'),'#') user_ids").
		Joins("LEFT JOIN u_performance_industry I ON I.id=R.industry_id").
		Joins("LEFT JOIN b_user U ON U.id=R.user_id").
		Group("R.stock_id")

	amountsql := models.DB().Table("p_stock_tick").Select("stock_code,ROUND(AVG(amount)*1000,2) amount_avg").Where("trade_date BETWEEN ? AND ?", time.Now().AddDate(0, -2, 0).Format(util.YMD), time.Now().Format(util.YMD)).Group("stock_code")

	dbs := models.DB().Table("p_stock_report_period SR").
		Joins("LEFT JOIN (SELECT * FROM u_performance_forecast a WHERE left(a.code, 1) in ('0','3','6') AND LENGTH(a.code)=6 AND a.report_period = '"+qo.ReportPeriod+"' GROUP BY code) F ON F.code=SR.code ").
		Select("SR.*,S.name,GROUP_CONCAT(T.type separator ',') type,F.*,A.*,D.*,E.*,PA.net_profit_growth_rate pre_net_profit_growth_rate,u_stock_industry.head").
		Joins("LEFT JOIN u_performance_edit E ON SR.code=E.code AND E.report_period=SR.report_period").
		Joins("LEFT JOIN u_performance_announcement A ON SR.code=A.code AND A.report_period=SR.report_period").
		Joins("LEFT JOIN u_performance_type T ON T.code=SR.code AND T.report_period=SR.report_period").
		//Joins("LEFT JOIN p_stock_report_period SR ON SR.code=SR.code AND SR.report_period=SR.report_period").
		Joins("LEFT JOIN b_stock S ON S.id=SR.`code`").
		Joins("LEFT JOIN ? D ON D.stock_id=SR.code", subsql.SubQuery()).
		Joins("LEFT JOIN u_performance_announcement PA ON SR.code=PA.code AND PA.report_period=?", preReportPeriod).
		Joins("LEFT JOIN u_stock_industry on u_stock_industry.code=SR.code")

	if amountSizeList[qo.AmountSize] != "" {
		dbs = dbs.Joins("LEFT JOIN ? AM ON AM.stock_code=SR.code", amountsql.SubQuery()).
			Where(amountSizeList[qo.AmountSize])
	}
	dbs = dbs.Where("S.is_b=0 AND S.end_date>?", time.Now()).
		Where("SR.report_period=?", qo.ReportPeriod)
	if qo.DisclosureStartTime != "" {
		dbs = dbs.Where("disclosure_time >= ?", qo.DisclosureStartTime)
	}
	if qo.DisclosureEndTime != "" {
		t, err := time.ParseInLocation(util.YMD, qo.DisclosureEndTime, time.Local)
		if err != nil {
			return nil, 0, err
		}
		dbs = dbs.Where("disclosure_time < ?", t.AddDate(0, 0, 1).Format(util.YMD))
	}

	if qo.Recommend != "" {
		if qo.Recommend == "2" {
			dbs = dbs.Where("recommend = ? OR recommend IS NULL", qo.Recommend)
		} else {
			dbs = dbs.Where("recommend = ?", qo.Recommend)
		}
	}
	if qo.Important != "" {
		dbs = dbs.Where("important = ?", qo.Important)
	}
	if qo.Hurry != "0" && qo.Hurry != "" {
		if qo.Hurry == "1" {
			dbs = dbs.Where("hurry = ? OR hurry IS NULL", qo.Hurry)
		} else {
			dbs = dbs.Where("hurry = ?", qo.Hurry)
		}
	}
	if qo.Type != "" {
		types := strings.Split(qo.Type, ",")
		dbs = dbs.Where("S.id IN ?", models.DB().Select("code").
			Table("u_performance_type").Where("type IN (?)", types).Where("report_period=?", qo.ReportPeriod).SubQuery())
	}
	if qo.UserId > 0 {
		dbs = dbs.Where("user_ids like ?", "%#"+strconv.Itoa(qo.UserId)+"#%")
	}
	if qo.Search != "" {
		qo.Search = "%" + qo.Search + "%"
		dbs = dbs.Where("S.id like ? OR S.name like ?", qo.Search, qo.Search)
	}
	if qo.SearchIndustry != "" {
		qo.SearchIndustry = "%" + qo.SearchIndustry + "%"
		dbs = dbs.Where("industry_name like ?", qo.SearchIndustry)
	}
	if preNetProfitGrowthRateList[qo.PreNetProfitGrowthRate] != "" {
		dbs = dbs.Where(preNetProfitGrowthRateList[qo.PreNetProfitGrowthRate])
	}
	if boardList[qo.Board] != "" {
		dbs = dbs.Where(boardList[qo.Board])
	}
	dbs = dbs.Group("S.id")
	if qo.SortKey > 0 && qo.SortKey < len(orderList) {
		if qo.Direction {
			dbs = dbs.Order(orderList[qo.SortKey] + " ASC")
		} else {
			dbs = dbs.Order(orderList[qo.SortKey] + " DESC")
		}
	} else {
		today := time.Now().Format(util.YMD)
		dbs = dbs.Order("disclosure_time>='" + today + "' DESC,if(disclosure_time>='" + today + "',disclosure_time,0),disclosure_time ASC")
	}
	//dbs.Count(&cnt)
	err := dbs.Find(&dto).Error
	if err != nil {
		return nil, 0, err
	}
	preTenPreCloseMap, err := gredis.HGetAllStringMap(stock.PreTenPreCloseKey)
	nowPriceMap, err := gredis.Clone(setting.RedisSetting.StockDB).HGetAllStringMap("stock:tick")
	for i, v := range dto {
		if preTenPreclose, ok := preTenPreCloseMap[v.Code]; ok {
			if nowPrice, ok := nowPriceMap[v.Code]; ok {
				tenPreCloseD, terr := decimal.NewFromString(preTenPreclose)
				nowPriceD, nerr := decimal.NewFromString(nowPrice)
				dto[i].NearTenIncRate = util.CalculateRate(decimal.NullDecimal{tenPreCloseD, terr == nil}, decimal.NullDecimal{nowPriceD, nerr == nil})
			}
		}
	}
	if qo.SortKey == len(orderList) {
		sort.Slice(dto, func(i, j int) bool {
			return qo.Direction == dto[i].NearTenIncRate.Decimal.LessThan(dto[j].NearTenIncRate.Decimal)
		})
	}
	for _, v := range dto {
		ret = append(ret, v)
	}
	return ret[util.Min(qo.PageSize*(qo.PageNum-1), len(ret)):util.Min(qo.PageSize*qo.PageNum, len(ret))], len(ret), nil
}

func GetCalculateAll(qo *CaculateEPQO) ([]crud_service.DTO, error) {
	dto := make([]CalculateDTO, 0)
	ret := make([]crud_service.DTO, 0)
	preReportPeriod, _ := time.ParseInLocation(util.YMD, qo.ReportPeriod, time.Local)
	preReportPeriod = preReportPeriod.AddDate(0, 0, 1).AddDate(0, -3, -1)
	orderList := []string{
		"",
		"disclosure_time",
		"CAST(SUBSTRING_INDEX(forecast_proportion,'~',-1) AS DECIMAL(10,2))",
		"CAST(SUBSTRING_INDEX(forecast_net_profit,'~',-1) AS DECIMAL(10,2))",
		"IF(performance_incr_upper IS NULL,performance_incr_lower,performance_incr_upper)",
		"CAST(SUBSTRING_INDEX(broker_proportion,'~',-1) AS DECIMAL(10,2))", "net_profit",
		"net_profit_growth_rate",
		"business_revenue",
		"business_revenue_growth_rate",
		"net_profit_after_ded",
		"a_pre_one",
		"a_pre_three",
		"a_pre_five",
		"a_one",
		"a_three",
		"a_five",
		"f_pre_one",
		"f_pre_three",
		"f_pre_five",
		"f_one",
		"f_three",
		"f_five",
		"auto_cal_rate",
	}

	amountSizeList := []string{
		"",
		"amount_avg >500000000",
		"amount_avg BETWEEN 200000000 AND 500000000",
		"amount_avg <200000000",
	}

	preNetProfitGrowthRateList := []string{
		"",
		"PA.net_profit_growth_rate BETWEEN 30 AND 50",
		"PA.net_profit_growth_rate>50",
	}

	boardList := []string{
		"",
		"left(SR.code, 3) in ('300')", //创业板
		"left(SR.code, 2) in ('00') and left(SR.code, 3) not in ('000')", //中小板
		"left(SR.code, 3) in ('688')",                                    //科创
		"left(SR.code, 2) in ('60')",                                     //沪A
		"left(SR.code, 3) in ('000')",                                    //深A
	}

	subsql := models.DB().Table("u_stock_industry_relationship R").Select("R.stock_id,GROUP_CONCAT(industry_name separator '/') industry_name,IF(industry_interpretation='',NULL,GROUP_CONCAT(CONCAT(industry_name,':',industry_interpretation) separator '/')) industry_interpretation,GROUP_CONCAT(DISTINCT realname separator '/') user_name,CONCAT('#',GROUP_CONCAT(DISTINCT user_id separator '#,#'),'#') user_ids").
		Joins("LEFT JOIN u_performance_industry I ON I.id=R.industry_id").
		Joins("LEFT JOIN b_user U ON U.id=R.user_id").
		Group("R.stock_id")

	amountsql := models.DB().Table("p_stock_tick").Select("stock_code,ROUND(AVG(amount)*1000,2) amount_avg").Where("trade_date BETWEEN ? AND ?", time.Now().AddDate(0, -2, 0).Format(util.YMD), time.Now().Format(util.YMD)).Group("stock_code")

	dbs := models.DB().Table("p_stock_report_period SR").
		Joins("LEFT JOIN (SELECT * FROM u_performance_forecast a WHERE left(a.code, 1) in ('0','3','6') AND LENGTH(a.code)=6 AND a.report_period = '"+qo.ReportPeriod+"' GROUP BY code) F ON F.code=SR.code ").
		Select("SR.*,S.name,GROUP_CONCAT(T.type separator ',') type,F.*,A.*,D.*,E.*,PA.net_profit_growth_rate pre_net_profit_growth_rate").
		Joins("LEFT JOIN u_performance_edit E ON SR.code=E.code AND E.report_period=SR.report_period").
		Joins("LEFT JOIN u_performance_announcement A ON SR.code=A.code AND A.report_period=SR.report_period").
		Joins("LEFT JOIN u_performance_type T ON T.code=SR.code AND T.report_period=SR.report_period").
		//Joins("LEFT JOIN p_stock_report_period SR ON SR.code=SR.code AND SR.report_period=SR.report_period").
		Joins("LEFT JOIN b_stock S ON S.id=SR.`code`").
		Joins("LEFT JOIN ? D ON D.stock_id=SR.code", subsql.SubQuery()).
		Joins("LEFT JOIN u_performance_announcement PA ON SR.code=PA.code AND PA.report_period=?", preReportPeriod)

	if amountSizeList[qo.AmountSize] != "" {
		dbs = dbs.Joins("LEFT JOIN ? AM ON AM.stock_code=SR.code", amountsql.SubQuery()).
			Where(amountSizeList[qo.AmountSize])
	}
	dbs = dbs.Where("S.is_b=0 AND S.end_date>?", time.Now()).
		Where("SR.report_period=?", qo.ReportPeriod)
	if qo.DisclosureStartTime != "" {
		dbs = dbs.Where("disclosure_time >= ?", qo.DisclosureStartTime)
	}
	if qo.DisclosureStartTime != "" {
		dbs = dbs.Where("disclosure_time >= ?", qo.DisclosureStartTime)
	}
	if qo.DisclosureEndTime != "" {
		t, err := time.ParseInLocation(util.YMD, qo.DisclosureEndTime, time.Local)
		if err != nil {
			return nil, err
		}
		dbs = dbs.Where("disclosure_time < ?", t.AddDate(0, 0, 1).Format(util.YMD))
	}

	if qo.Recommend != "" {
		if qo.Recommend == "2" {
			dbs = dbs.Where("recommend = ? OR recommend IS NULL", qo.Recommend)
		} else {
			dbs = dbs.Where("recommend = ?", qo.Recommend)
		}
	}
	if qo.Important != "" {
		dbs = dbs.Where("important = ?", qo.Important)
	}
	if qo.Hurry != "0" && qo.Hurry != "" {
		if qo.Hurry == "1" {
			dbs = dbs.Where("hurry = ? OR hurry IS NULL", qo.Hurry)
		} else {
			dbs = dbs.Where("hurry = ?", qo.Hurry)
		}
	}
	if qo.Type != "" {
		types := strings.Split(qo.Type, ",")
		dbs = dbs.Where("S.id IN ?", models.DB().Select("code").
			Table("u_performance_type").Where("type IN (?)", types).Where("report_period=?", qo.ReportPeriod).SubQuery())
	}
	if qo.UserId > 0 {
		dbs = dbs.Where("user_ids like ?", "%#"+strconv.Itoa(qo.UserId)+"#%")
	}
	if qo.Search != "" {
		qo.Search = "%" + qo.Search + "%"
		dbs = dbs.Where("S.id like ? OR S.name like ?", qo.Search, qo.Search)
	}
	if qo.SearchIndustry != "" {
		qo.SearchIndustry = "%" + qo.SearchIndustry + "%"
		dbs = dbs.Where("industry_name like ?", qo.SearchIndustry)
	}
	if preNetProfitGrowthRateList[qo.PreNetProfitGrowthRate] != "" {
		dbs = dbs.Where(preNetProfitGrowthRateList[qo.PreNetProfitGrowthRate])
	}
	if boardList[qo.Board] != "" {
		dbs = dbs.Where(boardList[qo.Board])
	}
	dbs = dbs.Group("S.id")
	if qo.SortKey > 0 && qo.SortKey < len(orderList) {
		if qo.Direction {
			dbs = dbs.Order(orderList[qo.SortKey] + " ASC")
		} else {
			dbs = dbs.Order(orderList[qo.SortKey] + " DESC")
		}
	} else {
		today := time.Now().Format(util.YMD)
		dbs = dbs.Order("disclosure_time>='" + today + "' DESC,if(disclosure_time>='" + today + "',disclosure_time,0),disclosure_time ASC")
	}
	err := dbs.Find(&dto).Error
	if err != nil {
		return nil, err
	}
	preTenPreCloseMap, err := gredis.HGetAllStringMap(stock.PreTenPreCloseKey)
	nowPriceMap, err := gredis.Clone(setting.RedisSetting.StockDB).HGetAllStringMap("stock:tick")
	for i, v := range dto {
		if preTenPreclose, ok := preTenPreCloseMap[v.Code]; ok {
			if nowPrice, ok := nowPriceMap[v.Code]; ok {
				tenPreCloseD, terr := decimal.NewFromString(preTenPreclose)
				nowPriceD, nerr := decimal.NewFromString(nowPrice)
				dto[i].NearTenIncRate = util.CalculateRate(decimal.NullDecimal{tenPreCloseD, terr == nil}, decimal.NullDecimal{nowPriceD, nerr == nil})
			}
		}
	}
	if qo.SortKey == len(orderList) {
		sort.Slice(dto, func(i, j int) bool {
			return qo.Direction == dto[i].NearTenIncRate.Decimal.LessThan(dto[j].NearTenIncRate.Decimal)
		})
	}
	for _, v := range dto {
		ret = append(ret, v)
	}
	return ret, nil
}

var PreTenRecommendKey = "pre_ten:recommend"

type PreTenRecommend struct {
	PreClose      decimal.Decimal `json:"pre_close"`
	RecommendDate time.Time       `json:"recommend_date"`
	PublishDate   *time.Time      `json:"publish_date,omitempty"`
	DayCount      int             `json:"day_count"`
}

func EditPerformanceEdit(io *PerformanceEditIO) error {
	defer func() {
		if r := recover(); r != nil {
			logging.Error("Recovered in EditPerformanceEdit:", r)
		}
	}()
	reportPeriodTime, _ := time.ParseInLocation(util.YMD, io.ReportPeriod, time.Local)
	dto := PerformanceEditDTO{
		models.Simple{},
		io.Code,
		io.ForecastProportion,
		io.BrokerProportion,
		io.BrokerInterpretation,
		io.ForecastNetProfit,
		io.HighLights,
		io.Important,
		io.Recommend,
		//		io.Type,
		io.UserId,
		&reportPeriodTime,
		io.Hurry,
	}
	types := strings.Split(io.Type, ",")
	tx := models.DB().Begin()
	err := models.DB().Table("u_performance_edit").Where("code=? AND report_period=?", io.Code, io.ReportPeriod).First(&dto).Error
	if err == gorm.ErrRecordNotFound {
		err = models.DB().Table("u_performance_edit").Save(&dto).Error
	} else {
		if err != nil {
			tx.Rollback()
			return errors.New("编辑失败，请重试" + err.Error())
		} else {
			err = models.DB().Exec("UPDATE u_performance_edit SET forecast_proportion=?,broker_proportion=?,broker_interpretation=?,forecast_net_profit=?,high_lights=?,important=?,recommend=?,user_id=?,hurry=? "+
				"WHERE code=? AND report_period=?", io.ForecastProportion, io.BrokerProportion, io.BrokerInterpretation, io.ForecastNetProfit, io.HighLights, io.Important, io.Recommend, io.UserId, io.Hurry, io.Code, io.ReportPeriod).Error
		}
	}

	if err != nil {
		tx.Rollback()
		return errors.New("编辑失败，请重试" + err.Error())
	}
	if io.Type != "" {
		err = models.DB().Exec("DELETE FROM u_performance_type WHERE code=? AND report_period=?", io.Code, io.ReportPeriod).Error
		if err != nil {
			tx.Rollback()
			return errors.New("编辑失败，请重试" + err.Error())
		}
		insertSql := "INSERT INTO u_performance_type (code,report_period,type) VALUES("
		for i, t := range types {
			if i > 0 {
				insertSql = insertSql + "),("
			}
			insertSql = insertSql + "'" + io.Code + "','" + io.ReportPeriod + "','" + t + "'"
		}
		insertSql = insertSql + ")"
		err = models.DB().Exec(insertSql).Error
		if err != nil {
			tx.Rollback()
			return errors.New("编辑失败，请重试" + err.Error())
		}
	}
	if io.Recommend == 1 {
		if !gredis.HExists(PreTenRecommendKey+io.ReportPeriod, io.Code) {
			data := PreTenRecommend{}
			tradeDate := common.TradeDate{}
			if !tradeDate.GetDay(time.Now().Format(util.YMD), 0) {
				tradeDate.GetDay(time.Now().Format(util.YMD), 1)
			}
			data.RecommendDate = tradeDate.Time
			publishDateList := make([]*time.Time, 0)
			err = models.DB().Table("u_performance_announcement").Where("code=?", io.Code).Where("report_period=?", io.ReportPeriod).Pluck("publish_date", &publishDateList).Error
			if err != nil {
				tx.Rollback()
				return errors.New("编辑失败，请重试" + err.Error())
			}
			data.PublishDate = publishDateList[0]
			preClose, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString("stock:last_close", io.Code)
			if err != nil {
				tx.Rollback()
				return errors.New("编辑失败，请重试" + err.Error())
			}
			err = data.PreClose.Scan(preClose)
			if err != nil {
				tx.Rollback()
				return errors.New("编辑失败，请重试" + err.Error())
			}
			dataJs, err := json.Marshal(data)
			if err != nil {
				tx.Rollback()
				return errors.New("编辑失败，请重试" + err.Error())
			}
			err = gredis.HSet(PreTenRecommendKey+io.ReportPeriod, io.Code, dataJs)
			if err != nil {
				tx.Rollback()
				return errors.New("编辑失败，请重试" + err.Error())
			}
		}
	} else {
		err = gredis.HDel(PreTenRecommendKey, io.Code)
		if err != nil {
			logging.Error(io.Code + "取消推荐10日监控失败")
		}
	}
	tx.Commit()
	return nil
}

func ImportPerformanceEdit(rows [][]string, uid, reportPeriod string) []string {
	codeN := 0
	nameN := 1
	BPN := 2
	BPN2 := 3
	oneBP := 4 //券商预测上下限一起时列数
	twoBP := 5 //券商预测上下限分开时列数
	var failCodes []string
	var codeList []struct {
		Code string `json:"code"`
		Name string `json:"name"`
	}
	var codeListMap = make(map[string]string, 0)
	var codeNameMap = make(map[string]string, 0)
	err := models.DB().Table("b_stock").Select("DISTINCT id as code,name").Find(&codeList).Error
	if err != nil {
		logging.Error("ImportPerformanceEdit 获取股票代码列表失败")
	}
	if codeList != nil {
		for _, v := range codeList {
			codeListMap[v.Code] = v.Name
			codeNameMap[v.Name] = v.Code
		}
	}
	for _, row := range rows[1:] {
		code := strings.Trim(strings.Split(row[codeN], ".")[0], " ")
		var brokerProportion string
		if len(row) == oneBP {
			row[BPN] = strings.TrimRight(strings.Trim(row[BPN], " "), "-")
			if !strings.Contains(row[BPN], "%") && row[BPN] != "" {
				failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+reportPeriod+" 券商预测单元格格式应设置为百分比保留2位小数")
				continue
			}
			brokerProportion = strings.TrimRight(row[BPN], "%")
		} else if len(row) == twoBP {
			if (!strings.Contains(row[BPN], "%") && row[BPN] != "") || (!strings.Contains(row[BPN2], "%") && row[BPN2] != "") {
				failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+reportPeriod+" 券商预测单元格格式应设置为百分比保留2位小数")
				continue
			}
			if row[BPN] > row[BPN2] {
				row[BPN], row[BPN2] = row[BPN2], row[BPN]
			}
			brokerProportion = strings.TrimRight(row[BPN], "%") + "~" + strings.TrimRight(row[BPN2], "%")
		} else {
			failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+reportPeriod+" 列数不正确")
			continue
		}
		//row[0], row[1] = row[1], code
		if len(codeListMap) > 0 {
			if code != "" {
				if n, ok := codeListMap[code]; ok {
					if row[nameN] != "" {
						if strings.Trim(row[nameN], " ") != n {
							failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+reportPeriod+" 证券代码与股票名不对映")
							continue
						}
					}
				} else {
					failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+reportPeriod+" 错误证券代码")
					continue
				}
			} else {
				if row[nameN] != "" {
					if c, ok := codeNameMap[row[nameN]]; !ok {
						failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+reportPeriod+" 股票名错误")
						continue
					} else {
						code = c
					}
				} else {
					failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+reportPeriod+" 股票和代码缺失")
					continue
				}
			}
		}
		sql := "INSERT INTO  u_performance_edit (code,broker_proportion,high_lights,user_id,report_period)VALUES('"
		sql = sql + code + "','" + brokerProportion + "','" + strings.Join(row[len(row)-1:], "','") + "','" + uid + "','" + reportPeriod + "')"
		sql = sql + " ON DUPLICATE KEY UPDATE broker_proportion=VALUES(broker_proportion),high_lights=VALUES(high_lights),user_id=VALUES(user_id)"
		err := models.DB().Exec(sql).Error
		if err != nil {
			failCodes = append(failCodes, row[codeN]+" "+row[nameN]+" "+reportPeriod+err.Error())
		}
	}
	return failCodes
}

func EditRelationShip(id, industryId int, userId string) error {
	err := models.DB().Table("u_stock_industry_relationship").Where("id=?", id).Updates(map[string]interface{}{
		"industry_id": industryId,
		"user_id":     userId,
	}).Error
	return err
}

func AddRelationShip(industryId int, stockId, userId string) error {
	dto := RelationShipDTO{
		IndustryId: industryId,
		StockId:    stockId,
		UserId:     userId,
	}
	err := models.DB().Table("u_stock_industry_relationship").
		Where("industry_id=? AND stock_id=? and user_id=?", industryId, stockId, userId).
		FirstOrCreate(&dto).Error
	return err
}

func GetUserInfo() ([]UserInfoDTO, error) {
	dto := make([]UserInfoDTO, 0)
	err := models.DB().Table("u_stock_industry_relationship").
		Select("user_id id,U.realname").
		Joins("LEFT JOIN b_user U ON u_stock_industry_relationship.user_id=U.id").
		Where("user_id>0").
		Group("user_id").Find(&dto).Error
	if err != nil {
		return nil, err
	}
	return dto, err
}

func GetPerformanceIndustry(search string) ([]IndustryDTO, error) {
	dto := make([]IndustryDTO, 0)
	dbs := models.DB().Table("u_performance_industry").
		Select("DISTINCT industry_name,id,industry_interpretation,industry")
	if search != "" {
		dbs = dbs.Where("industry_name like ?", "%"+search+"%")
	}
	err := dbs.Find(&dto).Error
	if err != nil {
		return nil, err
	}
	return dto, err
}

func GetPerformanceTrend(qo TrendQO) ([]TrendDTO, int, error) {
	dto := make([]TrendDTO, 0)
	cnt := 0
	now, _ := time.ParseInLocation(util.YMD, qo.ReportPeriod, time.Local)
	thisYear := now.Format(util.YMD)
	lastYear := now.AddDate(-1, 0, 0).Format(util.YMD)
	BeforeLastYear := now.AddDate(-2, 0, 0).Format(util.YMD)
	dbs := models.DB().Table("u_performance_announcement A").
		Select("*").
		Joins("LEFT JOIN (SELECT stock_id,industry_id FROM u_stock_industry_relationship GROUP BY stock_id,industry_id) R ON  A.code=R.stock_id").
		Joins("LEFT JOIN u_performance_industry I ON  I.id=R.industry_id").
		Where("A.report_period BETWEEN ? AND ? OR A.report_period=?", lastYear, thisYear, BeforeLastYear).
		Where("left(A.code, 1) in ('0','3','6') AND LENGTH(A.code)=6").
		Where("R.industry_id IS NOT NULL")
	if qo.IndustrySearch != "" {
		dbs = dbs.Where("industry_name like ?", "%"+qo.IndustrySearch+"%")
	}
	//dbs.Count(&cnt)
	err := dbs.Order("industry_id,code,report_period ASC").
		Find(&dto).Error

	return dto, cnt, err
}

func GetPerformanceTrendUpdateTime() (*time.Time, error) {
	type updateTime struct {
		UpdateTime *time.Time `json:"update_time"`
	}
	t := updateTime{}
	err := models.DB().Raw("SELECT MAX(updated_at) update_time FROM u_performance_announcement WHERE deleted_at IS NULL").Scan(&t).Error
	if err != nil {
		return nil, err
	}
	return t.UpdateTime, nil
}

type ForecastStatisticsDTO struct {
	Code             string     `json:"code"`
	Name             string     `json:"name"`
	Report           string     `json:"report"`
	Dates            string     `json:"dates"`
	LastReportPeriod *time.Time `json:"last_report_period"`
}

type ForecastStatisticsVO struct {
	Code             string `json:"code"`
	Name             string `json:"name"`
	Report           string `json:"report"`
	Dates            string `json:"dates"`
	LastReportPeriod string `json:"last_report_period"`
}

type ForecastStatisticsQO struct {
	SortKey      int    `json:"sort_key" form:"sort_key"`
	Direction    bool   `json:"direction" form:"direction"`
	Search       string `json:"search" form:"search"`
	ReportPeriod string `json:"report_period" form:"report_period" binding:"required"`
	PageNum      int    `json:"page_num" form:"page_num" binding:"gt=0"`
	PageSize     int    `json:"page_size" form:"page_size" binding:"gt=0"`
	Published    *bool  `json:"published" form:"published" binding:"omitempty"`
	Month        string `json:"month" form:"month"`
}

func (dto ForecastStatisticsDTO) DTO2VO() crud_service.VO {
	return &ForecastStatisticsVO{
		dto.Code,
		dto.Name,
		dto.Report,
		dto.Dates,
		util.TimePtr2String(dto.LastReportPeriod, util.YMD),
	}
}

func GetForecastStatistics(qo ForecastStatisticsQO) ([]crud_service.DTO, int, error) {
	thisYear := time.Now().Year()
	dto := make([]ForecastStatisticsDTO, 0)
	res := make([]crud_service.DTO, 0)
	cnt := 0
	sql := "SELECT code,b_stock.`name`,GROUP_CONCAT(CONCAT(num,'次','(',m,'月)') ORDER BY num DESC SEPARATOR '   ') report,GROUP_CONCAT(publish_date ORDER BY publish_date DESC SEPARATOR '  |  ') dates,MAX(num) maxnum,MAX(maxreport_period) last_report_period FROM (SELECT code,DATE_FORMAT(publish_date,'%m') m,COUNT(DATE_FORMAT(publish_date,'%m')) num, GROUP_CONCAT(publish_date ORDER BY publish_date DESC separator '   ') publish_date,MAX(report_period) maxreport_period FROM u_performance_forecast  WHERE report_period IN (?,?,?,?,?) AND publish_date IS NOT NULL "

	if qo.Search != "" {
		sql = sql + " AND (code like '%" + qo.Search + "%' OR name like '%" + qo.Search + "%')"
	}
	sql = sql + " GROUP BY code,m ORDER BY code,report_period DESC) T  LEFT JOIN b_stock ON b_stock.id=T.code GROUP BY code "
	if qo.Published != nil {
		if *qo.Published {
			sql = sql + "HAVING last_report_period='" + strconv.Itoa(thisYear) + "-" + qo.ReportPeriod + "'"
		} else {
			sql = sql + "HAVING last_report_period<'" + strconv.Itoa(thisYear) + "-" + qo.ReportPeriod + "'"
		}
	}
	if qo.SortKey != 0 {
		if qo.Direction {
			sql = sql + " ORDER BY maxnum ASC"
		} else {
			sql = sql + " ORDER BY maxnum DESC"
		}
	}
	dbs := models.DB().Raw(sql, strconv.Itoa(thisYear-4)+qo.ReportPeriod, strconv.Itoa(thisYear-3)+"-"+qo.ReportPeriod, strconv.Itoa(thisYear-2)+"-"+qo.ReportPeriod, strconv.Itoa(thisYear-1)+"-"+qo.ReportPeriod, strconv.Itoa(thisYear)+"-"+qo.ReportPeriod)
	countSql := "SELECT COUNT(*) count FROM ? N"
	if qo.Month != "" {
		countSql = countSql + " where report like '%" + qo.Month + "%'"
	}
	models.DB().Raw(countSql, dbs.SubQuery()).Count(&cnt)
	err := dbs.Find(&dto).Error
	for _, d := range dto {
		if qo.Month != "" {
			if strings.Contains(d.Report, qo.Month) {
				res = append(res, d)
			}
		} else {
			res = append(res, d)
		}
	}
	return res[qo.PageSize*(qo.PageNum-1) : util.Min(qo.PageNum*qo.PageSize, len(res))], cnt, err
}

func GetForecastUpdateTime() (*time.Time, error) {
	type updateTime struct {
		UpdateTime *time.Time `json:"update_time"`
	}
	t := updateTime{}
	err := models.DB().Raw("SELECT MAX(updated_at) update_time FROM u_performance_forecast WHERE deleted_at IS NULL AND publish_date IS NOT NULL").Scan(&t).Error
	if err != nil {
		return nil, err
	}
	return t.UpdateTime, nil
}

type HighLightQO struct {
	Code     string `json:"code" form:"code" binding:"required"`
	PageSize int    `json:"page_size" form:"page_size" binding:"gt=0"`
	PageNum  int    `json:"page_num" form:"page_num" binding:"gt=0"`
}

func GetPerformanceHighLightsHistory(qo *HighLightQO) ([]crud_service.DTO, int, error) {
	dtos := make([]PerformanceEditDTO, 0)
	res := make([]crud_service.DTO, 0)
	cnt := 0
	dbs := models.DB().Table("u_performance_edit").Where("code=?", qo.Code)
	dbs.Count(&cnt)
	err := dbs.Order("report_period desc").
		Find(&dtos).Error
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		res = append(res, dto)
	}
	return res, cnt, nil
}

func GetReportPeriodList() []string {
	list := make([]string, 0)
	for i := 0; i < 6; i++ {
		reportPeriod, _ := util.ReportPeriod(-i)
		list = append(list, reportPeriod)
	}
	return list
}
