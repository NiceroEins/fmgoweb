package event

import (
	"datacenter/models"
	"datacenter/models/performance"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"datacenter/pkg/setting"
	"encoding/json"
	"fmt"
	"testing"
)

func TestSetPerformanceTrace(t *testing.T) {
	setting.Setup()
	models.Setup()
	mongo.Setup()
	gredis.Setup()
	logging.Setup()
	c:=map[string]interface{}{
		"code":             "603208",
		"name":             "江山欧派",
		"type":             "performance",
		"highly_recommend": true,
		"pf_type":          "预告",
		"reason":           "预告" + "涨停上涨",
		"content":          fmt.Sprintf("%s业绩%s披露涨停后连续上涨", "哈森股份", "预告"),

	}
	bt, _ := json.Marshal(c)
	m:=string(bt)
	pd:=Push{
		Content:&m,
	}
	SetPerformanceTrace(&pd,performance.FROM_TRACE)
}
