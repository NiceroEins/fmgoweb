package event

import (
	"datacenter/models"
	"datacenter/pkg/logging"
)

type Event struct {
	models.Simple
	UserID   string `json:"user_id"`
	ObjectID int64  `json:"object_id"`
	EventStr string `json:"event_str"`
	Type     string `json:"type"`
}

func (Event) TableName() string {
	return "u_event"
}

func CreateEvent(event *Event) error {
	if err := models.DB().Create(event).Error; err != nil {
		logging.Error("event.CreateEvent() Errors: ", err.Error())
		return err
	}
	return nil
}

func FetchEvent(userID string, objectID int) []string {
	var ret []string
	if err := models.DB().Model(Event{}).Select("event_str").Where("user_id = ?", userID).Where("object_id = ?", objectID).Pluck("event_str", &ret).Error; err != nil {
		logging.Error("event.FetchEvent() Errors: ", err.Error())
	}
	return ret
}
