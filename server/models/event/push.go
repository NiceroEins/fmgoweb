package event

import (
	"datacenter/models"
	"datacenter/models/performance"
	"datacenter/models/user"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/service/stock_pool_service"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"strconv"
	"strings"
	"time"
)

type PushRequest struct {
	Begin    string `json:"begin" form:"begin"`
	End      string `json:"end" form:"end"`
	From     string `json:":from" form:"from"`
	Content  string `json:"content" form:"content"`
	BMine    bool   `json:"b_mine" form:"b_mine"`
	Type     string `json:"type" form:"type"`
	PageSize int    `json:"size" form:"size"`
	PageNum  int    `json:"n" form:"n"`
}

type Push struct {
	models.Simple
	FromID  string
	ToID    *string
	Content *string
	Type    string
}

func (Push) TableName() string {
	return "u_push"
}

type PushData struct {
	models.Model
	FromID  string                 `json:"from"`
	ToID    string                 `json:"to"`
	Type    string                 `json:"type"`
	Content string                 `json:"content"`
	Name    string                 `json:"name" gorm:"column:realname"`
	Data    map[string]interface{} `json:"data" gorm:"-"`
}

func (PushData) TableName() string {
	return "u_push"
}

func InsertPushData(pd *Push) error {
	if pd.FromID == "system" {
		pd.FromID = "0"
	}
	if err := models.DB().Create(pd).Error; err != nil {
		logging.Error("push.InsertPushData() Errors: ", err.Error())
		return err
	}
	if pd != nil && (pd.Type == "message" || pd.Type == "performance" || pd.Type == "report" || pd.Type == "home_stock_pool" || pd.Type == "event_recommend") && util.Atoi(pd.FromID) > 0 && pd.Content != nil {
		if strings.Index(*pd.Content, "推荐") >= 0 {
			logging.Info("push.InsertPushData() recommend exec")
			go asyncUpdate(pd)
		} else if pd.Type == "event_recommend" {
			logging.Info("push.InsertPushData() recommend exec")
			go asyncUpdate(pd)
		}
		//// 个人中心 股池推荐入考核
		//if pd.Type == "home_stock_pool" {
		//	logging.Info("push.InsertPushData() recommend  home_stock_pool")
		//	go asyncUpdate(pd)
		//}
	}
	if pd != nil && (pd.Type == "performance_stock" || pd.Type == "performance") && pd.Content != nil {
		go SetPerformanceTrace(pd, performance.FROM_TRACE)
	}
	return nil
}

func QueryPushData(pr *PushRequest, uid string) ([]PushData, int) {
	data := make([]PushData, 0)
	cnt := 0
	dbs := models.DB().Model(PushData{}).Select("u_push.*, b_user.realname").Joins("LEFT JOIN b_user ON u_push.from_id = b_user.id").Where("u_push.created_at BETWEEN ? AND ?", pr.Begin, pr.End+" 23:59:59").Order("u_push.created_at DESC")
	if pr.From != "" {
		dbs = dbs.Where("b_user.realname like ?", "%"+pr.From+"%")
	}
	if pr.BMine {
		if user.CheckPrivilege(uid, "/recommend_performance") {
			dbs = dbs.Where("u_push.from_id = ? OR u_push.type IN ('system_performance', 'performance_stock')", uid)
		} else {
			dbs = dbs.Where("u_push.from_id = ?", uid)
		}
	}
	if pr.Type != "" {
		types := strings.Split(pr.Type, ",")
		dbs = dbs.Where("u_push.type in (?)", types)
	}
	if pr.Content != "" {
		dbs = dbs.Where("u_push.content like ?", "%"+pr.Content+"%")
	}
	dbs.Count(&cnt)
	if err := dbs.Offset(pr.PageSize * (pr.PageNum - 1)).Limit(pr.PageSize).Find(&data).Error; err != nil {
		logging.Error("push.QueryPushData() Errors: ", err.Error())
	}
	var ret []PushData
	for _, v := range data {
		p := unmarshal(&v)
		if p != nil {
			ret = append(ret, *p)
		}
	}
	return ret, cnt
}

type StockShare struct {
	Reason  string
	OwnerID string
	Creator string
	Status  string
	Updated string
	Created string
	Urgent  int
}

type StockShareNew struct {
	models.Simple
	Reason  string
	OwnerID string
	Creator string
	Status  string
	Urgent  int
	Files   string
}

func asyncUpdate(pd *Push) {
	db := models.DB()
	var username []string
	if err := db.Table("b_user").Select("username").Where("id = ?", pd.FromID).Pluck("username", &username).Error; err != nil || len(username) == 0 || username[0] == "" {
		return
	}
	//un := username[0]
	data := make(map[string]interface{})
	err := json.Unmarshal([]byte(*pd.Content), &data)
	if err != nil {
		logging.Error("event.asyncUpdate() unmarshal content Errors: ", err.Error())
		return
	}
	title, ok := util.GetString(data, "title")
	if !ok || title == "" {
		title, ok = util.GetString(data, "content")
		if !ok || title == "" {
			return
		}
	}

	if pd.Type == "message" {
		if user.CheckPrivilege(pd.FromID, "/push_stock_pool") {
			logging.Info("event.asyncUpdate() exec")
			stock_pool_service.GenRecommendStock(title, pd.FromID)
		}
		//asyncUpdateOld(title, un)
	}

	var files []string
	ok, urls := util.GetArray(data, "urlList")
	//logging.Info("event.asyncUpdate() urlList: ", urls)
	if ok {
		for _, k := range urls {
			w, ok := k.(map[string]interface{})
			if ok {
				v, ok := util.GetString(w, "url")
				if ok {
					files = append(files, v)
				}
			}
		}
	}

	// 个人中心股池推送
	if pd.Type == "home_stock_pool" {
		name, ok := util.GetString(data, "name")
		if !ok || name == "" {
			return
		}
		title = "推荐：" + name + "," + title
	}

	if err := models.DB().Table("p_stock_share").Create(&StockShareNew{
		Reason:  title,
		OwnerID: pd.FromID,
		Creator: pd.FromID,
		Status:  "normal",
		Urgent:  1,
		Files:   strings.Join(files, ","),
	}).Error; err != nil {
		logging.Error("event.asyncUpdate() update p_stock_share_new Errors: ", err.Error())
		return
	}

}

func asyncUpdateOld(title, un string) {
	dbs, err := gorm.Open(setting.DatabaseSetting.Type, fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		"root",
		"82h^f8&8#2by(7",
		"rm-bp1abk8tbln4c8y129o.mysql.rds.aliyuncs.com:3306",
		"friday"))

	if err != nil {
		logging.Error("event.asyncUpdateOld() dial remote sql Errors: ", err.Error())
		return
	}

	var id []int
	if err := dbs.Table("b_user").Select("id").Where("username = ?", un).Pluck("id", &id).Error; err != nil || len(id) == 0 || id[0] == 0 {
		return
	}

	if err := dbs.Table("p_stock_share").Create(&StockShare{
		Reason:  title,
		OwnerID: strconv.Itoa(id[0]),
		Creator: strconv.Itoa(id[0]),
		Status:  "normal",
		Updated: time.Now().Format("2006-01-02 15:04:05"),
		Created: time.Now().Format("2006-01-02 15:04:05"),
		Urgent:  1,
	}).Error; err != nil {
		logging.Error("event.asyncUpdate() update p_stock_share Errors: ", err.Error())
		return
	}
}

func unmarshal(pd *PushData) *PushData {
	data := make(map[string]interface{})
	if err := json.Unmarshal([]byte(pd.Content), &data); err != nil {
		logging.Error("push.unmarshal() Errors: ", err.Error())
		return nil
	}
	pd.Data = data
	return pd
}

func SetPerformanceTrace(pd *Push, from string) {
	logging.Info("业绩跟踪数据录入")
	if pd == nil || pd.Content == nil {
		logging.Error("业绩跟踪推送数据为空")
		return
	}
	type content struct {
		Code       string `json:"code"`
		Name       string `json:"name"`
		Reason     string `json:"reason"`
		PublishKey string `json:"publish_key"`
		PfType     string `json:"pf_type"`
	}
	type objId struct {
		Id           int       `json:"id"`
		ReportPeriod time.Time `json:"report_period"`
	}
	data := content{}
	oids := make([]objId, 0)
	err := json.Unmarshal([]byte(*pd.Content), &data)
	if err != nil {
		logging.Error("业绩跟踪数据反序列化报错：", *pd.Content)
		return
	}

	if data.Code == "" || data.Name == "" || data.Reason == "" {
		logging.Info("未指定个股传入")
		return
	}
	io := performance.TraceIO{
		Code:   data.Code,
		Name:   data.Name,
		Reason: data.Reason,
		From:   from,
	}
	table := ""
	switch data.PfType {
	case "公告":
		io.Type = "announcement"
		table = "u_performance_announcement"
		break
	case "预告":
		io.Type = "forecast"
		table = "u_performance_forecast"
		break
	default:
		logging.Error("SetPerformanceTrace 输入非法类型：", *pd.Content)
		return

	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	price, err := conn.HGetString("stock:tick", data.Code)
	if err != nil {
		logging.Error("业绩跟踪获取"+data.Code+"当前价格报错：", err.Error())
	} else {
		err := io.Price.Decimal.Scan(price)
		if err == nil {
			io.Price.Valid = true
		} else {
			logging.Error("解析"+data.Code+"当前价"+price+"报错", err.Error())
		}
	}
	indexCode := util.GetIndexFromCode(data.Code, true)
	indexPrice, err := conn.HGetString("stock:ts_tick", indexCode)
	if err != nil {
		logging.Error("业绩跟踪获取"+data.Code+"当前对应指数价格报错：", err.Error())
	} else {
		err := io.IndexPrice.Decimal.Scan(indexPrice)
		if err == nil {
			io.IndexPrice.Valid = true
		}
	}
	//publishDate, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString("performance:publish_date", data.Code)
	publishDate, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString(data.PublishKey+":publish_date", data.Code)
	ioStr, _ := json.Marshal(io)
	if err != nil {
		logging.Error("获取"+data.Code+"披露日期失败", string(ioStr))
		return
	}
	err = models.DB().Table(table).Where("code=?", data.Code).Where("publish_date=?", publishDate).Find(&oids).Error
	if err != nil {
		logging.Error("业绩跟踪获取对应"+data.PfType+"失败 code:"+data.Code+" 公告日期："+publishDate, err.Error())
		return
	}
	if len(oids) <= 0 {
		logging.Error("业绩跟踪获取"+publishDate+"报告期失败", string(ioStr))
	}
	for _, oid := range oids {
		io.ObjId = oid.Id
		io.ReportPeriod = oid.ReportPeriod.Format(util.YMD)
		oid.Id = 0 //gorm id非空会自动加where条件 所以清空
		err = models.DB().Table("p_push_trace").Where(map[string]interface{}{
			"code":          io.Code,
			"type":          io.Type,
			"reason":        io.Reason,
			"obj_id":        io.ObjId,
			"report_period": oid.ReportPeriod,
		}).First(&oid).Error
		if oid.Id <= 0 || err != nil {
			err = models.DB().Table("p_push_trace").Create(&io).Error
			if err != nil {
				logging.Error("业绩跟踪 插入p_push_trace失败", string(ioStr), err.Error())
			}
		}
	}
}
