package event

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"github.com/pkg/errors"
	"time"
)

type DataType string
type DateType string

const (
	Day          DateType = "day"
	ReportPeriod DateType = "report_period"
)

const (
	INT    DataType = "int"
	STRING DataType = "string"
	FLOAT  DataType = "decimal"
	BIGINT DataType = "bigint"
)

func GetCreateSqlFromType(eventType string, dataType DataType) string {
	ret := "`" + eventType + "`"
	switch dataType {
	case STRING:
		return ret + " varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL,"
	case INT:
		return ret + " int(11) DEFAULT NULL,"
	case BIGINT:
		return ret + " bigint(20) DEFAULT NULL,"
	case FLOAT:
		return ret + " decimal(16,2) DEFAULT NULL,"
	default:
		return ret + " varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL,"
	}
}

type StockEventDTO struct {
	Id        int64     `json:"id"`
	Code      string    `json:"code"`
	Date      time.Time `json:"date"`
	DateType  DateType  `json:"date_type"`
	Type      string    `json:"type"`
	Data      string    `json:"data"`
	DataType  DataType  `json:"data_type"`
	CreatedAt time.Time `json:"created_at"`
}

type StockEventIO struct {
	Code     string   `json:"code" form:"code"`
	Date     string   `json:"date" form:"date"`
	DateType DateType `json:"date_type" form:"date_type"`
	Type     string   `json:"type" form:"type"`
	Data     string   `json:"data" form:"data"`
	DataType DataType `json:"data_type" form:"data_type"`
}

func SetStockEvent(io StockEventIO) error {
	dto := StockEventDTO{}
	if io.Code == "" || io.Data == "" || io.DataType == "" || io.Type == "" || io.Date == "" || io.DateType == "" {
		logging.Error("数据不完整", io)
		return errors.New("数据不完整")
	}
	err := models.DB().Table("b_stock_event").
		Where("code=? and date=? and date_type=? and type=?", io.Code, io.Date, io.DateType, io.Type).
		First(&dto).Error
	if dto.Id > 0 {
		logging.Error("SetStockEvent 数据冲突", dto, err.Error())
		return errors.New("数据存在冲突")
	}
	err = models.DB().Table("b_stock_event").Create(io).Error
	if err != nil {
		logging.Error("b_stock_event 录入报错", io, err.Error())
	}
	return err
}
