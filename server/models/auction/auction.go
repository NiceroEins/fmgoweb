package auction

import (
	"time"
)

type AuctionListRequest struct {
	PageSize  int       `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum   int       `json:"page_num" form:"page_num" binding:"required,gte=1"`
	MaxID     int       `json:"max_id" form:"max_id"`
	Status    int       `json:"status" form:"status"`   // 拍卖状态 0:预告中,1:开拍中,2:已结束/中止 3:全部
	Start     time.Time `json:"start" form:"start"  time_format:"2006-01-02"`
	End       time.Time `json:"end" form:"end"  time_format:"2006-01-02"`
	Platform  string    `json:"platform" form:"platform"`
	Title     string    `json:"title" form:"title"`
}

type AuctionDealRequest struct {
	Id  int64       `json:"id" form:"id" binding:"required,gte=1"`
}