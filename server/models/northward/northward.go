package northward

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"reflect"
	"sort"
	"time"
)

type DTO interface {
	NewDTOList()
}
type VO interface {
	DTO2VO(dto DTO)
}

type DTOList []NorthWardDTO

type VOList []NorthWardVO

type NorthWardQO struct {
	Search    string `json:"search" form:"search"`
	SortKey   int    `json:"sort_key" form:"sort_key"`
	Direction bool   `json:"direction" form:"direction"`
	models.Paginate
}

type NorthWardRow struct {
	Code      string          `json:"code"`
	Date      time.Time       `json:"date"`
	TotalHold decimal.Decimal `json:"total_hold"`
	AddHold   decimal.Decimal `json:"add_hold"`
	CreatedAt time.Time       `json:"created_at"`
	UpdatedAt time.Time       `json:"updated_at"`
}

type NorthWardDTO struct {
	Code           string          `json:"code"`
	Name           string          `json:"name"`
	TotalHold      decimal.Decimal `json:"total_hold"`
	AddHold        decimal.Decimal `json:"add_hold"`
	Rate           decimal.Decimal `json:"rate"`
	AddHoldRate    decimal.Decimal `json:"add_hold_rate"`
	PreFiveAddHold decimal.Decimal `json:"pre_five_add_hold"`
	PreTenAddHold  decimal.Decimal `json:"pre_ten_add_hold"`
	Close          decimal.Decimal `json:"close"`
	Zsz            decimal.Decimal `json:"zsz"`
	CreatedAt      time.Time       `json:"created_at"`
	UpdatedAt      time.Time       `json:"updated_at"`
	NearData       []NorthWardRow  `json:"near_data" gorm:"FOREIGNKEY:Code;ASSOCIATION_FOREIGNKEY:Code"`
}

type NorthWardVO struct {
	Code           string `json:"code"`
	Name           string `json:"name"`
	AddHold        string `json:"add_hold"`
	TotalHold      string `json:"total_hold"`
	PreFiveAddHold string `json:"pre_five_add_hold"`
	PreTenAddHold  string `json:"pre_ten_add_hold"`
	Rate           string `json:"rate"`
	AddHoldRate    string `json:"add_hold_rate"`
	Zsz            string `json:"zsz"`
	CreatedAt      string `json:"created_at"`
	UpdatedAt      string `json:"updated_at"`
}

func (vo *NorthWardVO) DTO2VO(d NorthWardDTO) {
	vo.Code = d.Code
	vo.Name = d.Name
	vo.UpdatedAt = d.UpdatedAt.Format(util.YMDHMS)
	vo.CreatedAt = d.CreatedAt.Format(util.YMD)
	vo.TotalHold = d.TotalHold.String()
	vo.AddHold = d.AddHold.String()
	vo.Rate = d.Rate.String()
	vo.AddHoldRate = d.AddHoldRate.String()
	vo.Zsz = d.Zsz.String()
	vo.PreFiveAddHold = d.PreFiveAddHold.String()
	vo.PreTenAddHold = d.PreTenAddHold.String()
}

func (list *VOList) DTOList2VOList(dtos DTOList) {
	for _, dto := range dtos {
		vo := NorthWardVO{}
		vo.DTO2VO(dto)
		*list = append(*list, vo)
	}
}

func NewDTOList() DTOList {
	return make([]NorthWardDTO, 0)
}

func NewVOList() VOList {
	return make([]NorthWardVO, 0)
}

func (l *DTOList) GetList(qo *NorthWardQO) (int, error) {
	preFive := 5
	preTen := 10
	sortArray := []string{
		"",
		"AddHold",
		"AddHoldRate",
		"TotalHold",
		"Rate",
		"Zsz",
		"PreFiveAddHold",
		"PreTenAddHold",
	}
	dbs := models.DB().Table("p_stock_northward").
		Select("p_stock_northward.code,b_stock.name,p_stock_northward.total_hold,p_stock_northward.add_hold,p_stock_northward.created_at,p_stock_northward.updated_at,p_stock_tick.close").
		Preload("NearData", func(db *gorm.DB) *gorm.DB {
			return models.DB().Table("(select *,row_number() over(partition by code order by date desc) as n from p_stock_northward) a").
				Where("n<=?", preTen).
				Where("date<=(select max(date) date from p_stock_northward)").
				Order("code desc,date desc")
		}).
		Joins("LEFT JOIN b_stock ON p_stock_northward.code=b_stock.id").
		Joins("LEFT JOIN p_stock_tick ON p_stock_tick.stock_code=p_stock_northward.code and p_stock_tick.trade_date=p_stock_northward.date").
		Where("date = (select max(date) date from p_stock_northward)")
	if qo.Search != "" {
		dbs = dbs.Where("code like ? OR b_stock.name like ?", "%"+qo.Search+"%", "%"+qo.Search+"%")
	}
	err := dbs.Order("code desc").Find(&l).Error
	if err != nil {
		logging.Error("获取北向资金列表失败", err.Error())
		return 0, err
	}
	ltgMap, err := gredis.Clone(setting.RedisSetting.StockDB).HGetAllStringMap("stock:ltg")
	if err != nil {
		logging.Error("获取ltgmap失败", err.Error())
	}
	for i, _ := range *l {
		for j, _ := range (*l)[i].NearData {
			if j < preFive {
				(*l)[i].PreFiveAddHold = (*l)[i].PreFiveAddHold.Add((*l)[i].NearData[j].AddHold)
			}
			if j < preTen {
				(*l)[i].PreTenAddHold = (*l)[i].PreTenAddHold.Add((*l)[i].NearData[j].AddHold)
			}
		}
		//ltgStr, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString("stock:ltg", (*l)[i].Code)

		if ltgStr, ok := ltgMap[(*l)[i].Code]; ok {
			ltg := decimal.Decimal{}
			err = ltg.Scan(ltgStr)
			if err != nil {
				logging.Error("解析"+(*l)[i].Code+"ltg"+ltgStr+"失败", err.Error())
			} else {
				(*l)[i].Rate = (*l)[i].TotalHold.Mul(decimal.NewFromInt(100)).DivRound(ltg, 2)
				(*l)[i].Zsz = (*l)[i].Close.Mul(ltg)
				(*l)[i].AddHoldRate = (*l)[i].AddHold.Mul(decimal.NewFromInt(100)).DivRound(ltg, 2)
			}
		} else {
			logging.Error("获取" + (*l)[i].Code + "ltg失败")
		}

	}
	if qo.SortKey > 0 && qo.SortKey < len(sortArray) {
		sort.Slice(*l, func(i, j int) bool {
			return qo.Direction == util.Compare(reflect.ValueOf((*l)[i]).FieldByName(sortArray[qo.SortKey]), reflect.ValueOf((*l)[j]).FieldByName(sortArray[qo.SortKey]))
		})
	}
	cnt := len(*l)
	*l = (*l)[util.Min((qo.PageNum-1)*qo.PageSize, len(*l)):util.Min((qo.PageNum)*qo.PageSize, len(*l))]
	return cnt, nil
}

func GetLatestTime() (time.Time, error) {
	updatedAt := make([]time.Time, 0)
	err := models.DB().Table("p_stock_northward").Pluck("Max(date)", &updatedAt).Error
	if err != nil {
		logging.Error("获取p_stock_northward最新时间失败", err.Error())
		return time.Time{}, err
	}
	if len(updatedAt) != 1 {
		return time.Time{}, errors.New("获取p_stock_northward最新时间异常")
	}
	return updatedAt[0], nil
}
