package user

import (
	"datacenter/models"
	"fmt"
	"github.com/jinzhu/gorm"
)

type Profile struct {
	models.Simple
	UserID string `json:"user_id"`
	Data   string `json:"data" form:"user_id" gorm:"column:profile"`
}

func (Profile) TableName() string {
	return "b_user_profile"
}

func UpdateProfile(profile *Profile) error {
	if profile == nil || profile.UserID == "" {
		return fmt.Errorf("wrong param")
	}
	err := models.DB().Where(&Profile{
		UserID: profile.UserID,
	}).Assign(profile).FirstOrCreate(&Profile{}).Error
	return err
}

func QueryProfile(userID string) (Profile, error) {
	var ret Profile
	if err := models.DB().Where("user_id = ?", userID).First(&ret).Error; err != nil && err != gorm.ErrRecordNotFound {
		//logging.Error("profile.QueryProfile() Errors: ", err.Error())
		return ret, err
	} else {
		if err == gorm.ErrRecordNotFound {
			ret = Profile{
				UserID: userID,
				Data:   "[]",
			}
		}
	}
	return ret, nil
}
