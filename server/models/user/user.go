package user

import (
	"datacenter/models"
	"datacenter/models/template"
	"datacenter/pkg/gredis"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	user_service "datacenter/service/user_service"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// BPrivilege 权限表
type BPrivilege struct {
	ID                   int    `gorm:"primary_key;column:id;type:int(10) unsigned;not null"`
	PrivilegeName        string `gorm:"column:privilege_name;type:varchar(255)" json:"privilege_name,omitempty" ` // 权限
	PrivilegeEnglishName string `json:"privilege_english_name"`                                                   //权限英文名称
	ParentID             string `gorm:"column:parent_id;type:varchar(255)" json:"parent_id,omitempty"`            // 父级权限id(0 顶级)
	Level                int8   `gorm:"column:level;type:tinyint(4)" json:"level,omitempty"`                      // 层级( 0 顶级，1 一级,2 二级)
	Router               string `gorm:"column:router;type:varchar(255)" json:"router,omitempty"`                  // 权限路由
	DelayPushTime        *int   `json:"delay_push_time"`                                                          //延迟时间
	Type                 int    `json:"type"`                                                                     //类型(页面-0，功能-1)
}

// BTemplate 权限模版表
type BTemplate struct {
	ID           int    `gorm:"primary_key;column:id;type:int(10) unsigned;not null" json:"-"`
	TemplateName string `gorm:"column:template_name;type:varchar(255)" json:"template_name"` // 权限模版名称
	PrivilegeIDs string `gorm:"column:privilege_ids;type:varchar(255)" json:"privilege_ids"` // 权限id(权限表主键id)
}

// BUserPrivilegeRelationship 用户与权限关系表
type BUserPrivilegeRelationship struct {
	ID            int  `gorm:"primary_key;column:id;type:int(10) unsigned;not null" json:",omitempty"`
	UserID        int  `gorm:"column:user_id;type:int(10) unsigned" json:"user_id"`           // 用户id
	PrivilegeID   int  `gorm:"column:privilege_id;type:int(10) unsigned" json:"privilege_id"` // 权限id
	DelayPushTime *int `json:"delay_push_time"`                                               //延迟推送时间(秒)
}

func AddUser(user_name, real_name, mobile, privilege string) (success bool, err error) {
	var (
		b_u template.BUser
	)
	timeNow, _ := util.GenDateFormatTime()
	privilegeArray := strings.Split(privilege, ",")

	b_u.Username = user_name
	b_u.Realname = real_name
	b_u.Mobile = mobile
	b_u.Pwd = util.EncodeMD5(setting.AppSetting.DefaultPwd)
	b_u.CreatedAt = timeNow
	b_u.UpdatedAt = timeNow
	b_u.Status = "normal"
	b_u.LastLoginTime = nil

	tx := models.DB().Begin()
	err = tx.Table("b_user").Create(&b_u).Error
	if err != nil {
		tx.Rollback()
		return false, fmt.Errorf("用户名重复")
	}
	fmt.Printf("b_u.id = %v", b_u.ID)
	for _, v := range privilegeArray {
		var b_u_p_r BUserPrivilegeRelationship
		b_u_p_r.UserID = int(b_u.ID)
		middleSlice := strings.Split(v, "-")
		if len(middleSlice) == 1 {
			b_u_p_r.PrivilegeID, _ = strconv.Atoi(middleSlice[0])
		} else {
			b_u_p_r.PrivilegeID, _ = strconv.Atoi(middleSlice[0])
			delayPushTime, _ := strconv.Atoi(middleSlice[1])
			b_u_p_r.DelayPushTime = &delayPushTime
		}
		err = tx.Table("b_user_privilege_relationship").Create(&b_u_p_r).Error
		if err != nil {
			tx.Rollback()
			return false, err
		}
	}
	err = tx.Commit().Error
	if err != nil {
		tx.Rollback()
		return false, err
	} else {
		return true, err
	}
}

func GetUserList(user_name, real_name, mobile string, page, page_size int) (res []template.BUser, total int, err error) {
	dbs := models.DB().Table("b_user").Select("id,username,realname,mobile,status,ip_address,last_login_time,created_at")
	if user_name != "" {
		//账号搜索
		dbs = dbs.Where("username = ?", user_name)
	}
	if real_name != "" {
		//姓名搜索
		dbs = dbs.Where("realname = ?", real_name)
	}
	if mobile != "" {
		//手机号搜索
		dbs = dbs.Where("mobile = ?", mobile)
	}
	dbs.Where("deleted_at is null").Count(&total)
	dbs = dbs.Limit(page_size).Offset((page - 1) * page_size)
	err = dbs.Find(&res).Error

	if err != nil {
		return res, 0, err
	} else {
		return
	}
}

func EditStatus(p user_service.EditStatusRequest) (success bool, err error) {
	err = models.DB().Table("b_user").Select("status").Where("id = ?", p.UserId).Updates(map[string]interface{}{
		"status": p.Status,
	}).Error
	if err != nil {
		return false, fmt.Errorf("更改用户状态失败")
	}
	return true, nil
}

func EditPwd(p user_service.EditPwdRequest) (bool, error) {
	err := models.DB().Table("b_user").Select("pwd").Where("id = ?", p.UserId).Updates(map[string]interface{}{
		"pwd": util.EncodeMD5(setting.AppSetting.DefaultPwd),
	}).Error
	if err != nil {
		return false, fmt.Errorf("重置密码失败")
	}
	return true, nil
}

func DelUser(p user_service.EditPwdRequest) (bool, error) {
	err := models.DB().Table("b_user").Select("deleted_at").Where("id = ?", p.UserId).Updates(map[string]interface{}{
		"deleted_at": util.GenDateFormat(),
	}).Error
	if err != nil {
		return false, fmt.Errorf("删除用户失败")
	}
	return true, nil
}

func EditUser(p user_service.EditUserRequest) (bool, error) {
	privilegeArray := strings.Split(p.Privilege, ",")
	err := models.DB().Table("b_user").Select("realname,mobile").Where("id = ?", p.UserId).Updates(map[string]interface{}{
		"realname": p.RealName,
		"mobile":   p.Mobile,
	}).Error
	if err != nil {
		return false, fmt.Errorf("修改用户信息失败")
	}
	models.DB().Table("b_user_privilege_relationship").Select("deleted_at").Where("user_id = ?", p.UserId).Updates(map[string]interface{}{
		"deleted_at": util.GenDateFormat(),
	})
	if len(privilegeArray) != 0 {
		for _, v := range privilegeArray {
			var b_u_p_r BUserPrivilegeRelationship
			b_u_p_r.UserID = p.UserId
			provilegeSlice := strings.Split(v, "-") //处理延迟推送
			if len(provilegeSlice) == 1 {
				b_u_p_r.PrivilegeID, _ = strconv.Atoi(provilegeSlice[0])
			} else {
				//有延迟推送设置
				b_u_p_r.PrivilegeID, _ = strconv.Atoi(provilegeSlice[0])
				var delayPushTime int
				delayPushTime, _ = strconv.Atoi(provilegeSlice[1])
				b_u_p_r.DelayPushTime = &delayPushTime
			}
			err = models.DB().Table("b_user_privilege_relationship").Create(&b_u_p_r).Error
			if err != nil {
				return false, err
			}

		}
	}
	// delete permission cache
	_, _ = gredis.Delete(generateCache(strconv.Itoa(p.UserId)))

	return true, nil
}

func EditUserPwd(p user_service.EditUserPwdRequest) (bool, error) {
	var b_u template.BUser
	models.DB().Table("b_user").Select("pwd").Where("id = ?", p.UserId).Find(&b_u)
	if b_u.Pwd != p.OldPwd {
		return false, fmt.Errorf("旧密码不对")
	}
	err := models.DB().Table("b_user").Select("pwd").Where("id = ?", p.UserId).Updates(map[string]interface{}{
		"pwd": p.NewPwd,
	}).Error
	if err != nil {
		return false, fmt.Errorf("修改新密码失败")
	}
	return true, nil
}

func UserStatus(user_id string) (string, *time.Time) {
	var b_u template.BUser
	models.DB().Table("b_user").Unscoped().Select("status,deleted_at").Where("id = ?", user_id).Find(&b_u)
	return b_u.Status, b_u.DeletedAt
}
