package user

import (
	"datacenter/models"
	"datacenter/models/template"
	"datacenter/pkg/util"
	"fmt"
)

type Auth struct {
	ID        int    `gorm:"primary_key" json:"id"`
	Realname  string `json:"realname"`
	DeletedAt string `json:"deleted_at"`
}

func Check(username, pwd, ip string) (int, string, bool, error) {
	userId, realName, success, err := CheckAuth(username, pwd)
	if err != nil {
		return 0, "", false, err
	}
	//记录登陆ip地址,以及最后登陆时间
	timeNow := util.GenDateFormat()
	models.DB().Table("b_user").Where("username = ?", username).Updates(map[string]interface{}{
		"ip_address":      ip,
		"last_login_time": timeNow,
	})
	return userId, realName, success, nil
}

func CheckAuth(username, pwd string) (int, string, bool, error) {
	var b_u Auth

	err := models.DB().Table("b_user").Unscoped().Select("id,realname,deleted_at").Where(template.BUser{Username: username, Pwd: pwd}).First(&b_u).Error
	if b_u.DeletedAt != "" {
		return 0, "", false, fmt.Errorf("用户已被删除")
	}
	if err != nil {
		return 0, "", false, fmt.Errorf("用户名或密码错误")
	}

	if b_u.ID > 0 {
		return b_u.ID, b_u.Realname, true, nil
	}
	return 0, "", false, nil
}
