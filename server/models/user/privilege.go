package user

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"fmt"
	"strconv"
	"sync"
	"time"
)

type GetPrivilegeListResponse struct {
	Data []DataTotal `json:"data"`
}

type DataTotal struct {
	Name         string       `json:"name"`
	ID           int          `json:"id"`
	PermissionId string       `json:"permissionId"`
	Children     []BPrivilege `json:"children"`
}
type Router struct {
	Cnt    int    `json:"cnt"`
	Router string `json:"router"`
}

func GetPrivilegeList(user_id int) (resp []DataTotal, err error) {
	var (
		res       []BPrivilege
		b_u_p_r   []BUserPrivilegeRelationship
		g_p_l_res GetPrivilegeListResponse
		dataTotal DataTotal
		ids       []int
		delayIds  []BPrivilege
	)

	delayPushTime := []int{} //需要延时推送的权限id
	models.DB().Table("b_privilege").Select("id").Where("parent_id = ?", 102).Find(&delayIds)
	for _, v := range delayIds {
		delayPushTime = append(delayPushTime, v.ID)
	}

	if user_id == 0 {
		//获取所有
		//获取顶级
		err = models.DB().Table("b_privilege").Select("id,privilege_name,router,privilege_english_name").Where("level = ?", 0).Order("sort asc").Find(&res).Error
		if err != nil {
			return
		}
	} else {
		//获取当前用户所有的权限
		err = models.DB().Table("b_user_privilege_relationship").Select("user_id,privilege_id,delay_push_time").Where("user_id = ?", user_id).Where("deleted_at is null").Find(&b_u_p_r).Error
		if err != nil {
			return
		}
		//fmt.Printf("b_u_p_r:%#v\n", b_u_p_r)

		for _, v := range b_u_p_r {
			ids = append(ids, v.PrivilegeID)
		}

		//该用户的顶级权限
		err = models.DB().Table("b_privilege").Select("id,privilege_name,router,privilege_english_name").Where("level = ?", 0).Where("id in (?)", ids).Order("sort asc").Find(&res).Error
		if err != nil {
			return
		}

	}
	fmt.Printf("res:%#v\n", res)
	for _, v := range res {
		var resFirst []BPrivilege
		//获取一级
		dbs := models.DB().Table("b_privilege").Select("id,privilege_name,router,privilege_english_name").Where("level = ?", 1).Where("parent_id = ?", v.ID)
		if user_id != 0 {
			dbs = dbs.Where("id in (?)", ids)
		}
		err = dbs.Find(&resFirst).Error
		if user_id != 0 {
			for k, vv := range resFirst {
				var b_u_p_r BUserPrivilegeRelationship
				models.DB().Table("b_user_privilege_relationship").Select("delay_push_time").Where("user_id = ?", user_id).Where("privilege_id = ?", vv.ID).Find(&b_u_p_r)

				if util.InIntSlice(vv.ID, delayPushTime) {
					if b_u_p_r.DelayPushTime != nil {
						resFirst[k].DelayPushTime = b_u_p_r.DelayPushTime
					} else {
						zero := 0
						resFirst[k].DelayPushTime = &zero
					}
				} else {
					resFirst[k].DelayPushTime = nil
				}
			}
		} else {
			for k, vv := range resFirst {
				zero := 0
				if util.InIntSlice(vv.ID, delayPushTime) {
					resFirst[k].DelayPushTime = &zero
				} else {
					resFirst[k].DelayPushTime = nil
				}
			}
		}
		if err != nil {
			return
		}
		dataTotal.Name = v.PrivilegeName
		dataTotal.ID = v.ID
		dataTotal.PermissionId = v.PrivilegeEnglishName
		dataTotal.Children = resFirst
		g_p_l_res.Data = append(g_p_l_res.Data, dataTotal)
		//获取二级 (todo 后续有二级的话在此扩展)
	}

	//整合返回数据
	resp = g_p_l_res.Data
	return resp, err
}

func GetPrivilegeListV2(userId, destId string) ([]*DataTotal, error) {
	var ruleTable []BPrivilege
	var ret, res []*DataTotal
	var userRule []BUserPrivilegeRelationship
	ruleMap := make(map[string]*DataTotal)
	bSecondary := false
	bList := destId == "" || destId == "0"

	db := models.DB().Table("b_user_privilege_relationship").Where("deleted_at IS NULL")
	if bList {
		up := GetPrivileges(userId)
		if c, ok := util.GetInt(up, "/api/v1/account_management"); !ok || c == 0 {
			if s, _ := util.GetInt(up, "/api/v1/permission_secondary"); s == 0 {
				//logging.Error("privilege.GetPrivilegeListV2() permission denied: ", userId)
				//return res, nil
			} else {
				bSecondary = true
			}
		}
	} else {
		db = db.Where("user_id = ?", destId)
		if err := db.Order("privilege_id").Find(&userRule).Error; err != nil {
			logging.Error("privilege.GetPrivilegeListV2() Fetch userRule Errors: ", err.Error())
			return res, err
		}
	}

	dbs := models.DB().Table("b_privilege").Where("deleted_at IS NULL")
	if bSecondary {
		dbs = dbs.Where("secondary = 1")
	}
	if err := dbs.Order("level, sort, parent_id").Find(&ruleTable).Error; err != nil {
		logging.Error("privilege.GetPrivilegeListV2() Fetch ruleTable Errors: ", err.Error())
		return res, err
	}

	for _, v := range ruleTable {
		if v.Level == 0 {
			dt := DataTotal{
				Name:         v.PrivilegeName,
				ID:           v.ID,
				PermissionId: v.PrivilegeEnglishName,
				Children:     nil,
			}
			ruleMap[strconv.Itoa(v.ID)] = &dt
			ret = append(ret, &dt)
		} else {
			if dt, ok := ruleMap[v.ParentID]; ok && dt != nil {
				bHave := false
				for _, u := range userRule {
					if u.PrivilegeID == v.ID {
						bHave = true
						if u.DelayPushTime != nil {
							//prevent GC problem
							c := *u.DelayPushTime
							v.DelayPushTime = &c
						}
					}
				}
				if bList || bHave {
					dt.Children = append(dt.Children, v)
				}
			}
		}
	}
	for _, v := range ret {
		if len(v.Children) > 0 {
			res = append(res, v)
		}
	}
	return res, nil
}

func GetPrivileges(userID string) map[string]interface{} {
	var routers []Router
	ret := make(map[string]interface{})
	//dbs := db.Table("b_user_privilege_relationship as a").Joins("right join b_privilege as b on a.privilege_id = b.id")
	//dbs.Select("b.router").Where("a.user_id = ?", user_id).Where("a.deleted_at is null").Find(&routers)
	//for _, v := range routers {
	//	routersArray = append(routersArray, v.Router)
	//}

	if data, err := gredis.HGetAllStringMap(generateCache(userID)); err == nil && len(data) != 0 {
		for k, v := range data {
			ret[k] = util.Atoi(v)
		}
		return ret
	}

	if err := models.DB().Table("b_privilege AS b").Select(fmt.Sprintf("DISTINCT(b.router), IF(EXISTS(SELECT user_id FROM b_user_privilege_relationship WHERE user_id= %s AND privilege_id = b.id AND deleted_at IS NULL),1,0) AS cnt", userID)).Joins("LEFT JOIN b_user_privilege_relationship AS a ON a.privilege_id = b.id").Where("a.deleted_at IS NULL").Where("b.level = 1").Find(&routers).Error; err != nil {
		logging.Error("privilege.GetPrivileges() Errors: ", err.Error())
		return ret
	}
	gredis.Delete(generateCache(userID))
	for _, v := range routers {
		ret[v.Router] = v.Cnt
		// cache
		_ = gredis.HSet(generateCache(userID), v.Router, v.Cnt)
	}

	return ret
}

func CheckPrivilege(userID, router string) bool {
	pr := GetUserHavePrivilege(router)
	for _, v := range pr {
		if v.UserID == userID {
			return true
		}
	}
	return false
}

func GetUserHavePrivilege(router string) []Privileges {
	var ret []Privileges
	if v, ok := privilegeMap.Load(router); !ok || time.Now().Sub(timeStamp) > 2*time.Second {
		ret = getUserHavePrivilege(router)
		privilegeMap.Store(router, ret)
	} else {
		ret = v.([]Privileges)
	}
	return ret
}

func getUserHavePrivilege(router string) []Privileges {
	var userList []Privileges
	if err := models.DB().Table("b_user_privilege_relationship").Select("DISTINCT(b_user_privilege_relationship.user_id), b_user_privilege_relationship.delay_push_time").Joins("LEFT JOIN b_privilege ON b_user_privilege_relationship.privilege_id = b_privilege.id").Where("b_user_privilege_relationship.deleted_at is null").Where("b_privilege.router = ?", router).Find(&userList).Error; err != nil {
		logging.Error("privilege.GetUserHavePrivilege() Errors: ", err.Error())
	}
	return userList
}

type Privileges struct {
	UserID string `gorm:"column:user_id"`
	Delay  int    `gorm:"column:delay_push_time"`
}

var (
	privilegeMap sync.Map
	timeStamp    time.Time = time.Now()
)

func generateCache(userID string) string {
	return "permission:all:" + userID
}
