package fund

// 基金公告添加
type FundNoticeAddRequest struct {
    FundID          int        `json:"fund_id" form:"fund_id"`
    Author          string     `json:"author" form:"author"`
    Title           string     `json:"title" form:"title" binding:"required"`
    PublishDate     string     `json:"publish_date" binding:"required"`
    Content         string     `json:"content" form:"content" binding:"required"`
}

type  FundNoticeDelByIDRequest struct {
    ID           int    `json:"id" form:"id" binding:"required,gt=0"`
}

type FundNoticeEditRequest struct {
    FundNoticeDelByIDRequest
    FundNoticeAddRequest
}