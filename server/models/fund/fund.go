package fund

type FundRequest struct {
    PageNum      int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
    PageSize     int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
}

type FundAddRequest struct {
    Name        string  `json:"name" form:"name" binding:"required"`
}

type  FundDelByIDRequest struct {
    ID           int    `json:"id" form:"id" binding:"required,gt=0"`
}

type FundEditRequest struct {
    ID                   int       `json:"id" form:"id"`
    Name                 string    `json:"name" form:"name"`                             // 名称
    LastUpdateTime       string    `json:"last_update_time" form:"last_update_time"`    // 最后更新时间
    UnitValue            string    `json:"unit_value" form:"unit_value"`                // 单位净值
    Value                string    `json:"value" form:"value"`                          // 累计净值
    Created              string    `json:"created" form:"created"`                      // 创建时间
    Memo                 string    `json:"memo" form:"memo"`                            // 备注
    OldName              string    `json:"old_name" form:"old_name"`                    // 之前的名称
}

type FundByNameRequest struct {
    Name         string `json:"name" form:"name" binding:"required"`
    PageNum      int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
    PageSize     int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
}

type FundListByUserId struct {
    UserID           int    `json:"user_id" form:"user_id" binding:"required,gt=0"`
}

type FundTickCustomerRequest struct {
    UserID           int       `json:"user_id" form:"user_id" binding:"required,gt=0"`
    FundID           int       `json:"fund_id" form:"fund_id" binding:"required,gt=0"`
    Day              int       `json:"day" form:"day"`
}

type FundNoticeRequest struct {
    NoticeID         int       `json:"notice_id" form:"notice_id"`
}
type FundBonusRequest struct {
    BonusID         int       `json:"bonus_id" form:"bonus_id"`
}
