package fund

type FundTickRequest struct {
    FundID       int     `json:"fund_id" form:"fund_id" binding:"required,gt=0"`       // 基金ID
}

type FundTickAddRequest struct {
    Status               int       `json:"status" form:"status"`                        // 状态  0：未分红；1：分红
    FundID               int       `json:"fund_id" form:"fund_id"`                      // 基金ID
    Name                 string    `json:"name" form:"name"`                            // 名称
    ValueDate            string    `json:"value_date" form:"value_date"`                // 记录日期
    UnitValue            string    `json:"unit_value" form:"unit_value"`                // 单位净值
    Value                string    `json:"value" form:"value"`                          // 累计净值
    Created              string    `json:"created" form:"created"`                      // 创建时间
}

type  FundTickDelByIDRequest struct {
    ID           int    `json:"id" form:"id" binding:"required,gt=0"`
}

type FundTickEditRequest struct {
    FundTickAddRequest
    FundTickDelByIDRequest
}