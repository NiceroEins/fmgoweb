package fund


type  BonusDelByIDRequest struct {
    ID           int    `json:"id" form:"id" binding:"required,gt=0"`
}

// 用户份额
type BonusFundRequest struct {
    PageNum         int         `json:"page_num" form:"page_num" binding:"required,gt=0"`
    PageSize        int         `json:"page_size" form:"page_size" binding:"required,gt=0"`
    FundID          int         `json:"fund_id" form:"fund_id"`
    UserName        string      `json:"user_name" form:"user_name"`
    BonusType       int         `json:"bonus_type" form:"bonus_type"`
    RegisterDate    string      `json:"register_date"form:"register_date"`
}

// 用户份额添加
type BonusFundAddRequest struct {
    FundID             int              `json:"fund_id" form:"fund_id" binding:"required,gt=0"`
    UserID             int              `json:"user_id" form:"user_id" binding:"required,gt=0"`
    BonusType          int              `json:"bonus_type" form:"bonus_type" binding:"required"`
    RegisterDate       string           `json:"register_date" form:"register_date" binding:"required"`
    InvestNum          string           `json:"invest_num" form:"invest_num"`
    UnitBonus          string           `json:"unit_bonus" form:"unit_bonus"`
    CashBonus          string            `json:"cash_bonus" form:"cash_bonus"`
    InvesteMoney       string            `json:"investe_money" form:"investe_money"`
    InvesteUnitValue   string             `json:"investe_unit_value" form:"investe_unit_value"`
}

type BonusFundEditRequest struct {
    BonusDelByIDRequest
    BonusFundAddRequest
}
