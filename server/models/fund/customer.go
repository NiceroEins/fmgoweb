package fund

type CustomerRequest struct {
    PageNum              int        `json:"page_num" form:"page_num" binding:"required,gt=0"`
    PageSize             int        `json:"page_size" form:"page_size" binding:"required,gt=0"`
    Realname             string     `json:"realname" form:"realname"`                                    // 姓名
    IdCardNo             string     `json:"id_card_no" form:"id_card_no"`                                // 身份证号
    UserName             string     `json:"username" form:"username"`                                    // 登录账号
}

type CustomerAddRequest struct {
    Realname             string     `json:"realname" form:"realname" binding:"required"`                 // 姓名
    IdCardNo             string     `json:"id_card_no" form:"id_card_no" binding:"required"`             // 身份证号
    UserName             string     `json:"username" form:"username" binding:"required"`                 // 登录账号
}

type  CustomerDelByIDRequest struct {
    ID           int    `json:"id" form:"id" binding:"required,gt=0"`
}

type CustomerEditRequest struct {
    ID                  int       `json:"id" form:"id"`
    Realname            string    `json:"realname" form:"realname"`           // 姓名
    IdCardNo            string    `json:"id_card_no" form:"id_card_no"`       // 身份证号
    UserName            string    `json:"username" form:"username"`           // 登录账号
}

// 用户份额
type CustomerFundRequest struct {
    PageNum      int     `json:"page_num" form:"page_num" binding:"required,gt=0"`
    PageSize     int     `json:"page_size" form:"page_size" binding:"required,gt=0"`
    FundID       int     `json:"fund_id" form:"fund_id"`
    Realname     string  `json:"realname" form:"realname"`
}

// 用户份额添加
type CustomerFundAddRequest struct {
    FundID       int     `json:"fund_id" form:"fund_id" binding:"required,gt=0"`
    UserID       int     `json:"user_id" form:"user_id" binding:"required,gt=0"`
    Num          string  `json:"num" form:"num" binding:"required,gt=0"`
}

type CustomerFundEditRequest struct {
    CustomerDelByIDRequest
    CustomerFundAddRequest
}


type LoginRequest struct {
    Username string `json:"username" form:"username" valid:"Required; MaxSize(50)"`
    Pwd      string ` json:"pwd" form:"pwd" valid:"Required; MaxSize(50)"`
}
type EditCustomerPwdRequest struct {
    Username string    `json:"username" form:"username" binding:"required"`
    OldPwd   string    `json:"old_pwd" form:"old_pwd" binding:"required"`
    NewPwd   string    `json:"new_pwd" form:"new_pwd" binding:"required"`
}

// 用户份额
type CustomerNameRequest struct {
    Realname     string  `json:"realname" form:"realname"`
}

