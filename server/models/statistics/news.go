package statistics

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/service/crud_service"
	"strconv"
	"time"
)

type NewsCount struct {
	Date string
	Cnt  int
}

type UserStatisticsDTO struct {
	ID        int        `json:"id"`
	Realname  string     `json:"realname"`
	QaTotal   int        `json:"qa_total"`
	Qa        int        `json:"qa"`
	NewsTotal int        `json:"news_total"`
	News      int        `json:"news"`
	LastTime  *time.Time `json:"last_time"`
}

var (
	NewsTotalKey = "statistics:news:"
	QaTotalKey   = "statistics:qa:"
)

type UserStatisticsVO struct {
	ID        int    `json:"id"`
	Realname  string `json:"realname"`
	QaTotal   int    `json:"qa_total"`
	Qa        int    `json:"qa"`
	NewsTotal int    `json:"news_total"`
	News      int    `json:"news"`
	LastTime  string `json:"last_time"`
}

func (dto UserStatisticsDTO) DTO2VO() crud_service.VO {
	return UserStatisticsVO{
		ID:        dto.ID,
		Realname:  dto.Realname,
		QaTotal:   dto.QaTotal,
		Qa:        dto.Qa,
		News:      dto.News,
		NewsTotal: dto.NewsTotal,
		LastTime:  dto.LastTime.Format("2006-01-02 15:04:05"),
	}
}

type TopUserStatisticsDTO struct {
	ID       int    `json:"id"`
	Realname string `json:"realname"`
	Num      int    `json:"num"`
}

type TopUserStatisticsVO struct {
	ID       int    `json:"id"`
	Realname string `json:"realname"`
	Num      int    `json:"num"`
}

func (dto TopUserStatisticsDTO) DTO2VO() crud_service.VO {
	return TopUserStatisticsVO{
		ID:       dto.ID,
		Realname: dto.Realname,
		Num:      dto.Num,
	}
}

type PersonStatisticsDTO struct {
	CatchPath string    `json:"catch_path"`
	Title     string    `json:"title"`
	Content   string    `json:"content"`
	Type      string    `json:"type"`
	NewsTime  time.Time `json:"news_time"`
	OptTime   time.Time `json:"opt_time"`
}

type PersonStatisticsVO struct {
	CatchPath string `json:"catch_path"`
	Title     string `json:"title"`
	Content   string `json:"content"`
	Type      string `json:"type"`
	NewsTime  string `json:"news_time"`
	OptTime   string `json:"opt_time"`
}

func (dto PersonStatisticsDTO) DTO2VO() crud_service.VO {
	return PersonStatisticsVO{
		CatchPath: dto.CatchPath,
		Title:     dto.Title,
		Content:   dto.Content,
		Type:      dto.Type,
		NewsTime:  dto.NewsTime.Format("2006-01-02 15:04:05"),
		OptTime:   dto.OptTime.Format("2006-01-02 15:04:05"),
	}
}

func UpdateTotalNum(key string, uid string, v int) error {
	var err error
	now := time.Now().Format("20060102")
	if !gredis.Exists(key + now) {
		err = gredis.HSet(key+now, uid, v)
		//deadline, _ := time.ParseInLocation("2006-01-02 00:00:00", time.Now().AddDate(0, 1, 0).Format("2006-01-02 00:00:00"), time.Local)
		//timeout := deadline.Unix() - time.Now().Unix()
		//gredis.Expire(key+now, timeout)
	} else {
		err = gredis.HINCRBY(key+now, uid, v)
	}
	return err
}

func CountNews(tsBegin, tsEnd time.Time, category, showPolicy string) []NewsCount {
	var ret []NewsCount
	for tm := tsBegin; !tm.After(tsEnd); tm = tm.Add(24 * time.Hour) {
		var n int
		if cnt := CountNewsByCache(tm, category, showPolicy); cnt < 10 {
			n = CountNewsBySql(tm, category, showPolicy)
		} else {
			n = cnt
		}
		ret = append(ret, NewsCount{
			Date: tm.Format("01-02"),
			Cnt:  n,
		})
	}
	return ret
}

func CountNewsBySql(tm time.Time, category string, showPolicy string) int {
	var cnt int
	db := models.DB().Table("u_feed").Joins("LEFT JOIN u_seed ON u_feed.seed_id = u_seed.id")

	switch showPolicy {
	case "show":
		db = db.Where("u_feed.parent_id = 0").Where("u_seed.hidden = 1 OR ((u_feed.correlation > 0 OR u_feed.correlation IS NULL) AND (u_feed.duplication_id = '' OR u_feed.duplication_id IS NULL OR u_feed.duplication_id = '0'))").Where("u_seed.show_in_home = 1 OR u_feed.keyword_hit = 1")
	case "all":
	}

	switch category {
	case "news":
		db = db.Where("u_feed.type NOT IN (?)", []string{"q", "qa"})
	case "qa":
		db = db.Where("u_feed.type IN (?)", []string{"q", "qa"})
	}

	if err := db.Where("u_feed.created_at BETWEEN ? AND ?", tm.Format("2006-01-02"), tm.Add(24*time.Hour).Format("2006-01-02")).Count(&cnt).Error; err != nil {
		logging.Error("statistics.CountNewsBySql() Errors: ", err.Error())
		return 0
	}
	return cnt
}

func CountNewsByCache(tm time.Time, category string, showPolicy string) int {
	cnt, err := gredis.HGetFloat64(GenerateNewsCountKey(tm), GenerateNewsCountField(category, showPolicy))
	if err != nil || cnt == 0 {
		return 0
	}
	return int(cnt)
}

func GenerateNewsCountKey(tm time.Time) string {
	return "news:statistics:" + tm.Format("20060102")
}

func GenerateNewsCountField(category, showPolicy string) string {
	return category + "_" + showPolicy
}

func GetUserQaTotal(date string, uid string) (int, error) {
	if !gredis.HExists(QaTotalKey+date, uid) {
		return 0, nil
	}
	num, err := gredis.HGetString(QaTotalKey+date, uid)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(num)
}

func GetUserNewsTotal(date string, uid string) (int, error) {
	if !gredis.HExists(NewsTotalKey+date, uid) {
		return 0, nil
	}
	num, err := gredis.HGetString(NewsTotalKey+date, uid)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(num)
}

func GetUserStatisticsList(dateTime, name string, pageNum, pageSize int) ([]crud_service.DTO, int, error) {
	cnt := 0
	var datetimeKey string
	dto := make([]UserStatisticsDTO, 0)
	ret := make([]crud_service.DTO, 0)
	datetimeKey = time.Now().Format("20060102")
	qxl := models.DB().Table("u_event").Select("user_id,object_id,MAX(created_at) last_time").Group("user_id,object_id")
	if dateTime != "" {
		datetime, syserr := time.ParseInLocation("2006-01-02", dateTime, time.Local)
		if syserr != nil {
			return nil, 0, syserr
		}
		begin := datetime.Format("2006-01-02")
		end := datetime.AddDate(0, 0, 1).Format("2006-01-02")
		qxl = qxl.Where("object_id>0 AND user_id>0 AND u_event.created_at BETWEEN ? AND ?", begin, end)
		datetimeKey = datetime.Format("20060102")
	}
	dbs := models.DB()
	dbs = dbs.Joins("SELECT user_id id,realname,COUNT(u_feed.type='qa' OR u_feed.type='q' OR NULL) qa, "+
		"COUNT((u_feed.type!='qa' AND u_feed.type!='q') OR NULL) news,MAX(last_time) last_time FROM ?  E LEFT JOIN u_feed ON u_feed.id=E.object_id LEFT JOIN b_user ON user_id=b_user.id", qxl.SubQuery())
	if name != "" {
		dbs = dbs.Raw("WHERE b_user.realname like ?", "%"+name+"%")
	} else {
		dbs = dbs.Raw("WHERE user_id>0")
	}
	dbs = dbs.Group("user_id")
	_ = models.DB().Raw("SELECT count(*) as name FROM ? T", dbs.SubQuery()).Row().Scan(&cnt)
	err := dbs.Offset((pageNum - 1) * pageSize).Limit(pageSize).
		Find(&dto).Error
	if err != nil {
		return nil, cnt, err
	}
	for _, v := range dto {
		v.QaTotal, err = GetUserQaTotal(datetimeKey, strconv.Itoa(v.ID))
		if err != nil {
			return nil, 0, err
		}
		v.NewsTotal, err = GetUserNewsTotal(datetimeKey, strconv.Itoa(v.ID))
		if err != nil {
			return nil, 0, err
		}
		ret = append(ret, v)
	}
	return ret, cnt, err
}

func GetTopUserStatisticsList(startTime, endTime time.Time, limit int) ([]crud_service.DTO, error) {
	dto := make([]TopUserStatisticsDTO, 0)
	ret := make([]crud_service.DTO, 0)
	begin := startTime.Format("2006-01-02")
	end := endTime.AddDate(0, 0, 1).Format("2006-01-02")
	dbs := models.DB().Table("u_event").Select("user_id id,realname,COUNT(DISTINCT object_id) num").
		Joins("LEFT JOIN b_user ON user_id=b_user.id ").
		Where("object_id>0 AND user_id>0 AND u_event.created_at BETWEEN ? AND ?", begin, end).
		Group("user_id").
		Order("num DESC")
	err := dbs.Limit(limit).Find(&dto).Error
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		ret = append(ret, v)
	}
	return ret, err
}

func GetPersonStatisticsList(id, pageNum, pageSize int, dateTime string) ([]crud_service.DTO, int, error) {
	cnt := 0
	dto := make([]PersonStatisticsDTO, 0)
	ret := make([]crud_service.DTO, 0)
	dbs := models.DB().Table("u_event").Select("u_seed.catch_path,title,content,u_feed.type,u_feed.created_at news_time,MIN(u_event.created_at) opt_time ").
		Joins("LEFT JOIN u_feed ON u_feed.id=u_event.object_id ").
		Joins("LEFT JOIN u_seed ON u_feed.seed_id=u_seed.id ").
		Where("object_id>0 AND user_id= ?", id).
		Group("object_id")
	if dateTime != "" {
		datetime, syserr := time.ParseInLocation("2006-01-02", dateTime, time.Local)
		if syserr != nil {
			return nil, 0, syserr
		}
		begin := datetime.Format("2006-01-02")
		end := datetime.AddDate(0, 0, 1).Format("2006-01-02")
		dbs = dbs.Where("u_event.created_at BETWEEN ? AND ?", begin, end)
	}
	dbs.Count(&cnt)
	err := dbs.Offset((pageNum - 1) * pageSize).Limit(pageSize).Order("opt_time DESC").
		Find(&dto).Error
	if err != nil {
		return nil, cnt, err
	}
	for _, v := range dto {
		ret = append(ret, v)
	}
	return ret, cnt, err
}
