package statistics

import (
	"datacenter/models"
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"github.com/jinzhu/gorm"
	"sort"
	"strings"
	"time"
)

type AuthorCount struct {
	Author string `json:"author"`
	Cnt    int    `json:"cnt"`
}

func CountAuthor(tsBegin, tsEnd time.Time, limit int) []AuthorCount {
	var ret []AuthorCount
	db := models.DB().Table("u_feed")
	db = db.Select("author, COUNT(1) AS cnt").Where("type IN (?)", []string{"qa", "q"}).Where("author IS NOT NULL").Where("author <> ''").Where("created_at BETWEEN ? AND ?", tsBegin.Format("2006-01-02"), tsEnd.Format("2006-01-02")+" 23:59:59")
	if err := db.Group("author").Order("cnt DESC").Limit(limit).Find(&ret).Error; err != nil {
		logging.Error("statistics.CountAuthor() Errors: ", err.Error())
	}
	return ret
}

type ObjectCount struct {
	Object  string `json:"object"`
	Name    string `json:"name"`
	Cnt     int    `json:"cnt"`
	Today   int    `json:"today"`
	History int    `json:"history"`
}

func CountObject(tsBegin, tsEnd time.Time, category string, limit int) []ObjectCount {
	var ret []ObjectCount
	dbs := models.DB().Table("u_feed").Joins("LEFT JOIN b_stock ON b_stock.id = u_feed.object")
	dbs = dbs.Select("u_feed.object, b_stock.name, COUNT(1) AS cnt").Where("u_feed.object IS NOT NULL").Where("u_feed.object <> ''").Where("u_feed.created_at BETWEEN ? AND ?", tsBegin.Format("2006-01-02"), tsEnd.Format("2006-01-02")+" 23:59:59")
	dbs = dbs.Scopes(scopeCategory(category, "where"))
	sub := dbs.Group("u_feed.object").Order("cnt DESC").Limit(limit).SubQuery()

	begin := "0"
	end := "0"
	if tsEnd.Format("20060102") == time.Now().Format("20060102") {
		begin = tsEnd.Format("2006-01-02")
		end = tsEnd.Format("2006-01-02") + " 23:59:59"
	}
	dbt := models.DB().Select("t.*, COUNT(CASE WHEN u_feed.id IS NOT NULL THEN 1 ELSE NULL END) AS today").Table("u_feed").Joins("RIGHT JOIN ? t ON t.object = u_feed.object AND (u_feed.created_at BETWEEN ? AND ?) AND ?", sub, begin, end, models.DB().Scopes(scopeCategory(category, "raw")).QueryExpr()).Group("t.object").SubQuery()
	db := models.DB().Raw("SELECT s.*, s.cnt-s.today AS history FROM ? AS s", dbt).Order("s.cnt DESC")
	if err := db.Find(&ret).Error; err != nil {
		logging.Error("statistics.CountAuthor() Errors: ", err.Error())
	}
	return ret
}

type KeywordCount struct {
	Keyword string `json:"keyword"`
	Cnt     int    `json:"cnt"`
	Today   int    `json:"today"`
	History int    `json:"history"`
}

type KeywordCounts []KeywordCount

func (p KeywordCounts) Len() int {
	return len(p)
}

func (p KeywordCounts) Less(i, j int) bool {
	return p[i].Cnt > p[j].Cnt
}

func (p KeywordCounts) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func CountKeyword(data []spider.NewsEx, keywords []string) []KeywordCount {
	ret := make([]KeywordCount, len(keywords))
	now := time.Now()
	for i := 0; i < len(data); i++ {
		for j := 0; j < len(keywords); j++ {
			if strings.Index(data[i].Title, keywords[j]) >= 0 || strings.Index(data[i].Content, keywords[j]) >= 0 {
				// match
				ret[j].Keyword = keywords[j]
				ret[j].Cnt++
				if data[i].CreatedAt.Format("20060102") == now.Format("20060102") {
					ret[j].Today++
				} else {
					ret[j].History++
				}
			}
		}
	}

	sort.Sort(KeywordCounts(ret))
	return ret
}

func scopeCategory(category, method string) func(*gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		switch {
		case category == "all" && method == "where":
			db = db.Where("u_feed.type IN (?)", []string{"qa", "q"})
		case category == "all" && method == "raw":
			db = db.Raw("u_feed.type IN (?)", []string{"qa", "q"})
		case category == "q" && method == "where":
			db = db.Where("u_feed.type = 'q'")
		case category == "q" && method == "raw":
			db = db.Raw("u_feed.type = 'q'")
		case category == "qa" && method == "where":
			db = db.Where("u_feed.type = 'qa'")
		case category == "qa" && method == "raw":
			db = db.Raw("u_feed.type = 'qa'")
		}
		return db
	}
}
