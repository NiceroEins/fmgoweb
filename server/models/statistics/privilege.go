package statistics

import (
	"datacenter/models"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"time"
)

type PrivilegeStatisticListQO struct {
	StartTime string `json:"start_time" form:"start_time"`
	EndTime   string `json:"end_time" form:"end_time"`
}

type PrivilegeStatisticsListDTO struct {
	ID             int64  `json:"id"`
	UID            int    `json:"uid"`
	PrivilegeId    int    `json:"privilege_id"`
	PPrivilegeId   int    `json:"p_privilege_id"`
	PrivilegeName  string `json:"privilege_name"`
	PPrivilegeName string `json:"p_privilege_name"`
	DuringTime     int    `json:"during_time"`
	PV             int    `json:"pv"`
	UV             int    `json:"uv"`
}

type PrivilegeStatisticsListVO struct {
	ID             int64  `json:"id"`
	UID            int    `json:"uid"`
	PrivilegeId    int    `json:"privilege_id"`
	PPrivilegeId   int    `json:"p_privilege_id"`
	PrivilegeName  string `json:"privilege_name"`
	PPrivilegeName string `json:"p_privilege_name"`
	DuringTime     int    `json:"during_time"`
	PV             int    `json:"pv"`
	UV             int    `json:"uv"`
}

func (dto PrivilegeStatisticsListDTO) DTO2VO() crud_service.VO {
	return &PrivilegeStatisticsListVO{
		ID:             dto.ID,
		UID:            dto.UID,
		PrivilegeId:    dto.PrivilegeId,
		PPrivilegeId:   dto.PPrivilegeId,
		PrivilegeName:  dto.PrivilegeName,
		PPrivilegeName: dto.PPrivilegeName,
		DuringTime:     dto.DuringTime,
		PV:             dto.PV,
		UV:             dto.UV,
	}
}

func GetPrivilegeStatisticsList(qo PrivilegeStatisticListQO) ([]crud_service.DTO, error) {
	dto := make([]PrivilegeStatisticsListDTO, 0)
	ret := make([]crud_service.DTO, 0)
	dbs := models.DB().Select("PV.id,uid,PP.id p_privilege_id," +
		"PV.privilege_id," +
		"PP.privilege_name p_privilege_name," +
		"P.privilege_name,count(distinct uid) uv," +
		"count(uid) pv,sum(during_time)  during_time").
		Table("u_user_privilege_statistics PV").
		Joins("LEFT JOIN b_privilege P ON P.id=PV.privilege_id ").
		Joins("LEFT JOIN b_privilege PP ON PP.id=P.parent_id ").
		Group("privilege_id,p_privilege_id")
	if qo.StartTime != "" {
		dbs = dbs.Where("PV.created_at>=?", qo.StartTime)
	}
	if qo.EndTime != "" {
		endTime, _ := time.ParseInLocation(util.YMD, qo.EndTime, time.Local)
		qo.EndTime = endTime.AddDate(0, 0, 1).Format(util.YMD)
		dbs = dbs.Where("PV.created_at<?", qo.EndTime)
	}
	err := dbs.Find(&dto).Error
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		ret = append(ret, v)
	}
	return ret, err
}

type PrivilegeStatisticUserListQO struct {
	PrivilegeId int    `json:"privilege_id" form:"privilege_id" binding:"gt=0"`
	StartTime   string `json:"start_time" form:"start_time"`
	EndTime     string `json:"end_time" form:"end_time"`
}

type UserPrivilegeListDTO struct {
	ID         int64  `json:"id"`
	Realname   string `json:"realname"`
	DuringTime int    `json:"during_time"`
	PV         int    `json:"pv"`
}

type UserPrivilegeListVO struct {
	ID         int64  `json:"id"`
	Realname   string `json:"realname"`
	DuringTime int    `json:"during_time"`
	PV         int    `json:"pv"`
}

func (dto UserPrivilegeListDTO) DTO2VO() crud_service.VO {
	return &UserPrivilegeListVO{
		ID:         dto.ID,
		Realname:   dto.Realname,
		DuringTime: dto.DuringTime,
		PV:         dto.PV,
	}
}

func GetPrivilegeStatisticsUserList(qo PrivilegeStatisticUserListQO) ([]crud_service.DTO, error) {
	dto := make([]UserPrivilegeListDTO, 0)
	ret := make([]crud_service.DTO, 0)
	dbs := models.DB().Select("U.id,realname,"+
		"count(uid) pv,sum(during_time)  during_time").
		Table("u_user_privilege_statistics PV").
		Joins("LEFT JOIN b_user U ON U.id=PV.uid ").
		Where("privilege_id=?", qo.PrivilegeId).
		Group("uid")
	if qo.StartTime != "" {
		dbs = dbs.Where("PV.created_at>=?", qo.StartTime)
	}
	if qo.EndTime != "" {
		endTime, _ := time.ParseInLocation(util.YMD, qo.EndTime, time.Local)
		qo.EndTime = endTime.AddDate(0, 0, 1).Format(util.YMD)
		dbs = dbs.Where("PV.created_at<?", qo.EndTime)
	}
	err := dbs.Find(&dto).Error
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		ret = append(ret, v)
	}
	return ret, err
}

type UserPrivilegeStatisticTotalQO struct {
	StartTime string `json:"start_time" form:"start_time"`
	EndTime   string `json:"end_time" form:"end_time"`
	Name      string `json:"name" form:"name"`
}

type UserPrivilegeStatisticsTotalDTO struct {
	ID             int64      `json:"id"`
	UID            int        `json:"uid"`
	Realname       string     `json:"realname"`
	PrivilegeId    int        `json:"privilege_id"`
	PPrivilegeId   int        `json:"p_privilege_id"`
	PrivilegeName  string     `json:"privilege_name"`
	PPrivilegeName string     `json:"p_privilege_name"`
	PrivilegeCount int        `json:"privilege_count"`
	DuringTime     int        `json:"during_time"`
	LastTime       *time.Time `json:"last_time"`
}

type UserPrivilegeStatisticsTotalVO struct {
	ID             int64  `json:"id"`
	UID            int    `json:"uid"`
	Realname       string `json:"realname"`
	PrivilegeId    int    `json:"privilege_id"`
	PPrivilegeId   int    `json:"p_privilege_id"`
	PrivilegeName  string `json:"privilege_name"`
	PPrivilegeName string `json:"p_privilege_name"`
	PrivilegeCount int    `json:"privilege_count"`
	DuringTime     int    `json:"during_time"`
	LastTime       string `json:"last_time"`
}

func (dto UserPrivilegeStatisticsTotalDTO) DTO2VO() crud_service.VO {
	return &UserPrivilegeStatisticsTotalVO{
		ID:             dto.ID,
		UID:            dto.UID,
		Realname:       dto.Realname,
		PrivilegeId:    dto.PrivilegeId,
		PPrivilegeId:   dto.PPrivilegeId,
		PrivilegeName:  dto.PrivilegeName,
		PPrivilegeName: dto.PPrivilegeName,
		PrivilegeCount: dto.PrivilegeCount,
		DuringTime:     dto.DuringTime,
		LastTime:       util.TimePtr2String(dto.LastTime, util.YMDHMS),
	}
}

func GetUserPrivilegeStatisticsTotal(qo UserPrivilegeStatisticTotalQO) ([]crud_service.DTO, error) {
	dto := make([]UserPrivilegeStatisticsTotalDTO, 0)
	ret := make([]crud_service.DTO, 0)
	subSql := models.DB().Select("MAX(PV.id) id,uid,count(distinct privilege_id) privilege_count,sum(during_time) during_time ,MAX(PV.created_at) last_time").Table("u_user_privilege_statistics PV")
	if qo.StartTime != "" {
		subSql = subSql.Where("PV.created_at>=?", qo.StartTime)
	}
	if qo.EndTime != "" {
		endTime, _ := time.ParseInLocation(util.YMD, qo.EndTime, time.Local)
		qo.EndTime = endTime.AddDate(0, 0, 1).Format(util.YMD)
		subSql = subSql.Where("PV.created_at<?", qo.EndTime)
	}
	subSql = subSql.Group("uid")
	dbs := models.DB().Select("realname,G.*, PV.privilege_id, P.privilege_name, PP.privilege_name p_privilege_name,P.parent_id p_privilege_id").
		Table("u_user_privilege_statistics PV").
		Joins("RIGHT JOIN ? G ON PV.id = G.id ", subSql.SubQuery()).
		Joins("LEFT JOIN b_privilege P ON P.id = PV.privilege_id").
		Joins("LEFT JOIN b_privilege PP ON PP.id=P.parent_id ").
		Joins("LEFT JOIN b_user U ON U.id = G.uid")
	if qo.Name != "" {
		dbs = dbs.Where("realname like ?", "%"+qo.Name+"%")
	}
	err := dbs.Find(&dto).Error
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		ret = append(ret, v)
	}
	return ret, err
}

type UserPrivilegeStatisticQO struct {
	UID       int    `json:"uid" form:"uid" binding:"gt=0"`
	StartTime string `json:"start_time" form:"start_time"`
	EndTime   string `json:"end_time" form:"end_time"`
}

type UserPrivilegeStatisticsDTO struct {
	ID             int64  `json:"id"`
	UID            int    `json:"uid"`
	PrivilegeId    int    `json:"privilege_id"`
	PPrivilegeId   int    `json:"p_privilege_id"`
	PrivilegeName  string `json:"privilege_name"`
	PPrivilegeName string `json:"p_privilege_name"`
	DuringTime     int    `json:"during_time"`
	PV             int    `json:"pv"`
}

type UserPrivilegeStatisticsVO struct {
	ID             int64  `json:"id"`
	UID            int    `json:"uid"`
	PrivilegeId    int    `json:"privilege_id"`
	PPrivilegeId   int    `json:"p_privilege_id"`
	PrivilegeName  string `json:"privilege_name"`
	PPrivilegeName string `json:"p_privilege_name"`
	DuringTime     int    `json:"during_time"`
	PV             int    `json:"pv"`
}

func (dto UserPrivilegeStatisticsDTO) DTO2VO() crud_service.VO {
	return &UserPrivilegeStatisticsVO{
		ID:             dto.ID,
		UID:            dto.UID,
		PrivilegeId:    dto.PrivilegeId,
		PPrivilegeId:   dto.PPrivilegeId,
		PrivilegeName:  dto.PrivilegeName,
		PPrivilegeName: dto.PPrivilegeName,
		DuringTime:     dto.DuringTime,
		PV:             dto.PV,
	}
}

func GetUserPrivilegeStatisticsDetail(qo UserPrivilegeStatisticQO) ([]crud_service.DTO, error) {
	dto := make([]UserPrivilegeStatisticsDTO, 0)
	ret := make([]crud_service.DTO, 0)
	dbs := models.DB().Select("PV.id,uid,PP.id p_privilege_id,"+
		"PV.privilege_id,"+
		"PP.privilege_name p_privilege_name,"+
		"P.privilege_name,"+
		"count(uid) pv,sum(during_time) during_time").
		Table("u_user_privilege_statistics PV").
		Joins("LEFT JOIN b_privilege P ON P.id=PV.privilege_id ").
		Joins("LEFT JOIN b_privilege PP ON PP.id=P.parent_id ").
		Where("uid=?", qo.UID).
		Group("privilege_id")
	if qo.StartTime != "" {
		dbs = dbs.Where("PV.created_at>=?", qo.StartTime)
	}
	if qo.EndTime != "" {
		endTime, _ := time.ParseInLocation(util.YMD, qo.EndTime, time.Local)
		qo.EndTime = endTime.AddDate(0, 0, 1).Format(util.YMD)
		dbs = dbs.Where("PV.created_at<?", qo.EndTime)
	}
	err := dbs.Order("PV.id DESC").Find(&dto).Error
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		ret = append(ret, v)
	}
	return ret, err
}

type UserPrivilegeStatisticMapQO struct {
	UID       int    `json:"uid" form:"uid"`
	StartTime string `json:"start_time" form:"start_time" binding:"required"`
	EndTime   string `json:"end_time" form:"end_time" binding:"required"`
	Type      string `json:"type" form:"type" binding:"oneof=days hours"`
}

type UserPrivilegeStatisticsMapDTO struct {
	Times          string `json:"times"`
	PrivilegeCount int    `json:"privilege_count"`
	PV             int    `json:"pv"`
	DuringTime     int    `json:"during_time"`
}

type UserPrivilegeStatisticsMapVO struct {
	Times          string `json:"times"`
	PrivilegeCount int    `json:"privilege_count"`
	PV             int    `json:"pv"`
	DuringTime     int    `json:"during_time"`
}

func (dto UserPrivilegeStatisticsMapDTO) DTO2VO() crud_service.VO {
	return &UserPrivilegeStatisticsMapVO{
		Times:          dto.Times,
		PrivilegeCount: dto.PrivilegeCount,
		PV:             dto.PV,
		DuringTime:     dto.DuringTime,
	}
}

func GetUserPrivilegeStatisticsMap(qo UserPrivilegeStatisticMapQO) ([]crud_service.DTO, error) {
	dto := make([]UserPrivilegeStatisticsMapDTO, 0)
	ret := make([]crud_service.DTO, 0)
	var dbs *gorm.DB
	var timeSize time.Duration
	var timeLayout string
	switch qo.Type {
	case "hours":
		timeSize = time.Hour
		timeLayout = util.YMDHMS
		dbs = models.DB().Select("DATE_FORMAT(PV.created_at,'%Y-%m-%d %H:00:00') times,count(distinct privilege_id) privilege_count,count(uid) pv,sum(during_time) during_time").
			Table("u_user_privilege_statistics PV")
		break
	case "days":
		timeSize = time.Hour * 24
		timeLayout = util.YMD
		dbs = models.DB().Select("DATE_FORMAT(PV.created_at,'%Y-%m-%d') times,count(distinct privilege_id) privilege_count,count(uid) pv,sum(during_time) during_time").
			Table("u_user_privilege_statistics PV")
		break
	default:
		return nil, errors.New("错误时间类型")
	}
	if qo.UID > 0 {
		dbs = dbs.Where("uid=?", qo.UID)
	}
	//if qo.StartTime != "" {
	startTime, err := time.ParseInLocation(util.YMD, qo.StartTime, time.Local)
	if err != nil {
		return nil, err
	}
	dbs = dbs.Where("PV.created_at>=?", qo.StartTime)
	//}
	//if qo.EndTime != "" {
	endTime, err := time.ParseInLocation(util.YMD, qo.EndTime, time.Local)
	if err != nil {
		return nil, err
	}
	endTime = endTime.AddDate(0, 0, 1)
	qo.EndTime = endTime.Format(util.YMD)
	dbs = dbs.Where("PV.created_at<?", qo.EndTime)
	//}

	err = dbs.Group("times").Find(&dto).Error
	if err != nil {
		return nil, err
	}
	timeLen := int(endTime.Sub(startTime) / timeSize)

	for i := 0; i < timeLen; i++ {
		data := UserPrivilegeStatisticsMapDTO{}
		data.Times = startTime.Format(timeLayout)
		for _, v := range dto {
			if data.Times == v.Times {
				data.PrivilegeCount = v.PrivilegeCount
				data.PV = v.PV
				data.DuringTime = v.DuringTime
			}
		}
		ret = append(ret, data)
		startTime = startTime.Add(timeSize)
	}
	return ret, err
}
