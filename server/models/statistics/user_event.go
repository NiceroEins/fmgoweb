package statistics

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"encoding/json"
	"sync"
	"time"
)

var UserStatusPool sync.Map

type EventModel struct {
	Uid         int `json:"uid"`
	PrivilegeID int `json:"privilege_id"`
	DuringTime  int `json:"during_time"`
}

type UserEvent struct {
	EventType string `json:"event_type"`
	Object    string `json:"object"`
	Uid       int    `json:"uid"`
	IsFocused bool   `json:"focus"`
}

func (EventModel) TableName() string {
	return "u_user_privilege_statistics"
}

func Do(eventStr string) {
	var ue UserEvent

	if err := json.Unmarshal([]byte(eventStr), &ue); err != nil {
		logging.Error("statistics.Do() parse Errors: ", err.Error())
		return
	}
	uo := GetUserObj(ue.Uid)
	go uo.Event(&ue)
}

func privilegeID(privilege string) int {
	ret, err := gredis.HGetFloat64("privilege_name_id", privilege)
	if err != nil {
		getPrivileges()
		if ret, err = gredis.HGetFloat64("privilege_name_id", privilege); err != nil {
			logging.Error("statistics.privilegeID() Unknown privilege: ", privilege)
			return 0
		}
	}
	return int(ret)
}

func getPrivileges() {
	var bp []struct {
		ID                   int
		PrivilegeEnglishName string
	}
	if err := models.DB().Table("b_privilege").Find(&bp).Error; err != nil {
		logging.Error("statistics.Do() fetch privilege Errors: ", err.Error())
		return
	}
	for _, v := range bp {
		gredis.HSet("privilege_name_id", v.PrivilegeEnglishName, v.ID)
	}
}

type Obj struct {
	Uid    int
	Status UserStatus
	focus  string
	begin  time.Time
	cur    time.Time
	idle   time.Time
	//mutex  int64
}

type UserObj struct {
	Obj
	locker sync.Mutex
}

type UserStatus string

const (
	User_Free    UserStatus = "free"
	User_Focused UserStatus = "focused"
	User_Idle    UserStatus = "idle"

	Min_Time_Interval time.Duration = 30 * time.Second
)

func GetUserObj(uid int) *UserObj {
	var ret *UserObj
	if v, ok := UserStatusPool.Load(uid); !ok {
		ret = newUserStatus(uid)
		UserStatusPool.Store(uid, ret)
	} else {
		ret, _ = v.(*UserObj)
	}
	return ret
}

func newUserStatus(uid int) *UserObj {
	return &UserObj{
		Obj: Obj{
			Uid:    uid,
			Status: User_Free,
			begin:  time.Now(),
			cur:    time.Now(),
		},
	}
}

func (uo *UserObj) Event(ue *UserEvent) {
	uo.locker.Lock()
	defer uo.locker.Unlock()

	if ue.Uid == 21 {
		bt, _ := json.Marshal(ue)
		logging.Info("statistics.Event() event: ", string(bt))
	}

	switch ue.EventType {
	case "leave":
		if uo.Status != User_Free {
			uo.cur = time.Now()
			go Settle(uo.Obj)
			uo.SetStatus(User_Free)
		}
		uo.reset()
	case "enter":
		if uo.Status != User_Free {
			go Settle(uo.Obj)
			uo.reset()
		}
		uo.focusTo(ue.Object)
	case "heartbeat":
		switch uo.Status {
		case User_Free:
			// begin
			uo.focusTo(ue.Object)
		case User_Focused:
			if uo.focus != ue.Object {
				if ue.IsFocused == true {
					if time.Now().Sub(uo.cur) < Min_Time_Interval {
						uo.cur = time.Now()
					}
					go Settle(uo.Obj)
					uo.focusTo(ue.Object)
				} else {
					uo.cur = time.Now()
				}
			} else {
				uo.cur = time.Now()
			}

			//// focused
			//if !ue.IsFocused {
			//	if uo.idle.IsZero() {
			//		uo.cur = time.Now()
			//		uo.idle = time.Now()
			//	} else {
			//		if time.Now().Sub(uo.idle) > 120*time.Second {
			//			uo.idle = time.Now()
			//			uo.SetStatus(User_Idle)
			//		}
			//	}
			//} else {
			//	uo.cur = time.Now()
			//}
			//case User_Idle:
			//	if ue.IsFocused {
			//		if uo.focus != ue.Object {
			//			go Settle(uo.Obj, uo.idle)
			//			uo.begin = time.Now()
			//			uo.cur = time.Now()
			//			uo.setFocus(ue.Object)
			//		} else {
			//			// update time
			//			uo.begin = uo.begin.Add(time.Now().Sub(uo.idle))
			//			uo.cur = time.Now()
			//		}
			//		uo.SetStatus(User_Focused)
			//	}
		}

	default:
		logging.Info("statistics.Event() unknown event: ", ue.EventType)
		return
	}
}

func (uo *UserObj) SetStatus(us UserStatus) {
	uo.Status = us
}

func (uo *UserObj) setFocus(focus string) {
	uo.focus = focus
}

func (uo *UserObj) reset() {
	uo.cur = time.Time{}
	uo.idle = time.Time{}
	uo.begin = time.Time{}
	uo.focus = ""
}

func (uo *UserObj) focusTo(object string) {
	//if !atomic.CompareAndSwapInt64(&uo.mutex, 0, 1) {
	//	return
	//}
	//defer atomic.StoreInt64(&uo.mutex, 0)
	uo.begin = time.Now()
	uo.cur = uo.begin
	uo.setFocus(object)
	uo.SetStatus(User_Focused)
}

func Settle(uo Obj) {
	//if uo.Uid == 21 {
	//	bt, _ := json.Marshal(uo)
	//	logging.Info("statistics.Settle() obj: ", string(bt))
	//}

	if uo.cur.IsZero() {
		uo.cur = time.Now()
	}
	dt := uo.cur.Sub(uo.begin).Seconds()
	if dt < 0 {
		dt = 0
	}
	pi := privilegeID(uo.focus)
	if err := models.DB().Table("u_user_privilege_statistics").Create(&EventModel{
		Uid:         uo.Uid,
		PrivilegeID: pi,
		DuringTime:  int(dt),
	}).Error; err != nil {
		logging.Error("statistics.Settle() create record Errors: ", err.Error())
		return
	}
}
