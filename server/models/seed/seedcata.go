package seed

import (
    "datacenter/models"
)

type SeedCataDTO struct {
    models.Simple
    Name       string `json:"name" form:"name"`
    Key        string `json:"key" form:"key"`
    Value      string `json:"value" form:"value"`
    Status     string `json:"status" form:"status"`
    OrderIndex string `json:"order_index" form:"order_index"`
    Level      string `json:"level" form:"level"`
    ParentId   string `json:"parent_id" form:"parent_id"`
    Creator    string `json:"creator" form:"creator"`
}

type SeedCataVO struct {
    ID    int    `json:"id" form:"id"`
    Value string `json:"value" form:"value"`
}

type SeedCataQO struct {
    Value string `json:"value" form:"value"`
}

func (SeedCataDTO) TableName() string {
    return "u_map_config"
}

func (dto *SeedCataDTO) DTO2VO() *SeedCataVO {
    return &SeedCataVO{
        ID:    dto.ID,
        Value: dto.Value,
    }
}

func GetAll() ([]SeedCataDTO, error) {
    dtos := make([]SeedCataDTO, 0)
    err := models.DB().Model(&SeedCataDTO{}).Find(&dtos).Error
    return dtos, err
}

func SearchSeedCata(qo SeedCataQO) ([]SeedCataDTO, error) {
    dtos := make([]SeedCataDTO, 0)
    err := models.DB().Model(&SeedCataDTO{}).Where("value like ?", "%"+qo.Value+"%").Find(&dtos).Error
    return dtos, err
}
