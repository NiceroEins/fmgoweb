package seed

import (
	"datacenter/controller/task"
	"datacenter/models"
	"datacenter/models/spider"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"encoding/json"
	"strconv"
	"strings"
	"time"
)

type SeedDTO struct {
	models.Simple
	LastCatchTime     *time.Time `json:"catch_time"  gorm:"-"`
	CatchSite         string     `json:"catch_site"`
	CatchPath         string     `json:"catch_path"`
	CatchLink         string     `json:"catch_link"`
	Star              int        `json:"star"`
	Cata              string     `json:"cata"`
	CatchInterval     int        `json:"catch_interval"`
	SelectorPageLink  string     `json:"selector_page_link"`
	SelectorListTime  string     `json:"selector_list_time"`
	SelectorListTitle string     `json:"selector_list_title"`
	CatchMode         string     `json:"catch_mode"`
	Status            string     `json:"status"`
	ShowInHome        *int8      `json:"show_in_home"`
	ApiLink           *string    `json:"api_link"`
}

type SeedVO struct {
	ID                int     `gorm:"primary_key" json:"id"`
	CreatedAt         string  `json:"created_at"`
	CatchTime         string  `json:"catch_time"`
	CatchSite         string  `json:"catch_site"`
	CatchPath         string  `json:"catch_path"`
	CatchLink         string  `json:"catch_link"`
	Star              int     `json:"star"`
	StarCN            string  `json:"star_cn"`
	Cata              string  `json:"cata"`
	CataCN            string  `json:"cata_cn"`
	CatchInterval     int     `json:"catch_interval"`
	SelectorPageLink  string  `json:"selector_page_link"`
	SelectorListTime  string  `json:"selector_list_time"`
	SelectorListTitle string  `json:"selector_list_title"`
	CatchMode         string  `json:"catch_mode"`
	Status            string  `json:"status"`
	ShowInHome        int8    `json:"show_in_home"`
	ApiLink           *string `json:"api_link" form:"api_link"`
}

type SeedIO struct {
	CatchSite         string  `json:"catch_site" form:"catch_site" binding:"required"`
	CatchPath         string  `json:"catch_path" form:"catch_path" binding:"required"`
	CatchLink         string  `json:"catch_link" form:"catch_link" binding:"required"`
	Star              int     `json:"star" form:"star" binding:"required,gt=0"`
	Cata              string  `json:"cata" form:"cata" binding:"required"`
	CatchInterval     int     `json:"catch_interval" form:"catch_interval" binding:"required,gt=0"`
	SelectorPageLink  string  `json:"selector_page_link" form:"selector_page_link" binding:"required"`
	SelectorListTime  string  `json:"selector_list_time" form:"selector_list_time"`
	SelectorListTitle string  `json:"selector_list_title" form:"selector_list_title" binding:"required"`
	CatchMode         string  `json:"catch_mode" form:"catch_mode" binding:"required"`
	Status            string  `json:"status" form:"status" binding:"required"`
	ShowInHome        *int8   `json:"show_in_home" form:"show_in_home" binding:"gte=0,lte=1"`
	ApiLink           *string `json:"api_link,omitempty" form:"api_link"`
}

type SeedEO struct {
	ID                int     `json:"id" form:"id" binding:"required"`
	CatchSite         string  `json:"catch_site" form:"catch_site" binding:"required"`
	CatchPath         string  `json:"catch_path" form:"catch_path" binding:"required"`
	CatchLink         string  `json:"catch_link" form:"catch_link" binding:"required"`
	Star              int     `json:"star" form:"star" binding:"required,gt=0"`
	Cata              string  `json:"cata" form:"cata" binding:"required"`
	CatchInterval     int     `json:"catch_interval" form:"catch_interval" binding:"required,gt=0"`
	SelectorPageLink  string  `json:"selector_page_link" form:"selector_page_link" binding:"required"`
	SelectorListTime  string  `json:"selector_list_time" form:"selector_list_time"`
	SelectorListTitle string  `json:"selector_list_title" form:"selector_list_title" binding:"required"`
	CatchMode         string  `json:"catch_mode" form:"catch_mode" binding:"required"`
	Status            string  `json:"status" form:"status" binding:"required"`
	ShowInHome        *int8   `json:"show_in_home" form:"show_in_home" binding:"required,gte=0,lte=1"`
	ApiLink           *string `json:"api_link,omitempty" form:"api_link"`
}

type SeedQO struct {
	ID         int64  `json:"id" form:"id"`
	CatchSite  string `json:"catch_site" form:"catch_site"`
	CatchPath  string `json:"catch_path" form:"catch_path"`
	CatchLink  string `json:"catch_link" form:"catch_link"`
	Star       int    `json:"star" form:"star" binding:"gte=0"`
	Cata       int    `json:"cata" form:"cata"`
	CatchMode  string `json:"catch_mode" form:"catch_mode"`
	Status     string `json:"status" form:"status"`
	ShowInHome *int8  `json:"show_in_home" form:"show_in_home"`
	PageSize   int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
	PageNum    int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
}

func (SeedDTO) TableName() string {
	return "u_seed"
}

func (io SeedIO) IO2DTO() *SeedDTO {
	return &SeedDTO{
		Star:              io.Star,
		Cata:              "#" + strings.Join(strings.Split(io.Cata, ","), "#,#") + "#",
		CatchSite:         io.CatchSite,
		CatchPath:         io.CatchPath,
		CatchLink:         io.CatchLink,
		CatchInterval:     io.CatchInterval,
		SelectorPageLink:  io.SelectorPageLink,
		SelectorListTime:  io.SelectorListTime,
		SelectorListTitle: io.SelectorListTitle,
		CatchMode:         io.CatchMode,
		Status:            io.Status,
		ShowInHome:        io.ShowInHome,
		ApiLink:           io.ApiLink,
	}
}

func (eo SeedEO) EO2DTO() *SeedDTO {
	dto := &SeedDTO{
		Simple:            models.Simple{UpdatedAt: time.Now()},
		Star:              eo.Star,
		Cata:              "#" + strings.Join(strings.Split(eo.Cata, ","), "#,#") + "#",
		CatchSite:         eo.CatchSite,
		CatchPath:         eo.CatchPath,
		CatchLink:         eo.CatchLink,
		CatchInterval:     eo.CatchInterval,
		SelectorPageLink:  eo.SelectorPageLink,
		SelectorListTime:  eo.SelectorListTime,
		SelectorListTitle: eo.SelectorListTitle,
		CatchMode:         eo.CatchMode,
		Status:            eo.Status,
		ShowInHome:        eo.ShowInHome,
		ApiLink:           eo.ApiLink,
	}
	dto.ID = eo.ID
	return dto
}

func (dto SeedDTO) DTO2VO() *SeedVO {
	chineseNum := []string{"一", "二", "三", "四", "五", "六", "七", "八", "九"}
	vo := &SeedVO{
		ID:                dto.ID,
		Star:              dto.Star,
		Cata:              dto.Cata,
		CreatedAt:         dto.CreatedAt.Format("2006-01-02 15:04:05"),
		CatchSite:         dto.CatchSite,
		CatchPath:         dto.CatchPath,
		CatchLink:         dto.CatchLink,
		CatchInterval:     dto.CatchInterval,
		SelectorPageLink:  dto.SelectorPageLink,
		SelectorListTime:  dto.SelectorListTime,
		SelectorListTitle: dto.SelectorListTitle,
		CatchMode:         dto.CatchMode,
		Status:            dto.Status,
		ShowInHome:        *dto.ShowInHome,
		ApiLink:           dto.ApiLink,
	}
	if dto.LastCatchTime != nil {
		vo.CatchTime = dto.LastCatchTime.Format("2006-01-02 15:04:05")
	}
	if dto.Cata != "" {
		catas := strings.Split(strings.Replace(dto.Cata, "#", "", -1), ",")
		for _, cata := range catas {
			if gredis.Clone(setting.RedisSetting.SpiderDB).HExists("seed:seed_cata", cata) {
				catabody, err := gredis.Clone(setting.RedisSetting.SpiderDB).HGet("seed:seed_cata", cata)
				if err != nil {
					logging.Error("seed.DTO2VO() err:", err.Error())
				}
				var cataarr SeedCataDTO
				jerr := json.Unmarshal(catabody.([]byte), &cataarr)
				if jerr != nil {
					logging.Error("seed.DTO2VO() err:", jerr.Error())
				}
				if vo.CataCN != "" {
					vo.CataCN = vo.CataCN + ", "
				}
				vo.CataCN = vo.CataCN + cataarr.Value
			} else {
				catalist, err := GetAll()
				if err != nil {
					logging.Error("seed.DTO2VO() err:", err.Error())
				}
				for _, v := range catalist {
					jsonv, _ := json.Marshal(v)
					_ = gredis.Clone(setting.RedisSetting.SpiderDB).HSet("seed:seed_cata", strconv.Itoa(v.ID), jsonv)
					if v.ID == vo.ID {
						if vo.CataCN != "" {
							vo.CataCN = vo.CataCN + ", "
						}
						vo.CataCN = vo.CataCN + v.Value
					}
				}
			}
		}
	}
	vo.StarCN = chineseNum[(dto.Star-1)%10] + "星"
	return vo
}

func (dto SeedDTO) DTO2Seed() *spider.Seed {
	uSeed := spider.USeed{
		Simple:            dto.Simple,
		CatchSite:         dto.CatchSite,
		CatchMode:         dto.CatchMode,
		CatchLink:         dto.CatchLink,
		Cata:              dto.Cata,
		CatchPath:         dto.CatchPath,
		CatchInterval:     dto.CatchInterval,
		Star:              dto.Star,
		Status:            dto.Status,
		SelectorPageLink:  dto.SelectorPageLink,
		SelectorListTime:  dto.SelectorListTime,
		SelectorListTitle: dto.SelectorListTitle,
	}
	if dto.ApiLink != nil {
		uSeed.ApiLink = *dto.ApiLink
	}
	return spider.USeed2Seed(&uSeed)
}

func InsertSeed(io *SeedIO) error {
	dto := io.IO2DTO()
	err := models.DB().Model(SeedDTO{}).Create(&dto).Error
	if err != nil {
		logging.Error("seed.InsertSeed() err:", err.Error())
		return err
	}
	setErr := task.SaveSeed2Redis(strconv.Itoa(dto.ID), *dto.DTO2Seed())
	if setErr != nil {
		logging.Error("新增新闻源 redis记录 "+task.SeedStaticKey+" 失败", setErr.Error())
		return setErr
	}
	_, setErr = gredis.Clone(setting.RedisSetting.SpiderDB).SAdd(task.SeedOPTKey, strconv.Itoa(dto.ID))
	if setErr != nil {
		logging.Error("新增新闻源 redis记录 "+task.SeedOPTKey+" 失败", setErr.Error())
		return setErr
	}
	return nil
}

func GetSeedList(qo *SeedQO) ([]SeedDTO, int, error) {
	cnt := 0
	dto := make([]SeedDTO, 0)
	//列表不展示的catch_mode
	modeLimit := []string{"forecast", "announcement", "disclosure", "iwc", "yb-dc-list", "yb-lb-list", "yb-hb-list", "ipo-dc-list", "yb-email", "popular-stock"}
	dbs := models.DB().Model(SeedDTO{}).Select("*").Where("catch_mode NOT IN (?)", modeLimit)
	if qo.ID > int64(0) {
		dbs = dbs.Where("id = ?", qo.ID)
	}
	if qo.Star > 0 {
		dbs = dbs.Where("star = ?", qo.Star)
	}
	if qo.ShowInHome != nil {
		dbs = dbs.Where("show_in_home = ?", qo.ShowInHome)
	}
	if qo.CatchLink != "" {
		dbs = dbs.Where("catch_link like ?", "%"+qo.CatchLink+"%")
	}
	if qo.CatchPath != "" {
		dbs = dbs.Where("catch_path like ?", "%"+qo.CatchPath+"%")
	}
	if qo.CatchSite != "" {
		dbs = dbs.Where("catch_site like ?", "%"+qo.CatchSite+"%")
	}
	if qo.Status != "" {
		dbs = dbs.Where("status = ?", qo.Status)
	}
	if qo.CatchMode != "" {
		dbs = dbs.Where("catch_mode = ?", qo.CatchMode)
	}
	if qo.Cata != 0 {
		dbs = dbs.Where("cata like ?", "%#"+strconv.Itoa(qo.Cata)+"#%")
	}
	dbs.Count(&cnt)
	err := dbs.Joins("LEFT JOIN (SELECT seed_id,MAX(created_at) last_catch_time FROM u_feed GROUP BY seed_id) F ON F.seed_id=u_seed.id").Offset(qo.PageSize * (qo.PageNum - 1)).Limit(qo.PageSize).Order("id DESC").Find(&dto).Error
	if err != nil {
		logging.Error("seed.GetSeedList() err:", err.Error())
		return nil, cnt, err
	}
	return dto, cnt, nil
}

func EditSeed(eo *SeedEO) error {
	dto := eo.EO2DTO()
	var err error
	if dto.Status == "banned" {
		err = gredis.Clone(setting.RedisSetting.SpiderDB).SREM(task.SeedOPTKey, strconv.Itoa(dto.ID))
		if err != nil {
			logging.Error("seed.DelSeed() 删除opt err:", err.Error())
			return err
		}
		err = gredis.Clone(setting.RedisSetting.SpiderDB).HDel(task.SeedStaticKey, strconv.Itoa(dto.ID))
		if err != nil {
			logging.Error("seed.DelSeed() 删除static err:", err.Error())
			return err
		}
	} else {
		_, err = gredis.Clone(setting.RedisSetting.SpiderDB).SAdd(task.SeedOPTKey, strconv.Itoa(dto.ID))
		if err != nil {
			logging.Error("新增新闻源 redis记录 "+task.SeedOPTKey+" 失败", err.Error())
			return err
		}
		err = task.SaveSeed2Redis(strconv.Itoa(eo.ID), *dto.DTO2Seed())
		if err != nil {
			logging.Error("seed.EditSeed() err:", err.Error())
			return err
		}
	}
	err = models.DB().Model(SeedDTO{}).Where("id = ?", eo.ID).UpdateColumns(&dto).Error
	if err != nil {
		logging.Error("seed.EditSeed() err:", err.Error())
		return err
	}
	if dto.ApiLink == nil {
		err = models.DB().Model(SeedDTO{}).Where("id = ?", eo.ID).Update("api_link", dto.ApiLink).Error
		if err != nil {
			logging.Error("seed.EditSeed() err:", err.Error())
			return err
		}
	}
	return nil
}

func DelSeed(id int) error {
	t := time.Now()
	err := models.DB().Model(SeedDTO{}).Where("id = ?", id).UpdateColumn("deleted_at", t).Error
	remErr := gredis.Clone(setting.RedisSetting.SpiderDB).SREM(task.SeedOPTKey, strconv.Itoa(id))
	if remErr != nil {
		logging.Error("seed.DelSeed() err:", remErr.Error())
		return remErr
	}
	remErr = gredis.Clone(setting.RedisSetting.SpiderDB).HDel(task.SeedStaticKey, strconv.Itoa(id))
	if remErr != nil {
		logging.Error("seed.DelSeed() err:", remErr.Error())
		return remErr
	}
	if err != nil {
		logging.Error("seed.DelSeed() err:", err.Error())
		return err
	}
	return nil
}

func ResetSeed(id int) error {
	err := models.DB().Table("u_seed").Where("id = ?", id).Update("error_status", "normal").Error
	if err != nil {
		return err
	}
	return nil
}
