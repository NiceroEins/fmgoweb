package research_report_service

import (
	"fmt"
	"strings"
	"time"
)

type GetResearchReportRequest struct {
	Organization string `json:"organization" form:"organization"` //机构名称
	Search       string `json:"search" form:"search"`             //股票代码或名称
	KeyWord      string `json:"key_word" form:"key_word"`         //关键词
	DateBegin    string `json:"date_begin" form:"date_begin"`     //开始日期
	DateEnd      string `json:"date_end" form:"date_end"`         //结束日期
	Level        string `json:"level" form:"level"`               //相关评级
	Content      string `json:"content" form:"content"`           //研报内容
	Type         string `json:"type" form:"type"`                 //类型
	Sort         string `json:"sort" form:"sort"`                 //排序
	Page         int    `json:"page" form:"page" binding:"required"`
	PageSize     int    `json:"page_size" form:"page_size" binding:"required"`
}

type EditResearchReportRecommendRequest struct {
	Id        int    `json:"id" form:"id" binding:"required"`
	Recommend string `json:"recommend" form:"recommend" binding:"required"`
	UserId    int    `json:"user_id" form:"user_id" binding:"required"`
}

type MapField struct {
	Name   string
	Trans  string
	Format string
	Field  int
}

func Map(data map[string]interface{}) (map[string]interface{}, []string) {
	fields := []MapField{
		{
			Name:   "code",
			Trans:  "股票信息",
			Format: "",
			Field:  1,
		},
		{
			Name:   "name",
			Trans:  "股票名称",
			Format: "",
			Field:  1,
		},
		{
			Name:   "organization",
			Trans:  "机构名称",
			Format: "",
			Field:  1,
		},
		{
			Name:   "author",
			Trans:  "作者",
			Format: "",
			Field:  1,
		},
		{
			Name:   "level",
			Trans:  "评级",
			Format: "",
			Field:  1,
		},
		{
			Name:   "title",
			Trans:  "观点",
			Format: "",
			Field:  1,
		},
		{
			Name:   "pre_close",
			Trans:  "昨收价",
			Format: "",
			Field:  1,
		},
		{
			Name:   "target_price",
			Trans:  "目标价",
			Format: "",
			Field:  2,
		},
		{
			Name:   "rising_space",
			Trans:  "上升空间",
			Format: "",
			Field:  2,
		},
		{
			Name:   "expected_net_profit",
			Trans:  "预测净利润",
			Format: "",
			Field:  3,
		},
		{
			Name:   "forecast_eps",
			Trans:  "预测EPS",
			Format: "",
			Field:  3,
		},
		{
			Name:   "price_earning_ratio",
			Trans:  "预测PE",
			Format: "",
			Field:  3,
		},
		{
			Name:   "heat",
			Trans:  "热度",
			Format: "",
			Field:  1,
		},
		{
			Name:   "spider_time",
			Trans:  "日期",
			Format: "time",
			Field:  1,
		},
	}
	//var trans = []string {
	//	"股票信息",
	//	"机构名称",
	//	"作者",
	//	"评级",
	//	"观点",
	//	"昨收价",
	//	"目标价",
	//	"上升空间",
	//	"预测净利润",
	//	"预测EPS",
	//	"预测PE",
	//	"热度",
	//	"日期",
	//}
	var trans []string
	ret := make(map[string]interface{})
	for _, v := range fields {
		bMul := v.Field > 1
		for k := 0; k < v.Field; k++ {
			ct := v.Trans
			if bMul {
				ct += fmt.Sprintf("-%d", k+1)
			}
			trans = append(trans, ct)

			if field, ok := data[v.Name]; ok {
				if str, b := field.(string); !b {
					if k == 0 {
						ret[ct] = field
					}
					continue
				} else {
					var sp []string
					if v.Field > 1 {
						sp = strings.Split(str, ",")
					} else {
						sp = append(sp, str)
					}
					if len(sp) <= k {
						ret[ct] = nil
						continue
					}
					spc := sp[k]
					switch v.Format {
					case "":
						ret[ct] = spc
					case "time":
						t, err := time.ParseInLocation(time.RFC3339, spc, time.Local)
						if err != nil {
							ret[ct] = spc
						} else {
							ret[ct] = t.Format("2006-01-02 15:04:05")
						}
					}
				}
			} else {
				ret[ct] = nil
			}
		}
	}

	return ret, trans
}
