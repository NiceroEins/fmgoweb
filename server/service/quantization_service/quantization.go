package quantization_service

import (
	"fmt"
	"strings"
	"time"
)

type MapField struct {
	Name   string
	Trans  string
	Format string
	Field  int
}

func Map(data map[string]interface{}) (map[string]interface{}, []string) {
	fields := []MapField{
		{
			Name:   "code",
			Trans:  "股票代码",
			Format: "",
			Field:  1,
		},
		{
			Name:   "name",
			Trans:  "股票简称",
			Format: "",
			Field:  1,
		},
		{
			Name:   "begin",
			Trans:  "起始时间",
			Format: "day",
			Field:  1,
		},
		{
			Name:   "end",
			Trans:  "终止时间",
			Format: "day",
			Field:  1,
		},
		{
			Name:   "price_begin",
			Trans:  "起始价",
			Format: "",
			Field:  1,
		},
		{
			Name:   "price_end",
			Trans:  "终止价",
			Format: "",
			Field:  1,
		},
		{
			Name:   "change",
			Trans:  "区间涨跌幅",
			Format: "",
			Field:  1,
		},
	}

	var trans []string
	ret := make(map[string]interface{})
	for _, v := range fields {
		bMul := v.Field > 1
		for k := 0; k < v.Field; k++ {
			ct := v.Trans
			if bMul {
				ct += fmt.Sprintf("-%d", k+1)
			}
			trans = append(trans, ct)

			if field, ok := data[v.Name]; ok {
				if str, b := field.(string); !b {
					if k == 0 {
						ret[ct] = field
					}
					continue
				} else {
					var sp []string
					if v.Field > 1 {
						sp = strings.Split(str, ",")
					} else {
						sp = append(sp, str)
					}
					if len(sp) <= k {
						ret[ct] = nil
						continue
					}
					spc := sp[k]
					switch v.Format {
					case "":
						ret[ct] = spc
					case "time":
						t, err := time.ParseInLocation(time.RFC3339, spc, time.Local)
						if err != nil {
							ret[ct] = spc
						} else {
							ret[ct] = t.Format("2006-01-02 15:04:05")
						}
					case "day":
						t, err := time.ParseInLocation(time.RFC3339, spc, time.Local)
						if err != nil {
							ret[ct] = spc
						} else {
							ret[ct] = t.Format("2006-01-02")
						}
					}
				}
			} else {
				ret[ct] = nil
			}
		}
	}

	return ret, trans
}
