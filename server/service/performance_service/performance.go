package performance_service

import "time"

type SearchIndustryTrackRequest struct {
	IsFirst                 string `json:"is_first" form:"is_first"`
	Sort                    string `json:"sort" form:"sort"`
	ReportPeriod            string `json:"report_period" form:"report_period" binding:"required"`                         //报告期(公告表里面的报告期)
	ForecastIncreaseDay     int    `json:"forecast_increase_day" form:"forecast_increase_day" binding:"required"`         //预告涨幅( 1,3,5 天)
	AnnouncementIncreaseDay int    `json:"announcement_increase_day" form:"announcement_increase_day" binding:"required"` //公告涨幅 (1,3,5 天)
	Search                  string `form:"search" json:"search" `                                                         //搜索:根据股票名称,代码,行业内容搜索(公告表+行业表里面)
	UserId                  string `json:"user_id" form:"user_id"`                                                        //用户id(行业表里面的用户id)
	Page                    int    `json:"page" form:"page" binding:"required"`
	PageSize                int    `json:"page_size" form:"page_size" binding:"required"`
}

//数据库返回数据绑定结构体
type GetPerformanceMysql struct {
	ID        int        `gorm:"primary_key" json:"id,omitempty"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" sql:"index"`
	//股票信息
	Name string `json:"name"`
	//股票代码
	Code string `json:"code"`
	//披露时间
	ExpectedPublishDate   *string `json:"expected_publish_date"`
	DisclosureTimeHistory string  `json:"disclosure_time_history"`
	DisclosureChangeAt    string  `json:"disclosure_change_at"`

	//预测比例(上下限)
	ForecastProportionLower *float64 `json:"forecast_proportion_lower"`
	ForecastProportionUpper *float64 `json:"forecast_proportion_upper"`

	//预测净利润
	ForecastNetProfitLower *float64 `json:"forecast_net_profit_lower"`
	ForecastNetProfitUpper *float64 `json:"forecast_net_profit_upper"`

	//券商预测(上下限)
	BrokerProportionLower *float64 `json:"broker_proportion_lower"`
	BrokerProportionUpper *float64 `json:"broker_proportion_upper"`

	//券商解读
	BrokerInterpretation       string `json:"broker_interpretation"`
	BrokerInterpretationSub100 string `json:"broker_interpretation_sub_100"`

	//亮点
	HighLights string `json:"high_lights"`
	//亮点截取100个字,后面 + ...
	HighLightsSub100 string `json:"high_lights_sub_100"`

	//本期预告(业绩变动幅度上限)
	PerformanceIncrUpper *float64 `json:"performance_incr_upper"`
	//本期预告(业绩变动幅度下限)
	PerformanceIncrLower *float64 `json:"performance_incr_lower"`

	//净利润
	NetProfit *float64 `json:"net_profit"`
	//净利润同比
	NetProfitGrowthRate *float64 `json:"net_profit_growth_rate"`

	//营业总收入
	BusinessRevenue *float64 `json:"business_revenue"`

	//营业总收入同比
	BusinessRevenueGrowthRate *float64 `json:"business_revenue_growth_rate"`
	//扣非净利润
	NetProfitAfterDed *float64 `json:"net_profit_after_ded"`

	//重要程度
	Important string `json:"important"`
	//姓名
	UserName string `json:"user_name"`
	//展真实姓名
	RealName string `json:"real_name"`
	//行业
	Industry     string `json:"industry"`
	IndustryName string `json:"industry_name"`
	//行业解读
	IndustryInterpretation string `json:"industry_interpretation"`
	//业绩变动原因
	PerformanceReason string `json:"performance_reason"`
	//类型
	Type      string `json:"type"`
	Recommend string `json:"recommend"`
}
