package keyword_service

//分页参数
type Page struct {
	Page     int `json:"page" form:"page" binding:"required"`
	PageSize int `json:"page_size" form:"page_size" binding:"required"`
}

type KeywordListRequest struct {
	Keyword   string `json:"keyword" form:"keyword"`       //关键词
	Category  string `json:"category" form:"category"`     //分类
	Star      int    `json:"star" form:"star"`             //重要等级
	CreatorId int    `json:"creator_id" form:"creator_id"` //创建者id
	Status    string `json:"status" form:"status"`         //状态
	Case      string `json:"case" form:"case"`             //应用场景
	Page
}

type AddKeywordRequest struct {
	Id        int    `json:"id" form:"id"`
	Keyword   string `json:"keyword" form:"keyword" binding:"required"`       //关键词
	Category  string    `json:"category" form:"category" binding:"required"`     //分类
	Star      int    `json:"star" form:"star" binding:"required"`             //重要等级
	CreatorId int    `json:"creator_id" form:"creator_id" binding:"required"` //创建者id
	Status    string `json:"status" form:"status" binding:"required"`         //状态
	Case      string `json:"case" form:"case" binding:"required"`             //应用场景
}

type DelKeywordRequest struct {
	Id int `json:"id" form:"id" binding:"required"`
}
