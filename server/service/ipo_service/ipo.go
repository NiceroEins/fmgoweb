package ipo_service

type IpoListRequest struct {
	Order string `json:"order" form:"order" binding:"required"`
	Page     int `json:"page" form:"page" binding:"required"`
	PageSize int `json:"page_size" form:"page_size" binding:"required"`
}
