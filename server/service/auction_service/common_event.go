package auction_service

import (
    "datacenter/models"
    "datacenter/pkg/logging"
)

type CommonEvent struct {
    models.Simple
    UserID         string `json:"user_id"`
    ObjectID       int64  `json:"object_id"`
    ObjectType     string `json:"object_type"`
    EventType      string `json:"event_type"`
}

func (CommonEvent) TableName() string {
    return "u_common_event"
}

func CreateEvent(event *CommonEvent) error {
    if err := models.DB().Create(event).Error; err != nil {
        logging.Error("event.CreateEvent() Errors: ", err.Error())
        return err
    }
    return nil
}
