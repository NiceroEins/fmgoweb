package auction_service

import (
	"datacenter/models/template"
	"time"
)

type Auction struct {
	ID                int
	Status            int
	OfferCount        int
	UserID            string
	Title             string
	Link              string
	Platform          string
	KeyWord           string
	Code              string

	Start             time.Time
	End               time.Time
	PageNum            int
	PageSize           int
	MaxID              int
}

func (c *Auction) GetAuctionLists() ([]*template.AuctionResponse, error) {
	var (
		calendarsList []*template.AuctionResponse
	)
	maps := make(map[string]interface{})
	maps["start"] = c.Start
	maps["end"] = c.End
	maps["title"] = c.Title
	maps["page_num"] = c.PageNum
	maps["page_size"] = c.PageSize
	maps["platform"] = c.Platform
	maps["max_id"]   = c.MaxID
	maps["status"]   = c.Status
	maps["user_id"]   = c.UserID
	calendarsList,err := template.GetAuctionLists(maps)
	if err != nil {
		return calendarsList,err
	}
	return calendarsList, nil
}
func (c *Auction) Count() (int, error) {
	maps := make(map[string]interface{})
	maps["start"] = c.Start
	maps["end"] = c.End
	maps["title"] = c.Title
	maps["page_num"] = c.PageNum
	maps["platform"] = c.Platform
	maps["status"]   = c.Status
	return template.GetAuctionTotal(maps)
}
