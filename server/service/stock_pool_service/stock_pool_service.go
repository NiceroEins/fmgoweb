package stock_pool_service

import (
	"datacenter/models"
	"datacenter/models/stock"
	"datacenter/models/template"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"github.com/shopspring/decimal"
	"strconv"
	"strings"
	"time"
)
const  DELETE_STATU      = 1     //个人股池删除
const  RECOMMEND_STAUTS  = 2     //推荐加入个人股池
const  RECOMMEND_DEL_STATUS = 3  //推荐加入个人股池删除
const  NORMAL_STATUS   = 0      //用户推荐的
var (
	currentPriceKey           = "stock:tick"
)
type StockPoolListRequest struct {
	Page     int `json:"page" form:"page" binding:"required"`
	PageSize int `json:"page_size" form:"page_size" binding:"required"`
}

type StockRecommondForm struct {
	Code               string `form:"code" binding:"required" json:"code"`
	RecommendReason    string `form:"recommend_reason" binding:"required" json:"recommend_reason"`
	UserId             string `form:"user_id" binding:"required" json:"user_id"`
}

type DelRecommondForm struct {
	ID         int    `form:"id" json:"id"`
	DelReason  string `form:"del_reason" json:"del_reason"`
}

type EditRecommondForm struct {
	ID               int    `form:"id" binding:"required" json:"id"`
	RecommendReason  string `form:"recommend_reason" binding:"required" json:"recommend_reason"`
}

type StockPoolManage struct {
	Id                         int                `gorm:"column:id"`                               //个人股票管理表
	Code                       string             `gorm:"column:code"`                             //股票代码
	UserId                     string             `gorm:"column:user_id"`                          //用户Id
	RecommendReason            string             `gorm:"column:recommend_reason"`                 //推荐理由
	DelReason                  string             `gorm:"column:del_reason"`                       //删除理由
	Status                     int                `gorm:"column:status"`                           //状态
	TransferPrice          decimal.NullDecimal    `gorm:"column:transfer_price" json:"-"`
}
type Stock struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
func (t *StockPoolManage) ExistByCode() (bool, error) {
	return template.ExistStockRecommondByCode(t.Code,t.UserId,t.Status)
}

func (t *StockPoolManage) ExistByID() (bool, error) {
	return template.ExistStockRecommondByID(t.Id)
}


func (t *StockPoolManage) CheckByCode() (bool, error)   {
	return template.StockRecommondByCodeNum(t.UserId,t.Status)
}

func (t *StockPoolManage) Add() error {
	return template.AddStockRecommend(t.Code, t.UserId, t.RecommendReason,t.Status)
}

func (t *StockPoolManage) Del() error {
	data := make(map[string]interface{})
	stockManage := StockPoolManage{}
	err := models.DB().Table("b_stock_pool_manage").Where("id = ?", t.Id).Find(&stockManage).Error
	if err != nil {
		logging.Info("stock_pool.Edit() ,Errors::",err)
		return err
	}
	data["status"] = t.Status
	if stockManage.Status == RECOMMEND_STAUTS {
		data["status"] = RECOMMEND_DEL_STATUS
	}
	currentPriceKey   := "stock:tick"
	data["transfer_price_now"] = template.GetRedisValue(currentPriceKey, stockManage.Code)
	currentPriceIndexKey := "stock:ts_tick"
	data["index_transfer_now"] = template.GetRedisValue(currentPriceIndexKey, stock.GetIndexCode(stockManage.Code))
	data["del_reason"] = t.DelReason
	return template.EditStockRecommend(t.Id, data)
}

func (t *StockPoolManage) Edit() error {
	data := make(map[string]interface{})
	stockManage := StockPoolManage{}
	err := models.DB().Table("b_stock_pool_manage").Where("id = ?", t.Id).Find(&stockManage).Error
	if err != nil {
		logging.Info("stock_pool.Edit() ,Errors::",err)
		return err
	}
	data["recommend_reason"] = t.RecommendReason
	return template.EditStockRecommend(t.Id, data)
}

func GenRecommendStock(content,uid string) bool  {
	// 查找内容
	dto := []Stock{}
	err := models.DB().Table("b_stock").Find(&dto).Error;
	if err != nil {
		logging.Error("stock.GenRecommendStock() err:", err.Error())
	}
	stockName := util.SubString(content,0,20)   //截取前20个字
	for _,v := range dto {
		if !strings.Contains(stockName,v.Name){
			continue
		}
		stockService := StockPoolManage{
			Code:            v.ID,
			RecommendReason: content,
			UserId:          uid,
			Status:          RECOMMEND_STAUTS,
		}

		exists, err := stockService.ExistByCode()
		if err != nil ||  exists{
			logging.Error("stock.GenRecommendStock() err:", err)
			continue
		}
		userID,_ := strconv.Atoi(uid)
		err = template.AddUserStockPool(v.ID,"stock_pool","",userID)
		if err != nil {
			logging.Error("user_stock_pool.Add() err:", err.Error())
		}
		err = stockService.Add()
		if err != nil {
			logging.Error("stock.GenRecommendStock() err:", err.Error())
			continue
		}
	}
	return true
}

func UpdateTransferPrice()  {
	stockPoolManages := []StockPoolManage{}
	stockTickDto := struct {
		Created     time.Time       `json:"trade_date" gorm:"column:trade_date"`
	}{}
	err := models.DB().Table("b_trade_date").
		Where("trade_date < ?",time.Now().Format("2006-01-02")).Order("id desc").First(&stockTickDto).Error
	if err != nil {
		logging.Info("stock.UpdateTransferPrice() err:", err.Error())
		return
	}
	err = models.DB().Table("b_stock_pool_manage").
		Where("created_at > ? ",stockTickDto.Created.Format("2006-01-02")+" 15:00:00").
		Where("status != ?",DELETE_STATU).
		Find(&stockPoolManages).Error
	if err != nil {
		logging.Info("stock.GenRecommendStock() err:", err)
		return
	}
	for _,v := range stockPoolManages {
		tranferPrice,err := decimal.NewFromString(template.GetRedisValue(currentPriceKey, v.Code))
		if err != nil {
			logging.Error("stock.GenRecommendStock() err:", err)
			continue
		}
		err = models.DB().Table("b_stock_pool_manage").Where("id = ?",v.Id).
			Update(StockPoolManage{TransferPrice:decimal.NullDecimal{Decimal:tranferPrice,Valid: true} }).Error
		if err != nil {
			logging.Info("stock.GenRecommendStock() err:", err)
			continue
		}
	}
}