package user_service

type GetUserInfoRequest struct {
	UserName string `json:"user_name" form:"user_name"`
}
type EditStatusRequest struct {
	UserId int    `json:"user_id" form:"user_id" binding:"required"`
	Status string `json:"status" form:"status" binding:"required,oneof=normal banned"`
}

type EditPwdRequest struct {
	UserId int `json:"user_id" form:"user_id" binding:"required,number"`
}

type EditUserRequest struct {
	UserId    int    `json:"user_id" form:"user_id" binding:"required"`     //账号(数据库中用户名)
	RealName  string `json:"real_name" form:"real_name" binding:"required"` //姓名(数据库中真实姓名)
	Mobile    string `json:"mobile" form:"mobile"`                          //手机号
	Privilege string `json:"privilege" form:"privilege" binding:"required"` //权限,eg:1,2,3,4
}

type EditUserPwdRequest struct {
	UserId int    `json:"user_id" form:"user_id" binding:"required"`
	OldPwd string `json:"old_pwd" form:"old_pwd" binding:"required"`
	NewPwd string `json:"new_pwd" form:"new_pwd" binding:"required"`
}
