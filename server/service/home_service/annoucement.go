package home_service

import (
	"datacenter/models/home"
)

type AnnouncementType struct {
}

func (a *AnnouncementType) GetAnnouncementType() ([]home.AnnouncemenTypeList, error) {
	return home.GetAnnouncementType()
}
