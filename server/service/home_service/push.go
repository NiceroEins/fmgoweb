package home_service

import (
	"datacenter/models/template"
	"datacenter/pkg/logging"
	"time"
)

type StockPush struct {
	ID       int
	UserId   int
	Code     string
	CodeName string
	EventStr string
	UserName string
	Start    string
	End      string

	SearchType string
	PageNum    int
	PageSize   int
}

func (c *StockPush) GetStockPushLists() ([]*template.PushResponse, error) {
	var (
		pushResponse []*template.PushResponse
	)
	pushResponse, err := template.GetStockPushLists(getMaps(c))
	if err != nil {
		return pushResponse, err
	}
	return pushResponse, nil
}
func (c *StockPush) GetStockPushListsCount() (int, error) {
	return template.GetStockPushListsCount(getMaps(c))
}
func (c *StockPush) GetAnnouncementPushLists() ([]*template.PushAnnouncementResponse, error) {
	var (
		pushResponse []*template.PushAnnouncementResponse
	)
	pushResponse, err := template.GetAnnouncementPushLists(getMaps(c))
	if err != nil {
		return pushResponse, err
	}
	return pushResponse, nil
}
func (c *StockPush) GetAnnouncementPushListsCount() (int, error) {
	return template.GetAnnouncementPushListsCount(getMaps(c))
}
func (c *StockPush) GetReportPushLists() ([]*template.PushReportResponse, error) {
	var (
		pushResponse []*template.PushReportResponse
	)
	pushResponse, err := template.GetReportPushLists(getMaps(c))
	if err != nil {
		return pushResponse, err
	}
	return pushResponse, nil
}
func (c *StockPush) GetReportPushListsCount() (int, error) {
	return template.GetReportPushListsCount(getMaps(c))
}
func getMaps(c *StockPush) map[string]interface{} {
	maps := make(map[string]interface{})
	maps["user_id"] = c.UserId
	maps["code_name"] = c.CodeName
	maps["start"] = c.Start
	maps["end"] = c.End
	maps["search_type"] = c.SearchType
	maps["page_size"] = c.PageSize
	maps["page_num"] = c.PageNum
	return maps
}

// 行业管理
type IndustryNamesUser struct {
	ID            int
	UserId        int
	IndustryNames string
	UserName      string
}
type IndustryNameAdd struct {
	UserId         int
	UserName       string
	FirstIndustry  string
	SecondIndustry string
	IndustryID     int
}

func (i *IndustryNamesUser) GetLists() ([]template.IndustryResponse, error) {
	var (
		industyNames []template.IndustryResponse
	)
	industyNames, err := template.GetIndustryNamesLists()
	if err != nil {
		return industyNames, err
	}
	return industyNames, nil
}

// 行业添加
func (i *IndustryNameAdd) AddIndustry() (bool, error) {
	user := template.IndustryNamesAdd{
		UserName:       i.UserName,
		UserID:         i.UserId,
		SecondIndustry: i.SecondIndustry,
		FirstIndustry:  i.FirstIndustry,
		IndustryId:     i.IndustryID,
	}
	err := template.AddIndustyNames(user)
	if err != nil {
		logging.Info("home_service.editindustry err is ", err)
		return false, err
	}
	return true, nil
}

// 行业删除
func (i *IndustryNamesUser) DelIndustry() error {
	return template.DelIndustyNames(i.UserId)
}

// 获取行业数据
func (i *IndustryNamesUser) GetIndustryNames(username string) (template.IndustryNamesUser, error) {
	return template.GetIndustryNames(username)
}

// 工作统计
type Staticstic struct {
	UserID               int
	PageSize             int
	PageNum              int
	SearchStaticsticDate time.Time
}

// 获取用户行业
func (i *Staticstic) GetStatisticData() []template.Staticstic {
	maps := make(map[string]interface{})
	maps["start"] = i.SearchStaticsticDate.Format("2006-01-02 00:00:00")
	maps["end"] = i.SearchStaticsticDate.AddDate(0, 0, 1).Format("2006-01-02 00:00:00")
	maps["page_size"] = i.PageSize
	maps["page_num"] = i.PageNum
	maps["user_id"] = i.UserID
	return template.GetWorkStaticstic(maps)
}

// 获取用户总数
func (i *Staticstic) GetStatisticsCount() int {
	maps := make(map[string]interface{})
	maps["user_id"] = i.UserID
	return template.GetStatiscticCount(maps)
}

// 获取研报详情
func (i *Staticstic) GetStatisticsDetail() []template.StaticsticDetail {
	maps := make(map[string]interface{})
	maps["user_id"] = i.UserID
	maps["page_size"] = i.PageSize
	maps["page_num"] = i.PageNum
	maps["start"] = i.SearchStaticsticDate.Format("2006-01-02 00:00:00")
	maps["end"] = i.SearchStaticsticDate.AddDate(0, 0, 1).Format("2006-01-02 00:00:00")
	return template.GetUserStatistic(maps)
}

// 获取用户总数
func (i *Staticstic) GetStatisticsDetailCount() int {
	maps := make(map[string]interface{})
	maps["user_id"] = i.UserID
	maps["start"] = i.SearchStaticsticDate.Format("2006-01-02 00:00:00")
	maps["end"] = i.SearchStaticsticDate.AddDate(0, 0, 1).Format("2006-01-02 00:00:00")
	return template.GetUserStatisticCount(maps)
}
