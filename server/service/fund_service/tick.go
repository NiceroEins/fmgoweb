package fund_service

import (
    "datacenter/models/template"
    "errors"
    "github.com/shopspring/decimal"
    "time"
)

type FundTick struct {
    ID                   int
    Status               int       // 状态  0：不分红；1：分红
    FundID               int       // 基金ID
    Day                  int       // 获取时长
    Name                 string    // 名称
    UnitValue            string    // 单位净值
    Value                string    // 累计净值
    Created              time.Time // 创建时间
    ValueDate            time.Time // 记录日期

    PageNum  int
    PageSize int
}


func (f *FundTick) GetLists() ([]template.FundTick, error) {
    var (
        funds []template.FundTick
    )
    funds, err := template.GetFundTicks(f.FundID,-1)
    if err != nil {
        return nil, err
    }
    return funds, nil
}

func (f *FundTick) ExistByFundId() (int, error) {
    return template.ExistRecord(f.FundID,f.ValueDate)
}

func (f *FundTick) ExistByID() (bool, error) {
    return template.ExistRecordById(f.ID)
}

func (f *FundTick) Add() error {
    unitValue,err  := decimal.NewFromString(f.UnitValue)
    if err != nil {
        return errors.New("参数不合法")
    }
    value,err  := decimal.NewFromString(f.Value)
    if err != nil {
        return errors.New("参数不合法")
    }
    data := template.FundTick{
       FundId: f.FundID,
       Status: f.Status,
       ValueDate: f.ValueDate,
       Created: f.Created,
       Value: value,
       UnitValue: unitValue,
    }
    return template.AddFundTick(data)
}

func (f *FundTick) Edit() error {
    data := make(map[string]interface{})
    data["value"]       = f.Value
    data["unit_value"]  = f.UnitValue
    data["status"]      = f.Status
    data["fund_id"]     = f.FundID
    return template.EditFundTicks(f.ID, data)
}

func (f *FundTick) Del() error {
    return template.DeleteFundTicks(f.ID)
}

func (f *FundTick) GetFundTickByFundID() ([]template.FundTick, error) {
    var (
        funds []template.FundTick
    )
    funds, err := template.GetFundTicks(f.FundID,f.Day)
    if err != nil {
        return nil, err
    }
    return funds, nil
}
func (f *FundTick) GetFundTickDateByFundID(ticks []template.FundTick) (maxValueDate,minValueDate decimal.Decimal) {
    return template.GetSpecialUnitValue(ticks)
}
func (f *FundTick) GetFundTickDateOne() ([]template.FundTick,error) {
    data := make(map[string]interface{})
    data["fund_id"] = f.FundID
    data["status"]  = 1
    data["day"] = f.Day
    return template.GetLastCashRecordByFundId(data)
}


