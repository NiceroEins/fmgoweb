package fund_service

import (
    "datacenter/models/template"
    "datacenter/pkg/logging"
    "datacenter/pkg/util"
    "fmt"
)

type Customer struct {
    ID                   int
    Realname             string     // 姓名
    IdCardNo             string     // 身份证号
    UserName             string     // 登录账号
    Type                 string     // 证件类型
    Value                string     // 累计净值
    Created              string     // 创建时间
    LoginPwd             string     // 登录密码
    OldPwd               string     // 旧登录密码

    PageNum  int
    PageSize int
}


func (c *Customer) GetLists() ([]template.Customer, error) {
    var (
        customers []template.Customer
    )
    data := template.Customer{
        UserName: c.UserName,
        IdCardNo: c.IdCardNo,
        Realname: c.Realname,
    }
    customers, err := template.GetCustomers(c.PageNum, c.PageSize,data)
    if err != nil {
        return nil, err
    }
    return customers, nil
}

func (c *Customer) Exist() (bool, error) {
    return template.ExistCustomer(c.getMaps())
}

func (c *Customer) Add() error {
    data := template.Customer{
        Created: c.Created,
        Realname: c.Realname,
        UserName: c.UserName,
        IdCardNo: c.IdCardNo,
        LoginPwd: util.EncodeMD5("201712251128678"+util.EncodeMD5(c.UserName)),
    }
    return template.AddCustomer(data)
}

func (c *Customer) Edit() error {
    data := make(map[string]interface{})
    data["realname"]    = c.Realname
    data["username"]    = c.UserName
    data["id_card_no"]  = c.IdCardNo
    return template.EditCustomer(c.ID, data)
}

func (c *Customer) Count() (int, error) {
    data := template.Customer{
        UserName: c.UserName,
        IdCardNo: c.IdCardNo,
        Realname: c.Realname,
    }
    return template.GetCustomerTotal(data)
}

func (c *Customer) Del() error {
    data := make(map[string]interface{})
    data["deleted"]    = 1
    return template.EditCustomer(c.ID, data)
}

func (c *Customer) getMaps() map[string]interface{} {
    maps := make(map[string]interface{})
    if c.Realname != "" {
        maps["username"] = c.UserName
    }
    if c.ID > 0 {
        maps["id"] = c.ID
    }

    return maps
}

func (c *Customer) ExistByName() (bool, error) {
    return template.ExistCustomerByName(c.Realname,c.IdCardNo)
}

func (c *Customer) ExistByNameAndNo() (bool, error) {
    data := make(map[string]interface{})
    data["realname"]    = c.Realname
    data["id_card_no"]  = c.IdCardNo
    data["username"]    = c.UserName
    return template.ExistCustomerByNameAndId(data)
}

func (f *Customer) GetNames() ([]template.CustomerNames, error) {
    var (
        customers []template.CustomerNames
    )
    customers, err := template.GetCustomersNames(f.Realname)
    if err != nil {
        return nil, err
    }
    return customers, nil
}

func (f *Customer) ResetPwd() error {
    result := make(map[string]interface{})
    result["id"]    = f.ID
    data,err := template.GetCustomer(result)
    if err != nil {
        return err
    }
    dataMap := make(map[string]interface{})
    dataMap["login_pwd"]   = util.EncodeMD5("201712251128678"+util.EncodeMD5(data.UserName))
    return template.EditCustomer(data.ID, dataMap)
}


func (c *Customer) Check() (template.Customer, error) {
    data := make(map[string]interface{})
    data["username"] = c.UserName
    data["login_pwd"] = util.EncodeMD5("201712251128678"+c.LoginPwd)
    return template.Check(data)
}

func (c *Customer) EditPwd() (error) {
    data := make(map[string]interface{})
    data["username"] = c.UserName
    data["login_pwd"] = util.EncodeMD5("201712251128678"+c.OldPwd)
    customer,err := template.Check(data)
    if err != nil {
        logging.Error("front.EditPwd() err:", err.Error())
        return fmt.Errorf("旧密码不对")
    }
    edit := make(map[string]interface{})
    edit["login_pwd"] = util.EncodeMD5("201712251128678"+c.LoginPwd)
    return template.EditPwd(customer.ID,edit)
}


