package fund_service

import (
    "datacenter/models/template"
    "time"
)

type FundNotice struct {
    ID                 int
    Title              string         // 标题
    Content            string         // 内容
    Author             string         // 作者
    FundID             int            // 基金ID
    FundName           string         // 关联基金
    Created            time.Time      // 创建时间
    PublishDate        time.Time      // 展示时间


    PageNum  int
    PageSize int
}

func (c *FundNotice) GetLists() ([]template.FundNoticeResponse, error) {
    var (
        fundNotices []template.FundNoticeResponse
    )
    fundNotices, err := template.GetFundNotice()
    if err != nil {
        return nil, err
    }
    return fundNotices, nil
}

func (c *FundNotice) Exist() (bool, error) {
    data := make(map[string]interface{})
    data["id"]      = c.ID
    return template.ExistFundNotice(data)
}

func (c *FundNotice) Add() error {
    data := template.FundNotice{
        Created: c.Created,
        FundID: c.FundID,
        Title: c.Title,
        Content: c.Content,
        PublishDate: c.PublishDate,
        Author: c.Author,
    }
    return template.AddFundNotice(data)
}

func (c *FundNotice) Edit() error {
    data := make(map[string]interface{})
    data["fund_id"]      = c.FundID
    data["title"]        = c.Title
    data["content"]      = c.Content
    data["publish_date"] = c.PublishDate
    data["author"]       = c.Author
    return template.EditFundNotice(c.ID, data)
}

func (c *FundNotice) Del() error {
    data := make(map[string]interface{})
    data["deleted"]      = 1
    return template.EditFundNotice(c.ID, data)
}

func (c *FundNotice) getMaps() map[string]interface{} {
    maps := make(map[string]interface{})
    if c.FundID > 0  {
        maps["fund_id"] = c.FundID
    }
    if c.Title != "" {
        maps["title"] = c.Title
    }
    return maps
}

func (c *FundNotice) ExistByID() (bool, error) {
    maps := make(map[string]interface{})
    if c.ID > 0  {
        maps["id"] = c.ID
    }
    return template.ExistFundNotice(maps)
}

func (c *FundNotice) ExistByFund()  (bool, error) {
    if c.FundID > 0 {
        maps := make(map[string]interface{})
        maps["id"] = c.FundID
        return template.ExistFundByID(maps)
    }
    return true , nil
}


func (c *FundNotice) GetAllCustomerLists() ([]template.CustomerFundNotice, error) {
    var (
        fundNotices []template.CustomerFundNotice
    )
    maps := make(map[string]interface{})
    maps["fund_id"] = 0
    fundNotices, err := template.GetNotice(maps)
    if err != nil {
        return nil, err
    }
    return fundNotices, nil
}

func (c *FundNotice) GetNoticeByFund() ([]template.CustomerFundNotice, error) {
    var (
        fundNotices []template.CustomerFundNotice
    )
    maps := make(map[string]interface{})
    maps["fund_id"] = c.FundID
    fundNotices, err := template.GetNotice(maps)
    if err != nil {
        return nil, err
    }
    return fundNotices, nil
}

func (c *FundNotice) GetNoticeContentByFundID() (template.FundNoticeContent, error) {
    var (
        fundNotice template.FundNoticeContent
    )
    maps := make(map[string]interface{})
    maps["id"] = c.ID
    fundNotice, err := template.GetNoticeContent(maps)
    if err != nil {
        return fundNotice, err
    }
    return fundNotice, nil
}