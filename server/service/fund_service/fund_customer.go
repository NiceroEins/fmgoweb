package fund_service

import (
    "datacenter/models/template"
    "github.com/360EntSecGroup-Skylar/excelize"
    "github.com/shopspring/decimal"
    "io"
    "time"
)

type FundCustomer struct {
    ID                   int
    FundID               int        // 基金id
    UserID               int        // 用户ID
    Realname             string     // 姓名
    FundName             string     // 基金名称
    Updated              string     // 更新时间
    Created              string     // 创建时间
    Num                  string     // 基金份额

    PageNum  int
    PageSize int
}

func (c *FundCustomer) GetLists() ([]template.FundCustomerResponse, error) {
    var (
        customers []template.FundCustomerResponse
    )
    customers, err := template.GetFundCustomers(c.PageNum, c.PageSize,c.getMaps(),c.Realname)
    if err != nil {
        return nil, err
    }
    return customers, nil
}

func (c *FundCustomer) Exist() (bool, error) {
    return template.ExistCustomer(c.getMaps())
}

func (c *FundCustomer) Add() error {
    data := template.FundCustomerAdd{
        Created: c.Created,
        FundID: c.FundID,
        UserID: c.UserID,
        Num: c.Num,
        Updated: c.Updated,
    }
    return template.AddFundCustomer(data)
}

func (c *FundCustomer) Edit() error {
    data := make(map[string]interface{})
    data["fund_id"]    = c.FundID
    data["owner_id"]    = c.UserID
    data["num"]  = c.Num
    return template.EditFundCustomer(c.ID, data)
}

func (c *FundCustomer) Count() (int, error) {
    return template.GetFundCustomerTotal(c.getMaps(),c.Realname)
}

func (c *FundCustomer) Del() error {
    data := make(map[string]interface{})
    data["deleted"]    = 1
    return template.EditFundCustomer(c.ID, data)
}

func (c *FundCustomer) getMaps() map[string]interface{} {
    maps := make(map[string]interface{})
    if c.FundID > 0  {
        maps["fund_id"] = c.FundID
    }
    if c.UserID > 0 {
        maps["owner_id"] = c.UserID
    }

    return maps
}

func (c *FundCustomer) ExistByName() (bool, error) {
    return template.ExistFundCustomer(c.getMaps())
}

func (c *FundCustomer) ExistByFund()  (bool, error) {
    maps := make(map[string]interface{})
    maps["id"] = c.FundID
    return template.ExistFundByID(maps)
}

func (c *FundCustomer) ExistByCustomer()  (bool, error) {
    maps := make(map[string]interface{})
    maps["id"] = c.UserID
    return template.ExistCustomer(maps)
}
func (c *FundCustomer) CheckFundCostmer()  (bool, error) {
    return template.CheckFundCostmer(c.FundID,c.UserID)
}
func (t *FundCustomer) Import(r io.Reader) (int,int,error) {
    xlsx, err := excelize.OpenReader(r)
    var (
        failRaws = 0
        successRaws = 0
        userID = 0
        fundID = 0
        number = false
        updateData = make(map[string]interface{})
        customerNum decimal.Decimal
    )
    if err != nil {
        return failRaws,successRaws,err
    }

    rows := xlsx.GetRows("Sheet1")
    usersInfo := make(map[int]template.FundCustomerImport)
    for irow, row := range rows {
        if irow == 0  {
            continue
        }
        var data []string
        for _, cell := range row {
            data = append(data, cell)
        }
        if len(data) == 0 || data[1] == "" || data[0] == "" {
            failRaws++
            continue
        }
        userID,err  = template.GetCustomerIdByName(data[1])
        if err != nil {
            failRaws++
            continue
        }
        fundID,err  = template.GetFundIdByName(data[0])
        if err != nil {
            failRaws++
            continue
        }
        customerNum,err  = decimal.NewFromString(data[2])
        if err != nil {
            failRaws++
            continue
        }
        number,err = template.CheckFundCostmer(fundID,userID)
        if err != nil {
            failRaws ++
            continue
        }
        if number {
            failRaws ++
            continue
        }
        if _,ok := usersInfo[userID];ok {
            usersInfo[userID] = template.FundCustomerImport{
                userID,
                fundID,
                usersInfo[userID].Num.Add(customerNum)}
        }else {
            usersInfo[userID] = template.FundCustomerImport{
                userID,
                fundID,
                customerNum}
        }
    }

    for _, value := range usersInfo {
        successRaws ++
        fundCustomer,err := template.FindFundCostmerByName(value.UserID,value.FundID)
        if err != nil {
            failRaws ++
            continue
        }
        if fundCustomer.ID > 0  {
            updateData["fund_id"]   = value.FundID
            updateData["owner_id"]  = value.UserID
            updateData["num"]       = value.Num.String()
            err =template.EditFundCustomer(fundCustomer.ID, updateData)
        } else {
            fundCustomerService := FundCustomer{
                Created: time.Now().String(),
                FundID: value.FundID,
                UserID: value.UserID,
                Num: value.Num.String(),
                Updated: time.Now().String(),
            }
            err =fundCustomerService.Add()
        }

        if err != nil {
            failRaws ++
            continue
        }
    }

    return failRaws,successRaws,nil
}

func (c *FundCustomer) GetListsByUserID() ([]template.FundCustomerList, string,error) {
    var (
        funds []template.FundCustomerList
    )
    funds,registeDate, err := template.GetCustomerFundList(c.UserID,c.FundID)
    if err != nil {
        return nil,"", err
    }
    return funds,registeDate, nil
}
