package fund_service

import (
    "datacenter/models/template"
)

type Fund struct {
    ID                   int
    Name                 string     // 名称
    LastUpdateTime       string     // 最后更新时间
    UnitValue            string     // 单位净值
    Value                string     // 累计净值
    Created              string     // 创建时间
    Memo                 string     // 备注
    OldName              string     // 之前的名称
    Status               int        // 分红状态 1代表分红；2代表不分红

    PageNum  int
    PageSize int
}


func (f *Fund) GetLists() ([]template.Fund, error) {
    var (
        funds []template.Fund
    )
    funds, err := template.GetFunds()
    if err != nil {
        return nil, err
    }
    return funds, nil
}

func (f *Fund) ExistByName() (bool, error) {
    return template.ExistFundByName(f.Name)
}

func (f *Fund) ExistByID() (bool, error) {
    return template.ExistFundByID(f.getMaps())
}

func (f *Fund) AddName() error {
    return template.AddFundName(f.Name,f.Created)
}

func (f *Fund) Edit() error {
    data := make(map[string]interface{})
    data["name"]        = f.Name
    return template.EditFund(f.getMaps(), data)
}

func (f *Fund) GetListsByName() ([]template.Fund, error) {
    var (
        funds []template.Fund
    )
    funds, err := template.GetFundsByName(f.PageNum, f.PageSize,f.Name)
    if err != nil {
        return nil, err
    }
    return funds, nil
}

func (f *Fund) Del() error {
    return template.DeleteFundAll(f.ID)
    //return template.DeleteFund(f.getMaps())
}

func (f *Fund) getMaps() map[string]interface{} {
    maps := make(map[string]interface{})
    if f.OldName != "" {
        maps["name"] = f.OldName
    }
    if f.ID > 0 {
        maps["id"] = f.ID
    }

    return maps
}

func (f *Fund) GetNames() ([]template.FundNames, error) {
    var (
        funds []template.FundNames
    )
    funds, err := template.GetNames()
    if err != nil {
        return nil, err
    }
    return funds, nil
}