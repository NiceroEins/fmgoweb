package fund_service

import (
    "datacenter/models/template"
    "github.com/360EntSecGroup-Skylar/excelize"
    "github.com/shopspring/decimal"
    "io"
    "strconv"
    "time"
)

type FundBonus struct {
    ID                   int
    FundID               int                 // 基金id
    UserID               int                 // 用户ID
    BonusType            int                 // 分红方式
    UserName             string              // 用户名称
    FundName             string              // 基金名称
    RegisterDate         time.Time           // 登记时间
    Updated              time.Time           // 更新时间
    Created              time.Time           // 创建时间
    InvestNum            decimal.Decimal     // 再投资份额
    UnitBonus            decimal.Decimal     // 每单位分红
    CashBonus            decimal.Decimal     // 现金红利
    InvesteMoney         decimal.Decimal     // 再投资金额
    InvesteUnitValue     decimal.Decimal     // 再投资单位净值

    PageNum  int
    PageSize int
}


func (c *FundBonus) GetLists() ([]template.FundBonusResponse, error) {
    var (
        bonus []template.FundBonusResponse
    )
    bonus, err := template.GetFundBonus(c.PageNum, c.PageSize,c.getMaps(),c.UserName,c.RegisterDate)
    if err != nil {
        return nil, err
    }
    return bonus, nil
}

func (c *FundBonus) Exist() (bool, error) {
    return template.ExistFundBonus(c.getMaps())
}

func (c *FundBonus) Add() error {
    data := template.FundBonus{
        Created: c.Created,
        FundID: c.FundID,
        UserID: c.UserID,
        InvesteMoney: c.InvesteMoney,
        BonusType: c.BonusType,
        InvestNum: c.InvestNum,
        UnitBonus: c.UnitBonus,
        CashBonus: c.CashBonus,
        InvesteUnitValue: c.InvesteUnitValue,
        RegisterDate: c.RegisterDate,
        Updated: time.Now(),
    }
    return template.AddFundBonus(data)
}

func (c *FundBonus) Edit() error {
    data := make(map[string]interface{})
    data["fund_id"]    = c.FundID
    data["user_id"]    = c.UserID
    data["unit_bonus"]  = c.UnitBonus
    data["bonus_type"]  = c.BonusType
    data["register_date"]  = c.RegisterDate
    data["cash_bonus"]  = c.CashBonus
    data["investe_money"]  = c.InvesteMoney
    data["investe_num"]  = c.InvestNum
    data["investe_unit_value"]  = c.InvesteUnitValue
    return template.EditFundBonus(c.ID, data)
}

func (c *FundBonus) Count() (int, error) {
    return template.GetFundBonusTotal(c.getMaps(),c.UserName)
}

func (c *FundBonus) Del() error {
    return template.DeleteFundBonus(c.ID)
}

func (c *FundBonus) getMaps() map[string]interface{} {
    maps := make(map[string]interface{})
    if c.FundID > 0  {
        maps["fund_id"] = c.FundID
    }
    if c.UserID > 0 {
        maps["user_id"] = c.UserID
    }
    if c.BonusType != 0 {
        maps["bonus_type"] = c.BonusType
    }
    return maps
}

func (c *FundBonus) ExistByName() (bool, error) {
    return template.ExistFundBonus(c.getMaps())
}

func ExistByFund(fundId int)  (bool, error) {
    maps := make(map[string]interface{})
    maps["id"] = fundId
    return template.ExistFundByID(maps)
}
func ExistByCustomer(userId int)  (bool, error) {
    maps := make(map[string]interface{})
    maps["id"] = userId
    return template.ExistCustomer(maps)
}
func ExistByID(id int)  (bool, error) {
    maps := make(map[string]interface{})
    maps["id"] = id
    return template.ExistFundBonus(maps)
}
func ExistParams(userID,fundID int ,registerDate time.Time) (int,error) {
    return template.CheckDuplicateData(userID,fundID,registerDate)
}
func (f *FundBonus) GetFundNotice()  (template.FundBonusResponse, error)  {
    var (
        bonus template.FundBonusResponse
    )
    maps := make(map[string]interface{})
    maps["id"] = f.ID
    bonus, err := template.GetFundBonusByID(maps)
    if err != nil {
        return bonus, err
    }
    return bonus, nil
}
func (f *FundBonus) GetFundNoticeByFundId()  (template.FundBonusResponse, error)  {
    var (
        bonus template.FundBonusResponse
    )
    maps := make(map[string]interface{})
    maps["id"] = f.ID
    bonus, err := template.GetFundBonusByID(maps)
    if err != nil {
        return bonus, err
    }
    return bonus, nil
}

func (f *FundBonus) GetFundNoticeContentByFundId() ([]template.FundBonusContent,error){
    var (
        bonus []template.FundBonusContent
    )
    maps := make(map[string]interface{})
    maps["fund_id"] = f.FundID
    maps["user_id"] = f.UserID
    bonus, err := template.GetFundNoticeContent(maps)
    if err != nil {
        return nil, err
    }
    return bonus, nil
}
func (t *FundBonus) Import(r io.Reader) (int,int,error) {
    xlsx, err := excelize.OpenReader(r)
    var (
        failRaws = 0
        successRaws = 0
        userID = 0
        fundID = 0
        rawNumber = 0
        number = 0
        insertData =  template.FundBonus{}
        key = ""
    )
    if err != nil {
        return failRaws,successRaws,err
    }

    rows := xlsx.GetRows("Sheet1")
    usersInfo := make(map[string]template.FundBonus)
    for irow, row := range rows {
        if irow == 0  {
            continue
        }
        var data []string
        for _, cell := range row {
            data = append(data, cell)
        }
        if len(data) == 0 {          // 导入一行空数据
            failRaws ++
            continue
        }
        rawNumber,err = template.GetCustomerCountByName(data[0]) // 判断是否重名(查询条数>2)
        if err != nil {
            failRaws ++
            continue
        }
        if rawNumber > 1 && data[9] != "" {  // 通过用户名和账号来确定
            userID,err  = template.GetCustomerByNameAndOwnerId(data[0],data[9])
        }else {
            userID,err  = template.GetCustomerIdByName(data[0])
        }
        if err != nil {
            failRaws ++
            continue
        }
        fundID,err  = template.GetFundIdByName(data[1])  // 查询基金名称是否匹配
        if err != nil {
            failRaws ++
            continue
        }
        registerDate,dateErr := time.Parse("2006-01-02",convertToFormatDay(data[4]))
        if dateErr != nil {
            failRaws ++
            continue
        }
        number,err = template.CheckDuplicateData(userID,fundID,registerDate)  // 查询基金+用户登录账号+登记日期是否重复
        if err != nil {
            failRaws ++
            continue
        }
        if number > 0  {
            failRaws ++
            continue
        }
        checkNumFormate,err := template.CheckNumParms(data[6],data[7],data[2],data[5],data[8])
        if err != nil {
            failRaws ++
            continue
        }
        insertData.UserID =  userID
        insertData.FundID = fundID
        insertData.InvesteMoney     = checkNumFormate["investeMoney"]
        insertData.InvestNum        = checkNumFormate["investNum"]
        insertData.CashBonus        = checkNumFormate["cashBonus"]
        insertData.UnitBonus        = checkNumFormate["unitBonus"]
        insertData.InvesteUnitValue = checkNumFormate["investeUnitValue"]
        insertData.BonusType        = getBonusType(data[3])
        insertData.RegisterDate     = registerDate
        insertData.Created          = time.Now()
        insertData.Updated          = time.Now()
        key  = strconv.Itoa(userID)+strconv.Itoa(fundID)+data[3]+registerDate.String()
        if _,ok := usersInfo[key];ok {
            if insertData.BonusType == 1 {
                insertData.CashBonus = usersInfo[key].CashBonus.Add(insertData.CashBonus)
            }
            if insertData.BonusType == 2 {
                insertData.InvesteMoney = usersInfo[key].InvesteMoney.Add(insertData.InvesteMoney)
                insertData.InvestNum    = usersInfo[key].InvestNum.Add(insertData.InvestNum)
            }
        }
        usersInfo[key] = insertData
    }
    for _, value := range usersInfo {
        successRaws ++
        err =template.AddFundBonus(value)
        if err != nil {
            failRaws ++
            continue
        }
    }
    return failRaws,successRaws,nil
}

// excel日期字段格式化 yyyy-mm-dd
func convertToFormatDay(excelDaysString string)string{
    // 2006-01-02 距离 1900-01-01的天数
    baseDiffDay := 38719 //在网上工具计算的天数需要加2天，什么原因没弄清楚
    curDiffDay := excelDaysString
    b,_ := strconv.Atoi(curDiffDay)
    // 获取excel的日期距离2006-01-02的天数
    realDiffDay := b - baseDiffDay
    // 距离2006-01-02 秒数
    realDiffSecond := realDiffDay * 24 * 3600
    // 2006-01-02 15:04:05距离1970-01-01 08:00:00的秒数 网上工具可查出
    baseOriginSecond := 1136185445
    resultTime := time.Unix(int64(baseOriginSecond + realDiffSecond), 0).Format("2006-01-02")
    return resultTime
}

func getBonusType(bonusType string) int {
    if bonusType == "红利再投资"{
        return 2
    }else if bonusType == "现金红利" {
        return 1
    }
    return 0
}