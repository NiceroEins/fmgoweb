package changelog_service

import (
	"datacenter/models"
	"datacenter/models/template"
	"github.com/jinzhu/gorm"
	"time"
)

type ChangeLog struct {
	ID                int
	UserID            int
	Content           string

	PageNum  int
	PageSize int
}

func (c *ChangeLog) GetChangelogLists() ([]*template.ChangelogResponse, error) {
	var (
		changelogList []*template.ChangelogResponse
	)
	changelogList,err := template.GetChangelogLists()
	if err != nil {
		return changelogList,err
	}
	return changelogList, nil
}

func (t *ChangeLog) Count() (int, error) {
	return template.GetChangelogTotal()
}

func (c *ChangeLog) Add() error {
	changelog := template.ChangelogAdd{
		c.Content,
		time.Now(),
		c.UserID,
	}
	if err := template.AddChangelog(changelog); err != nil {
		return err
	}

	return nil
}

func (c *ChangeLog) Edit() error {
	return template.EditChangelog(c.ID, map[string]interface{}{
		"content":          c.Content,
		"user_id":          c.UserID,
	})
}
func (c *ChangeLog) ExistChangelogByID() (bool, error) {
	var changelog template.ChangelogResponse
	err := models.DB().Select("id").Where("id = ?", c.ID).First(&changelog).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}

	if changelog.ID > 0 {
		return true, nil
	}

	return false, nil
}

func (c *ChangeLog) Delete() error {
	return template.DeleteChangelog(c.ID)
}