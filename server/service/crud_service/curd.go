package crud_service

type QO interface {
}

type DTO interface {
	DTO2VO() VO
}
type VO interface {
}

type Response struct {
	List    []VO `json:"list"`
	Total   int  `json:"total"`
	PageNum int  `json:"page_num"`
}

func GetResponse(vo []VO, total int, pageNum int) Response {
	return Response{
		List:    vo,
		Total:   total,
		PageNum: pageNum,
	}
}
