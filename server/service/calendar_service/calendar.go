package calendar_service

import (
	"datacenter/models"
	"datacenter/models/industry"
	"datacenter/models/template"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"github.com/jinzhu/gorm"
	"sort"
	"strconv"
	"strings"
	"time"
)

const EVENT_CALENDAR = 1 // 事件日历
const STOCK_CALENDAR = 2 // 个股日历
const IS_DISPLAY = 1     // 是模糊时间
const NOT_DISPLAY = 2    // 不是模糊时间
type Calendar struct {
	ID          int
	IsDisplay   int
	Star        int
	Type        int
	Creator     int
	Content     string
	Detail      string
	Code        string
	Link        string
	Name        string
	PublishDate string
	EventDate   string
	Created     string
	Updated     string
	Start       string
	End         string

	PageNum    int
	PageSize   int
	ManyMonths int
	ManyWeeks  int

	Keyword string
	Sort    int
	AddEvent   int
}

type EventIndustryData struct {

}
func (c *Calendar) GetCalendarLists() ([]*template.CalendarResponse, error) {
	var (
		calendarsList []*template.CalendarResponse
	)
	calendarsList, err := template.GetCalendarsLists(c.PageNum, c.PageSize, c.Type, c.Keyword, c.Sort)
	if err != nil {
		return calendarsList, err
	}
	return calendarsList, nil
}

func (t *Calendar) Count() (int, error) {
	return template.GetCalendarsTotal(t.Type, t.Keyword)
}

func (c *Calendar) Add() error {
	calendar := template.CalendarAdd{
		PublishDate: c.PublishDate,
		EventDate:   c.EventDate,
		IsDisplay:   c.IsDisplay,
		Star:        c.Star,
		Content:     c.Content,
		Detail:      c.Detail,
		Created:     c.Created,
		Creator:     c.Creator,
		Name:        c.Name,
		Link:        c.Link,
		Type:        c.Type,
		Code:        c.Code,
		IsAddEvent:  c.AddEvent,
	}

	var date_list []string
	if c.ManyMonths != 0 {
		date_list = get_date(c.PublishDate, "months", 5)
	}
	if c.ManyWeeks != 0 {
		date_list = get_date(c.PublishDate, "weeks", c.ManyWeeks)
	}
	if len(date_list) > 0 {
		tx := models.DB().Begin()
		for _, pub_date := range date_list {
			calendar.PublishDate = pub_date
			now,_ := time.Parse(util.YMDHMS,pub_date)
			if err := tx.Table("p_calendar").Create(&calendar).Error; err != nil {
				tx.Rollback()
				return err
			}

			if calendar.IsAddEvent > 0 {
				err := industry.SaveEventData("日历事件",calendar.Content,strconv.Itoa(c.Star),now,strings.Split(calendar.Code,","))
				if err != nil {
					logging.Info("save calendar event fail err :",err)
				}
			}
		}
		tx.Commit()
	} else {
		if err := template.AddCalendar(calendar); err != nil {
			return err
		}
		now,_ := time.Parse(util.YMDHMS,calendar.PublishDate)
		if calendar.IsAddEvent > 0 {
			err := industry.SaveEventData("日历事件",calendar.Content,strconv.Itoa(c.Star),now,strings.Split(calendar.Code,","))
			if err != nil {
				logging.Info("save calendar event fail err :",err)
			}
		}
	}
	return nil
}

func (c *Calendar) ExistCalendarByID() (bool, error) {
	var calendar template.CalendarResponse
	err := models.DB().Select("id").Where("id = ?", c.ID).First(&calendar).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}

	if calendar.ID > 0 {
		return true, nil
	}

	return false, nil
}

func (c *Calendar) Edit() error {
	ret := template.EditCalendar(c.ID, map[string]interface{}{
		"publish_date": c.PublishDate,
		"event_date":   c.EventDate,
		"is_display":   c.IsDisplay,
		"star":         c.Star,
		"content":      c.Content,
		"detail":       c.Detail,
		"code":         c.Code,
		"link":         c.Link,
		"name":         c.Name,
		"creator":      c.Creator,
		"type":         c.Type,
		"is_add_event": c.AddEvent,
	})
	var err error
	if ret == nil {
		now,_ := time.Parse(util.YMDHMS,c.PublishDate)
		dataId := existEvents(c)
		if dataId > 0 {
			err = models.DB().Table("p_event_industry_data").
				Where("id = ? ",dataId).Delete(industry.EventData{}).Error
			err = models.DB().Table("p_event_industry_stock").
				Where("data_id = ? ",dataId).Delete(&industry.EventStock{}).Error
			if err != nil {
				logging.Info("save calendar event fail err :",err)
			}
		}
		if c.AddEvent > 0 {
			err = industry.SaveEventData("日历事件",c.Content,strconv.Itoa(c.Star),now,strings.Split(c.Code,","))
			if err != nil {
				logging.Info("save calendar event fail err :",err)
			}
		}
	}
	return ret
}

func (c *Calendar) Delete() error {
	return template.DeleteCalendar(c.ID)
}

func (c *Calendar) GetCalendarMonthLists() ([]*template.CalendarMonthList, error) {
	var (
		calendarsList []*template.CalendarMonthList
	)
	maps := make(map[string]interface{})
	maps["is_display"] = c.IsDisplay // 1 为是模糊展示 2 为 不是模糊展示
	maps["type"] = c.Type            // 1 事件类型 2 为股票类型
	calendarsList, err := template.GetCalendarsMonthLists(maps, c.Start, c.End)
	if err != nil {
		return calendarsList, err
	}
	return calendarsList, nil
}

func AssbleCalendarList(datas []*template.CalendarMonthList) [][]*template.CalendarMonthList {
	tmp := make(map[string][]*template.CalendarMonthList)
	ret := [][]*template.CalendarMonthList{}
	publishDate := ""
	for _, value := range datas {
		publishDate = value.PublishDate.Format("2006-01-02")
		tmp[publishDate] = append(tmp[publishDate], value)
	}
	for _, value := range tmp {
		ret = append(ret, value)
	}
	sort.Slice(ret, func(i, j int) bool {
		return ret[i][0].PublishDate.Sub(ret[j][0].PublishDate) < 0
	})
	return ret
}

func get_date(tim string, tp string, num int) (date_list []string) {
	now, _ := time.Parse("2006-01-02 15:04:05", tim)
	if tp == "months" {
		//月份
		for i := int(now.Month()); i < 13; i++ {
			day := time.Date(now.Year(), time.Month(i), now.Day(), now.Hour(), now.Minute(), now.Second(), 0, time.UTC)

			date_list = append(date_list, day.Format("2006-01-02 15:04:05"))
		}
	}
	if tp == "weeks" {
		//生成周
		for i := 0; i < num; i++ {

			h, _ := time.ParseDuration(strconv.Itoa(i*7*24) + "h")
			day := now.Add(h).Format("2006-01-02 15:04:05")
			date_list = append(date_list, day)
		}
	}
	return
}


func existEvents(c *Calendar) int {
   var err error
   eventData := industry.EventData{}
   now, _ := time.ParseInLocation(util.YMDHMS, c.PublishDate,time.Local)
   err = models.DB().Table("p_event_industry_data").
   	        Where("category_name = ? ",c.Content).
   	        Where("publish_date = ?",now.Format(util.YMD)).First(&eventData).Error
   if err != nil {
		return 0
   }
   return eventData.Id
}