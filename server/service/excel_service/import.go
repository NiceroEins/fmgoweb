package excel_service

import (
	"github.com/360EntSecGroup-Skylar/excelize"
	"io"
)

func UploadHandler(file io.Reader, sheet string) ([][]string, error) {
	xlsx, err := excelize.OpenReader(file)
	if err != nil {
		return nil, err
	}
	if sheet != "" {
		return xlsx.GetRows(sheet), nil
	} else {
		return xlsx.GetRows("Sheet1"), nil
	}
}
