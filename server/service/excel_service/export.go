package excel_service

import (
	"encoding/json"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/gin-gonic/gin"
	"reflect"
	"strconv"
)

func StructToMapViaJson(v interface{}) (map[string]interface{}, error) {
	m := make(map[string]interface{})
	j, _ := json.Marshal(v)
	err := json.Unmarshal(j, &m)
	if err != nil {
		return map[string]interface{}{}, err
	}
	return m, nil
}

func HandlerDownload(c *gin.Context, filename string, values []interface{}, title []string) error {
	xlsx := excelize.NewFile()
	var A = 'A'
	var B = 0
	ts := make(map[string]string)
	for _, t := range title {
		s := string(B) + string(A)
		xlsx.SetCellValue("Sheet1", s+"1", t)
		ts[t] = s
		A++
		if A > 'Z' {
			if B > 0 {
				B++
			} else {
				B = 'A'
			}
			A = 'A'
		}
	}
	i := 1

	for _, value := range values {
		vi := reflect.TypeOf(value)
		m := make(map[string]interface{})
		var ok bool
		var err error
		switch vi.Kind() {
		case reflect.Struct:
			m, err = StructToMapViaJson(value)
			if err != nil {
				continue
			}
			i++
			for k, v := range m {
				xlsx.SetCellValue("Sheet1", k+strconv.Itoa(i), v)
			}
		case reflect.Map:
			m, ok = value.(map[string]interface{})
			if !ok {
				continue
			}
			i++
			for k, v := range m {
				//u, _ := ts[k]
				xlsx.SetCellValue("Sheet1", k+strconv.Itoa(i), v)
			}
		}

	}

	// 保存文件方式
	// _ = xlsx.SaveAs("./aaa.xlsx")

	c.Header("Content-Type", "application/octet-stream")
	c.Header("Content-Disposition", "attachment; filename="+filename+".xlsx")
	c.Header("Content-Transfer-Encoding", "binary")

	// 回写到web 流媒体 形成下载
	err := xlsx.Write(c.Writer)

	if err != nil {
		return err
	}
	return nil

}
