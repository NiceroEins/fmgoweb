package relpay

import (
	"datacenter/models/replay"
	"datacenter/service/crud_service"
	"datacenter/service/excel_service"
	"errors"
	"io"
	"sort"
)

func GetTotalReplayList(qo replay.ReplayQO) (crud_service.VO, int, error) {
	vos := make([]crud_service.VO, 0)
	dtos, _, err := replay.GetTotalReplayList(qo)
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		vo := dto.DTO2VO()
		vos = append(vos, vo)
	}
	sort.Slice(vos, func(i, j int) bool {
		return vos[i].(replay.TotalReplayVO).Count > vos[j].(replay.TotalReplayVO).Count
	})
	return vos, 0, nil
}

func GetMyReplayList(qo replay.ReplayQO) (crud_service.VO, int, error) {
	vos := make([]crud_service.VO, 0)
	dtos, _, err := replay.GetMyReplayList(qo)
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		vo := dto.DTO2VO()
		vos = append(vos, vo)
	}
	return vos, 0, nil
}

func EditMyReplay(eo replay.ReplayEO) error {
	err := replay.EditMyReplay(eo)
	return err
}

func CreateMyReplay(io replay.ReplayIO) error {
	err := replay.CreateMyReplay(io)
	return err
}

func DelMyReplay(id int) error {
	err := replay.DelMyReplay(id)
	return err
}

func UploadExcel(file io.Reader, uid, datetime string) (int, []string, error) {
	rows, err := excel_service.UploadHandler(file, "")
	if err != nil {
		return 0, nil, err
	}
	if len(rows)==0 || len(rows[1]) != 5 {
		return 0, nil, errors.New("文件格式有误")
	}
	failCodes := replay.ImportReplay(rows, uid, datetime)
	return len(rows[1:]), failCodes, nil
}

func GetReplayStatistics(qo replay.ReplayQO) (crud_service.VO, error) {
	dto, err := replay.GetReplayStatistics(qo)
	if err != nil {
		return replay.ReplayVO{}, err
	}
	return dto.DTO2VO(), nil
}
