package market

import (
	"datacenter/models/market"
	"github.com/pkg/errors"
	"strings"
)

const PRODUCTTYPE = 2 //百川app
const BDITYPE = 3     //bdi

type ProductReq struct {
	CategoryID int `json:"category_id" form:"category_id"`
	Type       int `json:"type" form:"type"`
}

type ProductStockRate struct {
	StockName string `json:"stock_name"`
	StockCode string `json:stock_code`
	OneBegin  string
	OneEnd    string
	ThreeEnd  string
	FiveEnd   string
}

func QueryProductValueList(req ProductReq) ([]market.ProductValueList, error) {
	valueList := make([]market.ProductValueList, 0)
	var er error
	if req.Type == 1 {
		//查询时间历史全部数据走势
		var cname string
		cate, err := market.GetCategoryById(req.CategoryID)
		if err != nil {
			return valueList, err
		}
		//查询产品名称
		if strings.Index(cate.Name, "日") != -1 {
			cname = strings.Split(cate.Name, "日")[0]
		} else if strings.Index(cate.Name, "周") != -1 {
			cname = strings.Split(cate.Name, "周")[0]
		} else {
			return valueList, nil
		}
		//查询走势图
		valueList = market.GetProValueList(cname)
		er = nil
	} else if req.Type == 2 {
		valueList = market.GetBdiValueList()
		er = nil
	} else {
		return valueList, errors.Errorf("type error")
	}
	return valueList, er

}

func QueryProductStockRate(req ProductReq) ([]ProductStockRate, error) {
	//产品个股成功率

	RateList := make([]ProductStockRate, 0)
	//查询日期和对应股票
	dateList, err := market.GetCategoryDateList(req.CategoryID)
	if err != nil {
		return RateList, err
	}
	stockList, err := market.GetCategoryStockList(req.CategoryID)
	if err != nil {
		return RateList, err
	}

	for _, stock := range stockList {

		var psr = ProductStockRate{
			StockCode: stock.Code,
			StockName: stock.Name,
		}

		//次日开盘卖出成功率
		psr.OneBegin = market.GetStockRate(dateList, stock.Code, 1, "open", req.Type)
		//次日收盘卖出成功率
		psr.OneEnd = market.GetStockRate(dateList, stock.Code, 1, "close", req.Type)
		//三日收盘卖出成功率
		psr.ThreeEnd = market.GetStockRate(dateList, stock.Code, 3, "close", req.Type)
		//五日收盘卖出成功率
		psr.FiveEnd = market.GetStockRate(dateList, stock.Code, 5, "close", req.Type)

		RateList = append(RateList, psr)

	}

	return RateList, err
}
