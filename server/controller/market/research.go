package market

import (
	"datacenter/models/market"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"reflect"
	"sort"
	"strings"
)

type ResearchReq struct {
	market.ResearchReq
	Sort  string `json:"sort,omitempty" form:"sort,omitempty"`
	Order string `json:"order,omitempty" form:"order,omitempty"`
}

type ResearchDetail struct {
	Code             string   `json:"code"`
	Name             string   `json:"name"`
	TargetPriceUpper *float64 `json:"target_price_upper"`
	TargetPriceLower *float64 `json:"target_price_lower"`
	RisingSpaceUpper *float64 `json:"rising_space_upper"`
	RisingSpaceLower *float64 `json:"rising_space_lower"`
	Rate1            *float64 `json:"rate_1"`
	Rate5            *float64 `json:"rate_5"`
	Rate15           *float64 `json:"rate_15"`
	Rate30           *float64 `json:"rate_30"`
	ReachTarget      bool     `json:"reach_target"`
	PublishDate      string   `json:"publish_date"`
}

type ResearchResp struct {
	Data  []ResearchDetail `json:"data"`
	Total int              `json:"total"`
}

func QueryResearchDetail(req ResearchReq) (ResearchResp, error) {
	var ret ResearchResp
	var rds []ResearchDetail
	data, total, err := market.QueryResearch(req.ResearchReq)
	if err != nil {
		return ret, err
	}
	for _, v := range data {
		tick, err := market.QueryTick(v.Code, v.PublishDate.AddDate(0, 0, 1).Format(util.YMD), 31)
		if err != nil {
			logging.Error("market.QueryResearchDetail() query tick errors: ", v.Code, v.PublishDate.Format(util.YMD), err)
			continue
		}
		rd := ResearchDetail{
			Code: v.Code,
			Name: v.Name,
		}
		var targetPrice *float64
		rd.TargetPriceLower, rd.TargetPriceUpper, targetPrice = splitPrice(v.TargetPrice)
		rd.RisingSpaceLower, rd.RisingSpaceUpper, _ = splitPrice(v.RisingSpace)
		rd.Rate1, _ = risingRate(tick, 1, nil)
		rd.Rate5, _ = risingRate(tick, 5, nil)
		rd.Rate15, _ = risingRate(tick, 15, nil)
		rd.Rate30, rd.ReachTarget = risingRate(tick, 30, targetPrice)
		rd.PublishDate = v.PublishDate.Format(util.YMD)

		rds = append(rds, rd)
	}

	ret.Total = total
	sortResearchDetail(ret.Data, req.Sort, req.Order)

	//pagination
	if req.PageSize > 0 && req.PageNum > 0 {
		ret.Data = rds[util.Min(len(data), req.PageSize*(req.PageNum-1)):util.Min(len(data), req.PageSize*req.PageNum)]
	} else {
		ret.Data = rds
	}
	return ret, nil
}

// lower, upper, middle
func splitPrice(str string) (*float64, *float64, *float64) {
	if sl := strings.Split(str, ","); len(sl) > 1 {
		tpl := util.Atof(sl[0])
		tpu := util.Atof(sl[1])
		tpm := (tpl + tpu) / 2
		return &tpl, &tpu, &tpm
	} else {
		tpu := util.Atof(sl[0])
		tpm := tpu
		return nil, &tpu, &tpm
	}
}

func risingRate(tick []market.Tick, day int, targetPrice *float64) (*float64, bool) {
	if len(tick) < day+1 || tick[0].Open <= 0 {
		return nil, false
	}
	var bReach bool
	if targetPrice != nil {
		for i := 1; i < day+1; i++ {
			if tick[i].High >= *targetPrice {
				bReach = true
				break
			}
		}
	}
	rp := tick[day].Open/tick[0].Open - 1
	return &rp, bReach
}

func sortResearchDetail(data []ResearchDetail, sortKey, order string) {
	if sortKey != "" {
		sort.Slice(data, func(i, j int) bool {
			v := reflect.ValueOf(data[i])
			u := reflect.ValueOf(data[j])
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(data[i]).Field(k).Tag.Get("json") == sortKey {
					if order == "asc" {
						return util.Compare(v.Field(k), u.Field(k))
					} else {
						return util.Compare(u.Field(k), v.Field(k))
					}
				}
			}
			return false
		})
	}
}
