package market

import (
	"datacenter/models/market"
	"datacenter/pkg/gredis"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"fmt"
	"reflect"
	"sort"
	"strings"
	"sync"
	"time"
)

type Class struct {
	ID       int     `json:"value"`
	Name     string  `json:"label"`
	Children []Class `json:"children,omitempty"`
}

type ClassResp struct {
	Data []Class `json:"options"`
}

type CategoryReq struct {
	CategoryID int `json:"category_id" form:"category_id"`
}

type Category struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Frequency string `json:"frequency"`
}

type CategoryResp struct {
	Data []Category `json:"data"`
}

type DataReq struct {
	Name     string    `json:"name,omitempty" form:"name,omitempty"`
	ClassID  int       `json:"class_id,omitempty" form:"class_id,omitempty"`
	Sort     string    `json:"sort,omitempty" form:"sort,omitempty"`
	Order    string    `json:"order,omitempty" form:"order,omitempty"`
	Begin    time.Time `json:"begin" form:"begin,omitempty"`
	End      time.Time `json:"end" form:"end,omitempty"`
	PageSize int       `json:"n" form:"n"`
	PageNum  int       `json:"p" form:"p"`
}

type Stock struct {
	Code string `json:"code" form:"code"`
	Name string `json:"name" form:"name,omitempty"`
}

type MarketData struct {
	ID         int      `json:"id"`
	Name       string   `json:"name"`
	RawValue   float64  `json:"raw_value"`
	Value      string   `json:"value"`
	Unit       string   `json:"unit"`
	YoyIncr    *float64 `json:"yoy_incr"`
	PopIncr    *float64 `json:"pop_incr"`
	Rate       *Rate    `json:"rate"`
	Frequency  string   `json:"frequency"`
	Stock      []Stock  `json:"stock"`
	UpdateTime string   `json:"update_time"`
	CategoryID int      `json:"category_id"`
	RawID      int      `json:"-"`
	RawName    string   `json:"raw_name"`
	TimeStr    string   `json:"time_str"`
	Type       int      `json:"type"`
	RawUnit    string   `json:"raw_unit"`
	ClassName  string   `json:"class_name"`
	//Raw        *market.Data `json:"-"`
}

type DataResp struct {
	Data    []MarketData `json:"data"`
	Total   int          `json:"total"`
	PageNum int          `json:"p"`
}

type DetailReq struct {
	CategoryID int       `json:"category_id" form:"category_id"`
	Begin      time.Time `json:"begin" form:"begin,omitempty"`
	End        time.Time `json:"end" form:"end,omitempty"`
	Sort       string    `json:"sort,omitempty" form:"sort,omitempty"`
	Order      string    `json:"order,omitempty" form:"order,omitempty"`
}

type Change struct {
	Day       int     `json:"day"`
	Value     float64 `json:"value"`
	ValueOpen float64 `json:"value_open"`
	NcoRate   float64 `json:"nco_rate"`
}

type Detail struct {
	MarketData
	Change     []Change `json:"change"`
	Change1    *float64 `json:"change1"`
	Change3    *float64 `json:"change3"`
	Change5    *float64 `json:"change5"`
	ChangePre1 *float64 `json:"change_pre1"`
	ChangePre3 *float64 `json:"change_pre3"`
	ChangePre5 *float64 `json:"change_pre5"`
	ChangeOpen *float64 `json:"change_open"`
}

type DetailResp struct {
	Data  []Detail `json:"data"`
	Total int      `json:"total"`
}

type EditStockReq struct {
	CategoryID int     `json:"category_id" form:"category_id"`
	Data       []Stock `json:"data" form:"data"`
}

type EditDataReq struct {
	ID    int     `json:"id" form:"id"`
	Value float64 `json:"value" form:"value"`
}

type Rate struct {
	Current int     `json:"current"`
	Total   int     `json:"total"`
	Rate    float64 `json:"rate"`
}

var cache sync.Map

func ListClass() (ClassResp, error) {
	var ret ClassResp
	raw, err := market.ListClass()
	if err != nil {
		return ret, err
	}
	for _, v := range raw {
		cg := Class{
			ID:   v.ID,
			Name: v.Name,
		}
		if v.Level == 1 {
			ret.Data = append(ret.Data, cg)
		} else {
			for i := 0; i < len(ret.Data); i++ {
				if ret.Data[i].ID == v.ParentID {
					ret.Data[i].Children = append(ret.Data[i].Children, cg)
					break
				}
			}
		}
	}
	return ret, nil
}

func QueryCategory(req CategoryReq) (CategoryResp, error) {
	var id int
	ret := CategoryResp{
		Data: make([]Category, 0),
	}

	class, err := market.QueryClass(req.CategoryID)
	if err != nil {
		return ret, err
	}
	if class.SecondClassID > 0 {
		id = class.SecondClassID
	} else {
		id = class.FirstClassID
	}
	raw, err := market.QueryCategory(id)
	if err != nil {
		return ret, err
	}
	for _, v := range raw {
		freq, _ := market.QueryFreq(v.ID)
		ret.Data = append(ret.Data, Category{
			ID:        v.ID,
			Name:      v.Name,
			Frequency: freq,
		})
	}
	return ret, nil
}

func QueryData(req DataReq) (DataResp, error) {
	var data []MarketData
	ret := DataResp{
		Data: make([]MarketData, 0),
	}

	raw, err := market.QueryData(req.Name, req.ClassID, 0)
	if err != nil {
		return ret, err
	}

	for k, v := range raw {
		if v.RowNum != 1 || !req.Begin.IsZero() && v.CreatedAt.Before(req.Begin) || !req.End.IsZero() && v.CreatedAt.After(req.End) {
			continue
		}
		vl, un := generateUnit(v.Value, v.Unit)
		ts := timeUnit(&v)
		md := MarketData{
			ID:         v.ID,
			Name:       v.CategoryName,
			ClassName:  v.ClassName,
			TimeStr:    ts,
			RawName:    v.CategoryName,
			RawValue:   v.Value,
			Value:      fmt.Sprintf("%.2f", vl),
			Unit:       un,
			RawUnit:    v.Unit,
			YoyIncr:    calcYearOnYear(k, raw),
			PopIncr:    calcPeriodOnPeriod(k, raw),
			Rate:       nil,
			Frequency:  v.Frequency,
			Stock:      parseStock(&v),
			CategoryID: v.CategoryID,
			UpdateTime: v.CreatedAt.Format(util.YMD),
			Type:       v.Type,
		}
		data = append(data, md)
	}

	//sort
	sortData(data, req.Sort, req.Order)

	//pagination
	if req.PageSize > 0 && req.PageNum > 0 {
		ret.Data = data[util.Min(len(data), req.PageSize*(req.PageNum-1)):util.Min(len(data), req.PageSize*req.PageNum)]
	} else {
		ret.Data = data
	}

	//calc rate
	for i := 0; i < len(ret.Data); i++ {
		var arr []market.Data
		for j := 0; j < len(raw); j++ {
			if raw[j].CategoryID == ret.Data[i].CategoryID {
				arr = append(arr, raw[j])
			}
		}
		if len(arr) > 0 {
			//cache
			var rate *Rate
			v, ok := cache.Load(arr[0].CategoryID)
			if ok {
				rate = v.(*Rate)
			} else {
				switch arr[0].Type {
				case 0, 3:
					rate = rateByMethod(arr, "yoy")
				case 2:
					mr := market.GetProductRateById(arr[0].CategoryID, 1, "close", market.TICKTP)
					if mr == nil {
						rate = nil
					} else {
						rate = &Rate{
							Current: mr.Current,
							Total:   mr.Total,
							Rate:    mr.Rate,
						}
					}
				case 1:
					mr := market.GetProductRateById(arr[0].CategoryID, 1, "close", market.MINUTETP)
					if mr == nil {
						rate = nil
					} else {
						rate = &Rate{
							Current: mr.Current,
							Total:   mr.Total,
							Rate:    mr.Rate,
						}
					}
				case 4:
					rate = rateByMethod(arr, "pop")
				default:
					rate = calcRate(arr)
				}
				cache.Store(arr[0].CategoryID, rate)
			}
			ret.Data[i].Rate = rate
		}
	}

	ret.Total = len(data)
	ret.PageNum = req.PageNum
	return ret, nil
}

func QueryDetail(req DetailReq) (DetailResp, error) {
	var data []MarketData
	ret := DetailResp{
		Data: make([]Detail, 0),
	}

	raw, err := market.QueryData("", 0, req.CategoryID)
	if err != nil {
		return ret, err
	}
	for k, v := range raw {
		var year, month, day int = 0, 1, 1
		if v.Year > 0 {
			year = v.Year
		} else {
			year = time.Now().Year()
		}
		if v.Month > 0 {
			month = v.Month
		}
		if v.Week > 0 {
			day += v.Week * 7
		}
		if v.Day > 0 {
			day += v.Day
		}
		t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
		if !req.Begin.IsZero() && t.Before(req.Begin) || !req.End.IsZero() && t.After(req.End) {
			continue
		}

		vl, un := generateUnit(v.Value, v.Unit)
		md := MarketData{
			ID:         v.ID,
			Name:       t.Format(util.YMD),
			RawName:    v.CategoryName,
			ClassName:  v.ClassName,
			RawValue:   v.Value,
			Value:      fmt.Sprintf("%.2f", vl),
			Unit:       un,
			RawUnit:    v.Unit,
			YoyIncr:    calcYearOnYear(k, raw),
			PopIncr:    calcPeriodOnPeriod(k, raw),
			RawID:      k,
			Frequency:  v.Frequency,
			Stock:      parseStock(&v),
			CategoryID: v.CategoryID,
			UpdateTime: v.CreatedAt.Format(util.YMD),
			Type:       v.Type,
			Rate:       nil,
		}
		data = append(data, md)
	}
	//sort
	sortData(data, req.Sort, req.Order)
	ret.Total = len(data)

	//assemble
	for _, v := range data {
		change, err := calcChange(&raw[v.RawID])
		if err != nil {
			return ret, err
		}
		dt := Detail{
			MarketData: v,
			Change:     change,
		}
		var io int = -1
		for i := 0; i < len(dt.Change); i++ {
			switch dt.Change[i].Day {
			case 1:
				dt.Change1 = &dt.Change[i].Value
				io = i
			case 3:
				dt.Change3 = &dt.Change[i].Value
			case 5:
				dt.Change5 = &dt.Change[i].Value
			case -1:
				dt.ChangePre1 = &dt.Change[i].Value
			case -3:
				dt.ChangePre3 = &dt.Change[i].Value
			case -5:
				dt.ChangePre5 = &dt.Change[i].Value
			}
		}
		if len(dt.Change) > 0 && io >= 0 {
			dt.ChangeOpen = &dt.Change[io].ValueOpen
		} else {
			dt.ChangeOpen = nil
		}
		ret.Data = append(ret.Data, dt)
	}

	return ret, nil
}

func EditStock(er EditStockReq) error {
	stock, err := market.QueryStock(er.CategoryID)
	if err != nil {
		return err
	}
	defer cache.Delete(er.CategoryID)

	//delete
	for _, v := range stock {
		bExist := false
		for _, u := range er.Data {
			if v.Code == u.Code {
				bExist = true
				break
			}
		}
		if !bExist {
			if err := market.DeleteStock(v); err != nil {
				return err
			}
		}
	}

	category, err := market.QueryClass(er.CategoryID)
	if err != nil {
		return err
	}
	//update
	for _, u := range er.Data {
		bExist := false
		for _, v := range stock {
			if v.Code == u.Code {
				bExist = true
				break
			}
		}
		if !bExist {
			if err := market.InsertStock(market.Stock{
				CategoryID:   er.CategoryID,
				CategoryName: category.Name,
				Code:         u.Code,
			}); err != nil {
				return err
			}
		}
	}
	return nil
}

func EditData(ed EditDataReq) error {
	md := market.GetInstance(ed.ID)
	if md != nil {
		if err := md.Set(ed.Value); err != nil {
			return err
		} else {
			cache.Delete(md.CategoryID)
		}
	}
	return nil
}

func calcYearOnYear(id int, data []market.Data) *float64 {
	for i := 0; i < len(data); i++ {
		if data[i].CategoryID == data[id].CategoryID &&
			data[i].Year == data[id].Year-1 &&
			data[i].Month == data[id].Month &&
			data[i].Day == data[id].Day &&
			data[i].Week == data[id].Week {
			var rate float64
			if data[i].Value != 0 {
				rate = data[id].Value/data[i].Value - 1
			} else {
				return nil
			}
			return &rate
		}
	}
	return nil
}

func calcPeriodOnPeriod(id int, data []market.Data) *float64 {
	if id+1 >= len(data) || data[id].CategoryID != data[id+1].CategoryID {
		return nil
	}
	var rate float64
	//var bExist bool
	//t1 := parseTime(&data[id+1])
	//t2 := parseTime(&data[id])
	//switch data[id].Frequency {
	//case "年":
	//	bExist = t2.Before(t1.Add(367*24*time.Hour)) && t2.After(t1.Add(360*24*time.Hour))
	//case "月":
	//	bExist = t2.Before(t1.Add(32*24*time.Hour)) && t2.After(t1.Add(27*24*time.Hour))
	//case "日":
	//	bExist = t2.Equal(t1.Add(24 * time.Hour))
	//}

	if data[id+1].Value != 0 {
		rate = data[id].Value/data[id+1].Value - 1
	} else {
		return nil
	}
	return &rate
}

func timeUnit(data *market.Data) string {
	var ret string
	if data.Day > 0 {
		ret = fmt.Sprintf("%d日", data.Day) + ret
	}
	if data.Week > 0 {
		ret = fmt.Sprintf("第%d周", data.Week) + ret
	}
	if data.Month > 0 {
		ret = fmt.Sprintf("%d月", data.Month) + ret
	}
	if data.Year > 0 {
		ret = fmt.Sprintf("%d年", data.Year) + ret
	}
	//if ret != "" {
	//	ret = fmt.Sprintf("(%s)", ret)
	//}
	return ret
}

func sortData(data []MarketData, sortKey, order string) {
	if sortKey != "" {
		sort.Slice(data, func(i, j int) bool {
			v := reflect.ValueOf(data[i])
			u := reflect.ValueOf(data[j])
			for k := 0; k < v.NumField(); k++ {
				if reflect.TypeOf(data[i]).Field(k).Tag.Get("json") == sortKey {
					if order == "asc" {
						return util.Compare(v.Field(k), u.Field(k))
					} else {
						return util.Compare(u.Field(k), v.Field(k))
					}
				}
			}
			return false
		})
	}
}

func calcChange(raw *market.Data) ([]Change, error) {
	var ret []Change
	codes := strings.Split(raw.Code, ",")
	tempCache := make(map[string]map[int]market.Tick)
	for _, day := range []int{1, 3, 5, -1, -3, -5} {
		var cc, co, ccn, con, nco float64
		for _, v := range codes {
			// tick[0~4] = day[1~5]
			// tick[-1~-5] = day[-1~-5]
			tick := make(map[int]market.Tick)
			if c, ok := tempCache[v]; ok {
				tick = c
			} else {
				t, err := market.QueryTick(v, raw.PublishDate.Format(util.YMD), 6)
				if err != nil {
					return ret, err
				}
				mt, err := market.QueryTick(v, raw.PublishDate.Format(util.YMD), -5)
				if err != nil {
					return ret, err
				}

				for tk, tv := range t {
					tick[tk] = tv
				}
				for mtk, mtv := range mt {
					tick[-mtk-1] = mtv
				}
				tempCache[v] = tick
			}
			if day > 0 {
				if td, btd := tick[day]; btd && td.Open > 0 && td.Close > 0 {
					if t0, ok := tick[0]; ok && t0.PreClose > 0 {
						cc += td.Close/t0.PreClose - 1
						ccn += 1
					}
					if t1, ok := tick[day-1]; ok && t1.Open > 0 {
						sco := td.Open/t1.Open - 1
						if sco > 0 {
							nco += 1
						}
						co += sco
						con += 1
					}
				} else {
					continue
				}
			} else {
				if t2, ok := tick[day]; ok && t2.Open > 0 {
					cc += tick[day].PctChg / 100
					sco := tick[day+1].Open/tick[day].Open - 1
					if sco > 0 {
						nco += 1
					}
					co += sco
					ccn += 1
					con += 1
				}
			}
		}
		if ccn > 0 && con > 0 {
			cc = cc / ccn
			co = co / con
		} else {
			continue
		}

		ret = append(ret, Change{
			Day:       day,
			Value:     cc,
			ValueOpen: co,
			NcoRate:   nco / con,
		})

	}
	return ret, nil
}

func parseTime(md *market.Data) time.Time {
	var year, month, day int
	if md.Year == 0 {
		year = time.Now().Year()
	} else {
		year = md.Year
	}
	month = md.Month
	day = md.Week*7 + md.Day
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
}

func parseStock(md *market.Data) []Stock {
	ret := make([]Stock, 0)
	codes := strings.Split(md.Code, ",")
	//names := strings.Split(md.Stock, ",")
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for i := 0; i < len(codes); i++ {
		name, _ := conn.HGetString("stock:name", codes[i])
		ret = append(ret, Stock{
			Code: codes[i],
			Name: name,
		})
	}
	return ret
}

func calcRate(data []market.Data) *Rate {
	var ret Rate
	for _, v := range data {
		change, err := calcChange(&v)
		if err != nil {
			return nil
		}
		if len(change) <= 0 {
			continue
		}
		for _, u := range change {
			if u.Day == 1 {
				if u.NcoRate >= 0.5 {
					ret.Current++
				}
				ret.Total++
				break
			}
		}
	}
	if ret.Total > 0 {
		ret.Rate = float64(ret.Current) / float64(ret.Total)
		return &ret
	}
	return nil
}

func combineRate(data []Rate) Rate {
	var ret Rate
	for _, v := range data {
		ret.Current += v.Current
		ret.Total += v.Total
	}
	ret.Rate = float64(ret.Current) / float64(ret.Total)
	return ret
}

func generateUnit(value float64, unit string) (float64, string) {
	words := []string{"", "万", "亿", "万亿"}
	var ret string
	iw := 0
	v := value
	switch unit {
	case "w", "W", "瓦":
		if v > 1000000 {
			v /= 1000000000
			ret = "GW"
		} else {
			ret = unit
		}
	case "kw", "KW", "千瓦":
		if v > 1000 {
			v /= 1000000
			ret = "GW"
		} else {
			ret = unit
		}
	default:
		for v >= 10000 {
			v /= 10000
			iw++
		}
		ret = words[iw] + unit
	}
	return v, ret
}

func rateByMethod(data []market.Data, method string) *Rate {
	var ret Rate
	// k, u := range data[0:util.Min(24, len(data))]
	max := util.Min(24, len(data))
	for k := 0; k < max; k++ {
		u := data[k]
		switch method {
		case "yoy":
			yoy := calcYearOnYear(k, data)
			if yoy == nil || *yoy <= 0 {
				continue
			}
		case "pop":
			pop := calcPeriodOnPeriod(k, data)
			if pop == nil || *pop <= 0 {
				continue
			}
		default:
			continue
		}

		var vCount, vTotal float64
		codes := strings.Split(u.Code, ",")
		for _, v := range codes {
			tick, err := market.QueryTick(v, u.PublishDate.Format(util.YMD), 2)
			if err == nil && len(tick) == 2 {
				if tick[0].Open > 0 && tick[1].Open > 0 {
					vTotal += tick[1].Open/tick[0].Open - 1
					vCount += 1
				}
			}
		}
		if vCount > 0 {
			if vTotal > 0 {
				ret.Current++
			}
			ret.Total++
		} else {
			max = util.Min(max+1, len(data))
		}
	}
	if ret.Total == 0 {
		return nil
	} else {
		ret.Rate = float64(ret.Current) / float64(ret.Total)
		return &ret
	}
}
