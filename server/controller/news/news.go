package news

import (
	"datacenter/models/event"
	"datacenter/models/spider"
	"datacenter/pkg/util"
	"fmt"
)

// 封装回包数据
func Assemble(data []spider.NewsEx, policy *spider.NewsPolicy, uid string, additional ...string) []spider.NewsEx {
	ret := make([]spider.NewsEx, 0)
	for _, v := range data {
		nrd := v
		nrd.Source = nrd.CatchPath
		nrd.TitleRaw = v.Title
		nrd.ContentRaw = v.Content
		nrd.Year = nrd.CreatedAt.Format("2006")
		nrd.Date = nrd.CreatedAt.Format("01-02")
		nrd.Time = nrd.CreatedAt.Format("15:04:05")

		// 0609 fix: bug #2, #4, #8
		switch v.Type {
		case "qa", "q":
			nrd.Title = addTag(v.Title, policy.MarkWord["qa"])
			if nrd.Content == "" {
				nrd.Content = "暂无回答"
			} else {
				nrd.Content = addTag(v.Content, policy.MarkWord["qa"])
			}
			switch nrd.Source {
			case "深互动":
				nrd.CatchLink = fmt.Sprintf("http://irm.cninfo.com.cn/ircs/company/companyDetail?stockcode=%s", nrd.Code)
			default:
				if nrd.Link != "" {
					nrd.CatchLink = nrd.Link
				} else {
					nrd.CatchLink = "http://sns.sseinfo.com/index.do"
				}
				nrd.Link = addUrlLabel(nrd.Link, nrd.ID)
			}
			nrd.CatchLink = addUrlLabel(nrd.CatchLink, nrd.ID)
		case "wechat-detail":
			nrd.Type = "wechat"
			nrd.Title = addTag(v.Title, policy.MarkWord["news"])
			content := addTag(v.Content, policy.CustomKeyword)
			if idx := compareStrings(content, v.Content); idx >= 0 {
				r := []rune(v.Content)
				begin := 0
				end := len(r) - 1
				if idx > 100 {
					begin = idx - 100
				}
				if idx+100 < len(r) {
					end = idx + 100
				}
				nrd.Content = addTag(string(r[begin:end]), policy.MarkWord["qa"])
				nrd.ContentRaw = string(r[begin:end])
			} else {
				nrd.Content = ""
				nrd.ContentRaw = ""
			}
			if util.ContainsAny(policy.NewsType, "wechat-source") {
				nrd.Content = ""
				nrd.ContentRaw = ""
			}
		default:
			nrd.Type = "news"
			nrd.Title = addTag(v.Title, policy.MarkWord["news"])
		}

		// if news has an empty link,
		// fill it with its source_link
		if nrd.Link == "" {
			nrd.Link = nrd.CatchLink
		}

		if len(additional) > 0 && additional[0] == "no-event" {
			// no event
		} else {
			// add event tag
			events := event.FetchEvent(uid, v.ID)
			for _, v := range events {
				switch v {
				case "copy":
					nrd.IsCopy = true
				case "recommend":
					nrd.IsRecommend = true
				case "highly_recommend":
					nrd.IsHighlyRecommend = true
				case "link":
					nrd.IsLink = true
				}
			}
		}

		ret = append(ret, nrd)
	}
	return ret
}
