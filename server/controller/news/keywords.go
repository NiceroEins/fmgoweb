package news

import (
	"bytes"
	"datacenter/pkg/util"
	"fmt"
	"regexp"
	"sort"
	"strings"
)

// 0616 fix bug: #25
func addTag(content string, keywords []string) string {
	// <span style="color:red;font-weight:bold">Content</span>
	var ctx = "<span style=\"color:%s;font-weight:bold\">"
	var edx = "</span>"
	var index IndexSlice
	kw := util.Clear(keywords)
	//sort.Sort(sort.Reverse(stringSlice(kw)))
	// 0609 fix: bug #7
	for _, v := range kw {
		re := regexp.MustCompile("(?i)" + v)
		//raw := re.FindString(content)
		//if raw == "" {
		//	raw = v
		//}
		for _, v := range re.FindAllStringIndex(content, -1) {
			if v != nil && v[0] >= 0 {
				index = append(index, Index{
					idx:    v[0],
					tag:    "red",
					bFront: true,
					bAvail: true,
				}, Index{
					idx:    v[1],
					tag:    "red",
					bFront: false,
					bAvail: true,
				})
			}
		}
		//str := fmt.Sprintf("<span style=\"color:red;font-weight:bold\">%s</span>", raw)
		////ret = strings.Replace(ret, v, str, -1)
		//ret = re.ReplaceAllString(ret, str)
	}
	sort.Sort(index)
	for k, v := range index {
		if !v.bAvail || !v.bFront {
			continue
		}
		// 不同种类的tags：保留
		// 同种类不交叉的tags： 删除内层
		// 同种类交叉tags： 保留最外层
		t := 1
		for i := k + 1; i < len(index); i++ {
			if index[i].tag != v.tag || !index[i].bAvail {
				continue
			}
			if index[i].bFront {
				t++
				index[i].bAvail = false
			} else {
				t--
				if t > 0 {
					index[i].bAvail = false
				} else {
					break
				}
			}
		}
	}
	var buffer bytes.Buffer
	j := 0
	for i := 0; i < len(index) && j < len([]byte(content)); i++ {
		if !index[i].bAvail {
			continue
		}
		k := index[i].idx
		if j < k && k <= len([]byte(content)) {
			buffer.WriteString(content[j:k])
		}
		if index[i].bFront {
			buffer.WriteString(fmt.Sprintf(ctx, index[i].tag))
		} else {
			buffer.WriteString(edx)
		}
		j = k
	}
	if j < len([]byte(content)) {
		buffer.WriteString(content[j:])
	}
	return buffer.String()
}

func addUrlLabel(src string, label interface{}) string {
	ret := strings.TrimRight(src, "/")
	ret = strings.TrimRight(ret, "\\")
	ret += fmt.Sprintf("&z=%v", label)
	return ret
}

func compareStrings(src, dest string) int {
	rSrc := []rune(src)
	rDest := []rune(dest)
	for i := 0; i < len(rSrc) && i < len(rDest); i++ {
		if rSrc[i] != rDest[i] {
			return i
		}
	}
	return -1
}

type stringSlice []string

func (s stringSlice) Len() int {
	return len(s)
}

func (s stringSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s stringSlice) Less(i, j int) bool {
	d := len(s[i]) - len(s[j])
	switch {
	case d > 0:
		return false
	case d < 0:
		return true
	default:
		return s[i] < s[j]
	}
}

type Index struct {
	idx    int
	tag    string // color
	bFront bool
	bAvail bool // available
}

type IndexSlice []Index

func (s IndexSlice) Len() int {
	return len(s)
}

func (s IndexSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s IndexSlice) Less(i, j int) bool {
	return s[i].idx <= s[j].idx
}
