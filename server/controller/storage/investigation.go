package storage

import (
	"datacenter/models/spider"
	"datacenter/service/crud_service"
)

func GetInvestigationSearchList(qo *spider.InvestigationSearchQO) ([]crud_service.VO, int, error) {
	vos := make([]crud_service.VO, 0)
	dtos, cnt, err := spider.GetInvestigationSearchList(qo)
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		vo := dto.DTO2VO()
		vos = append(vos, vo)
	}
	return vos, cnt, nil
}


func GetInvestigationFilteredList(qo *spider.InvestigationFilteredQO) ([]crud_service.VO, int, error) {
	vos := make([]crud_service.VO, 0)
	dtos, cnt, err := spider.GetInvestigationFilteredList(qo)
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		vo := dto.DTO2VO()
		vos = append(vos, vo)
	}
	return vos, cnt, nil
}