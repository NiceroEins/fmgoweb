package storage

import (
	"datacenter/controller/async"
	"datacenter/controller/statistics"
	"datacenter/controller/task"
	msg "datacenter/middleware/message"
	"datacenter/models"
	"datacenter/models/ipo"
	"datacenter/models/message"
	"datacenter/models/spider"
	"datacenter/models/template"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/pkg/websocket"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func Store(report *spider.Report) (bool, int, error) {
	bStored := false
	var retErr error
	var id int
	for _, v := range report.Content {
		ok := false
		var err error
		switch v.Type {
		case "q", "qa":
			ok, err = StoreQA(&v)
		case "normal", "strong", "special", "outer":
			ok, id, err = StoreNews(&v)
		case "forecast":
			// 预告不存快照
			_, err = StoreForecast(&v)
		case "announcement":
			// 公告不存快照
			_, err = StoreAnnouncement(&v)
		case "disclosure":
			// 披露时间不存快照
			_, err = StoreDisclosure(&v)
		case "iwc":
			// 问财不存快照
			_, err = StoreIWC(&v)
		case "qa-hdy":
			// 互动易不存快照
			_, err = StoreHDY(&v)
		case "wb":
			ok, err = ProcWb(&v)
		case "wb-detail":
			ok, id, err = StoreWbDetail(&v)
		case "yb-dc-list", "yb-lb-list", "yb-hb-list", "yb-nxny-list":
			_, err = ProcResearchList(&v)
		case "yb-dc-detail", "yb-lb-detail", "yb-hb-detail", "yb-nxny-detail":
			_, err = ProcResearchDetail(&v)
		case "research-pdf":
			_, err = StoreResearchPDF(&v)
		case "ipo-dc-list":
			_, err = ProcIpoDcList(&v)
		case "ipo-tyc-list":
			_, err = ProcIpoTycList(&v)
		case "ipo-tyc-detail":
			_, err = ProcIpoTycDetail(&v)
		case "yb-email":
			_, err = ProcResearchDetail(&v)
		case "popular-stock":
			_, err = ProcPopularStock(&v)
		case "wechat-list":
			_, err = ProcWeChatList(&v)
		case "wechat-detail":
			_, id, err = StoreWeChatDetail(&v)
		case "ipo-tyc-list-search":
			_, err = ipo.ProcIpoTycList(&v)
		case "ipo-tyc-detail-search":
			_, err = ipo.IpoTycDetailSearch(&v)
		case "reorganization":
			_, err = StoreReorganization(&v)
		case "yb-dc-industry-list", "yb-nxny-industry-list":
			_, err = ProcResearchList(&v)
		case "yb-dc-industry-detail", "yb-hb-industry":
			_, err = StoreIndustryResearch(&v)
		case "cninfo-notice-list":
			_, err = StoreNoticeList(&v)
		case "twitter":
			_, id, err = StoreTwitter(&v)
		case "auction":
			_, err = StoreAuctionList(&v)
		case "futures-info":
			_, err = StoreFuturesInfo(&v)
		case "futures-setprice":
			_, err = StoreFuturesSetpriceInfo(&v)
		case "future-up":
			_, err = StoreFuturesUpInfo(&v)
		case "sh-investigation", "cninfo-investigation-list":
			_, err = StoreInvestigationList(&v)
		case "investigation-detail":
			_, err = StoreInvestigationDetail(&v)
		case "forecast-ths": // 同花顺公告补充
			// 预告不存快照
			_, err = StoreForecastTHS(&v)
		default:
			logging.Warn("storage.Store() Unknown type: ", v.Type)
		}
		if err == nil && ok {
			bStored = true
		} else {
			retErr = err
		}
	}
	if bStored {
		// add snapshot
		if err := Snapshot(report); err != nil {
			logging.Error("storage.Store() snapshot Errors: ", err.Error())
		}
	}
	if retErr != nil {
		logging.Error("storage.Store() store Errors: ", retErr.Error())
	}
	return bStored, id, retErr
}

func StoreQA(value *spider.NewsContent) (bool, error) {
	// 0611 add: pre-process for keyword
	keyHit := 0
	// "q:322"
	ok, err := addToSet(value.Type+":"+value.SeedID, value.IndexId)
	if err != nil {
		logging.Error("storage.StoreQA() add to set Errors: " + err.Error())
		return false, err
	}

	if ok {
		if spider.Contains(value.Title, spider.Fetch("qa")) || spider.Contains(value.Content, spider.Fetch("qa")) {
			keyHit = 1
		}
		//库中不存在,存成功,并且入mysql
		//sql := "insert into u_feed (title,link,title_time,seed_id,cata,catch_time,keyword_hit,type,index_id,content,content_time,object,author) values(" + "'" + v.Title + "'," + "'" + v.Link + "'," + "'" + v.TitleTime + "'," + "'" + v.SeedID + "'," + "'" + v.Cata + "'," + "'" + v.CatchTime + "'," + "'" + keyHit + "'," + "'" + v.Type + "','" + v.IndexId + "','" + v.Content + "','" + v.ContentTime + "','" + v.Object + "','" + v.Author + "')"
		news := spider.Content2News(value, 0, keyHit)
		async.Before(&news)
		if err := spider.InsertNews(&news); err != nil {
			logging.Error("storage.StoreQA() insert news failed: ", err.Error())
			return false, err
		}

		//// 0630 add: qa-hdy
		//mode := "qa-hdy"
		//if value.SeedID == "322" {
		//	seed := spider.Content2QASeed(value, news.ID, mode)
		//	if seed != nil {
		//		task.TryEnqueue(seed, mode)
		//	}
		//}

		//go async.ProcQA(&news)
		// 写统计数据
		statistics.StatStorage()
	}

	return ok, nil
}

func CopyWechatToResearch(value *spider.NewsContent) {
	if value == nil {
		logging.Error("CopyWechatToResearch 传入空指针")
		return
	}
	re := regexp.MustCompile("(\\D|\\b)[036][\\d]{5}(\\b|\\D)")
	//re:=regexp.MustCompile("(^|('\\D+))[036]['\\d]{5}(('\\D+)|$)")
	checkCodes := re.FindAllString(value.WeChatDetail.Title, -1)
	name := make([]string, 0)
	for _, code := range checkCodes {
		re = regexp.MustCompile("[036][\\d]{5}")
		code = re.FindString(code)
		err := models.DB().Table("b_stock").Where("id=?", code).Pluck("name", &name).Error
		if err != nil || len(name) == 0 {
			logging.Info("复制公众号到研报 code：" + code + "获取股票信息失败")
			continue
		}
		urrType := "个股"
		_, err = spider.InsertURR(&spider.URR{
			Code:         code,
			Title:        &value.WeChatDetail.Title,
			Organization: &value.WeChatDetail.Name,
			Name:         &name[0],
			SpiderTime:   &value.SpiderTime,
			CatchTime:    &value.CatchTime,
			PublishDate:  &value.SpiderTime,
			Type:         &urrType,
			SourceLink:   &value.WeChatDetail.Link,
			Content:      &value.WeChatDetail.Content,
		})
		if err != nil {
			logging.Error("复制公众号到研报 code："+code+"获取股票信息失败", err.Error())
		}
		break
	}
}

func StoreNews(value *spider.NewsContent) (bool, int, error) {
	keyHit := 0
	if value.Type == "wechat-detail" {
		if spider.Contains(value.Title, spider.Fetch("news")) || spider.Contains(value.Title, spider.Fetch("qa")) {
			keyHit = 1
		}
		//拷贝个股相关公众号信息到研报
		go CopyWechatToResearch(value)
	} else {
		if spider.Contains(value.Title, spider.Fetch("news")) || spider.Contains(value.Title, spider.Fetch("company")) {
			keyHit = 1
		}
	}

	//新闻类:去重
	titleMd5 := util.EncodeMD5(value.Title)

	//第一步:查看redis news-zset-single,是否有重复,如果有,则不做处理,没有,则写入,并进入第二步
	bStore, err := addToSet("news:zset:single:"+value.SeedID, titleMd5)
	if err != nil {
		logging.Error("storage.StoreNews() add to single set Errors: " + err.Error())
		return false, 0, err
	}

	var id int
	if bStore {
		var news spider.News
		//全局检查
		if ok, _ := addToSet("news:zset:all", titleMd5); ok {
			////news:zset:all中有该数据,则写mysql,parent_id=1
			//sql := "insert into u_feed (title,link,title_time,seed_id,cata,catch_time,keyword_hit,type,parent_id) values(" + "'" + v.Title + "'," + "'" + v.Link + "'," + "'" + v.TitleTime + "'," + "'" + v.SeedID + "'," + "'" + v.Cata + "'," + "'" + v.CatchTime + "'," + "'" + keyHit + "'," + "'" + v.Type + "','1')"
			//db.Exec(sql)

			news = spider.Content2News(value, 0, keyHit)
		} else {
			////news:zset:all中不存在,则写入redis,并写入mysql,parent_id=0(默认值)并进行第三步
			//sql := "insert into u_feed (title,link,title_time,seed_id,cata,catch_time,keyword_hit,type,parent_id) values(" + "'" + v.Title + "'," + "'" + v.Link + "'," + "'" + v.TitleTime + "'," + "'" + v.SeedID + "'," + "'" + v.Cata + "'," + "'" + v.CatchTime + "'," + "'" + keyHit + "'," + "'" + v.Type + "','0')"
			//db.Exec(sql)

			news = spider.Content2News(value, 1, keyHit)
		}

		// 0701 add: correlation
		async.Before(&news)
		if err := spider.InsertNews(&news); err != nil {
			logging.Error("storage.StoreNews() insert Errors: ", err.Error())
			return false, 0, err
		}
		id = news.ID
		// after insert
		async.After(&news)
		statistics.StatStorage()
	}
	return bStore, id, nil
}

func StoreForecast(value *spider.NewsContent) (bool, error) {
	if value.Forecast == (spider.Forecast{}) {
		return false, fmt.Errorf("empty forecast")
	}
	upf := spider.Forecast2UPF(value.Forecast)
	_, err := spider.InsertUPF(&upf)
	return err == nil, err
}

func StoreAnnouncement(value *spider.NewsContent) (bool, error) {
	if value.Announcement == (spider.Announcement{}) {
		return false, fmt.Errorf("empty announcement")
	}
	upa := spider.Announcement2UPA(value.Announcement)
	err := spider.InsertUPA(upa)
	return err == nil, err
}

func StoreDisclosure(value *spider.NewsContent) (bool, error) {
	if value.Disclosure == (spider.Disclosure{}) {
		return false, fmt.Errorf("empty disclosure")
	}
	bPush := false
	upa := spider.Disclosure2UPA(value.Disclosure)
	upf := spider.Disclosure2UPF(value.Disclosure)
	err := spider.InsertUPA(&upa)
	if err != nil {
		return false, err
	}
	bPush, err = spider.InsertUPF(&upf)
	if bPush {
		data := map[string]interface{}{
			"code":                    upf.Code,
			"name":                    upf.Name,
			"disclosure_time":         upf.DisclosureTime,
			"disclosure_time_history": upf.DisclosureTimeHistory,
			"disclosure_change_at":    upf.DisclosureChangeAt,
			"type":                    "disclosure_change",
		}
		bt, _ := json.Marshal(data)
		md5 := util.EncodeMD5(upf.Code + "&" + *upf.DisclosureTime)
		msg.Push(string(bt), message.Message_System_Performance, md5)
	}
	return err == nil, err
}

func StoreIWC(value *spider.NewsContent) (bool, error) {
	if value.IWC == (spider.IWC{}) {
		return false, fmt.Errorf("empty iwc")
	}
	iwc := spider.IWC2UPA(value.IWC)
	err := spider.InsertUPA(iwc)
	return err == nil, err
}

func StoreHDY(value *spider.NewsContent) (bool, error) {
	if value.HDY == (spider.HDY{}) {
		return false, fmt.Errorf("empty hdy")
	}
	err := spider.UpdateHDY(&value.HDY)
	return err == nil, err
}

func StoreWbDetail(value *spider.NewsContent) (bool, int, error) {
	if value.WBDetail == (spider.WBDetail{}) {
		return false, 0, fmt.Errorf("empty wb-detail")
	}
	content := spider.ParseWBDetail(value)
	if content != nil {
		return StoreNews(content)
	} else {
		return false, 0, fmt.Errorf("empty wb-detail")
	}
}

func ProcWb(value *spider.NewsContent) (bool, error) {
	if value.WB == (spider.WB{}) {
		return false, fmt.Errorf("empty wb")
	}
	mode := "wb-detail"
	seed := spider.Content2WBSeed(value, mode)

	md5 := util.EncodeMD5(value.WB.ID + "&" + value.WB.UId)
	bStore, err := addToSet("wb:zset:single:", md5)
	if err != nil {
		logging.Error("storage.ProcWb() add to single set Errors: " + err.Error())
		return false, err
	}
	if seed != nil && bStore {
		return task.TryEnqueue(seed, mode), nil
	}
	return false, nil
}

func ProcResearchList(value *spider.NewsContent) (bool, error) {
	if value.ResearchList == (spider.ResearchList{}) || value.ResearchList.Title == nil {
		return false, nil
	}

	modes := strings.Split(value.Type, "-")
	modes[len(modes)-1] = "detail"
	mode := strings.Join(modes, "-")
	md5 := util.EncodeMD5(value.SeedID + "&" + *value.ResearchList.Title)
	bStore, err := addToSet("research:zset:list:", md5)
	if err != nil {
		logging.Error("storage.ProcResearchList() add to list set Errors: " + err.Error())
		return false, err
	}
	if bStore {
		urr := spider.ResearchList2URR(&value.ResearchList)
		urr.SeedID = &value.SeedID
		urr.CatchTime = &value.CatchTime
		urr.SpiderTime = &value.SpiderTime
		var id int
		if urr.ID > 0 && urr.Parent == 1 {
			log, _ := json.Marshal(urr)
			logv, _ := json.Marshal(value)
			logging.Error("研报判重：ProcResearchList", string(log))
			logging.Error("研报判重：报文", string(logv))
		}
		if ret, err := spider.InsertURR(urr); err != nil {
			return false, err
		} else {
			id = ret.ID
		}
		rm := assemble(urr, nil)
		if rm != nil {
			pushRecommend(rm)
		}
		maps := make(map[string]interface{})
		maps["code"] = urr.Code
		userID, _ := template.GetUserIdByCode(maps)
		if userID > 0 {
			PushMsg(userID)
		}
		seed := spider.Content2ResearchSeed(value, id, mode)
		if seed != nil {
			return task.TryEnqueue(seed, mode), nil
		}
	}

	return false, nil
}

func ProcResearchDetail(value *spider.NewsContent) (bool, error) {
	//if value.ResearchDetail.ID == 0 {
	//	return false, nil
	//}

	mode := "research-pdf"
	//md5 := util.EncodeMD5(*value.ResearchDetail.SourceLink)
	var key string
	if value.ResearchDetail.ID == 0 {
		if value.ResearchDetail.SourceLink == nil {
			logging.Error("storage.ProcResearchDetail() Nil id and source_link")
			return false, nil
		}
		key = util.EncodeMD5(*value.ResearchDetail.SourceLink)
	} else {
		key = strconv.Itoa(value.ResearchDetail.ID)
	}
	bStore, err := addToSet("research:zset:detail:", key)
	if err != nil {
		logging.Error("storage.ProcResearchDetail() add to detail set Errors: " + err.Error())
		return false, err
	}
	if bStore {
		urr := spider.ResearchDetail2URR(&value.ResearchDetail)
		if urr.ID == 0 {
			urr.SeedID = &value.SeedID
			urr.CatchTime = &value.CatchTime
			urr.SpiderTime = &value.SpiderTime

		}
		if urr.ID > 0 && urr.Parent == 1 {
			log, _ := json.Marshal(urr)
			logv, _ := json.Marshal(value)
			logging.Error("研报判重：ProcResearchDetail", string(log))
			logging.Error("研报判重：报文", string(logv))
		}
		if err := spider.UpdateURR(urr); err != nil {
			logging.Error("storage.ProcResearchDetail() update urr Errors: " + err.Error())
			return false, err
		}
		if value.ResearchDetail.ID > 0 {
			urr = spider.QueryURR(value.ResearchDetail.ID)
		} else {
			value.ResearchDetail.ID = urr.ID
		}
		var preClose float64
		for i := 0; i < len(value.ResearchDetail.Data); i++ {
			urrd := spider.ResearchDetail2URRD(&value.ResearchDetail, &value.ResearchDetail.Data[i])
			urrd.SeedID = &value.SeedID
			if urrd, err = spider.InsertURRD(urrd); err != nil {
				logging.Error("storage.ProcResearchDetail() insert urrd Errors: " + err.Error())
				continue
			}
			//logging.Info("storage.ProcResearchDetail() insert urrd: ", urrd)
			//logging.Info("storage.ProcResearchDetail() using urr: ", urr)
			maps := make(map[string]interface{})
			maps["code"] = urrd.Code
			userID, _ := template.GetUserIdByCode(maps)
			if userID > 0 {
				logging.Info("storage.PushMsg() insert urrd: ", urrd)
				PushMsg(userID)
			}
			rm := assemble(urr, urrd)
			if rm != nil {
				pushRecommend(rm)
			}
			if urrd != nil && urrd.PreClose != nil {
				preClose = *urrd.PreClose
			}
		}
		if value.ResearchDetail.ID > 0 {
			seed := spider.Content2PDFSeed(value, value.ResearchDetail.ID, mode, preClose)
			if seed != nil {
				return task.TryEnqueue(seed, mode), nil
			}
		}
	}

	return false, nil
}

func StoreResearchPDF(value *spider.NewsContent) (bool, error) {
	if value.ResearchPDF.Content == nil {
		return false, nil
	}

	md5 := util.EncodeMD5(strconv.Itoa(value.ResearchPDF.ReportID) + "&" + *value.ResearchPDF.Content)
	bStore, err := addToSet("research:zset:pdf:", md5)
	if err != nil {
		logging.Error("storage.StoreResearchPDF() add to pdf set Errors: " + err.Error())
		return false, err
	}

	if bStore {
		urr := spider.QueryURR(value.ResearchPDF.ReportID)
		if urr == nil {
			logging.Error("storage.StoreResearchPDF() Invalid urr id: ", value.ResearchPDF.ReportID)
			return false, nil
		}
		updateUrr := spider.URR{
			Content:          value.ResearchPDF.Content,
			PdfRaise:         value.ResearchPDF.PdfRaise,
			PdfFirstCoverage: value.ResearchPDF.PdfFirstCoverage,
		}
		updateUrr.ID = urr.ID
		/*urr.Content = value.ResearchPDF.Content
				urr.PdfRaise = value.ResearchPDF.PdfRaise
		<<<<<<< HEAD
				urr.PdfFirstCoverage = value.ResearchPDF.PdfFirstCoverage
				if urr.ID>0 && urr.Parent==1{
					logging.Error("研报判重：StoreResearchPDF",&urr)
					logging.Error("研报判重：报文",&value)
				}
				if err := spider.UpdateURR(urr); err != nil {
		=======
				urr.PdfFirstCoverage = value.ResearchPDF.PdfFirstCoverage*/
		if updateUrr.ID > 0 && updateUrr.Parent == 1 {
			log, _ := json.Marshal(updateUrr)
			logv, _ := json.Marshal(value)
			logging.Error("研报判重：StoreResearchPDF", string(log))
			logging.Error("研报判重：报文", string(logv))
		}
		if err := spider.UpdateURR(&updateUrr); err != nil {
			logging.Error("storage.StoreResearchPDF() update urr Errors: " + err.Error())
			return false, err
		}
		var err error
		for i := 0; i < len(value.ResearchPDF.Data); i++ {
			if urrd, e := spider.InsertURRD(&value.ResearchPDF.Data[i]); e != nil {
				logging.Error("storage.StoreResearchPDF() insert urrd Errors: " + e.Error())
				err = e
			} else {
				//urr = spider.QueryURR(urrd.ReportID)
				rm := assemble(urr, urrd)
				if rm != nil {
					pushRecommend(rm)
				}
			}
		}

		return err == nil, err
	}

	return false, nil
}

func ProcIpoDcList(value *spider.NewsContent) (bool, error) {
	if util.IsEmpty(value.IpoDcList.Company) {
		logging.Error(fmt.Sprintf("storage.go ProcIpoDcList() err :公司名为空,value = %v", value))
		return false, fmt.Errorf("storage.go ProcIpoDcList() err :公司名为空")
	}
	// 1 去重入mysql,东财表,并设置redis计数器
	//  将顶级存入redis,  string : key id(东财表主键id),value num(二级数量,初始为0)
	id, company, _, err := spider.DuplicationInsertUIpoEquity(value)
	if err != nil {
		return false, err
	}
	// 2 入JonChan:ipo-tyc-list
	var seed *spider.Seed
	mode := "ipo-tyc-list"
	seedId, err := strconv.Atoi(value.SeedID)
	if err != nil {
		return false, err
	}
	seed = spider.Content2IopEquitySeed(seedId, id, mode, company)
	if seed != nil {
		return task.TryEnqueue(seed, mode), nil
	}
	return false, nil
}

func ProcIpoTycList(value *spider.NewsContent) (bool, error) {
	//更新东财表,cid,equity_link
	_, err := spider.UpdateUIpoEquity(value)
	if err != nil {
		return false, err
	}
	//投任务队列
	var seed *spider.Seed
	mode := "ipo-tyc-detail"
	seed = spider.Content2IopEquityTycSeed(value.SeedID, value.IpoTycList.Id, value.IpoTycList.Cid, mode)
	if seed != nil {
		return task.TryEnqueue(seed, mode), nil
	}
	return true, nil
}

func ProcIpoTycDetail(value *spider.NewsContent) (bool, error) {
	var count int
	//入第二张表,去重(通过cid,id),返回东财表主键id
	id, _, err := spider.DuplicationInsertUIopEquityRelationship(value)
	if err != nil {
		return false, err
	}

	//入队列(条件:hasnode=true,并且cid在东财表中有,不是上市公司,不是人,cid!=0)
	inDc := false
	models.DB().Table("u_iop_equity").Where("cid = ?", value.IpoTycDetail.ParentCid).Count(&count)
	if count > 0 {
		inDc = true
	}

	//logging.Info(fmt.Sprintf("二级处理,判断是否入队三级: value = %v,cid = %v,value.IpoTycDetail.HasNode = %v,inDc = %v,value.IpoTycDetail.NameType = %v,value.IpoTycDetail.Cid = %v", value, value.IpoTycDetail.Cid, value.IpoTycDetail.HasNode, inDc, value.IpoTycDetail.NameType, value.IpoTycDetail.Cid))

	if value.IpoTycDetail.HasNode && inDc && value.IpoTycDetail.NameType == 102 && value.IpoTycDetail.Cid != 0 {
		logging.Info(fmt.Sprintf("符合入detail队列条件,value=%v", value))
		//将符合条件的二级加入队列 ,三级只入mysql,不入队列
		//并且设置redis计数+1
		var u_i_e spider.UIopEquityRelationship
		models.DB().Table("u_iop_equity_relationship").Select("iop_equity_id").Where("cid = ?", value.IpoTycDetail.Cid).Find(&u_i_e)
		redisNum := gredis.IncrBy("ipo:"+strconv.Itoa(u_i_e.IopEquityID), 1)
		logging.Info(fmt.Sprintf("设置符合条件的二级加入队列,id = %v,redisNum=%v", u_i_e.IopEquityID, redisNum))

		//投任务队列
		var seed *spider.Seed
		mode := "ipo-tyc-detail"
		seed = spider.Content2IopEquityTycSeed2(value.SeedID, value.IpoTycDetail.Cid, mode)
		if seed != nil {
			return task.TryEnqueue(seed, mode), nil
		}
	}
	//logging.Info(fmt.Sprintf("不符合入detail队列条件,value=%v", value))
	//将三级的父级cid插入set,之后判断set成员个数是否和 刚才哪个一致,如果一致,则ws推送
	//否则不推送
	var data spider.UIopEquityRelationship
	models.DB().Table("u_iop_equity_relationship").Select("iop_equity_id").Where("cid = ?", value.IpoTycDetail.ParentCid).Find(&data)
	//入set  东财主键id  二级cid
	//logging.Info(fmt.Sprintf("redis入三级集合:ipo:third_req_times_%v,parent_cid=%v", data.IopEquityID, value.IpoTycDetail.ParentCid))
	gredis.SAdd("ipo:third_req_times_"+strconv.Itoa(data.IopEquityID), strconv.Itoa(value.IpoTycDetail.ParentCid))

	//push判断,如果 相等 则push否和 不处理
	if IpoPushCondition(data.IopEquityID) {
		//logging.Info("达到触发ws推送条件,data.IopEquityID = %v", data.IopEquityID)
		var ipoWsMsg spider.IpoWsPushMessageJson
		err := spider.CalcIpoWsPushMsg(&ipoWsMsg, data.IopEquityID)
		ipoWsMsg.QuantityToBeIssued, _ = util.ConvScientificCount(ipoWsMsg.QuantityToBeIssued)
		//logging.Info(fmt.Sprintf("触发ws推送结构体,ipoWsMsg = %v", ipoWsMsg))
		//a, _ := json.Marshal(ipoWsMsg)
		//logging.Info(fmt.Sprintf("触发ws推送json,a = %v", string(a)))
		if err != nil {
			logging.Error(fmt.Sprintf("生成ipo-websocket推送失败,ipoWsMsg = %v", ipoWsMsg))
		}
		//logging.Info(fmt.Sprintf("正式推送,ipoWsMsg = %v", ipoWsMsg))
		//推送策略,二级有上市公司的就推,或者 二级不是上市公司,但是其下面的三级有上市公司也推
		if len(ipoWsMsg.SecondCompany) > 0 || (len(ipoWsMsg.SecondCompany) == 0 && len(ipoWsMsg.ThirdCompany) > 0) {
			pushIPO(id)
		}
	}

	logging.Info("4个条件筛选失败,不能入队3级")
	return true, nil
}

func ProcPopularStock(value *spider.NewsContent) (bool, error) {
	if util.IsEmpty(value.PopularStock) {
		err := fmt.Errorf("storage ProcPopularStock() empty content: %v", value)
		return false, err
	}
	ok := spider.UpdatePopular(&value.PopularStock)
	return ok, nil
}

func ProcWeChatList(value *spider.NewsContent) (bool, error) {
	if util.IsEmpty(value.WeChatList) {
		err := fmt.Errorf("storage ProcWeChatList() empty content: %v", value)
		return false, err
	}
	mode := "wechat-detail"
	seed := spider.Content2WeChatDetail(value.WeChatList.Link, mode)

	md5 := util.EncodeMD5(value.WeChatList.Link)
	bStore, err := addToSet("wechat:zset:single:", md5)
	if err != nil {
		logging.Error("storage.ProcWeChatList() add to single set Errors: " + err.Error())
		return false, err
	}
	if seed != nil && bStore {
		return task.TryEnqueue(seed, mode), nil
	}

	return true, nil
}

func StoreWeChatDetail(value *spider.NewsContent) (bool, int, error) {
	if value.WeChatDetail == (spider.WeChatDetail{}) {
		return false, 0, fmt.Errorf("empty wechat-detail")
	}
	content := spider.ParseWeChatDetail(value)
	if content != nil {
		return StoreNews(content)
	} else {
		return false, 0, fmt.Errorf("empty wb-detail")
	}
}

func StoreTwitter(value *spider.NewsContent) (bool, int, error) {
	if value.Twitter == (spider.Twitter{}) {
		return false, 0, nil
	}
	md5 := util.EncodeMD5(value.Twitter.Title)
	bStore, err := addToSet("twitter:zset:single:", md5)
	if err != nil {
		logging.Error("storage.StoreTwitter() add to single set Errors: " + err.Error())
		return false, 0, err
	}

	if bStore {
		content := spider.ParseTwitterDetail(value)
		if content != nil {
			return StoreNews(content)
		} else {
			return false, 0, nil
		}
	}
	return false, 0, nil
}

func StoreReorganization(value *spider.NewsContent) (bool, error) {
	value.Reorganization.CatchTime = value.CatchTime
	value.Reorganization.SpiderTime = value.SpiderTime
	err := spider.InsertReorganization(&value.Reorganization)
	return err == nil, err
}

func StoreNoticeList(value *spider.NewsContent) (bool, error) {
	p := &value.CninfoNoticList
	err := p.Save()
	return err == nil, err
}

func StoreIndustryResearch(value *spider.NewsContent) (bool, error) {
	var err error
	if value.YbDcIndustryDetail != (spider.IndustryResearch{}) {
		value.YbDcIndustryDetail.SeedID = value.SeedID
		if err = spider.UpdateIndustryResearch(&value.YbDcIndustryDetail); err != nil {
			return false, err
		}
	}
	if value.YbHbIndustry != (spider.IndustryResearch{}) {
		md5 := util.EncodeMD5(value.YbHbIndustry.Title + "&" + value.YbHbIndustry.Organization)
		bStore, _ := addToSet("industry:zset:single:", md5)
		if bStore {
			value.YbHbIndustry.SeedID = value.SeedID
			err = spider.UpdateIndustryResearch(&value.YbHbIndustry)
		}
	}
	return err == nil, err
}

func IpoPushCondition(key int) bool {
	num1, _ := gredis.GetInt("ipo:" + strconv.Itoa(key))
	logging.Info(fmt.Sprintf("ws redis:num1=%v", num1))
	logging.Info(fmt.Sprintf("ws redis:key=%v", key))
	num2, _ := gredis.SCard("ipo:third_req_times_" + strconv.Itoa(key))
	logging.Info(fmt.Sprintf("ws redis:num2=%v", num2))

	if num1 == int64(num2) {
		logging.Info("num1 = %d,num2 = %d,相等", num1, num2)
		return true
	} else {
		logging.Info("num1 = %d,num2 = %d,不相等", num1, num2)
		return false
	}
}

// 增量数据写快照
func Snapshot(report *spider.Report) error {
	//将这些增量数据写入 11库,string类型,key(自定义字母+时间戳),value ,Content中的一条
	//conn, err := redis.Dial("tcp", setting.RedisSetting.Host, redis.DialPassword(setting.RedisSetting.Password), redis.DialDatabase(11))
	//if err != nil {
	//	logging.Error("storage.Snapshot ")
	//	return err
	//}
	//defer conn.Close()
	//data, err := json.Marshal(report.Content)
	//if err != nil {
	//	logging.Error("storage.Snapshot() marshal json Errors: ", err.Error())
	//	return err
	//}
	//time := util.GenMicTime()
	//conn := gredis.Clone(11)
	////一次性插入
	//return conn.Set("post_report_"+report.ID+"_"+strconv.FormatInt(time, 10), string(data), 24*60*60*2)
	// 11.24 因存量问题，暂停写入快照
	return nil
}

func addToSet(key, field string) (bool, error) {
	timeMic := util.GenMicTime() //毫秒
	ret, err := gredis.ZNewAdd(key, strconv.FormatInt(timeMic, 10), field)
	if err != nil {
		logging.Error("storage.addToSet() Errors: " + err.Error())
	}
	return err == nil && ret.(int64) > 0, err
}

type RecommendMessage struct {
	//recommender: 推荐人（写系统推荐好了）
	//link: 研报pdf链接地址
	//date: 日期（07-23）,
	//year: 年份（2020），
	//organization: 机构名称，
	//level: 评级,
	//type: 'report’,（就填这个）
	//time: 时间 （12:33:20
	//code: 股票代码,
	//author: 作者,
	//name: 股票名称,
	// content: 标题,
	//target:目标价,
	//yesterdayPrice:昨收价,
	// upSpace: 上涨空间,
	//highly_recommend: true （都为true）
	Recommender     string   `json:"recommender"`
	Link            *string  `json:"link"`
	Date            string   `json:"date"`
	Year            string   `json:"year"`
	Organization    *string  `json:"organization"`
	Level           *string  `json:"level"`
	Type            *string  `json:"type"`
	Time            string   `json:"time"`
	Code            string   `json:"code"`
	Author          *string  `json:"author"`
	Name            *string  `json:"name"`
	Content         *string  `json:"content"`
	Target          *string  `json:"target"`
	PreClose        *float64 `json:"yesterdayPrice"`
	RisingSpace     *string  `json:"upSpace"`
	HighlyRecommend bool     `json:"highly_recommend"`
	Reason          *string  `json:"reason"`
}

func assemble(urr *spider.URR, urrd *spider.URRD) *RecommendMessage {
	if urr == nil {
		return nil
	}
	var reason *string = nil
	var reasonArr []string
	var reasonStr string
	//switch {
	if urrd != nil && len(urrd.RisingSpaces) > 0 && urrd.RisingSpaces[len(urrd.RisingSpaces)-1] >= 0.35 {
		//上涨空间大于35%
		reasonArr = append(reasonArr, "上涨空间大于35%")
	}
	if urr.LevelChange != nil && *urr.LevelChange == "首次" {
		//首次推荐
		reasonArr = append(reasonArr, "首次推荐")
	}
	if urr.LevelCalc != nil && *urr.LevelCalc == "up" {
		//评级上调
		reasonArr = append(reasonArr, "评级上调")
	}
	//if urr.Organization != nil && urr.Author != nil && importantAuthor(urr) {
	//	//重点机构作者
	//	reasonArr = append(reasonArr, "重点券商")
	//	if urr.Type != nil {
	//		t := "重点券商"
	//		urr.Type = &t
	//	}
	//}
	if len(reasonArr) > 0 {
		reasonStr = strings.Join(reasonArr, "，")
		reason = &reasonStr
	} else {
		return nil
	}

	var tm time.Time
	if urr.PublishDate != nil && *urr.PublishDate != "" {
		tm, _ = time.Parse("2006-01-02T15:04:05Z07:00", *urr.PublishDate)
	} else {
		tm = time.Now()
	}
	if urr.CatchTime != nil && *urr.CatchTime != "" {
		tm, _ = time.Parse("2006-01-02T15:04:05Z07:00", *urr.CatchTime)
	} else {
		tm = time.Now()
	}

	report := "system_report"
	rm := RecommendMessage{
		Recommender: "系统推荐",
		Link:        urr.SourceLink,
		//Date:            tm.Format("01-02"),
		//Year:            tm.Format("2006"),
		Organization:    urr.Organization,
		Level:           urr.Level,
		Type:            &report,
		Time:            tm.Format("2006-01-02 15:04:05"),
		Code:            urr.Code,
		Author:          urr.Author,
		Name:            urr.Name,
		Content:         urr.Title,
		HighlyRecommend: true,
		Reason:          reason,
	}
	if urrd != nil {
		rm.Target = urrd.TargetPrice
		rm.PreClose = urrd.PreClose
		rm.RisingSpace = urrd.RisingSpace
	}

	return &rm
}

func pushRecommend(rm *RecommendMessage) {
	bt, _ := json.Marshal(*rm)
	var title string
	if rm.Content != nil {
		title = *rm.Content
	}
	md5 := util.EncodeMD5(fmt.Sprintf("%s&%s", rm.Code, title))
	msg.Push(string(bt), message.Message_System_Report, md5)
	//msg.Push(string(bt), message.Message_Report)
}
func push(m map[string]interface{}, userId int, msgType message.MessageType) {
	ws := websocket.GetInstance()
	bt, _ := json.Marshal(m)
	msg := message.ParseChanelMsg(msgType, "system", strconv.Itoa(userId), string(bt), "home", true)
	ws.SysSend(msg)
}
func PushMsg(userId int) {
	data := template.AssmblePushMsg(userId)
	push(map[string]interface{}{"data": data}, userId, message.Message_User_Center)
}

//ipo股权页面推送
func pushIPO(id int) {
	//logging.Info(fmt.Sprintf("pushIPO()推送消息,rm=%+v", rm))
	var data struct {
		Type string `json:"type"`
		*template.UIpoEquity
	}
	data.UIpoEquity = template.AssembleIPO(template.QueryIPOByID(id))
	if data.UIpoEquity != nil {
		data.Type = "ipo"
		bt, _ := json.Marshal(data)
		md5 := util.EncodeMD5(fmt.Sprintf("%s&%s", data.Company, data.DeclareTime))
		msg.Push(string(bt), message.Message_System_IPO, md5)
	}
}

func importantAuthor(urr *spider.URR) bool {
	db := models.DB()
	var industry []string
	err := db.Table("p_public_company").Where("stock_code = ?", urr.Code).Pluck("industry", &industry).Error
	if err != nil || len(industry) == 0 {
		return false
	}
	ins := strings.Split(industry[0], " ")
	var authors []string
	err = db.Table("u_industry_securities_author_relationship").Where("industry_name = ?", ins[0]).Where("securities = ?", *urr.Organization).Pluck("author", &authors).Error
	if err != nil || len(authors) == 0 {
		return false
	}
	aus := strings.Split(strings.Replace(*urr.Author, " ", "", -1), ",")
	if util.ContainsAny(aus, authors...) {
		return true
	}
	return false
}

func StoreAuctionList(value *spider.NewsContent) (bool, error) {
	auctionList := &value.AuctionList
	ret, err := auctionList.Save()
	if err == nil && ret {
		pushRule(auctionList)
	}
	return err == nil, err
}

func StoreInvestigationList(value *spider.NewsContent) (bool, error) {
	var io spider.InvestigationIO
	var seed *spider.Seed
	if value.InvestigationIO == (spider.InvestigationIO{}) || value.InvestigationIO.Title == nil {
		if value.InvestigationSHIO == (spider.InvestigationIO{}) || value.InvestigationSHIO.Title == nil {
			return false, nil
		} else {
			io = value.InvestigationSHIO
		}
	} else {
		io = value.InvestigationIO
	}
	dto := spider.InvestigationDTO{}
	err := models.DB().Table("p_investigation").
		Where("code=? AND title=?", io.Code, io.Title).
		First(&dto).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	if dto.Id > 0 {
		err = models.DB().Table("p_investigation").Where("id=?", dto.Id).Updates(io).Error
		if dto.AliLink == "" {
			//modes:=strings.Split(value.Type,"-")
			//mode:=strings.Join(modes[1:],"-")+"-detail"
			io.Id = dto.Id
			seed = value.InvestigationIO2Seed("investigation-detail", &io)
		} else {
			return false, nil
		}
	} else {
		err = models.DB().Table("p_investigation").Create(&io).Error
		seed = value.InvestigationIO2Seed("investigation-detail", &io)
		if io.Id > 0 {
			//err := models.DB().Table("p_investigation").
			//	Where("id=?", io.Id).
			//	First(&dto).Error
			//if err != nil {
			//	logging.Error("StoreInvestigationList() 获取"+strconv.Itoa(io.Id)+"dto信息失败", err.Error())
			//} else {
			logging.Info("异步回测")
			if io.PublishTime == nil {
				logging.Error("调研纪要爬虫输入publishTime为空")
			} else {
				publishTime, err := time.ParseInLocation(util.YMDHMS, *io.PublishTime, time.Local)
				if err != nil {
					logging.Error("调研纪要publishTime解析报错："+*io.PublishTime, err.Error())
				} else {
					if !(io.Code == nil || *io.Code == "") {
						dto = spider.InvestigationDTO{
							Id:          io.Id,
							Code:        *io.Code,
							PublishTime: &publishTime,
						}
						go async.GetChangeAndFallback(&dto)
					} else {
						logging.Error("调研纪要code缺失", io.Id)
					}
				}
			}
			//}
		} else {
			logging.Error("StoreInvestigationList() io 返回id信息失败", err)
		}
	}
	if err != nil {
		return false, err
	}
	return task.TryEnqueue(seed, "investigation-detail"), nil

}

func StoreInvestigationDetail(value *spider.NewsContent) (bool, error) {
	if value.InvestigationDetail == (spider.InvestigationIO{}) || value.InvestigationDetail.Id <= 0 {
		return false, nil
	}
	io := spider.InvestigationIO{
		//Id:      value.InvestigationDetail.Id,
		Content:            value.InvestigationDetail.Content,
		AliLink:            value.InvestigationDetail.AliLink,
		CompNum:            value.InvestigationDetail.CompNum,
		InstitutionContent: value.InvestigationDetail.InstitutionContent,
	}
	err := models.DB().Table("p_investigation").Where("id=?", value.InvestigationDetail.Id).Updates(io).Error
	if err == nil && value.InvestigationDetail.Content != nil {
		async.SetKeyWords(&spider.InvestigationDTO{
			Id:      value.InvestigationDetail.Id,
			Content: *value.InvestigationDetail.Content,
		})
	}
	return err == nil, err

}

//test
func PushIPO(id int) {
	pushIPO(id)
}

// 1、上市公司有拍卖（拍卖前3个交易日提示），一家公司每日最多推送一次；
//
// 2、有人报名推送；
//
// 3、拍卖成功；终止拍卖；流拍
func pushRule(auc *spider.AuctionList) {
	pushAuctionKey := time.Now().Format("20060102") + "auctionpush"
	ret, err := gredis.SMEMBERS(pushAuctionKey, auc.Keyword)
	if err != nil || ret {
		return
	}
	// 流拍的不推送
	if auc.Status == 2 {
		return
	}
	if auc.StartTime == "" {
		return
	}
	data := make(map[string]interface{})
	data["type"] = "auction"
	// 获取当前记录的前三天合计占股比超过5%
	threeAuction := spider.AuctionPush{}
	preThreeDay := time.Now().AddDate(0, 0, -3).Format(util.YMD)
	err = models.DB().Table("u_auction").
		Select("sum(share_ratio) as share_ratio").
		Where("keyword = ? ", auc.Keyword).
		Where("created_at >= ?", preThreeDay).
		Order("id desc").Find(&threeAuction).Error

	if err == nil && threeAuction.TotalShareRatio.Decimal.Sub(decimal.NewFromInt(5.00)).Sign() > 0 {
		data["content"] = fmt.Sprintf("%s近日拍卖%v%%股权", auc.Keyword, threeAuction.TotalShareRatio.Decimal)
		gredis.SAdd(pushAuctionKey, auc.Keyword)
		pushAuction(data, message.Message_Auction)
		return
	}
	// 获取之前的记录
	preAuctionList := spider.AuctionList{}
	err = models.DB().Table("u_auction").Where("title = ? ", auc.Title).Where("platform = ? ", auc.Platform).
		Where("start_time = ? ", auc.StartTime).Where("keyword != ? ", "").Order("id desc").First(&preAuctionList).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("pushRule err is ", err)
		return
	}
	data["content"] = fmt.Sprintf("上市公司%s：有拍卖，%s开始拍卖", auc.Keyword, auc.StartTime)
	gredis.SAdd(pushAuctionKey, auc.Keyword)
	pushAuction(data, message.Message_Auction)
	return
}

// 期货更新详情
func StoreFuturesSetpriceInfo(value *spider.NewsContent) (bool, error) {
	futureInfoList := &value.FuturesSetprice
	err := futureInfoList.SaveDetail()
	return err == nil, err
}

// 更新stop_range
func StoreFuturesUpInfo(value *spider.NewsContent) (bool, error) {
	futureInfoList := &value.FutureUp
	err := futureInfoList.SaveDetail()
	return err == nil, err
}
func StoreFuturesInfo(value *spider.NewsContent) (bool, error) {
	futureInfoList := &value.FuturesInfo
	err := futureInfoList.Save()
	futureInfoList.Time = time.Now().Format(util.YMDHM)
	ret := futureInfoList.PushStopRangeRule()
	if len(ret) > 0 {
		logging.Info("futrure info push 期货涨幅离涨停价差1%提示")
		pushAuction(ret, message.Message_Futures_Info) // 期货涨幅离涨停价差1%提示
	}
	// retThree := futureInfoList.ThreeMonthPush()
	// if len(retThree) > 0 {
	// 	logging.Info("futrure info push 三月内最高")
	// 	pushAuction(retThree, message.Message_Futures_Info) // 3个月内创新高
	// }
	if futureInfoList.Mechanism == "" {
		return err == nil, err
	}
	//retThree := futureInfoList.PushThreeMinute()
	//if len(retThree) > 0 {
	//	logging.Info("futrure info push 五分钟内拉升3个点")
	//	pushAuction(retThree, message.Message_Futures_Info) // 五分钟内拉升3个点
	//}
	fiveIncrease := futureInfoList.PushStopRangeRuleIncrease()
	if len(fiveIncrease) > 0 {
		logging.Info("futrure info push 涨幅超过5%")
		pushAuction(fiveIncrease, message.Message_Futures_Info) // 涨幅超过5%
	}
	highest := futureInfoList.PushStopRangeRuleThreeDay()
	if len(highest) > 0 {
		logging.Info("futrure info push 3天总涨幅超过10%")
		pushAuction(highest, message.Message_Futures_Info) // 3天总涨幅超过10%
	}
	return err == nil, err
}
func pushAuction(m map[string]interface{}, msgType message.MessageType) {
	bt, _ := json.Marshal(m)
	msg.Push(string(bt), msgType)
}

func StoreForecastTHS(value *spider.NewsContent) (bool, error) {
	if value.ForecastTHS == (spider.Forecast{}) {
		return false, fmt.Errorf("empty forecast")
	}
	upf := spider.Forecast2UPF(value.ForecastTHS)
	_, err := spider.InsertUPFTHS(&upf)
	return err == nil, err
}

func TestAuction() {
	// rand.Seed(time.Now().Unix()) // unix 时间戳，秒
	// i := rand.Intn(100)
	// fmt.Println(i)
	auc := spider.AuctionList{
		Title:      "晋宁区农村信用合作联社股金（0100000349575779）646816股",
		Link:       "https://paimai.jd.com/115533440",
		StartTime:  "2020-03-01 12:24:29",
		SpiderTime: "2020-11-05 15:24:44",
		Keyword:    "000003",
		OfferCount: "1",
		Status:     2,
		Platform:   "京东拍卖",
	}
	_, err := auc.Save()
	fmt.Println(err)
	pushRule(&auc)
}
