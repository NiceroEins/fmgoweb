package storage

import (
	msg "datacenter/middleware/message"
	"datacenter/models"
	"datacenter/models/message"
	"datacenter/models/spider"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"encoding/json"
	"testing"
)

func TestPushIPO(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()
	PushIPO(83)
	PushIPO(91)
}

func TestPushDisclosure(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	data := map[string]interface{}{
		"code":                    "123456",
		"name":                    "测试测试",
		"disclosure_time":         "2020-09-18",
		"disclosure_time_history": "2020-09-11",
		"disclosure_change_at":    "2020-09-18 01:02:03",
		"type":                    "disclosure_change",
	}
	bt, _ := json.Marshal(data)
	md5 := util.EncodeMD5("123123" + "&" + "2020-09-18")
	msg.Push(string(bt), message.Message_System_Performance, md5)
}

func TestCopyWechatToResearch(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()
	CopyWechatToResearch(&spider.NewsContent{
		WeChatDetail:spider.WeChatDetail{
			Title: "600189ff660841ds3009733",
		},
	})
}
