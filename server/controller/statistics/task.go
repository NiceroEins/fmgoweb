package statistics

import (
	"datacenter/pkg/gredis"
	"datacenter/pkg/util"
	"strconv"
)

func StatTask(cate string, num int) {
	switch cate {
	case "":
	}
}

//统计 day生成任务个数
func DayProductTaskNum() {
	timeNow := util.GenSecondTime()
	_, timeEnd := util.GenBeginEndTime()
	expireTime := timeEnd - timeNow

	gredis.IncrBy("day_product_task_num", 1)
	gredis.Expire("day_product_task_num", expireTime)

}

func SendTask(num int) {
	go daySendTaskNum(num)
	go hourSendTaskNum(num)
	go secondSendTaskNum(num)
}

func ProduceTask() {
	go HourProductTaskNum()
	go DayProductTaskNum()
	go SecondProductTaskNum()
}

//统计 day 发任务个数
func daySendTaskNum(num int) {
	timeNow := util.GenSecondTime()
	_, timeEnd := util.GenBeginEndTime()
	expireTime := timeEnd - timeNow

	gredis.IncrBy("day_send_task_num", num)
	gredis.Expire("day_send_task_num", expireTime)
}

//统计 hour生成任务个数
func HourProductTaskNum() {
	timeSecondNow := util.GenSecondTime()
	key := timeSecondNow % 3600

	gredis.IncrBy("hour_product_task_num:"+strconv.FormatInt(key, 10), 1)
	gredis.Expire("hour_product_task_num:"+strconv.FormatInt(key, 10), 3599)
}

//统计 hour 发任务个数
func hourSendTaskNum(num int) {
	timeSecondNow := util.GenSecondTime()
	key := timeSecondNow % 3600

	gredis.IncrBy("hour_send_task_num:"+strconv.FormatInt(key, 10), num)
	gredis.Expire("hour_send_task_num:"+strconv.FormatInt(key, 10), 3599)
}

//统计 10s second 生成任务个数
func SecondProductTaskNum() {
	timeSecondNow := util.GenSecondTime()
	key := timeSecondNow % 10

	gredis.IncrBy("second_product_task_num:"+strconv.FormatInt(key, 10), 1)
	gredis.Expire("second_product_task_num:"+strconv.FormatInt(key, 10), 9)
}

//统计 10s second 发任务个数
func secondSendTaskNum(num int) {
	timeSecondNow := util.GenSecondTime()
	key := timeSecondNow % 10

	gredis.IncrBy("second_send_task_num:"+strconv.FormatInt(key, 10), num)
	gredis.Expire("second_send_task_num:"+strconv.FormatInt(key, 10), 9)
}
