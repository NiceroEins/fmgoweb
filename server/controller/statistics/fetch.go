package statistics

import (
	"datacenter/models"
	"datacenter/models/statistics"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"strings"
	"time"
)

func FetchNewsNumByDate() func() {
	return func() {
		tm := time.Now().Add(-24 * time.Hour)
		types := []string{"news", "qa", "all"}
		showPolicy := []string{"show", "all"}
		for _, v := range types {
			for _, sp := range showPolicy {
				cnt := statistics.CountNewsBySql(tm, v, sp)
				gredis.HSet(statistics.GenerateNewsCountKey(tm), statistics.GenerateNewsCountField(v, sp), cnt)
			}
		}
	}
}

func Test() {
	types := []string{"news", "qa", "all"}
	showPolicy := []string{"show", "all"}
	for i := 1; i < 30; i++ {
		for _, v := range types {
			for _, sp := range showPolicy {
				tm := time.Now().Add(-24 * time.Duration(i) * time.Hour)
				cnt := statistics.CountNewsBySql(tm, v, sp)
				gredis.HSet(statistics.GenerateNewsCountKey(tm), statistics.GenerateNewsCountField(v, sp), cnt)
			}
		}
	}

}

func ResetSeed() {
	models.DB().Table("u_seed").Where("error_status = ?", "error").Update("error_status", "normal").Update("error_reason", "")
}

func CheckSeed() {
	var seeds []int
	models.DB().Table("u_seed").Where("deleted_at IS NULL").Where("status = ?", "normal").Where("catch_mode <> ?", "wechat").Pluck("id", &seeds)
	for _, v := range seeds {
		var reason []string
		if ruleWeek(v) {
			reason = append(reason, "近7日每日都有抓取数量，今日抓取数量为0")
		}
		if ruleImportantSeed(v) {
			reason = append(reason, "星级≥4，连续2日抓取数量为0")
		}
		if ruleDecr(v) {
			reason = append(reason, "今日抓取数量与上日相比下降50%以上")
		}
		if len(reason) > 0 {
			updateInvalidSeed(v, strings.Join(reason, ";"))
		}
	}
}

func ruleDecr(seedID int) bool {
	return checkToday(seedID) < (countSeed(seedID, yesterday()) / 2)
}

func yesterday() string {
	t := time.Now()
	for {
		t = t.Add(-24 * time.Hour)
		if t.Weekday() == time.Saturday || t.Weekday() == time.Sunday {
			continue
		}
		return t.Format("2006-01-02")
	}
}

func ruleImportantSeed(seedID int) bool {
	return star(seedID) >= 4 && checkRecent(seedID, 2)
}

func star(seedID int) int {
	var star []int
	models.DB().Table("u_seed").Where("id = ?", seedID).Pluck("star", &star)
	if len(star) > 0 {
		return star[0]
	} else {
		return 0
	}
}

func checkRecent(seedID, day int) bool {
	t := time.Now()
	for i := 1; i <= day; {
		t = t.Add(-24 * time.Hour)
		if t.Weekday() == time.Saturday || t.Weekday() == time.Sunday {
			continue
		}
		if countSeed(seedID, t.Format("2006-01-02")) > 0 {
			return false
		}
		i++
	}
	return true
}

// 近七日每日都有抓取数量，今日抓取数量为0
func ruleWeek(seedID int) bool {
	return checkWeek(seedID) && (checkToday(seedID) == 0)
}

func checkWeek(seedID int) bool {
	for i := 1; i <= 7; i++ {
		t := time.Now().Add(time.Duration(-i) * 24 * time.Hour)
		if t.Weekday() == time.Saturday || t.Weekday() == time.Sunday {
			continue
		}
		if countSeed(seedID, t.Format("2006-01-02")) == 0 {
			return false
		}
	}
	return true
}

func checkToday(seedID int) int {
	return countSeed(seedID, time.Now().Format("2006-01-02"))
}

func countSeed(seedID int, date string) int {
	var cnt int
	if err := models.DB().Table("u_feed").Where("seed_id = ?", seedID).Where("created_at BETWEEN ? AND ?", date, date+" 23:59:59").Count(&cnt).Error; err != nil {
		logging.Error("statistics.CheckSeedNum() Count num Errors: ", err.Error())
		return 0
	}
	return cnt
}

func updateInvalidSeed(seedID int, reason string) {
	models.DB().Table("u_seed").Where("id = ?", seedID).Update("error_status", "error").Update("error_reason", reason)
}
