package statistics

import (
	"datacenter/models/spider"
	"datacenter/models/statistics"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"
)

type StatData struct {
	NewsCount    NewsCount    `json:"news"`
	AuthorCount  AuthorCount  `json:"author"`
	ObjectCount  ObjectCount  `json:"object"`
	KeywordCount KeywordCount `json:"keyword"`
	ProcCount    ProcCount    `json:"proc"`
}

type NewsCount struct {
	Total []statistics.NewsCount `json:"total"`
	Show  []statistics.NewsCount `json:"show"`
}

type AuthorCount struct {
	Total []statistics.AuthorCount `json:"total"`
}

type ObjectCount struct {
	Total []statistics.ObjectCount `json:"total"`
	Q     []statistics.ObjectCount `json:"q"`
	QA    []statistics.ObjectCount `json:"qa"`
}

type KeywordCount struct {
	Total []statistics.KeywordCount `json:"total"`
}

type ProcCount struct {
	Total []statistics.TopUserStatisticsVO `json:"total"`
}

type StatReq struct {
	Begin time.Time `json:"begin"`
	End   time.Time `json:"end"`
	Field string    `json:"field"`
	Limit int       `json:"limit"`
}

func StatNews(req StatReq) *StatData {
	if pData := statsByCache(req); pData != nil {
		return pData
	}

	fields := strings.Split(req.Field, ",")
	var bAll bool = len(fields) == 0

	var wg sync.WaitGroup
	var ret StatData

	cates := []string{"news_all", "news_show", "author", "object_all", "object_q", "object_qa", "keyword_all", "proc"}
	for _, v := range cates {
		if bAll || util.ContainsAny(fields, v) {
			wg.Add(1)
			go func(cate string) {
				defer wg.Done()
				switch cate {
				case "news_all":
					ret.NewsCount.Total = statistics.CountNews(req.Begin, req.End, "news", "all")
				case "news_show":
					ret.NewsCount.Show = statistics.CountNews(req.Begin, req.End, "news", "show")
				case "author":
					ret.AuthorCount.Total = statistics.CountAuthor(req.Begin, req.End, req.Limit)
				case "object_all":
					ret.ObjectCount.Total = statistics.CountObject(req.Begin, req.End, "all", req.Limit)
				case "object_q":
					ret.ObjectCount.Q = statistics.CountObject(req.Begin, req.End, "q", req.Limit)
				case "object_qa":
					ret.ObjectCount.QA = statistics.CountObject(req.Begin, req.End, "qa", req.Limit)
				case "keyword_all":
					ret.KeywordCount.Total = CountKeyword(req.Begin, req.End, req.Limit)
				case "proc":
					ret.ProcCount.Total = CountProcNews(req.Begin, req.End, req.Limit)
				}
			}(v)
		}
	}
	wg.Wait()
	cacheStat(req, &ret)
	return &ret
	//if bAll || util.ContainsAny(fields, "news_all") {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		ret.NewsCount.Total = statistics.CountNews(req.Begin, req.End, "news", "all")
	//	}()
	//}
	//if bAll || util.ContainsAny(fields, "news_show") {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		ret.NewsCount.Show = statistics.CountNews(req.Begin, req.End, "news", "show")
	//	}()
	//}
	//if bAll || util.ContainsAny(fields, "author") {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		ret.AuthorCount.Total = statistics.CountAuthor(req.Begin, req.End, limit)
	//	}()
	//}
	//if bAll || util.ContainsAny(fields, "object_all") {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		ret.ObjectCount.Total = statistics.CountObject(req.Begin, req.End, "all", limit)
	//	}()
	//}
	//if bAll || util.ContainsAny(fields, "object_q") {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		ret.ObjectCount.Q = statistics.CountObject(req.Begin, req.End, "q", limit)
	//	}()
	//}
	//if bAll || util.ContainsAny(fields, "object_qa") {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		ret.ObjectCount.QA = statistics.CountObject(req.Begin, req.End, "qa", limit)
	//	}()
	//}
	//if bAll || util.ContainsAny(fields, "keyword_all") {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		ret.KeywordCount.Total = CountKeyword(req.Begin, req.End, limit)
	//	}()
	//}
	//if bAll || util.ContainsAny(fields, "proc") {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		ret.ProcCount.Total = CountProcNews(req.Begin, req.End, limit)
	//	}()
	//}
	//wg.Wait()
	//return ret
}

func CountKeyword(begin, end time.Time, limit int) []statistics.KeywordCount {
	policy := &spider.NewsPolicy{}
	keywords := spider.FetchKeyword("qa", 0)
	policy.Initialize()
	policy = policy.SetTrim("\n", "\r").SetSep(",").SetMode("or")
	policy.SetType("qa,q").Rule("all").SetKeyword(strings.Join(keywords, ",")).Do("")
	if raw, _, err := spider.FetchNews(policy, false, 0, 0, 1000000, "", begin.Format("2006-01-02"), end.Format("2006-01-02")+" 23:59:59"); err != nil {
		logging.Error("statistics.CountKeyword() Fetch news Errors: ", err.Error())
		return []statistics.KeywordCount{}
	} else {
		return statistics.CountKeyword(raw, keywords)[:util.Min(limit, len(keywords))]
	}
}

func CountProcNews(begin, end time.Time, limit int) []statistics.TopUserStatisticsVO {
	var vo []statistics.TopUserStatisticsVO
	dto, err := statistics.GetTopUserStatisticsList(begin, end, limit)
	if err != nil {
		logging.Error("news.CountProcNews() GetTopUserStatisticsList Errors: ", err.Error())
		return vo
	}
	for _, v := range dto {
		data, _ := v.DTO2VO().(statistics.TopUserStatisticsVO)
		vo = append(vo, data)
	}
	return vo
}

func statsByCache(req StatReq) *StatData {
	var ret StatData
	key := generateStatCacheKey(req)
	if data, err := gredis.Get(key); err == nil {
		err = json.Unmarshal(data, &ret)
		if err == nil {
			return &ret
		}
	}
	return nil
}

func cacheStat(req StatReq, data *StatData) {
	if data == nil {
		return
	}
	//bt, _ := json.Marshal(data)
	_ = gredis.Set(generateStatCacheKey(req), data, -1)
	if req.End.Format("20060102") == time.Now().Format("20060102") {
		//当日缓存5min
		//非当日永久缓存
		gredis.Expire(generateStatCacheKey(req), 60*5)
	}
}

func generateStatCacheKey(req StatReq) string {
	return fmt.Sprintf("cache:stat:%d", util.BKDRHash(req.Begin.Format("20060102")+"&"+req.End.Format("20060102")+"&"+req.Field+"&"+strconv.Itoa(req.Limit)))
}
