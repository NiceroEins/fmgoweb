package statistics

import (
	"datacenter/models"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"testing"
)

func TestStatistics(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	CheckSeed()
}
