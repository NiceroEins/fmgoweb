package statistics

import (
	"datacenter/models"
	"datacenter/models/spider"
	"datacenter/models/statistics"
	"datacenter/pkg/gredis"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"strconv"
	"strings"
	"time"
)

func StatStorage() {
	go dayStoreMysqlNum()
	go hourStoreMysqlNum()
	go secondStoreMysqlNum()
}

func StatReceive(num int) {
	go dayReceiveDataNum(num)
	go hourReceiveDataNum(num)
	go secondReceiveDataNum(num)
}

//统计 day接收到的数据条数
func dayReceiveDataNum(num int) {
	//todo day 收到爬虫任务个数
	timeNow := util.GenSecondTime()
	_, timeEnd := util.GenBeginEndTime()
	expireTime := timeEnd - timeNow

	gredis.IncrBy("day_receive_data_num", num)
	gredis.Expire("day_receive_data_num", expireTime)
}

//统计 day入库mysql数据条数
func dayStoreMysqlNum() {

	timeNow := util.GenSecondTime()
	_, timeEnd := util.GenBeginEndTime()
	expireTime := timeEnd - timeNow

	gredis.IncrBy("day_store_mysql_num", 1)
	gredis.Expire("day_store_mysql_num", expireTime)

}

//统计 hour接收到数据条数
func hourReceiveDataNum(num int) {
	timeNow := util.GenSecondTime()
	key := timeNow % 3600

	gredis.IncrBy("hour_receive_data_num:"+strconv.FormatInt(key, 10), num)
	gredis.Expire("hour_receive_data_num:"+strconv.FormatInt(key, 10), 3599)
}

//统计 hour入库mysql数据条数
func hourStoreMysqlNum() {
	timeNow := util.GenSecondTime()
	key := timeNow % 3600
	gredis.IncrBy("hour_store_mysql_num:"+strconv.FormatInt(key, 10), 1)
	gredis.Expire("hour_store_mysql_num:"+strconv.FormatInt(key, 10), 3599)
}

//统计 10s second接收到的数据条数
func secondReceiveDataNum(num int) {
	timeNow := util.GenSecondTime()
	key := timeNow % 10
	gredis.IncrBy("second_receive_data_num:"+strconv.FormatInt(key, 10), 1)
	gredis.Expire("second_receive_data_num:"+strconv.FormatInt(key, 10), 9)
}

//统计 10s second入库mysql数据条数
func secondStoreMysqlNum() {
	timeNow := util.GenSecondTime()
	key := timeNow % 10
	gredis.IncrBy("second_store_mysql_num:"+strconv.FormatInt(key, 10), 1)
	gredis.Expire("second_store_mysql_num:"+strconv.FormatInt(key, 10), 9)
}

func StoreLog(report *spider.Report) {
	//ReportLog.Data = append(ReportLog.Data, report)
	//ReportLog.Size++
}

type SeedStatisticsDTO struct {
	ID        int `json:"id"`
	CatchPath string
	Today     int64
	Yesterday int64
	Avg       int64
	UpdatedAt *time.Time
	Err       string
}
type SeedStatisticsVO struct {
	ID        int    `json:"id"`
	CatchPath string `json:"catch_path"`
	Today     int64  `json:"today"`
	Yesterday int64  `json:"yesterday"`
	Avg       int64  `json:"avg"`
	Err       bool   `json:"err"`
	UpdatedAt string `json:"updated_at"`
}
type SeedStatisticsQO struct {
	PageSize  int    `json:"page_size" form:"page_size" binding:"required,gte=1"`
	PageNum   int    `json:"page_num" form:"page_num" binding:"required,gte=1"`
	DateTime  string `json:"date_time" form:"date_time" binding:"required"`
	Err       *bool  `json:"err" form:"err"`
	CatchPath string `json:"catch_path" form:"catch_path"`
	SortParam int    `json:"sort_param" form:"sort_param" binding:"gte=0,lte=3"`
	Direction int    `json:"direction" form:"direction" binding:"gte=0,lte=1"`
}

type SingleSeedStatisticsQO struct {
	ID       int    `json:"id" form:"id" binding:"required,gt=0"`
	DateTime string `json:"date_time" form:"date_time" binding:"required"`
}

type SingeSeedStatisticsDTO struct {
	Value    *int
	Datetime string
}

type SingeSeedStatisticsVO struct {
	Value    *int   `json:"value"`
	Datetime string `json:"datetime"`
}

func getUpdateAt(t *time.Time) string {
	if t != nil {
		return t.Format("2006-01-02 15:04:05")
	} else {
		return ""
	}
}
func (dto SeedStatisticsDTO) DTO2VO() crud_service.VO {
	return &SeedStatisticsVO{
		ID:        dto.ID,
		CatchPath: dto.CatchPath,
		Today:     dto.Today,
		Yesterday: dto.Yesterday,
		Avg:       dto.Avg,
		Err:       dto.Err == "normal",
		UpdatedAt: getUpdateAt(dto.UpdatedAt),
	}
}

func (dto SingeSeedStatisticsDTO) DTO2VO() crud_service.VO {
	return &SingeSeedStatisticsVO{
		Value:    dto.Value,
		Datetime: dto.Datetime,
	}
}

func (form SeedStatisticsQO) GetList() ([]crud_service.DTO, int, error) {
	timeLayout := "2006-01-02 15:04:05"
	SortParam := []string{"updated_at", "avg", "yesterday", "today"}
	Direction := []string{"DESC", "ASC"}
	var days int64 = 7
	todayBegin, todayEnd := util.GenBeginEndTimeThree(form.DateTime)
	yesterdayBegin := time.Unix(todayBegin-86400, 0).Format(timeLayout)
	yeterdayEnd := time.Unix(todayEnd-86400, 0).Format(timeLayout)
	sevenDayBeforeBegin := time.Unix(todayBegin-86400*days, 0).Format(timeLayout)
	dto := make([]SeedStatisticsDTO, 0)
	filted := make([]SeedStatisticsDTO, 0)
	ret := make([]crud_service.DTO, 0)

	err := models.DB().Table("Y").Raw("SELECT u_seed.id,catch_path, "+
		"COUNT(u_feed.created_at BETWEEN ? AND ? OR NULL) today, "+
		"COUNT(u_feed.created_at BETWEEN ? AND ? OR NULL) yesterday, "+
		"ROUND(COUNT(u_feed.created_at BETWEEN ? AND ? OR NULL)/?) avg, "+
		"MAX(u_feed.created_at) updated_at,error_status err "+
		"FROM u_seed LEFT JOIN u_feed ON u_feed.seed_id = u_seed.id WHERE u_seed.deleted_at IS NULL "+
		" GROUP BY u_seed.id ",
		time.Unix(todayBegin, 0).Format(timeLayout),
		time.Unix(todayEnd, 0).Format(timeLayout),
		yesterdayBegin,
		yeterdayEnd,
		sevenDayBeforeBegin,
		time.Unix(todayBegin, 0).Format(timeLayout),
		days,
	).Order(SortParam[form.SortParam] + " " + Direction[form.Direction]).Find(&dto).Error
	for _, o := range dto {
		errFilter, pathFilter := true, true
		if form.Err != nil {
			errFilter = *form.Err == (o.Err == "normal")
		}
		if form.CatchPath != "" {
			if !strings.Contains(o.CatchPath, form.CatchPath) {
				pathFilter = false
			}
		}
		if errFilter && pathFilter {
			filted = append(filted, o)
		}

	}
	cnt := len(filted)
	for j, v := range filted {
		if j >= (form.PageNum-1)*form.PageSize && j < form.PageSize*form.PageNum {
			ret = append(ret, v)
		}
	}
	return ret, cnt, err
}

func (qo SingleSeedStatisticsQO) GetOne() ([]crud_service.DTO, error) {
	timeLayout := "2006-01-02 15:04:05"
	todayBegin, todayEnd := util.GenBeginEndTimeThree(qo.DateTime)
	begin := time.Unix(todayBegin-7*86400, 0).Format(timeLayout)
	end := time.Unix(todayEnd, 0).Format(timeLayout)
	dto := make([]SingeSeedStatisticsDTO, 0)
	ret := make([]crud_service.DTO, 0)
	err := models.DB().Raw("SELECT  COUNT(1) value,DATE_FORMAT(updated_at,'%m-%d') datetime FROM `u_feed` "+
		"WHERE created_at BETWEEN ? AND ? AND seed_id=? "+
		"GROUP BY datetime", begin, end, qo.ID).Find(&dto).Error
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		ret = append(ret, v)
	}
	return ret, nil

}

type UserStatisticsQO struct {
	DateTime string `json:"date_time" form:"date_time"`
	Name     string `json:"name" form:"name"`
	PageNum  int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
	PageSize int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
}

func GetUserStatistics(qo UserStatisticsQO) ([]crud_service.VO, int, error) {
	ret := make([]crud_service.VO, 0)
	dto, cnt, err := statistics.GetUserStatisticsList(qo.DateTime, qo.Name, qo.PageNum, qo.PageSize)
	if err != nil {
		return nil, 0, err
	}
	for _, v := range dto {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	return ret, cnt, err
}

type PersonStatisticsQO struct {
	ID       int    `json:"id" form:"id" binding:"required"`
	DateTime string `json:"date_time" form:"date_time"`
	PageNum  int    `json:"page_num" form:"page_num" binding:"required,gt=0"`
	PageSize int    `json:"page_size" form:"page_size" binding:"required,gt=0"`
}

func GetPersonStatistics(qo PersonStatisticsQO) ([]crud_service.VO, int, error) {
	ret := make([]crud_service.VO, 0)
	dto, cnt, err := statistics.GetPersonStatisticsList(qo.ID, qo.PageNum, qo.PageSize, qo.DateTime)
	if err != nil {
		return nil, 0, err
	}
	for _, v := range dto {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	return ret, cnt, err
}

func GetPrivilegeStatisticsList(qo statistics.PrivilegeStatisticListQO) ([]crud_service.VO, error) {
	ret := make([]crud_service.VO, 0)
	dto, err := statistics.GetPrivilegeStatisticsList(qo)
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	return ret, err
}

func GetPrivilegeStatisticsUserList(qo statistics.PrivilegeStatisticUserListQO) ([]crud_service.VO, error) {
	ret := make([]crud_service.VO, 0)
	dto, err := statistics.GetPrivilegeStatisticsUserList(qo)
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	return ret, err
}

func GetUserPrivilegeStatisticsTotal(qo statistics.UserPrivilegeStatisticTotalQO) ([]crud_service.VO, error) {
	ret := make([]crud_service.VO, 0)
	dto, err := statistics.GetUserPrivilegeStatisticsTotal(qo)
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	return ret, err
}

func GetUserPrivilegeStatisticsDetail(qo statistics.UserPrivilegeStatisticQO) ([]crud_service.VO, error) {
	ret := make([]crud_service.VO, 0)
	dto, err := statistics.GetUserPrivilegeStatisticsDetail(qo)
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	return ret, err
}

func GetUserPrivilegeStatisticsMap(qo statistics.UserPrivilegeStatisticMapQO) ([]crud_service.VO, error) {
	ret := make([]crud_service.VO, 0)
	dto, err := statistics.GetUserPrivilegeStatisticsMap(qo)
	if err != nil {
		return nil, err
	}
	for _, v := range dto {
		vo := v.DTO2VO()
		ret = append(ret, vo)
	}
	return ret, err
}
