package async

import (
	"datacenter/models"
	"datacenter/models/nlp"
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

func ProcDuplication(news spider.News) {
	if news.ID == 0 || len(news.Title) == 0 {
		return
	}
	var ret string
	var dup string
	var duration int64 = 0
	if news.ParentID > 0 {
		// 已去重
		return
	} else {
		t1 := time.Now()
		duplicatedID, duplication, err := Duplication(&news)
		if err != nil {
			logging.Debug("async.ProcDuplication() call Duplication Errors: ", err.Error())
			return
		}
		if duplicatedID != "" {
			// 有关
			ret = duplicatedID
			dup = duplication
		} else {
			dup = duplication
		}
		duration = time.Since(t1).Milliseconds()
	}

	if err := nlp.UpdateDuplication(news.ID, ret, dup, duration); err != nil {
		logging.Error("async.ProcDuplication() update duplication Errors: ", err.Error())
	}
}

//func Duplication(id int, text string) (int, float64, error) {
//	if len(text) <= 0 {
//		return 0, 0, fmt.Errorf("empty text")
//	}
//
//	client := &http.Client{
//		Timeout: time.Duration(5 * time.Second),
//	}
//
//	urlString := fmt.Sprintf("%s/vector?text=%s", setting.NLPSetting.DuplicationHost, url.QueryEscape(text))
//	req, err := http.NewRequest("GET", urlString, nil)
//	if err != nil {
//		logging.Debug(err)
//		return 0, 0, err
//	}
//	req.Header.Add("Accept", "application/json")
//	resp, err := client.Do(req)
//	if err != nil {
//		logging.Error(err)
//		return 0, 0, err
//	}
//	bytes, err := ioutil.ReadAll(resp.Body)
//	if err != nil {
//		logging.Error(err)
//		return 0, 0, err
//	}
//	defer func() {
//		_ = resp.Body.Close()
//	}()
//
//	dup, err := nlp.ParseDuplication(string(bytes))
//	if err != nil {
//		logging.Error("async.Duplication() parse Errors: ", err.Error())
//		return 0, 0, err
//	}
//	if len(dup.Data) == 0 {
//		err = fmt.Errorf("async.Duplication() empty dup.Data")
//		logging.Error(err.Error())
//		return 0, 0, err
//	}
//
//	defer nlp.SetDuplication(id, dup.Data[0].Vector)
//
//	return nlp.FindDuplicationID(id, dup.Data[0].Vector)
//}

func Duplication(news *spider.News) (string, string, error) {
	text := news.Title
	if len(text) <= 0 {
		return "", "", fmt.Errorf("empty text")
	}

	client := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	if !spider.Contains(text, spider.NLPKeyword()) && showInHome(news.SeedID) {
		return "", "", nil
	}

	urlString := fmt.Sprintf("%s/predict?text=%s", setting.NLPSetting.DuplicationHost, url.QueryEscape(text))
	req, err := http.NewRequest("GET", urlString, nil)
	if err != nil {
		logging.Debug(err)
		return "", "", err
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		logging.Error(err)
		return "", "", err
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Error(err)
		return "", "", err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	dup, err := nlp.ParseDuplication(string(bytes))
	if err != nil {
		logging.Error("async.Duplication() parse Errors: ", err.Error())
		logging.Debug("async.Duplication() reports: ", string(bytes))
		return "", "", err
	}
	if len(dup.Data) == 0 {
		err = fmt.Errorf("async.Duplication() empty dup.Data")
		logging.Error(err.Error())
		return "", "", err
	}

	if dup.Data[0].Pred == 1 {
		suffix := "0"
		if dup.Data[0].DuplicateInHalfHours {
			suffix = "1"
		}
		return dup.Data[0].DuplicatedID + "_" + suffix, dup.Data[0].TextNlpID, nil
	} else {
		return "", dup.Data[0].TextNlpID, nil
	}
}

func showInHome(id int64) bool {
	var n int
	models.DB().Table("u_seed").Where("id = ?", id).Where("show_in_home = 1").Count(&n)
	return n > 0
}
