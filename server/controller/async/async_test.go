package async

import (
	"datacenter/models"
	"datacenter/models/spider"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func TestCorrelation(t *testing.T) {
	setting.Setup()
	models.Setup()

	ProcNamedEntity(spider.News{
		Simple: models.Simple{
			ID: 0,
		},
		SeedID: 322,
		Title:  "中芯国际A股上市翌日吐逾6%",
	})
}

func TestDuplication(t *testing.T) {
	setting.Setup()
	models.Setup()
	gredis.Setup()

	ProcDuplication(spider.News{
		Simple: models.Simple{
			ID: 1443999,
		},
		SeedID: 322,
		Title:  "丰田拟在2025年前量产固态电池,使用30年可保持90%性能",
	})
}

func TestNLPGetKeywords(t *testing.T) {
	setting.Setup()
	models.Setup()
	keywords, err := NLPGetKeywords("随着进入“无屏幕，不显示”时代，光学膜市场空间巨大。公司目前是国内唯一一家实现复合膜片月产量超百万片的企业。公司主攻高端复合膜产品，与三星及国内龙头深化合作市场份额逐步提升。光学膜片下游客户包括：三星、京东方、华为、小米、冠捷、TCL、海信、长虹、创维、康佳等。光学基材已为LG旗下企业进行供货，光学基材对国内市场销售也稳步增长，公司光学膜片市场份额不断提高")
	if err != nil {
		fmt.Println(err.Error())
	}
	keywordJson, err := json.Marshal(keywords)
	if err != nil {
		logging.Info("keyword json序列化失败", 618, err.Error())
		return
	}
	err = models.DB().Table("p_investigation").Where("id=?", 618).Update("keywords", string(keywordJson)).Error
	fmt.Println(keywords)
}

func TestGetChangeAndFallback(t *testing.T) {
	setting.Setup()
	models.Setup()
	publishTime:=time.Date(2020,11,24,0,0,0,0,time.Local)
	dto:=spider.InvestigationDTO{
		Id: 620,
		Code: "300625",
		PublishTime: &publishTime,
	}
	GetChangeAndFallback(&dto)
}
