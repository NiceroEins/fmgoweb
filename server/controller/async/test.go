package async

import (
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

func ProcTest(news *spider.News) (string, error) {
	if len(news.Title) <= 0 {
		return "", fmt.Errorf("empty text")
	}

	client := &http.Client{
		Timeout: time.Duration(10 * time.Second),
	}

	urlString := fmt.Sprintf("%s/predict", setting.NLPSetting.TestHost)
	req, err := http.NewRequest("POST", urlString, strings.NewReader("text="+url.QueryEscape(news.Title)))
	if err != nil {
		logging.Debug(err)
		return "", err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		logging.Error(err)
		return "", err
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Error(err)
		return "", err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	ret := string(bytes)
	//logging.Info("nlp.ProcTest() returns: ", ret)
	return ret, nil
}
