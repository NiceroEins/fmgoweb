package async

import (
	"datacenter/models/nlp"
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

func ProcSentiment(news spider.News) {
	var duration int64 = 0
	t1 := time.Now()
	sentiment, err := Sentiment(news.Title)
	if err != nil {
		logging.Debug("async.ProcSentiment() call Sentiment() Errors: ", err.Error())
		return
	}
	duration = time.Since(t1).Milliseconds()

	if err := nlp.UpdateSentiment(news.ID, sentiment, duration); err != nil {
		logging.Error("async.ProcSentiment() update sentiment Errors: ", err.Error())
	}
}

func Sentiment(text string) (string, error) {
	if len(text) <= 0 {
		return "", fmt.Errorf("empty text")
	}

	client := &http.Client{
		Timeout: time.Duration(10 * time.Second),
	}

	urlString := fmt.Sprintf("%s/predict?text=%s", setting.NLPSetting.SentimentHost, url.QueryEscape(text))
	req, err := http.NewRequest("GET", urlString, nil)
	if err != nil {
		logging.Debug(err)
		return "", err
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		logging.Error(err)
		return "", err
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Error(err)
		return "", err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	ne, err := nlp.ParseSentiment(string(bytes))
	if err != nil {
		logging.Error("async.Sentiment() parse Errors: ", err.Error())
		return "", err
	}
	return nlp.CheckSentiment(ne), nil
}
