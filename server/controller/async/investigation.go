package async

import (
	"context"
	"datacenter/models"
	"datacenter/models/nlp"
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func SetKeyWords(dto *spider.InvestigationDTO) {
	go NLPSetKeywords(dto)
}

func NLPSetKeywords(dto *spider.InvestigationDTO) {
	keywords, err := NLPGetKeywords(dto.Content)
	if err != nil {
		logging.Info("nlp 获取调研关键词失败", dto.Id, err.Error())
		return
	}
	keywordJson, err := json.Marshal(keywords)
	if err != nil {
		logging.Info("keyword json序列化失败", dto.Id, err.Error())
		return
	}
	err = models.DB().Table("p_investigation").Where("id=?", dto.Id).Update("keywords", string(keywordJson)).Error
	if err != nil {
		logging.Error("持久化调研关键词失败", dto.Id, keywordJson, err.Error())
		return
	}

}

func NLPGetKeywords(text string) ([]string, error) {
	if len(text) <= 0 {
		return nil, fmt.Errorf("empty text")
	}

	client := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	urlString := fmt.Sprintf("%s/predict", setting.NLPSetting.KeywordsHost)
	req, err := http.NewRequest("POST", urlString, strings.NewReader("text="+text))
	if err != nil {
		logging.Debug(err)
		return nil, err
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	resp, err := client.Do(req.WithContext(context.TODO()))
	if err != nil {
		logging.Error(err)
		return nil, err
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Error(err)
		return nil, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	respStruct, err := nlp.ParseKeywords(string(bytes))
	if err != nil {
		return nil, err
	}
	if len(respStruct.Data) == 0 {
		err = errors.New("async.NLPGetKeywords() empty respStruct.Data")
		return nil, err
	}

	return respStruct.Data[0].ModelNer, nil

}

func GetChangeAndFallback(dto *spider.InvestigationDTO) {
	/*type area struct {
		Start string `json:"start"`
		End string `json:"end"`
	}*/
	/*type stockDTO struct {
		Code string `json:"code"`
		Time *time.Time `json:"time"`
		Close decimal.NullDecimal `json:"close"`
		High decimal.NullDecimal `json:"high"`
		Low decimal.NullDecimal `json:"low"`
		Pctchange decimal.NullDecimal `json:"pctchaange"`

	}*/
	if dto != nil {
		//dtojosn, _ := json.Marshal(*dto)
		logging.Info("开启回测", dto.Id)
	} else {
		logging.Error("空dto回测")
		return
	}
	type stockDTO struct {
		StockCode string              `json:"stock_code"`
		TradeDate *time.Time          `json:"trade_date"`
		Open      decimal.NullDecimal `json:"open"`
		Close     decimal.NullDecimal `json:"close"`
		High      decimal.NullDecimal `json:"high"`
		Low       decimal.NullDecimal `json:"low"`
		PctChg    decimal.NullDecimal `json:"pct_chg"`
	}

	//areaDto :=area{}
	max := decimal.Zero
	finalMax := decimal.Zero
	stockDtos := make([]stockDTO, 0)
	subSql := fmt.Sprintf("select * from p_stock_tick where stock_code='%s'  and  trade_date<'%s' and vol>0 order by trade_date desc limit 10", dto.Code, dto.PublishTime.Format(util.YMD))
	//sql:="select MAX(trade_date) end,MIN(trade_date) start FROM ("+subSql+") A"
	//err:=models.DB().Raw(sql).Find(&areaDto).Error
	err := models.DB().Raw(subSql).Find(&stockDtos).Error
	if err != nil {
		logging.Error("GetChangeAndFallback() 获取时间区间报错：", err.Error())
	}
	/*err = models.Data().Table("p_stock_minute").
		Where("code=?",dto.Code).
		Where("time >= ? and time <=?", areaDto.Start, areaDto.End).
		Find(&stockMinuteDtos).Error
	if err != nil {
		logging.Error("GetChangeAndFallback() 获取分钟数据报错：",err.Error())
	}*/
	for k, h := range stockDtos { //dtos 为逆序
		for i := k + 1; i < len(stockDtos); i++ {
			max = stockDtos[i].Close.Decimal.Sub(h.Close.Decimal).Div(stockDtos[i].Close.Decimal)
			if finalMax.LessThan(max) {
				finalMax = max
			}
		}
		/*for _,l:=range stockDtos[k+1:]{

		}*/
	}
	incSum := stockDtos[0].Close.Decimal.Sub(stockDtos[len(stockDtos)-1].Open.Decimal).Mul(decimal.NewFromInt32(100)).Div(stockDtos[len(stockDtos)-1].Open.Decimal)
	err = models.DB().Table("p_investigation").Where("id=?", dto.Id).Updates(map[string]interface{}{
		"inc_sum":      incSum.Round(2).String(),
		"max_fallback": finalMax.Mul(decimal.NewFromInt32(100)).Round(2).String(),
	}).Error
	if err != nil {
		logging.Error("GetChangeAndFallback() 持久化报错：", err.Error())
	}
}
