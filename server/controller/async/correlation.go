package async

import (
	"context"
	"datacenter/models/nlp"
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

func ProcCorrelation(news spider.News) (int, int64) {
	ret := 0
	var duration int64 = 0
	if spider.Contains(news.Title, spider.NLPKeyword()) || seedIn(news.SeedID) {
		ret = 2
	} else {
		t1 := time.Now()
		bRelated, err := Correlation(news.Title)
		if err != nil {
			logging.Error("async.ProcCorrelation() call Correlation Errors: ", err.Error())
			return ret, duration
		}
		if bRelated {
			// 有关
			ret = 1
		}
		duration = time.Since(t1).Milliseconds()
	}

	return ret, duration
}

func Correlation(text string) (bool, error) {
	if len(text) <= 0 {
		return false, fmt.Errorf("empty text")
	}

	client := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}

	urlString := fmt.Sprintf("%s/predict", setting.NLPSetting.CorrelationHost)
	req, err := http.NewRequest("POST", urlString, strings.NewReader("text="+url.QueryEscape(text)))
	if err != nil {
		logging.Debug(err)
		return false, err
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	resp, err := client.Do(req.WithContext(context.TODO()))
	if err != nil {
		logging.Error(err)
		return false, err
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Error(err)
		return false, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	cor, err := nlp.ParseCorrelation(string(bytes))
	if err != nil {
		logging.Error("async.Correlation() parse Errors: ", err.Error())
		return false, err
	}
	return nlp.CheckCorrelation(cor), nil
}

func ProcQA(news spider.News) (int, int64) {
	ret := 0
	var duration int64 = 0
	if spider.Contains(news.Title, spider.NLPKeyword()) || seedIn(news.SeedID) {
		ret = 2
	} else {
		t1 := time.Now()
		bRelated, err := QACorrelation(&news)
		if err != nil {
			logging.Error("async.ProcCorrelation() call Correlation Errors: ", err.Error())
			return ret, duration
		}
		if bRelated {
			// 有关
			ret = 1
		}
		duration = time.Since(t1).Milliseconds()
	}

	return ret, duration
}

func QACorrelation(news *spider.News) (bool, error) {
	if len(news.Title) <= 0 {
		return false, fmt.Errorf("empty text")
	}

	client := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	urlString := fmt.Sprintf("%s/predict", setting.NLPSetting.QAHost)
	//req, err := http.NewRequest("GET", urlString, nil)
	req, err := http.NewRequest("POST", urlString, strings.NewReader(fmt.Sprintf("q=%s&a=%s", url.QueryEscape(news.Title), url.QueryEscape(news.Content))))
	if err != nil {
		logging.Error(err)
		return false, err
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	resp, err := client.Do(req.WithContext(context.TODO()))
	if err != nil {
		logging.Error(err)
		return false, err
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Error(err)
		return false, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	cor, err := nlp.ParseQACorrelation(bytes)
	if err != nil {
		logging.Error("async.Correlation() parse Errors: ", err.Error())
		return false, err
	}
	return nlp.CheckQACorrelation(cor), nil
}

func seedIn(seedID int64) bool {
	if seedID == 80 || seedID == 3363 || seedID == 3364 || seedID == 4411 ||
		seedID == 81 || seedID == 1818 || seedID == 1852 {
		return true
	}
	return false
}
