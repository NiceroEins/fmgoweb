package async

import (
	"datacenter/models/nlp"
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"sync"
	"time"
)

func After(news *spider.News) {
	//go ProcCorrelation(*news)
	//go ProcDuplication(*news)
	if news.Type != "wechat-detail" || exclude(news.ID) {
		Serialize(ProcDuplication, *news)
	}
	go ProcNamedEntity(*news)
	go ProcSentiment(*news)
	go ProcTest(news)
}

func Before(news *spider.News) {
	timer := time.NewTimer(1 * time.Second)
	ch := make(chan int, 1)
	var ret int
	var duration int64
	if news.Type == "twitter" || news.Type == "wechat-detail" || exclude(news.ID) {
		return
	}
	go func() {
		switch news.Type {
		case "qa", "q":
			ret, duration = ProcQA(*news)
			ch <- 1
		default:
			ret, duration = ProcCorrelation(*news)
			ch <- 1
		}

	}()
	select {
	case <-ch:
		news.Duration = &duration
		news.Correlation = &ret
		return
	case <-timer.C:
		go func(c chan int, r *int, d *int64) {
			<-c
			ticker := time.NewTicker(1 * time.Second)
			defer ticker.Stop()
			t := 0
			for {
				if news.ID > 0 {
					if err := nlp.UpdateCorrelation(news.ID, *r, *d); err != nil {
						logging.Error("async.Before() update correlation Errors: ", err.Error())
					}
					return
				}
				if t >= 5 {
					return
				}
				t++
				<-ticker.C
			}
		}(ch, &ret, &duration)

	}
}

type Serialization struct {
	f    func(news spider.News)
	news spider.News
}

var once sync.Once
var ch chan Serialization

func Serialize(f func(news spider.News), news spider.News) {
	once.Do(func() {
		ch = make(chan Serialization)
		go procSerialize()
	})
	ch <- Serialization{
		f:    f,
		news: news,
	}
}

func procSerialize() {
	for {
		s := <-ch
		if s.f != nil {
			s.f(s.news)
		}
	}
}

func exclude(id int) bool {
	if id == 5079 {
		return true
	}
	return false
}
