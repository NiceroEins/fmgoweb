package async

import (
	"datacenter/models/nlp"
	"datacenter/models/spider"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

func ProcNamedEntity(news spider.News) {
	var duration int64 = 0
	t1 := time.Now()
	keyword, err := NamedEntity(news.Title)
	if err != nil {
		logging.Debug("async.ProcNamedEntity() call NamedEntity Errors: ", err.Error())
		return
	}
	duration = time.Since(t1).Milliseconds()

	if err := nlp.UpdateNamedEntity(news.ID, keyword, duration); err != nil {
		logging.Error("async.ProcNamedEntity() update named_entity Errors: ", err.Error())
	}
}

func NamedEntity(text string) (string, error) {
	if len(text) <= 0 {
		return "", fmt.Errorf("empty text")
	}

	client := &http.Client{
		Timeout: time.Duration(10 * time.Second),
	}

	urlString := fmt.Sprintf("%s/predict?text=%s", setting.NLPSetting.NamedEntityHost, url.QueryEscape(text))
	req, err := http.NewRequest("GET", urlString, nil)
	if err != nil {
		logging.Debug(err)
		return "", err
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		logging.Error(err)
		return "", err
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Error(err)
		return "", err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	ne, err := nlp.ParseNamedEntity(string(bytes))
	if err != nil {
		logging.Error("async.NamedEntityHost() parse Errors: ", err.Error())
		return "", err
	}
	return nlp.CheckNamedEntity(ne), nil
}
