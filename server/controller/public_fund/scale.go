package public_fund

import (
	"datacenter/models/public_fund"
)

func GetScaleList() (public_fund.ScaleResp,error) {
	return public_fund.GetScaleList()
}

func GetStockList(req *public_fund.FundHistoryReq) (public_fund.FundStockRsp,int,error) {
	return public_fund.GetFundStock(req)
}

func GetPositionList(req *public_fund.FundPositionReq) ([]public_fund.FundPositionResp,int,error) {
	return public_fund.GetStockPosition(req)
}

func GetManagerData(req *public_fund.FundManagerReq) ([]public_fund.FundManagerResp,int,error) {
	return public_fund.GetStockManager(req)
}

func GetManagerHeadData(req *public_fund.FundManagerReq) (public_fund.FundManagerHeadResp,error) {
	return public_fund.GetManagerStockScale(req)
}

func GetManagerNameSreach(req *public_fund.FundManagerSearchReq) ([]public_fund.ManagerList,error){
	return public_fund.GetManagerRank(req)
}

func GetStarManagerList() ([]public_fund.ManagerList,error){
	return public_fund.GetStarManagerList()
}
