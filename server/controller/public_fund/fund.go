package public_fund

import (
    "datacenter/models/public_fund"
)

func GetFundNameSreach(req *public_fund.FundNameSearchReq) ([]public_fund.FundNameListResp,error){
    return public_fund.GetFundNameList(req)
}

func GetFundList(req *public_fund.FundReq) ([]public_fund.FundList,int,error) {
    return public_fund.GetFundList(req)
}

func GetFundScale(req *public_fund.FundReq) (public_fund.FundScaleResp,error) {
   return public_fund.GetFundScaleList(req)
}

func GetStockFundDetail(req *public_fund.FundStockDetailReq) (public_fund.FundStockDetailResp,int,error)  {
    return public_fund.GetFundStockDetail(req)
}

func GetReportPeriod() (public_fund.ReportPeriod,error) {
    return public_fund.GetReportPeriodList()
}

func GetFundIndustry(req *public_fund.FundIndustryReq) ([]public_fund.FundIndustryResp,error)  {
    return public_fund.GetIndustryListByFund(req)
}

func GetFundRank(req *public_fund.FundRankReq) ([]public_fund.FundRankResp,int,error) {
    return public_fund.GetFundRankList(req)
}
func GetFundNew(req *public_fund.FundNewReq) ([]public_fund.FundNewListResp,error)  {
    return public_fund.GetFundNewList(req)
}
func GetFundNewCount(req *public_fund.FundNewReq) (int, error) {
    return public_fund.GetFundNewListCount(req)
}