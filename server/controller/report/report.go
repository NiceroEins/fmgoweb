package report

import (
	"datacenter/controller/statistics"
	"datacenter/controller/storage"
	"datacenter/controller/task"
	"datacenter/models/spider"
	"datacenter/pkg/curl"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

func HandleReportFailed(task_id string, seed_id string, report *spider.Report) {
	//添加到redis
	num := gredis.IncrBy("HandleReportFailed:"+task_id+"_"+seed_id, 1)
	if num >= 3 && num < 5 {
		//投入JobChan 对应类型的failed chan中
		seed, err := gredis.Get("SeedStatic:" + seed_id)
		if err != nil {
			logging.Error("从SeedStatic中获取seed失败:" + err.Error())
		}

		seedStruct := spider.Seed{}
		err = json.Unmarshal(seed, &seedStruct)
		if err != nil {
			logging.Error("seed json解析到结构体失败:" + err.Error())
		}
		//入到对应类型失败的队列,eg: normal_failed
		task.TryEnqueue(&seedStruct, seedStruct.Mode+"_failed")
		return
	}
	if num >= 5 {
		//触发钉钉报警，之后redis中删掉
		msg := "task_id:" + task_id + ",seed_id=" + seed_id + "失败已到达5次" //换成自定义消息
		jsonPostStr := `{
   			 			"msgtype": "text", 
    					"text": {
	                    "content":` + msg + `}, 
   				        "at": {
     			           "atMobiles": [
         			        "156xxxx8827", 
           				     "189xxxx8325"
       					   ], 
      			        "isAtAll": false
    			       }
					}`
		postHandle := curl.NewPost("https://oapi.dingtalk.com/robot/send?access_token=73b92551c6d13f8c2e1273392cd890104f3a71d27d7f573ee3ab717e1672e7da", jsonPostStr)
		_, err := postHandle.Send()
		if err != nil {
			fmt.Println("钉钉机器人消息发送失败:" + err.Error())
		}
		//从报警里面移除
		gredis.Delete("HandleReportFailed:" + task_id + "_" + seed_id)
		return
	}
}

// 处理爬虫回调：上报信息
func ProcReport(report *spider.Report) (int, error) {
	if report == nil || report.Type == nil {
		return 0, nil
	}
	//实时监控统计(redis)
	num := len(report.Content)
	statistics.StatReceive(num)

	//转数组
	//ids := strings.Split(report.ID, "-")
	//id, _ := strconv.Atoi(ids[len(ids)-1])
	//任务id 是数组的第一个值
	//taskID := ids[0]
	//seedId := ids[1]

	taskIp, _ := gredis.HGetString("TaskMap", report.ID)
	//if err != nil {
	//	logging.Error("report.go ProcReport():从TaskMap中获取数据失败:", err.Error())
	//}

	//根据传入类型(type),来做不同处理
	//周五新增 case 200,300,400 三个分支里面都新增一个方法,
	//redis（新建一个目录）: task_id-seed_id:1,都+1，当+到3的时候，
	//往对应类型的(eg:normal_failed chan)中加入,(GetTask方法中)之后在从Jobchan中建立出队优先级,先从failed chan中获取
	//+到5的时候报警,并从redis中清除
	var err error
	var id int
	switch *report.Type {
	case 102:
		//102(请求成功但数据为空)
		logging.Info(fmt.Sprintf("%s empty data", report.ID))
		//ProcInvalidSeed(id, report)
		//写日志
		//statistics.StoreLog(report)
		ProcFailure(report)
	case 200:
		logging.Info(fmt.Sprintf("%s time out", report.ID))
		ProcTimeOut(taskIp)
		ProcFailure(report)
		//statistics.StoreLog(report)
	case 300:
		//服务端报错如404,500等
		//这个同1 一样的处理
		logging.Info(fmt.Sprintf("%s server errors: %s", report.ID, report.Msg))
		//ProcInvalidSeed(id, report)
		//statistics.StoreLog(report)
		ProcFailure(report)
		//HandleReportFailed(taskID, seedId, report)
	case 400:
		//其他类型错误,只记录日志
		logging.Info(fmt.Sprintf("%s errors: %s", report.ID, report.Msg))
		//statistics.StoreLog(report)
		ProcFailure(report)
		//HandleReportFailed(taskID, seedId, report)
	default:
		// 0623 restructure
		//爬虫处理成功,不同类型做不同的处理
		_, id, err = storage.Store(report)
		ProcOK(report)
	}

	// 0617 add: delete from TaskMap
	_ = gredis.HDelField("TaskMap", report.ID)
	return id, err
}

// ok
func ProcOK(report *spider.Report) {
	if report == nil {
		return
	}

	//_ = gredis.HDelField("seed:list", report.ID)
	_ = gredis.HDelField("seed:retries", report.ID)
}

//超时
func ProcTimeOut(task_ip string) bool {
	if task_ip != "" {
		var confidence int
		//代理地址不为空
		ip := task_ip

		//从ReportProxyMap中检查是否有该ip
		res, err := gredis.HGet("ReportProxyMap", "ip")
		if err != nil {
			fmt.Println("ProcTimeOut ReportProxyMap获取数据失败:", err.Error())
		}

		if res == nil {
			//没有 confidence=0
			confidence = 0
		}

		//有 confidence +1
		confidence = confidence + 1
		if confidence >= 3 {
			//如果confidence >=3，则从ProxyMap中删除,ReportProxyMap中删除
			//gredis.HDel("ProxyMap", ip)
			gredis.HDel("ReportProxyMap", ip)
		} else {
			//<3,ReportProxyMap中存储
			gredis.HSet("ReportProxyMap", ip, confidence)
		}
	}
	return true
}

//无效seed
func ProcInvalidSeed(seedID int, report *spider.Report) bool {

	//种子map,查看SeedMap中是否有该seedID的值
	seedIdStr := strconv.Itoa(seedID)

	ret := gredis.HExists("SeedMap", seedIdStr)
	var confidence int

	if ret {
		//如果有
		res, err := gredis.HGet("ReportSeedMap", seedIdStr)
		if err != nil {
			fmt.Println("ReportSeedMap 通过seedID获取失败:", err.Error())
		}

		if res == "" {
			confidence = 0
		}

		//+1
		//可信度 0最好
		confidence = confidence + 1

		if confidence >= 6 {
			//confidence >=6,则从SeedMap，ReportSeedMap中移除,UnusedSeedMap中加入
			//RemoveSeed(seedID, report)暂时不执行 todo
		} else {
			//confidence <6,则ReportSeedMap中存入
			gredis.HSet("ReportSeedMap", seedIdStr, confidence)
		}
	}
	return ret
}

// 处理失败
func ProcFailure(report *spider.Report) {
	if report == nil {
		return
	}
	//v := report.Content[0]
	//switch v.Type {
	//case "wb-detail", "yb-dc-detail", "yb-lb-detail", "yb-hb-detail", "research-pdf", "ipo-tyc-detail":
	data, _ := gredis.HGetString("seed:list:"+time.Now().Format("2006010215"), report.ID)
	if data != "" {
		var seed spider.Seed
		if err := json.Unmarshal([]byte(data), &seed); err == nil {
			bRetry := false
			bCopy := false
			switch seed.Mode {
			case "normal", "special":
				t := time.Now()
				if t.Hour() < 15 && t.Hour() >= 9 && t.Weekday() != time.Saturday && t.Weekday() != time.Sunday {
					bCopy = true
				}
			case "wb-detail":
				bRetry = true
			case "yb-dc-detail", "yb-lb-detail", "yb-hb-detail":
				bRetry = true
			case "research-pdf":
				bRetry = true
			//case "ipo-tyc-list":
			//	bRetry = true
			case "ipo-tyc-detail":
				bRetry = true
			case "wechat-detail":
				bRetry = true
			case "strong":
				t := time.Now()
				if t.Hour() < 15 && t.Hour() >= 9 && t.Weekday() != time.Saturday && t.Weekday() != time.Sunday {
					bCopy = true
				}
			}
			if bRetry {
				seed.Retries++
				go func(s *spider.Seed) {
					time.Sleep(1 * time.Second)
					task.TryEnqueue(s, s.Mode)
				}(&seed)
			}
			if bCopy {
				seed.Retries++
				go func(s *spider.Seed) {
					time.Sleep(1 * time.Second)
					task.TryEnqueue(s, s.Mode+"-copy")
				}(&seed)
			}
		} else {
			logging.Error("report.ProcFailure() Unmarshal data Errors: ", err.Error())
		}
	}
}
