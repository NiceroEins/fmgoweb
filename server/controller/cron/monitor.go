package cron

import (
	"datacenter/models"
	"datacenter/models/home"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"sort"
	"strings"
	"time"
)

type Stock struct {
	Code           string          `gorm:"column:code"`
	SecondIndustry string          `gorm:"column:second_industry"`
	Name           string          `gorm:"column:name"`
	UpDownRange    decimal.Decimal `gorm:"column:up_down_range"`
	Turnover       decimal.Decimal `gorm:"column:turnover"`
	TotalTurnover  decimal.Decimal `gorm:"column:total_turnover"`
}

func IndexMonitorScript()  {
	if setting.ServerSetting.Stock == "on" {
		if home.GetStockTradeDate() {
			date := home.GetTimeDuration(time.Now())
			if util.ContainsAny(date, time.Now().Format(util.YMDHM)){
				IndexMonitorUpDownChange(time.Now())
				IndexMonitorUpDownChangeReleative(time.Now())
				SendAmountRate(time.Now())
			}
		}
	}
}
func IndexMonitorUpDownChange(now time.Time) {
	type TradeDateStruct struct {
		TradeDate         time.Time            `gorm:"column:trade_date"`
	}
	type IndexSumChange struct {
		PctChange         decimal.Decimal         `gorm:"column:pctchange"`
		Time              time.Time               `gorm:"column:time"`
	}
	type IndustryMonitor struct {
		SecondIndustry    string               `gorm:"column:second_industry"`
		UpDownRange       decimal.Decimal      `gorm:"column:up_down_range"`
	}
	var err error
	type IndexClose struct {
		Time           time.Time               `gorm:"column:time"`
		PctChange         decimal.Decimal      `gorm:"column:close"`
	}
	preDay := IndexClose{}
	// 获取每条指数点位和上一个交易日的收盘价
	err = models.DB().Table("p_stock_minute_realtime").
		Where("stock_code = '000001.SH' ").
		Where("time < ? ",now.Format(util.YMD)+" 09:30:00").Order("id desc").First(&preDay).Error
	// 获取当前时间半个小时内的最高值和最低值
	indexMax := IndexClose{}
	err = models.DB().Table("p_stock_minute_realtime").
		Where("stock_code = '000001.SH'").
		Where("time >= ? ",now.Add(-10*time.Minute).Format(util.YMDHMS)).
		Where("id = (select id from p_stock_minute_realtime WHERE (stock_code = '000001.SH') AND (time > ? ) AND (time <= ? ) AND close > 0  ORDER BY close desc limit 1)",now.Add(-10*time.Minute).Format(util.YMDHMS),now.Format(util.YMDHMS)).
		Where("time <= ? ",now.Format(util.YMDHMS)).
		Find(&indexMax).Error
	if err != nil {
		logging.Info("获取失败")
		return
	}
	indexMin := IndexClose{}
	err = models.DB().Table("p_stock_minute_realtime").
		Where("stock_code = '000001.SH'").
		Where("time >= ? ",now.Add(-10*time.Minute).Format(util.YMDHMS)).
		Where("id = (select id from p_stock_minute_realtime WHERE (stock_code = '000001.SH') AND (time > ? )  AND close > 0 AND (time <= ? ) ORDER BY close asc limit 1)",now.Add(-10*time.Minute).Format(util.YMDHMS),now.Format(util.YMDHMS)).
		Where("time <= ? ",now.Format(util.YMDHMS)).
		Find(&indexMin).Error
	if err != nil {
		logging.Info("获取失败")
		return
	}

	rate := (indexMax.PctChange.Sub(indexMin.PctChange)).Div(preDay.PctChange).Round(4)
	if rate.Sub(decimal.NewFromFloat(0.0050)).Sign() > 0 || rate.Sub(decimal.NewFromFloat(-0.0050)).Sign() < 0 {
		industryName := GetIndustry()
		logging.Info("开始时间："+now.Format(util.YMDHMS))
		logging.Info("最大值",indexMax)
		logging.Info("最小值",indexMin)
		logging.Info(fmt.Sprintf("当前指数涨跌幅：%v",rate))
		// 获取当前时间
		industry := []IndustryMonitor{}
		err = models.DB().Table("u_industry_monitor").
			Where("created_at < ?",indexMax.Time).
			Where("second_industry in (?)", industryName).
			Where("created_at >= ?",indexMax.Time.Add(-60).Format(util.YMDHM)).Find(&industry).Error
		preIndustry := []IndustryMonitor{}
		err = models.DB().Table("u_industry_monitor").
			Where("created_at < ?",indexMin.Time).
			Where("second_industry in (?)", industryName).
			Where("created_at >= ?",indexMin.Time.Add(-60).Format(util.YMDHM)).Find(&preIndustry).Error
		tmp := map[string]decimal.Decimal{}
		for _,item := range preIndustry {
			tmp[item.SecondIndustry] = item.UpDownRange
		}
		ret := []IndustryMonitor{}
		for _,item := range industry {
			ret = append(ret,IndustryMonitor{SecondIndustry: item.SecondIndustry,
				UpDownRange: item.UpDownRange.Sub(tmp[item.SecondIndustry]).Round(3)})
		}
		sort.Slice(ret, func(i, j int) bool {
			return ret[i].UpDownRange.Sub(ret[j].UpDownRange).Sign() > 0
		})
		logging.Info(fmt.Sprintf("当前开始时间%v结束时间%v",indexMax.Time,indexMin.Time))
		logging.Info("行业涨跌幅：",ret[0:15],ret[len(ret)-15:])
		logging.Info("************************************************************")
		for _,value := range ret {
			eventStr := ""
			if value.UpDownRange.Sign() > 0 && rate.Sub(decimal.NewFromFloat(0.0050)).Sign() > 0 {
				eventStr = "逆势上涨"
				if rate.Sub(decimal.NewFromFloat(0.0050)).Sign() > 0 {
					eventStr = "上涨冲刺"
				}
			}else {
				eventStr = "跳水下跌"
				if rate.Sub(decimal.NewFromFloat(0.0050)).Sign() > 0 {
					eventStr = "逆势上涨"
				}
			}
			_ = SaveEventData(value.SecondIndustry,eventStr,fmt.Sprintf("%v~%v,%v行业%v",
				indexMax.Time.Format("15:04"),indexMin.Time.Format("15:04"),value.SecondIndustry,eventStr))
		}
		return
	}
}
func IndexMonitorUpDownChangeReleative(now time.Time) {
	type IndexSumChange struct {
		PctChange         decimal.Decimal      `gorm:"column:sum_pctchange"`
	}
	type IndustryMonitor struct {
		SecondIndustry    string               `gorm:"column:second_industry"`
		UpDownRange       decimal.Decimal      `gorm:"column:up_down_range"`
	}
	type TradeDateStruct struct {
		TradeDate         time.Time            `gorm:"column:trade_date"`
	}
	var err error
	timeS := now.Format(util.YMDHMS)
	timeInit,_ := time.Parse(util.YMDHMS,timeS)
	preTime := now.Add(-30*time.Minute).Format(util.YMDHMS)
	// 获取指数10分钟内的涨跌幅
	index := IndexSumChange{}
	err = models.DB().Table("p_stock_minute_realtime").Select("sum(pctchange) as sum_pctchange").
		Where("stock_code = '000001.SH'").
		Where("time > ? ",preTime).
		Where("time <= ? ",timeS).
		First(&index).Error
	if err != nil {
		logging.Info("获取失败")
		return
	}
	rate := index.PctChange.Mul(decimal.NewFromInt(100)).Round(4)
	if rate.Sub(decimal.NewFromFloat(0.80)).Sign() > 0 || rate.Sub(decimal.NewFromFloat(-0.50)).Sign() < 0 {
		logging.Info("开始时间："+timeInit.Format(util.YMDHMS))
		logging.Info(fmt.Sprintf("当前指数涨跌幅：%v",rate))
		industryName := GetIndustry()
		// 获取当前时间
		last,_ := time.Parse(util.YMDHMS,preTime)
		industry := []IndustryMonitor{}
		err = models.DB().Table("u_industry_monitor").
			Where("created_at < ?",preTime).
			Where("second_industry in (?)", industryName).
			Where("created_at >= ?",last.Add(-60*time.Second).Format(util.YMDHM)).Find(&industry).Error
		now,_ := time.Parse(util.YMDHMS,timeS)
		preIndustry := []IndustryMonitor{}
		err = models.DB().Table("u_industry_monitor").
			Where("created_at < ?",timeS).
			Where("second_industry in (?)", industryName).
			Where("created_at >= ?",now.Add(-60*time.Second).Format(util.YMDHM)).Find(&preIndustry).Error
		tmp := map[string]decimal.Decimal{}
		for _,item := range preIndustry {
			tmp[item.SecondIndustry] = item.UpDownRange
		}
		ret := []IndustryMonitor{}
		for _,item := range industry {
			ret = append(ret,IndustryMonitor{SecondIndustry: item.SecondIndustry,
				UpDownRange: item.UpDownRange.Sub(tmp[item.SecondIndustry]).Round(3)})
		}
		sort.Slice(ret, func(i, j int) bool {
			return ret[i].UpDownRange.Sub(ret[j].UpDownRange).Sign() > 0
		})
		for _,value := range ret {
			eventStr := ""
			if value.UpDownRange.Sign() > 0 && rate.Sub(decimal.NewFromFloat(0.0050)).Sign() > 0 {
				eventStr = "逆势上涨"
				if rate.Sub(decimal.NewFromFloat(0.0050)).Sign() > 0 {
					eventStr = "上涨冲刺"
				}
			}else {
				eventStr = "跳水下跌"
				if rate.Sub(decimal.NewFromFloat(0.0050)).Sign() > 0 {
					eventStr = "逆势上涨"
				}
			}
			_ = SaveEventData(value.SecondIndustry,eventStr,fmt.Sprintf("%v~%v,%v行业%v",timeInit.Add(-10*time.Minute).Format("15:04"),
				timeInit.Format("15:04"),value.SecondIndustry,eventStr))
		}
	}
	return
}
func IndexTestM() {
	type TradeDateStruct struct {
		Time           time.Time              `gorm:"column:trade_date"`
	}
	type IndexSumChange struct {
		Time           time.Time               `gorm:"column:time"`
		PctChange         decimal.Decimal      `gorm:"column:close"`
	}
	type IndustryMonitor struct {
		SecondIndustry    string               `gorm:"column:second_industry"`
		UpDownRange       decimal.Decimal      `gorm:"column:close"`
		Time              time.Time               `gorm:"column:time"`
	}
	day := []TradeDateStruct{}
	var err error
	err = models.DB().Table("b_trade_date").
		Where("trade_date >= '2021-01-15'").
		Where("trade_date < '2021-02-23'").Find(&day).Error
	if err != nil {
		return
	}
	redisKey := "industry"
	for _,v := range day {
		preDay := IndexSumChange{}
		// 获取每条指数点位和上一个交易日的收盘价
		err = models.DB().Table("p_stock_minute_realtime").
			Where("code = '000001' ").
			Where("time = ? ",v.Time.Format(util.YMD)+" 09:30:00").First(&preDay).Error
		preIndustry := []IndustryMonitor{}
		err = models.DB().Table("p_stock_minute_realtime").
			Where("time > ?",v.Time.Format(util.YMD)).
			Where("code = '000001'").
			Where("time <= ?",v.Time.AddDate(0,0,1).Format(util.YMD)).
			Find(&preIndustry).Error
		for _,value := range preIndustry {
			if value.UpDownRange.IsZero() {
				continue
			}
			if gredis.HExists(redisKey,value.Time.Format(util.YMD)) {
				continue
			}
			if value.UpDownRange.Sub(preDay.PctChange).Div(value.UpDownRange).Sub(decimal.NewFromFloat(0.01)).Sign() > 0 {
				//logging.Info("当前时间涨跌幅：",v.PctChange)
				preIndustry := []IndustryMonitor{}
				err = models.DB().Table("u_industry_monitor").
					Where("created_at < ?",value.Time).
					Where("created_at >= ?",value.Time.Add(-60*time.Second).Format(util.YMDHM)).Find(&preIndustry).Error
				sort.Slice(preIndustry, func(i, j int) bool {
					return preIndustry[i].UpDownRange.Sub(preIndustry[j].UpDownRange).Sign() > 0
				})
				if len(preIndustry) > 15 {
					//gredis.HSet(value.Time.Format(util.YMD),value.UpDownRange.Sub(preDay.PctChange).Div(value.UpDownRange).Round(3))
					logging.Info("当前时间点位：",value.UpDownRange)
					logging.Info("开盘价：",preDay.PctChange)
					logging.Info("当前时间：",value.Time)
					logging.Info("当前涨跌幅：",value.UpDownRange.Sub(preDay.PctChange).Div(value.UpDownRange).Round(3))
					logging.Info("当前10分钟内行业涨跌幅：",preIndustry[0:15],preIndustry[len(preIndustry)-15:])
					logging.Info("************************************************************")
				}

			}
		}
	}
}
type IndustryMonitor struct {
	SecondIndustry string          `gorm:"column:second_industry"`
	UpDownRange    decimal.Decimal `gorm:"column:up_down_range"`
	Time           time.Time       `gorm:"column:time"`
}
func IndustryMonitorFunc()  {
	type TradeDateStruct struct {
		Time time.Time `gorm:"column:trade_date"`
	}
	type IndexSumChange struct {
		Time      time.Time       `gorm:"column:time"`
		PctChange decimal.Decimal `gorm:"column:close"`
	}
	type IndustryMonitorTurnover struct {
		SecondIndustry string          `gorm:"column:second_industry"`
		UpDownRange    decimal.Decimal `gorm:"column:up_down_range"`
		Time           time.Time       `gorm:"column:time"`
		TotalTurnover  decimal.Decimal `gorm:"column:total_turnover"`
		IncreaseNum    int             `gorm:"column:increase_num"`
		DecreaseNum    int             `gorm:"colunm:decrease_num"`
		AmountRate     decimal.Decimal             `gorm:"colunm:amount_rate"`
	}
	var err error
	ret := []string{}
	industry := GetIndustry()
	// 获取每天的15:00指数涨幅大于3% 并且量比大于1.5
	now := time.Now()
	mIndustry := []IndustryMonitor{}
	err = models.DB().Table("u_industry_monitor").
		Where("minute_time = ?",now.Format("15:04")).
		Where("second_industry in (?)", industry).
		Where("time = ?", now.Format(util.YMD)).
		Where("up_down_range < -3 ").Find(&mIndustry).Error
	if err != nil {
		logging.Info("get industry err", err)
		return
	}
	for _, m := range mIndustry {
		if !util.ContainsAny(ret, m.SecondIndustry) {
			ret = append(ret, m.SecondIndustry)
		}
	}
	retAfter := []string{}
	for _, value := range ret {
		// 获取当前指数的情况
		nowIndustry := IndustryMonitorTurnover{}
		err = models.DB().Table("u_industry_monitor").
			Where("minute_time = ?",now.Format("15:04")).
			Where("second_industry = ? ", value).
			Where("time = ?", now.Format(util.YMD)).First(&nowIndustry).Error
		if err != nil {
			logging.Info("get industry err", err)
		}
		// 获取每一个行业内对应的股票
		increaseNum := 0
		stocks := []Stock{}
		err = models.DB().Table("u_industry_stock_minute").
			Select("u_industry_stock_minute.*,up_down_range*turnover as total_turnover").
			Where("created_at >= ?", now.Format(util.YMDHM)).
			Where("created_at < ?", now.Add(time.Second*60).Format(util.YMDHM)).
			Where("second_industry = ? ", value).Find(&stocks).Error
		for _, item := range stocks {
			if item.UpDownRange.Sub(nowIndustry.UpDownRange.Div(decimal.NewFromInt(2))).Sign() < 0 {
				increaseNum++
			}
		}
		totalNum := nowIndustry.IncreaseNum + nowIndustry.DecreaseNum
		if nowIndustry.IncreaseNum < 1 {
			continue
		}
		if decimal.NewFromInt(int64(increaseNum)).Div(decimal.NewFromInt(int64(totalNum))).Sub(decimal.NewFromFloat(0.50)).Sign() > 0 {
			if decimal.NewFromInt(int64(nowIndustry.DecreaseNum)).Div(decimal.NewFromInt(int64(nowIndustry.IncreaseNum))).Sub(decimal.NewFromFloat(2.0)).Sign() > 0 {
				// logging.Info("总数量", totalNum)
				// logging.Info("跌幅超过一半以上的数量", increaseNum)
				// logging.Info("当前指数涨跌幅", nowIndustry.UpDownRange)
				// logging.Info("当前指数量比", nowIndustry.AmountRate)
				// logging.Info("当前行业为：", nowIndustry.SecondIndustry)
				// logging.Info("当前时间为：",  now.Format(util.YMD)+" 15:00:00")
				// err = SaveEventData(nowIndustry.SecondIndustry,"今日行业","指数涨幅大于3% 并且量比大于1.5，行业名称："+nowIndustry.SecondIndustry)
				retAfter = append(retAfter, nowIndustry.SecondIndustry)
			}
		}
	}

	// 获取每天的11:30指数涨幅大于3% 并且量比大于1.5
	fIndustry := []IndustryMonitor{}
	err = models.DB().Table("u_industry_monitor").
		Where("minute_time = ?",now.Format("15:04")).
		Where("second_industry in (?)", industry).
		Where("time = ?", now.Format(util.YMD)).
		Where("up_down_range > 3 and amount_rate > 1.5").Find(&fIndustry).Error
	if err != nil {
		logging.Info("get industry err", err)
	}
	// 连续三日涨幅超10%
	tIndustry := []IndustryMonitor{}
	err = models.DB().Raw("select * from (select second_industry,sum(up_down_range) as up_down_range from  u_industry_monitor "+
		"where created_at >= ? and created_at < ? and minute_time = ?  GROUP BY second_industry) as a where a.up_down_range > 8 and a.second_industry in (?)", now.AddDate(0, 0, -3).Format(util.YMD), now.Format(util.YMD), now.Format("15:04"),industry).Find(&tIndustry).Error
	if err != nil {
		logging.Info("get industry err", err)
	}
	for _, five := range fIndustry {
		if !util.ContainsAny(ret, five.SecondIndustry) {
			retAfter = append(retAfter, five.SecondIndustry)
		}
	}
	for _, t := range tIndustry {
		if !util.ContainsAny(ret, t.SecondIndustry) {
			retAfter = append(retAfter, t.SecondIndustry)
		}
	}
	logging.Info("get industry ret ",retAfter)
	for _, value := range retAfter {
		// 获取当前指数的情况
		nowIndustry := IndustryMonitorTurnover{}
		err = models.DB().Table("u_industry_monitor").
			Where("minute_time = ?",now.Format("15:04")).
			Where("second_industry = ? ", value).
			Where("time = ?", now.Format(util.YMD)).First(&nowIndustry).Error
		if err != nil {
			logging.Info("get industry err", err)
		}
		// 获取每一个行业内对应的股票
		increaseNum := 0
		stocks := []Stock{}
		err = models.DB().Table("u_industry_stock_minute").
			Select("u_industry_stock_minute.*,up_down_range*turnover as total_turnover").
			Where("created_at >= ?",now.Format(util.YMDHM)).
			Where("created_at < ?", now.Add(time.Second*60).Format(util.YMDHM)).
			Where("second_industry = ? ", value).Find(&stocks).Error
		sort.Slice(stocks, func(i, j int) bool {
			return stocks[i].TotalTurnover.GreaterThan(stocks[j].TotalTurnover)
		})
		logging.Info(stocks[0:1])
		logging.Info((stocks[0].TotalTurnover.Add(stocks[1].TotalTurnover)).Div(nowIndustry.TotalTurnover.Mul(nowIndustry.UpDownRange)))
		if stocks[0].TotalTurnover.Div(nowIndustry.TotalTurnover.Mul(nowIndustry.UpDownRange)).
			Sub(decimal.NewFromFloat(0.60)).Sign() > 0 {
			continue
		}
		for _, item := range stocks {
			if item.UpDownRange.Sub(nowIndustry.UpDownRange.Div(decimal.NewFromInt(2))).Sign() > 0 {
				increaseNum++
			}
		}
		totalNum := nowIndustry.IncreaseNum + nowIndustry.DecreaseNum
		if decimal.NewFromInt(int64(increaseNum)).Div(decimal.NewFromInt(int64(totalNum))).Sub(decimal.NewFromFloat(0.30)).Sign() > 0 {
			logging.Info("总数量", totalNum)
			logging.Info("涨幅超过一半以上的数量", increaseNum)
			logging.Info("当前指数涨跌幅", nowIndustry.UpDownRange)
			logging.Info("当前指数量比", nowIndustry.AmountRate)
			logging.Info("当前行业为：", nowIndustry.SecondIndustry)
			logging.Info("当前时间为：", now.Format(util.YMD)+" 15:00:00")
			_ = SaveEventData(nowIndustry.SecondIndustry,"今日行业",nowIndustry.SecondIndustry+"行业连续三日涨幅超10%")
		}
	}
}
func GetIndustry() []string {
	type IndustryName struct {
		SecondIndustry string `gorm:"column:second_industry"`
	}
	var err error
	industry := []string{}
	// 获取重点行业
	importIndustry := []IndustryName{}
	err = models.DB().Table("u_performance_industry").Select("industry_name as second_industry").Where("important = 1").Find(&importIndustry).Error
	// 获取满足条件的行业
	industryNames := []IndustryName{}
	err = models.DB().Raw(
		"select * from (select count(*) as count,second_industry from u_stock_industry GROUP BY second_industry) as a " +
			" where a.count > 5 ").Find(&industryNames).Error
	if err != nil && len(importIndustry) == 0 {
		return industry
	}
	// 对重点行业进行合并
	for _, v := range importIndustry {
		industry = append(industry, strings.Split(v.SecondIndustry, "-")[1])
	}
	for _, v := range industryNames {
		if strings.ContainsAny(v.SecondIndustry, "II") {
			continue
		}
		if strings.ContainsAny(v.SecondIndustry, "未分配") {
			continue
		}
		if util.ContainsAny(industry,v.SecondIndustry) {
			continue
		}
		industry = append(industry, v.SecondIndustry)
	}
	return industry
}
// 监控量比
func SendAmountRate(now time.Time)  {
	industry := []IndustryMonitor{}
	industryName := GetIndustry()
	err := models.DB().Table("u_industry_monitor").
		Where("minute_time = ? ",now.Format("15:04")).
		Where("amount_rate > 2").
		Where("second_industry in (?)", industryName).
		Where("time = ? ",now.Format(util.YMD)).Find(&industry).Error
	if err != nil {
		return
	}
	for _,value := range industry {
		_ = SaveEventData(value.SecondIndustry,"量能放大",value.SecondIndustry+"行业量比达到2")
	}
	return
}
// 根据行业获取贡献度排名前三的股票
func GetStockByIndustry(industry string) []Stock {
	stocks := []Stock{}
	now := time.Now()
	err := models.DB().Table("u_industry_stock_minute").
		Select("u_industry_stock_minute.*,up_down_range*turnover as total_turnover").
		Where("created_at >= ?",now.Format(util.YMDHM)).
		Where("created_at < ?", now.Add(time.Second*60).Format(util.YMDHM)).
		Where("second_industry = ? ", industry).Find(&stocks).Error
	if err != nil {
		logging.Info("get stock industry err",err)
		return nil
	}
	sort.Slice(stocks, func(i, j int) bool {
		return stocks[i].TotalTurnover.GreaterThan(stocks[j].TotalTurnover)
	})
	logging.Info(stocks[0:3])
	return stocks[0:3]
}
const IndustryEventName = "行业事件"
// 二级分类
type EventCategory struct {
	Id                int              `gorm:"column:id"`
	FirstClassId      int              `gorm:"column:first_class_id"`
	SecondClassId     int              `gorm:"column:second_class_id"`
	Name              string           `gorm:"column:name"`
}
func(EventCategory) TableName() string {
	return "p_event_category"
}
type EventClass struct {
	Id                int            `gorm:"column:id"`
	Name              string         `gorm:"column:name"`
	Level             int            `gorm:"column:level"`
}
func(EventClass) TableName() string {
	return "p_event_class"
}
type EventData struct {
	Id                         int               `gorm:"column:id"`
	CategoryId                 int               `gorm:"cloumn:category_id"`
	CategoryName               string            `gorm:"cloumn:category_name"`
	Frequency                  string            `gorm:"cloumn:frequency"`
	Year                       int               `gorm:"cloumn:year"`
	Day                        int               `gorm:"cloumn:day"`
	Month                      int               `gorm:"cloumn:month"`
	Value                      string            `gorm:"cloumn:value"`
}
type EventStock struct {
	CategoryId                 int               `gorm:"cloumn:category_id"`
	CategoryName               string            `gorm:"cloumn:category_name"`
	Code                       string            `gorm:"cloumn:code"`
	DataId                     int               `gorm:"cloumn:data_id"`
}
func(EventStock) TableName() string {
	return "p_event_stock"
}
// 事件库新增行业
func SaveEventClass() error {
	// 存入一级分类  p_event_class
	var err error
	event := EventClass{}
	err = models.DB().Table("p_event_industry_class").Where("name = ? ",IndustryEventName).First(&event).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}
	if err == gorm.ErrRecordNotFound {
		eventClass := EventClass{}
		eventClass.Name = IndustryEventName
		eventClass.Level = 1
		dbs := models.DB().Table("p_event_industry_class").Create(&eventClass)
		err = dbs.Error
		if err != nil {
			return err
		}
	}
	return nil
}
// 插入事件库
func SaveEventData(industryName,eventString,desc string) error {
	redisKey := time.Now().Format(util.YMD)+"_"+getEnglishName(eventString)+"_industry_push"
	if gredis.HExists(redisKey,industryName) {
		return nil
	}
	var err error
	// 获取一级分类
	eventClass := EventClass{}
	err = models.DB().Table("p_event_industry_class").Where("name = ? ",IndustryEventName).First(&eventClass).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}
	if err == gorm.ErrRecordNotFound {
		err = SaveEventClass()
		if err != nil {
			return err
		}
	}
	eventFirstId := EventClass{}
	err = models.DB().Table("p_event_industry_class").Where("name = ? ",IndustryEventName).First(&eventFirstId).Error
	eventCategory := EventCategory{}
	err = models.DB().Table("p_event_industry_category").Where("name = ? ",eventString).
		Where("first_class_id = ? ",eventFirstId.Id).First(&eventCategory).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}
	if err == gorm.ErrRecordNotFound {
		eventCategory.Name = eventString
		eventCategory.FirstClassId = eventFirstId.Id
		eventCategory.SecondClassId = 2   // 默认是2级
		dbs := models.DB().Table("p_event_industry_category").Create(&eventCategory)
		err = dbs.Error
		if err != nil {
			return err
		}
	}
	gredis.HSet(redisKey,industryName,time.Now().Format(util.YMDHMS))
	gredis.Expire(redisKey,9*3600)
    eventData := EventData{
    	CategoryId: eventCategory.Id,
    	CategoryName: desc,
    	Value: "",
    	Frequency: "日",
    	Year: time.Now().Year(),
    	Day: time.Now().Day(),
    	Month: int(time.Now().Month()),
	}
	err = models.DB().Table("p_event_industry_data").Create(&eventData).Error
	if err != nil {
		logging.Info("存入行业监控数据失败")
		return err
	}
	// 添加股票数据
	// 对行业数据进行保存
	industryStocks := GetStockByIndustry(industryName)
	for _,stock := range industryStocks {
		eventStock := EventStock{
			CategoryId: eventCategory.Id,
			CategoryName: desc,
			Code: stock.Code,
			DataId: eventData.Id,
		}
		err = models.DB().Table("p_event_industry_stock").Create(&eventStock).Error
		if err != nil {
			logging.Info("存入行业监控数据失败")
			return err
		}
	}
	return nil
}



// 跑出一个月的历史数据
// func Test()  {
// 	type TradeDateStruct struct {
// 		Time           time.Time              `gorm:"column:trade_date"`
// 	}
// 	type IndustryMonitor struct {
// 		SecondIndustry    string               `gorm:"column:second_industry"`
// 		UpDownRange       decimal.Decimal      `gorm:"column:close"`
// 		Time              time.Time               `gorm:"column:time"`
// 	}
// 	day := []TradeDateStruct{}
// 	var err error
// 	err = models.DB().Table("b_trade_date").
// 		Where("trade_date >= '2021-01-03'").
// 		Where("trade_date = '2021-03-03'").
// 		Where("trade_date < '2021-03-04'").Find(&day).Error
// 	if err != nil {
// 		return
// 	}
// 	for _,v := range day {
// 		preIndustry := []IndustryMonitor{}
// 		err = models.DB().Table("p_index_minute").
// 			Where("time > ?",v.Time.Format(util.YMD)).
// 			Where("code = '000001'").
// 			Where("time <= ?",v.Time.AddDate(0,0,1).Format(util.YMD)).
// 			Find(&preIndustry).Error
// 		for _,value := range preIndustry {
// 			IndexMonitorScript(value.Time)
// 		}
// 	}
// }

func getEnglishName(title string) string {
	switch title {
	case "上涨冲刺":
		return "increase"
	case "跳水下跌":
		return "decrease"
	case "逆势上涨":
		return "revert_decreas"
	case "量能放大":
		return "amount_rate"
	case "今日行业":
		return "today"
	}
	return ""
}