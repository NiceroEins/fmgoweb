package cron

import (
	home2 "datacenter/models/home"
	"datacenter/models/message"
	"datacenter/pkg/setting"
	"datacenter/pkg/websocket"
	"encoding/json"
	"github.com/robfig/cron"
	"strconv"
)

// ┌─────────────second 范围 (0 - 60)
// │ ┌───────────── min (0 - 59)
// │ │ ┌────────────── hour (0 - 23)
// │ │ │ ┌─────────────── day of month (1 - 31)
// │ │ │ │ ┌──────────────── month (1 - 12)
// │ │ │ │ │ ┌───────────────── day of week (0 - 6) (0 to 6 are Sunday to
// │ │ │ │ │ │                  Saturday)
// │ │ │ │ │ │
// │ │ │ │ │ │
// * * * * * *

var instance *cron.Cron

func Setup() {
	if setting.ServerSetting.Type == "web" {
		instance = cron.New()
		//_ = instance.AddFunc("0 0 14 * * ?", performanceUndisclosed)
		//_ = instance.AddFunc("0 55 8 * * ?", performanceUndisclosed)
		//_ = instance.AddFunc("0 0 9 * * ?", performanceDisclosed)
		//_ = instance.AddFunc("0 0 16 * * ?", stock.Cron)
		//_ = instance.AddFunc("1 0 0 * * ?", StockPoolTimeOut)
		//_ = instance.AddFunc("1 0 0 * * ?", statistics.FetchNewsNumByDate())
		//_ = instance.AddFunc("0 10 16 * * ?", performance.Monster)
		//_ = instance.AddFunc("0 10 16 * * ?", performance.HighRatio)
		//_ = instance.AddFunc("0 52 23 * * ?", statistics.CheckSeed)
		//_ = instance.AddFunc("0 50 23 * * ?", statistics.ResetSeed)
		//_ = instance.AddFunc("0 25 09 * * ?", stock_pool_service.UpdateTransferPrice)
		////_ = instance.AddFunc("*/10 * * * * ?", storage.TestAuction)  //测试
		////_ = instance.AddFunc("0 50 15 * * ?", replay.CronReplayStatistics)
		////_ = instance.AddFunc("1 0 18 * * ?", AutoCalculateRateSet)
		////_ = instance.AddFunc("0 10 16 * * ?", StockReportPeriodSet)
		//_ = instance.AddFunc("0 10 23 * * ?", StockReportPeriodSet)
		//_ = instance.AddFunc("0 00 17 * * ?", InvestigationFallBack)
		////重点券商存量标记 最近60天
		//_ = instance.AddFunc("0 10 18 * * ?", SetImportantNearly60Days)
		//// 定时更新股票代码和名称 一天一次
		//_ = instance.AddFunc("0 00 12 * * ?", UpdateStockNames)
		////拷贝新股票到b_stock表
		////_ = instance.AddFunc("0 30 15 * * ?", CopyNewCode2BStock)
		////shares_info 拷贝新股票到b_stock表
		//_ = instance.AddFunc("0 20 23 * * ?", CopyNewCode2BStockFromSharesInfo)
		////研报行情走势提醒
		//_ = instance.AddFunc("0 40 15 * * ?", reportDisclosed)
		////前一交易日成交额写入到redis 需要日线已经跑完
		////_ = instance.AddFunc("1 0 0 * * ?", SetPreVol2Redis)
		////盘后脚本集合 顺序执行
		//_ = instance.AddFunc("0 0 16 * * ?", AfterScriptSet)
		////业绩预告规则提醒
		//_ = instance.AddFunc("0 56 8 * * ?", PerformanceForecastPublishAlert)
		//
		////从redis更新股票分时数据
		////_ = instance.AddFunc("0 * * * * ?", home.StoreIndustryStockMinute)
		//// 从redis更新统计数据
		//_ = instance.AddFunc("0 */1 * * * ?", home.StoreIndustryStockStatistics)
		//// 更新收盘价
		//_ = instance.AddFunc("1 33 15 * * ?", home.UpdateClose)
		//// 公募基金推送
		//_ = instance.AddFunc("0 0 9 * * ?", public_fund.PushPublicFundData)
		//_ = instance.AddFunc("0 */1 * * * ?", public_fund.PushFundList)
		////成交前200 每五分钟一次
		//
		////_ = instance.AddFunc("0 */15 * * * ?", AmountRankMonitor)
		//
		////净利润预测上调监控
		//_ = instance.AddFunc("0 */5 * * * ?", YearNetProfitChangeUpSet)
		//_ = instance.AddFunc("0 */5 * * * ?", MultiFirstResearchReportMonitor)
		////AH股涨跌幅监控
		//_ = instance.AddFunc("0 */5 * * * ?", ahMonitor)
		//
		//_ = instance.AddFunc("0 */10 * * * ?", BlockRate)
		//_ = instance.AddFunc("0 */30 * * * ?", PushIndexNum)
		//
		//_ = instance.AddFunc("0 45 9 * * ?", LastPushCount)
		//_ = instance.AddFunc("0 31 11 * * ?", HomeStockPush)
		//_ = instance.AddFunc("0 01 15 * * ?", HomeStockPush)
		//// 核按钮监控
		////_ = instance.AddFunc("0 33 09 * * ?", NuclearButton)
		//_ = instance.AddFunc("0 0 9 * * ?", northWardPush)
		//
		////10个交易日前昨收价写入redis
		//_ = instance.AddFunc("0 55 15 * * ?", PreSeveralDayTranscation)
		//
		////预发测试行情5分钟线
		////_ = instance.AddFunc("0 0-59 10-15 * * ?", quantization.MinuteStock)
		////超买超卖
		//_ = instance.AddFunc("0 0-59/5 * * * ?", quantization.SuperCharge)
		//
		//_ = instance.AddFunc("0 */10 * * * ?", OverOrLowerPerformanceMonitor)
		//// 预发单次执行
		////_ = instance.AddFunc("0 41 17 * * ?", quantization.GenerateTechnical)
		////_ = instance.AddFunc("0 55 16 * * ?", OnceScript)
		//
		//_ = instance.AddFunc("0 15 9 * * ?", StockBulkPush)
		//
		//// 市场行情数据
		//_ = instance.AddFunc("0 31 11 * * ?", IndustryMonitorFunc)
		//_ = instance.AddFunc("0 01 15 * * ?", IndustryMonitorFunc)
		//_ = instance.AddFunc("40 * * * * ?", IndexMonitorScript)
		//
		////预发测试分钟线
		//_ = instance.AddFunc("0 */1 * * * ?", stock2.Exec)
		instance.Start()
	}
}

func push(m map[string]interface{}, msgType message.MessageType) {
	ws := websocket.GetInstance()
	bt, _ := json.Marshal(m)
	msg := message.ParseMsg(msgType, "system", "", string(bt), true)
	ws.SysSend(msg)
}

func pushByCode(m map[string]interface{}, code string, msgType message.MessageType) {
	userList := home2.GetUserListByCode(code)
	ws := websocket.GetInstance()
	bt, _ := json.Marshal(m)
	for _, v := range userList {
		msg := message.ParseMsg(msgType, "system", strconv.Itoa(v), string(bt), true)
		ws.SysSend(msg)
	}
}
