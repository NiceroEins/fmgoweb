package cron

import (
	"datacenter/models"
	"datacenter/models/home"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"fmt"
	"time"
)

func reportDisclosed() {
	date := time.Now().Format("2006-01-02")
	subscribeReport(date)
	subscribeReportMore(date)
}

func subscribeReport(date string) {
	var data []struct {
		Code     string
		Name     string
		PreClose float64
		Close    float64
	}
	if err := models.DB().Table("p_stock_tick, u_research_report_data, u_research_report, u_industry_securities_author_relationship, b_trade_date, b_trade_date A, p_stock_tick B, u_stock_industry").
		Select("u_research_report_data.code, u_research_report_data.name, u_research_report.publish_date, u_research_report.organization, u_research_report.author, u_research_report_data.rising_space, p_stock_tick.pre_close, p_stock_tick.close, B.amount").
		Where("p_stock_tick.stock_code = u_research_report_data.code").
		Where("u_research_report.id = u_research_report_data.report_id").
		Where("u_research_report_data.rising_space IS NOT NULL").
		Where("u_research_report_data.rising_space > 0.35 OR SUBSTRING_INDEX(u_research_report_data.rising_space,',',1) > 0.35 OR SUBSTRING_INDEX(u_research_report_data.rising_space,',',-1) > 0.35").
		Where("u_research_report_data.publish_date = ?", date).
		Where("p_stock_tick.trade_date = ?", date).
		Where("u_industry_securities_author_relationship.securities = u_research_report.organization").
		Where("u_research_report.author LIKE CONCAT('%',u_industry_securities_author_relationship.author,'%')").
		Where("p_stock_tick.close > p_stock_tick.pre_close * 1.05").
		Where("b_trade_date.trade_date = p_stock_tick.trade_date").
		Where("A.id = b_trade_date.id - 1").
		Where("B.trade_date = A.trade_date").
		Where("B.stock_code = u_research_report_data.code").
		Where("B.amount > 300000").
		Where("u_stock_industry.code = u_research_report_data.code").
		Where("u_stock_industry.first_industry = u_industry_securities_author_relationship.industry_name").
		Group("u_research_report_data.code, u_research_report.publish_date").
		Find(&data).Error; err != nil {
		logging.Error("cron.subscribeReport() Errors: ", err.Error())

	}
	for _, v := range data {
		if v.PreClose > 0 {
			change := (v.Close - v.PreClose) / v.PreClose * 100
			home.PushByCode(map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s %s 今日发布研报上涨%.2f%%", v.Code, v.Name, change),
			}, v.Code, "今日发布研报上涨")
		}
	}
}

func subscribeReportMore(date string) {
	var data []struct {
		Code     string
		Name     string
		PreClose float64
		Close    float64
		Highest  float64
	}
	if err := models.DB().Table("p_stock_tick, u_research_report_data, u_research_report, u_industry_securities_author_relationship, b_trade_date, b_trade_date A, p_stock_tick B, u_stock_industry").
		Select("u_research_report_data.code, u_research_report_data.name, u_research_report.publish_date, u_research_report.organization, u_research_report.author, u_research_report_data.rising_space, p_stock_tick.pre_close, p_stock_tick.close, B.amount, MAX(B.close) AS highest").
		Where("p_stock_tick.stock_code = u_research_report_data.code").
		Where("u_research_report.id = u_research_report_data.report_id").
		Where("u_research_report_data.rising_space IS NOT NULL").
		Where("u_research_report_data.rising_space > 0.35 OR SUBSTRING_INDEX(u_research_report_data.rising_space,',',1) > 0.35 OR SUBSTRING_INDEX(u_research_report_data.rising_space,',',-1) > 0.35").
		Where("u_research_report_data.publish_date = b_trade_date.trade_date").
		Where("A.trade_date = ?", date).
		Where("u_industry_securities_author_relationship.securities = u_research_report.organization").
		Where("u_research_report.author LIKE CONCAT('%',u_industry_securities_author_relationship.author,'%')").
		Where("b_trade_date.trade_date = p_stock_tick.trade_date").
		Where("B.trade_date = A.trade_date").
		Where("B.stock_code = u_research_report_data.code").
		Where("B.amount > 300000").
		Where("A.id - b_trade_date.id > 0 AND A.id - b_trade_date.id <= 5").
		Where("u_stock_industry.code = u_research_report_data.code").
		Where("u_stock_industry.first_industry = u_industry_securities_author_relationship.industry_name").
		Group("u_research_report_data.code, u_research_report.publish_date").
		Having("highest > p_stock_tick.pre_close * 1.1").
		Find(&data).Error; err != nil {
		logging.Error("cron.subscribeReport() Errors: ", err.Error())

	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, v := range data {
		if v.PreClose > 0 && !conn.Exists("report:incr:"+v.Code) {
			change := (v.Highest - v.PreClose) / v.PreClose * 100
			conn.Set("report:incr:"+v.Code, date, 3600*24*7)
			home.PushByCode(map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s %s 研报发布连续上涨%.2f%%", v.Code, v.Name, change),
			}, v.Code, "研报发布连续上涨")
		}
	}
}
