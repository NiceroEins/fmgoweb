package cron

import (
	"datacenter/models"
	"datacenter/models/common"
	"datacenter/models/event"
	"datacenter/models/home"
	"datacenter/models/message"
	"datacenter/models/northward"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"sort"
	"strings"
	"time"
)

func northWardPush() {
	tradeDate := common.TradeDate{}
	if !tradeDate.GetDay(time.Now().Format(util.YMD), 0) {
		return
	}
	type northWardPush struct {
		Code      string                   `json:"code"`
		Name      string                   `json:"name"`
		MinHold   decimal.Decimal          `json:"max_hold"`
		AddHold   decimal.Decimal          `json:"add_hold"`
		TotalHold decimal.Decimal          `json:"total_hold"`
		Days      int                      `json:"days"`
		Rate      decimal.Decimal          `json:"rate"`
		Close     decimal.Decimal          `json:"close"`
		Zsz       decimal.Decimal          `json:"zsz"`
		NearData  []northward.NorthWardRow `json:"near_data" gorm:"FOREIGNKEY:Code;ASSOCIATION_FOREIGNKEY:Code"`
	}
	type pushStruct struct {
		Code    string          `json:"code"`
		Name    string          `json:"name"`
		Days    int             `json:"days"`
		AddHold string          `json:"add_hold"`
		Rate    decimal.Decimal `json:"rate"`
	}
	dtos := make([]northWardPush, 0)
	dayArea := 15                 //15天内
	var szLimit int64 = 500000000 //5亿
	var rateLimit float32 = 1.5
	pushData := make([]pushStruct, 0)
	pushKey := "north_push"
	dbs := models.DB().Table("p_stock_northward").
		Select("p_stock_northward.code,b_stock.name,p_stock_northward.total_hold,p_stock_northward.add_hold,p_stock_northward.created_at,p_stock_northward.updated_at,p_stock_tick.close").
		Preload("NearData", func(db *gorm.DB) *gorm.DB {
			return models.DB().Table("(select *,row_number() over(partition by code order by date desc) as n from p_stock_northward) a").
				Where("n<=?", dayArea).
				Where("date<(select max(date) date from p_stock_northward)").
				Order("code desc,date desc")
		}).
		Joins("LEFT JOIN b_stock ON p_stock_northward.code=b_stock.id").
		Joins("LEFT JOIN p_stock_tick ON p_stock_tick.stock_code=p_stock_northward.code and p_stock_tick.trade_date=p_stock_northward.date").
		Where("date = (select max(date) date from p_stock_northward)")
	err := dbs.Order("code desc").Find(&dtos).Error
	if err != nil {
		logging.Error("获取北向资金列表失败", err.Error())
		return
	}
	ltgMap, err := gredis.Clone(setting.RedisSetting.StockDB).HGetAllStringMap("stock:ltg")
	if err != nil {
		logging.Error("获取ltgmap失败", err.Error())
	}
	for _, dto := range dtos {
		lastIsMax := true
		dto.MinHold = dto.TotalHold
		for j, _ := range dto.NearData {
			if dto.NearData[j].TotalHold.LessThan(dto.MinHold) {
				dto.MinHold = dto.NearData[j].TotalHold
				dto.Days = j + 2
			}
			if dto.NearData[j].TotalHold.GreaterThan(dto.TotalHold) {
				lastIsMax = false
				break
			}
		}
		if !lastIsMax {
			continue
		}
		if ltgStr, ok := ltgMap[dto.Code]; ok {
			ltg := decimal.Decimal{}
			err = ltg.Scan(ltgStr)
			if err != nil {
				logging.Error("解析"+dto.Code+"ltg"+ltgStr+"失败", err.Error())
			} else {
				dto.Rate = dto.TotalHold.Sub(dto.MinHold).Mul(decimal.NewFromInt(100)).Div(ltg)
				dto.Zsz = dto.Close.Mul(ltg)
			}
			if dto.Zsz.GreaterThan(decimal.NewFromInt(szLimit)) && dto.Rate.GreaterThan(decimal.NewFromFloat32(rateLimit)) {
				pushData = append(pushData, pushStruct{
					Code:    dto.Code,
					Name:    dto.Name,
					Days:    dto.Days,
					AddHold: dto.TotalHold.Sub(dto.MinHold).String(),
					Rate:    dto.Rate.Round(2),
				})
			}
		} else {
			logging.Error("获取" + dto.Code + "ltg失败")
		}

	}
	if len(pushData) > 0 {
		date, err := northward.GetLatestTime()
		if err != nil {
			logging.Error("northWardPush", err.Error())
		} else {
			lastTime, err := gredis.GetString(pushKey)
			if err != nil {
				logging.Error("redis获取northWardPush上次推送时间失败", err.Error())
			} else {
				if strings.Trim(lastTime, "\"") >= date.Format(util.YMD) {
					logging.Warn("northWardPush数据未更新")
					return
				}
			}
			sort.Slice(pushData, func(i, j int) bool {
				return pushData[i].Rate.GreaterThan(pushData[j].Rate)
			})
			push(map[string]interface{}{
				"type":  "north_push",
				"list":  pushData,
				"title": date.Format(util.YMD) + "北向资金累计大幅流入汇总",
			}, message.Message_Stock)
			err = gredis.Set(pushKey, date.Format(util.YMD), -1)
			if err != nil {
				logging.Error("northWardPush设置推送日前报错", err.Error())
			}
		}
	}
}

func syncHeadStock2StockPool() {
	desc := "重点股票"
	type head struct {
		Code   string `json:"code"`
		UserId int    `json:"user_id"`
	}
	type pool struct {
		Id     int    `json:"id"`
		Code   string `json:"code"`
		UserId int    `json:"user_id"`
	}
	headCodes := make([]head, 0)
	poolCodes := make([]pool, 0)
	err := models.DB().Table("u_stock_industry").
		Select("u_stock_industry.code,u_second_industry_user.user_id").
		Joins("LEFT JOIN u_second_industry_user on u_second_industry_user.industry_id=u_stock_industry.industry_id").
		Where("u_stock_industry.head=1").
		Where("u_second_industry_user.user_id>0 and u_second_industry_user.deleted_at is null").
		Find(&headCodes).Error
	if err != nil {
		logging.Error("获取重点股票列表失败", err.Error())
		return
	}
	err = models.DB().Table("u_user_stock").
		Where("type='stock_pool' and description=?", desc).
		Find(&poolCodes).Error
	if err != nil {
		logging.Error("获取自选股池重点股票列表失败", err.Error())
		return
	}
	for _, head := range headCodes {
		isNew := true
		for _, pool := range poolCodes {
			if head.Code == pool.Code && head.UserId == pool.UserId {
				isNew = false
			}
		}
		if isNew {
			sp := home.StockPool{
				Code:        head.Code,
				Type:        "stock_pool",
				Description: desc,
				UserID:      head.UserId,
			}
			err = sp.Add()
			if err != nil {
				logging.Error("自动录入重点股票添加失败", sp, err.Error())
			}
		}
	}
	for _, pool := range poolCodes {
		Rep := true
		for _, head := range headCodes {
			if head.Code == pool.Code && head.UserId == pool.UserId {
				Rep = false
			}
		}
		if Rep {
			sp := home.StockPool{
				Id: pool.Id,
			}
			err = sp.Del()
			if err != nil {
				logging.Error("自动录入重点股票删除失败", sp, err.Error())
			}
		}
	}
}

func OnceScript2() {
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	keys, err := conn.Keys("performance:industry:预告*")
	if err != nil {
		fmt.Println(err.Error())
	}
	type industryDTO struct {
		Code           string `json:"code"`
		SecondIndustry string `json:"second_industry"`
		IndustryId     string `json:"industry_id"`
	}
	dtos := make([]industryDTO, 0)
	err = models.DB().Table("u_stock_industry").Find(&dtos).Error
	if err != nil {
		fmt.Println(err.Error())
	}
	industryMap := map[string]string{}
	for _, dto := range dtos {
		industryMap[dto.Code] = dto.IndustryId
	}
	for _, key := range keys {
		infoMap, err := conn.HGetAllStringMap(key)
		if err != nil {
			fmt.Println(err.Error())
		}
		for code, name := range infoMap {
			if id, ok := industryMap[code]; ok {
				err = conn.HSet("performance:industry:预告2020-12-31:"+id, code, name)
				if err != nil {
					fmt.Println(err.Error())
				}
			} else {
				err = conn.HSet("performance:industry:预告2020-12-31:155", code, name)
			}
			fmt.Println(code)
			fmt.Println(name)
			//conn.HSet()
		}
	}
}

func autoUpdateBStockEventLine() {
	columns := make([]event.StockEventDTO, 0)
	datas := make([]event.StockEventDTO, 0)
	dateTypes := make([]string, 0)

	/*err:=models.DB().Table("b_stock_event").Group("type").Find(&columns).Error
	if err != nil {
		logging.Error("",err.Error())
	}*/
	err := models.DB().Table("b_stock_event").Group("date_type").Pluck("date_type", &dateTypes).Error
	if err != nil {
		logging.Error("", err.Error())
	}
	for _, dateType := range dateTypes {
		err := models.DB().Table("b_stock_event").Where("date_type=?", dateType).Group("type").Order("type").Find(&columns).Error
		if err != nil {
			logging.Error("", err.Error())
		}
		//生成零时表
		table := "b_stock_event_" + dateType
		tmpTable := table + "_tmp"
		err = models.DB().Exec("DROP TABLE IF EXISTS `" + tmpTable + "`").Error
		if err != nil {
			logging.Error("", err.Error())
			continue
		}
		sql := "CREATE TABLE `" + tmpTable + "` (`id` bigint(20) NOT NULL AUTO_INCREMENT,`code` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '证券代码',`date` date DEFAULT NULL,"

		for _, column := range columns {
			sql = sql + event.GetCreateSqlFromType(column.Type, column.DataType)
		}
		sql = sql + " `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间', `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间', PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;"

		err = models.DB().Exec(sql).Error
		if err != nil {
			logging.Error("", err.Error())
			continue
		}
		//插入数据
		err = models.DB().Table("b_stock_event").Where("date_type=?", dateType).Order("date,code").Find(&datas).Error
		if err != nil {
			logging.Error("", err.Error())
		}
		//preDate := time.Time{}
		preData := event.StockEventDTO{}
		insertSql := "INSERT INTO " + tmpTable + " (code,"
		columnsStr := ""
		datasStr := ""
		for _, data := range datas {
			if preData.Code == "" {
				preData.Code = data.Code
				preData.Date = data.Date
				preData.Type = data.Type
				datasStr = "'" + data.Code + "',"
			}
			if !preData.Date.Equal(data.Date) && !preData.Date.Equal(time.Time{}) || preData.Code != data.Code {
				err = models.DB().Exec(insertSql + columnsStr + "date) VALUES (" + datasStr + "'" + preData.Date.Format(util.YMD) + "')").Error
				if err != nil {
					logging.Error("", err.Error())
				}
				columnsStr = ""
				datasStr = "'" + data.Code + "',"
			}
			/*if data.Code != preData.Code {
				columnsStr = ""
				if datasStr != "'" + data.Code + "'," {
					datasStr = datasStr + "'" + preData.Date.Format(util.YMD) + "'),('" + data.Code + "',"
				}
			}*/
			columnsStr = columnsStr + data.Type + ","
			datasStr = datasStr + "'" + data.Data + "',"
			preData = data
		}
		if len(datas) > 0 {
			err = models.DB().Exec(insertSql + columnsStr + "date) VALUES (" + datasStr + "'" + datas[len(datas)-1].Date.Format(util.YMD) + "')").Error
			if err != nil {
				logging.Error("", err.Error())
				continue
			}
		}
		//替换表
		err = models.DB().Exec("DROP TABLE IF EXISTS `" + table + "`").Error
		if err != nil {
			logging.Error("", err.Error())
			continue
		}
		err = models.DB().Exec("ALTER TABLE `" + tmpTable + "` rename to `" + table + "`").Error
		if err != nil {
			logging.Error("", err.Error())
			continue
		}
	}
}
