package cron

import (
	"datacenter/controller/async"
	"datacenter/models"
	"datacenter/models/common"
	"datacenter/models/home"
	"datacenter/models/message"
	"datacenter/models/performance"
	"datacenter/models/replay"
	"datacenter/models/spider"
	"datacenter/models/stock"
	"datacenter/models/template"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/pkg/websocket"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Perf struct {
	Code               string `json:"code"`
	Name               string `json:"name"`
	ReportPeriod       string `json:"report_period"`       //报告期
	DisclosureTime     string `json:"disclosure_time"`     //披露时间
	ForecastProportion string `json:"forecast_proportion"` //预测比例
	ForecastNetProfit  string `json:"forecast_net_profit"` //预测净利润
	HighLights         string `json:"high_lights"`         //亮点
	BrokerProportion   string `json:"broker_proportion"`   //券商预测
}

func performanceUndisclosed() {
	for _, v := range []int{5, 3, 1} {
		data := subscribePerf(v)
		if len(data) > 0 {
			push(map[string]interface{}{
				"type":             "pre_forecast",
				"day":              v,
				"data":             data,
				"highly_recommend": true,
			}, message.Message_System_Performance)
		}
	}
}

func reportPeriod(t time.Time) []string {
	return []string{"2020-06-30"}
}

func subscribePerf(dayAfter int) []Perf {
	var ret []Perf
	for _, v := range queryPerf(time.Now().Add(time.Duration(dayAfter) * 24 * time.Hour).Format("2006-01-02")) {
		if filter(v.Code) {
			ret = append(ret, v)
		}
	}
	return ret
}

func queryPerf(date string) []Perf {
	var data []Perf
	db := models.DB()
	db = db.Table("u_performance_forecast").
		Select("u_performance_forecast.*, u_performance_edit.forecast_proportion, u_performance_edit.forecast_net_profit, u_performance_edit.high_lights, u_performance_edit.broker_proportion").
		Joins("LEFT JOIN u_stock_industry_relationship ON u_stock_industry_relationship.stock_id = u_performance_forecast.`code`").
		Joins("LEFT JOIN u_performance_edit ON u_performance_edit.`code` = u_performance_forecast.`code` AND u_performance_edit.report_period = u_performance_forecast.report_period").
		//Where("u_performance_forecast.report_period IN (?)", reportPeriod(time.Now())).
		Where("u_performance_forecast.disclosure_time = ?", date).
		Where("u_performance_forecast.performance IS NULL").
		Where("u_performance_edit.recommend = 1")
	if err := db.Order("u_performance_forecast.disclosure_time").Find(&data).Error; err != nil {
		logging.Error("cron.queryPerf() Query data Errors: ", err.Error())
	}
	return data
}

func filter(code string) bool {
	if strings.Contains(code, "ST") || strings.Contains(code, "st") {
		return false
	}
	return true
}

type PerfMore struct {
	Perf
	models.Simple
	PublishDate string
	RisingRate  string  `gorm:"-"`
	Ltg         float64 `json:"-"`
}

func performanceDisclosed() {
	for _, v := range []int{5, 3, 1} {
		data := subscribePerfMore(v)
		if len(data) > 0 {
			push(map[string]interface{}{
				"type":             "forecast",
				"day":              v,
				"data":             data,
				"highly_recommend": true,
			}, message.Message_System_Performance)
		}
	}
}

func subscribePerfMore(dayAfter int) []PerfMore {
	var ret []PerfMore
	data := queryPerfMore(time.Now().Add(time.Duration(dayAfter) * 24 * time.Hour).Format("2006-01-02"))
	for _, v := range data {
		if queryRisingRate(&v) && filter(v.Code) {
			ret = append(ret, v)
		}
	}
	return ret
}

func queryPerfMore(date string) []PerfMore {
	var data []PerfMore
	db := models.DB()
	db = db.Table("u_performance_forecast").
		Select("u_performance_forecast.*, u_performance_edit.forecast_proportion, u_performance_edit.forecast_net_profit, u_performance_edit.high_lights, u_performance_edit.broker_proportion, p_public_company.ltg").
		Joins("LEFT JOIN u_stock_industry_relationship ON u_stock_industry_relationship.stock_id = u_performance_forecast.`code`").
		Joins("LEFT JOIN u_performance_edit ON u_performance_edit.`code` = u_performance_forecast.`code` AND u_performance_edit.report_period = u_performance_forecast.report_period").
		Joins("LEFT JOIN p_public_company ON p_public_company.stock_code = u_performance_forecast.`code`").
		//Where("u_performance_forecast.report_period IN (?)", reportPeriod(time.Now())).
		Where("u_performance_forecast.disclosure_time = ?", date).
		Where("u_performance_forecast.performance IS NOT NULL")
	if err := db.Order("u_performance_forecast.disclosure_time").Find(&data).Error; err != nil {
		logging.Error("cron.queryPerfMore() Query data Errors: ", err.Error())
	}
	return data
}

func queryRisingRate(p *PerfMore) bool {
	var data []struct {
		Close    float64
		Block    float64
		PreClose float64
	}
	db := models.DB()
	db = db.Table("p_stock_tick").Where("stock_code = ?", p.Code)
	db = db.Where("trade_date >= ?", p.PublishDate)
	if err := db.Order("trade_date").Limit(2).Find(&data).Error; err != nil {
		logging.Error("cron.queryRisingRate() Query data Errors: ", err.Error())
		return false
	}

	for _, v := range data {
		if v.Close == v.Block && v.Close > 0 || p.Ltg*v.Close > 20000000000 && v.Close >= v.PreClose*1.05+0.01 {
			risingRate := v.Close/v.PreClose - 1
			if risingRate >= 0.11 || risingRate < 0.01 {
				//新股和停牌剔除
				return false
			}
			p.RisingRate = fmt.Sprintf("%.04f", risingRate)
			return true
		}
	}
	return false
}

func StockPoolTimeOut() {
	deadline := time.Unix(time.Now().Unix()-86400, 0).Format("2006-01-02 15:04:05")
	err := models.DB().Exec("DELETE FROM b_stock_pool_type WHERE timeout_at >'1900-01-01' AND timeout_at<?", deadline).Error
	if err != nil {
		logging.Error("stockPool timeout script err:", err.Error())
	}
}

type TotalChangeDTO struct {
	TradeDate *time.Time      `json:"trade_date"`
	PctChg    decimal.Decimal `json:"pct_chg"`
	TsCode    string          `json:"ts_code"`
}

func (dto TotalChangeDTO) TableName() string {
	return "p_stock_tick"
}

type CodeChangeDTO struct {
	Code      string           `json:"code"`
	PctChg    decimal.Decimal  `json:"pct_chg"`
	TradeDate *time.Time       `json:"trade_date"`
	Totals    []TotalChangeDTO `json:"total" gorm:"FOREIGNKEY:TradeDate;ASSOCIATION_FOREIGNKEY:TradeDate"`
}

type StockReportPeriodDTO struct {
	Id               string          `json:"id"`
	StartDate        *time.Time      `json:"start_date"` //上市时间
	AnnouncementDate *time.Time      `json:"announcement_date"`
	ForecastDate     *time.Time      `json:"forecast_date"`
	AnnChange        []CodeChangeDTO `json:"ann_change" gorm:"FOREIGNKEY:Code;ASSOCIATION_FOREIGNKEY:Id"`
	ForChange        []CodeChangeDTO `json:"for_change" gorm:"FOREIGNKEY:Code;ASSOCIATION_FOREIGNKEY:Id"`
}

type StockReportPeriodIO struct {
	Id               int64               `json:"id"`
	Code             string              `json:"code"`
	AOne             decimal.NullDecimal `json:"a_one"`
	AThree           decimal.NullDecimal `json:"a_three"`
	AFive            decimal.NullDecimal `json:"a_five"`
	APreOne          decimal.NullDecimal `json:"a_pre_one"`
	APreThree        decimal.NullDecimal `json:"a_pre_three"`
	APreFive         decimal.NullDecimal `json:"a_pre_five"`
	FOne             decimal.NullDecimal `json:"f_one"`
	FThree           decimal.NullDecimal `json:"f_three"`
	FFive            decimal.NullDecimal `json:"f_five"`
	FPreOne          decimal.NullDecimal `json:"f_pre_one"`
	FPreThree        decimal.NullDecimal `json:"f_pre_three"`
	FPreFive         decimal.NullDecimal `json:"f_pre_five"`
	AnnouncementDate *time.Time          `json:"announcement_date"`
	ForecastDate     *time.Time          `json:"forecast_date"`
	AutoCalRate      decimal.NullDecimal `json:"auto_cal_rate"`
	AutoRateRule     string              `json:"auto_rate_rule"`
	ReportPeriod     string              `json:"report_period"`
}

func (io StockReportPeriodIO) TableName() string {
	return "p_stock_report_period"
}

func (dto CodeChangeDTO) getCodeSubTotalChangeSum() decimal.NullDecimal {
	var res decimal.NullDecimal
	var tsCode string
	switch {
	case !strings.HasPrefix(dto.Code, "000") && strings.HasPrefix(dto.Code, "00"):
		tsCode = "399005.XSHE"
	case strings.HasPrefix(dto.Code, "000"):
		tsCode = "399001.XSHE"
	case strings.HasPrefix(dto.Code, "300"):
		tsCode = "399006.XSHE"
	case strings.HasPrefix(dto.Code, "688"):
		tsCode = "000688.XSHG"
	case !strings.HasPrefix(dto.Code, "688") && strings.HasPrefix(dto.Code, "6"):
		tsCode = "000001.XSHG"
	}
	for _, v := range dto.Totals {
		if v.TsCode == tsCode {
			res = decimal.NullDecimal{Decimal: dto.PctChg.Sub(v.PctChg), Valid: true}
			break
		}
	}
	return res
}

func StockReportPeriodData(offset int) {
	nowReportPeriod, _ := util.ReportPeriod(offset)
	dtos := make([]StockReportPeriodDTO, 0)
	//1 3 5 天前
	err := models.DB().Table("b_stock S").
		Select("S.*,A.publish_date announcement_date,F.publish_date forecast_date").
		Preload("AnnChange", func(db *gorm.DB) *gorm.DB {
			return db.Table("( SELECT `A`.`code`,`A`.`publish_date`,`AST`.`pct_chg`,`AST`.`trade_date`,ROW_NUMBER() over(partition by `A`.`code` order by `AST`.`trade_date` DESC) n FROM u_performance_announcement A LEFT JOIN p_stock_tick AST ON `AST`.`stock_code`=`A`.`code` WHERE `AST`.`trade_date`<`A`.`publish_date` AND `A`.`report_period`='"+nowReportPeriod+"' ) ATOTAL").
				Preload("Totals", func(db *gorm.DB) *gorm.DB {
					return db.Where("stock_code IS NULL")
				}).
				Select("code,publish_date,`pct_chg`,trade_date ").
				Where("n<=5")
		}).
		Preload("ForChange", func(db *gorm.DB) *gorm.DB {
			return db.Table("( SELECT `F`.`code`,`F`.`publish_date`,`FST`.`pct_chg`,`FST`.`trade_date`,ROW_NUMBER() over(partition by `F`.`code` order by `FST`.`trade_date` DESC) n FROM u_performance_forecast F LEFT JOIN p_stock_tick FST ON `FST`.`stock_code`=`F`.`code` WHERE `FST`.`trade_date`<`F`.`publish_date` AND `F`.`report_period`='"+nowReportPeriod+"' ) ATOTAL").
				Preload("Totals", func(db *gorm.DB) *gorm.DB {
					return db.Where("stock_code IS NULL")
				}).
				Select("code,publish_date,`pct_chg`,trade_date ").
				Where("n<=5")
		}).
		Joins("LEFT JOIN u_performance_announcement A ON A.code=S.id AND A.report_period=?", nowReportPeriod).
		Joins("LEFT JOIN u_performance_forecast F ON F.code=S.id AND F.report_period=?", nowReportPeriod).
		//Where("A.report_period=?", nowReportPeriod).
		//Where("F.report_period=?", nowReportPeriod).
		Where("A.publish_date IS NOT NULL OR F.publish_date IS NOT NULL").
		Find(&dtos).Error
	if err != nil {
		logging.Error("StockReportPeriodData() 获取公告预告135天前涨幅失败", err.Error())
	}
	for _, v := range dtos {
		achangeSum := decimal.NullDecimal{}
		fchangeSum := decimal.NullDecimal{}
		io := StockReportPeriodIO{}
		io.Code = v.Id
		io.AnnouncementDate = v.AnnouncementDate
		io.ForecastDate = v.ForecastDate
		io.ReportPeriod = nowReportPeriod
		for j, a := range v.AnnChange {
			if v.AnnouncementDate != nil && v.StartDate != nil && v.AnnouncementDate.Format(util.YMD) <= v.StartDate.Format(util.YMD) {
				break
			}
			subchange := a.getCodeSubTotalChangeSum()
			achangeSum.Decimal = achangeSum.Decimal.Add(subchange.Decimal)
			achangeSum.Valid = subchange.Valid
			switch j {
			case 0:
				io.APreOne.Decimal = achangeSum.Decimal
				io.APreOne.Valid = achangeSum.Valid
				break
			case 2:
				io.APreThree.Decimal = achangeSum.Decimal
				io.APreThree.Valid = achangeSum.Valid
				break
			case 4:
				io.APreFive.Decimal = achangeSum.Decimal
				io.APreFive.Valid = achangeSum.Valid
				break
			}
		}
		for j, f := range v.ForChange {
			if v.ForecastDate != nil && v.StartDate != nil && v.ForecastDate.Format(util.YMD) <= v.StartDate.Format(util.YMD) {
				break
			}
			subchange := f.getCodeSubTotalChangeSum()
			fchangeSum.Decimal = fchangeSum.Decimal.Add(subchange.Decimal)
			fchangeSum.Valid = subchange.Valid
			switch j {
			case 0:
				io.FPreOne.Decimal = fchangeSum.Decimal
				io.FPreOne.Valid = fchangeSum.Valid
				break
			case 2:
				io.FPreThree.Decimal = fchangeSum.Decimal
				io.FPreThree.Valid = fchangeSum.Valid
				break
			case 4:
				io.FPreFive.Decimal = fchangeSum.Decimal
				io.FPreFive.Valid = fchangeSum.Valid
				break
			}
		}
		models.DB().Assign(io).Where("code=? AND report_period=?", io.Code, io.ReportPeriod).FirstOrCreate(&StockReportPeriodIO{})
	}
	//1 3 5天后
	err = models.DB().Table("b_stock S").
		Select("S.*,A.publish_date announcement_date,F.publish_date forecast_date").
		Preload("AnnChange", func(db *gorm.DB) *gorm.DB {
			return db.Table("( SELECT `A`.`code`,`A`.`publish_date`,`AST`.`pct_chg`,`AST`.`trade_date`,ROW_NUMBER() over(partition by `A`.`code` order by `AST`.`trade_date` ASC) n FROM u_performance_announcement A LEFT JOIN p_stock_tick AST ON `AST`.`stock_code`=`A`.`code` WHERE `AST`.`trade_date`>`A`.`publish_date` AND `A`.`report_period`='"+nowReportPeriod+"' ) ATOTAL").
				Preload("Totals", func(db *gorm.DB) *gorm.DB {
					return db.Where("stock_code IS NULL")
				}).
				Select("code,publish_date,`pct_chg`,trade_date ").
				Where("n<=5")
		}).
		Preload("ForChange", func(db *gorm.DB) *gorm.DB {
			return db.Table("( SELECT `F`.`code`,`F`.`publish_date`,`FST`.`pct_chg`,`FST`.`trade_date`,ROW_NUMBER() over(partition by `F`.`code` order by `FST`.`trade_date` ASC) n FROM u_performance_forecast F LEFT JOIN p_stock_tick FST ON `FST`.`stock_code`=`F`.`code` WHERE `FST`.`trade_date`>`F`.`publish_date` AND `F`.`report_period`='"+nowReportPeriod+"' ) ATOTAL").
				Preload("Totals", func(db *gorm.DB) *gorm.DB {
					return db.Where("stock_code IS NULL")
				}).
				Select("code,publish_date,`pct_chg`,trade_date ").
				Where("n<=5")
		}).
		Joins("LEFT JOIN u_performance_announcement A ON A.code=S.id AND A.report_period=?", nowReportPeriod).
		Joins("LEFT JOIN u_performance_forecast F ON F.code=S.id AND F.report_period=?", nowReportPeriod).
		//Where("A.report_period=?", nowReportPeriod).
		//Where("F.report_period=?", nowReportPeriod).
		Where("A.publish_date IS NOT NULL OR F.publish_date IS NOT NULL").
		Find(&dtos).Error
	if err != nil {
		logging.Error("StockReportPeriodData() 获取公告预告135天后涨幅失败", err.Error())
	}
	for _, v := range dtos {
		achangeSum := decimal.NullDecimal{}
		fchangeSum := decimal.NullDecimal{}
		io := StockReportPeriodIO{}
		io.Code = v.Id
		io.AnnouncementDate = v.AnnouncementDate
		io.ForecastDate = v.ForecastDate
		io.ReportPeriod = nowReportPeriod
		for j, a := range v.AnnChange {
			subchange := a.getCodeSubTotalChangeSum()
			achangeSum.Decimal = achangeSum.Decimal.Add(subchange.Decimal)
			achangeSum.Valid = subchange.Valid
			switch j {
			case 0:
				io.AOne.Decimal = achangeSum.Decimal
				io.AOne.Valid = achangeSum.Valid
				break
			case 2:
				io.AThree.Decimal = achangeSum.Decimal
				io.AThree.Valid = achangeSum.Valid
				break
			case 4:
				io.AFive.Decimal = achangeSum.Decimal
				io.AFive.Valid = achangeSum.Valid
				break
			}
		}
		for j, f := range v.ForChange {
			if v.ForecastDate != nil && v.StartDate != nil && v.ForecastDate.Format(util.YMD) <= v.StartDate.Format(util.YMD) {
				break
			}
			subchange := f.getCodeSubTotalChangeSum()
			fchangeSum.Decimal = fchangeSum.Decimal.Add(subchange.Decimal)
			fchangeSum.Valid = subchange.Valid
			switch j {
			case 0:
				io.FOne.Decimal = fchangeSum.Decimal
				io.FOne.Valid = fchangeSum.Valid
				break
			case 2:
				io.FThree.Decimal = fchangeSum.Decimal
				io.FThree.Valid = fchangeSum.Valid
				break
			case 4:
				io.FFive.Decimal = fchangeSum.Decimal
				io.FFive.Valid = fchangeSum.Valid
				break
			}
		}
		models.DB().Assign(io).Where("code=? AND report_period=?", io.Code, io.ReportPeriod).FirstOrCreate(&StockReportPeriodIO{})
	}
}

type AutoCalRule struct {
	Rate     decimal.NullDecimal
	Rule     func(code string, offset int) decimal.NullDecimal
	RuleName string
}

//券商最近两个月预测年度净利润平均值
func NearTwoMonthYearAVG(code string, offset int) decimal.NullDecimal {
	//offset = offset * 3 //3个月一个报告期
	reportPeriod, _ := util.ReportPeriod(offset)
	reportPeriodTime, _ := time.ParseInLocation(util.YMD, reportPeriod, time.Local)
	if reportPeriodTime.After(time.Now()) {
		reportPeriodTime = time.Now().AddDate(0, -1, 0)
	}
	start := reportPeriodTime.AddDate(0, 0, 1).AddDate(0, -1, 0).Format(util.YMD)
	end := reportPeriodTime.AddDate(0, 0, 1).AddDate(0, 1, 0).Format(util.YMD)
	year := strconv.Itoa(reportPeriodTime.Year())
	var netProfitList []map[string]float64
	var sumNetProfit decimal.Decimal
	var count int32 = 0
	err := mongo.GetMongoDB("shares", "F10_mechanismForecast", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"code":         code,
			"publish_date": bson.M{"$gte": start, "$lte": end},
		}).Select(bson.M{"_id": 0, year: 1}).All(&netProfitList)
	})
	if err != nil {
		logging.Error(code + " mongo获取近两个月券商预测失败")
	}
	for _, n := range netProfitList {
		if netPrfit, ok := n[year]; ok {
			count++
			sumNetProfit = sumNetProfit.Add(decimal.NewFromFloat(netPrfit))
		}
	}
	if count > 0 {
		return decimal.NullDecimal{Decimal: sumNetProfit.Div(decimal.NewFromInt32(count)), Valid: err == nil}
	} else {
		return decimal.NullDecimal{}
	}
}

func calculateRate(thisYear, lastYear decimal.Decimal) decimal.Decimal {
	return thisYear.Sub(lastYear).Mul(decimal.NewFromInt32(100)).DivRound(lastYear.Abs(), 2)
}

func lastNetProfit(code string, offset int) (decimal.NullDecimal, decimal.NullDecimal) {
	lastReportPeriod, _ := util.ReportPeriod(offset - 1)
	lastNetProfit := struct {
		NetProfit           decimal.NullDecimal `json:"net_profit"`
		NetProfitGrowthRate decimal.NullDecimal `json:"net_profit_growth_rate"`
	}{}
	err := models.DB().Table("u_performance_announcement").Where("code=? AND report_period=?", code, lastReportPeriod).First(&lastNetProfit).Error
	if err != nil {
		logging.Error(code + " 获取" + lastReportPeriod + "净利润失败")
		return decimal.NullDecimal{}, decimal.NullDecimal{}
	}
	/*if lastNetProfit.NetProfit.Decimal.Cmp(decimal.Zero) < 0 {
		return decimal.NullDecimal{}, decimal.NullDecimal{}
	}*/
	return lastNetProfit.NetProfit, lastNetProfit.NetProfitGrowthRate
}

type netProfitSet struct {
	Code                       string              `json:"code"`
	NetProfit                  decimal.NullDecimal `json:"net_profit"`
	NetProfitGrowthRate        decimal.NullDecimal `json:"net_profit_growth_rate"`
	NetProfitQuarter           decimal.NullDecimal `json:"net_profit"`
	NetProfitGrowthRateQuarter decimal.NullDecimal `json:"net_profit_growth_rate"`
}

func getNetProfitSet(code string, offset int) (netProfitSet, error) {
	lastReportPeriod, _ := util.ReportPeriod(offset)
	lastNetProfitSet := netProfitSet{}
	err := models.DB().Table("u_performance_announcement").Where("code=? AND report_period=?", code, lastReportPeriod).First(&lastNetProfitSet).Error
	if err != nil {
		logging.Warn(code + " 获取" + lastReportPeriod + "单季度净利润失败")
		return netProfitSet{}, err
	}
	return lastNetProfitSet, nil
}

func qOneRule1(code string, offset int) decimal.NullDecimal {
	lastYearNetProfit, lastYearNetProfitGrowthRate := lastNetProfit(code, offset-3)
	if !lastYearNetProfit.Valid || lastYearNetProfit.Decimal.Cmp(decimal.Zero) < 0 {
		return decimal.NullDecimal{}
	}
	return decimal.NullDecimal{Decimal: lastYearNetProfitGrowthRate.Decimal.Div(decimal.NewFromInt32(100)).Add(decimal.NewFromInt32(1)).Mul(lastYearNetProfit.Decimal), Valid: lastYearNetProfit.Valid && lastYearNetProfitGrowthRate.Valid}
}

func qOneRule2(code string, offset int) decimal.NullDecimal {
	type netProfits struct {
		//NetProfit        decimal.NullDecimal `json:"net_profit"`
		NetProfitQuarter decimal.NullDecimal `json:"net_profit_quarter"`
		ReportPeriod     *time.Time          `json:"report_period"`
	}
	NetProfits := make([]netProfits, 0)
	reportPeriods := []string{}
	for i := 1; i <= 3; i++ {
		reportPeriods = append(reportPeriods, strconv.Itoa(time.Now().AddDate(-i, 0, 0).Year())+"-"+"03-31")
		//reportPeriods = append(reportPeriods, strconv.Itoa(time.Now().AddDate(-i, 0, 0).Year())+"-"+"12-31")
	}
	err := models.DB().Table("u_performance_announcement").Where("report_period IN (?) AND code=?", reportPeriods, code).Order("report_period ASC").Find(&NetProfits).Error
	if err != nil {
		logging.Error(code + " 获取过去3年数据失败")
		return decimal.NullDecimal{}
	}
	totalRate := decimal.NullDecimal{}
	var rateCount int32 = 0
	for _, v := range NetProfits {
		if v.NetProfitQuarter.Decimal.Cmp(decimal.Zero) > 0 && v.NetProfitQuarter.Valid && v.ReportPeriod.Format(util.MD) == "03-31" {
			rateCount++
			totalRate.Decimal = totalRate.Decimal.Add(v.NetProfitQuarter.Decimal)
			totalRate.Valid = true
		}
	}
	if rateCount > 0 {
		lastYearTotalNetProfit, _ := lastNetProfit(code, offset)
		if !lastYearTotalNetProfit.Valid || lastYearTotalNetProfit.Decimal.Cmp(decimal.Zero) == 0 {
			return decimal.NullDecimal{}
		}
		nearTwoMonthYearAVG := NearTwoMonthYearAVG(code, offset)
		if !nearTwoMonthYearAVG.Valid {
			return decimal.NullDecimal{}
		}
		if totalRate.Decimal.Div(decimal.NewFromInt32(rateCount)).Div(lastYearTotalNetProfit.Decimal).Cmp(decimal.NewFromInt32(1)) >= 0 {
			return decimal.NullDecimal{}
		}
		return decimal.NullDecimal{
			Decimal: totalRate.Decimal.Div(decimal.NewFromInt32(rateCount)).Div(lastYearTotalNetProfit.Decimal).Mul(nearTwoMonthYearAVG.Decimal),
			Valid:   totalRate.Valid && nearTwoMonthYearAVG.Valid && lastYearTotalNetProfit.Valid,
		}
	}
	return decimal.NullDecimal{}
}

func qOneRule3(code string, offset int) decimal.NullDecimal {
	nearTwoMonthYearAVG := NearTwoMonthYearAVG(code, offset)
	if !nearTwoMonthYearAVG.Valid {
		return decimal.NullDecimal{}
	}
	return decimal.NullDecimal{
		Decimal: nearTwoMonthYearAVG.Decimal.Div(decimal.NewFromInt32(4)),
		Valid:   nearTwoMonthYearAVG.Valid,
	}
}

func qTwoRule1(code string, offset int) decimal.NullDecimal {
	lastNetProfit, _ := lastNetProfit(code, offset)
	if !lastNetProfit.Valid || lastNetProfit.Decimal.Cmp(decimal.Zero) < 0 {
		return decimal.NullDecimal{}
	}
	return decimal.NullDecimal{
		Decimal: lastNetProfit.Decimal.Mul(decimal.NewFromInt32(2)),
		Valid:   lastNetProfit.Valid,
	}
}
func qTwoRule2(code string, offset int) decimal.NullDecimal {
	lastNetProfit, _ := lastNetProfit(code, offset)
	if !lastNetProfit.Valid || lastNetProfit.Decimal.Cmp(decimal.Zero) < 0 {
		return decimal.NullDecimal{}
	}
	nearTwoMonthYearAVG := NearTwoMonthYearAVG(code, offset)
	if !nearTwoMonthYearAVG.Valid {
		return decimal.NullDecimal{}
	}
	return decimal.NullDecimal{
		Decimal: nearTwoMonthYearAVG.Decimal.Sub(lastNetProfit.Decimal).Div(decimal.NewFromInt32(3)).Add(lastNetProfit.Decimal),
		Valid:   nearTwoMonthYearAVG.Valid && lastNetProfit.Valid,
	}
}
func qTwoRule3(code string, offset int) decimal.NullDecimal {
	nearTwoMonthYearAVG := NearTwoMonthYearAVG(code, offset)
	if !nearTwoMonthYearAVG.Valid {
		return decimal.NullDecimal{}
	}
	return decimal.NullDecimal{
		Decimal: nearTwoMonthYearAVG.Decimal.Div(decimal.NewFromInt32(2)),
		Valid:   nearTwoMonthYearAVG.Valid,
	}
}

func qThreeRule1(code string, offset int) decimal.NullDecimal {
	lastNetProfit, _ := lastNetProfit(code, offset)
	if !lastNetProfit.Valid || lastNetProfit.Decimal.Cmp(decimal.Zero) < 0 {
		return decimal.NullDecimal{}
	}
	nearTwoMonthYearAVG := NearTwoMonthYearAVG(code, offset)
	if !nearTwoMonthYearAVG.Valid {
		return decimal.NullDecimal{}
	}
	return decimal.NullDecimal{
		Decimal: nearTwoMonthYearAVG.Decimal.Add(lastNetProfit.Decimal).DivRound(decimal.NewFromInt32(2), 2),
		Valid:   nearTwoMonthYearAVG.Valid && lastNetProfit.Valid,
	}
}
func qThreeRule2(code string, offset int) decimal.NullDecimal {
	type netProfits struct {
		//NetProfit        decimal.NullDecimal `json:"net_profit"`
		NetProfitQuarter decimal.NullDecimal `json:"net_profit_quarter"`
		ReportPeriod     *time.Time          `json:"report_period"`
	}
	NetProfits := make([]netProfits, 0)
	reportPeriods := []string{}
	for i := 1; i <= 3; i++ {
		reportPeriods = append(reportPeriods, strconv.Itoa(time.Now().AddDate(-i, 0, 0).Year())+"-"+"09-30")
		//reportPeriods = append(reportPeriods, strconv.Itoa(time.Now().AddDate(-i, 0, 0).Year())+"-"+"12-31")
	}
	err := models.DB().Table("u_performance_announcement").Where("report_period IN (?) AND code=?", reportPeriods, code).Order("report_period ASC").Find(&NetProfits).Error
	if err != nil {
		logging.Error(code + " 获取过去3年数据失败")
		return decimal.NullDecimal{}
	}
	totalRate := decimal.NullDecimal{}
	var rateCount int32 = 0
	for _, v := range NetProfits {
		if v.NetProfitQuarter.Decimal.Cmp(decimal.Zero) > 0 && v.NetProfitQuarter.Valid && v.ReportPeriod.Format(util.MD) == "09-30" {
			rateCount++
			totalRate.Decimal = totalRate.Decimal.Add(v.NetProfitQuarter.Decimal)
			totalRate.Valid = true
		}
	}
	if rateCount > 0 {
		lastYearTotalNetProfit, _ := lastNetProfit(code, offset-2)
		if !lastYearTotalNetProfit.Valid || lastYearTotalNetProfit.Decimal.Cmp(decimal.Zero) == 0 {
			return decimal.NullDecimal{}
		}
		lastNetProfit, _ := lastNetProfit(code, offset)
		if !lastNetProfit.Valid || lastNetProfit.Decimal.Cmp(decimal.Zero) < 0 {
			return decimal.NullDecimal{}
		}
		nearTwoMonthYearAVG := NearTwoMonthYearAVG(code, offset)
		if !nearTwoMonthYearAVG.Valid {
			return decimal.NullDecimal{}
		}
		if totalRate.Decimal.Div(decimal.NewFromInt32(rateCount)).Div(lastYearTotalNetProfit.Decimal).Cmp(decimal.NewFromInt32(1)) >= 0 {
			return decimal.NullDecimal{}
		}
		return decimal.NullDecimal{
			Decimal: totalRate.Decimal.Div(decimal.NewFromInt32(rateCount)).Mul(nearTwoMonthYearAVG.Decimal).Div(lastYearTotalNetProfit.Decimal).Add(lastNetProfit.Decimal),
			Valid:   totalRate.Valid && nearTwoMonthYearAVG.Valid && lastNetProfit.Valid && lastYearTotalNetProfit.Valid,
		}
	}
	return decimal.NullDecimal{}
}
func qThreeRule3(code string, offset int) decimal.NullDecimal {
	nearTwoMonthYearAVG := NearTwoMonthYearAVG(code, offset)
	return decimal.NullDecimal{
		Decimal: nearTwoMonthYearAVG.Decimal.Mul(decimal.NewFromFloat32(0.75)).Round(2),
		Valid:   nearTwoMonthYearAVG.Valid}
}

func qFourRule(code string, offset int) decimal.NullDecimal {
	nearTwoMonthYearAVG := NearTwoMonthYearAVG(code, offset)
	return decimal.NullDecimal{Decimal: nearTwoMonthYearAVG.Decimal.Round(2), Valid: nearTwoMonthYearAVG.Valid}
}

func forwardCalculate(code string, offset int, season int) (decimal.NullDecimal, string) {
	years := 3 //必须大于1
	limitRate := 0.1
	avgRate := decimal.NullDecimal{Valid: true}
	avgGrowRateQuarter := decimal.NullDecimal{Valid: true}
	limitProfitGrowQuarter := decimal.NullDecimal{Valid: true}
	rates := make([]decimal.NullDecimal, 0)
	for i := 1; i <= years; i++ {
		preNetProfit, err := getNetProfitSet(code, offset-(years-i+1)*4-1)
		if err != nil {
			return decimal.NullDecimal{}, ""
		}
		netProfit, err := getNetProfitSet(code, offset-(years-i+1)*4)
		if err != nil {
			return decimal.NullDecimal{}, ""
		}
		if !netProfit.NetProfitQuarter.Valid || netProfit.NetProfitQuarter.Decimal.Cmp(decimal.Zero) <= 0 {
			return decimal.NullDecimal{}, ""
		}
		if preNetProfit.NetProfitQuarter.Valid && preNetProfit.NetProfitQuarter.Decimal.Add(netProfit.NetProfitQuarter.Decimal).Cmp(decimal.Zero) != 0 {
			rate := preNetProfit.NetProfitQuarter.Decimal.Div(preNetProfit.NetProfitQuarter.Decimal.Add(netProfit.NetProfitQuarter.Decimal))
			rates = append(rates, decimal.NullDecimal{Decimal: rate, Valid: preNetProfit.NetProfitQuarter.Valid && netProfit.NetProfitQuarter.Valid})
			avgRate.Decimal = avgRate.Decimal.Add(rate)
		} else {
			rates = append(rates, decimal.NullDecimal{})
		}
		avgRate.Valid = avgRate.Valid && preNetProfit.NetProfitQuarter.Valid && netProfit.NetProfitQuarter.Valid
		avgGrowRateQuarter.Decimal = avgGrowRateQuarter.Decimal.Add(netProfit.NetProfitGrowthRateQuarter.Decimal)
		avgGrowRateQuarter.Valid = avgGrowRateQuarter.Valid && netProfit.NetProfitGrowthRateQuarter.Valid
		if i < years {
			limitProfitGrowQuarter.Decimal = limitProfitGrowQuarter.Decimal.Add(netProfit.NetProfitQuarter.Decimal)
		} else {
			limitProfitGrowQuarter.Decimal = limitProfitGrowQuarter.Decimal.Div(decimal.NewFromInt32(int32(years - 1))).Sub(netProfit.NetProfitQuarter.Decimal).Abs().Div(netProfit.NetProfitQuarter.Decimal)
		}
		limitProfitGrowQuarter.Valid = limitProfitGrowQuarter.Valid && preNetProfit.NetProfitQuarter.Valid
	}
	lastNetProfit, err := getNetProfitSet(code, offset-1)
	if err != nil {
		return decimal.NullDecimal{}, ""
	}
	if lastNetProfit.NetProfitQuarter.Decimal.Cmp(decimal.Zero) <= 0 || lastNetProfit.NetProfitGrowthRateQuarter.Decimal.Cmp(decimal.NewFromInt32(200)) > 0 {
		return decimal.NullDecimal{}, ""
	}
	avgRate.Decimal = avgRate.Decimal.Div(decimal.NewFromInt32(int32(years)))
	avgGrowRateQuarter.Decimal = avgGrowRateQuarter.Decimal.Div(decimal.NewFromInt32(int32(years)))
	lastYearNetProfit, err := getNetProfitSet(code, offset-4)
	if err != nil || lastYearNetProfit.NetProfit.Decimal.Cmp(decimal.Zero) == 0 {
		return decimal.NullDecimal{}, ""
	}
	for _, rate := range rates {
		if !rate.Valid || rate.Decimal.Sub(avgRate.Decimal).Abs().Cmp(decimal.NewFromFloat(limitRate)) > 0 {
			if limitProfitGrowQuarter.Valid && limitProfitGrowQuarter.Decimal.Cmp(decimal.NewFromFloat(limitRate)) < 0 {
				//平均增速
				return decimal.NullDecimal{
					Decimal: lastYearNetProfit.NetProfitQuarter.Decimal.Add(lastYearNetProfit.NetProfitQuarter.Decimal.Mul(avgGrowRateQuarter.Decimal)).Add(lastNetProfit.NetProfit.Decimal).Sub(lastYearNetProfit.NetProfit.Decimal).Div(lastYearNetProfit.NetProfit.Decimal).Mul(decimal.NewFromInt32(100)),
					Valid:   lastYearNetProfit.NetProfitQuarter.Valid && lastNetProfit.NetProfit.Valid && lastYearNetProfit.NetProfit.Valid,
				}, "((去年同期单季度净利润*(1+过去三年同期单季度净利润同比增长的平均)+上季度整年净利润)-去年同期季度整年净利润)/去年同期季度整年净利润"
			}
			return decimal.NullDecimal{}, ""
		}
	}
	//历史平均分布
	return decimal.NullDecimal{
			Decimal: lastNetProfit.NetProfitQuarter.Decimal.Div(avgRate.Decimal).Sub(lastNetProfit.NetProfitQuarter.Decimal).Add(lastNetProfit.NetProfit.Decimal).Sub(lastYearNetProfit.NetProfit.Decimal).Div(lastYearNetProfit.NetProfit.Decimal).Mul(decimal.NewFromInt32(100)),
			Valid:   lastNetProfit.NetProfitQuarter.Valid && avgRate.Valid && lastYearNetProfit.NetProfit.Valid,
		},
		"(((上季度单季度净利润/过去三年上季度占本季度比例的平均)-上季度单季度净利润)+上季度整年净利润)-去年同期季度整年净利润)/去年同期季度整年净利润"
}

func normalCalculate(code string, rules []AutoCalRule, offset int) (decimal.NullDecimal, string) {
	max := 0
	for i, v := range rules {
		rules[i].Rate = v.Rule(code, offset)
		if i > 0 {
			if (rules[i].Rate.Decimal.Cmp(rules[max].Rate.Decimal) > 0 && rules[i].Rate.Valid) || !rules[max].Rate.Valid {
				max = i
			}
		}
	}
	/*io := StockReportPeriodIO{Code: c.Id, AutoCalRate: rules[max].Rate, ReportPeriod: nowReportPeriod}
	  if rules[max].Rate.Valid {
	  	io.AutoRateRule = rules[max].RuleName
	  }*/
	var autoRateRule string
	var maxRate decimal.NullDecimal
	lastYearNetProfit, _ := lastNetProfit(code, offset-3)
	if !lastYearNetProfit.Valid || lastYearNetProfit.Decimal.Cmp(decimal.Zero) == 0 {
		maxRate = decimal.NullDecimal{}
	} else {
		maxRate = decimal.NullDecimal{
			Decimal: calculateRate(rules[max].Rate.Decimal, lastYearNetProfit.Decimal),
			Valid:   lastYearNetProfit.Valid && rules[max].Rate.Valid,
		}
	}
	if maxRate.Valid {
		autoRateRule = rules[max].RuleName
	}
	return maxRate, autoRateRule
}

//offset 报告期偏移量
func AutoCalculateRate(offset int) {
	logging.Info("业绩计算脚本开始")
	autoCalRules := [4][]AutoCalRule{
		//一季度
		{
			{
				Rule:     qOneRule1,
				RuleName: "过去一年1季度业绩*（1+过去一年一季度业绩增速）",
			},
			{
				Rule:     qOneRule2,
				RuleName: "过去三年一季度净利润相对去年全年净利润的比例来计算*券商最近两个月年度预测净利润均值",
			},
			{
				Rule:     qOneRule3,
				RuleName: "券商全年的4分之一",
			},
		},
		//二季度
		{
			{
				Rule:     qTwoRule1,
				RuleName: "一季度业绩*2",
			},
			{
				Rule:     qTwoRule2,
				RuleName: "券商全年-一季度）/3+一季度",
			},
			{
				Rule:     qTwoRule3,
				RuleName: "券商全年/2",
			},
		},
		//三季度
		{
			{
				Rule:     qThreeRule1,
				RuleName: "中报业绩+（券商最近两个月预测年度净利润平均值-中报业绩）/2",
			},
			{
				Rule:     qThreeRule2,
				RuleName: "过去三年单季度净利润占去年全年比例*券商最近两个月年度预测净利润均值+半年度业绩",
			},
			{
				Rule:     qThreeRule3,
				RuleName: "券商全年/4*3",
			},
		},
		//四季度
		{
			{
				Rule:     qFourRule,
				RuleName: "券商最近两个月预测年度净利润平均值",
			},
		},
	}
	type code struct {
		Id string `json:"id"`
	}
	codeList := make([]code, 0)
	err := models.DB().Table("b_stock").Where("is_b=0 AND end_date>?", time.Now()).Find(&codeList).Error
	if err != nil {
		logging.Error("AutoCalculateRate() 获取code列表失败")
	}
	nowReportPeriod, q := util.ReportPeriod(offset)
	rules := autoCalRules[q-1]
	for _, c := range codeList {
		var maxRate decimal.NullDecimal
		var autoRateRule string
		maxRate, autoRateRule = forwardCalculate(c.Id, offset, q)
		if !maxRate.Valid {
			maxRate, autoRateRule = normalCalculate(c.Id, rules, offset)
		}
		models.DB().Exec("INSERT INTO p_stock_report_period (code,auto_cal_rate,auto_rate_rule,report_period)VALUES(?,?,?,?) ON DUPLICATE KEY UPDATE auto_cal_rate=VALUES(auto_cal_rate),auto_rate_rule=VALUES(auto_rate_rule) ", c.Id, maxRate, autoRateRule, nowReportPeriod)
		//models.DB().Assign(io).Where("code=? AND report_period=?", io.Code, io.ReportPeriod).FirstOrCreate(&StockReportPeriodIO{})
	}

}

func AutoCalculateRateSet() {
	if time.Now().Format(util.MD) > "01-01" && time.Now().Format(util.MD) < "04-30" {
		AutoCalculateRate(-1) //1月1号就展示一季报但是上年年报还是要用
	}
	AutoCalculateRate(0)
}

func StockReportPeriodSet() {
	if time.Now().Format(util.MD) > "01-01" && time.Now().Format(util.MD) < "04-30" {
		StockReportPeriodData(-1) //1月1号就展示一季报但是上年年报还是要用
	}
	StockReportPeriodData(0)
}

func SetStockIndustry() {
	spacial := map[string]struct{}{
		"002129": {},
		"002202": {},
		"002218": {},
		"002459": {},
		"002506": {},
		"002531": {},
		"300111": {},
		"300118": {},
		"300129": {},
		"300185": {},
		"300274": {},
		"300316": {},
		"300317": {},
		"300393": {},
		"300443": {},
		"300569": {},
		"300593": {},
		"300763": {},
		"300772": {},
		"300827": {},
		"300850": {},
		"600207": {},
		"600438": {},
		"600537": {},
		"601012": {},
		"601218": {},
		"601615": {},
		"601908": {},
		"603105": {},
		"603212": {},
		"603218": {},
		"603396": {},
		"603507": {},
		"603806": {},
		"688390": {},
		"688408": {},
		"688516": {},
		"688599": {},
	}
	var industryList []map[string]string
	err := mongo.GetMongoDB("shares", "F10_companyInfo", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{}).Select(bson.M{"code": 1, "industry": 1}).All(&industryList)
	})
	if err != nil {
		logging.Error("mongo 获取行业信息报错：", err.Error())
	}
	for _, line := range industryList {
		if code, ok := line["code"]; ok {
			if industry, ok := line["industry"]; ok {
				industry := strings.Split(strings.TrimRight(industry, " "), " ")
				actual := industry[0]
				if _, ok := spacial[code]; ok {
					actual = "光伏风电"
				}
				models.DB().Exec("INSERT INTO u_stock_industry (code,first_industry,second_industry,actual_industry) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE first_industry=VALUES(first_industry),second_industry=VALUES(second_industry),actual_industry=VALUES(actual_industry)", code, industry[0], industry[len(industry)-1], actual)
			} else {
				logging.Error(code + "SetStockIndustry() industry mongo获取失败")
			}
		}
	}

}

func InvestigationFallBack() {
	dtos := make([]spider.InvestigationDTO, 0)
	err := models.DB().Table("p_investigation").Where("inc_sum is NULL").Find(&dtos).Error
	if err != nil {
		logging.Error("获取调研纪要空回测数据失败")
		return
	}
	var wg sync.WaitGroup
	wg.Add(len(dtos))
	for i, _ := range dtos {
		go func(i int) {
			defer wg.Done()
			async.GetChangeAndFallback(&dtos[i])
		}(i)
	}
	wg.Wait()
}

type StockNames struct {
	models.Simple
	Code string `json:"code" gorm:"code"`
	Name string `json:"name" gorm:"name"`
}

func (StockNames) TableName() string {
	return "b_stock_names"
}

//更新最新的股票信息
func UpdateStockNames() {
	redisKey := "stock:name"
	ret, err := gredis.Clone(setting.RedisSetting.StockDB).HGetAllStringMap(redisKey)
	if err != nil {
		logging.Info("UpdateStockNames() get stock from redis err ", err)
		return
	}
	if len(ret) < 1 {
		logging.Info("UpdateStockNames() get stock from redis err", ret)
		return
	}
	stockList := []StockNames{}
	err = models.DB().Table("b_stock_names").Find(&stockList).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		logging.Info("get b_stock_names.stocks() get stock from b_stock_names table  err", ret)
		return
	}
	var (
		retCodes   []string
		stockCodes []string
	)
	for index, _ := range ret {
		retCodes = append(retCodes, index)
	}
	for _, value := range stockList {
		stockCodes = append(stockCodes, value.Code)
	}
	//redis里面有股票表里面没包含的添加
	for index, value := range ret {
		insertCodes := StockNames{
			Code: index,
			Name: value,
		}
		_ = models.DB().Table("b_stock_names").Where(&StockNames{
			Code: index,
		}).Assign(insertCodes).FirstOrCreate(&StockNames{}).Error

	}
	//数据表有redis里面没有包含的就删除
	for _, value := range stockList {
		if len(retCodes) != 0 && !util.ContainsAny(retCodes, value.Code) {
			err = models.DB().Table("b_stock_names").Where("code = ? ", value.Code).Delete(StockNames{}).Error
			if err != nil {
				logging.Info("insert b_stock_names err", err, value.Code)
			}
		}

	}
	return
}

func SetImportantNearly60Days() {
	dtos := make([]spider.URR, 0)
	err := models.DB().
		Select("id,code,organization,author").
		Where("type=?", "个股").
		Where("parent=0").
		Where("created_at>=?", time.Now().AddDate(0, 0, -60)).
		Find(&dtos).Error
	if err != nil {
		logging.Error("重点券商存量数据获取失败", err.Error())
	}
	for _, dto := range dtos {
		if spider.CheckImportantOrg(&dto) {
			t := "重点券商"
			err = models.DB().Table("u_research_report").
				Where("id=?", dto.ID).
				Update(spider.URR{
					Type: &t,
				}).Error
			if err != nil {
				logging.Error("重点券商标记失败")
			}
		}
	}
}

func CopyNewCode2BStock() {
	err := models.DB().Exec("insert into b_stock (id, name, start_date, created,end_date,is_b) select sn.code id, sn.name, sn.created_at start_date, now() created,'2200-01-01' end_date,if(sn.name like '%B%',1,0) is_b from b_stock_names sn left join b_stock bs on sn.code = bs.id " + fmt.Sprintf(" where bs.id is null or sn.updated_at>='%s' ON DUPLICATE KEY UPDATE name=values(name)", time.Now().Format(util.YMD))).Error
	if err != nil {
		logging.Info("insert into b_stock (id, name, start_date, created,end_date,is_b) select sn.code id, sn.name, sn.created_at start_date, now() created,'2200-01-01' end_date,if(sn.name like '%B%',1,0) is_b from b_stock_names sn left join b_stock bs on sn.code = bs.id " + fmt.Sprintf(" where bs.id is null or sn.updated_at>='%s' ON DUPLICATE KEY UPDATE name=values(name)", time.Now().Format(util.YMD)))
		logging.Error("拷贝新股到b_stock报错", err.Error())
	}
}

func CopyNewCode2BStockFromSharesInfo() {
	err := models.DB().Exec("insert into b_stock (id, name, start_date, created,end_date,is_b) select left(si.code,6) id, si.name, si.start_date, si.created_at created,if(si.end_date='None','2200-01-01',si.end_date) end_date,if(si.name like '%B%',1,0) is_b from u_shares_info si left join b_stock bs on left(si.code,6) = bs.id where bs.id is null").Error
	if err != nil {
		logging.Info("insert into b_stock (id, name, start_date, created,end_date,is_b) select left(si.code,6) id, si.name, si.start_date, si.created_at created,if(si.end_date='None','2200-01-01',si.end_date) end_date,if(si.name like '%B%',1,0) is_b from u_shares_info si left join b_stock bs on left(si.code,6) = bs.id where bs.id is null")
		logging.Error("sharesinfo拷贝新股到b_stock报错", err.Error())
	}
}

func SetPreVol2Redis() {
	type codeVolStruct struct {
		StockCode string `json:"stock_code"`
		TradeDate string `json:"trade_date"`
		Amount    string `json:"amount"`
	}
	var lastTradeDate codeVolStruct
	err := models.DB().Select("Max(trade_date) trade_date").Table("p_stock_tick").First(&lastTradeDate).Error
	if err != nil {
		logging.Error("获取上个交易日失败", err.Error())
	}
	codeVols := make([]codeVolStruct, 0)
	err = models.DB().Table("p_stock_tick").Where("trade_date=?", lastTradeDate.TradeDate).Find(&codeVols).Error
	if err != nil {
		logging.Error("获取上个交易日股票信息失败", err.Error())
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, v := range codeVols {
		err := conn.HSet("stock:pre_amount", v.StockCode, v.Amount)
		if err != nil {
			logging.Error("设置redis "+v.StockCode+"上个交易日成交量失败", err.Error())
		}
	}

}

func PerformanceForecastPublishAlert() {
	nowTime := time.Now()
	timePoint := []time.Time{
		time.Date(nowTime.Year(), 1, 31, 0, 0, 0, 0, time.Local),
		time.Date(nowTime.Year(), 4, 15, 0, 0, 0, 0, time.Local),
		time.Date(nowTime.Year(), 7, 15, 0, 0, 0, 0, time.Local),
		time.Date(nowTime.Year(), 10, 15, 0, 0, 0, 0, time.Local),
		//time.Date(time.Now().Year()+1,1,31,0,0,0,0,time.Local),
	}
	offset := 0
	if time.Now().Format(util.MD) >= "01-01" && time.Now().Format(util.MD) <= "01-31" {
		offset = offset - 1
	}
	nowRportPeriod, _ := util.ReportPeriod(offset)
	lastRportPeriod, _ := util.ReportPeriod(offset - 1)
	codeList := make([]string, 0)
	codes := make([]string, 0)
	tradeDateList := make([]time.Time, 0)
	rateLimit := 50
	var amountLimit float64 = 100000 //千元
	for pointNum, day := range timePoint {
		if day.After(nowTime) {
			err := models.DB().Table("b_trade_date").Select("trade_date").
				Where("trade_date<?", day).
				Order("trade_date desc").
				Limit(15).Pluck("trade_date", &tradeDateList).Error
			if err != nil {
				logging.Error("performanceForecastPublishAlert获取交易日失败")
				return
			}
			for i, v := range tradeDateList {
				//时间点前 1 3 5 15个交易日
				if v.Before(nowTime) && v.AddDate(0, 0, 1).After(nowTime) && (i == 0 || i == 2 || i == 4 || i == 14) {
					err := models.DB().Table("u_performance_announcement").
						Joins("LEFT JOIN b_stock ON b_stock.id=u_performance_announcement.code").
						Where("net_profit_growth_rate>?", rateLimit).
						Where("report_period=?", lastRportPeriod).
						Where("b_stock.name not like '%ST%'").
						Pluck("u_performance_announcement.code", &codeList).Error
					if err != nil {
						logging.Error("performanceForecastPublishAlert 获取上季度利润信息失败")
						return
					}
					if pointNum == 0 {
						for _, code := range codeList {
							if checkPreVol(code, amountLimit) && (strings.HasPrefix(code, "60") || strings.HasPrefix(code, "000") || strings.HasPrefix(code, "300") || (strings.HasPrefix(code, "00") && !strings.HasPrefix(code, "000")) || strings.HasPrefix(code, "688")) {
								codes = append(codes, code)
							}
						}
					} else {
						for _, code := range codeList {
							if checkPreVol(code, amountLimit) && (strings.HasPrefix(code, "000") || strings.HasPrefix(code, "00") && !strings.HasPrefix(code, "000")) {
								codes = append(codes, code)
							}
						}
					}
					if len(codes) > 0 {
						codeNameList := make([]Perf, 0)
						err := models.DB().Table("b_stock").Select("b_stock.id code,b_stock.name, u_performance_edit.forecast_proportion, u_performance_edit.forecast_net_profit, u_performance_edit.high_lights, u_performance_edit.broker_proportion,u_performance_forecast.report_period,u_performance_forecast.disclosure_time").
							Joins("LEFT JOIN u_performance_forecast ON b_stock.id=u_performance_forecast.code AND u_performance_forecast.report_period=?", nowRportPeriod).
							Joins("LEFT JOIN u_performance_edit ON u_performance_edit.code=b_stock.id AND u_performance_edit.report_period=?", nowRportPeriod).
							Where("u_performance_forecast.report_period is NULL OR u_performance_forecast.publish_date IS NULL").
							Where("b_stock.id IN (?)", codes).
							Find(&codeNameList).Error
						if err != nil {
							logging.Error("performanceForecastPublishAlert 获取未预告code列表失败")
						}
						if len(codeNameList) > 0 {
							push(map[string]interface{}{
								"type":             "pre_forecast_alert",
								"day":              i + 1,
								"data":             codeNameList,
								"highly_recommend": true,
							}, message.Message_System_Performance)
						}
					} else {
						return
					}
				}
			}
		}
	}

}

func PrePerformanceRate() {
	maxDayLimit := 15
	minDayLimit := 5
	tradeDateList := make([]time.Time, 0)
	err := models.DB().Table("b_trade_date").Select("trade_date").
		Where("trade_date>?", time.Now()).
		Order("trade_date asc").
		Limit(maxDayLimit).Pluck("trade_date", &tradeDateList).Error
	if err != nil {
		logging.Error("PrePerformanceRate 获取未来交易日失败", err.Error())
		return
	}
	if len(tradeDateList) < maxDayLimit {
		logging.Warn("交易日表b_trade_date待更新")
	}
	startTime := ""
	endTime := ""
	for i, v := range tradeDateList {
		if i == minDayLimit-1 {
			startTime = v.Format(util.YMD) + " 00:00:00"
		}
		if i == maxDayLimit-1 {
			endTime = v.Format(util.YMD) + " 23:59:59"
		}
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	preAmountMap, err := conn.HGetAllStringMap("stock:pre_amount")
	codeList := make([]string, 0)
	for k, v := range preAmountMap {
		am, err := strconv.ParseFloat(v, 64)
		if err == nil && am >= 200000 {
			codeList = append(codeList, k)
		}
	}
	//startTime:=time.Now().AddDate(0,0,5).Format(util.YMD)+" 00:00:00"
	//endTime:=time.Now().AddDate(0,0,15).Format(util.YMD)+" 23:59:59"
	if len(codeList) > 0 {
		type disclosureTime struct {
			Code           string          `json:"code"`
			Name           string          `json:"name"`
			DisclosureTime *time.Time      `json:"disclosure_time"`
			Rate           decimal.Decimal `json:"rate"`
			ReportPeriod   time.Time       `json:"report_period"`
		}
		//获取报告期
		reportPeriod, season := util.ReportPeriod(0)
		dbs := models.DB().Table("u_performance_forecast").
			Select("code,name,disclosure_time,report_period").
			Where("disclosure_time>=?", startTime).
			Where("disclosure_time<=?", endTime)
		if season == 1 {
			preReportPeriod, _ := util.ReportPeriod(-1)
			dbs = dbs.Where("report_period=? OR report_period=?", reportPeriod, preReportPeriod)
		} else {
			dbs = dbs.Where("report_period=?", reportPeriod)
		}
		forecastList := make([]disclosureTime, 0)
		err := dbs.Where("code IN (?)", codeList).
			Find(&forecastList).Error
		if err != nil {
			logging.Error("PrePerformanceRate 获取预告信息失败", err.Error())
			return
		}
		for _, v := range forecastList {
			if v.DisclosureTime == nil {
				logging.Error(v.Code + "业绩预告披露时间缺失")
				continue
			}
			startCountTime := getTradeDateTime(*v.DisclosureTime, -maxDayLimit)
			rate := getSumRate(v.Code, startCountTime, time.Now())
			if rate.Cmp(decimal.NewFromFloat32(20)) >= 0 {
				v.Rate = rate.Round(2)
				tList := make([]disclosureTime, 0)
				err := models.DB().Table("p_stock_report_period").
					Where("code=?", v.Code).
					Where("report_period=?", v.ReportPeriod).
					Where("rate_push_time IS NULL").Find(&tList).Error
				if err != nil {
					logging.Error("PrePerformanceRate 获取推送记录失败", err.Error())
				}
				if len(tList) != 0 {
					push(map[string]interface{}{
						"type":            "pre_forecast_rate",
						"code":            v.Code,
						"name":            v.Name,
						"disclosure_time": util.TimePtr2String(v.DisclosureTime, util.YMD),
						"rate":            v.Rate.String(),
						"report_period":   v.ReportPeriod.Format(util.YMD),
						//"highly_recommend": true,
					}, message.Message_System_Performance)
					err = models.DB().Table("p_stock_report_period").
						Where("code=?", v.Code).
						Where("report_period=?", reportPeriod).
						UpdateColumn("rate_push_time", time.Now()).Error
					if err != nil {
						logging.Error("PrePerformanceRate 更新推送记录失败")
					}
				}
			}
		}

	} else {
		return
	}

}

func checkPreVol(code string, limit float64) bool {
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	vol, err := conn.HGetFloat64("stock:pre_amount", code)
	if err != nil {
		logging.Error("redis "+code+" 获取前一交易日成交额失败", err.Error())
		return false
	}
	return vol >= limit
}

//day 前后多少个交易日 <0 前 >0后
func getTradeDateTime(startTime time.Time, day int) time.Time {
	if day == 0 {
		return time.Time{}
	}
	tradeDateList := make([]time.Time, 0)
	dbs := models.DB().Table("b_trade_date").Select("trade_date")
	if day > 0 {
		dbs = dbs.Where("trade_date>?", startTime).
			Order("trade_date asc")
	} else {
		dbs = dbs.Where("trade_date<?", startTime).
			Order("trade_date desc")
		day = -day
	}
	err := dbs.Limit(day).Pluck("trade_date", &tradeDateList).Error
	if err != nil {
		logging.Error("getTradeDateTime 交易日失败", err.Error())
		return time.Time{}
	}
	if len(tradeDateList) < day {
		logging.Error("getTradeDateTime 超范围获取交易日")
		return time.Time{}
	}
	return tradeDateList[day-1]
}

func getSumRate(code string, startTime time.Time, endTime time.Time) decimal.Decimal {
	type tick struct {
		StockCode string          `json:"stock_code"`
		TradeDate string          `json:"trade_date"`
		PreClose  decimal.Decimal `json:"pre_close"`
		Close     decimal.Decimal `json:"close"`
	}
	tickList := make([]tick, 0)
	err := models.DB().Table("p_stock_tick").
		Where("stock_code=?", code).
		Where("trade_date>=?", startTime).
		Where("trade_date<?", endTime).
		Order("trade_date asc").
		Find(&tickList).Error
	if err != nil {
		logging.Error(code + "获取" + startTime.Format(util.YMD) + "到" + endTime.Format(util.YMD) + "失败")
		return decimal.Decimal{}
	}
	if len(tickList) < 1 || tickList[0].PreClose.Cmp(decimal.Zero) == 0 {
		return decimal.Decimal{}
	}
	return tickList[len(tickList)-1].Close.Sub(tickList[0].PreClose).Mul(decimal.NewFromInt32(100)).Div(tickList[0].PreClose.Abs())
}

type amountSum struct {
	Code      string          `json:"code"`
	AmountSum decimal.Decimal `json:"amount_sum"`
}

type industryInfo struct {
	SecondIndustry string            `json:"second_industry"`
	FirstIndustry  string            `json:"first_industry"`
	StockCount     int               `json:"stock_count"`
	Rate           decimal.Decimal   `json:"rate"`
	StockList      []amountRankStock `json:"stock_list"`
	Amount         decimal.Decimal   `json:"amount"`
}

type amountRankStock struct {
	Code           string              `json:"code"`
	Name           string              `json:"name"`
	FirstIndustry  string              `json:"first_industry"`
	SecondIndustry string              `json:"second_industry"`
	StockQrr       decimal.Decimal     `json:"stock_qrr"`
	PctChange      decimal.NullDecimal `json:"pct_change"` //涨幅
	Amount         decimal.Decimal     `json:"amount"`
	Rank           int                 `json:"rank"`
}

func AmountRankMonitor() {
	endTime := "15:15:00"
	if time.Now().Format(util.HMS) < "09:45:00" || time.Now().Format(util.HMS) > endTime {
		return
	}
	logging.Info("开始成交额前200监控 port:" + strconv.Itoa(setting.ServerSetting.HttpPort))
	predays := 3
	preFilterDayRange := 20
	preFilterDay := 18
	tradeDayList := make([]time.Time, 0)
	bandCodeList := make([]string, 0)
	blockStockList := make([]string, 0)
	bandCodeMap := map[string]struct{}{}
	err := models.DB().Table("b_trade_date").
		Where("trade_date<=?", time.Now()).
		Limit(preFilterDayRange+1).
		Order("id desc").
		Pluck("trade_date", &tradeDayList).Error
	if err != nil {
		logging.Error("AmountRankMonitor获取交易日失败", err.Error())
		return
	}
	lastTradeDayOffset := 1
	if tradeDayList[0].Format(util.YMD) < time.Now().Format(util.YMD) {
		//非交易日退出
		return
	}
	lastTradeDay := tradeDayList[lastTradeDayOffset]
	sql := "select stock_code from" +
		"(select stock_code,amount,row_number() over(partition by trade_date order by amount desc) as n " +
		"from p_stock_tick where trade_date IN (?) and stock_code!='' ) a where n<=200 group by stock_code having count(stock_code)>=?"
	err = models.DB().Raw(sql, tradeDayList[lastTradeDayOffset:preFilterDayRange], preFilterDay).Pluck("code", &bandCodeList).Error
	if err != nil {
		logging.Error("AmountRankMonitor 获取屏蔽高频股票列表失败")
	}
	err = models.DB().Table("p_stock_tick").Where("trade_date=? and is_block=1", lastTradeDay).Pluck("stock_code", &blockStockList).Error
	if err != nil {
		logging.Error("AmountRankMonitor mysql 获取"+lastTradeDay.Format(util.YMD)+"涨停股票失败", err.Error())
	}
	bandCodeList = append(bandCodeList, blockStockList...)
	for _, v := range bandCodeList {
		bandCodeMap[v] = struct{}{}
	}
	rankRange := 200
	industryTop := 3 //行业前3
	//stockTop := 3            //行业内前3股票
	stockLimit := 2          //独立行业股票个数限定
	rateLimit := float32(3)  //并入其他百分比限定 3% 新进个股涨幅限制
	qrrLimit := float32(1.5) //新进个股量比限制
	amountRankStockList := make([]amountRankStock, 0)
	preAmountRankStockList := make([]amountRankStock, 0)
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	codeList := make([]string, 0)
	amountList := make([]decimal.Decimal, 0)
	amountSumList := make([]amountSum, 0)
	industryMap := map[string]industryInfo{}
	industryList := make([]industryInfo, 0)
	industryLessList := make([]industryInfo, 0)
	amountTotalSum := decimal.Zero
	pushStocks := make([]amountRankStock, 0)
	nowRankStocks := make([]amountRankStock, 0)
	/*timeArea := [][2]string{
		{"09:45:00", "10:15:00"},
		{"11:45:00", "12:15:00"},
		{"14:45:00", "15:15:00"},
	}*/
	codeAmountList, err := conn.ZRange("stock:zset:amount", -rankRange, -1, true)
	if err != nil {
		logging.Error("stock:zet:amount获取成交额前200股票失败")
		return
	}
	for i, v := range codeAmountList {
		if i%2 == 1 {
			amount, err := decimal.NewFromString(v)
			if err != nil {
				logging.Error("code:"+codeAmountList[i-1]+"成交量:"+v+"解析失败", err.Error())
				amount = decimal.Zero
			}
			amountList = append(amountList, amount)
		} else {
			codeList = append(codeList, v)
		}
	}
	err = models.DB().Table("u_stock_industry I").
		Select("I.code,S.name,I.first_industry,I.second_industry").
		Joins("LEFT JOIN b_stock S ON S.id=I.code").
		Where("I.code IN (?)", codeList).
		//Where("I.code NOT IN (?)",bandCodeList).
		Find(&amountRankStockList).Error
	if err != nil {
		logging.Error("获取成交额前200股票基本信息失败")
		return
	}
	dbs := models.Data().Table("p_stock_minute FORCE INDEX(IDX_UNION)").Select("code,sum(amount) as amount_sum").Where("code IN (?)", codeList)
	where := ""
	for i := lastTradeDayOffset; i <= predays; i++ {
		where = where + fmt.Sprintf("(time>'%s' AND time<'%s')", tradeDayList[i].Format(util.YMD)+" 09:30:00", tradeDayList[i].Format(util.YMD)+" "+time.Now().Format("15:04")+":00")
		if i < predays {
			where = where + " OR "
		}
	}
	err = dbs.Where(where).Group("code").Find(&amountSumList).Error
	if err != nil {
		logging.Error("获取过去"+fmt.Sprintf("%d", predays)+"天交易量失败", err.Error())
	}
	for i, _ := range amountRankStockList {
		//股票成交额
		for j, amount := range amountList {
			if amountRankStockList[i].Code == codeList[j] {
				amountRankStockList[i].Amount = amount
				amountRankStockList[i].Rank = rankRange - j
			}
			if i == 0 {
				amountTotalSum = amountTotalSum.Add(amount)
			}
		}
		//股票量比
		for _, amountSum := range amountSumList {
			if amountRankStockList[i].Code == amountSum.Code {
				amountRankStockList[i].StockQrr = amountRankStockList[i].Amount.Mul(decimal.NewFromInt(int64(predays))).Div(amountSum.AmountSum)
			}
		}
		//股票涨幅
		pecChange, err := conn.HGetString("stock:change", amountRankStockList[i].Code)
		if err != nil {
			logging.Error("获取redis"+amountRankStockList[i].Code+"涨幅失败", err.Error())
		} else {
			err = amountRankStockList[i].PctChange.Scan(pecChange)
			if err != nil {
				logging.Error("解析"+pecChange+"为"+amountRankStockList[i].Code+"涨幅失败", err.Error())
			}
		}
		//行业信息统计
		if stock, ok := industryMap[amountRankStockList[i].SecondIndustry]; ok {
			stock.Amount = stock.Amount.Add(amountRankStockList[i].Amount)
			stock.StockCount++
			stock.FirstIndustry = amountRankStockList[i].FirstIndustry
			stock.SecondIndustry = amountRankStockList[i].SecondIndustry
			stock.StockList = append(stock.StockList, amountRankStockList[i])
			industryMap[amountRankStockList[i].SecondIndustry] = stock
		} else {
			stock.Amount = amountRankStockList[i].Amount
			stock.StockCount = 1
			stock.FirstIndustry = amountRankStockList[i].FirstIndustry
			stock.SecondIndustry = amountRankStockList[i].SecondIndustry
			stock.StockList = append(stock.StockList, amountRankStockList[i])
			industryMap[amountRankStockList[i].SecondIndustry] = stock
		}
	}
	//前交易日股票和判断新进股票 前交易日行业和新进行业判断
	byteList, err := gredis.Get("amount_rank:stock_list" + lastTradeDay.Format("20060102"))
	if err != nil {
		logging.Error("AmountRankMonitor redis 获取"+lastTradeDay.Format(util.YMD)+"成交额前200行业和股票失败", err.Error())
		err = models.DB().Table("p_stock_tick").
			Select("stock_code code,amount,first_industry,second_industry ").
			Joins("left join u_stock_industry usi on p_stock_tick.stock_code = usi.code").
			Where("trade_date=? and stock_code!=''", lastTradeDay).
			Order("amount desc").
			Limit(rankRange).
			Find(&preAmountRankStockList).Error
		if err != nil {
			logging.Error("AmountRankMonitor mysql 获取"+lastTradeDay.Format(util.YMD)+"成交额前200行业和股票失败", err.Error())
		}
	} else {
		err = json.Unmarshal(byteList, &preAmountRankStockList)
		if err != nil {
			logging.Error("AmountRankMonitor 解析"+lastTradeDay.Format(util.YMD)+"日数据报错", err.Error())
			err = models.DB().Table("p_stock_tick").
				Select("stock_code code,amount,first_industry,second_industry ").
				Joins("left join u_stock_industry usi on p_stock_tick.stock_code = usi.code").
				Where("trade_date=? and stock_code!=''", lastTradeDay).
				Order("amount desc").
				Limit(rankRange).
				Find(&preAmountRankStockList).Error
			if err != nil {
				logging.Error("AmountRankMonitor mysql 获取"+lastTradeDay.Format(util.YMD)+"成交额前200行业和股票失败", err.Error())
			}
		}
	}
	/*type stock struct {
		Code string `json:"code"`
		Name string `json:"name"`
	}*/
	pushStock := make([]string, 0) //make([]stock, 0)
	//newStockCount:=0
	for _, v := range amountRankStockList {
		repeatIndustry := false
		repeatStock := false
		for _, p := range preAmountRankStockList {
			if v.FirstIndustry == p.FirstIndustry {
				repeatIndustry = true
			}
			if v.Code == p.Code {
				repeatStock = true
			}
		}
		if !repeatIndustry {
			if industryMap[v.SecondIndustry].StockCount > stockLimit {
				//行业一天一次
				if !gredis.Exists("amount_rank_once:industry:" + v.SecondIndustry) {
					logging.Info("成交额前200 行业推送")
					push(map[string]interface{}{
						"type":    "amount_rank_industry",
						"content": v.SecondIndustry + "行业今日成交额较大",
					}, message.Message_Stock)
					_ = gredis.Set("amount_rank_once:industry:"+v.SecondIndustry, time.Now().Format(util.YMDHMS), 60*60*10)
				}
			}
		} else {
			if !repeatStock && v.StockQrr.GreaterThan(decimal.NewFromFloat32(qrrLimit)) && v.PctChange.Valid && v.PctChange.Decimal.GreaterThan(decimal.NewFromFloat32(rateLimit)) {
				//去除高频个股
				if _, ok := bandCodeMap[v.Code]; !ok {
					nowRankStocks = append(nowRankStocks, v)
					//股票一天一次
					if !gredis.HExists("amount_rank_once:stock", v.Code) {
						pushStock = append(pushStock, v.Name)
						pushStocks = append(pushStocks, v)
						//newStockCount++
						if !gredis.Exists("amount_rank_once:stock") {
							data, _ := json.Marshal(v)
							_ = gredis.HSet("amount_rank_once:stock", v.Code, data)
							gredis.Expire("amount_rank_once:stock", 10*60*60)
						} else {
							data, _ := json.Marshal(v)
							_ = gredis.HSet("amount_rank_once:stock", v.Code, data)
						}
					}
				}
			}
		}
	}

	//存储今日行业和股票
	if time.Now().Format(util.HMS) >= endTime {
		err := gredis.Set("amount_rank:stock_list"+time.Now().Format("20060102"), amountRankStockList, 7*24*60*60)
		if err != nil {
			logging.Error("AmountRankMonitor 存储今日成交额前200股票和行业失败")
		}
	}

	//行业合并
	for _, industry := range industryMap {
		if industry.StockCount <= stockLimit {
			industryLessList = append(industryLessList, industry)
		} else {
			industryList = append(industryList, industry)
		}
	}
	if len(industryList) == 0 {
		industryList = industryLessList
	} else {
		sort.Slice(industryList, func(i, j int) bool {
			return !industryList[i].Amount.LessThan(industryList[j].Amount)
		})
		length := len(industryList)
		for _, l := range industryLessList {
			insert := true
			for j := 0; j < length; j++ {
				if l.FirstIndustry == industryList[j].FirstIndustry {
					industryList[j].StockList = append(industryList[j].StockList, l.StockList...)
					industryList[j].Amount = industryList[j].Amount.Add(l.Amount)
					industryList[j].StockCount = industryList[j].StockCount + l.StockCount
					insert = false
					break
				}
			}
			if insert {
				industryList = append(industryList, l)
			}
		}
	}
	sort.Slice(industryList, func(i, j int) bool {
		return !industryList[i].Amount.LessThan(industryList[j].Amount)
	})

	//定时推送
	//for _, times := range timeArea {
	//if time.Now().Format(util.HMS) > times[0] && time.Now().Format(util.HMS) < times[1] {
	if time.Now().Format(util.HMS) >= endTime {
		/*pushStocks = []amountRankStock{}
		  pushStocksTmp, _ := gredis.HGetAllStringMap("amount_rank_once:stock")
		  for _, stock := range pushStocksTmp {
		  	v := amountRankStock{}
		  	err = json.Unmarshal([]byte(stock), &v)
		  	if err != nil {
		  		logging.Error("AmountRankMonitor json解析amountRankStock:"+stock+"失败", err.Error())
		  	} else {
		  		pushStocks = append(pushStocks, v)
		  	}
		  }*/
		pushStocks = nowRankStocks
	}
	if len(pushStock) <= 3 && time.Now().Format(util.HMS) < endTime {
		//15点及以后的不再推送
		if time.Now().Format(util.HMS) > "15:00:00" {
			return
		}
		if len(pushStock) > 0 {
			//for _, v := range pushStock {
			logging.Info("成交额前200 推送个股")
			push(map[string]interface{}{
				"type":    "amount_rank_stock",
				"content": "新进成交额前200",
				"stocks":  pushStock,
			}, message.Message_Stock)
			//}
		}
	} else {
		//没有新进个股 不推
		if len(pushStocks) == 0 {
			return
		}
		//行业占比
		if amountTotalSum == decimal.Zero {
			logging.Error("前200总成交额异常")
		} else {
			other := 0
			for i, industry := range industryList {
				industryList[i].Rate = industry.Amount.Mul(decimal.NewFromInt32(100)).Div(amountTotalSum).Round(2)
				//小于3%行业显示为其他
				if industryList[i].Rate.LessThan(decimal.NewFromFloat32(rateLimit)) {
					if other == 0 {
						other = i
					}
					if i == other {
						industryList[other].SecondIndustry = "其他"
					} else {
						industryList[other].StockList = append(industryList[other].StockList, industryList[i].StockList...)
						industryList[other].StockCount = industryList[other].StockCount + industryList[i].StockCount
						industryList[other].Amount = industryList[other].Amount.Add(industryList[i].Amount)
						industryList[other].Rate = industryList[other].Rate.Add(industryList[i].Rate)
					}
				} else {
					sort.Slice(industryList[i].StockList, func(j, k int) bool {
						if industryList[i].StockList[j].PctChange.Decimal.Cmp(industryList[i].StockList[k].PctChange.Decimal) == 0 {
							return !industryList[i].StockList[j].Amount.LessThan(industryList[i].StockList[k].Amount)
						} else {
							return !industryList[i].StockList[j].PctChange.Decimal.LessThan(industryList[i].StockList[k].PctChange.Decimal)
						}

					})
				}
			}
			if len(industryList) < industryTop {
				logging.Error("AmountRankMonitor行业统计异常")
				return
			}
			/*for _, industry := range industryList[:util.Min(industryTop, len(industryList))] {
				for _, stock := range industry.StockList[:util.Min(stockTop, len(industry.StockList))] {
					if stock.StockQrr.LessThan(decimal.NewFromInt(2)) {
						pushStocks = append(pushStocks, stock)
					}
				}
			}*/
			/*for _, stock := range amountRankStockList {
				if stock.StockQrr.Cmp(decimal.NewFromInt(2)) >= 0 {
					pushStocks = append(pushStocks, stock)
				}
			}*/
			if other == 0 {
				other = len(industryList) - 1
			}

			sort.Slice(pushStocks, func(i, j int) bool {
				return pushStocks[i].Rank < pushStocks[j].Rank
			})
			logging.Info("成交额前200推送列表")
			push(map[string]interface{}{
				"type":          "amount_rank_map",
				"industry_list": industryList[:other+1],
				"stock_list":    pushStocks,
			}, message.Message_Stock)
		}
	}
	//}
	//}

}

func YearNetProfitChangeUpSet() {
	YearNetProfitChangeUp(time.Now().AddDate(-1, 0, 0).Format("2006"))
	YearNetProfitChangeUp(time.Now().Format("2006"))
	YearNetProfitChangeUp(time.Now().AddDate(1, 0, 0).Format("2006"))
}

func YearNetProfitChangeUp(year string) {
	defer func() {
		if r := recover(); r != nil {
			logging.Error("Recovered in YearNetProfitChangeUp", r)
		}
	}()
	dayLimit := 40
	rateLimit := float32(15)
	var start string
	end := time.Now().Format(util.YMD)
	tradeDayList := make([]time.Time, 0)
	netProfitList := make([]map[string]interface{}, 0)
	bandCodeList := make([]string, 0)
	type pushMsg struct {
		Code         string          `json:"code"`
		Name         string          `json:"name"`
		Organization string          `json:"organization"`
		Author       string          `json:"author"`
		NetProfit    decimal.Decimal `json:"net_profit"`
		Rate         decimal.Decimal `json:"rate"`
	}
	type netProfitStruct struct {
		Code                string `json:"code"`
		LatestNetProfitList []pushMsg
		LatestPublishDate   string          `json:"latest_publish_date"`
		LatestCreatedAt     time.Time       `json:"latest_created_at"`
		PreCount            int             `json:"pre_count"`
		AvgNetProfit        decimal.Decimal `json:"avg_net_profit"`
		Organization        string          `json:"organization"`
		Author              string          `json:"author"`
	}
	netProfitStructMap := map[string]netProfitStruct{}
	err := models.DB().Table("b_trade_date").Where("trade_date<?", time.Now().Format(util.YMD)).Limit(dayLimit).Order("trade_date desc").Pluck("trade_date", &tradeDayList).Error
	if err != nil {
		logging.Error("YearNetProfitChangeUp 获取交易日失败")
		return
	}
	//近15天平均成交量小于等于1亿股票需剔除
	err = models.DB().Table("p_stock_tick").
		Where("stock_code is not null").
		Where("trade_date in (?)", tradeDayList).
		Group("stock_code").
		Having("avg(amount)<=?", 100000).
		Pluck("stock_code", &bandCodeList).Error
	if err != nil {
		logging.Error("YearNetProfitChangeUp 获取前15交易日日均成交额小于3亿股票失败", err.Error())
	}
	start = tradeDayList[len(tradeDayList)-1].Format(util.YMD)
	err = mongo.GetMongoDB("shares", "F10_mechanismForecast", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"publish_date": bson.M{"$gte": start, "$lte": end},
			"code":         bson.M{"$nin": bandCodeList},
		}).Sort("-code", "-publish_date").All(&netProfitList)
	})
	if err != nil {
		logging.Error("YearNetProfitChangeUp 获取预测净利润失败")
		return
	}
	for _, v := range netProfitList {
		if ok, _ := util.Getstring(v, year); ok {
			continue
		}
		data := decimal.Decimal{}
		err := data.Scan(v[year])
		if err != nil {
			logging.Warn("YearNetProfitChangeUp 解析净利润"+v["code"].(string)+"报错", err.Error())
			continue
		}

		if netProfit, ok := netProfitStructMap[v["code"].(string)]; ok {
			if netProfit.LatestPublishDate > v["publish_date"].(string) {
				netProfit.AvgNetProfit = netProfit.AvgNetProfit.Add(data)
				netProfit.PreCount++
			} else {
				netProfit.LatestNetProfitList = append(netProfit.LatestNetProfitList, pushMsg{
					Code:         v["code"].(string),
					Organization: v["organization_name"].(string),
					Author:       v["researcher"].(string),
					NetProfit:    data,
				})
			}
			if netProfit.LatestCreatedAt.Before(v["_id"].(bson.ObjectId).Time()) {
				netProfit.LatestCreatedAt = v["_id"].(bson.ObjectId).Time()
			}
			netProfitStructMap[v["code"].(string)] = netProfit
		} else {
			netProfitStructMap[v["code"].(string)] = netProfitStruct{
				Code: v["code"].(string),
				LatestNetProfitList: []pushMsg{
					{
						Code:         v["code"].(string),
						Organization: v["organization_name"].(string),
						Author:       v["researcher"].(string),
						NetProfit:    data,
					},
				},
				LatestPublishDate: v["publish_date"].(string),
				LatestCreatedAt:   v["_id"].(bson.ObjectId).Time(),
				Organization:      v["organization_name"].(string),
				Author:            v["researcher"].(string),
			}
		}
	}
	for _, v := range netProfitStructMap {
		if v.PreCount > 0 {
			v.AvgNetProfit = v.AvgNetProfit.Div(decimal.NewFromInt32(int32(v.PreCount)))
			avgValue := bson.M{}
			avg := decimal.Decimal{}
			err = mongo.GetMongoDB("shares", "F10_profitForecast", func(collection *mgo.Collection) error {
				return collection.Find(bson.M{
					"particular_year": year,
					"code":            v.Code,
				}).Select(bson.M{"_id": 0, "avg_value": 1}).One(&avgValue)
			})
			if err == nil {
				err = avg.Scan(avgValue["avg_value"])
				if err != nil {
					logging.Error(v.Code+"解析近6月均值"+avgValue["avg_value"].(string)+"失败", err.Error())
				}
			}
			if avg.Equal(decimal.Zero) {
				continue
			}
			for _, n := range v.LatestNetProfitList {
				if v.AvgNetProfit.Equal(decimal.Zero) {
					break
				}
				n.Rate = n.NetProfit.Sub(v.AvgNetProfit).Mul(decimal.NewFromInt32(100)).Div(v.AvgNetProfit.Abs())
				//大于等于限制上涨比率 并且是今天新爬取到的数据
				if (n.Rate.Cmp(decimal.NewFromFloat32(rateLimit)) >= 0 || n.NetProfit.Sub(avg.Mul(decimal.NewFromInt32(100000000))).Mul(decimal.NewFromInt32(100)).Div(avg.Mul(decimal.NewFromInt32(100000000)).Abs()).Cmp(decimal.NewFromFloat32(rateLimit)) >= 0) && v.LatestCreatedAt.Format(util.YMDHMS) >= time.Now().Format(util.YMD)+" 00:00:00" {
					name, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString("stock:name", n.Code)
					if err != nil {
						logging.Error("redis获取股票名称失败", err.Error())
					}
					n.Name = name
					io := home.NetProfitChangePushIO{
						Code:         n.Code,
						Organization: n.Organization,
						Author:       n.Author,
						//Rate:            n.Rate.Round(2),
						//AvgNetProfit:    v.AvgNetProfit,
						Name:            n.Name,
						Year:            year,
						AvgValue:        avg,
						LatestNetProfit: n.NetProfit,
						PublishDate:     v.LatestPublishDate,
					}
					//产品要求 增长率两个都满足取40天均值算的
					if n.Rate.Cmp(decimal.NewFromFloat32(rateLimit)) >= 0 {
						io.Rate = n.Rate.Round(2)
						io.AvgNetProfit = v.AvgNetProfit
					} else {
						io.Rate = n.NetProfit.Sub(avg.Mul(decimal.NewFromInt32(100000000))).Mul(decimal.NewFromInt32(100)).DivRound(avg.Mul(decimal.NewFromInt32(100000000)).Abs(), 2)
						io.AvgNetProfit = avg.Mul(decimal.NewFromInt32(100000000))
					}
					if io.Insert() {
						pushByCode(map[string]interface{}{
							"type": "net_profit_change",
							"data": io,
						}, n.Code, message.Message_User_Center)
					}
				}
			}
		}
	}

}

func MultiFirstResearchReportMonitor() {
	logging.Info("开始多次首次研报监控 port:" + strconv.Itoa(setting.ServerSetting.HttpPort))
	multiFirstKey := "multi_first:stock"
	type firstResearchReportCount struct {
		Code  string `json:"code"`
		Name  string `json:"name"`
		Count int    `json:"count"`
	}
	startTime := time.Now().AddDate(0, 0, -30).Format(util.YMD)
	bandOrg := []string{
		"开源证券",
		"世纪证券",
		"财信证券",
		"华金证券",
		"中国银河",
		"爱建证券",
		"恒泰证券",
		"华融证券",
		"东海证券",
		"万联证券",
		"粤开证券",
		"山西证券",
		"渤海证券",
		"华鑫证券",
		"万和证券",
		"华菁证券",
		"湘财证券",
		"华融证券",
		"东海证券",
		"华宝证券",
		"平安证券",
		"群益证券",
		"东莞证券",
		"国都证券",
		"第一上海",
		"财达证券",
		"华福证券",
		"德邦证券",
		"申港证券",
		"华龙证券",
		"上海证券",
		"中航证券",
	}
	alertList := make([]firstResearchReportCount, 0)
	err := models.DB().Table("u_research_report urr").
		Select("urr.code, urr.name,urr.created_at,count(urr.id) count").
		Joins("left join u_performance_announcement upa on urr.code = upa.code and upa.publish_date >= ?", startTime).
		Joins("left join u_performance_forecast upf on urr.code = upf.code and upf.publish_date >= ?", startTime).
		Joins("left join p_acem_express pae on upa.code = pae.code and pae.notice_date >= ?", startTime).
		Where("urr.created_at >=?", startTime).
		Where("urr.created_at<upa.publish_date or urr.created_at>DATE_ADD(upa.publish_date,INTERVAL 2 DAY) or upa.publish_date is null ").
		Where("urr.created_at<upf.publish_date or urr.created_at>DATE_ADD(upf.publish_date,INTERVAL 2 DAY) or upf.publish_date is null ").
		Where("urr.created_at<pae.notice_date or urr.created_at>DATE_ADD(pae.notice_date,INTERVAL 2 DAY) or pae.notice_date is null ").
		Where("parent=0").
		Where("level_change = '首次'").
		Where("urr.code <> ''").
		Where("urr.organization not in (?)", bandOrg).
		Group("urr.code").
		Having("count(urr.id)>=2").
		Find(&alertList).Error
	if err != nil {
		logging.Error("获取近30天首次研报次数报错", err.Error())
		return
	}
	for _, code := range alertList {
		count, err := gredis.HGetFloat64(multiFirstKey, code.Code)
		if err == nil {
			if code.Count > int(count) {
				pushByCode(map[string]interface{}{
					"type": "multi_first",
					"data": code,
				}, code.Code, message.Message_User_Center)
			}
		} else {
			logging.Info("redis获取"+code.Code+"近30天首次研报次数失败", err.Error())
			pushByCode(map[string]interface{}{
				"type": "multi_first",
				"data": code,
			}, code.Code, message.Message_User_Center)
		}
		err = gredis.HSet(multiFirstKey, code.Code, code.Count)
		if err != nil {
			logging.Error("reids 记录"+code.Code+"近30天首次研报次数失败", err.Error())
		}
	}

}

//盘后脚本集合 有先后执行顺序要求
func AfterScriptSet() {
	go syncHeadStock2StockPool()
	CopyNewCode2BStock()
	StockReportPeriodSet()
	replay.CronReplayStatistics()
	//距离业绩发布涨幅情况推送 需要在跑今天成交额之前
	PrePerformanceRate()
	//前一交易日成交额写入到redis 需要日线已经跑完
	SetPreVol2Redis()
	AutoCalculateRateSet()
	//MultiFirstResearchReportMonitor()
}

//大盘强弱监控
//1.炸板率
func BlockRate() {
	if time.Now().Format(util.HMS) < "09:45:00" || time.Now().Format(util.HMS) > "15:00:00" {
		return
	}
	if !GetStockTradeDate() {
		return
	}
	// 获取当前打板数量
	key := "block_rate"
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	codes, err := conn.HGetAllStringMap("stock:block")
	if err != nil {
		return
	}
	if len(codes) < 35 {
		logging.Info("block rate code amount", codes)
		return
	}
	blockMonitorKey := "stock:block_monitor"
	block, err := conn.MEMBERS(blockMonitorKey)
	if err != nil {
		logging.Info("get block_monitor err", err)
		return
	}
	blockMonitorNum := 0
	blockNum := 0
	code := []string{}
	blockCode := []string{}
	for _, value := range block {
		if !conn.HExists("stock:block", value) {
			blockMonitorNum++
			code = append(code, value)
		}
		if !conn.HExists("stock:pre_new", value) {
			blockNum++
			blockCode = append(blockCode, value)
		}
	}
	if blockMonitorNum < 1 {
		return
	}

	logging.Info("open_num is ", code)
	logging.Info("block_num is ", blockCode)
	rate := decimal.NewFromInt(int64(blockMonitorNum)).Div(decimal.NewFromInt(int64(blockNum))).Round(2)
	logging.Info("index_monitor rate ", rate)
	preRate, err := conn.HGetString("stock:index_monitor", "rate")
	//获取之前的炸板率
	conn.HSet("stock:index_monitor", "rate", rate)
	conn.Expire("stock:index_monitor", 9*3600)
	if err != nil {
		logging.Info("get block_monitor err", err)
		return
	}
	rateD, err := decimal.NewFromString(preRate)
	if err != nil {
		logging.Info("get block_monitor err", err)
		return
	}
	if !checkStockMonitor(key) {
		//炸板率超过0.35
		if rate.Sub(decimal.NewFromFloat(0.35)).Sign() > 0 {
			push(map[string]interface{}{
				"type":    "index_monitor",
				"content": fmt.Sprintf("炸板率达到%v%%", rate.Mul(decimal.NewFromInt(100))),
			}, message.Message_Stock)
			conn.HSet("stock:index_monitor", key, fmt.Sprintf("炸板率达到%v%%", rate.Mul(decimal.NewFromInt(100))))
			conn.Expire("stock:index_monitor", 9*3600)
			return
		}

	}
	decreaseKey := "decrease_block_rate"
	if !checkStockMonitor(decreaseKey) {
		//炸板率下降5%做提示
		if rate.Sub(rateD).Sub(decimal.NewFromFloat(-0.05)).Sign() < 0 {
			push(map[string]interface{}{
				"type":    "index_monitor",
				"content": fmt.Sprintf("炸板率下降，有所回暖。"),
			}, message.Message_Stock)
			conn := gredis.Clone(setting.RedisSetting.StockDB)
			conn.HSet("stock:index_monitor", decreaseKey, "炸板率下降，有所回暖。")
			conn.Expire("stock:index_monitor", 9*3600)
			return
		}
		//炸板率提升0.03
		if rate.Sub(rateD).Sub(decimal.NewFromFloat(0.05)).Sign() > 0 {
			push(map[string]interface{}{
				"type":    "index_monitor",
				"content": fmt.Sprintf("炸板率提升，注意风险。"),
			}, message.Message_Stock)
			conn.HSet("stock:index_monitor", decreaseKey, "炸板率提升，注意风险。")
			conn.Expire("stock:index_monitor", 9*3600)
			return
		}
	}
}

//昨日涨停股表现
func LastBlockOpen() (bool, error) {
	if !GetStockTradeDate() {
		return false, errors.New("time unvalid")
	}
	var yesterday, codes []string
	date := time.Now().Format("2006-01-02")
	if err := models.DB().Table("b_trade_date").Where("trade_date < ?", date).Order("trade_date DESC").Limit(1).Pluck("trade_date", &yesterday).Error; err != nil {
		logging.Error("stock.blockOpen() fetching yesterday Errors: ", err.Error())
		return false, err
	}
	if len(yesterday) < 1 {
		return false, errors.New("date unvalid")
	}
	if err := models.Data().Table("p_stock_tick").
		Joins("LEFT JOIN b_stock_names on p_stock_tick.stock_code = b_stock_names.code").
		Where("b_stock_names.name NOT LIKE '%ST%'").
		Where("p_stock_tick.amount > 0 ").
		Where("trade_date = ?", yesterday[0]).Where("block = close").
		Pluck("stock_code", &codes).Error; err != nil {
		logging.Error("stock.blockOpen() fetching codes Errors: ", err.Error())
		return false, err
	}
	var change decimal.Decimal
	code := []string{}
	//获取昨日涨停股
	for _, v := range codes {
		// 去掉新股
		count := 0
		err := models.DB().Table("p_stock_tick").Where("is_block = 0 ").Where("stock_code = ?", v).Count(&count).Error
		if err != nil {
			continue
		}
		if count < 1 {
			continue
		}
		var codeChange decimal.Decimal
		err = codeChange.Scan(template.GetRedisValue("stock:change", v))
		if err != nil {
			continue
		}
		change = change.Add(codeChange)
		code = append(code, v)
	}

	logging.Info("block code detail ", code)

	logging.Info("change rate ", change)
	avgChange := change.Div(decimal.NewFromInt(int64(len(code)))).Round(2)
	logging.Info("avg_change rate ", avgChange)
	if avgChange.Sub(decimal.NewFromFloat(3)).Sign() < 0 {
		return false, nil
	} else {
		return true, nil
	}
}

func LastPushCount() {
	if !GetStockTradeDate() {
		return
	}
	var count int
	date := time.Now().Format(util.YMD) + " 09:20:00"
	preDate := time.Now().Format(util.YMD) + " 09:40:00"
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	if err := models.DB().Table("u_push").Select("count(*) as count").
		Where("created_at <= ?", preDate).Where("created_at >= ?", date).
		Where("(content -> '$.content' LIKE '%集合%' or content -> '$.content' LIKE '%抢筹%' or content -> '$.content' LIKE '%突破历史新高%')").
		Count(&count).Error; err != nil {
		logging.Error("stock.blockOpen() fetching yesterday Errors: ", err.Error())
		return
	}
	logging.Info("LastPushCount 当前弹窗数量", count)
	if count > 15 {
		var yesterday []string
		dateNow := time.Now().Format("2006-01-02")
		if err := models.DB().Table("b_trade_date").Where("trade_date < ?", dateNow).
			Order("trade_date DESC").Limit(1).Pluck("trade_date", &yesterday).Error; err != nil {
			logging.Error("stock.blockOpen() fetching yesterday Errors: ", err.Error())
			return
		}
		if len(yesterday) < 1 {
			return
		}
		// 获取上一个交易日上涨家数
		var preCount int
		err := models.Data().Table("p_stock_tick").
			Where("stock_code != ''").
			Where("trade_date = ? ", yesterday[0]).Count(&preCount).Error
		logging.Info("LastPushCount 昨日总股票数量", preCount)
		if err != nil {
			return
		}
		var increaseCount int
		err = models.Data().Table("p_stock_tick").
			Where("`change` > 0 ").
			Where("stock_code != ''").
			Where("trade_date = ? ", yesterday[0]).Count(&increaseCount).Error
		if err != nil {
			return
		}
		logging.Info("LastPushCount 昨日总上涨家数", increaseCount)
		// 1、上一交易日上涨家数小于四分之一，则提示话术：今日或为反弹行情，注意节制
		if preCount > 0 {
			rate := decimal.NewFromInt(int64(increaseCount)).Div(decimal.NewFromInt(int64(preCount)))
			logging.Info("昨日上涨比例", rate)
			if rate.Sub(decimal.NewFromFloat(0.25)).Sign() <= 0 {
				push(map[string]interface{}{
					"type":    "index_monitor",
					"content": fmt.Sprintf("今日或为反弹行情，注意节制"),
				}, message.Message_Stock)
				return
			}
			codes, err := conn.HGetAllStringMap("stock:increase")
			if err != nil {
				return
			}
			total, err := conn.HGetAllStringMap("stock:name")
			if err != nil {
				return
			}
			if len(total) < 1 {
				return
			}
			rate = decimal.NewFromInt(int64(len(codes))).Div(decimal.NewFromInt(int64(len(total))))
			logging.Info("LastPushCount 此时上涨", rate)
			logging.Info("LastPushCount 今日上涨", rate)
			logging.Info("LastPushCount 今日股票总数", len(total))
			logging.Info("LastPushCount 今日上涨总数", len(codes))
			ret, err := LastBlockOpen()
			logging.Info("LastPushCount 是否溢价", strconv.FormatBool(ret))
			if err != nil {
				return
			}
			if rate.Sub(decimal.NewFromFloat(0.33)).Sign() <= 0 && !ret {
				push(map[string]interface{}{
					"type":    "index_monitor",
					"content": fmt.Sprintf("今日为局部行情"),
				}, message.Message_Stock)
				return
			}
			if rate.Sub(decimal.NewFromFloat(0.33)).Sign() <= 0 && ret {
				push(map[string]interface{}{
					"type":    "index_monitor",
					"content": fmt.Sprintf("行情较昨日有所减弱"),
				}, message.Message_Stock)
				return
			}
			if rate.Sub(decimal.NewFromFloat(0.33)).Sign() > 0 && ret {
				push(map[string]interface{}{
					"type":    "index_monitor",
					"content": fmt.Sprintf("今日行情接力不错"),
				}, message.Message_Stock)
				return
			}
			if rate.Sub(decimal.NewFromFloat(0.33)).Sign() > 0 && !ret {
				push(map[string]interface{}{
					"type":    "index_monitor",
					"content": fmt.Sprintf("今日为震荡行情"),
				}, message.Message_Stock)
				return
			}
		}
		return
	}
	if count <= 10 {
		push(map[string]interface{}{
			"type":    "index_monitor",
			"content": fmt.Sprintf("早盘情绪较弱，注意风险"),
		}, message.Message_Stock)
		return
	}
	return
}

// 弹出大盘强弱指数 v2
func PushIndexNum() {
	if time.Now().Format(util.HMS) < "09:35:00" || time.Now().Format(util.HMS) > "15:00:00" {
		return
	}
	if !GetStockTradeDate() {
		return
	}
	key := "amount"
	if checkStockMonitor(key) {
		return
	}
	var yesterday []string
	date := time.Now().Format("2006-01-02")
	if err := models.DB().Table("b_trade_date").Where("trade_date < ?", date).
		Order("trade_date DESC").Limit(3).Pluck("trade_date", &yesterday).Error; err != nil {
		logging.Error("stock.blockOpen() fetching yesterday Errors: ", err.Error())
		return
	}
	if len(yesterday) < 3 {
		return
	}
	nowtime := time.Now().Format("15:04") + ":00"
	type Amount struct {
		Amount decimal.Decimal `json:"amount" gorm:"column:amount"`
	}
	amount := Amount{}
	err := models.DB().Table("p_index_minute").Select("sum(amount) as amount").
		Where("time >= ? ", yesterday[2]).
		Where("time < ? ", date).
		Where("DATE_FORMAT(time,'%T') > '09:29:00'").
		Where("code in (?)", []string{"399001", "000001"}).
		Where("DATE_FORMAT(time,'%T') < ? ", nowtime).Find(&amount).Error
	if err != nil {
		return
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	// 获取每一个指数的成交量
	shAmount, err := conn.HGetFloat64("stock:ts_amount", "SH000001")
	if err != nil {
		logging.Info("get SH000001 err", err)
		return
	}
	szAmount, err := conn.HGetFloat64("stock:ts_amount", "SZ399001")
	if err != nil {
		logging.Info("get SZ399001 err", err)
		return
	}
	sumAmount := shAmount + szAmount
	logging.Info("get time amount ", sumAmount)
	logging.Info("get last amount ", amount.Amount.Div(decimal.NewFromFloat(3.00)))
	rate := decimal.NewFromFloat(sumAmount).Div(amount.Amount.Div(decimal.NewFromFloat(3.00))).Round(2)
	logging.Info("rate is ", rate)
	if rate.Sub(decimal.NewFromFloat(1.20)).Sign() > 0 {
		push(map[string]interface{}{
			"type":    "index_monitor",
			"content": fmt.Sprintf("大盘量较前三日放大"),
		}, message.Message_Stock)
		conn.HSet("stock:index_monitor", key, "大盘量较前三日放大")
		conn.Expire("stock:index_monitor", 9*3600)
	}
	if rate.Sub(decimal.NewFromFloat(0.8)).Sign() < 0 {
		push(map[string]interface{}{
			"type":    "index_monitor",
			"content": fmt.Sprintf("大盘量较前三日缩小"),
		}, message.Message_Stock)
		conn.HSet("stock:index_monitor", key, "大盘量较前三日缩小")
		conn.Expire("stock:index_monitor", 9*3600)
	}
}
func checkStockMonitor(key string) bool {
	//获取当前的炸板数量
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	//推送内容
	pushContent := conn.HExists("stock:index_monitor", key)
	if pushContent {
		return true
	}
	return false
}

// 核按钮监控
func NuclearButton() {
	if !GetStockTradeDate() {
		return
	}
	type Stock struct {
		Name string `json:"name" gorm:"column:name"`
		Code string `json:"code" gorm:"column:id"`
	}
	sto := []Stock{}
	err := models.DB().Table("b_stock").Find(&sto).Error
	if err != nil {
		return
	}
	type Str struct {
		PctChange decimal.Decimal `json:"pct_change" gorm:"column:pctchange"`
		Code      string          `json:"code" gorm:"column:code"`
		Close     decimal.Decimal `json:"close" gorm:"column:close"`
	}
	date := time.Now().Format(util.YMD)
	code := []string{}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, value := range sto {
		var price decimal.NullDecimal
		var total decimal.NullDecimal
		var zgb decimal.NullDecimal
		//股票涨幅
		if !conn.HExists("stock:tick", value.Code) {
			continue
		}
		perPrice, err := conn.HGetString("stock:tick", value.Code)
		if err != nil {
			logging.Error("funcs.go NuclearButton get redis price"+value.Code, err.Error())
			continue
		}
		err = price.Scan(perPrice)
		if err != nil {
			logging.Error("funcs.go NuclearButton scan redis price"+value.Code, err.Error())
			continue
		}
		perZgb, err := conn.HGetString("stock:zgb", value.Code)
		if err != nil {
			logging.Error("funcs.go NuclearButton get redis stock:zgb"+value.Code, err.Error())
			continue
		}
		err = zgb.Scan(perZgb)
		if err != nil {
			logging.Error("funcs.go NuclearButton scan redis stock:zgb"+value.Code, err.Error())
			continue
		}
		total.Decimal = zgb.Decimal.Mul(price.Decimal)
		total.Valid = true
		if total.Decimal.Sub(decimal.NewFromInt(8000000000)).Sign() < 0 {
			continue
		}
		stas := Str{}
		models.DB().Raw("select up_down_range as pct_change ,p.code from u_industry_stock_minute  as p "+
			"LEFT JOIN b_stock_names as namea on namea.`code` = p.code  where p.created_at > ? and  p.created_at < ? and p.code = ? "+
			" and namea.`name`  not like '%N%' and namea.`name`  not like '%ST%' and namea.`name`  not like '%C%' ",
			date+" 09:30:00", date+" 09:31:00", value.Code).
			First(&stas)
		if stas.PctChange.Sub(decimal.NewFromFloat(-0.08)).Sign() < 0 {
			continue
		}
		if stas.Close.IsZero() {
			continue
		}
		fmt.Println(stas.Close)
		staa := Str{}
		models.DB().Raw("select p.code,up_down_range as pct_change,p.close  from u_industry_stock_minute as p "+
			"LEFT JOIN b_stock_names as namea on namea.`code` = p.code  where p.created_at > ? and  p.created_at < ? and p.code = ? "+
			" and namea.`name`  not like '%N%' and namea.`name`  not like '%ST%' and namea.`name`  not like '%C%' ",
			date+" 09:31:00", date+" 09:32:00", value.Code).
			First(&staa)
		fmt.Println(staa.Close)
		tmp1 := staa.Close.Sub(stas.Close).Div(stas.Close)
		if tmp1.Sub(decimal.NewFromFloat(-0.08)).Sign() < 0 {
			code = append(code, value.Code)
		}
		fmt.Println(staa.Close.Sub(stas.Close).Div(stas.Close))
		stat := Str{}
		models.Data().Raw("select p.code,up_down_range as pct_change,p.close  from u_industry_stock_minute as p "+
			"LEFT JOIN b_stock_names as namea on namea.`code` = p.code  where p.created_at > ? and  p.created_at < ? and p.code = ? "+
			" and namea.`name`  not like '%N%' and namea.`name`  not like '%ST%' and namea.`name`  not like '%C%' ",
			date+" 09:32:00", date+" 09:33:00", value.Code).
			First(&stat)
		tmp2 := stat.Close.Sub(stas.Close).Div(stas.Close)
		fmt.Println(stat.Close.Sub(stas.Close).Div(stas.Close))
		if tmp2.Sub(decimal.NewFromFloat(-0.08)).Sign() < 0 {
			code = append(code, value.Code)
		}
		if tmp1.Add(tmp2).Sub(decimal.NewFromFloat(-0.08)).Sign() < 0 {
			code = append(code, value.Code)
		}
	}
	fmt.Println(code)
	logging.Info("nuclear button code num ", code)
	if len(code) > 4 {
		push(map[string]interface{}{
			"type":    "index_monitor",
			"content": fmt.Sprintf("出现核按钮"),
		}, message.Message_Stock)
		return
	}
	return
}
func GetStockTradeDate() bool {
	count := 0
	err := models.DB().Table("b_trade_date").Where("trade_date = ?", time.Now().Format(util.YMD)).Count(&count).Error
	if err != nil {
		return false
	}
	if count > 0 {
		return true
	}
	return false
}

type RealNetProfit struct {
	Code         string          `json:"code"`
	Name         string          `json:"name"`
	NetProfit    decimal.Decimal `json:"net_profit"`
	ReportPeriod time.Time       `json:"report_period"`
}

func OverOrLowerPerformanceMonitor() {
	logging.Info("开始超（低于）券商预测监控")
	dataList := make([]RealNetProfit, 0)
	err := models.DB().Table("u_performance_announcement").
		Joins("LEFT JOIN u_performance_forecast on u_performance_forecast.code=u_performance_announcement.code and u_performance_announcement.report_period=u_performance_forecast.report_period").
		Where("u_performance_announcement.publish_date=?", time.Now().Format(util.YMD)).
		Where("u_performance_forecast.publish_date is null").
		Find(&dataList).Error
	if err != nil {
		logging.Error("获取今天披露公告列表失败", err.Error())
	}
	overOrLowerPush(dataList, performance.ANNOUNCEMENT)
	err = models.DB().Table("p_acem_express").
		Select("p_acem_express.code,p_acem_express.name,p_acem_express.net_profit,notice_date publish_date,report_date report_period").
		Joins("LEFT JOIN u_performance_forecast on u_performance_forecast.code=p_acem_express.code and u_performance_forecast.report_period=p_acem_express.report_date").
		Where("notice_date=?", time.Now().Format(util.YMD)).
		Where("u_performance_forecast.publish_date is null").
		Find(&dataList).Error
	if err != nil {
		logging.Error("获取今天披露快报列表失败", err.Error())
	}
	overOrLowerPush(dataList, performance.EXPRESS)

}

func overOrLowerPush(dataList []RealNetProfit, pfType string) {
	var rateLimit int32 = 5
	type broker struct {
		Code              string    `json:"code"`
		BrokerProportion  string    `json:"broker_proportion"`
		ForecastNetProfit string    `json:"forecast_net_profit"`
		ReportPeriod      time.Time `json:"report_period"`
	}
	brokerList := make([]broker, 0)
	lastReportPeriod, _ := util.ReportPeriod(-1)
	err := models.DB().Table("u_performance_edit").Where("broker_proportion!='' OR forecast_net_profit!=''").Where("report_period>=?", lastReportPeriod).Find(&brokerList).Error
	if err != nil {
		logging.Error("获取券商预测列表失败", err.Error())
	}
	preData := RealNetProfit{}
	for _, data := range dataList {
		for _, broker := range brokerList {
			if data.Code == broker.Code && data.ReportPeriod == broker.ReportPeriod {
				err := models.DB().Table("u_performance_announcement").
					Where("code=?", data.Code).
					Where("report_period=?", data.ReportPeriod.AddDate(-1, 0, 0)).
					First(&preData).Error
				if err != nil {
					logging.Error("获取"+data.Code+" "+data.ReportPeriod.Format(util.YMD)+"失败", err.Error())
					break
				}
				/*rate := util.CalculateRate(decimal.NullDecimal{preData.NetProfit, !preData.NetProfit.Equal(decimal.Zero)}, decimal.NullDecimal{data.NetProfit, !data.NetProfit.Equal(decimal.Zero)})
				if !rate.Valid {
					break
				}*/
				brokerRateArr := strings.Split(broker.BrokerProportion, "~")
				brokerRate := decimal.NullDecimal{Valid: true}
				for _, fnp := range brokerRateArr {
					tmpRate := decimal.NullDecimal{}
					err := tmpRate.Scan(fnp)
					if err != nil {
						logging.Error("解析券商预测报错"+fnp, err.Error())
						brokerRate.Valid = false
						break
					}
					brokerRate.Decimal = brokerRate.Decimal.Add(tmpRate.Decimal)
					brokerRate.Valid = brokerRate.Valid && tmpRate.Valid
				}
				if brokerRate.Valid && !preData.NetProfit.Equal(decimal.Zero) {
					brokerRate.Decimal = brokerRate.Decimal.Div(decimal.NewFromInt(int64(len(brokerRateArr))))
					brokerNerProfit := preData.NetProfit.Add(preData.NetProfit.Mul(brokerRate.Decimal.Div(decimal.NewFromInt(100))))
					rate := util.CalculateRate(decimal.NullDecimal{data.NetProfit, !data.NetProfit.Equal(decimal.Zero)}, decimal.NullDecimal{brokerNerProfit, !brokerNerProfit.Equal(decimal.Zero)})
					if !rate.Valid {
						break
					}
					if rate.Decimal.Abs().GreaterThan(decimal.NewFromInt32(rateLimit)) {
						cmp := "低于"
						res := cmp
						if data.NetProfit.Sub(brokerNerProfit).Div(brokerNerProfit).GreaterThan(decimal.Zero) {
							cmp = "高于"
							res = "超"
						}
						tc := performance.TraceContent{
							Code:   data.Code,
							Name:   data.Name,
							PfType: pfType,
							Reason: "业绩" + res + "预期",
							From:   performance.FROM_BROKER,
						}
						if tc.SetPerformanceTrace(data.ReportPeriod.Format(util.YMD), false) {
							pushByCode(map[string]interface{}{
								"type": "performance",
								"content": data.Name + "净利润" +
									data.NetProfit.DivRound(decimal.NewFromInt(100000000), 2).String() + "亿" +
									cmp + "预测净利润" +
									rate.Decimal.Abs().Round(2).String() + "%",
							}, data.Code, message.Message_Home_Performance)
						}
					}
				} else {
					fNetProfitArr := strings.Split(broker.ForecastNetProfit, "~")
					brokerRate := decimal.NullDecimal{Valid: true}
					fNetProfit := decimal.NullDecimal{Valid: true}
					for _, fnp := range fNetProfitArr {
						tmpNetProfit := decimal.NullDecimal{}
						err := tmpNetProfit.Scan(fnp)
						if err != nil {
							logging.Error("解析券商预测报错"+fnp, err.Error())
							fNetProfit.Valid = false
							break
						}
						fNetProfit.Decimal = fNetProfit.Decimal.Add(tmpNetProfit.Decimal).Mul(decimal.NewFromInt(100000000))
						fNetProfit.Valid = fNetProfit.Valid && tmpNetProfit.Valid
					}
					if fNetProfit.Valid {
						fNetProfit.Decimal = fNetProfit.Decimal.Div(decimal.NewFromInt(int64(len(brokerRateArr))))
						//brokerRate = util.CalculateRate(decimal.NullDecimal{preData.NetProfit, true}, fNetPrit)
						if fNetProfit.Valid {
							rate := util.CalculateRate(decimal.NullDecimal{data.NetProfit, !data.NetProfit.Equal(decimal.Zero)}, fNetProfit)
							if !rate.Valid {
								break
							}
							if rate.Decimal.Abs().GreaterThan(decimal.NewFromInt32(rateLimit)) {
								cmp := "低于"
								res := cmp
								if data.NetProfit.Sub(fNetProfit.Decimal).Div(fNetProfit.Decimal).GreaterThan(brokerRate.Decimal) {
									cmp = "高于"
									res = "超"
								}
								tc := performance.TraceContent{
									Code:   data.Code,
									Name:   data.Name,
									PfType: pfType,
									Reason: "业绩" + res + "预期",
									From:   performance.FROM_BROKER,
								}
								if tc.SetPerformanceTrace(data.ReportPeriod.Format(util.YMD), false) {
									pushByCode(map[string]interface{}{
										"type": "performance",
										"content": data.Name + "净利润" +
											data.NetProfit.DivRound(decimal.NewFromInt(100000000), 2).String() + "亿" +
											cmp + "预测净利润" +
											rate.Decimal.Abs().Round(2).String() + "%",
									}, data.Code, message.Message_Home_Performance)
								}
							}
						}
					}
				}
				break
			}
		}
	}
}

type User struct {
	UserID int `json:"user_id" gorm:"column:user_id"`
}
type HomeStock struct {
	Code           string              `json:"code"`
	Name           string              `json:"name"`
	FirstIndustry  string              `json:"first_industry"`
	SecondIndustry string              `json:"second_industry"`
	StockQrr       decimal.Decimal     `json:"stock_qrr"`
	PctChange      decimal.NullDecimal `json:"pct_change"` //涨幅
	Amount         decimal.Decimal     `json:"amount"`
	Rank           int                 `json:"rank"`
	Price          decimal.NullDecimal `json:"price"`
}

func HomeStockPush() {
	predays := 3
	preFilterDayRange := 20
	tradeDayList := make([]time.Time, 0)
	err := models.DB().Table("b_trade_date").
		Where("trade_date<=?", time.Now()).
		Limit(preFilterDayRange+1).
		Order("id desc").
		Pluck("trade_date", &tradeDayList).Error
	if err != nil {
		logging.Error("AmountRankMonitor获取交易日失败", err.Error())
		return
	}
	lastTradeDayOffset := 1
	if tradeDayList[0].Format(util.YMD) < time.Now().Format(util.YMD) {
		//非交易日退出
		return
	}
	rankRange := 300
	amountRankStockList := make([]HomeStock, 0)
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	codeList := make([]string, 0)
	amountList := make([]decimal.Decimal, 0)
	amountSumList := make([]amountSum, 0)
	amountTotalSum := decimal.Zero
	codeAmountList, err := conn.ZRange("stock:zset:amount", -rankRange, -1, true)
	if err != nil {
		logging.Error("stock:zet:amount获取成交额前200股票失败")
		return
	}
	for i, v := range codeAmountList {
		if i%2 == 1 {
			amount, err := decimal.NewFromString(v)
			if err != nil {
				logging.Error("code:"+codeAmountList[i-1]+"成交量:"+v+"解析失败", err.Error())
				amount = decimal.Zero
			}
			amountList = append(amountList, amount)
		} else {
			codeList = append(codeList, v)
		}
	}

	err = models.DB().Table("u_stock_industry I").
		Select("I.code,S.name,I.first_industry,I.second_industry").
		Joins("LEFT JOIN b_stock S ON S.id=I.code").
		Where("I.code IN (?)", codeList).
		Find(&amountRankStockList).Error
	if err != nil {
		logging.Error("获取成交额前200股票基本信息失败")
		return
	}
	dbs := models.DB().Table("p_stock_minute_realtime").Select("code,sum(amount) as amount_sum").Where("code IN (?)", codeList)
	where := ""
	for i := lastTradeDayOffset; i <= predays; i++ {
		where = where + fmt.Sprintf("(time>'%s' AND time<'%s')", tradeDayList[i].Format(util.YMD)+" 09:30:00", tradeDayList[i].Format(util.YMD)+" "+time.Now().Format("15:04")+":00")
		if i < predays {
			where = where + " OR "
		}
	}
	err = dbs.Where(where).Group("code").Find(&amountSumList).Error
	if err != nil {
		logging.Error("获取过去"+fmt.Sprintf("%d", predays)+"天交易量失败", err.Error())
	}
	for i, _ := range amountRankStockList {
		//股票成交额
		for j, amount := range amountList {
			if amountRankStockList[i].Code == codeList[j] {
				amountRankStockList[i].Amount = amount
				amountRankStockList[i].Rank = rankRange - j
			}
			if i == 0 {
				amountTotalSum = amountTotalSum.Add(amount)
			}
		}
		//股票量比
		for _, amountSum := range amountSumList {
			if amountRankStockList[i].Code == amountSum.Code {
				amountRankStockList[i].StockQrr = amountRankStockList[i].Amount.Mul(decimal.NewFromInt(int64(predays))).Div(amountSum.AmountSum)
			}
		}
		//股票涨幅
		pecChange, err := conn.HGetString("stock:change", amountRankStockList[i].Code)
		if err != nil {
			logging.Error("获取redis"+amountRankStockList[i].Code+"涨幅失败", err.Error())
		} else {
			err = amountRankStockList[i].PctChange.Scan(pecChange)
			if err != nil {
				logging.Error("解析"+pecChange+"为"+amountRankStockList[i].Code+"涨幅失败", err.Error())
			}
		}
		//股票现价
		currentPrice, err := conn.HGetString("stock:tick", amountRankStockList[i].Code)
		if err != nil {
			logging.Error("获取redis"+amountRankStockList[i].Code+"现价失败", err.Error())
		} else {
			err = amountRankStockList[i].Price.Scan(currentPrice)
			if err != nil {
				logging.Error("解析"+pecChange+"为"+amountRankStockList[i].Code+"涨幅失败", err.Error())
			}
		}
	}
	ret := make(map[string][]HomeStock, 0)
	// 获取每只股票对应的用户
	for _, value := range amountRankStockList {
		// 获取对应的用户
		userModel := []User{}
		err := models.DB().Table("u_stock_industry").
			Select("distinct u_second_industry_user.user_id").
			Joins("left join u_second_industry_user on u_stock_industry.second_industry = u_second_industry_user.second_industry").
			Where("u_stock_industry.code = ?", value.Code).
			Where("u_second_industry_user.deleted_at is null ").Find(&userModel).Error
		if err != nil {
			continue
		}
		userIds := []string{}
		for _, v := range userModel {
			sp := home.StockPool{
				UserID:      v.UserID,
				Code:        value.Code,
				Type:        "personal_stock",
				Description: "成交额前300",
			}
			sp.Add()
			if !util.ContainsAny(userIds, strconv.Itoa(v.UserID)) {
				userIds = append(userIds, strconv.Itoa(v.UserID))
			}
		}
		userCodeModel := []User{}
		codeErr := models.DB().Table("u_user_stock").
			Select("distinct u_user_stock.user_id").
			Where("u_user_stock.code = ?", value.Code).
			Where("u_user_stock.deleted_at is null ").
			Where("type = ? ", "stock_pool").Find(&userCodeModel).Error
		if codeErr != nil {
			continue
		}
		for _, v := range userCodeModel {
			if !util.ContainsAny(userIds, strconv.Itoa(v.UserID)) {
				userIds = append(userIds, strconv.Itoa(v.UserID))
			}
		}
		for _, v := range userIds {
			ret[v] = append(ret[v], value)
		}
	}

	for _, value := range ret {
		sort.Slice(value, func(i, j int) bool {
			return !value[i].Amount.LessThan(value[j].Amount)
		})
	}

	for index, value := range ret {
		m := map[string]interface{}{
			"type":    "large_amount",
			"content": "成交额前300",
			"stocks":  value,
		}
		if index == "23" {
			logging.Info("get value ", value)
		}
		ws := websocket.GetInstance()
		bt, _ := json.Marshal(m)
		msg := message.ParseChanelMsg(message.Message_Personal_Stock, "system", index, string(bt), "", true)
		ws.SysSend(msg)
	}
}

func PreSeveralDayTranscation() {
	//一季度要同时更新年报信息
	if time.Now().Format(util.MD) >= "01-01" && time.Now().Format(util.MD) <= "03-31" {
		recommendStockUpdate(-1)
	}
	recommendStockUpdate(0)
	preSeveralDayIncrease(-10)
}

func recommendStockUpdate(offset int) {
	reportPeriod, _ := util.ReportPeriod(offset)
	preTenRecommned := performance.PreTenRecommend{}
	recommendStockMap, err := gredis.HGetAllStringMap(performance.PreTenRecommendKey + reportPeriod)
	if err != nil {
		logging.Error("获取"+performance.PreTenRecommendKey+reportPeriod+"redis信息失败", err.Error())
		return
	}
	tradeDate := common.TradeDate{}
	isTradeDate := tradeDate.GetDay(time.Now().Format(util.YMD), 0)
	for c, v := range recommendStockMap {
		err = json.Unmarshal([]byte(v), &preTenRecommned)
		if err != nil {
			logging.Error("解析"+c+" "+reportPeriod+"推荐信息失败", err.Error())
			continue
		}
		if isTradeDate {
			preTenRecommned.DayCount++
		}
		err = models.DB().Table("u_performance_announcement").
			Select("publish_date").
			Where("report_period=?", reportPeriod).
			Where("code=?", c).
			First(&preTenRecommned).Error
		data, err := json.Marshal(preTenRecommned)
		if err != nil {
			logging.Error("获取"+c+" "+reportPeriod+"公告日期信息失败", err.Error())
			continue
		}
		err = gredis.HSet(performance.PreTenRecommendKey+reportPeriod, c, data)
	}
}

func preSeveralDayIncrease(day int) {
	tradeDate := common.TradeDate{}
	tradeDate.GetDay(time.Now().Format(util.YMD), day)
	tickList := make([]stock.TickDTO, 0)
	err := models.DB().Table("p_stock_tick").Where("trade_date=?", tradeDate.Time).Find(&tickList).Error
	if err != nil {
		logging.Error("preSeveralDayIncrease获取日线数据报错", err.Error())
		return
	}
	for _, v := range tickList {
		err = gredis.HSet(stock.PreTenPreCloseKey, v.StockCode, v.PreClose)
		if err != nil {
			logging.Error(stock.PreTenPreCloseKey+" 写入"+v.StockCode+"失败", err.Error())
		}
	}
}

func ahMonitor() {
	tradeDate := common.TradeDate{}
	if !tradeDate.GetDay(time.Now().Format(util.YMD), 0) || time.Now().Weekday() == time.Saturday || time.Now().Weekday() == time.Sunday || time.Now().Format(util.HMS) < "09:30:00" || time.Now().Format(util.HMS) > "15:10:00" {
		return
	}
	cnt := 0
	err := models.DB().Table("p_ahstock_tick as a").
		Joins("LEFT JOIN p_ahstock_tick as b on a.code=b.code").
		Where("a.trade_date=?", time.Now().Format(util.YMD)).
		Where("b.trade_date=?", time.Now().AddDate(0, 0, -1).Format(util.YMD)).
		Where("a.h_pct_change=b.h_pct_change").
		Where("a.hprice=b.hprice").
		Count(&cnt).Error
	if cnt > 100 {
		//暂定上百港股价格不更新 认为港股不开市 ah股共130多个
		return
	}
	todayLimit := 5
	todayBlockCmpLimit := 6
	hIncLimit := 15
	dayLimit := 4  //要取三天前收盘价所以为4
	msgType := "ah_stock"
	preDaysHigh := 15
	preDaysLow := 10
	amountLimit := 300000 //3亿
	pushKey := "ah_stock:push"
	type ahDTO struct {
		Id         int64               `json:"id"`
		Code       string              `json:"code"`
		Hcode      string              `json:"hcode"`
		Aname      string              `json:"aname"`
		Hname      string              `json:"hname"`
		Price      decimal.NullDecimal `json:"price"`
		PctChange  decimal.NullDecimal `json:"pct_change"`
		Hprice     decimal.NullDecimal `json:"hprice"`
		HPctChange decimal.NullDecimal `json:"h_pct_change"`
		TradeDate  time.Time           `json:"trade_date"`
	}
	ahList := make([]ahDTO, 0)
	err = models.DB().Table("p_ahstock_tick").Where("trade_date=?", time.Now().Format(util.YMD)).Order("trade_date desc").Find(&ahList).Error
	if err != nil {
		logging.Error("获取ah列表失败", err.Error())
	}
	if !gredis.Exists(pushKey) {
		defer gredis.Expire(pushKey, 10*60*60)
	}
	for _, ah := range ahList {
		//if ah.TradeDate.Format(util.YMD)==time.Now().Format(util.YMD){
		amount := decimal.Decimal{}
		preAmount, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString("stock:pre_amount", ah.Code)
		if err != nil {
			logging.Error("ahMonitor获取"+ah.Code+"昨日成交额失败", err.Error())
			continue
		}
		err = amount.Scan(preAmount)
		if err != nil {
			logging.Error("ahMonitor解析"+ah.Code+"昨日成交额失败", err.Error())
		}
		if ah.PctChange.Valid && ah.HPctChange.Valid && amount.GreaterThan(decimal.NewFromInt(int64(amountLimit))) {
			//AH对比，任何一方涨幅大于另一方5%以上；话术：XXXH股（A股），涨幅明显大于A股（H股） 且大的股票涨幅大于5%
			if ah.PctChange.Decimal.Sub(ah.HPctChange.Decimal).Abs().GreaterThan(decimal.NewFromInt32(int32(todayLimit))) && (ah.PctChange.Decimal.GreaterThan(decimal.NewFromInt32(int32(todayLimit))) || ah.HPctChange.Decimal.GreaterThan(decimal.NewFromInt32(int32(todayLimit)))) {
				if gredis.HExists(pushKey, ah.Code) {
					continue
				}
				aStr := "A股涨" + ah.PctChange.Decimal.Round(2).String() + "%"
				hStr := "H股涨" + ah.HPctChange.Decimal.Round(2).String() + "%"
				str := ah.Aname + aStr + "," + hStr
				if ah.HPctChange.Decimal.GreaterThan(ah.PctChange.Decimal) {
					str = ah.Hname + hStr + "," + aStr
				}
				push(map[string]interface{}{
					"content": str,
					"type":    msgType,
				}, message.Message_Stock)
				_ = gredis.HSet(pushKey, ah.Code, str)
				continue
			}
			//一方当日涨幅达10%（9.98%），另一方小于6%；话术：XXXA股(H股)涨N%
			if ah.PctChange.Decimal.GreaterThan(decimal.NewFromFloat32(9.98)) && ah.HPctChange.Decimal.LessThan(decimal.NewFromInt32(int32(todayBlockCmpLimit))) {
				if gredis.HExists(pushKey, ah.Code) {
					continue
				}
				push(map[string]interface{}{
					"content": ah.Aname + "A股涨" + ah.PctChange.Decimal.Round(2).String() + "%",
					"type":    msgType,
				}, message.Message_Stock)
				_ = gredis.HSet(pushKey, ah.Code, ah.Aname+"A股涨"+ah.PctChange.Decimal.Round(2).String()+"%")
				continue
			}
			if ah.HPctChange.Decimal.GreaterThan(decimal.NewFromFloat32(9.98)) && ah.PctChange.Decimal.LessThan(decimal.NewFromInt32(int32(todayBlockCmpLimit))) {
				if gredis.HExists(pushKey, ah.Code) {
					continue
				}
				push(map[string]interface{}{
					"content": ah.Hname + "H股涨" + ah.HPctChange.Decimal.Round(2).String() + "%",
					"type":    msgType,
				}, message.Message_Stock)
				_ = gredis.HSet(pushKey, ah.Code, ah.Hname+"H股涨"+ah.HPctChange.Decimal.Round(2).String()+"%")
				continue
			}
			if ah.HPctChange.Decimal.GreaterThan(decimal.NewFromInt32(int32(hIncLimit))) && ah.PctChange.Decimal.LessThan(decimal.NewFromFloat32(9.98)) {
				if gredis.HExists(pushKey, ah.Code) {
					continue
				}
				push(map[string]interface{}{
					"content": ah.Hname + "H股涨幅达" + ah.PctChange.Decimal.Round(2).String() + "%",
					"type":    msgType,
				}, message.Message_Stock)
				_ = gredis.HSet(pushKey, ah.Code, ah.Hname+"H股涨幅达"+ah.PctChange.Decimal.Round(2).String()+"%")
				continue
			}

		} else {
			logging.Info(ah.TradeDate.Format(util.YMD) + "ah信息不完整")
		}
		//}
	}
	type preDaysAH struct {
		Code  string  `json:"code"`
		Aname string  `json:"aname"`
		Hname string  `json:"hname"`
		Hcode string  `json:"hcode"`
		AhDTO []ahDTO `json:"ah_dto" gorm:"FOREIGNKEY:Code;ASSOCIATION_FOREIGNKEY:Code"`
	}
	preAHList := make([]preDaysAH, 0)
	err = models.DB().Select("code,hcode,aname,hname").Table("p_ahstock_tick").Preload("AhDTO", func(db *gorm.DB) *gorm.DB {
		return models.DB().Table("p_ahstock_tick").
			Joins(fmt.Sprintf("inner join (select  distinct trade_date from p_ahstock_tick order by trade_date desc limit %d) D on D.trade_date=p_ahstock_tick.trade_date", dayLimit)).Order("code asc,p_ahstock_tick.trade_date desc")
	}).Group("code").Find(&preAHList).Error
	if err != nil {
		logging.Error("获取ah3日列表失败", err.Error())
	}
	/*err=models.DB().Raw(fmt.Sprintf("select * from p_ahstock_tick pat inner join (select  distinct trade_date from p_ahstock_tick order by trade_date desc limit %d) D on D.trade_date=pat.trade_date order by code asc,pat.trade_date desc",dayLimit)).Find(&ahList).Error*/
	/*err=models.DB().Table("p_ahstock_tick").
		Joins(fmt.Sprintf("inner join (select  distinct trade_date from p_ahstock_tick order by trade_date desc limit %d) D on D.trade_date=p_ahstock_tick.trade_date ",dayLimit)).Order("code asc,p_ahstock_tick.trade_date desc").
	Find(&ahList).Error*/
	for _, preAh := range preAHList {
		if len(preAh.AhDTO) == dayLimit {
			amount := decimal.Decimal{}
			preAmount, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString("stock:pre_amount", preAh.Code)
			if err != nil {
				logging.Error("ahMonitor获取"+preAh.Code+"昨日成交额失败", err.Error())
				continue
			}
			err = amount.Scan(preAmount)
			if err != nil {
				logging.Error("ahMonitor解析"+preAh.Code+"昨日成交额失败", err.Error())
			}
			if preAh.AhDTO[0].Price.Valid && preAh.AhDTO[0].Hprice.Valid && preAh.AhDTO[dayLimit-1].Price.Valid && preAh.AhDTO[dayLimit-1].Hprice.Valid && amount.GreaterThan(decimal.NewFromInt(int64(amountLimit))) {
				aRate := util.CalculateRate(preAh.AhDTO[dayLimit-1].Price, preAh.AhDTO[0].Price)
				hRate := util.CalculateRate(preAh.AhDTO[dayLimit-1].Hprice, preAh.AhDTO[0].Hprice)
				if aRate.Valid && hRate.Valid {
					if aRate.Decimal.GreaterThan(decimal.NewFromInt32(int32(preDaysHigh))) && hRate.Decimal.LessThan(decimal.NewFromInt32(int32(preDaysLow))) && preAh.AhDTO[dayLimit-1].Price.Valid && preAh.AhDTO[dayLimit-1].Price.Decimal.GreaterThan(decimal.Zero) {
						if gredis.HExists(pushKey, preAh.Code) {
							continue
						}
						push(map[string]interface{}{
							"content": preAh.Aname + "A股近3日涨幅达" + aRate.Decimal.Round(2).String() + "%",
							"type":    msgType,
						}, message.Message_Stock)
						_ = gredis.HSet(pushKey, preAh.Code, preAh.Aname+"A股近3日涨幅达"+aRate.Decimal.Round(2).String()+"%")
						continue
					}
					if hRate.Decimal.GreaterThan(decimal.NewFromInt32(int32(preDaysHigh))) && aRate.Decimal.LessThan(decimal.NewFromInt32(int32(preDaysLow))) && preAh.AhDTO[dayLimit-1].Hprice.Valid && preAh.AhDTO[dayLimit-1].Hprice.Decimal.GreaterThan(decimal.Zero) {
						if gredis.HExists(pushKey, preAh.Code) {
							continue
						}
						push(map[string]interface{}{
							"content": preAh.Hname + "H股近3日涨幅达" + hRate.Decimal.Round(2).String() + "%",
							"type":    msgType,
						}, message.Message_Stock)
						_ = gredis.HSet(pushKey, preAh.Code, preAh.Hname+"H股近3日涨幅达"+hRate.Decimal.Round(2).String()+"%")
						continue
					}
				}
			}
		}
	}
}
func OnceScript() {
	type data struct {
		Code         string    `json:"code"`
		ReportPeriod time.Time `json:"report_period"`
		performance.PreTenRecommend
	}
	preTenRecommnedList := make([]data, 0)
	err := models.DB().Raw("select upe.code,upe.report_period,upe.updated_at,upf.publish_date,pst.pre_close,pst.trade_date recommend_date,DATEDIFF(now(),trade_date) day_count from u_performance_edit upe left join u_performance_forecast upf on upe.code = upf.code and upe.report_period=upf.report_period left join (select upe.id,upe.updated_at,MAX(pst.trade_date) maxtrade_date from u_performance_edit upe left join p_stock_tick pst on date(upe.updated_at) >= pst.trade_date and pst.stock_code=upe.code where upe.recommend=1 and upe.report_period>='2020-12-31' GROUP BY upe.id) D on D.id=upe.id left join p_stock_tick pst on D.maxtrade_date = pst.trade_date and pst.stock_code=upe.code where upe.recommend=1 and upe.report_period>='2020-12-31' and upf.publish_date is null").Find(&preTenRecommnedList).Error
	if err != nil {
		logging.Error("一次脚本获取推荐列表失败")
	}
	for _, recommend := range preTenRecommnedList {
		if gredis.HExists(performance.PreTenRecommendKey+recommend.ReportPeriod.Format(util.YMD), recommend.Code) {
			continue
		}
		dataJs, _ := json.Marshal(recommend.PreTenRecommend)
		err = gredis.HSet(performance.PreTenRecommendKey+recommend.ReportPeriod.Format(util.YMD), recommend.Code, dataJs)
		if err != nil {
			logging.Error("设置" + performance.PreTenRecommendKey + recommend.ReportPeriod.Format(util.YMD) + " " + recommend.Code + "失败：")
		}
	}
}

func StockBulkPush(){
	//大宗交易推送
	var (
		Error error
		suffix string
		isTradeDate int
	)
	oldNameList := make(map[string]int)
	//判断今天是否为交易日
	models.DB().Table("b_trade_date").Where("trade_date = ?",time.Now().Format(util.YMD)).Count(&isTradeDate)
	if isTradeDate == 0{
		return
	}
	//获取上一交易日
	trade,_ := common.GetTradeDateList(time.Now().Format(util.YMD),-1,false)
	if trade == nil{
		logging.Error("select trade_date error")
		return
	}
	tradeDate := trade[0].Format(util.YMD)
	//查询溢价率大于百分之10
	dbs := models.DB().Table("p_stock_bulk").Where("date = ?",tradeDate).Select("name")

	for _,k := range []int{1,2,3}{
		nameList := make([]string,0)
		if k==1 {
			//同时满足高溢价 机构买入
			suffix = " 大宗高溢价机构买入"
			linNameList := make([]string,0)
			Error := dbs.Where("premium_rate > ?",10).Pluck("name",&linNameList).Error
			if Error !=nil{
				continue
			}
			Error = dbs.Where("name in (?)", linNameList).
				Where("buyer = ?","机构专用").Group("code").Pluck("name", &nameList).Error

		}else if k==2{
			//只有机构买入
			suffix = " 大宗机构买入"
			Error = dbs.Where("premium_rate < ?", 10).
				Where("buyer = ?","机构专用").Group("code").Pluck("name", &nameList).Error
		}else{
			//只有溢价率
			suffix = " 大宗高溢价"
			Error = dbs.Where("premium_rate >= ?", 10).
				Where("buyer != ?","机构专用").Group("code").Pluck("name", &nameList).Error

		}
		if Error != nil{
			logging.Error("db error by p_stock_bulk")
			return
		}
		res :=""
		for _,name := range nameList{
			if _,ok := oldNameList[name];!ok{
				res += name + ","
				oldNameList[name] = 1
			}
		}

		if res != ""{
			logging.Info("stcok_bulk success")
			push(map[string]interface{}{
			"type":    "stock_bulk",
			"content": res[:len(res)-1] + suffix,
			}, message.Message_Stock)
		}
	}
}
