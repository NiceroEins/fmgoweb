package performance

import (
	"datacenter/models/performance"
	"datacenter/pkg/logging"
	"datacenter/service/crud_service"
)

func GetIndustryList(qo *performance.IndustryUserQO) ([]crud_service.VO, int, error) {
	vo := make([]crud_service.VO, 0)
	data, cnt, err := performance.GetIndustryList(qo)
	if err != nil {
		logging.Error("GetIndustryList() err:", err.Error())
		return nil, 0, err
	}
	for _, v := range data {
		p := v.DTO2VO()
		vo = append(vo, p)
	}
	return vo, cnt, err
}

func GetIndustryDetail(qo *performance.IndustryDetailQO) ([]crud_service.VO,int, error) {
	vo := make([]crud_service.VO, 0)
	data,cnt, err := performance.GetIndustryDetail(qo)
	if err != nil {
		logging.Error("GetIndustryDetail() err:", err.Error())
		return nil,0, err
	}
	for _, v := range data {
		p := v.DTO2VO()
		vo = append(vo, p)
	}
	return vo,cnt, nil
}

func EditIndustry(qo *performance.IndustryEO) error {
	err := performance.EditIndustryAndUser(qo)
	if err != nil {
		logging.Error("EditIndustry() err:", err.Error())
		return err
	}
	return nil
}

func AddIndustry(qo *performance.IndustryIO) error {
	err := performance.AddIndustry(qo)
	if err != nil {
		logging.Error("AddIndustry() err:", err.Error())
		return err
	}
	return nil
}

func IndustryAddCode(qo *performance.IndustryCodeAdder) error {
	err := performance.IndustryAddCode(qo)
	if err != nil {
		logging.Error("IndustryAddCode() err:", err.Error())
		return err
	}
	return nil
}

func DelIndustry(qo *performance.IndustryDelQO) error {
	err := performance.DelIndustry(qo)
	if err != nil {
		logging.Error("DelIndustry() err:", err.Error())
		return err
	}
	return nil
}

func IndustryDelCode(qo *performance.IndustryDelCodeQO) error {
	err := performance.IndustryDelCode(qo)
	if err != nil {
		logging.Error("IndustryDelCode() err:", err.Error())
		return err
	}
	return nil
}

func SetIndustryHeadCode(qo *performance.CodeEO) error {
	err := performance.SetIndustryHeadCode(qo)
	if err != nil {
		logging.Error("SetIndustryHeadCode() err:", err.Error())
		return err
	}
	return nil
}