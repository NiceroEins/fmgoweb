package performance

import (
	"datacenter/models/performance"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"datacenter/service/excel_service"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"io"
	"reflect"
	"sort"
	"strconv"
	"time"
)

type quarter struct {
	BusinessRevenueGrowthRateQuarter   string `json:"business_revenue_growth_rate_quarter"`     //营业收入同比增长（单季度）(%)
	BusinessRevenueQuarter             string `json:"business_revenue_quarter"`                 //营业收入（单季度）
	CashFlowPerShareQuarter            string `json:"cash_flow_per_share_quarter"`              //每股经营现金流量（单季度）
	GrossProfitMarginQuarter           string `json:"gross_profit_margin_quarter"`              //单季度销售毛利率(%)
	NetProfitAfterDedGrowthRateQuarter string `json:"net_profit_after_ded_growth_rate_quarter"` //扣非净利润同比增长率（单季度）(%)
	NetProfitAfterDedQuarter           string `json:"net_profit_after_ded_quarter"`             //扣非净利润（单季度）
	NetProfitGrowthRateQuarter         string `json:"net_profit_growth_rate_quarter"`           //净利润同比增长（单季度）
	NetProfitQuarter                   string `json:"net_profit_quarter"`                       //净利润（单季度）
}
type PerformanceVO struct {
	Code                   string     `json:"code"`
	ExpectedPublishDate    string     `json:"expected_publish_date"`  //披露时间
	IndividualShareTrack   string     `json:"individual_share_track"` //个股跟踪
	Industry               string     `json:"industry"`
	IndustryInterpretation string     `json:"industry_interpretation"`
	Name                   string     `json:"name"`
	ForecastPublishDate    string     `json:"forecast_publish_date"` //预告日期
	PublishHistory         string     `json:"publish_history"`       //预告时间历史
	DisclosureChangeAt     string     `json:"disclosure_change_at"`
	Quarters               [4]quarter `json:"quarters"`
	ReportPeriod           string     `json:"report_period"` //报告期
	UserName               string     `json:"user_name"`
	UserId                 string     `json:"user_id"`
	PerformanceIncrUpper   string     `json:"performance_incr_upper"` //业绩变动幅度上限(行业追踪里面的本期预告)
	PerformanceIncrLower   string     `json:"performance_incr_lower"` //业绩变动幅度下限(行业跟踪里面的本期预告)
	NetProfitYear          string     `json:"net_profit_year"`        //本期公告(年度净利润)
	AnnouncementIncrease   string     `json:"announcement_increase"`  //公告涨幅
	ForecastIncrease       string     `json:"forecast_increase"`      //预告涨幅
}

func DecimalPtr2StringAndMul(d *decimal.Decimal) string {
	if d != nil {
		return d.Mul(decimal.NewFromInt(100)).Round(2).String()
	} else {
		return ""
	}
}

func DTO2VO(dto *performance.PerformanceDTO, nowQuarter *int) *PerformanceVO {
	nowKey := 4
	if dto[nowKey] != nil {
		if *nowQuarter <= 0 {
			quarterMap := map[string]int{
				"03-31": 0,
				"06-30": 1,
				"09-30": 2,
				"12-31": 3,
			}
			nowReportPeriod := util.TimePtr2String(dto[nowKey].ReportPeripod, util.MD)
			*nowQuarter = quarterMap[nowReportPeriod]
		}
		vo := PerformanceVO{
			Code:                 dto[nowKey].Code,
			Name:                 dto[nowKey].Name,
			IndividualShareTrack: dto[nowKey].IndividualShareTrack,
			PublishHistory:       util.TimePtr2String(dto[nowKey].DisclosureTimeHistory, util.YMD),
			DisclosureChangeAt:   util.TimePtr2String(dto[nowKey].DisclosureChangeAt, util.YMD),
			UserName:             dto[nowKey].Realname,
			PerformanceIncrUpper: dto[nowKey].PerformanceIncrUpper,
			PerformanceIncrLower: dto[nowKey].PerformanceIncrLower,
			NetProfitYear:        util.DecimalPtr2String(dto[nowKey].NetProfitYear),
			AnnouncementIncrease: DecimalPtr2StringAndMul(dto[nowKey].AnnouncementIncrease),
			ForecastIncrease:     DecimalPtr2StringAndMul(dto[nowKey].ForecastIncrease),
		}
		if dto[nowKey].IndustryName != nil {
			for _, v := range dto[nowKey].IndustryName {
				if vo.UserName != "" && v.RealName != "" {
					vo.UserName = vo.UserName + "/"
				}
				vo.UserName = vo.UserName + v.RealName
				if vo.UserId != "" && v.UserId != "" {
					vo.UserId = vo.UserId + "/"
				}
				vo.UserId = vo.UserId + v.UserId
				if vo.Industry != "" && v.IndustryName != "" {
					vo.Industry = vo.Industry + "/"
				}
				vo.Industry = vo.Industry + v.IndustryName
				if v.IndustryInterpretation != "" {
					if v.IndustryInterpretation != "" {
						vo.IndustryInterpretation = vo.IndustryInterpretation + "/"
					}
					vo.IndustryInterpretation = vo.IndustryInterpretation + v.IndustryName + ":" + v.IndustryInterpretation
				}
			}
		}
		if dto[nowKey].ForecastPublishDate != nil {
			vo.ForecastPublishDate = dto[nowKey].ForecastPublishDate.Format(util.MD)
		}
		if dto[nowKey].ReportPeripod != nil {
			vo.ReportPeriod = dto[nowKey].ReportPeripod.Format(util.YMD)
		}
		if dto[nowKey].ExpectedPublishDate != nil {
			vo.ExpectedPublishDate = dto[nowKey].ExpectedPublishDate.Format(util.YMD)
		}

		for i, v := range dto[0:4] {
			j := (*nowQuarter + i) % 4
			if v != nil {
				vo.Quarters[j].BusinessRevenueGrowthRateQuarter = DecimalPtr2StringAndMul(v.BusinessRevenueGrowthRateQuarter)
				vo.Quarters[j].BusinessRevenueQuarter = util.DecimalPtr2String(v.BusinessRevenueQuarter)
				vo.Quarters[j].CashFlowPerShareQuarter = util.DecimalPtr2String(v.CashFlowPerShareQuarter)
				vo.Quarters[j].GrossProfitMarginQuarter = util.DecimalPtr2String(v.GrossProfitMarginQuarter)
				vo.Quarters[j].NetProfitAfterDedGrowthRateQuarter = DecimalPtr2StringAndMul(v.NetProfitAfterDedGrowthRateQuarter)
				vo.Quarters[j].NetProfitAfterDedQuarter = util.DecimalPtr2String(v.NetProfitAfterDedQuarter)
				vo.Quarters[j].NetProfitGrowthRateQuarter = DecimalPtr2StringAndMul(v.NetProfitGrowthRateQuarter)
				vo.Quarters[j].NetProfitQuarter = util.DecimalPtr2String(v.NetProfitQuarter)
			}
		}
		return &vo
	} else {
		return &PerformanceVO{}
	}
}

func GetPerformanceIndustryTrack(qo *performance.PrformanceQO) ([]crud_service.VO, int, error) {
	ret := make([]crud_service.VO, 0)
	req := performance.SearchIndustryTrackRequest{
		Stock:        qo.Search,
		IndustryName: qo.SearchIndustry,
		UserId:       qo.UserId,
		ReportPeriod: qo.ReportPeriod,
	}
	var err error
	req.ForecastIncreaseDay, err = strconv.Atoi(qo.ForecastDays)
	if err != nil {
		return nil, 0, err
	}
	req.AnnouncementIncreaseDay, err = strconv.Atoi(qo.AnnounceDays)
	if err != nil {
		return nil, 0, err
	}
	dto, err := performance.SearchIndustryTrackList(req)
	if err != nil {
		return nil, 0, err
	}
	if qo.SortKey != "" {
		key := util.Snake2Camel(qo.SortKey)
		if qo.SortId > 0 && qo.SortId < 5 {
			sort.Slice(dto, func(i, j int) bool {
				if dto[i][qo.SortId-1] == nil || dto[j][qo.SortId-1] == nil {
					return dto[i][qo.SortId-1] == nil
				}
				before := reflect.ValueOf(*dto[i][qo.SortId-1]).FieldByName(key)
				after := reflect.ValueOf(*dto[j][qo.SortId-1]).FieldByName(key)
				return qo.Direction == util.Compare(before, after)
			})
		} else {
			sort.Slice(dto, func(i, j int) bool {
				if dto[i][4] == nil || dto[j][4] == nil {
					return dto[i][4] == nil
				}
				return qo.Direction == util.Compare(reflect.ValueOf(*dto[i][4]).FieldByName(key),
					reflect.ValueOf(*dto[j][4]).FieldByName(key))
			})
		}
	} else {
		sort.Slice(dto, func(i, j int) bool {
			if dto[i][4] == nil || dto[j][4] == nil {
				return dto[i][4] == nil
			}
			if dto[i][4].ExpectedPublishDate == dto[j][4].ExpectedPublishDate {
				return dto[i][4].Code < dto[j][4].Code
			}
			if dto[i][4].ExpectedPublishDate == nil || dto[j][4].ExpectedPublishDate == nil {
				return !(dto[i][4].ExpectedPublishDate == nil)
			}
			now := time.Now().Local()
			if dto[i][4].ExpectedPublishDate.Before(now) != dto[j][4].ExpectedPublishDate.Before(now) {
				return dto[j][4].ExpectedPublishDate.Before(now)
			}
			return dto[i][4].ExpectedPublishDate.Before(*dto[j][4].ExpectedPublishDate)
		})
	}
	var nowQuarter int
	for i, v := range dto {
		if i >= qo.PageSize*(qo.PageNum-1) && i < qo.PageSize*qo.PageNum {
			vo := DTO2VO(&v, &nowQuarter)
			ret = append(ret, vo)
		}
	}

	return ret, len(dto), err
}

func PerformanceCalculate(qo *performance.CaculateQO) ([]crud_service.VO, int, error) {
	vo := make([]crud_service.VO, 0)
	data, cnt, err := performance.GetCalculate(qo)
	if err != nil {
		logging.Error("PerformanceCalculate() err:", err.Error())
		return nil, 0, err
	}
	for _, v := range data {
		p := v.DTO2VO()
		vo = append(vo, p)
	}
	return vo, cnt, err
}

func PerformanceCalculateAll(qo *performance.CaculateEPQO) ([]crud_service.DTO, error) {
	data, err := performance.GetCalculateAll(qo)
	if err != nil {
		logging.Error("PerformanceCalculate() err:", err.Error())
		return nil, err
	}
	return data, err
}

func EditPerformanceEdit(io *performance.PerformanceEditIO) error {
	err := performance.EditPerformanceEdit(io)
	if err != nil {
		logging.Error("EditPerformanceEdit() err:", err.Error())
		return err
	}
	return err
}

func ImportPerformanceEdit(file io.Reader, uid, reportPeriod string) (int, []string, error) {
	rows, err := excel_service.UploadHandler(file, "总表")
	if err != nil {
		return 0, nil, err
	}
	if len(rows) == 0 || (len(rows[1]) != 5 && len(rows[1]) != 4) {
		return 0, nil, errors.New("文件格式有误")
	}
	failCodes := performance.ImportPerformanceEdit(rows, uid, reportPeriod)
	return len(rows[1:]), failCodes, nil
}

func GetUserInfo() ([]crud_service.VO, error) {
	vo := make([]crud_service.VO, 0)
	data, err := performance.GetUserInfo()
	if err != nil {
		return nil, err
	}
	for _, v := range data {
		p := v.DTO2VO()
		vo = append(vo, p)
	}
	return vo, err
}

func GetPerformanceIndustry(search string) ([]crud_service.VO, error) {
	vo := make([]crud_service.VO, 0)
	data, err := performance.GetPerformanceIndustry(search)
	if err != nil {
		return nil, err
	}
	for _, v := range data {
		p := v.DTO2VO()
		vo = append(vo, p)
	}
	return vo, err
}

func EditRelationShip(id, industryId int, userId string) error {
	err := performance.EditRelationShip(id, industryId, userId)
	if err != nil {
		return err
	}
	return nil
}

func AddRelationShip(industryId int, stockId, userId string) error {
	err := performance.AddRelationShip(industryId, stockId, userId)
	if err != nil {
		return err
	}
	return nil
}

func GetPerformanceTrend(qo performance.TrendQO) ([]performance.TrendVO, int, error) {
	data, _, err := performance.GetPerformanceTrend(qo)
	if err != nil {
		return nil, 0, err
	}
	vos, err := performance.DTO2VO(data, qo.ReportPeriod)
	if err != nil {
		return nil, 0, err
	}
	//if qo.SortKey > 0 {
	sort.Slice(vos, func(i, j int) bool {
		switch qo.SortKey {
		case 1:
			if vos[i].StockCount == vos[j].StockCount {
				return qo.Direction == (vos[i].IndustryName < vos[j].IndustryName)
			} else {
				return qo.Direction == (vos[i].StockCount < vos[j].StockCount)
			}
		case 2:
			if vos[i].ReportCount == vos[j].ReportCount {
				if vos[i].StockCount == vos[j].StockCount {
					return qo.Direction == (vos[i].IndustryName < vos[j].IndustryName)
				} else {
					return qo.Direction == (vos[i].StockCount < vos[j].StockCount)
				}
			} else {
				return qo.Direction == (vos[i].ReportCount < vos[j].ReportCount)
			}

		case 3:
			if vos[i].Point == vos[j].Point {
				if vos[i].StockCount == vos[j].StockCount {
					return qo.Direction == (vos[i].IndustryName < vos[j].IndustryName)
				} else {
					return qo.Direction == (vos[i].StockCount < vos[j].StockCount)
				}
			} else {
				return qo.Direction == (vos[i].Point < vos[j].Point)
			}
		default:
			return qo.Direction == (vos[i].IndustryName < vos[j].IndustryName)

		}
	})
	//}
	return vos[(qo.PageNum-1)*qo.PageSize : util.Min(qo.PageNum*qo.PageSize, len(vos))], len(vos), nil
}
func GetPerformanceTrendUpdateTime() (string, error) {
	updateTime, err := performance.GetPerformanceTrendUpdateTime()
	if err != nil {
		return "", err
	}
	return updateTime.Format(util.YMDHMS), nil
}

func GetForecastStatistics(qo performance.ForecastStatisticsQO) ([]crud_service.VO, int, error) {
	vo := make([]crud_service.VO, 0)
	data, cnt, err := performance.GetForecastStatistics(qo)
	if err != nil {
		logging.Error("GetForecastStatistics() err:", err.Error())
		return nil, 0, err
	}
	for _, v := range data {
		p := v.DTO2VO()
		vo = append(vo, p)
	}
	return vo, cnt, err
}
func GetForecastUpdateTime() (string, error) {
	updateTime, err := performance.GetForecastUpdateTime()
	if err != nil {
		return "", err
	}
	return updateTime.Format(util.YMDHMS), nil
}

func GetTrace(qo *performance.TraceQO) ([]crud_service.VO, int, error) {
	vo := make([]crud_service.VO, 0)
	data, cnt, err := performance.GetPerformanceTrace(qo)
	if err != nil {
		logging.Error("GetTrace() err:", err.Error())
		return nil, 0, err
	}
	for _, v := range data {
		p := v.DTO2VO()
		vo = append(vo, p)
	}
	return vo, cnt, err
}

func GetPerformanceHighLightsHistory(qo *performance.HighLightQO) ([]crud_service.VO, int, error) {
	vo := make([]crud_service.VO, 0)
	data, cnt, err := performance.GetPerformanceHighLightsHistory(qo)
	if err != nil {
		logging.Error("GetPerformanceHighLightsHistory() err:", err.Error())
		return nil, 0, err
	}
	for _, v := range data {
		p := v.DTO2VO()
		vo = append(vo, p)
	}
	return vo, cnt, err
}

func GetReportPeriodList() []string {
	return performance.GetReportPeriodList()
}
