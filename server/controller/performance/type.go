package performance

import (
	"datacenter/models"
	"datacenter/pkg/logging"
	"fmt"
	"strings"
	"time"
)

func Monster() {
	var codes []string
	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Order("code").Pluck("code", &codes).Error; err != nil {
		logging.Error("performance.Monster() Query codes Errors: ", err.Error())
		return
	}
	var tsBegin, tsEnd, rp string
	// test
	//day := calcDay(11, 1)
	day := calcDay(int(time.Now().Month()), time.Now().Day())
	switch {
	case day <= calcDay(3, 20):
		tsBegin = fmt.Sprintf("%d-12-21", time.Now().Year()-1)
		tsEnd = fmt.Sprintf("%d-03-21", time.Now().Year())
		rp = fmt.Sprintf("%d-03-31", time.Now().Year())
	case day > calcDay(12, 20):
		tsBegin = fmt.Sprintf("%d-12-21", time.Now().Year())
		tsEnd = fmt.Sprintf("%d-03-21", time.Now().Year()+1)
		rp = fmt.Sprintf("%d-03-31", time.Now().Year()+1)
	case day > calcDay(3, 20) && day <= calcDay(5, 20):
		tsBegin = fmt.Sprintf("%d-03-21", time.Now().Year())
		tsEnd = fmt.Sprintf("%d-05-21", time.Now().Year())
		rp = fmt.Sprintf("%d-06-30", time.Now().Year())
	case day > calcDay(5, 20) && day <= calcDay(9, 20):
		tsBegin = fmt.Sprintf("%d-05-21", time.Now().Year())
		tsEnd = fmt.Sprintf("%d-09-21", time.Now().Year())
		rp = fmt.Sprintf("%d-09-30", time.Now().Year())
	default:
		tsBegin = fmt.Sprintf("%d-09-21", time.Now().Year())
		tsEnd = fmt.Sprintf("%d-12-21", time.Now().Year())
		rp = fmt.Sprintf("%d-12-31", time.Now().Year())
	}
	days := 0
	var date string
	for _, v := range codes {
		var data []struct {
			StockCode string
			TradeDate time.Time
			Block     float64
			Change    float64
			PctChg    float64
			Open      float64
			Close     float64
			High      float64
			Low       float64
			PreClose  float64
			Amount    float64
			Name      string
		}
		if err := models.DB().Table("p_stock_tick").Select("p_stock_tick.*, b_stock_names.name").Joins("LEFT JOIN b_stock_names ON p_stock_tick.stock_code = b_stock_names.code").Where("p_stock_tick.stock_code IS NOT NULL").Where("p_stock_tick.stock_code = ?", v).Where("p_stock_tick.trade_date BETWEEN ? AND ?", tsBegin, tsEnd).Where("p_stock_tick.amount > 0").Order("p_stock_tick.trade_date").Find(&data).Error; err != nil {
			logging.Error("performance.Monster() Errors: ", err.Error())
			continue
		}
		// fix bug #173
		if days == 0 {
			days = len(data)
		}
		if date == "" && len(data) > 0 {
			date = data[0].TradeDate.Format("2006-01-02")
		}
		//if len(data) == 0 || len(data) < days && data[0].TradeDate.Format("2006-01-02") != date {
		//	logging.Error("performance.Monster() Lack days: ", v, len(data), days, tsBegin, tsEnd)
		//	continue
		//}
		if len(data) == 0 || data[0].Close == data[0].Block && newStock(v, tsBegin) {
			continue
		}

		blockCnt := 0
		//var wbp float64
		var bNew = false
		for i, u := range data {
			if u.PctChg > 20.1 {
				bNew = true
			}
			if u.Block == u.Close && !bNew {
				blockCnt++
			} else {
				blockCnt = 0
				bNew = false
			}
			if blockCnt >= 3 {
				store(u.StockCode, rp, "妖股")
				break
			}

			//weekday
			//if u.TradeDate.Weekday() == 1 || wbp == 0 {
			//	wbp = u.Close
			//}
			//if u.TradeDate.Weekday() == 5 || (i < len(data)-1 && data[i+1].TradeDate.Weekday() < u.TradeDate.Weekday()) {
			//	if wbp > 0 && u.Close/wbp > 1.4 {
			//		store(u.StockCode, rp, "妖股")
			//		break
			//	}
			//}

			//12.22 new method
			bMonster := false
			if i >= 4 {
				for k := 1; k <= 4; k++ {
					if data[i].Close > data[i-k].Close*1.4 {
						bMonster = true
						break
					}
				}
			}
			if bMonster {
				store(u.StockCode, rp, "妖股")
				break
			}
		}
	}
}

type PerfAnno struct {
	Code              string
	ReportPeriod      string
	NetProfitQuarter  float64
	NetProfitAfterDed float64
}

func HighRatio() {
	var codes []string
	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("performance.Monster() Query codes Errors: ", err.Error())
		return
	}
	var rp, pre, cur string
	day := calcDay(int(time.Now().Month()), time.Now().Day())
	switch {
	case day <= calcDay(3, 31):
		rp = fmt.Sprintf("%d-12-31", time.Now().Year()-1)
		pre = fmt.Sprintf("%d-12-31", time.Now().Year()-2)
		cur = fmt.Sprintf("%d-03-31", time.Now().Year())
	case day > calcDay(3, 31) && day <= calcDay(6, 30):
		rp = fmt.Sprintf("%d-03-31", time.Now().Year())
		pre = fmt.Sprintf("%d-03-31", time.Now().Year()-1)
		cur = fmt.Sprintf("%d-06-30", time.Now().Year())
	case day > calcDay(6, 30) && day <= calcDay(9, 30):
		rp = fmt.Sprintf("%d-06-30", time.Now().Year())
		pre = fmt.Sprintf("%d-06-30", time.Now().Year()-1)
		cur = fmt.Sprintf("%d-09-30", time.Now().Year())
	default:
		rp = fmt.Sprintf("%d-09-30", time.Now().Year())
		pre = fmt.Sprintf("%d-09-30", time.Now().Year()-1)
		cur = fmt.Sprintf("%d-12-31", time.Now().Year())
	}
	for _, v := range codes {
		var now, last PerfAnno
		models.DB().Table("u_performance_announcement").Where("report_period = ?", rp).Where("code = ?", v).First(&now)
		models.DB().Table("u_performance_announcement").Where("report_period = ?", pre).Where("code = ?", v).First(&last)
		if now.NetProfitQuarter > 0 && now.NetProfitAfterDed > 0 && last.NetProfitQuarter > 0 && last.NetProfitAfterDed > 0 {
			if now.NetProfitQuarter+now.NetProfitAfterDed > (last.NetProfitQuarter+last.NetProfitAfterDed)*2 {
				store(v, cur, "高比例")
			}
		}
	}
}

type UPT struct {
	models.Simple
	Code         string
	ReportPeriod string
	Type         string
}

func (UPT) TableName() string {
	return "u_performance_type"
}

func Reorganize() {
	var ro []struct {
		Code         string
		Name         string
		Date         *time.Time
		AffectPeriod *time.Time
	}

	if err := models.DB().Table("u_reorganization").Where("deleted_at IS NULL").Find(&ro).Error; err != nil {
		logging.Error("performance.Reorganize() Errors: ", err.Error())
		return
	}
	for _, v := range ro {
		var rp []string
		if v.AffectPeriod == nil {
			if v.Date == nil {
				continue
			}
			rp = calcPeriod(v.Date)
		} else {
			s := []string{
				"03-31",
				"06-30",
				"09-30",
				"12-31",
			}
			for _, u := range s {
				rp = append(rp, fmt.Sprintf("%d-%s", v.AffectPeriod.Year(), u))
			}
		}
		for _, w := range rp {
			data := UPT{
				Code:         v.Code,
				ReportPeriod: w,
				Type:         "并购重组",
			}
			err := models.DB().Where(&UPT{
				Code:         v.Code,
				ReportPeriod: w,
				Type:         "并购重组",
			}).Assign(&data).FirstOrCreate(&UPT{}).Error
			if err != nil {
				logging.Error("performance.Reorganize() Errors: ", err.Error())
				continue
			}
		}
	}
}

type PerfType struct {
	models.Simple
	Code         string
	ReportPeriod string
	Type         string
}

func (PerfType) TableName() string {
	return "u_performance_type"
}

func store(code, rp, tp string) {
	var data = PerfType{
		Code:         code,
		ReportPeriod: rp,
		Type:         tp,
	}
	var name []string
	models.DB().Table("b_stock_names").Where("code = ?", code).Pluck("name", &name)
	if len(name) == 0 {
		return
	}
	if strings.Contains(name[0], "ST") {
		return
	}

	if err := models.DB().Table("u_performance_type").Where(&data).Assign(data).FirstOrCreate(&PerfType{}).Error; err != nil {
		logging.Error("performance.store() Update Errors: ", err.Error())
	}
}

func calcDay(month, day int) int {
	return month*31 + day
}

func calcPeriod(t *time.Time) []string {
	var ret []string
	for i := 0; i < 4; i++ {
		year := int(t.Year())
		month := int(int(t.Month())/3)*3 + 3 + i
		if month > 12 {
			month = month - 12
			year = year + 1
		}
		day := 30
		if month == 3 || month == 12 {
			day = 31
		}
		ret = append(ret, fmt.Sprintf("%d-%02d-%d", year, month, day))
	}
	return ret
}

func newStock(code, date string) bool {
	var cnt int
	models.DB().Table("p_stock_tick").Where("stock_code = ?", code).Where("trade_date < ?", date).Count(&cnt)
	if cnt > 10 {
		return false
	}
	return true
}
