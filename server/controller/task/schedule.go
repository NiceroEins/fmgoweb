package task

import (
	"datacenter/controller/statistics"
	"datacenter/models"
	"datacenter/models/spider"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/parser"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"sync/atomic"

	"time"
)

// 0707: fix concurrent map read and map write
//var JobChan map[string]chan *models.Seed
var JobChan sync.Map
var SeedStaticKey string = "seed:static"
var SeedRunningKey string = "seed:running"
var SeedOPTKey string = "seed:opt"

func Start() {
	//从数据库中获取数据
	uSeeds, err := spider.FetchUSeedsFromMysql()
	if err != nil {
		logging.Error(err)
		return
	}

	normal := make(chan *spider.Seed, setting.SpiderSetting.PoolSize)
	strong := make(chan *spider.Seed, setting.SpiderSetting.PoolSize)
	//JobChan = make(map[string]chan *models.Seed)
	//JobChan["normal"] = normal
	//JobChan["strong"] = strong
	JobChan.Store("normal", normal)
	JobChan.Store("strong", strong)

	//运行前先清理running key
	_, err = gredis.Clone(setting.RedisSetting.SpiderDB).Delete(SeedRunningKey)
	if err != nil {
		logging.Error("清理running key 失败")
	}
	_, err = gredis.Clone(setting.RedisSetting.SpiderDB).Delete(SeedStaticKey)
	if err != nil {
		logging.Error("清理 static key 失败")
	}
	_, err = gredis.Clone(setting.RedisSetting.SpiderDB).Delete(SeedOPTKey)
	if err != nil {
		logging.Error("清理 opt key 失败")
	}
	//curl远方地址,获取数据,并且存入ProxyMap(代理地址)中
	spider.FetchProxyNew(0)
	//将SeedMap中的数据，在启动的时候,加入JobChan,使它俩数据相同 todo：目前每次重启都需要手动清理redis中的SeedMap
	for i := 0; i < len(uSeeds); i++ {
		_, errset := gredis.Clone(setting.RedisSetting.SpiderDB).SAdd(SeedOPTKey, strconv.Itoa(uSeeds[i].ID))
		if errset != nil {
			logging.Error("初始化"+SeedOPTKey+strconv.Itoa(uSeeds[i].ID)+"失败", errset.Error())
		}
		t := *spider.USeed2Seed(&uSeeds[i])
		go Work(t)
	}
	go BDSearch()
	go WorkMonitor()

}

//获取队列的当前信息
func Monitor() *spider.LaunchStatus {
	var ret spider.LaunchStatus
	seedCnt := 0
	proxyCnt := 0

	JobChan.Range(func(k, v interface{}) bool {
		var status string
		value, ok := v.(chan *spider.Seed)
		if !ok {
			logging.Error("schedule.Monitor() range JobChan Errors")
			return false
		}
		switch {
		case len(value) >= cap(value):
			status = "full"
		case len(value) == 0:
			status = "empty"
		default:
			status = "ok"
		}
		ret.Task = append(ret.Task, spider.ElemStatus{
			Name:   k.(string),
			Length: len(value),
			Status: status,
		})
		return true
	})
	//for k, v := range JobChan {
	//
	//	var status string
	//	switch {
	//	case len(v) >= cap(v):
	//		status = "full"
	//	case len(v) == 0:
	//		status = "empty"
	//	default:
	//		status = "ok"
	//	}
	//	ret.Task = append(ret.Task, models.ElemStatus{
	//		Name:   k,
	//		Length: len(v),
	//		Status: status,
	//	})
	//}

	//计算SeedMap中有多少键值对
	seedMap, err := gredis.HGetAllStringMap("SeedMap")
	if err != nil {
		logging.Error("Monitor 从redis SeedMap 中获取数据失败:", err.Error())
	}
	seedCnt = len(seedMap)

	//计算ProxyMap中有多少键值对
	proxyMap, err := gredis.HGetAllStringMap("ProxyMap")
	if err != nil {
		logging.Error("Monitor 从redis ProxyMap 中获取数据失败:", err.Error())
	}
	proxyCnt = len(proxyMap)

	ret.Seed = append(ret.Seed, spider.ElemStatus{
		Name:   "seed",
		Length: seedCnt,
		Status: "ok",
	})
	ret.Proxy = append(ret.Proxy, spider.ElemStatus{
		Name:   "proxy",
		Length: proxyCnt,
		Status: "ok",
	})
	return &ret
}

func WorkMonitor() {
	var (
		ticker = time.NewTicker(time.Second)
	)
	logging.Info("WorkMonitor 监控 开启:")
	time.Sleep(3 * time.Second)
	for {
		<-ticker.C
		var (
			lock int64 = 0
			ids  []int
		)
		diff, err := gredis.Clone(setting.RedisSetting.SpiderDB).SDiffString(SeedOPTKey, SeedRunningKey)
		if err != nil {
			logging.Error("Work 监控 redis取差集报错:", err.Error())
		}
		for _, v := range diff {
			id, coverr := strconv.Atoi(v)
			if coverr != nil {
				logging.Error("Work 监控 "+v+" Atoi报错", coverr.Error())
				continue
			}
			ids = append(ids, id)
		}
		if ids != nil {
			if !atomic.CompareAndSwapInt64(&lock, 0, 1) {
				continue
			}
			for _, id := range ids {
				seed, errRedis := GetSeedFromRedis(strconv.Itoa(id))
				if errRedis != nil || seed == nil {
					logging.Error("Work 监控 新增源报错", errRedis.Error())
					//atomic.StoreInt64(&lock, 0)
					continue
				}
				go Work(*seed)
				seed = nil
			}
			ids = nil
			diff = nil
			atomic.StoreInt64(&lock, 0)
		}
	}
}

func SaveSeed2Redis(idStr string, seed spider.Seed) error {
	seedJson, ujserr := json.Marshal(seed)
	if ujserr != nil {
		logging.Error("Seed" + idStr + "转json失败:" + ujserr.Error())
	}
	err := gredis.Clone(setting.RedisSetting.SpiderDB).HSet(SeedStaticKey, idStr, string(seedJson))

	if err != nil {
		logging.Error("存入redis " + SeedStaticKey + idStr + " 失败:" + err.Error())
		return err
	}
	return nil
}

func GetSeedFromRedis(idStr string) (*spider.Seed, error) {
	var (
		conn = gredis.Clone(setting.RedisSetting.SpiderDB)
		seed spider.Seed
	)
	js, err := conn.HGetBytes(SeedStaticKey, idStr)
	if js == nil {
		return nil, errors.New("GetSeedFromRedis()获取:" + idStr + "失败")
	}
	errJs := json.Unmarshal(js, &seed)
	if errJs != nil {
		logging.Error("GetSeedFromRedis" + SeedStaticKey + idStr + " json 解析失败:" + errJs.Error())
		return &seed, err
	}
	return &seed, nil
}

// each source has its own work routine
func Work(uSeed spider.Seed) {
	var conn = gredis.Clone(setting.RedisSetting.SpiderDB)
	if uSeed.CatchInterval < setting.SpiderSetting.MinCatchInterval {
		uSeed.CatchInterval = setting.SpiderSetting.MinCatchInterval
	}
	seedTicker := time.NewTicker(time.Duration(uSeed.CatchInterval) * time.Second)
	idStr := strconv.Itoa(uSeed.ID)

	err := SaveSeed2Redis(idStr, uSeed)
	if err != nil {
		return
	}
	var s *spider.Seed
	s = uSeed.Filter()
	if !s.Available() {
		_ = conn.HDel("SeedMap", idStr)
		logging.Error("源：" + idStr + " 不可用 删除opt")
		_ = conn.SREM(SeedOPTKey, idStr)
		return
	}
	// fmt.Printf("seed.id:%#v\n",seed.ID)
	// fmt.Printf("seed:%#v\n",seed)

	// 周五新增:redis里面新增一个SeedStatic,把seed_id,类型存入,只存不删(暂时过期时间为一周),
	// err := gredis.Set("SeedStatic:"+strconv.Itoa(seed.ID), seed, 24*60*60*7)

	ok, setErr := conn.SAdd(SeedRunningKey, idStr)
	if setErr != nil {
		logging.Error("Work 记录 "+SeedRunningKey+idStr+" 失败", setErr.Error())
		return
	}
	if !ok {
		logging.Info("Work " + SeedRunningKey + idStr + "重复启动退出 ")
		return
	}

	defer func() {
		logging.Info(SeedRunningKey + idStr + " exit")
		_ = conn.SREM(SeedRunningKey, idStr)
	}()

	logging.Info("start woker" + idStr)
	// 不停的入队 JobChan
	for {
		// 检查从数据库中查出的id,是否在SeedMap中,如果有,则跳过,没有则存入JobChan
		// ok := gredis.HExists("SeedMap", strconv.Itoa(uSeed.ID))
		// ok := false
		// if !ok {
		// SeedMap中不存在时候, 入JobChan ,并且存入redis

		//// 生成任务统计个数
		//go DayProductTaskNum()
		//// 最近一小时生成任务统计
		//go HourProductTaskNum()
		//// 最近10s 生成任务统计
		//go SecondProductTaskNum()
		go statistics.ProduceTask()
		var uS *spider.Seed

		uS, errRedis := GetSeedFromRedis(idStr)
		if uS == nil {
			logging.Info("redis" + SeedStaticKey + idStr + " 空值 worker退出")
			return
		}
		if errRedis != nil {
			logging.Error("reids "+SeedStaticKey+idStr+" 获取失败:", errRedis.Error())
			continue
		}
		if uSeed.CatchInterval != uS.CatchInterval {
			if uS.CatchInterval > 0 {
				seedTicker.Stop()
				seedTicker = time.NewTicker(time.Duration(uS.CatchInterval) * time.Second)
				uSeed.CatchInterval = uS.CatchInterval
			} else {
				logging.Error(idStr + " 更新非法爬取时间")
			}
		}
		seed := uS.Filter()

		// 0617 fix: 特殊url处理
		// 0703 fix: 日报时间未动态更新
		seed.Url = parser.Parse(seed.Source)

		// 存入JobChan
		// fmt.Printf("seed:%#v,type:%v", seed, seed.Mode)
		// u_seed 特殊url处理

		ok := TryEnqueue(seed, seed.Mode) // 入JobChan成功

		if ok {
			// 0702 add: backup list
			// 1217 add: copy2 temp list
			if seed.Mode == "normal" {
				_ = TryEnqueue(seed, seed.Mode+"-copy2")
			}
			// 存入redis
			seedjson, err := json.Marshal(seed)
			if err != nil {
				logging.Error("Work seed结构体转json失败")
			}
			err = conn.HSet("SeedMap", strconv.Itoa(uSeed.ID), seedjson)
			if err != nil {
				logging.Error("Work SeedMap redis 入队失败:", err.Error())
			}
		}
		// } else {
		// todo: 任务堆积率计算

		// }

		<-seedTicker.C

	}

}

var BDKeywords sync.Map

func BDSearch() {
	if setting.ServerSetting.Type != "spider" {
		return
	}
	ticker := time.NewTicker(5 * time.Second)
	for {
		<-ticker.C
		keys := spider.BDKeyword()
		for _, v := range keys {
			if _, ok := BDKeywords.Load(v); ok {
				continue
			}
			useed := spider.USeed{
				Simple: models.Simple{ID: int(util.BKDRHash(v))},
				// ID:                 0,
				CatchSite:          "百度搜索",
				CatchPath:          "百度搜索",
				CatchLink:          fmt.Sprintf("https://www.baidu.com/s?ie=utf-8&cl=2&medium=1&rtt=4&bsst=1&rsv_dl=news_t_sk&tn=news&wd=%s&tfflag=0", v),
				Star:               3,
				Cata:               "#12#",
				Type:               "baidu",
				CatchInterval:      60,
				SelectorPageLink:   "div#content_left div div.result h3.c-title a",
				SelectorTitle:      "",
				SelectorTime:       "",
				SelectorSourceName: "",
				SelectorSourceLink: "",
				SelectorListTime:   "div#content_left div div.result div.c-summary.c-row p.c-author",
				SelectorListTitle:  "",
				CatchMode:          "special",
				Status:             "normal",
				Keyword:            v,
			}
			go Work(*spider.USeed2Seed(&useed))
			BDKeywords.Store(v, useed)
		}
	}
}

//JobChan<-seed:seed入队
func TryEnqueue(seed *spider.Seed, seedType string) bool {
	ch, ok := JobChan.Load(seedType)
	//ch, ok := JobChan[seedType]
	if !ok {
		ch = make(chan *spider.Seed, setting.SpiderSetting.PoolSize)
		JobChan.Store(seedType, ch)
		//JobChan[seedType] = ch
	}

	select {
	case ch.(chan *spider.Seed) <- seed:
		return true
	default:
		return false
	}
}

//<-JobChan:seed出队
//发任务:统计seeds长度
func Dequeue(seedNum int, seedType string) []*spider.Seed {
	seeds := make([]*spider.Seed, 0)
	for i := 0; i < seedNum; i++ {
		var seed *spider.Seed = nil
		//ch, ok := JobChan[seedType]
		ch, ok := JobChan.Load(seedType)
		if ch == nil || !ok {
			continue
		}
		//if seedType == "" {
		//	select {
		//	case seed = <-JobChan["normal"]:
		//		seeds = append(seeds, seed)
		//	case seed = <-JobChan["strong"]:
		//		seeds = append(seeds, seed)
		//	default:
		//		break
		//	}
		//} else if ok {
		select {
		case seed, _ = <-ch.(chan *spider.Seed):
			seeds = append(seeds, seed)
		default:
			break
		}
		//}
		if seed != nil {
			//SeedMap中移除
			err := gredis.HDelField("SeedMap", strconv.Itoa(seed.ID))
			if err != nil {
				logging.Error("Remove SeedMap failed: ", err.Error())
			}
		}
	}

	return seeds
}

//统计 day生成任务个数
func DayProductTaskNum() {
	timeNow := util.GenSecondTime()
	_, timeEnd := util.GenBeginEndTime()
	expireTime := timeEnd - timeNow

	gredis.IncrBy("day_product_task_num", 1)
	gredis.Expire("day_product_task_num", expireTime)

}

//统计 day 发任务个数
func DaySendTaskNum(num int) {
	timeNow := util.GenSecondTime()
	_, timeEnd := util.GenBeginEndTime()
	expireTime := timeEnd - timeNow

	gredis.IncrBy("day_send_task_num", num)
	gredis.Expire("day_send_task_num", expireTime)
}

//统计 hour生成任务个数
func HourProductTaskNum() {
	timeSecondNow := util.GenSecondTime()
	key := timeSecondNow % 3600

	gredis.IncrBy("hour_product_task_num:"+strconv.FormatInt(key, 10), 1)
	gredis.Expire("hour_product_task_num:"+strconv.FormatInt(key, 10), 3599)
}

//统计 hour 发任务个数
func HourSendTaskNum(num int) {
	timeSecondNow := util.GenSecondTime()
	key := timeSecondNow % 3600

	gredis.IncrBy("hour_send_task_num:"+strconv.FormatInt(key, 10), num)
	gredis.Expire("hour_send_task_num:"+strconv.FormatInt(key, 10), 3599)
}

//统计 10s second 生成任务个数
func SecondProductTaskNum() {
	timeSecondNow := util.GenSecondTime()
	key := timeSecondNow % 10

	gredis.IncrBy("second_product_task_num:"+strconv.FormatInt(key, 10), 1)
	gredis.Expire("second_product_task_num:"+strconv.FormatInt(key, 10), 9)
}

//统计 10s second 发任务个数
func SecondSendTaskNum(num int) {
	timeSecondNow := util.GenSecondTime()
	key := timeSecondNow % 10

	gredis.IncrBy("second_send_task_num:"+strconv.FormatInt(key, 10), num)
	gredis.Expire("second_send_task_num:"+strconv.FormatInt(key, 10), 9)
}
