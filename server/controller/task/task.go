package task

import (
	"datacenter/models/spider"
	"datacenter/pkg/util"
)

//var TaskMap sync.Map // string, *Task
func GetTask(taskNum int, taskType string) *spider.Task {
	t := spider.Task{
		TaskID: GenerateTaskID(),
	}

	//todo 周五新增:先从Jobchan的类型_failed chan中获取,有则直接返回,没有则继续往下走
	seedsFailed := Dequeue(taskNum, taskType+"_failed")
	if len(seedsFailed) > 0 {
		t.AppendSeeds(seedsFailed)
		return &t
	} else {
		// 从JobChan中获取数据seed
		seeds := Dequeue(taskNum, taskType)

		//day发任务统计个数
		//incrByNum := len(seeds)

		//go DaySendTaskNum(incrByNum)
		//go HourSendTaskNum(incrByNum)
		//go SecondSendTaskNum(incrByNum)

		if seeds == nil || len(seeds) == 0 {
			t.Seed = make([]*spider.Seed, 0)
			return &t
		}

		t.AppendSeeds(seeds)
		//从ProxyMap中取出proxy
		proxy := GetProxy(1)

		if len(proxy) > 0 {
			t.AppendProxy(proxy[0])
		}
	}

	t.SetLog()

	//// fix bug: t.Proxy will possibly be nil
	//if t.Proxy != nil {
	//	if err := gredis.HSet("TaskMap", t.TaskID, t.Proxy.Ip); err != nil {
	//		logging.Error("task.go GetTask():TaskMap存入数据失败:", err.Error())
	//		return nil
	//	}
	//}

	return &t
}

//生成随机32位的任务id
func GenerateTaskID() string {
	return util.GenerateRandomString(32)
}
