package task

import (
	"datacenter/models/spider"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"encoding/json"
	"time"
)

// var ProxyMap sync.Map //(string, Proxy)
// 从ProxyMap中获取对应个数的proxy
// 0609 fix: bug #14
func GetProxy(requiredNum int) []*spider.Proxy {

	ret := make([]*spider.Proxy, 0)

	proxyMap := CheckMap()
	cur := len(proxyMap)
	if cur < spider.AutoProxy() {
		if spider.FetchProxyNew(cur) == 0 {
			logging.Error("proxy.FetchProxy() zero proxy fetched")
		}
	}

	////改造: 遍历ProxyMap，返回有效的数据
	//ProxyData, err := gredis.HGetAll("ProxyMap")
	//if err != nil {
	//	logging.Error("proxy.go GetProxy():获取全部ProxyMap数据失败:" + err.Error())
	//}

	// fix bugs in allocate proxies
	count := 0
	for _, v := range proxyMap {
		//if count%2 != 0 {
		//	// redis hash 值
		//	var proxy Proxy
		//	err := json.Unmarshal(v.([]byte), &proxy)
		//	if err != nil {
		//		logging.Error("proxy.go GetProxy():json转Proxy结构体失败:" + err.Error())
		//	}
		//	ret = append(ret, &proxy)
		//}
		//count++
		if count < requiredNum {
			ret = append(ret, v)
		} else {
			break
		}
	}

	return ret
}

// 检查map
// 0609 fix: bug #14
func CheckMap() map[string]*spider.Proxy {
	ret := make(map[string]*spider.Proxy)
	//遍历ProxyMap
	proxyMap, err := gredis.HGetAllStringMap("ProxyMap")
	if err != nil {
		logging.Error("proxy.go CheckMap():获取ProxyMap全部数据失败:" + err.Error())
	}

	for k, v := range proxyMap {
		//redis中的值,遍历ProxyMap，和当前时间做比较,小于当前时间的,全部从ProxyMap中移除
		var proxy spider.Proxy
		err := json.Unmarshal([]byte(v), &proxy)
		if err != nil {
			logging.Error("proxy.go CheckMap():json转proxy结构失败:" + err.Error())
			continue
		}

		et, err := time.ParseInLocation("2006-01-02 15:04:05", proxy.ExpireTime, time.Local)
		if err != nil {
			logging.Error("Invalid Time format")
			continue
		}

		//12.21 过期前30秒内的全部移除
		//if et.Before(time.Now()) {
		if et.Sub(time.Now()) < 30*time.Second {
			//logging.Info(fmt.Sprintf("%s %s has expired", proxy.Ip, proxy.ExpireTime))
			err = gredis.HDelField("ProxyMap", k)
			if err != nil {
				logging.Error("proxy.go CheckMap():ProxyMap移除数据失败:", err.Error())
			}
			continue
		}

		ret[k] = &proxy
	}
	return ret
}
