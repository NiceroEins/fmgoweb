package northward

import (
	"datacenter/models/northward"
	"datacenter/pkg/util"
)

func GetList(qo *northward.NorthWardQO) (northward.VOList, int, error) {
	dtos := northward.NewDTOList()
	vos := northward.NewVOList()
	cnt, err := dtos.GetList(qo)
	vos.DTOList2VOList(dtos)
	return vos, cnt, err
}

func GetLatestTime() (string,error) {
	time,err:= northward.GetLatestTime()
	if err != nil {
		return "", err
	}
	return time.Format(util.YMD),nil
}
