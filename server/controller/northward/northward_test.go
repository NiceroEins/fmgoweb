package northward

import (
	"datacenter/models"
	"datacenter/models/northward"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"datacenter/pkg/setting"
	"fmt"
	"testing"
)

func TestNorthWardGetList(t *testing.T) {
	setting.Setup()
	models.Setup()
	mongo.Setup()
	gredis.Setup()
	logging.Setup()
	dtos:=northward.NewDTOList()
	vos:=northward.NewVOList()
	qo:=northward.NorthWardQO{
		SortKey: 1,
		Direction: true,
	}
	dtos.GetList(&qo)
	vos.DTOList2VOList(dtos)
	fmt.Println(333)
}
