package stock

import (
	"datacenter/models"
	"datacenter/models/performance"
	"datacenter/models/quantization"
	"datacenter/models/stock"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/mongo"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"fmt"
	"github.com/shopspring/decimal"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"strings"
	"time"
)

type Limit struct {
	Code   string
	HP     string `gorm:"column:highest_price"`
	Market string
}

func fetchLimit() error {
	var data []Limit
	if err := models.DB().Table("p_history_stock_limit").Find(&data).Error; err != nil {
		logging.Error("stock.fetchLimit() Errors: ", err.Error())
		return err
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, v := range data {
		conn.HSet("stock:limit:highest_price", v.Market+v.Code, v.HP)
	}
	return nil
}

func fetch30DayStock() error {
	return nil
}

func fetchCompanyIndustry() {
	/*var data []struct {
		StockCode string
		Industry  string
		Ltg       float64
		Zgb       float64
		Business  string
	}*/
	var zgbList []map[string]interface{}
	var ltgList []map[string]interface{}
	var industryList []map[string]string
	stockBaseInfoMap := map[string]stock.StockBaseInfoIO{}
	/*if err := models.DB().Table("p_public_company").Find(&data).Error; err != nil {
		logging.Error("stock.fetchCompanyIndustry() Errors: ", err.Error())
		return
	}*/
	err := mongo.GetMongoDB("shares", "F10_capitalStructure", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{"constitute": "总股本(股)"}).All(&zgbList)
	})
	if err != nil {
		logging.Error("mongo 获取总股本报错", err.Error())
	}
	err = mongo.GetMongoDB("shares", "F10_capitalStructure", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{"constitute": "流通A股(股)"}).All(&ltgList)
	})
	if err != nil {
		logging.Error("mongo 获取流通A股(股)报错", err.Error())
	}
	err = mongo.GetMongoDB("shares", "F10_companyInfo", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{}).Select(bson.M{"code": 1, "main_camp": 1, "industry": 1}).All(&industryList)
	})
	if err != nil {
		logging.Error("mongo 获取行业和主营报错", err.Error())
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, line := range zgbList {
		var code string
		var lastTime time.Time
		for k, v := range line {
			if k == "code" {
				code = v.(string)
			}
			if newTime, err := time.ParseInLocation(util.YMD, k, time.Local); err == nil {
				if newTime.After(lastTime) && time.Now().After(newTime) {
					lastTime = newTime
				}
			}
		}
		if zgb, ok := line[lastTime.Format(util.YMD)]; ok {
			zgbint64 := util.ToInt64(zgb)
			conn.HSet("stock:zgb", code, zgbint64)
			dZgb := decimal.NullDecimal{}
			dZgb.Scan(zgb)
			if v, ok := stockBaseInfoMap[code]; ok {
				v.Zgb = dZgb
				stockBaseInfoMap[code] = v
			} else {
				stockBaseInfoMap[code] = stock.StockBaseInfoIO{
					Code: code,
					Zgb:  dZgb,
				}
			}
		} else {
			logging.Error(code + "zgb mongo获取失败")
		}
	}
	for _, line := range ltgList {
		var code string
		var lastTime time.Time
		for k, v := range line {
			if k == "code" {
				code = v.(string)
			}
			if newTime, err := time.ParseInLocation(util.YMD, k, time.Local); err == nil {
				if newTime.After(lastTime) && time.Now().After(newTime) {
					lastTime = newTime
				}
			}
		}
		if ltg, ok := line[lastTime.Format(util.YMD)]; ok {
			ltgint64 := util.ToInt64(ltg)
			conn.HSet("stock:ltg", code, ltgint64)
			dLtg := decimal.NullDecimal{}
			dLtg.Scan(ltg)
			if v, ok := stockBaseInfoMap[code]; ok {
				v.Ltg = dLtg
				stockBaseInfoMap[code] = v
			} else {
				stockBaseInfoMap[code] = stock.StockBaseInfoIO{
					Code: code,
					Ltg:  dLtg,
				}
			}
		} else {
			logging.Error(code + "ltg mongo获取失败")
		}
	}
	stockIndeustryList := make([]string, 0)
	stockIndustryMap := make(map[string]struct{}, 0)
	err = models.DB().Table("u_stock_industry").Pluck("code", &stockIndeustryList).Error
	if err != nil {
		logging.Error("获取u_stock_industry code 列表失败", err.Error())
	}
	sharesInfoCodes := make([]string, 0)
	sharesInfoMap := make(map[string]struct{}, 0)
	err = models.DB().Table("u_shares_info").
		Where("state_id!=301007 and state_id!=301006").
		Pluck("left(code,6)", &sharesInfoCodes).Error
	for _, code := range sharesInfoCodes {
		sharesInfoMap[code] = struct{}{}
	}
	for _, code := range stockIndeustryList {
		stockIndustryMap[code] = struct{}{}
	}
	for _, line := range industryList {
		//var code string
		if code, ok := line["code"]; ok {
			//去除B股
			if strings.HasPrefix(code, "9") || strings.HasPrefix(code, "2") {
				continue
			}
			//剔除退市和未上市股票
			if _, ok := sharesInfoMap[code]; !ok {
				continue
			}
			//util.ToFloat64()
			if industry, ok := line["industry"]; ok {
				industry := strings.Split(strings.TrimRight(industry, " "), " ")
				conn.HSet("stock:industry:industry", code, industry[len(industry)-1])
				if _, ok := stockIndustryMap[code]; !ok {
					tx := models.DB().Begin()
					err = tx.Exec(fmt.Sprintf("INSERT INTO u_stock_industry (code,first_industry,second_industry,actual_industry,industry_id) VALUES ('%s','%s','%s','%s','%d')", code, performance.DefaultFirstIndustry, performance.DefaultSecondIndustry, performance.DefaultSecondIndustry, performance.DefaultIndustryId)).Error
					if err != nil {
						tx.Rollback()
						logging.Error(fmt.Sprintf("INSERT INTO u_stock_industry (code,first_industry,second_industry,actual_industry,industry_id) VALUES ('%s','%s','%s','%s','%d')", code, performance.DefaultFirstIndustry, performance.DefaultSecondIndustry, performance.DefaultSecondIndustry, performance.DefaultIndustryId)+"执行失败", err.Error())
					} else {
						err = tx.Exec(fmt.Sprintf("INSERT INTO u_stock_industry_relationship (stock_id,industry_id) VALUES ('%s','%d')", code, performance.DefaultIndustryId)).Error
						if err != nil {
							tx.Rollback()
							logging.Error(fmt.Sprintf("INSERT INTO u_stock_industry (code,first_industry,second_industry,actual_industry,industry_id) VALUES ('%s','%s','%s','%s','%d')", code, performance.DefaultFirstIndustry, performance.DefaultSecondIndustry, performance.DefaultSecondIndustry, performance.DefaultIndustryId)+"执行失败", err.Error())
						} else {
							tx.Commit()
						}
					}
				}
			} else {
				logging.Error(code + "industry:industry mongo获取失败")
			}
			if business, ok := line["main_camp"]; ok {
				conn.HSet("stock:industry:business", code, business)
			} else {
				logging.Error(code + "industry:business mongo获取失败")
			}
		}
	}
	for k, v := range stockBaseInfoMap {
		err := models.DB().Where("code=?", k).Assign(&v).FirstOrCreate(&stock.StockBaseInfoDTO{}).Error
		if err != nil {
			logging.Error("更新stockbaseinfo 报错", err.Error())
		}
	}
	/*conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, v := range data {
		industry := strings.Split(strings.TrimRight(v.Industry, " "), " ")
		conn.HSet("stock:industry:industry", v.StockCode, industry[len(industry)-1])
		conn.HSet("stock:industry:business", v.StockCode, v.Business)
		conn.HSet("stock:ltg", v.StockCode, int64(v.Ltg))
		conn.HSet("stock:zgb", v.StockCode, int64(v.Zgb))
		fetchIndustry(v.StockCode)
	}*/
}

func fetchIndustry(code string) {
	var industry []struct {
		ID           int
		IndustryName string
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	if err := models.DB().Table("u_performance_industry").Select("u_performance_industry.id, u_performance_industry.industry_name").Joins("LEFT JOIN u_stock_industry_relationship ON u_performance_industry.id = u_stock_industry_relationship.industry_id").Where("u_stock_industry_relationship.stock_id = ?", code).Find(&industry).Error; err == nil {
		if len(industry) > 0 {
			ret := strings.Split(industry[0].IndustryName, "-")
			switch len(ret) {
			case 0:
				return
			case 1:
				conn.HSet("stock:industry:industry", code, industry[0].IndustryName)
			case 2:
				conn.HSet("stock:industry:industry", code, industry[0].IndustryName)
			default:
				conn.HSet("stock:industry:industry", code, strings.Join(ret[len(ret)-2:], "-"))
			}
		}
	}
}

func fetchPlatform() {
	var codes []string
	var maDays int = 20
	var watchPeriod int = 10 + maDays
	var dailyPctg float64 = 1.08
	var watchPctg float64 = 1.1
	var limitLowerPctg float64 = 0.92
	var limitLowerPeriod int = 3
	var limitBreakMA int = 3
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:platform")
	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("stock.fetchPlatform() Query codes Errors: ", err.Error())
		return
	}
	for _, v := range codes {
		var data []struct {
			StockCode string
			TradeDate string
			Open      float64
			Close     float64
			High      float64
			Low       float64
			PreClose  float64
			Amount    float64
			Name      string
		}
		if err := models.DB().Table("p_stock_tick").Select("p_stock_tick.*, b_stock.name").Joins("LEFT JOIN b_stock ON p_stock_tick.stock_code = b_stock.id").Where("p_stock_tick.stock_code = ?", v).Order("p_stock_tick.trade_date DESC").Limit(watchPeriod).Find(&data).Error; err != nil {
			logging.Error("stock.fetchCompanyIndustry() Errors: ", err.Error())
			continue
		}
		if len(data) < watchPeriod {
			continue
		}
		if strings.Index(data[0].Name, "st") != -1 || strings.Index(data[0].Name, "ST") != -1 {
			continue
		}
		fmt.Printf("Fetching %s\n", data[0].StockCode)
		var high float64 = 0
		var low float64 = 9999
		bPlatform := true
		curFall := 0

		//剔除证券、银行
		industry, _ := conn.HGetString("stock:industry:industry", data[0].StockCode)
		if strings.Contains(industry, "银行") || strings.Contains(industry, "证券") || strings.Contains(data[0].Name, "中国石油") || strings.Contains(data[0].Name, "中国石化") {
			continue
		}

		// reserve
		for k, _ := range data {
			if k == watchPeriod/2 {
				break
			}
			data[k], data[watchPeriod-k-1] = data[watchPeriod-k-1], data[k]
		}

		breakMA := 0
		maxHigh := 0.0
		for k, w := range data {
			if k < maDays {
				continue
			}
			if w.Close > high {
				high = w.Close
			}
			if w.Open > high {
				high = w.Open
			}
			if w.Open < low {
				low = w.Open
			}
			if w.Close < low {
				low = w.Close
			}
			if w.High > w.Low*dailyPctg {
				// 当日振幅
				bPlatform = false
				break
			}
			// 连续5个阴线
			if k > 0 && w.Close < w.Open {
				curFall++
				if curFall >= 5 {
					bPlatform = false
					break
				}
				if curFall > 0 && k > limitLowerPeriod-1 {
					if w.Close < data[k-limitLowerPeriod].Close*limitLowerPctg {
						bPlatform = false
						break
					}
				}
			} else {
				curFall = 0
			}

			//去除有过涨跌停
			if w.High > w.PreClose*1.1-0.01 || w.Low < w.PreClose*0.9+0.01 {
				bPlatform = false
				break
			}

			//每日成交额大于2亿（amount单位千元）
			if w.Amount < 200000 {
				bPlatform = false
				break
			}

			//每日收盘价大于20日均线
			//跌破次数小于3次
			mas := 0.0
			for i := k - maDays + 1; i <= k; i++ {
				mas += data[i].Close
			}
			ma := mas / float64(maDays)
			if w.Close < ma {
				breakMA++
				if breakMA > limitBreakMA {
					bPlatform = false
					break
				}
			}

			//计算最高价（含盘中）
			if w.High > maxHigh {
				maxHigh = w.High
			}
		}
		if bPlatform {
			if high <= low*watchPctg {
				// 振幅大于10%
				conn.HSet("stock:platform", data[0].StockCode, maxHigh)
				//fmt.Printf("Platform access %s\n", data[0].StockCode)
				tpo := time.Now().Add(24 * time.Hour)
				go stockPool(data[0].StockCode, "筑平台", conn, time.Date(tpo.Year(), tpo.Month(), tpo.Day(), 0, 0, 0, 0, time.Local))
			}
		}
	}

}

func positivity() {
	var codes []string
	var watchPeriod int = 4
	var positiveRate float64 = 1.03
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:positivity")
	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("stock.fetchPlatform() Query codes Errors: ", err.Error())
		return
	}
	for _, v := range codes {
		var data []struct {
			StockCode string
			TradeDate string
			Open      float64
			Close     float64
			High      float64
			Low       float64
			PreClose  float64
			Amount    float64
			Name      string
		}
		if err := models.DB().Table("p_stock_tick").Select("p_stock_tick.*, b_stock.name").Joins("LEFT JOIN b_stock ON p_stock_tick.stock_code = b_stock.id").Where("p_stock_tick.stock_code = ?", v).Order("p_stock_tick.trade_date DESC").Limit(watchPeriod).Find(&data).Error; err != nil {
			logging.Error("stock.fetchCompanyIndustry() Errors: ", err.Error())
			continue
		}

		//剔除银行
		industry, _ := conn.HGetString("stock:industry:industry", data[0].StockCode)
		if strings.Contains(industry, "银行") || strings.Contains(data[0].Name, "中国石油") || strings.Contains(data[0].Name, "中国石化") {
			continue
		}

		//// reserve
		//for k, _ := range data {
		//	if k == watchPeriod/2 {
		//		break
		//	}
		//	data[k], data[watchPeriod-k-1] = data[watchPeriod-k-1], data[k]
		//}

		ok := true
		for _, w := range data {
			if w.Close <= w.Open || w.Close > w.Open*positiveRate {
				ok = false
				break
			}
			if w.Low <= w.PreClose*0.97 || w.High >= w.PreClose*1.05 {
				ok = false
				break
			}
			if w.Open > w.PreClose*1.03 {
				ok = false
				break
			}
			//每日成交额大于1亿（amount单位千元）
			if w.Amount <= 100000 {
				ok = false
				break
			}
		}
		if ok {
			conn.HSet("stock:positivity", data[0].StockCode, data[0].Name)
			//fmt.Printf("Positivity access %s\n", data[0].StockCode)
			tpo := time.Now().Add(24 * time.Hour)
			go stockPool(data[0].StockCode, "连续小阳线", conn, time.Date(tpo.Year(), tpo.Month(), tpo.Day(), 0, 0, 0, 0, time.Local))
		}
	}
}

func updateLimit() {
	var data []struct {
		StockCode string
		TsCode    string
		TradeDate string
		Open      float64
		Close     float64
		High      float64
		Low       float64
		PreClose  float64
		Amount    float64
	}
	if err := models.DB().Table("p_stock_tick").Where("trade_date = ?", time.Now().Format("2006-01-02")).Find(&data).Error; err != nil {
		logging.Error("stock.UpdateLimit() Fetch Errors: ", err.Error())
		return
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, v := range data {
		v.TsCode = convertTs(v.TsCode)
		cs := strings.Split(v.TsCode, ".")
		//tsCode := cs[1] + cs[0]
		code := cs[0]
		hp, _ := conn.HGetFloat64("stock:limit:highest_price", code)
		sHigh := fmt.Sprintf("%.2f", v.High)
		if v.High > hp {
			conn.HSet("stock:limit:highest_price", code, sHigh)
			logging.Info("stock.UpdateLimit() revised: ", code, hp, sHigh)
		}
		lp, _ := conn.HGetFloat64("stock:limit:last_highest", code)
		if v.High > lp {
			conn.HSet("stock:limit:last_highest", code, sHigh)
		}
	}
}

func fetchBlock() {
	var data []struct {
		StockCode string
		TsCode    string
		TradeDate string
		Close     float64
		Block     float64
	}
	if err := models.DB().Table("p_stock_tick").Where("trade_date = ?", time.Now().Format("2006-01-02")).Find(&data).Error; err != nil {
		logging.Error("stock.UpdateLimit() Fetch Errors: ", err.Error())
		return
	}
	if len(data) > 0 {
		conn := gredis.Clone(setting.RedisSetting.StockDB)
		conn.Delete("stock:last_block")
		for _, v := range data {
			v.TsCode = convertTs(v.TsCode)
			cs := strings.Split(v.TsCode, ".")
			//tsCode := cs[1] + cs[0]
			code := cs[0]
			if v.Close == v.Block {
				conn.HSet("stock:last_block", code, time.Now().Format("2006-01-02"))
			}
		}
	}
}

func blockPlatform() {
	var codes []string
	var watchPeriod int = 30
	conn := gredis.Clone(setting.RedisSetting.StockDB)

	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("stock.blockPlatform() Query codes Errors: ", err.Error())
		return
	}
	for _, v := range codes {
		var data []struct {
			StockCode string
			TradeDate string
			Open      float64
			Close     float64
			Block     float64
			High      float64
			Low       float64
			PreClose  float64
			Amount    float64
			Name      string
		}
		if err := models.DB().Table("p_stock_tick").Select("p_stock_tick.*, b_stock.name").Joins("LEFT JOIN b_stock ON p_stock_tick.stock_code = b_stock.id").Where("p_stock_tick.stock_code = ?", v).Order("p_stock_tick.trade_date DESC").Limit(watchPeriod).Find(&data).Error; err != nil {
			logging.Error("stock.blockPlatform() Errors: ", err.Error())
			continue
		}

		//剔除新股
		if len(data) <= 1 || []byte(data[0].Name)[0] == 'N' {
			continue
		}

		//reverse
		for k, _ := range data {
			if k == len(data)/2 {
				break
			}
			data[k], data[len(data)-k-1] = data[len(data)-k-1], data[k]
		}

		for k := len(data) - 12; k < len(data); k++ {
			if k < 0 {
				break
			}
			w := data[k]

			//剔除一字板涨停
			if w.Close == w.Block && w.Low < w.Block {
				conn.HDel("stock:block_platform", w.StockCode)
				conn.HDel("stock:block_platform:decrease", w.StockCode)
				conn.HDel("stock:block_platform:high", w.StockCode)
				ok := true
				for i := k - 5; i < k; i++ {
					if i < 0 {
						ok = false
						break
					}
					if data[i].Close == data[i].Block {
						ok = false
						break
					}
				}
				if ok {
					for i := k + 1; i < len(data); i++ {
						//成交额大于2亿
						if data[i].Amount <= 200000 {
							ok = false
							break
						}
						//收盘价高于涨停价*0.99且小于1.05
						if data[i].Close < w.Block*0.99 || data[i].Close > w.Block*1.05 {
							ok = false
							break
						}
						//盘中跌幅最低价不得低于涨停日涨停价的2%
						if data[i].Low < w.Block*0.98 {
							ok = false
							break
						}
						//盘整期间涨幅与涨停日涨停价大于5%即不算盘整阶段
						if data[i].High > w.Block*1.05 {
							ok = false
							break
						}
					}
					//涨停当日不算
					if ok && k+1 < len(data) {
						conn.HSet("stock:block_platform", w.StockCode, w.TradeDate)
						tpo := time.Now().Add(24 * time.Hour)
						go stockPool(w.StockCode, "涨停后调整", conn, time.Date(tpo.Year(), tpo.Month(), tpo.Day(), 0, 0, 0, 0, time.Local))
						amount := w.Amount
						high := data[k].High
						if k < len(data)-1 && data[k+1].Amount > amount {
							amount = data[k+1].Amount
						}
						for j := k + 1; j < len(data); j++ {
							if data[j].Amount <= amount*2/3 && j >= len(data)-5 {
								conn.HSet("stock:block_platform:decrease", w.StockCode, data[j].Amount*1000)
								for u := len(data) - 5; u < len(data); u++ {
									if data[u].High > high {
										high = data[u].High
									}
								}
								conn.HSet("stock:block_platform:high", w.StockCode, high)
							}
						}
						break
					}
				}
			}
		}
	}

}

func ma3Platform() {
	var codes []string
	var watchPeriod int = 10
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:ma3_platform")
	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("stock.ma3Platform() Query codes Errors: ", err.Error())
		return
	}

	for _, v := range codes {
		var data []struct {
			StockCode string
			TradeDate time.Time
			Open      float64
			Close     float64
			Block     float64
			High      float64
			Low       float64
			PreClose  float64
			Amount    float64
			Name      string
		}
		if err := models.DB().Table("p_stock_tick").Select("p_stock_tick.*, b_stock.name").Joins("LEFT JOIN b_stock ON p_stock_tick.stock_code = b_stock.id").Where("p_stock_tick.stock_code = ?", v).Order("p_stock_tick.trade_date DESC").Limit(watchPeriod).Find(&data).Error; err != nil {
			logging.Error("stock.ma3Platform() Errors: ", err.Error())
			continue
		}

		if len(data) < watchPeriod {
			continue
		}

		ok := false
		i := 0
		for ; i < len(data)-2; i++ {
			// ma3
			ma3 := (data[i].Close + data[i+1].Close + data[i+2].Close) / 3
			if data[i].Amount < 200000 {
				break
			}
			if data[i].Low < ma3*0.98 || data[i].Open < ma3*0.98 {
				break
			}
			if i > 2 && data[0].Close >= data[i].Close*1.07 {
				ok = true
				break
			}
		}
		if ok {
			conn.HSet("stock:ma3_platform", data[0].StockCode, data[i].TradeDate.Format("2006-01-02"))
		}
	}
}

func passivePositivePlatform() {
	var codes []string
	var watchPeriod int = 3
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:positive_platform")
	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("stock.longPositivePassive() Query codes Errors: ", err.Error())
		return
	}

	for _, v := range codes {
		var data []struct {
			StockCode string
			TradeDate time.Time
			Open      float64
			Close     float64
			Block     float64
			High      float64
			Low       float64
			PreClose  float64
			Amount    float64
			Name      string
		}
		if err := models.DB().Table("p_stock_tick").Select("p_stock_tick.*, b_stock.name").Joins("LEFT JOIN b_stock ON p_stock_tick.stock_code = b_stock.id").Where("p_stock_tick.stock_code = ?", v).Order("p_stock_tick.trade_date DESC").Limit(watchPeriod).Find(&data).Error; err != nil {
			logging.Error("stock.passivePositivePlatform() Errors: ", err.Error())
			continue
		}

		if len(data) < watchPeriod {
			continue
		}

		ok := false
		i := 2
		for ; i >= 1; i-- {
			if data[i].Amount > 300000 && data[i].Close > data[i].Open*1.05 && data[i].High > data[i].Low*1.07 {
				delta := data[i].Close - data[i].Open
				if data[i-1].Amount > 300000 && data[i-1].Close < data[i-1].Open && data[i-1].Close > data[i].Open+delta/4 {
					ok = true
					break
				}
			}
		}

		if (data[watchPeriod-1].Amount+data[watchPeriod-2].Amount+data[watchPeriod-3].Amount)/3 < 300000 {
			ok = false
		}

		if ok {
			conn.HSet("stock:positive_platform", data[0].StockCode, data[i].High)
		}
	}
}

func longPositiveShockPlatform() {
	var codes []string
	var watchPeriod int = 6
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:long_shock_platform")
	conn.Delete("stock:long_shock_high")
	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("stock.longPositiveShockPlatform() Query codes Errors: ", err.Error())
		return
	}

	for _, v := range codes {
		var data []struct {
			StockCode string
			TradeDate time.Time
			Open      float64
			Close     float64
			Block     float64
			High      float64
			Low       float64
			PreClose  float64
			Amount    float64
			Name      string
			IsBlock   int
		}
		if err := models.DB().Table("p_stock_tick").Select("p_stock_tick.*, b_stock.name").Joins("LEFT JOIN b_stock ON p_stock_tick.stock_code = b_stock.id").Where("p_stock_tick.stock_code = ?", v).Order("p_stock_tick.trade_date DESC").Limit(watchPeriod).Find(&data).Error; err != nil {
			logging.Error("stock.longPositiveShockPlatform() Errors: ", err.Error())
			continue
		}

		if len(data) < watchPeriod {
			continue
		}

		ok := false
		i := 5
		var cHigh float64
		for ; i >= 3; i-- {
			if data[i].IsBlock == 0 && data[i].Amount > 300000 && data[i].Close > data[i].Open*1.05 && data[i].High > data[i].Low*1.07 {
				b := true
				for k := 1; k <= i; k++ {
					if data[i-k].Close > data[i].Close*1.02 || data[i-k].Close < data[i].Close*0.98 || data[i-k].High > data[i-k].Low*1.05 {
						b = false
						break
					}
					if data[i-k].High > cHigh {
						cHigh = data[i-k].High
					}
				}
				if b {
					ok = true
					break
				}
			}
		}

		if (data[watchPeriod-1].Amount+data[watchPeriod-2].Amount+data[watchPeriod-3].Amount)/3 < 300000 {
			ok = false
		}

		if ok {
			conn.HSet("stock:long_shock_platform", data[0].StockCode, data[i].Close*1.05)
			conn.HSet("stock:long_shock_high", data[0].StockCode, cHigh)
		}
	}
}

func perf() {
	var date string
	date = time.Now().Format("2006-01-02")
	//date = "2021-01-18"
	if !checkTrade(date) {
		return
	}
	//先计算day1数据
	rise2(date)

	var fores, annos []struct {
		Code        string
		PublishDate time.Time
	}
	var dates []string
	dates = makeTradeDates(date)
	logging.Info("stock.perf() dates: ", dates)
	if err := models.DB().Table("u_performance_forecast").
		Where("publish_date IN (?)", dates).
		Find(&fores).Error; err != nil {
		logging.Error("stock.perf() fetching forecast Errors: ", err.Error())
	}

	if err := models.DB().Table("u_performance_announcement").
		Where("publish_date IN (?)", dates).
		Find(&annos).Error; err != nil {
		logging.Error("stock.perf() fetching announcement Errors: ", err.Error())
	}

	conn := gredis.Clone(setting.RedisSetting.StockDB)

	// 交易日需清除数据后写入
	//conn.Delete("performance:publish_date")
	conn.Delete("performance:fallback")
	conn.Delete("performance:rise:0")
	conn.Delete("performance:block:day1")

	for _, v := range fores {
		logging.Info("stock.perf() forecast: ", v)
		if preAmount(v.Code, v.PublishDate.Format(util.YMD)) <= 200000 {
			logging.Info("stock.perf() forecast skipped: ", v)
			continue
		}
		conn.HSet("performance:publish_date", v.Code, v.PublishDate.Format(util.YMD))
		conn.HSet("performance:type", v.Code, "forecast")
		block, _ := perfBlock(v.Code, date, conn)
		if block {
			conn.HSet("performance:block:publish_date", v.Code, v.PublishDate.Format(util.YMD))
		}
		if ok, high := fallback(v.Code, date); ok {
			conn.HSet("performance:fallback", v.Code, high)
			conn.HSet("performance:fallback:publish_date", v.Code, v.PublishDate.Format(util.YMD))
		}
		if ok, high := rise(v.Code, date); ok {
			conn.HSet("performance:rise:0", v.Code, high)
			conn.HSet("performance:rise:publish_date", v.Code, v.PublishDate.Format(util.YMD))
		}
	}
	for _, v := range annos {
		logging.Info("stock.perf() announcement: ", v)
		if preAmount(v.Code, v.PublishDate.Format(util.YMD)) <= 200000 {
			logging.Info("stock.perf() announcement skipped: ", v)
			continue
		}
		conn.HSet("performance:publish_date", v.Code, v.PublishDate.Format(util.YMD))
		conn.HSet("performance:type", v.Code, "announcement")
		block, _ := perfBlock(v.Code, date, conn)
		if block {
			conn.HSet("performance:block:publish_date", v.Code, v.PublishDate.Format(util.YMD))
		}
		if ok, high := fallback(v.Code, date); ok {
			conn.HSet("performance:fallback", v.Code, high)
			conn.HSet("performance:fallback:publish_date", v.Code, v.PublishDate.Format(util.YMD))
		}
		if ok, high := rise(v.Code, date); ok {
			conn.HSet("performance:rise:0", v.Code, high)
			conn.HSet("performance:rise:publish_date", v.Code, v.PublishDate.Format(util.YMD))
		}
	}
}

func strong() {
	var dates, codes []string
	date := time.Now().Format("2006-01-02")
	if err := models.DB().Table("b_trade_date").Where("trade_date <= ?", date).Order("trade_date DESC").Limit(3).Pluck("trade_date", &dates).Error; err != nil {
		logging.Error("stock.strong() fetching dates Errors: ", err.Error())
		return
	}

	if err := models.DB().Table("u_replay").Where("datetime IN (?)", dates).Where("type = ?", "强势股").Pluck("code", &codes).Error; err != nil {
		logging.Error("stock.strong() fetching codes Errors: ", err.Error())
		return
	}

	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:monitoring:strong")
	for _, v := range codes {
		conn.HSet("stock:monitoring:strong", v, date)
	}
}

func blockOpen() {
	var yesterday, codes []string
	date := time.Now().Format("2006-01-02")
	if err := models.DB().Table("b_trade_date").Where("trade_date < ?", date).Order("trade_date DESC").Limit(1).Pluck("trade_date", &yesterday).Error; err != nil {
		logging.Error("stock.blockOpen() fetching yesterday Errors: ", err.Error())
		return
	}
	if len(yesterday) < 1 {
		return
	}
	if err := models.DB().Table("p_stock_tick").Where("trade_date = ?", yesterday[0]).Where("block = high").Where("close < block").Pluck("stock_code", &codes).Error; err != nil {
		logging.Error("stock.blockOpen() fetching codes Errors: ", err.Error())
		return
	}

	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:monitoring:block_open")
	for _, v := range codes {
		conn.HSet("stock:monitoring:block_open", v, date)
	}
}

func perfBlock(code, date string, conn *gredis.RedisConn) (bool, float64) {
	ltg, _ := conn.HGetFloat64("stock:ltg", code)
	var data struct {
		StockCode string
		TradeDate time.Time
		Open      float64
		Close     float64
		Block     float64
		High      float64
		Low       float64
		PreClose  float64
		Name      string
	}

	if err := models.DB().Table("p_stock_tick").
		Select("p_stock_tick.*, b_stock.name").
		Joins("LEFT JOIN b_stock ON p_stock_tick.stock_code = b_stock.id").
		Where("p_stock_tick.stock_code = ?", code).
		Where("p_stock_tick.trade_date = ?", date).
		First(&data).Error; err != nil {
		//logging.Error("stock.perfBlock() fetch stock Errors: ", code, err.Error())
		return false, 0
	}

	if (data.Block == data.Close && data.Close > data.PreClose*1.05+0.01) || (ltg*data.Close > 20000000000 && data.Close > data.PreClose*1.05-0.01) || (data.Close > data.PreClose*1.1+0.01) {
		// fix task #133
		conn.HSet("performance:block:day1", code, data.High)
		return true, data.High
	}
	return false, 0
}

func notice() {
	var dayLine []quantization.DayLineEx
	tm := time.Now().Format("2006-01-02")
	//tm := "2021-01-04"
	if !checkTrade(tm) {
		return
	}

	if err := models.DB().Table("p_stock_tick").
		Select("p_stock_tick.*").
		Joins("LEFT JOIN u_cninfo_notice ON u_cninfo_notice.code = p_stock_tick.stock_code AND DATE(u_cninfo_notice.publish_time) = p_stock_tick.trade_date").
		Joins("LEFT JOIN u_user_event ON u_user_event.object_id = u_cninfo_notice.id").
		Where("u_user_event.object_type = ?", "announcement").
		Where("u_user_event.event_type = ?", "recommend").
		Where("p_stock_tick.trade_date = ?", tm).
		Group("p_stock_tick.stock_code").
		Order("p_stock_tick.stock_code").
		Find(&dayLine).Error; err != nil {
		logging.Error("stock.notice() Fetch Errors: ", err.Error())
		return
	}

	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, v := range dayLine {
		if v.High > v.Close*1.03 && v.Close > v.PreClose {
			conn.HSet("stock:notice:day", v.StockCode, tm)
			conn.HSet("stock:notice:high", v.StockCode, v.High)
		}
		if v.Open > v.High*0.99 && v.Close < v.PreClose && v.High > v.Close*1.03 {
			conn.HSet("stock:notice:day", v.StockCode, tm)
			conn.HSet("stock:notice:high", v.StockCode, v.High)
		}
		if v.Close < v.PreClose*0.98 {
			conn.HSet("stock:notice:day", v.StockCode, tm)
			conn.HSet("stock:notice:high", v.StockCode, v.High)
		}
	}

	m, _ := conn.HGetAllStringMap("stock:notice:day")
	cur, _ := conn.HGetFloat64("stock:trade_date", tm)
	for code, v := range m {
		idx, _ := conn.HGetFloat64("stock:trade_date", v)
		if idx > 0 && cur > 0 {
			if idx < cur-1 {
				conn.HDel("stock:notice:day", code)
				conn.HDel("stock:notice:high", code)
			}
		} else {
			conn.HDel("stock:notice:day", code)
			conn.HDel("stock:notice:high", code)
		}
	}
}

func rsi() {
	var codes []string
	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("stock.rsi() Query codes Errors: ", err.Error())
		return
	}

	t := time.Now()
	st := []struct {
		Name string
		Func func(code string, t time.Time, low float64, high float64) bool
	}{{
		"ystb",
		quantization.Ystb,
	}, {
		"jzcs",
		quantization.Jzcs,
	}, {
		"yszk",
		quantization.Yszk,
	}, {
		"kfjl",
		quantization.Kfjl,
	}, {
		"mllh",
		quantization.Mllh,
	}, {
		"jyxj",
		quantization.Jyxj,
	},
	}

	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, u := range st {
		conn.Delete("stock:rsi:" + u.Name)
	}
	for _, v := range codes {
		for _, u := range st {
			if u.Func(v, t, 0, 1000) {
				_ = conn.HSet("stock:rsi:"+u.Name, v, t.Format("2006-01-02"))
			}
		}
	}
}

func tradeDate() {
	var s []struct {
		ID        int
		TradeDate time.Time
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:trade_date")
	models.DB().Table("b_trade_date").Find(&s)
	for _, v := range s {
		if !v.TradeDate.IsZero() && v.ID > 0 {
			conn.HSet("stock:trade_date", v.TradeDate.Format("2006-01-02"), v.ID)
		}
	}
}

func preNew() {
	var data []struct {
		StockCode string
		MinDate   time.Time
	}
	if err := models.DB().Raw("SELECT s.stock_code, s.min_date FROM (SELECT stock_code, COUNT(1) AS cnt, MAX(trade_date) AS max, MIN(trade_date) AS min_date FROM p_stock_tick GROUP BY stock_code) s LEFT JOIN (SELECT stock_code, COUNT(1) AS cnt FROM p_stock_tick WHERE is_block = 1 GROUP BY stock_code) t ON t.stock_code = s.stock_code WHERE s.cnt = t.cnt AND s.max > DATE_ADD(CURRENT_DATE,INTERVAL -10 DAY) ORDER BY s.stock_code").Find(&data).Error; err != nil {
		logging.Error("stock.preNew() Query data Errors: ", err.Error())
		return
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:pre_new")
	for _, v := range data {
		conn.HSet("stock:pre_new", v.StockCode, v.MinDate.Format("2006-01-02"))
	}
}

func rsiEx() {
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	tm := time.Now().Format("2006-01-02")
	codes := preAmountAvg(3, 600000, 4000000)
	for _, v := range codes {
		if conn.HExists("stock:rsi:yszk", v) && conn.HExists("stock:rsi:ystb", v) {
			conn.HSet("stock:highest:rule1", v, tm)
		}
	}

	codes = preAmountAvg(3, 0, 4000000)
	for _, v := range codes {
		zgb, _ := conn.HGetFloat64("stock:zgb", v)
		np, _ := conn.HGetFloat64("stock:tick", v)
		if zgb > 0 && np > 0 && zgb*np < 40000000000 && conn.HExists("stock:rsi:kfjl", v) && conn.HExists("stock:rsi:yszk", v) && conn.HExists("stock:rsi:jzcs", v) {
			conn.HSet("stock:highest:rule2", v, tm)
		}
	}

	codes = preAmountAvg(3, 0, 5000000)
	for _, v := range codes {
		zgb, _ := conn.HGetFloat64("stock:zgb", v)
		np, _ := conn.HGetFloat64("stock:tick", v)
		if zgb > 0 && np > 0 && zgb*np < 54500000000 && conn.HExists("stock:rsi:yszk", v) && conn.HExists("stock:rsi:mllh", v) {
			conn.HSet("stock:highest:rule3", v, tm)
		}
	}

	codes = preAmountAvg(3, 0, 4000000)
	for _, v := range codes {
		zgb, _ := conn.HGetFloat64("stock:zgb", v)
		np, _ := conn.HGetFloat64("stock:tick", v)
		if zgb > 0 && np > 0 && zgb*np < 40000000000 && conn.HExists("stock:rsi:mllh", v) && conn.HExists("stock:rsi:yszk", v) {
			conn.HSet("stock:highest:rule4", v, tm)
		}
	}
}

func importantStockList() {
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	list, _ := conn.FetchList("stock:monitoring")
	m, _ := conn.HGetAllStringMap("stock:name")
	for _, v := range list {
		for code, name := range m {
			if name == v {
				np, _ := conn.HGetFloat64("stock:tick", code)
				_ = conn.HSet("stock:monitoring:list", code, np)
			}
		}
	}
}

func lastPriceAvg() {
	ts := time.Now().Format(util.YMD)
	if !inTradeDate(ts) {
		return
	}
	//test
	//ts = "2021-02-01"
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	var data []struct {
		StockCode string
		Avg       float64
	}
	if err := models.DB().Select("p_stock_tick.stock_code, AVG(p_stock_tick.`close`) AS avg").
		Table("p_stock_tick").
		Joins(fmt.Sprintf("LEFT JOIN b_trade_date A ON A.trade_date = '%s'", ts)).
		Joins("LEFT JOIN b_trade_date B ON B.id = A.id - 3").
		Joins("LEFT JOIN b_trade_date C ON C.id = A.id + 1").
		Where("p_stock_tick.trade_date >= B.trade_date").
		Where("p_stock_tick.trade_date < C.trade_date").
		Group("p_stock_tick.stock_code").
		Find(&data).Error; err != nil {
		logging.Error("stock.lastPriceAvg() fetch pre_price Errors: ", err.Error())
		return
	}
	for _, v := range data {
		conn.HSet("stock:pre_price:avg_4", v.StockCode, v.Avg)
	}
}

func importantStockListNew() {
	ts := time.Now().Format(util.YMD)
	if !inTradeDate(ts) {
		return
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:monitoring:list")

	var data []struct {
		StockCode string
		Total     float64
	}
	sub := models.DB().Select("MAX(report_date)").Table("p_public_fund_position")
	models.DB().Select("p_public_fund_position.stock_code, SUM(held_num) * p_stock_tick.`close` AS total").
		Table("p_public_fund_position").
		Joins("LEFT JOIN p_stock_tick ON p_stock_tick.stock_code = p_public_fund_position.stock_code AND p_stock_tick.trade_date = ?", ts).
		Where("p_public_fund_position.report_date = ?", sub.SubQuery()).
		Group("p_public_fund_position.stock_code").
		Order("total DESC").Limit(30).
		Find(&data)
	for _, v := range data {
		conn.HSet("stock:monitoring:list", v.StockCode, 1)
	}
}

func fallback(code, date string) (bool, float64) {
	var data struct {
		StockCode string
		TradeDate time.Time
		Open      float64
		Close     float64
		Block     float64
		High      float64
		Low       float64
		PreClose  float64
	}
	if err := models.DB().Table("p_stock_tick").
		Where("stock_code = ?", code).
		Where("trade_date = ?", date).
		First(&data).Error; err != nil {
		return false, 0
	}
	if data.Close > data.PreClose && data.High > data.Close*1.03 {
		return true, data.High
	}
	if data.Close < data.PreClose && data.Open > data.High*0.99 && data.High > data.PreClose {
		return true, data.High
	}
	if data.Close < data.PreClose*0.98 {
		return true, data.PreClose
	}

	return false, 0
}

func rise(code, date string) (bool, float64) {
	var data struct {
		StockCode string
		TradeDate time.Time
		Open      float64
		Close     float64
		Block     float64
		High      float64
		Low       float64
		PreClose  float64
	}
	if err := models.DB().Table("p_stock_tick").
		Where("stock_code = ?", code).
		Where("trade_date = ?", date).
		First(&data).Error; err != nil {
		return false, 0
	}
	if data.Close > data.PreClose {
		return true, data.High
	}
	return false, 0
}

func rise2(date string) {
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("performance:rise:1")
	m, err := conn.HGetAllStringMap("performance:rise:0")
	if err != nil {
		logging.Error("fetch.rise2() fetch Errors: ", err.Error())
		return
	}
	for k, v := range m {
		var data []struct {
			StockCode string
			Close     float64
		}
		models.DB().Table("p_stock_tick").Where("trade_date = ?", date).Where("stock_code = ?", k).Where("close > ?", v).Find(&data)
		if len(data) > 0 {
			c := data[0].Close
			conn.HSet("performance:rise:1", k, c)
		}
	}
}

func convertTs(tsCode string) string {
	t := strings.Replace(tsCode, "XSHG", "SH", -1)
	return strings.Replace(t, "XSHE", "SZ", -1)
}

func checkTrade(date string) bool {
	var cnt int
	models.DB().Table("p_stock_tick").Where("trade_date = ?", date).Count(&cnt)
	return cnt > 0
}

func makeTradeDates(date string) []string {
	ret := []string{date}
	tm, err := time.ParseInLocation("2006-01-02", date, time.Local)
	if err != nil {
		return ret
	}

	for tm = tm.Add(-24 * time.Hour); !checkTrade(tm.Format(util.YMD)); tm = tm.Add(-24 * time.Hour) {
		ret = append(ret, tm.Format(util.YMD))
	}
	return ret
}

func inTradeDate(date string) bool {
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	return conn.HExists("stock:trade_date", date)
}

func inTradeTime(t time.Time) bool {
	return inTradeDate(t.Format("2006-01-02")) &&
		(t.Hour() == 9 && t.Minute() >= 15 ||
			t.Hour() == 10 ||
			t.Hour() == 11 && t.Minute() <= 30 ||
			t.Hour() == 13 ||
			t.Hour() == 14 ||
			t.Hour() == 15 && t.Minute() <= 31)
}

func inTrade() bool {
	return inTradeTime(time.Now())
}

func preAmountAvg(day int, minAmount, maxAmount int64) []string {
	var codes []string
	if err := models.DB().Table("p_stock_tick A").
		Select("A.stock_code, AVG(B.amount) AS avg").
		Joins("LEFT JOIN b_trade_date D1 ON D1.trade_date = A.trade_date").
		Joins("LEFT JOIN p_stock_tick B ON A.stock_code = B.stock_code").
		Joins("LEFT JOIN b_trade_date D2 ON D2.trade_date = B.trade_date").
		Where("A.stock_code IS NOT NULL").
		Where("A.trade_date = CURRENT_DATE()").
		Where("D1.id < D2.id + "+strconv.Itoa(day)).
		Where("D1.id >= D2.id").
		//Where("avg > 600000 AND avg < ").
		Group("A.stock_code").
		Having(fmt.Sprintf("avg >= %d AND avg <= %d", minAmount, maxAmount)).
		Order("A.stock_code").
		Pluck("stock_code", &codes).Error; err != nil {
		logging.Error("stock.preAmountAvg() Query codes Errors: ", err.Error())
	}
	return codes
}

func preAmount(code, date string) float64 {
	var data struct {
		StockCode string
		Amount    float64
	}
	if err := models.DB().Table("p_stock_tick").
		Select("stock_code, amount").
		Where("trade_date < ?", date).
		Where("stock_code = ?", code).
		Order("trade_date DESC").Limit(1).Find(&data).Error; err != nil {
		logging.Error("stock.preAmount() Query ", code, date, " Errors: ", err.Error())
	}
	return data.Amount
}

func clean() {
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	conn.Delete("stock:auction:high")
	conn.Delete("stock:auction:low")
	conn.Delete("stock:auction:bp1")
	conn.Delete("stock:auction:bv1")
	cs := codes()
	for _, v := range cs {
		conn.Delete("auction:record:" + v)
	}
}

func codes() []string {
	var codes []string
	db := models.DB()
	if err := db.Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &codes).Error; err != nil {
		logging.Error("stock.codes() Query codes Errors: ", err.Error())
	}
	return codes
}

func setHsIndustry() {
	type stockIndustry struct {
		Id             int    `json:"id"`
		Code           string `json:"code"`
		FirstIndustry  string `json:"first_industry"`
		SecondIndustry string `json:"second_industry"`
		ActualIndustry string `json:"actual_industry"`
	}
	industryList := make([]stockIndustry, 0)
	err := models.DB().Table("u_stock_industry").
		Where("deleted_at is null").
		Where("industry_id!=?", performance.DefaultIndustryId).
		Find(&industryList).Error
	if err != nil {
		logging.Error("setHsIndustry获取行业列表失败", err.Error())
	}
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	for _, industry := range industryList {
		if !strings.HasSuffix(industry.SecondIndustry, "II") {
			err = conn.HSet("stock:industry:hs", industry.Code, industry.SecondIndustry)
			if err != nil {
				logging.Error("设置"+industry.Code+"hs行业报错", err.Error())
			}
		}
	}
}
