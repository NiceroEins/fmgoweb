package stock

import (
	"datacenter/models"
	"datacenter/models/performance"
	"datacenter/models/quantization"
	"datacenter/models/stock"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"github.com/shopspring/decimal"
	"strings"
	"time"
)

func record(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	conn.HSet("stock:tick", tick.Code, fmt.Sprintf("%.02f", tick.NewPrice))
	conn.HSet("stock:amount", tick.Code, fmt.Sprintf("%.02f", tick.Amount))
	conn.HSet("stock:price3", tick.Code, fmt.Sprintf("%.02f", tick.Price3))
	conn.HSet("stock:vol2", tick.Code, fmt.Sprintf("%.02f", tick.Vol2))
	conn.HSet("stock:last_close", tick.Code, fmt.Sprintf("%.02f", tick.LastClose))
	conn.HSet("stock:open", tick.Code, fmt.Sprintf("%.02f", tick.Open))
	conn.HSet("stock:high", tick.Code, fmt.Sprintf("%.02f", tick.High))
	conn.HSet("stock:low", tick.Code, fmt.Sprintf("%.02f", tick.Low))
	conn.HSet("stock:volume", tick.Code, fmt.Sprintf("%.02f", tick.Volume))
	conn.HSet("stock:name", tick.Code, tick.Name)
	for k := 0; k < 5; k++ {
		conn.HSet("stock:bp"+fmt.Sprintf("%d", k+1), tick.Code, tick.BP[k])
		conn.HSet("stock:bv"+fmt.Sprintf("%d", k+1), tick.Code, tick.BV[k])
	}

	if tick.LastClose > 0 {
		change := (tick.NewPrice/tick.LastClose - 1) * 100
		conn.HSet("stock:change", tick.Code, fmt.Sprintf("%.02f", change))
		if change > 0 {
			conn.HSet("stock:increase", tick.Code, fmt.Sprintf("%.02f", change))
			conn.HDel("stock:decrease", tick.Code)
			conn.Expire("stock:increase", 9*3600)
		} else if change < 0 {
			conn.HSet("stock:decrease", tick.Code, fmt.Sprintf("%.02f", change))
			conn.HDel("stock:increase", tick.Code)
			conn.Expire("stock:decrease", 9*3600)
		} else {
			conn.HDel("stock:increase", tick.Code)
			conn.HDel("stock:decrease", tick.Code)
		}
	}

	block := fmt.Sprintf("%.02f", tick.LastClose*1.1)
	if strings.HasPrefix(tick.Code, "300") {
		block = fmt.Sprintf("%.02f", tick.LastClose*1.2)
	}

	if fmt.Sprintf("%.02f", tick.NewPrice) == block {
		if !conn.HExists("stock:block", tick.Code) {
			conn.HSet("stock:block", tick.Code, time.Now().Format("2006-01-02 15:04:05.000"))
		}
		// 卖1等于0
		if tick.SV[0] == 0 && !conn.HExists("stock:pre_new", tick.Code) {
			if ok, _ := conn.SMEMBERS("stock:block_monitor", tick.Code); !ok {
				// 记录打板股票
				conn.SAdd("stock:block_monitor", tick.Code)
				conn.Expire("stock:block_monitor", 9*3600)
			}
		}
	} else {
		conn.HDel("stock:block", tick.Code)
	}

	// amount zset
	conn.ZNewAdd("stock:zset:amount", fmt.Sprintf("%d", int(tick.Amount)), tick.Code)

	//mem cache
	go quantization.RealTimeStock(tick.Code, float64(tick.NewPrice))
	return nil
}

func tsRecord(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	conn.HSet("stock:ts_tick", tick.TsCode, fmt.Sprintf("%.02f", tick.NewPrice))
	conn.HSet("stock:ts_amount", tick.TsCode, fmt.Sprintf("%.02f", tick.Amount))

	ins := stock.GetInstance()
	ins.Push(&stock.Minute{
		Code:      tick.Code,
		StockCode: tick.TsCode,
		TimeRaw:   time.Now(),
		Open:      float64(tick.NewPrice),
		Close:     float64(tick.NewPrice),
		High:      float64(tick.NewPrice),
		Low:       float64(tick.NewPrice),
		Volume:    float64(tick.Volume),
		Amount:    float64(tick.Amount),
	})
	return nil
}

func newHighest(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	np, _ := conn.HGetFloat64("stock:limit:highest_price", tick.Code)
	if conn.HExists("stock:limit:push_ex", tick.Code) {
		prePrice, _ := conn.HGetFloat64("stock:pre_price:avg_4", tick.Code)
		if prePrice > 0 && (float32(prePrice)*4+tick.NewPrice)/5*0.97 > tick.NewPrice {
			conn.HDel("stock:limit:push_ex", tick.Code)
		}
	}
	if np+0.01 <= float64(tick.NewPrice) || np+0.01 <= float64(tick.High) {
		_ = conn.HSetValue("stock:limit:highest_price", tick.Code, fmt.Sprintf("%.02f", tick.High))
		// 新高
		logging.Info("filter.newHighest() " + tick.Code + " reaches highest")
		//logging.Info(stockNewStock(tick, conn), np, tick.Low, tick.High, tick.NewPrice, tick.LastClose, tick.Name)

		// last highest price
		lp, _ := conn.HGetFloat64("stock:limit:last_highest", tick.Code)
		if np > 0 && !stockNewStock(tick, conn) && (tick.Low < tick.High || tick.NewPrice < tick.LastClose*1.1-0.01) && (float64(tick.LastClose) < lp || lp == 0) && strings.Index(tick.Name, "st") == -1 && strings.Index(tick.Name, "ST") == -1 {
			t := time.Now()
			if t.Hour() == 9 && t.Minute() <= 30 {
				if tick.Amount < 30000000 {
					return nil
				}
			}
			preAmount, _ := conn.HGetFloat64("stock:pre_amount", tick.Code)
			if !stockLimit2Days(tick.Code) && stockLtg(tick.Code, conn)*float64(tick.NewPrice) > 5000000000 {
				if preAmount >= 300000 {
					if !conn.HExists("stock:limit:last_price", tick.Code) {
						conn.HSet("stock:limit:last_price", tick.Code, np)
						conn.Expire("stock:limit:last_price", 3600*8)
					}
					return map[string]interface{}{
						"type":    "pool",
						"content": fmt.Sprintf("%s：突破历史新高", tick.Name),
					}
				}
			}
		}
	}
	return nil
}

func newHighestEx(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	t := time.Now()
	lID := lastTradeID(tick, conn)
	cID := curTradeID(conn)
	np, _ := conn.HGetFloat64("stock:limit:last_price", tick.Code)
	preAmount, _ := conn.HGetFloat64("stock:pre_amount", tick.Code)
	b := lID > 0 && cID > 0 && lID == cID-1
	if preAmount >= 500000 && (!b || float64(tick.NewPrice) > np*1.02) {
		conn.HSet("stock:limit:last_push", tick.Code, t.Format(util.YMDHMS))
		//prePrice, _ := conn.HGetFloat64("stock:pre_price:avg_4", tick.Code)
		if checkPreVol(tick, conn, 2500000) && !conn.HExists("stock:limit:push_ex", tick.Code) {
			conn.HSet("stock:limit:push_ex", tick.Code, t.Format(util.YMD))
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("特别提醒，%s创新高，经回测成功率较高", tick.Name),
			}
		} else {
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s：突破历史新高", tick.Name),
			}
		}
	}
	return nil
}

func newHighestIndustryNew(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {

	return nil
}

func newHighestIndustry(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	ind, err := conn.HGetString("stock:industry:industry", tick.Code)
	icp, _ := conn.HGetFloat64("stock:industry:icp", tick.Code)

	_ = conn.HINCRBY("stock:push:industry", ind, 1)
	conn.Expire("stock:push:industry", 3600*8)
	n, _ := conn.HGetFloat64("stock:push:industry", ind)
	if err == nil && ind != "" {
		if n < 3 {
			conn.PushList("stock:industry:"+ind, tick.Name)
			conn.Expire("stock:industry:"+ind, 3600*8)
		} else if n == 3 {
			list := industryStockNew(ind)
			data, _ := conn.PopList("stock:industry:" + ind)
			var ret []string
			for _, v := range list {
				if util.ContainsAny(data, v) {
					continue
				}
				if len(ret) < 3 {
					ret = append(ret, v)
				}
			}
			if len(ret) > 0 {
				return map[string]interface{}{
					"type":    "stock",
					"content": fmt.Sprintf("%s表现较好, 同属%s行业，建议关注同行业个股：%s", strings.Join(data, ","), ind, strings.Join(ret, ",")),
				}
			}
		} else {
			// 不推荐
			return nil
		}
	}
	return map[string]interface{}{
		"type":    "stock",
		"content": fmt.Sprintf("%s目前涨幅%.1f%%", tick.Name, icp),
	}
}

func stockLimit2Days(code string) bool {
	var stock []struct {
		StockCode string
		TradeDate string
		Close     float64
		Block     float64
	}
	if err := models.DB().Table("p_stock_tick").Where("stock_code = ?", code).Order("trade_date DESC").Limit(2).Find(&stock).Error; err != nil {
		logging.Error("filter.stockLimit2Days() Query limit Errors: ", err.Error())
		return false
	}
	if len(stock) > 0 && stock[0].Close == stock[0].Block ||
		len(stock) > 1 && stock[1].Close == stock[1].Block {
		return true
	}
	return false
}

func stockNewStock(tick *Tick, conn *gredis.RedisConn) bool {
	if newStock(tick) {
		return true
	}
	data, err := conn.HGetString("stock:new_stock", tick.Code)
	if err != nil || data == "" {
		return false
	}
	return true
}

func newStock(tick *Tick) bool {
	if []byte(tick.Name)[0] == 'N' {
		return true
	}
	return false
}

func preNewStock(tick *Tick) bool {
	if []byte(tick.Name)[0] == 'C' {
		return true
	}
	return false
}

func stockLtg(code string, conn *gredis.RedisConn) float64 {
	data, err := conn.HGetFloat64("stock:ltg", code)
	if err != nil || data == 0 {
		return 0
	}
	return data
}

func auction(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	ah, _ := conn.HGetFloat64("stock:auction:high", tick.Code)
	al, _ := conn.HGetFloat64("stock:auction:low", tick.Code)
	bp := float64(tick.BP[0])
	bv := float64(tick.BV[0])
	conn.HSet("stock:auction:bp1", tick.Code, bp)
	conn.HSet("stock:auction:bv1", tick.Code, bv)
	if al == 0 || (bp < al && bp > 0) {
		al = bp
		conn.HSet("stock:auction:low", tick.Code, al)
	}
	// 0826 fix: 集合竞价高点判断
	if bp > ah {
		ah = bp
		conn.HSet("stock:auction:high", tick.Code, ah)
	}

	// 抢筹20分后最低委托价，跌幅不得超过6个点
	if bp > al*1.025 && al >= float64(tick.LastClose)*0.94 {
		// bv手数
		if bv*bp*100 > 10000000 && !stockNewStock(tick, conn) {
			conn.HSet("stock:auction:scramble", tick.Code, bp)
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s：出现抢筹迹象", tick.Name),
			}
		}
	}

	return nil
}

func clear(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	conn.Delete("stock:auction:high")
	conn.Delete("stock:auction:low")
	conn.Delete("stock:auction:bp1")
	conn.Delete("stock:auction:bv1")
	conn.Delete("auction:record:" + tick.Code)
	return nil
}

func breakthrough(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	pf, err := conn.HGetFloat64("stock:platform", tick.Code)
	if err != nil || pf <= 0 {
		return nil
	}

	if float64(tick.NewPrice) > pf*1.02 {
		conn.HSet("stock:breakthrough", tick.Code, tick.NewPrice)
		// conn.HSet("stock:push:breakthrough", code, time.Now().Format("2006-01-02 15:04:05"))
		return map[string]interface{}{
			"type":    "stock",
			"content": fmt.Sprintf("%s：平台突破", tick.Name),
		}
	}
	return nil
}

func auctionRecord(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	data, _ := json.Marshal(tick)
	conn.HSet("auction:record:"+tick.Code, time.Now().Format("2006-01-02 15:04:05.000"), string(data))
	return nil
}

func continuousPositivity(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	pc, err := conn.HGetString("stock:positivity", tick.Code)
	if err != nil || pc == "" {
		return nil
	}
	if tick.NewPrice > tick.LastClose*1.05 {
		conn.HSet("stock:positivity:breakthrough", tick.Code, tick.NewPrice)
		return map[string]interface{}{
			"type":    "stock",
			"content": fmt.Sprintf("%s：小连阳后突破", tick.Name),
		}
	}
	return nil
}

func auctionAmount(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	bp := float64(tick.BP[0])
	bv := float64(tick.BV[0])
	if bp > float64(tick.LastClose)*1.02 {
		// bv手数
		// 集合委托大于3000万
		// 剔除新股
		amount := bv * bp * 100
		if amount > 30000000 && !stockNewStock(tick, conn) {
			// 剔除一字板
			al, _ := conn.HGetFloat64("stock:auction:low", tick.Code)
			if al >= float64(tick.LastClose)*1.1-0.01 {
				return nil
			}

			lb, _ := conn.HGetString("stock:last_block", tick.Code)
			if lb != "" {
				// 昨日涨停
				return nil
			}

			var amountStr string
			if amount > 100000000 {
				amountStr = fmt.Sprintf("%.2f亿", amount/100000000)
			} else {
				amountStr = fmt.Sprintf("%d万", int(amount/10000))
			}
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s: 集合竞价金额%s", tick.Name, amountStr),
			}
		}
	}
	return nil
}

func amountIncrease(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	dp, err := conn.HGetFloat64("stock:block_platform:decrease", tick.Code)
	if err == nil && dp > 0 {
		tNow := sec(time.Now().Hour(), time.Now().Minute(), time.Now().Second())
		tBegin := sec(9, 20, 0)
		tEnd := sec(15, 0, 0)
		hp, _ := conn.HGetFloat64("stock:block_platform:high", tick.Code)
		if float64(tick.Amount)*float64(tEnd-tBegin)/float64(tNow-tBegin) > dp*2 && (tick.NewPrice > tick.LastClose*1.03 || hp > 0 && float64(tick.NewPrice) > hp) {
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s: 涨停盘整后放量上涨", tick.Name),
			}
		}
	}
	return nil
}

// 创业板上涨超过10%
func chiNextIncr(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if stockChiNext(tick) && !preNewStock(tick) && !stockNewStock(tick, conn) {
		// 1216 change
		if tick.LastClose > 0 && tick.NewPrice > tick.LastClose*1.1 {
			if conn.HExists("stock:chi_next:incr_banned", tick.Code) {
				return nil
			}
			if tick.Amount < 300000000 && !checkPreVol(tick, conn, 300000) {
				_ = conn.HSet("stock:chi_next:incr_banned", tick.Code, time.Now().Format(util.YMD))
				conn.Expire("stock:chi_next:incr_banned", 10*3600)
				return nil
			}
			icp := float64(tick.NewPrice/tick.LastClose-1) * 100
			// industry, _ := conn.HGetString("stock:industry:industry", tick.Code)
			conn.HSet("stock:industry:icp", tick.Code, icp)
			conn.Expire("stock:industry:icp", 3600*8)
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s目前涨幅%.1f%%", tick.Name, icp),
			}
		}
	}
	return nil
}

// 创业板涨停
func chiNextBlock(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if stockChiNext(tick) && !preNewStock(tick) && !stockNewStock(tick, conn) {
		if tick.LastClose > 0 && (tick.NewPrice > tick.LastClose*1.2-0.01) {
			//tmStr, err := conn.HGetString("stock:push:chinext_increase", tick.Code)
			//if err == nil {
			//	tm, e := time.ParseInLocation("2006-01-02 15:04:05", tmStr, time.Local)
			//	if e == nil && tm.Format("2006-01-02") == time.Now().Format("2006-01-02") {
			//		// today
			//		return nil
			//	}
			//}
			// industry, _ := conn.HGetString("stock:industry:industry", tick.Code)
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s目前涨幅20%%", tick.Name),
			}
		}
	}
	return nil
}

func checkIndustry(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	// var ind string
	inds, id := industry(tick)
	// ind, _ = conn.HGetString("stock:industry:industry", tick.Code)
	icp, _ := conn.HGetFloat64("stock:industry:icp", tick.Code)
	if len(inds) > 0 {
		for i, v := range inds {
			if i < len(inds)-2 {
				continue
			}
			_ = conn.HINCRBY("stock:push:industry", v, 1)
			conn.Expire("stock:push:industry", 3600*8)
			n, _ := conn.HGetFloat64("stock:push:industry", v)
			if n < 3 {
				conn.PushList("stock:industry:"+v, tick.Name)
				conn.Expire("stock:industry:"+v, 3600*8)
			} else if n == 3 {
				list := industryStock(id)
				data, _ := conn.PopList("stock:industry:" + v)
				var ret []string
				for _, v := range list {
					if util.ContainsAny(data, v) {
						continue
					}
					if len(ret) < 3 {
						ret = append(ret, v)
					}
				}
				return map[string]interface{}{
					"type":    "stock",
					"content": fmt.Sprintf("%s表现较好, 同属%s行业，建议关注同行业个股：%s", strings.Join(data, ","), v, strings.Join(ret, ",")),
				}
			} else {
				// 不推荐
				return nil
			}
		}
	}
	return map[string]interface{}{
		"type":    "stock",
		"content": fmt.Sprintf("%s目前涨幅%.1f%%", tick.Name, icp),
	}
}

func checkIndustryNew(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	ind, err := conn.HGetString("stock:industry:hs", tick.Code)
	icp, _ := conn.HGetFloat64("stock:industry:icp", tick.Code)

	_ = conn.HINCRBY("stock:push:industry", ind, 1)
	conn.Expire("stock:push:industry", 3600*8)
	n, _ := conn.HGetFloat64("stock:push:industry", ind)
	if err == nil && ind != "" {
		if n < 3 {
			conn.PushList("stock:industry:"+ind, tick.Name)
			conn.Expire("stock:industry:"+ind, 3600*8)
		} else if n == 3 {
			list := industryStockNew(ind)
			data, _ := conn.PopList("stock:industry:" + ind)
			var ret []string
			for _, v := range list {
				if util.ContainsAny(data, v) {
					continue
				}
				if len(ret) < 3 {
					ret = append(ret, v)
				}
			}
			if len(ret) > 0 {
				return map[string]interface{}{
					"type":    "stock",
					"content": fmt.Sprintf("%s表现较好, 同属%s行业，建议关注同行业个股：%s", strings.Join(data, ","), ind, strings.Join(ret, ",")),
				}
			}
		} else {
			// 不推荐
			logging.Info("stock.checkIndustryNew() escaped ", tick.Code, tick.Name, ind, n)
			return nil
		}
	}
	return map[string]interface{}{
		"type":    "stock",
		"content": fmt.Sprintf("%s目前涨幅%.1f%%", tick.Name, icp),
	}
}

// func perfBlock(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
//	_, err := conn.HGetString("performance:publish_date", tick.Code)
//	if err == nil {
//		ltg, _ := conn.HGetFloat64("stock:ltg", tick.Code)
//		if (stockChiNext(tick) && tick.NewPrice > tick.LastClose*1.2-0.01) || tick.NewPrice > tick.LastClose*1.1-0.01 || (float32(ltg)*tick.NewPrice > 20000000000 && tick.NewPrice > tick.LastClose*1.05-0.01) {
//			conn.HSet("performance:block:day1", tick.Code, tick.High)
//			return map[string]interface{}{
//				"type":    "stock",
//				"content": fmt.Sprintf("%s业绩披露连续上涨", tick.Name),
//			}
//		}
//	}
//	return nil
// }

func perfType(tick *Tick, conn *gredis.RedisConn) string {
	ret, _ := conn.HGetString("performance:type", tick.Code)
	switch ret {
	case "forecast":
		return "预告"
	case "announcement":
		return "公告"
	default:
		return ""
	}
}

func perfBlock2(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	high, err := conn.HGetFloat64("performance:block:day1", tick.Code)
	if err == nil && high > 0 {
		// ltg, _ := conn.HGetFloat64("stock:ltg", tick.Code)
		if tick.NewPrice > tick.LastClose*1.03-0.01 {
			if checkPreVol(tick, conn, 100000) {
				performanceIndustryStorage(tick, conn)
			}
			pfType := perfType(tick, conn)
			return map[string]interface{}{
				"code":             tick.Code,
				"name":             tick.Name,
				"type":             "performance",
				"highly_recommend": true,
				"pf_type":          pfType,
				"reason":           pfType + "涨停上涨",
				"publish_key":      "performance:block",
				"content":          fmt.Sprintf("%s业绩%s披露涨停后连续上涨", tick.Name, pfType),
			}
		}
	}

	return nil
}

func perfReturn(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	high, err := conn.HGetFloat64("performance:fallback", tick.Code)
	if err == nil && high > 0 {
		if tick.NewPrice > float32(high) {
			if checkPreVol(tick, conn, 100000) {
				performanceIndustryStorage(tick, conn)
			}
			pfType := perfType(tick, conn)
			// 反包
			return map[string]interface{}{
				"code":             tick.Code,
				"name":             tick.Name,
				"type":             "performance",
				"highly_recommend": true,
				"pf_type":          pfType,
				"reason":           pfType + "持续上涨",
				"publish_key":      "performance:fallback",
				"content":          fmt.Sprintf("%s业绩%s披露行情反包", tick.Name, pfType),
			}
		}
	}

	return nil
}

func perfRise(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	high, err := conn.HGetFloat64("performance:rise:0", tick.Code)
	if err == nil && high > 0 {
		if tick.NewPrice > float32(high)*1.01 {
			//if checkPreVol(tick, conn, 100000) {
			//	performanceIndustryStorage(tick, conn)
			//}
			ts := time.Now().Format(util.YMD)
			id, _ := conn.HGetFloat64("stock:trade_date", ts)
			if id > 0 {
				conn.HSet("performance:rise:day1", tick.Code, id)
				conn.HSet("performance:rise:high", tick.Code, tick.High)
				//conn.HSet("performance:publish_date", tick.Code, ts)
			}

			//return map[string]interface{}{
			//	"type":    "stock",
			//	"content": fmt.Sprintf("%s业绩披露连续上涨", tick.Name),
			//}
			//pfType := perfType(tick, conn)
			//return map[string]interface{}{
			//	"code":             tick.Code,
			//	"name":             tick.Name,
			//	"type":             "performance",
			//	"highly_recommend": true,
			//	"pf_type":          pfType,
			//	"reason":           pfType + "持续上涨",
			//	"content":          fmt.Sprintf("%s业绩%s披露连续上涨", tick.Name, pfType),
			//}
		}
	}
	return nil
}

func perfRise2(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	high, err := conn.HGetFloat64("performance:rise:high", tick.Code)
	if err == nil && high > 0 {
		id, _ := conn.HGetFloat64("performance:rise:day1", tick.Code)
		if tick.NewPrice > float32(high) && id > 0 {
			cur, _ := conn.HGetFloat64("stock:trade_date", time.Now().Format("2006-01-02"))
			if cur == id+1 {
				if checkPreVol(tick, conn, 100000) {
					performanceIndustryStorage(tick, conn)
				}
				pfType := perfType(tick, conn)
				return map[string]interface{}{
					"code":             tick.Code,
					"name":             tick.Name,
					"type":             "performance",
					"highly_recommend": true,
					"pf_type":          pfType,
					"reason":           pfType + "持续上涨",
					"publish_key":      "performance:rise",
					"content":          fmt.Sprintf("%s业绩%s披露连续上涨", tick.Name, pfType),
				}
			} else if cur > id+1 {
				conn.HDel("performance:rise:day1", tick.Code)
			}
		}
	}
	return nil
}

func perfRiseNew(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if high, err := conn.HGetFloat64("performance:rise:1", tick.Code); err == nil {
		if high > 0 && tick.NewPrice > float32(high) {
			if checkPreVol(tick, conn, 100000) {
				performanceIndustryStorage(tick, conn)
			}
			pfType := perfType(tick, conn)
			return map[string]interface{}{
				"code":             tick.Code,
				"name":             tick.Name,
				"type":             "performance",
				"highly_recommend": true,
				"pf_type":          pfType,
				"reason":           pfType + "持续上涨",
				"publish_key":      "performance:rise",
				"content":          fmt.Sprintf("%s业绩%s披露连续上涨", tick.Name, pfType),
			}
		}
	}

	return nil
}

func importantStock(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if conn.HExists("stock:monitoring:list", tick.Code) {
		// important
		change, _ := conn.HGetFloat64("stock:change", tick.Code)
		// 1216 change
		if change > 3 {
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s涨幅%.1f%%", tick.Name, change),
			}
		} else if change < -3 {
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s跌幅%.1f%%", tick.Name, -change),
			}
		}
	}
	return nil
}

func importantRapid(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if conn.HExists("stock:monitoring:list", tick.Code) {
		go func(code string, np float32) {
			timer := time.NewTimer(1 * time.Minute)
			<-timer.C
			_ = conn.HSet("stock:monitoring:pre", code, np)
			conn.Expire("stock:monitoring:pre", 8*3600)
		}(tick.Code, tick.NewPrice)

		// rapid up/down
		pre, _ := conn.HGetFloat64("stock:monitoring:pre", tick.Code)
		if pre > 0 && tick.NewPrice > 0 {
			if float64(tick.NewPrice) > pre*1.03 {
				return map[string]interface{}{
					"type":    "stock",
					"content": fmt.Sprintf("%s快速上涨", tick.Name),
				}
			} else if float64(tick.NewPrice) < pre*0.97 {
				return map[string]interface{}{
					"type":    "stock",
					"content": fmt.Sprintf("%s快速下杀", tick.Name),
				}
			}
		}
	}
	return nil
}

func ma3Attack(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	ok := conn.HExists("stock:ma3_platform", tick.Code)
	if ok && !stockNewStock(tick, conn) && tick.NewPrice > tick.LastClose*1.05 {
		if tick.Open < tick.LastClose*1.03 {
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s三日均线进攻", tick.Name),
			}
		}
	}
	return nil
}

func longPositiveShock(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	plat, _ := conn.HGetFloat64("stock:long_shock_platform", tick.Code)
	high, _ := conn.HGetFloat64("stock:long_shock_high", tick.Code)
	if plat > 0 && high > 0 && !stockNewStock(tick, conn) && (tick.NewPrice > float32(high) || tick.NewPrice > float32(plat)) {
		return map[string]interface{}{
			"type":    "stock",
			"content": fmt.Sprintf("%s长阳震荡后突破", tick.Name),
		}
	}
	return nil
}

func passivePositive(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	high, err := conn.HGetFloat64("stock:positive_platform", tick.Code)
	if err == nil && high > 0 && !stockNewStock(tick, conn) && (tick.NewPrice >= tick.LastClose*1.05 && tick.NewPrice > float32(high) || tick.NewPrice > tick.LastClose*1.07) {
		return map[string]interface{}{
			"type":    "stock",
			"content": fmt.Sprintf("%s阴线反包", tick.Name),
		}
	}

	return nil
}

func pastStrong(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	bExist := conn.HExists("stock:monitoring:strong", tick.Code)
	if bExist {
		go func(code string, np float32) {
			timer := time.NewTimer(1 * time.Minute)
			<-timer.C
			_ = conn.HSet("stock:monitoring:strong:pre", code, np)
			conn.Expire("stock:monitoring:strong:pre", 8*3600)
		}(tick.Code, tick.NewPrice)

		// rapid up/down
		pre, _ := conn.HGetFloat64("stock:monitoring:strong:pre", tick.Code)
		if pre > 0 && tick.NewPrice > 0 {
			pctg := 0.05
			if stockChiNext(tick) {
				pctg = 0.07
			}
			if float64(tick.NewPrice) > pre*(1+pctg) {
				return map[string]interface{}{
					"type":    "stock",
					"content": fmt.Sprintf("%s快速上涨", tick.Name),
				}
			} else if float64(tick.NewPrice) < pre*(1-pctg) {
				return map[string]interface{}{
					"type":    "stock",
					"content": fmt.Sprintf("%s快速下跌", tick.Name),
				}
			}
		}
	}
	return nil
}

func pastBlockOpen(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	bExist := conn.HExists("stock:monitoring:block_open", tick.Code)
	if bExist && tick.NewPrice > 0 && tick.LastClose > 0 {
		if tick.NewPrice > tick.LastClose*1.08 {
			icp := float64(tick.NewPrice/tick.LastClose-1) * 100
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s目前涨幅%.1f%%", tick.Name, icp),
			}
		} else if tick.NewPrice < tick.LastClose*0.92 {
			icp := float64(tick.NewPrice/tick.LastClose-1) * 100
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s目前跌幅%.1f%%", tick.Name, -icp),
			}
		}
	}
	return nil
}

func noticePositive(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	high, err := conn.HGetFloat64("stock:notice:high", tick.Code)
	if err == nil && high > 0 && tick.NewPrice > float32(high) {
		day, _ := conn.HGetString("stock:notice:day", tick.Code)
		return map[string]interface{}{
			"type":    "stock",
			"content": fmt.Sprintf("%s，%s公告行情反包", tick.Name, day),
		}
	}
	return nil
}

func importantPrice(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if tick.Code == "600519" && tick.NewPrice >= 2000 {
		return map[string]interface{}{
			"type":    "stock",
			"content": fmt.Sprintf("%s突破2000元大关", tick.Name),
		}
	}
	return nil
}

func newHighestWithRsi(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if conn.HExists("stock:rsi:yszk", tick.Code) {

	}
	return nil
}

func industryTransaction(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if conn.HExists("stock:industry:hs", tick.Code) {
		//5分钟
		go func(code string, np float32) {
			timer := time.NewTimer(5 * time.Minute)
			<-timer.C
			_ = conn.HSet("stock:monitoring:pre_5", code, np)
			conn.Expire("stock:monitoring:pre_5", 8*3600)
		}(tick.Code, tick.NewPrice)

		pre, _ := conn.HGetFloat64("stock:monitoring:pre_5", tick.Code)
		cLast, _ := conn.HGetFloat64("stock:monitoring:incr_3", tick.Code)
		if pre > 0 && tick.LastClose > 0 && tick.NewPrice-float32(pre) > 0.03*tick.LastClose {
			//5分钟内上涨逾3%
			if cLast == 0 {
				ts := time.Now().Format(util.YMD)
				conn.HSet("stock:monitoring:incr_3", tick.Code, 1)
				ind, _ := conn.HGetString("stock:industry:hs", tick.Code)
				if ind != "" && !strings.Contains(ind, "分配") && !strings.Contains(ind, "II") {
					//conn.HINCRBY("stock:industry:incr_3", ind, 1)
					conn.HSet("stock:industry:incr_3:"+ind, tick.Code, 1)
					//n, _ := conn.HGetFloat64("stock:industry:incr_3", ind)
					date, _ := conn.HGetString("stock:industry:last_push", ind)
					n, _ := conn.HLEN("stock:industry:incr_3:" + ind)
					if n >= 3 && date != ts {
						m, _ := conn.HGetAllStringMap("stock:industry:incr_3:" + ind)
						if c, _ := conn.HLEN("stock:industry:incr_3:" + ind); c < 3 {
							return nil
						} else {
							conn.Delete("stock:industry:incr_3:" + ind)
						}
						conn.HSet("stock:industry:last_push", ind, ts)
						var codeList []string
						var content, ext string
						for k, _ := range m {
							name, _ := conn.HGetString("stock:name", k)
							codeList = append(codeList, name)
						}
						content = fmt.Sprintf("%s行业异动", ind)
						if len(codeList) > 0 {
							ext = fmt.Sprintf("（%s）", strings.Join(codeList, ","))
						}
						return map[string]interface{}{
							"type":         "stock",
							"content":      content + ext,
							"content_read": content,
						}
					}
				}
			}
		} else if cLast == 1 {
			conn.HSet("stock:monitoring:incr_3", tick.Code, 0)
			ind, _ := conn.HGetString("stock:industry:hs", tick.Code)
			conn.HDel("stock:industry:incr_3:"+ind, tick.Code)
		}
	}
	return nil
}

func industryStock(id int) []string {
	var ret []string
	models.DB().Table("b_stock").Select("b_stock.name").Joins("LEFT JOIN u_performance_announcement ON u_performance_announcement.code = b_stock.id").Joins("LEFT JOIN u_stock_industry_relationship ON u_stock_industry_relationship.stock_id = b_stock.id").Where("u_stock_industry_relationship.industry_id = ?", id).Group("b_stock.name").Order("u_performance_announcement.net_profit_growth_rate DESC").Limit(5).Pluck("b_stock.name", &ret)
	return ret
}

func industryStockNew(ind string) []string {
	var ret []string
	models.DB().Table("b_stock_names").
		Select("b_stock_names.name").
		Joins("LEFT JOIN u_performance_announcement ON u_performance_announcement.code = b_stock_names.code").
		Joins("LEFT JOIN u_stock_industry ON u_stock_industry.code = b_stock_names.code").
		Joins("b_trade_date B ON B.trade_date = ?", time.Now().Format("2006-01-02")).
		Joins("LEFT JOIN b_trade_date C ON C.id = B.id - 1").
		Joins("LEFT JOIN p_stock_tick ON p_stock_tick.stock_code = b_stock_names.code AND p_stock_tick.trade_date = C.trade_date").
		Where("u_stock_industry.second_industry = ?", ind).
		Where("p_stock_tick.amount > 300000").
		Group("b_stock_names.name").
		Order("u_performance_announcement.net_profit_growth_rate DESC").Limit(5).
		Pluck("b_stock_names.name", &ret)
	return ret
}

func industry(tick *Tick) ([]string, int) {
	var industry []struct {
		ID           int
		IndustryName string
	}
	var ret []string
	if err := models.DB().Table("u_performance_industry").Select("u_performance_industry.id, u_performance_industry.industry_name").Joins("LEFT JOIN u_stock_industry_relationship ON u_performance_industry.id = u_stock_industry_relationship.industry_id").Where("u_stock_industry_relationship.stock_id = ?", tick.Code).Find(&industry).Error; err == nil {
		if len(industry) > 0 {
			ret = strings.Split(industry[0].IndustryName, "-")
			return ret, industry[0].ID
		}
	}

	return nil, 0
}

func stockChiNext(tick *Tick) bool {
	if strings.HasPrefix(tick.Code, "300") {
		return true
	}
	return false
}

func performanceIndustryStorage(tick *Tick, conn *gredis.RedisConn) {
	industry, err := performance.GetIndustryByCode(tick.Code)
	if err != nil {
		logging.Error("行情业绩获取"+tick.Code+"redis行业报错:", err.Error())
		return
	}
	pfType := perfType(tick, conn)
	reportPeriods := getIndustryStorageByReportPeriod(tick.Code, pfType, conn)
	for _, reportPeriod := range reportPeriods {
		err := conn.HSet("performance:industry:"+pfType+reportPeriod.Format(util.YMD)+":"+fmt.Sprintf("%d", industry.SecondId), tick.Code, tick.Name)
		conn.Expire("performance:industry:"+pfType+reportPeriod.Format(util.YMD)+":"+fmt.Sprintf("%d", industry.SecondId), 3*31*24*60*60)
		if err != nil {
			logging.Error("行情业绩记录"+tick.Code+"行业报错", err.Error())
		}
	}
}

func getIndustryStorageByReportPeriod(code, pfType string, conn *gredis.RedisConn) []time.Time {
	reportPeriods := make([]time.Time, 0)
	table := ""
	switch pfType {
	case performance.ANNOUNCEMENT:
		table = "u_performance_announcement"
		break
	case performance.FORECAST:
		table = "u_performance_forecast"
		break
	default:
		logging.Error("setIndustryStorageByReportPeriod 输入非法类型：", pfType)
		return nil

	}
	publishDate, err := conn.HGetString("performance:publish_date", code)
	//publishDate, err := gredis.Clone(setting.RedisSetting.StockDB).HGetString(data.PublishKey+":publish_date", data.Code)
	if err != nil {
		logging.Error("获取"+code+"披露日期失败", err.Error())
		return nil
	}
	err = models.DB().Table(table).Where("code=?", code).Where("publish_date=?", publishDate).Pluck("report_period", &reportPeriods).Error
	if err != nil {
		logging.Error("setIndustryStorageByReportPeriod取对应"+pfType+"失败 code:"+code+" 公告日期："+publishDate, err.Error())
		return nil
	}
	if len(reportPeriods) <= 0 {
		logging.Error("setIndustryStorageByReportPeriod获取" + publishDate + "报告期失败")
		return nil
	}
	return reportPeriods

}

func pushPerformanceIndustry(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	industry, err := performance.GetIndustryByCode(tick.Code)
	if err != nil {
		logging.Error("行情业绩获取"+tick.Code+"redis行业报错:", err.Error())
		return nil
	}
	pfType := perfType(tick, conn)
	reportPeriods := getIndustryStorageByReportPeriod(tick.Code, pfType, conn)
	for _, reportPeriod := range reportPeriods {
		performanceIndustryKey := "performance:industry:" + pfType + reportPeriod.Format(util.YMD) + ":" + fmt.Sprintf("%d", industry.SecondId)
		cnt, err := conn.HLEN(performanceIndustryKey)
		if err != nil {
			logging.Error("获取"+performanceIndustryKey+"code数量失败", err.Error())
		}
		if cnt == 3 {
			codeMap, err := conn.HGetAllStringMap(performanceIndustryKey)
			if err != nil {
				logging.Error("行情业绩"+industry.SecondIndustry+"行业数量获取失败", err.Error())
				return nil
			}
			codelist := ""
			for _, v := range codeMap {
				codelist = codelist + v + " "
			}
			season := ""
			switch reportPeriod.Format(util.MD) {
			case "03-31":
				season = "一季报"
				break
			case "06-30":
				season = "中报"
				break
			case "09-30":
				season = "三季报"
				break
			case "12-31":
				season = "年报"
				break
			default:
				break
			}
			if strings.HasSuffix(industry.SecondIndustry, "II") {
				continue
			}
			return map[string]interface{}{
				"type":             "performance",
				"highly_recommend": true,
				"content":          fmt.Sprintf("%s %s业绩%s后行情表现突出。请关注%s行业", codelist, season, pfType, industry.SecondIndustry),
			}
		}
	}
	return nil
}

func superCharge(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	rank := conn.ZRevRank("stock:zset:amount", tick.Code)
	if rank >= 0 && rank < 40 {
		go func(code string, np float32) {
			timer := time.NewTimer(5 * time.Minute)
			<-timer.C
			_ = conn.HSet("stock:supercharge:pre_5", code, np)
			conn.Expire("stock:supercharge:pre_5", 8*3600)
		}(tick.Code, tick.NewPrice)

		pre, _ := conn.HGetFloat64("stock:supercharge:pre_5", tick.Code)
		if conn.HExists("stock:supercharge:sell", tick.Code) {
			conn.HDel("stock:supercharge:sell", tick.Code)
			if v, _ := conn.HGetString("stock:supercharge:last", tick.Code); v != "sell" {
				conn.HSet("stock:supercharge:last", tick.Code, "sell")
				if float64(tick.NewPrice) <= pre*0.98 && pre > 0 {
					return map[string]interface{}{
						"type":    superType(),
						"content": fmt.Sprintf("%s 超卖", tick.Name),
					}
				}
			}
		} else if conn.HExists("stock:supercharge:buy", tick.Code) {
			conn.HDel("stock:supercharge:buy", tick.Code)
			if v, _ := conn.HGetString("stock:supercharge:last", tick.Code); v != "buy" {
				conn.HSet("stock:supercharge:last", tick.Code, "buy")
				if float64(tick.NewPrice) >= pre*1.02 && pre > 0 {
					return map[string]interface{}{
						"type":    superType(),
						"content": fmt.Sprintf("%s 超买", tick.Name),
					}
				}
			}
		}
	}
	return nil
}

func superType() string {
	t := time.Now()
	if t.Hour() == 9 && t.Minute() < 40 {
		return "pool"
	} else {
		return "stock"
	}
}

func checkPreVol(tick *Tick, conn *gredis.RedisConn, limit float64) bool {
	vol, err := conn.HGetFloat64("stock:pre_amount", tick.Code)
	if err != nil {
		logging.Error("redis "+tick.Code+" 获取前一交易日成交额失败", err.Error())
		return false
	}
	return vol >= limit
}

func lastTradeID(tick *Tick, conn *gredis.RedisConn) int {
	var t time.Time
	lastPush, err := conn.HGetString("stock:limit:last_push", tick.Code)
	if err != nil {
		return 0
	}

	t, err = time.ParseInLocation(util.YMDHMS, lastPush, time.Local)
	if err != nil {
		return 0
	}

	ret, _ := conn.HGetFloat64("stock:trade_date", t.Format("2006-01-02"))
	return int(ret)
}

func curTradeID(conn *gredis.RedisConn) int {
	ret, _ := conn.HGetFloat64("stock:trade_date", time.Now().Format(util.YMD))
	return int(ret)
}

func recommendStockIncreaseOne2Four(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	return recommendStockIncrease(tick, conn, 0)
}

func recommendStockIncreaseFour(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if time.Now().Format(util.MD) > "01-01" && time.Now().Format(util.MD) <= "04-30" {
		return recommendStockIncrease(tick, conn, -1)
	}
	return nil
}

func recommendStockIncrease(tick *Tick, conn *gredis.RedisConn, offset int) map[string]interface{} {
	preTenPushedKey := "pre_ten:push"
	zsz := decimal.Zero
	nowPrice := decimal.Zero
	/*bigLimit:=decimal.NewFromInt(200*100000000)
	bigRateLimit:=decimal.NewFromInt(20)
	midLimit:=decimal.NewFromInt(50*100000000)
	midRateLimit := decimal.NewFromInt(30)*/
	limitList := []struct {
		Vol  decimal.Decimal
		Rate decimal.Decimal
	}{
		{
			decimal.NewFromInt(200 * 100000000),
			decimal.NewFromInt(20),
		},
		{
			decimal.NewFromInt(50 * 100000000),
			decimal.NewFromInt(30),
		},
	}
	preTenRecommend := performance.PreTenRecommend{}
	reportPeriod, _ := util.ReportPeriod(offset)
	if gredis.HExists(performance.PreTenRecommendKey+reportPeriod, tick.Code) && !gredis.HExists(preTenPushedKey+reportPeriod, tick.Code) {
		preTenBytes, err := gredis.HGetBytes(performance.PreTenRecommendKey+reportPeriod, tick.Code)
		if err != nil {
			logging.Error("获取"+tick.Code+"推荐时刻信息失败", err.Error())
			return nil
		}
		err = json.Unmarshal(preTenBytes, &preTenRecommend)
		if err != nil {
			logging.Error("解析"+tick.Code+"推荐时刻信息失败", err.Error())
			return nil
		}
		//已出公告则不监控
		if preTenRecommend.PublishDate != nil && preTenRecommend.PublishDate.Format(util.YMD) <= time.Now().Format(util.YMD) {
			/*err=gredis.HDel(performance.PreTenRecommendKey+reportPeriod,tick.Code)
			if err != nil {
				logging.Error(performance.PreTenRecommendKey+reportPeriod+"停止监控"+tick.Code+"失败")
			}*/
			return nil
		}
		ltg, err := conn.HGetString("stock:ltg", tick.Code)
		if err != nil {
			logging.Error("获取"+tick.Code+"流通股失败", err.Error())
			return nil
		}
		err = zsz.Scan(ltg)
		if err != nil {
			logging.Error("解析"+tick.Code+"流通股失败", err.Error())
			return nil
		}
		err = nowPrice.Scan(tick.NewPrice)
		if err != nil {
			logging.Error("解析"+tick.Code+"现价失败", err.Error())
			return nil
		}
		for _, v := range limitList {
			if zsz.Mul(nowPrice).GreaterThan(v.Vol) {
				rate := decimal.NullDecimal{}
				if preTenRecommend.DayCount > 10 {
					preClose := decimal.NullDecimal{}
					preCloseStr, err := gredis.HGetString(stock.PreTenPreCloseKey, tick.Code)
					if err != nil {
						logging.Error("获取前10交易日preClose报错", err.Error())
					}
					err = preClose.Scan(preCloseStr)
					if err != nil {
						logging.Error("解析前10交易日preClose"+preCloseStr+"报错", err.Error())
					}
					rate = util.CalculateRate(preClose, decimal.NullDecimal{nowPrice, true})
					preTenRecommend.DayCount = 10
				} else {
					rate = util.CalculateRate(decimal.NullDecimal{preTenRecommend.PreClose, true}, decimal.NullDecimal{nowPrice, true})
				}
				if rate.Valid {
					if rate.Decimal.GreaterThan(v.Rate) {
						err = gredis.HSet(preTenPushedKey+reportPeriod, tick.Code, time.Now().Format(util.YMDHMS))
						if err != nil {
							logging.Error("记录"+tick.Code+" "+reportPeriod+"已推送失败", err.Error())
						}
						rate.Decimal = rate.Decimal.Round(2)
						tc := performance.TraceContent{
							Code:     tick.Code,
							Name:     tick.Name,
							Reason:   "业绩推荐",
							PfType:   performance.ANNOUNCEMENT,
							From:     performance.FROM_RECOMMEND,
							NowPrice: tick.NewPrice,
						}
						go tc.SetPerformanceTrace(reportPeriod, true)
						return map[string]interface{}{
							"content": fmt.Sprintf("%s业绩推荐股票，近%d个交易日涨幅%s", tick.Name, preTenRecommend.DayCount, util.NullDecimal2String(rate)+"%"),
							"code":    tick.Code,
							"name":    tick.Name,
							"type":    "performance",
						}
					}
				}
				break
			}
		}
	}
	return nil
}
