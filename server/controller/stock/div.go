package stock

import (
	"datacenter/pkg/gredis"
	"fmt"
	"time"
)

func divTick(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	tm := time.Now().Format("15:04:05")
	if !conn.HExists("stock:div:"+tick.Code, tm) {
		conn.HSet("stock:div:"+tick.Code, tm, tick.NewPrice)
		conn.Expire("stock:div:"+tick.Code, 3600*8)
	}
	return nil
}

func largeAmount(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	rank := conn.ZRevRank("stock:zset:amount", tick.Code)
	if rank >= 0 && rank < 300 {
		return map[string]interface{}{
			"type":        "stock",
			"description": "成交额前300",
			"content":     fmt.Sprintf("%s：成交额较大", tick.Name),
		}
	}
	return nil
}

func focusRapid(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	// rapid up/down
	tm := time.Now()
	for i := 0; i < 30; i++ {
		ts := tm.Add(-1 * time.Second * time.Duration(i+60)).Format("15:04:05")
		if conn.HExists("stock:div:"+tick.Code, ts) {
			pre, _ := conn.HGetFloat64("stock:div:"+tick.Code, ts)
			preAmount, _ := conn.HGetFloat64("stock:pre_amount", tick.Code)
			if preAmount < 300000 || stockZgb(tick.Code, conn)*float64(tick.NewPrice) < 20000000000  {
				return nil
			}
			if pre > 0 && tick.NewPrice > 0 {
				if float64(tick.NewPrice) > pre*1.03 {
					return map[string]interface{}{
						"type":    "stock",
						"content": fmt.Sprintf("%s快速上涨", tick.Name),
					}
				} else if float64(tick.NewPrice) < pre*0.97 {
					return map[string]interface{}{
						"type":    "stock",
						"content": fmt.Sprintf("%s快速下跌", tick.Name),
					}
				}
			}
			return nil
		}
	}

	return nil
}

func divAuctionAmount(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	bp := float64(tick.BP[0])
	bv := float64(tick.BV[0])

	// bv手数
	// 集合委托大于3000万
	// 剔除新股
	amount := bv * bp * 100
	if amount > 30000000 {
		var amountStr string
		if amount > 100000000 {
			amountStr = fmt.Sprintf("%.2f亿", amount/100000000)
		} else {
			amountStr = fmt.Sprintf("%d万", int(amount/10000))
		}
		return map[string]interface{}{
			"type":    "stock",
			"content": fmt.Sprintf("%s: 集合成交%s", tick.Name, amountStr),
		}
	}

	return nil
}

func divAuctionOpen(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	if tick.LastClose > 0 && tick.NewPrice > tick.LastClose*1.03 {
		pct := (tick.NewPrice/tick.LastClose - 1) * 100
		return map[string]interface{}{
			"type":    "stock",
			"content": fmt.Sprintf("%s: 集合大幅高开%.2f%%", tick.Name, pct),
		}
	}

	return nil
}

func divNewHighest(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	np, err := conn.HGetFloat64("stock:div:highest_price", tick.Code)
	if np == 0 || err != nil {
		np, _ = conn.HGetFloat64("stock:limit:highest_price", tick.Code)
		if np > 0 {
			_ = conn.HSetValue("stock:div:highest_price", tick.Code, fmt.Sprintf("%.02f", np))
		} else {
			return nil
		}
	}
	if (np+0.01 <= float64(tick.NewPrice) || np+0.01 <= float64(tick.High)) && tick.LastClose > 0 {
		_ = conn.HSetValue("stock:div:highest_price", tick.Code, fmt.Sprintf("%.02f", tick.High))
		// 新高
		return map[string]interface{}{
			"type":        "stock",
			"description": "创历史新高",
			"content":     fmt.Sprintf("%s：突破历史新高", tick.Name),
		}
	}

	return nil
}

func divAmountIncr(tick *Tick, conn *gredis.RedisConn) map[string]interface{} {
	preAmount, _ := conn.HGetFloat64("stock:pre_amount", tick.Code)
	if preAmount < 300000 || stockZgb(tick.Code, conn)*float64(tick.NewPrice) < 20000000000  {
		return nil
	}
	if tick.Amount > 200000 && tick.LastClose > 0 {
		if tick.NewPrice > tick.LastClose*1.05 {
			change := (tick.NewPrice/tick.LastClose - 1) * 100
			return map[string]interface{}{
				"type":    "stock",
				"content": fmt.Sprintf("%s：目前涨幅%.2f%%", tick.Name, change),
			}
		}
	}
	return nil
}

func stockZgb(code string, conn *gredis.RedisConn) float64 {
	data, err := conn.HGetFloat64("stock:zgb", code)
	if err != nil || data == 0 {
		return 0
	}
	return data
}