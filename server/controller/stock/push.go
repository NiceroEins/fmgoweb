package stock

import (
	"datacenter/models/message"
	"datacenter/models/stock"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/websocket"
	"encoding/json"
	"github.com/shopspring/decimal"
	"strings"
	"time"
)

func push(m map[string]interface{}) {
	ws := websocket.GetInstance()
	bt, _ := json.Marshal(m)
	msg := message.ParseMsg(message.Message_Stock, "system", "", string(bt), true)
	logging.Info("stock.push() msg: ", string(bt))
	ws.SysSend(msg)
}

func pushPerf(m map[string]interface{}) {
	ws := websocket.GetInstance()
	bt, _ := json.Marshal(m)
	msg := message.ParseMsg(message.Message_Performance_Stock, "system", "", string(bt), true)
	ws.SysSend(msg)
}

func pool(tick *Tick, name string, conn *gredis.RedisConn, timeOutAt time.Time) {
	// test
	//con := gredis.Clone(17)
	if conn == nil {
		return
	}
	if isST(tick, conn) {
		return
	}

	price, err := conn.HGetFloat64("stock:tick", tick.Code)
	if err != nil {
		return
	}

	go stock.CreateOrUpdateStockPool(&stock.StockPoolTypeIO{
		Code:          tick.Code,
		Name:          name,
		TransferPrice: decimal.NewFromFloat(price),
		TimeoutAt:     &timeOutAt,
	})
}

func stockPool(code string, name string, conn *gredis.RedisConn, timeOutAt time.Time) {
	tsCode := code
	if len(code) > 6 {
		code = string([]rune(code)[2:])
	} else {
		if strings.HasPrefix(code, "6") {
			tsCode = "SH" + code
		} else {
			tsCode = "SZ" + code
		}
	}
	pool(&Tick{
		TsCode: tsCode,
		Code:   code,
	}, name, conn, timeOutAt)
}

func isST(tick *Tick, conn *gredis.RedisConn) bool {
	if tick.Name == "" && conn != nil {
		tick.Name, _ = conn.HGetString("stock:name", tick.Code)
	}
	if strings.HasPrefix(tick.Name, "ST") || strings.HasPrefix(tick.Name, "st") || strings.HasPrefix(tick.Name, "*ST") {
		return true
	}
	return false
}
