package stock

import (
	msg "datacenter/middleware/message"
	"datacenter/models"
	"datacenter/models/home"
	"datacenter/models/message"
	"datacenter/models/stock"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func TestTick(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()
	//conn := gredis.Clone(setting.RedisSetting.StockDB)
	//m, _ := conn.HGetAllStringMap("stock:tick")
	//var data []struct {
	//	StockCode string
	//	Industry  string
	//	StockName string
	//}
	//if err := models.DB().Table("p_public_company").Find(&data).Error; err != nil {
	//	t.Error("stock.fetchCompanyIndustry() Errors: ", err.Error())
	//	return
	//}
	//s := make(map[string]string)
	//for _, v := range data {
	//	s[v.StockCode] = v.StockName
	//}
	//for k, _ := range m {
	//	code := string([]byte(k)[2:])
	//	name, ok := s[code]
	//	if ok {
	//		println(k, name)
	//	} else {
	//		println(k)
	//	}
	//}
	bt, _ := json.Marshal(map[string]interface{}{
		"type":    "stock",
		"content": fmt.Sprintf("%s：小连阳后突破", "蒋益峰"),
		"time":    time.Now().Format("2006-01-02 15:04:05"),
	})
	msg.Push(string(bt), message.Message_Stock)
}

func TestPerf(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	var codes []string
	models.DB().Table("p_stock_tick").Where("trade_date = ?", "2020-10-14").Where("`change` > 0").Pluck("stock_code", &codes)
	conn := gredis.Clone(17)
	m, _ := conn.HGetAllStringMap("stock:increase")
	for _, v := range codes {
		if _, ok := m[v]; !ok {
			fmt.Println(v)
		}
	}

}

func TestMonitor(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	conn := gredis.Clone(17)
	list, _ := conn.FetchList("stock:monitoring")
	m, _ := conn.HGetAllStringMap("stock:name")
	for _, v := range list {
		for code, name := range m {
			if name == v {
				np, _ := conn.HGetFloat64("stock:tick", code)
				_ = conn.HSet("stock:monitoring:list", code, np)
			}
		}
	}
}

func TestPersonal(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	home.PushByCode(map[string]interface{}{
		"type":    "stock",
		"content": fmt.Sprintf("伊利股份：突破历史新高"),
	}, "600887", "")
}

func TestFetchCompanyIndustry(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()
	fetchCompanyIndustry()
}

func TestRecommend(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()
	conn := gredis.Clone(setting.RedisSetting.StockDB)
	recommendStockIncreaseFour(&Tick{
		Code: "000001",
		Name: "平安银行",
		Tick: &stock.Tick{
			NewPrice: 15,
		},
	}, conn)
	time.Sleep(5 * time.Second)
}

func TestSetHsIndustry(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	setHsIndustry()
}

func TestFund1(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	var day int = 3
	var pctg float64 = 0.07
	var pre int = 7
	//var summary Summary
	for i := 0; i < 11; i++ {
		var data []FundTick
		var idxLast int
		//var single Summary
		models.Data().Select("p_fund_tick.*, p_fund.name").Table("p_fund_tick").
			Joins("LEFT JOIN p_fund ON p_fund_tick.fund_id = p_fund.id").
			Where("p_fund_tick.status = ?", "normal").
			Where("p_fund_tick.deleted = ?", 0).
			Where("p_fund_tick.fund_id = ?", i).
			Order("p_fund_tick.value_date ASC").
			LogMode(false).
			Find(&data)
		if len(data) <= 0 {
			continue
		}
		fmt.Printf("\n\n\n=======基金代码：%d 名称：%s=========\n", data[i].FundID, data[i].Name)
		for j := day - 1; j < len(data); j++ {
			if data[j-day+1].Value-data[j].Value > pctg {
				var maxRec float64
				var idxStart int
				var maxValue, minValue float64
				minValue = 9999
				for k := pre; k > 0; k-- {
					if j-k-day < 0 {
						continue
					} else if idxStart == 0 {
						idxStart = j - k - day
					}
					for l := idxStart; l < j-day-k; l++ {
						rec := data[l].Value - data[j-k-day].Value
						if rec > maxRec {
							maxRec = rec
						}
					}
					if data[j-k-day].Value > maxValue {
						maxValue = data[j-k-day].Value
					}
					if data[j-k-day].Value < minValue {
						minValue = data[j-k-day].Value
					}
					printFund(&data[j-k-day], nil)
				}
				for k := day; k > 0; k-- {
					if j-k < 0 {
						continue
					}
					printFund(&data[j-k], nil)
				}
				fmt.Printf("回撤点->")
				printFund(&data[j], nil)
				if idxLast > 0 {
					fmt.Printf("距上个回撤点%d个交易日\n", j-idxLast)
				}
				fmt.Printf("最大回撤%.4f\n", maxRec)
				fmt.Printf("最高点与最低点差值%.4f\n\n", maxValue-minValue)
				idxLast = j
				j += day - 1
				//for k := 0; k < 3; k++ {
				//	if j+k >= len(data) {
				//		continue
				//	}
				//	c := printFund(&data[j+k], &data[j])
				//	if k == 0 {
				//		continue
				//	}
				//	switch c {
				//	case 1:
				//		single.Rise++
				//		summary.Rise++
				//	case -1:
				//		single.Drop++
				//		summary.Drop++
				//	case 0:
				//		single.Fair++
				//		summary.Drop++
				//	}
				//}
			}
		}
		//fmt.Printf("统计：上涨：%d\t下跌：%d\t持平：%d\t", single.Rise, single.Drop, single.Fair)
	}
	//fmt.Printf("\n\n统计：上涨：%d\t下跌：%d\t持平：%d\t", summary.Rise, summary.Drop, summary.Fair)
}

func TestFund2(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	var day int = 7
	var pctg float64 = 0.07
	var sumUp, sumDown Summary
	fmt.Printf("参数\n观测周期：%d天，周期内最大波动：%.3f", day, pctg)
	for i := 0; i < 11; i++ {
		var data []FundTick
		var single Summary
		models.Data().Select("p_fund_tick.*, p_fund.name").Table("p_fund_tick").
			Joins("LEFT JOIN p_fund ON p_fund_tick.fund_id = p_fund.id").
			Where("p_fund_tick.status = ?", "normal").
			Where("p_fund_tick.deleted = ?", 0).
			Where("p_fund_tick.fund_id = ?", i).
			Order("p_fund_tick.value_date ASC").
			LogMode(false).
			Find(&data)
		if len(data) <= 0 {
			continue
		}
		fmt.Printf("\n\n\n=======基金代码：%d 名称：%s=========\n", data[i].FundID, data[i].Name)
		var bPlat bool
		for j := day; j < len(data); j++ {
			bPlat = true
			idxLow := j - 1
			idxHigh := j - 1
			for k := 2; k <= day; k++ {
				if data[j-1].Value-data[j-k].Value > pctg || data[j-1].Value-data[j-k].Value < pctg*-1 {
					bPlat = false
					break
				}
				if data[j-k].Value < data[idxLow].Value {
					idxLow = j - k
				}
				if data[j-k].Value > data[idxHigh].Value {
					idxHigh = j - k
				}
			}
			if !bPlat {
				continue
			}

			if data[j].Value-data[j-day].Value > pctg {
				for k := day; k > 0; k-- {
					if j-k < 0 {
						continue
					}
					printFund(&data[j-k], nil)
				}
				fmt.Printf("向上突破->")
				for k := 0; k <= 3; k++ {
					var c int
					if j+k >= len(data) {
						continue
					}

					if k == 0 {
						c = printFund(&data[j+k], nil)
						continue
					} else {
						c = printFund(&data[j+k], &data[idxHigh])
					}
					switch c {
					case 1:
						single.Rise++
						sumUp.Rise++
					case -1:
						single.Drop++
						sumUp.Drop++
					case 0:
						single.Fair++
						sumUp.Drop++
					}
				}
				fmt.Printf("\n")
			}

			if data[j].Value-data[j-day].Value < pctg*-1 {
				for k := day; k > 0; k-- {
					if j-k < 0 {
						continue
					}
					printFund(&data[j-k], nil)
				}
				fmt.Printf("向下突破->")
				for k := 0; k <= 3; k++ {
					var c int
					if j+k >= len(data) {
						continue
					}

					if k == 0 {
						c = printFund(&data[j+k], nil)
						continue
					} else {
						c = printFund(&data[j+k], &data[idxLow])
					}
					switch c {
					case 1:
						single.Rise++
						sumDown.Rise++
					case -1:
						single.Drop++
						sumDown.Drop++
					case 0:
						single.Fair++
						sumDown.Drop++
					}
				}
				fmt.Printf("\n")
			}
		}
		fmt.Printf("统计：上涨：%d\t下跌：%d\t持平：%d\n", single.Rise, single.Drop, single.Fair)
	}

	fmt.Printf("\n\n\n总计：\n")
	fmt.Printf("向上突破：上涨：%d\t下跌：%d\t持平：%d\n", sumUp.Rise, sumUp.Drop, sumUp.Fair)
	fmt.Printf("向下突破：上涨：%d\t下跌：%d\t持平：%d\n", sumDown.Rise, sumDown.Drop, sumDown.Fair)
	fmt.Printf("合计：上涨：%d\t下跌：%d\t持平：%d\n", sumUp.Rise+sumDown.Rise, sumUp.Drop+sumDown.Drop, sumUp.Fair+sumDown.Fair)
}

func TestFund3(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	var day int = 7
	var pctg float64 = 0.08
	var after int = 3
	//var summary Summary
	for i := 0; i < 11; i++ {
		var data []FundTick
		//var idxLast int
		var single Summary
		models.Data().Select("p_fund_tick.*, p_fund.name").Table("p_fund_tick").
			Joins("LEFT JOIN p_fund ON p_fund_tick.fund_id = p_fund.id").
			Where("p_fund_tick.status = ?", "normal").
			Where("p_fund_tick.deleted = ?", 0).
			Where("p_fund_tick.fund_id = ?", i).
			Order("p_fund_tick.value_date ASC").
			LogMode(false).
			Find(&data)
		if len(data) <= 0 {
			continue
		}
		fmt.Printf("\n\n\n=======基金代码：%d 名称：%s=========\n", data[i].FundID, data[i].Name)
		var bPlat bool
		for j := day; j < len(data); j++ {
			bPlat = true
			idxLow := j - 1
			idxHigh := j - 1
			for k := 2; k <= day; k++ {
				if data[j-1].Value-data[j-k].Value > pctg || data[j-1].Value-data[j-k].Value < pctg*-1 {
					bPlat = false
					break
				}
				if data[j-k].Value < data[idxLow].Value {
					idxLow = j - k
				}
				if data[j-k].Value > data[idxHigh].Value {
					idxHigh = j - k
				}
			}
			if !bPlat {
				continue
			}
			if j+after >= len(data) {
				continue
			}
			c := data[j+after].Value - data[j].Value
			if c > 0 {
				single.Rise++
			} else if c < 0 {
				single.Drop++
			} else {
				single.Fair++
			}
			j += day - 1
		}
		fmt.Printf("统计：上涨：%d\t下跌：%d\t持平：%d\n", single.Rise, single.Drop, single.Fair)
	}
}

type FundTick struct {
	FundID    int
	Name      string
	Value     float64
	ValueDate time.Time
}

type Summary struct {
	Rise int
	Drop int
	Fair int
}

func printFund(ft *FundTick, base *FundTick) int {
	var ret int
	var desc string
	if base != nil {
		switch {
		case ft.Value > base.Value:
			desc = "上涨"
			ret = 1
		case ft.Value < base.Value:
			desc = "下跌"
			ret = -1
		case ft.Value == base.Value:
			desc = "持平"
			ret = 0
		}
	}

	fmt.Printf("净值: %.4f\t%s\t日期: %s\n", ft.Value, desc, ft.ValueDate.Format(util.YMD))
	return ret
}
