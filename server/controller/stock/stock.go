package stock

import (
	"datacenter/models/home"
	"datacenter/models/message"
	"datacenter/models/quantization"
	"datacenter/models/stock"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/pkg/websocket"
	"encoding/json"
	"math"
	"strings"
	"sync/atomic"
	"time"
)

var Instance *Stock

type Stock struct {
	filters  []*Filter
	global   []*Filter
	personal []*Filter
	group    map[string][]*Filter
}

type Filter struct {
	Freq         time.Duration //冷却周期
	SecBegin     int           //可推送开始时间
	SecEnd       int           //可推送结束时间
	Name         string        `json:"name"`
	Description  string        `json:"description"`
	Func         func(tick *Tick, conn *gredis.RedisConn) map[string]interface{}
	AfterFunc    func(tick *Tick, conn *gredis.RedisConn) map[string]interface{}
	PoolOut      int    //移出股池周期（天）
	PoolOutTrunc bool   //取整（取当日0点）
	Type         string //推送类型
	conn         *gredis.RedisConn
	locker       [1000000]int64
	//pushLocker    sync.Mutex
	pushMutex     int64
	DuplicatePush bool //同时推送Func及AfterFunc返回
	Group         string
	Serialization bool //串行（顺序）执行
	//sec         *sync.Map //map[string]time.Time
}

type Tick struct {
	*stock.Tick
	TsCode string
	Code   string
	Name   string
	Number int64
	//Change float32
}

func Init() {
	Instance.group = make(map[string][]*Filter)
	if setting.StockSetting.Source == "inner" {
		con := "stock"
		conn := internalConnect(con)
		go monitor(con, conn)
	} else {
		cons := []string{"SH", "SZ"}
		for _, v := range cons {
			conn := connect(v)
			go monitor(v, conn)
			//test
			//time.Sleep(3 * time.Second)
		}
	}
}

//TODO: cron定时任务更新新股
func Setup() {
	stock.Setup()
	Instance = &Stock{
		filters: make([]*Filter, 0),
	}

	if setting.StockSetting.Source != "" {
		//fetchCompanyIndustry()
		//fetchLimit()
		//fetchPlatform()
		//positivity()
		//fetchBlock()
		//updateLimit()
		//blockPlatform()
		//ma3Platform()
		//passivePositivePlatform()
		//perf()
		//notice()
		//rsi()
		//tradeDate()
		//go preNew()
		//rsiEx()
		//go quantization.FetchAll()
		//setHsIndustry()
		//lastPriceAvg()
		//importantStockListNew()
		//go quantization.SuperCharge()
		// 初始数据入库
		// ws建立连接
		Init()
		Instance.Global(&Filter{
			Freq:        0,
			SecBegin:    sec(9, 20, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "ts_record",
			Description: "实时行情全数据",
			Func:        tsRecord,
		}).Use(&Filter{
			Freq:        0,
			SecBegin:    sec(9, 20, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "record",
			Description: "实时行情录入",
			Func:        record,
		}).Use(&Filter{
			Freq:        0,
			SecBegin:    sec(9, 20, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "div_tick",
			Description: "实时行情分时数据",
			Func:        divTick,
		}).Use(&Filter{
			Freq:          10 * time.Hour,
			SecBegin:      sec(9, 25, 0),
			SecEnd:        sec(15, 31, 0),
			Name:          "highest",
			Description:   "突破历史新高",
			Func:          newHighest,
			AfterFunc:     newHighestEx,
			PoolOut:       3,
			PoolOutTrunc:  true,
			DuplicatePush: true,
		}).Use(&Filter{
			Freq:         20 * time.Hour,
			SecBegin:     sec(9, 20, 0),
			SecEnd:       sec(9, 25, 0),
			Name:         "auction_amount",
			Description:  "集合竞价大金额",
			Func:         auctionAmount,
			PoolOut:      1,
			PoolOutTrunc: true,
			Group:        "auction",
		}).Use(&Filter{
			Freq:         20 * time.Hour,
			SecBegin:     sec(9, 20, 0),
			SecEnd:       sec(9, 25, 1),
			Name:         "auction",
			Description:  "集合竞价抢筹",
			Func:         auction,
			PoolOut:      1,
			PoolOutTrunc: true,
			Group:        "auction",
		}).Use(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(14, 59, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "clear",
			Description: "清理当日缓存记录",
			Func:        clear,
		}).Use(&Filter{
			Freq:         120 * time.Hour,
			SecBegin:     sec(9, 25, 0),
			SecEnd:       sec(15, 31, 0),
			Name:         "platform",
			Description:  "平台突破",
			Func:         breakthrough,
			PoolOut:      3,
			PoolOutTrunc: true,
		}).Use(&Filter{
			Freq:        20 * time.Hour,
			SecBegin:    sec(9, 20, 0),
			SecEnd:      sec(9, 25, 1),
			Name:        "auction_record",
			Description: "集合竞价逐笔行情记录",
			Func:        auctionRecord,
		}).Use(&Filter{
			Freq:         10 * time.Hour,
			SecBegin:     sec(9, 25, 0),
			SecEnd:       sec(15, 31, 0),
			Name:         "continuous_positivity_breakthrough",
			Description:  "小连阳突破",
			Func:         continuousPositivity,
			PoolOut:      3,
			PoolOutTrunc: true,
		}).Use(&Filter{
			Freq:         10 * time.Hour,
			SecBegin:     sec(9, 25, 0),
			SecEnd:       sec(15, 31, 0),
			Name:         "block_increase",
			Description:  "涨停后换手",
			Func:         amountIncrease,
			PoolOut:      3,
			PoolOutTrunc: true,
		}).Use(&Filter{
			Freq:         10 * time.Hour,
			SecBegin:     sec(9, 25, 0),
			SecEnd:       sec(15, 31, 0),
			Name:         "chinext_increase",
			Description:  "创业板上涨",
			Func:         chiNextIncr,
			AfterFunc:    checkIndustryNew,
			PoolOut:      1,
			PoolOutTrunc: true,
			Group:        "chinext",
		}).Use(&Filter{
			Freq:         10 * time.Hour,
			SecBegin:     sec(9, 25, 0),
			SecEnd:       sec(15, 31, 0),
			Name:         "chinext_block",
			Description:  "创业板涨停",
			Func:         chiNextBlock,
			PoolOut:      1,
			PoolOutTrunc: true,
			Group:        "chinext",
		}).Use(&Filter{
			Freq:          82 * time.Hour,
			SecBegin:      sec(9, 25, 0),
			SecEnd:        sec(15, 31, 0),
			Name:          "performance_block2",
			Description:   "业绩披露涨停后连续上涨",
			Func:          perfBlock2,
			AfterFunc:     pushPerformanceIndustry,
			Type:          "performance",
			DuplicatePush: true,
			Group:         "performance",
		}).Use(&Filter{
			Freq:          82 * time.Hour,
			SecBegin:      sec(9, 25, 0),
			SecEnd:        sec(15, 31, 0),
			Name:          "performance_fallback_return",
			Description:   "业绩披露行情反包",
			Func:          perfReturn,
			AfterFunc:     pushPerformanceIndustry,
			Type:          "performance",
			DuplicatePush: true,
			Group:         "performance",
		}).Use(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "performance_rise_day1",
			Description: "业绩披露连续上涨day1",
			//Func:        perfRise,
			//AfterFunc:   pushPerformanceIndustry,
			Type: "performance",
		}).Use(&Filter{
			Freq:          82 * time.Hour,
			SecBegin:      sec(9, 25, 0),
			SecEnd:        sec(15, 31, 0),
			Name:          "performance_rise_day2",
			Description:   "业绩披露连续上涨day2",
			Func:          perfRiseNew,
			AfterFunc:     pushPerformanceIndustry,
			Type:          "performance",
			DuplicatePush: true,
			Group:         "performance",
		}).Use(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "important_stock",
			Description: "重点股票涨跌幅3%以上",
			Func:        importantStock,
		}).Use(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "important_rapid",
			Description: "重点股票快速上涨下杀",
			Func:        importantRapid,
		}).Use(&Filter{
			Freq:        48 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "ma3_attack",
			Description: "三日均线进攻",
			//Func:        ma3Attack,
		}).Use(&Filter{
			Freq:        120 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "long_positive_shock",
			Description: "长阳震荡",
			Func:        longPositiveShock,
		}).Use(&Filter{
			Freq:        120 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "passive_positive",
			Description: "阴线反包",
			Func:        passivePositive,
		}).Use(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "past_strong",
			Description: "近三日强势股",
			//Func:        pastStrong,
		}).Use(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "past_block_open",
			Description: "昨日炸板股",
			//Func:        pastBlockOpen,
		}).Use(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "notice_positive",
			Description: "公告行情反包",
			Func:        noticePositive,
		}).Use(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "important_price",
			Description: "重要股票突破价位",
			//Func:        importantPrice,
		}).Use(&Filter{
			Freq:        5 * time.Second,
			SecBegin:    sec(9, 30, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "supercharge",
			Description: "超买超卖",
			Func:        superCharge,
		}).Use(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 30, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "industry_transaction",
			Description: "行业异动",
			Func:        industryTransaction,
		}).Personal(&Filter{
			Freq:        20 * time.Hour,
			SecBegin:    sec(11, 29, 0),
			SecEnd:      sec(11, 31, 0),
			Name:        "personal_large_amount_mid",
			Description: "个人-大成交额-午间",
			//Func:        largeAmount,  //改由定时任务执行
		}).Personal(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(14, 59, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "personal_large_amount_after",
			Description: "个人-大成交额-盘后",
			//Func:        largeAmount,   //改由定时任务执行
		}).Personal(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "personal_rapid",
			Description: "个人-快速上涨下跌",
			Func:        focusRapid,
		}).Personal(&Filter{
			Freq:        20 * time.Hour,
			SecBegin:    sec(9, 20, 0),
			SecEnd:      sec(9, 25, 1),
			Name:        "personal_auction_amount",
			Description: "个人-集合大成交",
			Func:        divAuctionAmount,
		}).Personal(&Filter{
			Freq:        20 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(9, 25, 5),
			Name:        "personal_auction_open",
			Description: "个人-集合大幅高开",
			Func:        divAuctionOpen,
		}).Personal(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "personal_new_highest",
			Description: "个人-新高",
			Func:        divNewHighest,
		}).Personal(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "personal_amount_incr",
			Description: "个人-大成交额上涨",
			Func:        divAmountIncr,
		}).Personal(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "personal_recommend_incr",
			Description: "个人-业绩推荐股票近10个交易日涨幅1-3",
			Func:        recommendStockIncreaseOne2Four,
			Type:        message.Message_Home_Performance.String(),
		}).Personal(&Filter{
			Freq:        10 * time.Hour,
			SecBegin:    sec(9, 25, 0),
			SecEnd:      sec(15, 31, 0),
			Name:        "personal_recommend_incr",
			Description: "个人-业绩推荐股票近10个交易日涨幅4",
			Func:        recommendStockIncreaseFour,
			Type:        message.Message_Home_Performance.String(),
		})

		//// for test
		//for i := 0; i < 100; i++ {
		//	go Instance.procStock()
		//}

		// 11.19 add stock server
		if setting.ServerSetting.Stock == "on" {
			if setting.ServerSetting.RunMode != "debug" {
				// 0819 optimize: automatically adjust goroutine number
				go quantization.FetchAll()
				go autoProc()
			}
		}
	}
}

func (p *Stock) Use(f *Filter) *Stock {
	f.conn = gredis.Clone(setting.RedisSetting.StockDB)
	if f.Type == "" {
		f.Type = "stock"
	}

	p.filters = append(p.filters, f)
	f.pushMutex = 0

	//for group
	if f.Group != "" {
		c := p.group[f.Group]
		p.group[f.Group] = append(c, f)
	}

	return p
}

func (p *Stock) Global(f *Filter) *Stock {
	f.conn = gredis.Clone(setting.RedisSetting.StockDB)
	if f.Type == "" {
		f.Type = "stock"
	}
	//f.sec = new(sync.Map)
	p.global = append(p.global, f)
	f.pushMutex = 0
	return p
}

func (p *Stock) Personal(f *Filter) *Stock {
	f.conn = gredis.Clone(setting.RedisSetting.StockDB)
	if f.Type == "" {
		f.Type = "stock"
	}

	p.personal = append(p.personal, f)
	f.pushMutex = 0
	return p
}

func autoProc() {
	// max size
	var chanSize int = 4000
	var targetNum int = 10
	var procNum int = 0
	var maxSize int = 600
	ticker := time.NewTicker(2 * time.Second)
	defer ticker.Stop()

	conn := gredis.Clone(setting.RedisSetting.StockDB)
	////Test
	//go DivTest()
	for {
		for ; procNum < targetNum; procNum++ {
			go Instance.procStock()
		}
		<-ticker.C
		sz := len(stock.TickChan)
		conn.HSet("system:param", "tick_chan", sz)
		conn.HSet("system:param", "targetNum", targetNum)
		conn.HSet("system:param", "procNum", procNum)
		conn.HSet("system:param", "chanSize", chanSize)
		if sz > chanSize/2 {
			if targetNum < maxSize {
				targetNum = targetNum * 2
				logging.Info("stock.autoProc() resize chanSize to ", targetNum)
			} else {
				logging.Info("stock.autoProc() resize reached max size")
			}
		}
	}

}

func (p *Stock) procStock() {
	for {
		sTick := <-stock.TickChan
		tick := toTick(&sTick)
		if !inTrade() {
			//不在交易日
			logging.Warn("stock.procStock() received non-trading day data: ", tick)
			continue
		}
		if tick.NewPrice == 0 {
			//现价为0
			logging.Warn("stock.procStock() received zero data: ", tick)
			continue
		}
		for j := 0; j < len(p.global); j++ {
			u := p.global[j]
			go u.Func(tick, u.conn)
		}
		if tick.filter() == nil {
			continue
		}
		for i := 0; i < len(p.filters); i++ {
			v := p.filters[i]
			if bProcess := checkProcess(v, tick); bProcess {
				if !checkOccupy(v, tick) {
					continue
				}
				tsNow := time.Now()
				go func(s *Filter, tick *Tick, t time.Time) {
					if s.Func == nil {
						return
					}
					if content := s.Func(tick, s.conn); content != nil && len(content) > 0 {
						//exclude ST
						if isST(tick, nil) {
							return
						}
						if bPush := checkPush(s, tick); bPush {
							// log
							logging.Info("stock.procStock() before check delay: ", s.Name, tick.Name, time.Now().Format(util.YMDHMS))
							if !checkDelay(t, tick, s.Name) {
								return
							}
							// log
							logging.Info("stock.procStock() after check delay: ", s.Name, tick.Name, time.Now().Format(util.YMDHMS))
							if tp, _ := util.GetString(content, "type"); tp != "" {
								// log
								logging.Info("stock.procStock() before after_func: ", s.Name, tick.Name, time.Now().Format(util.YMDHMS))
								if (s.AfterFunc == nil || s.DuplicatePush) && tp != "pool" {
									pushStock(tick.Code, s.Type, content)
								}
								if s.AfterFunc != nil {
									if content = s.AfterFunc(tick, s.conn); content != nil && len(content) > 0 {
										// log
										logging.Info("stock.procStock() before push: ", s.Name, tick.Name, time.Now().Format(util.YMDHMS), s.Type, content)
										pushStock(tick.Code, s.Type, content)
									}
								}
							}
							if s.PoolOut > 0 {
								tpo := tradeDateAdd(s.PoolOut)
								if s.PoolOutTrunc {
									pool(tick, s.Description, s.conn, time.Date(tpo.Year(), tpo.Month(), tpo.Day(), 0, 0, 0, 0, time.Local))
								} else {
									pool(tick, s.Description, s.conn, time.Date(tpo.Year(), tpo.Month(), tpo.Day(), tpo.Hour(), tpo.Minute(), tpo.Second(), tpo.Nanosecond(), time.Local))
								}
							}
						} else {
							//_ = s.conn.HSet("stock:pre_push:"+s.Name, tick.Code, time.Now().Format("2006-01-02 15:04:05"))
						}
					}
				}(v, tick, tsNow)
				resetOccupy(v, tick)
			}
		}

		for i := 0; i < len(p.personal); i++ {
			v := p.personal[i]
			if bProcess := checkProcess(v, tick); bProcess {
				if !checkOccupy(v, tick) {
					continue
				}
				tsNow := time.Now()
				go func(s *Filter, tick *Tick, t time.Time) {
					if s.Func == nil {
						return
					}
					if content := s.Func(tick, s.conn); content != nil && len(content) > 0 {
						//exclude ST
						if isST(tick, nil) {
							return
						}
						if !checkDelay(t, tick, s.Name) {
							return
						}
						if bPush := checkPush(s, tick); bPush {
							//_ = s.conn.HSet(s.KeyName(), tick.Code, time.Now().Format("2006-01-02 15:04:05"))
							desc, _ := util.GetString(content, "description")
							if s.AfterFunc == nil || s.DuplicatePush {
								home.PushByCode(content, tick.Code, desc)
							}
						}
						//_ = s.conn.HSet("stock:personal:pre_push:"+s.Name, tick.Code, time.Now().Format("2006-01-02 15:04:05"))
					}
				}(v, tick, tsNow)
				resetOccupy(v, tick)
			}
		}
	}
}

func Cron() {
	go fetchPlatform()
	go fetchBlock()
	go positivity()
	go updateLimit()
	go fetchCompanyIndustry()
	go blockPlatform()
	go ma3Platform()
	go perf()
	go passivePositivePlatform()
	go longPositiveShockPlatform()
	go strong()
	go blockOpen()
	go notice()
	go rsi()
	go tradeDate()
	go preNew()
	go rsiEx()
	go importantStockListNew()
	go clean()
	go setHsIndustry()
	go lastPriceAvg()
}

func monitor(con string, c *websocket.Connection) {
	conn := c
	ticker := time.NewTicker(1 * time.Second)
	defer func() {
		ticker.Stop()
	}()
	for {
		<-ticker.C
		if conn == nil {
			if setting.StockSetting.Source == "inner" {
				conn = internalConnect(con)
			} else {
				conn = connect(con)
			}
			// 0806 fix: nil conn
			continue
		}
		if !conn.Available() {
			logging.Info("stock.monitor() Conn unavailable, reconnect...")
			conn.Close()
			if setting.StockSetting.Source == "inner" {
				conn = internalConnect(con)
			} else {
				conn = connect(con)
			}
		}
	}
}

func connect(con string) *websocket.Connection {
	ws := websocket.GetInstance()
	conn := ws.Client(setting.StockSetting.Scheme, setting.StockSetting.Address, setting.StockSetting.Path, setting.StockSetting.Query, con, false)
	if conn == nil {
		logging.Error("stock.connect() nil conn")
		return nil
	}
	login(con)
	unsubscribe(con)
	subscribe(con)
	return conn
}

func internalConnect(con string) *websocket.Connection {
	ws := websocket.GetInstance()
	conn := ws.Client(setting.StockSetting.Scheme, setting.StockSetting.Address, setting.StockSetting.Path, setting.StockSetting.Query, con, true)
	if conn == nil {
		logging.Error("stock.internalConnect() nil conn")
		return nil
	}

	return conn
}

func login(con string) {
	ws := websocket.GetInstance()
	conn := ws.GetConnection(con)
	if conn == nil {
		return
	}
	ctx, _ := json.Marshal(map[string]interface{}{
		"event":    "login",
		"needSign": true,
		"data":     map[string]interface{}{},
	})
	conn.Send(message.ParseMsg(message.Message_Tick, "system", "", string(ctx), false))
}

func subscribe(con string) {
	ws := websocket.GetInstance()
	conn := ws.GetConnection(con)
	if conn == nil {
		return
	}
	ctx, _ := json.Marshal(map[string]interface{}{
		"event":    "subscribe",
		"needSign": false,
		"data": map[string]interface{}{
			"market": con,
		},
	})
	conn.Send(message.ParseMsg(message.Message_Tick, "system", "", string(ctx), false))
}

func unsubscribe(con string) {
	ws := websocket.GetInstance()
	conn := ws.GetConnection(con)
	if conn == nil {
		return
	}
	ctx, _ := json.Marshal(map[string]interface{}{
		"event":    "unsubscribe",
		"needSign": false,
		"data": map[string]interface{}{
			"market": con,
		},
	})
	conn.Send(message.ParseMsg(message.Message_Tick, "system", "", string(ctx), false))
}

func sec(hour, minute, second int) int {
	return 3600*hour + 60*minute + second
}

// fix: 已推送过的股票
// 并不一定不需要执行处理逻辑
// bp-是否在处理时间 bt-是否在推送时间
func checkProcess(f *Filter, tick *Tick) bool {
	bProcess := false
	t := sec(time.Now().Hour(), time.Now().Minute(), time.Now().Second())
	if t < f.SecBegin || t > f.SecEnd {
		bProcess = false
	} else {
		bProcess = true
	}
	return bProcess
}

func checkPush(f *Filter, tick *Tick) bool {
	//f.pushLocker.Lock()
	//defer f.pushLocker.Unlock()
	for !atomic.CompareAndSwapInt64(&f.pushMutex, 0, 1) {
		timer := time.NewTimer(1 * time.Second)
		<-timer.C
	}
	defer atomic.StoreInt64(&f.pushMutex, 0)

	bPush := false
	tm, err := f.conn.HGetString(f.KeyName(), tick.Code)
	if err == nil && tm != "" {
		ts, e := time.ParseInLocation("2006-01-02 15:04:05", tm, time.Local)
		if e != nil {
			logging.Error("stock.checkTime() Parse time Errors: ", e.Error())
		} else {
			if time.Now().Sub(ts) < f.Freq {
				bPush = false
			} else {
				bPush = true
			}
		}
	} else {
		bPush = true
	}

	if bPush {
		_ = f.conn.HSet(f.KeyName(), tick.Code, time.Now().Format("2006-01-02 15:04:05"))
	} else {
		_ = f.conn.HSet("stock:pre_push:"+f.Name, tick.Code, time.Now().Format("2006-01-02 15:04:05"))
	}
	return bPush
}

func toTick(tick *stock.Tick) *Tick {
	bc, _ := util.GbkToUtf8(tick.Symbol[0:])
	tsCode := toString(bc)
	code := toString(bc[2:])

	bn, _ := util.GbkToUtf8(tick.RawName[0:])
	name := toString(bn)
	ci := util.Atoi(code)
	if ci < 0 || ci >= 1000000 {
		ci = 0
	}

	return &Tick{
		Tick:   tick,
		TsCode: tsCode,
		Code:   code,
		Name:   name,
		Number: int64(ci),
	}
}

// 去除非沪深数据
func exclude(tick *stock.Tick) bool {
	bc, _ := util.GbkToUtf8(tick.Symbol[0:])
	if bc[0] != 'S' && bc[0] != 's' {
		return true
	}
	if bc[2] != '0' && bc[2] != '6' && bc[2] != '3' {
		return true
	}
	return false
}

func toString(bt []byte) string {
	ret := make([]byte, 0)
	for i := 0; i < len(bt) && bt[i] != 0; i++ {
		ret = append(ret, bt[i])
	}
	return string(ret)
}

func (tick *Tick) filter() *Tick {
	//if len([]byte(tick.Code)) < 6 {
	//	return true
	//}
	//c := []byte(tick.Code)[0]
	//if c != '0' && c != '6' && c != '3' {
	//	return true
	//}
	if exclude(tick.Tick) {
		return nil
	}

	if strings.HasPrefix(tick.TsCode, "SH0") || strings.HasPrefix(tick.TsCode, "SH3") || strings.HasPrefix(tick.Code, "39") {
		return nil
	}

	////exclude ST
	//if isST(tick, nil) {
	//	return nil
	//}

	//科创板bv需除100（期望单位：手，传入单位：笔）
	if strings.HasPrefix(tick.TsCode, "SH688") || strings.HasPrefix(tick.TsCode, "SH689") {
		for i := 0; i < len(tick.BV); i++ {
			tick.BV[i] = float32(math.Floor(float64(tick.BV[i]/100) + 0.5))
		}
	}
	return tick
}

func (f *Filter) KeyName() string {
	if f.Group == "" {
		return "stock:push:" + f.Name
	} else {
		return "stock:push:group:" + f.Group
	}
}

func tradeDateAdd(day int) time.Time {
	t := time.Now()
	for i := 0; i < day; {
		t = t.Add(24 * time.Hour)
		if t.Weekday() == time.Saturday || t.Weekday() == time.Sunday {
			continue
		}
		i++
	}
	return t
}

func pushStock(code, contentType string, content map[string]interface{}) {
	if contentType == "performance" {
		pushPerf(content)
	} else {
		// log
		logging.Info("stock.pushStock() before push: ", code, contentType, content, time.Now().Format(util.YMDHMS))
		push(content)
	}
}

func checkOccupy(f *Filter, tick *Tick) bool {
	if !atomic.CompareAndSwapInt64(&f.locker[tick.Number], 0, 1) {
		logging.Info("stock.checkOccupy occupies: ", f.Name, tick.Code)
		return false
	}
	return true
}

func resetOccupy(f *Filter, tick *Tick) {
	//// unlock
	atomic.StoreInt64(&f.locker[tick.Number], 0)
}

func checkDelay(t time.Time, tick *Tick, name string) bool {
	sub := time.Now().Sub(t)
	if sub > 5*time.Second {
		logging.Error("stock.procStock() delays: ", sub.Seconds(), tick.Code, tick.NewPrice, t.Format("15:04:05.999"), time.Now().Format("15:04:05.999"), name)
		if sub > 60*time.Second {
			return false
		}
	}
	return true
}
