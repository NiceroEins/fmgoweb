package stock

import (
	"datacenter/models/home"
	"fmt"
	"time"
)

func DivTest() {
	ticker := time.NewTicker(10 * time.Second)
	for {
		<-ticker.C
		home.PushByCode(map[string]interface{}{
			"type":    "stock",
			"content": fmt.Sprintf("同花顺：突破历史新高"),
		}, "300033", "")
	}
}
