package quantization

import (
	"datacenter/models"
	"datacenter/models/quantization"
)

func calc(code string) {
	ts := "0"
	quantization.BOLL(code, ts)
	quantization.MACD(code, ts)
	quantization.KDJ(code, ts)
	quantization.RSI(code, ts)
}

func stockCodes() []string {
	var ret []string
	models.Data().Table("p_stock_tick").Select("DISTINCT(stock_code) AS code").Where("stock_code IS NOT NULL").Pluck("code", &ret)
	return ret
}

func GenerateTechnical() {
	codes := stockCodes()
	for _, v := range codes {
		calc(v)
	}
}
