package quantization

import (
	"datacenter/models/quantization"
	"datacenter/pkg/logging"
	"datacenter/pkg/util"
	"encoding/json"
	"fmt"
	"reflect"
	"sort"
	"sync"
	"time"
)

type HighestResp struct {
	Data  []quantization.HighestResp `json:"data"`
	Total int                        `json:"total"`
}

type HighestDetailResp struct {
	Data     []quantization.HighestDetailResp `json:"data"`
	PageNum  int                              `json:"p"`
	PageSize int                              `json:"n"`
	Total    int                              `json:"total"`
	Key      string                           `json:"key"`
}

var cache sync.Map

func Analyze(hr *quantization.HighestReq) (HighestResp, error) {
	var data []quantization.HighestDetailResp
	var raw []quantization.HighestDetailResp
	hrp := make([]quantization.HighestResp, len(hr.Period))
	ret := make([]quantization.HighestResp, 0)
	highest := quantization.Analyze(hr)

	for i := 0; i < len(hr.Period); i++ {
		hrp[i].MinRate = 100
		hrp[i].MaxRate = -100
	}
	for _, v := range highest {
		date, err := time.ParseInLocation(time.RFC3339, v.Date, time.Local)
		if err != nil {
			logging.Error("quantization.Analyze() Parse time Errors: ", err.Error())
			continue
		}
		//rsi check
		if !rsiCheck(v.Code, hr.RSI, date, hr.RsiDetail) {
			continue
		}
		for k, day := range hr.Period {
			hdr := quantization.Calc(v, day, highest)
			if hdr != nil {
				hdr.ID = quantization.GenerateID(hdr.Code, hdr.TimeBegin, hdr.TimeEnd)
				if quantization.HaveType(v.Types, "industry") {
					bInsert := false
					hdr.Industry = v.SecondIndustry
					hdr.TradeID = v.TradeID
					for i := 0; i < len(data); i++ {
						if data[i].Industry == v.SecondIndustry && data[i].Code != v.Code && data[i].Day == day && v.TradeID < data[i].TradeID+5 && v.TradeID > data[i].TradeID-5 {
							hdr.ID = quantization.GenerateID(hdr.Code, hdr.TimeBegin, hdr.TimeEnd)
							data[i].Children = append(data[i].Children, *hdr)
							bInsert = true
							//break
						}
					}
					if !bInsert {
						data = append(data, *hdr)
					}
				} else {
					data = append(data, *hdr)
				}
				if k == 0 {
					raw = append(raw, *hdr)
				}
				hrp[k].Period = day
				hrp[k].Num++
				hrp[k].Sum += hdr.Change
				if hdr.Change > hrp[k].MaxRate {
					hrp[k].MaxRate = hdr.Change
				}
				if hdr.Change < hrp[k].MinRate {
					hrp[k].MinRate = hdr.Change
				}
				if hdr.Change > 0 {
					hrp[k].RisingNum++
				}
			}
		}
	}

	var fData []quantization.HighestDetailResp
	for i := 0; i < len(data); i++ {
		if data[i].Industry != "" {
			if data[i].Children == nil || len(data[i].Children) == 0 {
				continue
			}
		}
		fData = append(fData, data[i])
	}

	// store
	cache.Store(generateKey(hr), fData)
	cache.Store(generateKey(hr)+"_raw", raw)

	for i := 0; i < len(hrp); i++ {
		if hrp[i].Num == 0 {
			continue
		}
		hrp[i].AvgRate = hrp[i].Sum / float64(hrp[i].Num)
		hrp[i].RiseProb = float64(hrp[i].RisingNum) / float64(hrp[i].Num)
		ret = append(ret, hrp[i])
	}
	return HighestResp{
		Data:  ret,
		Total: len(ret),
	}, nil
}

func Detail(hr *quantization.HighestDetailReq) (HighestDetailResp, error) {
	var ret HighestDetailResp
	var data []quantization.HighestDetailResp

	itf, ok := cache.Load(generateKey(&hr.HighestReq))
	if !ok {
		return ret, nil
	}

	hdr, ok := itf.([]quantization.HighestDetailResp)
	if !ok {
		return ret, nil
	}

	for _, v := range hdr {
		if v.Day == hr.Day {
			data = append(data, v)
		}
	}

	key := generateKey(&hr.HighestReq) + "_detail"
	cache.Store(key, data)

	if hr.Sort != "" && (hr.Order == "ASC" || hr.Order == "DESC") {
		sort.Slice(data, func(i, j int) bool {
			//if data[i][4] == nil || dto[j][4] == nil {
			//	return dto[i][4] == nil
			//}
			switch hr.Order {
			case "ASC":
				return util.Compare(reflect.ValueOf(data[i]).FieldByName("Change"), reflect.ValueOf(data[j]).FieldByName("Change"))
			case "DESC":
				return util.Compare(reflect.ValueOf(data[j]).FieldByName("Change"), reflect.ValueOf(data[i]).FieldByName("Change"))
			}
			return true
		})
	}

	pageData := data[hr.PageSize*(hr.PageNum-1) : util.Min(hr.PageSize*hr.PageNum, len(data))]

	ret = HighestDetailResp{
		Data:     pageData,
		PageNum:  hr.PageNum,
		PageSize: hr.PageSize,
		Total:    len(data),
		Key:      key,
	}

	return ret, nil
}

func Export(key string) ([]quantization.HighestDetailResp, error) {
	var ret []quantization.HighestDetailResp
	itf, ok := cache.Load(key)
	if !ok {
		return ret, nil
	}

	ret, ok = itf.([]quantization.HighestDetailResp)
	return ret, nil
}

func generateKey(hr interface{}) string {
	b, _ := json.Marshal(hr)
	hash := fmt.Sprintf("%d", util.BKDRHash(string(b)))
	return hash
}

func rsiCheck(code string, rsi []string, t time.Time, detail []quantization.RSIDetail) bool {
	if detail != nil && len(detail) > 0 {
		for _, u := range detail {
			if !check(code, u.Name, t, u.Low, u.High) {
				return false
			}
		}
	} else {
		for _, v := range rsi {
			if !check(code, v, t, 0, 1000) {
				return false
			}
		}
	}
	return true
}

func check(code, v string, t time.Time, low, high float64) bool {
	switch v {
	case "ystb":
		if !quantization.Ystb(code, t, low, high) {
			return false
		}
	case "kfjl":
		if !quantization.Kfjl(code, t, low, high) {
			return false
		}
	case "mllh":
		if !quantization.Mllh(code, t, low, high) {
			return false
		}
	case "jyxj":
		if !quantization.Jyxj(code, t, low, high) {
			return false
		}
	case "yszk":
		if !quantization.Yszk(code, t, low, high) {
			return false
		}
	case "jzcs":
		if !quantization.Jzcs(code, t, low, high) {
			return false
		}
	}
	return true
}
