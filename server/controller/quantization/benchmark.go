package quantization

import (
	"datacenter/models/quantization"
	"datacenter/pkg/util"
	"reflect"
	"sort"
)

type HighestBenchmarkResp struct {
	Data    []quantization.HighestBenchmarkResp  `json:"data"`
	Date    []string                             `json:"date"`    //日期数组
	Summary quantization.HighestBenchmarkSummary `json:"summary"` //统计
	StartID int                                  `json:"start_id"`
	Total   int                                  `json:"total"`
	Key     string                               `json:"key"`
}

func BenchmarkDetail(hr *quantization.HighestBenchmarkReq) (HighestBenchmarkResp, error) {
	var ret HighestBenchmarkResp

	itf, ok := cache.Load(generateKey(&hr.HighestReq) + "_raw")
	if !ok {
		return ret, nil
	}

	data, ok := itf.([]quantization.HighestDetailResp)
	if !ok {
		return ret, nil
	}

	sort.Slice(data, func(i, j int) bool {
		return util.Compare(reflect.ValueOf(data[i]).FieldByName("TimeBegin"), reflect.ValueOf(data[j]).FieldByName("TimeBegin"))
	})

	ret.Data = quantization.Benchmark(hr, data)
	ret.Date = quantization.TradeDate(hr)
	ret.Total = len(ret.Date)
	ret.StartID = hr.StartID
	if len(ret.Data) == len(ret.Date) {
		ret.Summary = quantization.Summary(hr)
	}

	return ret, nil
}
