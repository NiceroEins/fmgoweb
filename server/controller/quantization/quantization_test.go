package quantization

import (
	"datacenter/models"
	"datacenter/models/quantization"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"testing"
)

func Test(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	//quantization.GenerateNewStock()
	//quantization.GeneratePlatform()
	//calc("000001")
	//models.Data().Raw("TRUNCATE `p_stock_highest`")
	//quantization.GenerateHighest()

	//models.Data().Raw("TRUNCATE `p_stock_new`")
	//quantization.GenerateNewStock()

	models.Data().Raw("TRUNCATE `p_stock_platform`")
	quantization.GeneratePlatform()
}

func TestTechnical(t *testing.T) {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()
	util.Setup()

	GenerateTechnical()
}
