package quantization

import (
	"datacenter/models/quantization"
	_ "datacenter/pkg/logging"
	"datacenter/pkg/util"
	"reflect"
	"sort"
)

//
//type QAReq struct {
//	Data     []quantization.QAResp `json:"data"`
//	Total    int                   `json:"total"`
//	PageNum  int                   `json:"p"`
//	PageSize int                   `json:"n"`
//}

type QAResp struct {
	Data     []quantization.QAResp `json:"data"`
	PageNum  int                   `json:"p"`
	PageSize int                   `json:"n"`
	Total    int                   `json:"total"`
	Key      string                `json:"key"`
}

type QADetailResp struct {
	Data     []quantization.QADetailResp `json:"data"`
	PageNum  int                         `json:"p"`
	PageSize int                         `json:"n"`
	Total    int                         `json:"total"`
	Key      string                      `json:"key"`
}

func QAAnalyze(hr *quantization.QAReq) (QAResp, error) {
	//var data []quantization.QAResp
	var fData []quantization.QADetailResp
	hrp := make([]quantization.QAResp, 0)
	ret := make([]quantization.QAResp, 0)
	raw := quantization.QAAnalyze(hr)
	key := generateKey(hr.Req)
	itf, ok := cache.Load(key + "_req")
	if ok {
		hrp, _ = itf.([]quantization.QAResp)
	}

	if len(hrp) > 0 {
		ret = hrp
	} else {
		k := -1
		var lastAuthor string

		for _, v := range raw {
			//date, err := time.ParseInLocation(time.RFC3339, v.Date, time.Local)
			//if err != nil {
			//	logging.Error("quantization.QAAnalyze() Parse time Errors: ", err.Error())
			//	continue
			//}

			hdr := quantization.QACalc(v)
			if hdr != nil {
				if v.Author != lastAuthor {
					k = util.Max(0, k+1)
					if len(hrp) < k+1 {
						hrp = append(hrp, quantization.QAResp{})
					}
					hrp[k].MaxRate = -100
					hrp[k].MinRate = 999
					hrp[k].Author = v.Author
					lastAuthor = v.Author
				}
				hrp[k].Num++
				hrp[k].Sum += hdr.Change
				if hdr.Change > hrp[k].MaxRate {
					hrp[k].MaxRate = hdr.Change
				}
				if hdr.Change < hrp[k].MinRate {
					hrp[k].MinRate = hdr.Change
				}
				if hdr.Change > 0 {
					hrp[k].RisingNum++
				}
				fData = append(fData, *hdr)
			}
		}

		cache.Store(key+"_req", hrp)
		cache.Store(key, fData)

		for i := 0; i < len(hrp); i++ {
			if hrp[i].Num <= 0 {
				continue
			}
			hrp[i].AvgRate = hrp[i].Sum / float64(hrp[i].Num)
			hrp[i].RiseProb = float64(hrp[i].RisingNum) / float64(hrp[i].Num)
			ret = append(ret, hrp[i])
		}
	}

	if hr.Sort != "" && (hr.Order == "ASC" || hr.Order == "DESC") {
		sort.Slice(ret, func(i, j int) bool {
			switch hr.Order {
			case "ASC":
				return util.Compare(reflect.ValueOf(ret[i]).FieldByName(hr.Sort), reflect.ValueOf(ret[j]).FieldByName(hr.Sort))
			case "DESC":
				return util.Compare(reflect.ValueOf(ret[j]).FieldByName(hr.Sort), reflect.ValueOf(ret[i]).FieldByName(hr.Sort))
			}
			return true
		})
	}

	var pageData []quantization.QAResp
	if hr.PageSize*(hr.PageNum-1) >= len(ret) {
		pageData = make([]quantization.QAResp, 0)
	} else {
		pageData = ret[hr.PageSize*(hr.PageNum-1) : util.Min(hr.PageSize*hr.PageNum, len(ret))]
	}

	return QAResp{
		Data:     pageData,
		Total:    len(ret),
		PageNum:  hr.PageNum,
		PageSize: hr.PageSize,
	}, nil
}

func QADetail(hr *quantization.QADetailReq) (QADetailResp, error) {
	var ret QADetailResp
	var data []quantization.QADetailResp

	itf, ok := cache.Load(generateKey(hr.Req))
	if !ok {
		return ret, nil
	}

	hdr, ok := itf.([]quantization.QADetailResp)
	if !ok {
		return ret, nil
	}

	for _, v := range hdr {
		if v.Author == hr.Author {
			if hr.Succeed == "yes" && v.Change > 0 || hr.Succeed == "all" {
				data = append(data, v)
			}
		}
	}

	//key := util.GenerateRandomString(16)
	//cache.Store(key, data)

	if hr.Sort != "" && (hr.Order == "ASC" || hr.Order == "DESC") {
		sort.Slice(data, func(i, j int) bool {
			switch hr.Order {
			case "ASC":
				return util.Compare(reflect.ValueOf(data[i]).FieldByName(hr.Sort), reflect.ValueOf(data[j]).FieldByName(hr.Sort))
			case "DESC":
				return util.Compare(reflect.ValueOf(data[j]).FieldByName(hr.Sort), reflect.ValueOf(data[i]).FieldByName(hr.Sort))
			}
			return true
		})
	}

	pageData := data[hr.PageSize*(hr.PageNum-1) : util.Min(hr.PageSize*hr.PageNum, len(data))]

	ret = QADetailResp{
		Data:     pageData,
		PageNum:  hr.PageNum,
		PageSize: hr.PageSize,
		Total:    len(data),
	}

	return ret, nil
}

func syncProc(n int) {
	for i := 0; i < n; i++ {
		go func() {

		}()
	}
}
