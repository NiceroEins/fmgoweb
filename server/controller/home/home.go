package home

import (
	"datacenter/models"
	"datacenter/models/home"
	"datacenter/models/template"
	"datacenter/pkg/gredis"
	"datacenter/pkg/logging"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/service/crud_service"
	"datacenter/service/home_service"
	"github.com/shopspring/decimal"
	"strconv"
	"time"
)

func GetMyAnnouncementEventList(qo *home.AnnouncementEventQO) ([]crud_service.VO, int, error) {
	vos := make([]crud_service.VO, 0)
	dtos, cnt, err := home.GetMyAnnouncementList(qo)
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range dtos {
		vo := dto.DTO2VO()
		vos = append(vos, vo)
	}
	return vos, cnt, nil
}

func GetMyResearchEventList(qo *home.ResearchEventQO) (crud_service.VO, error) {
	vos := make([]crud_service.VO, 0)
	dtos, err := home.GetMyResearchList(qo)
	if err != nil {
		return nil, err
	}
	for _, dto := range dtos {
		vo := dto.DTO2VO()
		vos = append(vos, vo)
	}
	return vos, nil
}

func SetUserIndustry() {
	storeData := []home_service.IndustryNameAdd{}
	data := make(map[int][]home_service.IndustryNameAdd)
	_ = models.DB().Table("u_second_industry_user").Where("deleted_at is null").Find(&storeData)
	for _, value := range storeData {
		data[value.UserId] = append(data[value.UserId], value)
	}
	for index, value := range data {
		rediskey := "my_industry_list:" + strconv.Itoa(index)
		gredis.Set(rediskey, value, 9*3600)
	}
}

func GetMyInvestigationList(qo *home.InvestigationUCQO) (crud_service.VO, error) {
	vos := make([]crud_service.VO, 0)
	dtos, err := home.GetInvestigationList(qo)
	if err != nil {
		return nil, err
	}
	for _, dto := range dtos {
		vo := dto.DTO2VO()
		vos = append(vos, vo)
	}
	return vos, nil
}

func GetIndustryMonitorList(req *home.IndustryListReq) ([]home.Industry, int, error) {
	return home.GetIndustryMonitor(req)
}

func GetIndustryPic(req *home.IndustryPicReq) ([]home.IndustryPic, decimal.NullDecimal, map[string]interface{}, error) {
	return home.GetIndustryPic(req)
}

func GetIndustryPicPerMin(req *home.IndustryPicReq) ([]home.IndustryPic, decimal.NullDecimal, map[string]interface{}, error) {
	return home.GetIndustryPicPerMinute(req)
}

func GetTop(req *home.IndustryStockReq) (home.Statistics, error) {
	return home.GetTopData(req)
}
func GetIndustyStock(req *home.IndustryStockReq) ([]home.IndustryStock, int, error) {
	return home.GetIndustryStock(req)
}
func GetIndustryHistoryEvent(req *home.IndustryStockReq) ([]template.HistroyEvent, int, error) {
	return home.GetHistroyEvent(req)
}

// 存储主表数据
func StoreIndustryStockMinute() {
	// 交易日跑定时任务
	if setting.ServerSetting.Stock == "on" {
		if home.GetStockTradeDate() {
			date := home.GetTimeDuration(time.Now())
			if util.ContainsAny(date, time.Now().Format(util.YMDHM)) {
				home.StoreIndustryStockMinute()
			}
		}
	}

}

// 存储统计数据
func StoreIndustryStockStatistics() {
	// 交易日定时任务
	if setting.ServerSetting.Stock == "on" {
		if home.GetStockTradeDate() {
			date := home.GetTimeDuration(time.Now())
			auctionDate := home.GetTickAuctionDuraion()
			if util.ContainsAny(date, time.Now().Format(util.YMDHM)) || util.ContainsAny(auctionDate, time.Now().Format("15:04")) {
				home.StoreInustryStatistics()
			}
		}
	}
}

func StoreIndustryStockStatisticsTest() {
	home.StoreInustryStatisticsTest()
}

// 更新收盘价
func UpdateClose() {
	//home.UpdateIndustryPriceTest()
	if setting.ServerSetting.Stock == "on" {
		if time.Now().Hour() < 16 && time.Now().Hour() >= 15 {
			home.UpdateIndustryPrice()
		}
	}
}

func GetNetProfitChangePushList(qo *home.NetProfitChangePushQO) ([]crud_service.VO, int, error) {
	vos := make([]crud_service.VO, 0)
	data, cnt, err := home.GetList(qo)
	if err != nil {
		return nil, 0, err
	}
	for _, dto := range data {
		vo := dto.DTO2VO()
		vos = append(vos, vo)
	}
	return vos, cnt, nil
}

// 行业研报更新行业id
func UpdateReportIndustryId() {
	// 获取5分钟前的行业研报
	type ResearchReport struct {
		Id         int    `json:"id" gorm:"column:id"`
		Industry   string `json:"industry" gorm:"column:industry"`
		IndustryId int    `json:"industry_id" gorm:"column:industry_id"`
	}
	type FirstIndustry struct {
		Id           int    `json:"id" gorm:"column:id"`
		IndustryName string `json:"industry_name" gorm:"column:industry_name"`
	}
	type ResearchReportIndusty struct {
		FirstId      int    `json:"first_id" gorm:"column:first_id"`
		IndustryName string `json:"industry_name" gorm:"column:industry_name"`
	}
	type SecondIndustry struct {
		Id           int    `json:"id" gorm:"column:id"`
		IndustryName string `json:"name" gorm:"column:name"`
	}
	research := []ResearchReport{}
	var retErr error
	err := models.DB().Table("u_research_report").
		Where("updated_at >= ? ", time.Now().Add(-6*60)).
		Where("code = ''").Find(&research).Error
	if err != nil {
		return
	}
	for _, value := range research {
		first := FirstIndustry{}
		// 从一级行业表获取id
		err = models.DB().Table("u_first_industry").Where("name = ? ", value.Industry).First(&first).Error
		if err == nil {
			retErr = models.DB().Table("u_research_report").Where("id = ? ", value.Id).
				Updates(map[string]int{"industry_id": first.Id}).Error
			if retErr != nil {
				logging.Info("update research report err ", retErr)
			}
			continue
		}
		report := FirstIndustry{}
		// 从研报行业表获取
		err = models.DB().Table("u_research_report_industry").Where("industry_name = ? ", value.Industry).First(&report).Error
		if err == nil {
			retErr = models.DB().Table("u_research_report").Where("id = ? ", value.Id).
				Updates(map[string]int{"industry_id": report.Id}).Error
			if retErr != nil {
				logging.Info("update research report err ", retErr)
			}
			continue
		}
		// 从二级行业获取
		second := SecondIndustry{}
		err = models.DB().Table("u_performance_industry").Where("industry_name = ? ", value.Industry).First(&second).Error
		if err == nil {
			retErr = models.DB().Table("u_research_report").
				Where("id = ? ", value.Id).Updates(map[string]int{"industry_id": second.Id}).Error
			if retErr != nil {
				logging.Info("update research report err ", retErr)
			}
			continue
		}
		logging.Info("update research report err ", err)
	}
}
