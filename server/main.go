package main

import (
	"datacenter/controller/cron"
	message "datacenter/middleware/message"
	"datacenter/pkg/app"
	"datacenter/pkg/logging"
	"datacenter/pkg/websocket"
	"fmt"
	"log"
	"net/http"
	// _ "net/http/pprof"

	_ "datacenter/docs"
	"datacenter/models"
	"datacenter/pkg/setting"
	"datacenter/pkg/util"
	"datacenter/routers"
	"github.com/gin-gonic/gin"
)

func init() {
	setting.Setup()
	logging.Setup()
	//gredis.Setup()
	models.Setup()
	//controller.Setup()
	//cache.Setup()
	util.Setup()
	websocket.Setup()
	app.Init()
	message.Setup()
	//stock.Setup()
	//mongo.Setup()
	//home.SetUserIndustry()
	cron.Setup()
}

// @title 昊晟应用服务API
// @version 1.0
// @description Hao Sheng Internal API System
func main() {
	gin.SetMode(setting.ServerSetting.RunMode)

	routersInit := routers.InitRouter()
	readTimeout := setting.ServerSetting.ReadTimeout
	writeTimeout := setting.ServerSetting.WriteTimeout
	endPoint := fmt.Sprintf(":%d", setting.ServerSetting.HttpPort)
	maxHeaderBytes := 1 << 20

	server := &http.Server{
		Addr:           endPoint,
		Handler:        routersInit,
		ReadTimeout:    readTimeout,
		WriteTimeout:   writeTimeout,
		MaxHeaderBytes: maxHeaderBytes,
	}

	log.Printf("[info] start http server listening %s", endPoint)
	//go func() {
	//	http.ListenAndServe(":8008", nil)
	//}()
	server.ListenAndServe()

	// need Unix system, using github.com/fvbock/endless
	//endless.DefaultReadTimeOut = readTimeout
	//endless.DefaultWriteTimeOut = writeTimeout
	//endless.DefaultMaxHeaderBytes = maxHeaderBytes
	//server := endless.NewServer(endPoint, routersInit)
	//server.BeforeBegin = func(add string) {
	//	log.Printf("Actual pid is %d", syscall.Getpid())
	//}
	//
	//err := server.ListenAndServe()
	//if err != nil {
	//	log.Printf("Server err: %v", err)
	//}
}
